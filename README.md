Video Surveillance framework based on Qt/C++ & OpenCV

* Flexible architecture structured with Nodes, Server and Clients.
* Retrieves MJPEG RTP Stream
* Retrieves Low-Level image processing (background and tracking algorithms) from the S400 and ViSmart FPGA board
* OpenCV based plugin integration for high level video analysis on received stream.
* Centralizes alarms and events in the server database (PostgreSQL).


Structure
============

* `CvS400/advisor`: The main Qt/C++ GUI software
* `CvS400/x7s`: Core libraries for OpenCV, network, logging and XML.
* `CvS400/doc`: Files to generate the documentation using doxygen and Cmake. 
* `S7PReceiver`: Simple software to retrieve frame using the S7P (Smart Sevensols Protocol)
* `S4NPSimu`: Simulate the network protocol of S400 board.


Pre-requist
==============

* Platform: WinXP, Ubuntu (8.10)
* CMake to compile the software
* Qt for advisor software
* PostGreSQL for advisor software
* Doxygen to generate documentation
* OpenCV (x7cv)
* pcap (Winpcap or LibPcap)


First-steps
=================

1. Install Cmake
1. Install PCap
1. Install TinyXML
1. Install LibJPEG
1. Install OpenCV 2.x
1. Install PostGreSQL
1. Install Qt
1. Open Cmake GUI
1. Select `CvS400` as source code
1. Create the build directory outside the source tree
1. Click Configure and then Generate

Then you can also:

1. Install Doxygen
1. Install valgrind
1. Run make doc to generate the documentation
