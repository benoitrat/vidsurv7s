#!/bin/sh

## Copyright (C) 2001-2009 Roberto Bagnara
## http://www.cs.unipr.it/~bagnara/

if [ $# -ne 1  ]; then
        echo "Usage: `basename $0` file.bib" 1>&2
        exit 1
fi
# Note: this has been tested with bibtex2html version 1.92.

gen_file=../doc/mainpages/`basename $1 .bib`.h


echo "/** \n @page page_biblio Bibliography
" > $gen_file
bibtex2html -no-header -quiet -sort-as-bibtex -nodoc -noabstract \
            -nobibsource -nokeywords -style alpha --output - \
            -named-field SOURCE "Source code" $1 \
    | sed -f gen_dox_bibli.sed >> $gen_file
echo "**/" >> $gen_file

echo "Generate $gen_file"
