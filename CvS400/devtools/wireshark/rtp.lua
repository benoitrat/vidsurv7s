-- register RTP to handle ports 50000-50011
do
	local udp_port_table = DissectorTable.get("udp.port")
	local rtp_dissector = Dissector.get("rtp")
	for port=50000,50011 do
		udp_port_table:add(port,rtp_dissector)
	end
end
