-- S7P Protocol: Last modified 19/05/2009
-- dissects the S7P Protocol
packetnum = -1
s7p_proto = Proto("s7p","S7P","Smart Sevensols Protocol")
-- create a function to dissect it
function s7p_proto.dissector(buffer,pinfo,tree)
    pinfo.cols.protocol = "S7N"


	ar_cmd = {
		S7P_CMD_UNKOWN = 0x00, S7P_CMD_DEBUG = 0xFF,
		S7P_CMD_BOARD_ASK = 0x10, S7P_CMD_BOARD_ACK = 0x11, 
		S7P_CMD_PARAM_RST = 0x20, S7P_CMD_PARAM_APPLY = 0x21, S7P_CMD_PARAM_SET = 0x22, S7P_CMD_PARAM_GET = 0x23, S7P_CMD_PARAM_ACK = 0x24, S7P_CMD_PARAM_ERR = 0x25,
		S7P_CMD_SEND_SLOW = 0x80, S7P_CMD_SEND_FAST = 0x81, S7P_CMD_SEND_STOP = 0x82, S7P_CMD_SEND_START = 0x83, S7P_CMD_SEND_START_RTP = 0x84, S7P_CMD_SEND_ACK = 0x85,	
		S7P_CMD_FRAME_SETUP = 0xA0, S7P_CMD_FRAME_BLOCK = 0xA1, S7P_CMD_FRAME_LAST = 0xA2, S7P_CMD_FRAME_ACKS = 0xA3, 
		S7P_CMD_MDATA_FIRST = 0xB0, S7P_CMD_MDATA_BLOCK = 0xB1, S7P_CMD_MDATA_LAST = 0xB2, S7P_CMD_MDATA_ACK = 0xB3
	}


	ar_error = {
		S7P_ERR_UNKOWN=-1,	
		S7P_ERR_PRM_NOTDEF=-2,	
		S7P_ERR_PRM_READONLY=-3,
		S7P_ERR_PRM_OUTRANGE=-4,
		S7P_ERR_CAMID_NOTDEF=-5,
		S7P_ERR_CMD_NOTDEF=-6,			
	}
	
	ar_param = {
		
		S7P_PRM_RO_VFW=0x8000,	
		S7P_PRM_RO_VS7P=0x8001,		
		S7P_PRM_RO_NOFCAM=0x8002,		
		S7P_END_PRM_RO=0x8003,

		S7P_PRM_BRD_IP=0x8010,	
		S7P_PRM_BRD_PORT_S7P=0x8011,
		S7P_PRM_BRD_DHCP_ON=0x8012,
		S7P_PRM_BRD_MODES=0x8013,
		S7P_PRM_BRD_DTYPE=0x8014,
		S7P_PRM_BRD_WSIZE=0x8015,
		S7P_PRM_BRD_FRAMERATE=0x8016,
		S7P_PRM_BRD_PORT_RTP=0x8017,
		S7P_PRM_BRD_SSRCID=0x8018,
				
		S7P_PRM_CAM_ENABLE=0x8020,	
		S7P_PRM_CAM_RESO=0x8021,
		S7P_PRM_CAM_DTYPE=0x8022,
		S7P_PRM_CAM_MTYPE=0x8023,	
		S7P_PRM_CAM_QUALITY=0x8024,
		S7P_PRM_CAM_SWITCH_LED=0x8025,	
		S7P_PRM_CAM_BG_THRS=0x8026,	
		S7P_PRM_CAM_BG_INSERT=0x8027,
		S7P_PRM_CAM_BG_REFRESH=0x8028,
		S7P_PRM_CAM_FG_REFRESH=0x8029,
		S7P_PRM_CAM_CC_MINAREA=0x802A,
		S7P_PRM_CAM_TMP1=0x802B,
		S7P_PRM_CAM_TMP2=0x802C,
		S7P_PRM_CAM_TMP3=0x802D,
		S7P_END_PRM_CAM=0x802E,
			
		S7P_PRM_UNKNOWN=0x8060,
	}

	


	zeros = buffer(0,2):uint()
	if zeros == 0 then 
		start=2
		start_txt=" + 2"
	else 
		start=0
		start_txt=""
	end

	header_length = 8+start
	buff_len = buffer:len()-start

	local subtree_packet = tree:add(s7p_proto,buffer(),"S7P Packet ("..buff_len..start_txt..")")
	local subtree_header = subtree_packet:add_le(buffer(start,4), "S7P Header (".. header_length-start ..")")

	cmd = buffer(start,1):uint()
	cmd_str = "UNKOWN"
	for k,v in pairs(ar_cmd) do 
		if cmd == v then cmd_str = k end
	end



	--// CAM_ID
	camID=buffer(start+1,1):uint()
	camID_str="CAMID_NONE";
	if camID >= 0 and camID < 20  then camID_str = string.format("CAMID_%d", camID+1)
	elseif camID == 0xFF then camID_str = "CAMID_ALL"
	end
 
	fpID = buffer(start+2,2)
	val  = buffer(start+4,4)

	

	pinfo.cols.info:set("Command: "..cmd_str)
	subtree_header:add_le(buffer(start,1),"cmd:\t".. cmd_str .." (" .. string.format("0x%02X", buffer(start,1):uint()) ..")")
	subtree_header:add_le(buffer(start+1,1),"camID:\t" .. camID_str .. " (" .. camID .. ")")
	subtree_header:add_le(fpID,"fpID:\t" .. string.format("0x%04X (%u)",fpID:uint() ,fpID:uint()))
	subtree_header:add_le(val,"val:\t" .. string.format("0x%08X (%u)", val:uint(),val:uint()))

	datasize=buffer:len()-header_length


	--// PARAMETERS VALUE
	if cmd == ar_cmd.S7P_CMD_PARAM_SET or cmd == ar_cmd.S7P_CMD_PARAM_ACK or 
		cmd == ar_cmd.S7P_CMD_PARAM_GET or ar_cmd.S7P_CMD_PARAM_ERR then
		param = fpID:uint()
		param_str = "UNKOWN"
		for k,v in pairs(ar_param) do 
			if v == param  then param_str = k end
		end
		local subtree_param = subtree_header:add_le(buffer(start+2,6), "Param")
		subtree_param:add_le(fpID,"Type = "..param_str..string.format(" (0x%04X)",fpID:uint()))
		--// ERROR VALUE
		if cmd == ar_cmd.S7P_CMD_PARAM_ERR then
			val_str="UNKOWN"
			for k,v in pairs(ar_error) do 
				if v == (val:uint()-0xFFFFFFFF-1)  then val_str = k end
			end
			subtree_param:add_le(val, "Error= "..val_str.." ("..(val:uint()-0xFFFFFFFF-1)..")")
		else
			subtree_param:add_le(val, "Value= "..val:uint())
		end
	end

	
	
	

	--// DATASIZE
	if datasize > 0 then
		
		--// DEBUG VALUE.
		if cmd == ar_cmd.S7P_CMD_DEBUG then
			--//DEBUG BOARD
			if camID == 0xF0 or camID == 0xFF then

				p=header_length;
				mac_src=buffer(p,6); 	p=p+8;
				ip_src=buffer(p,4); 	p=p+4;
				port_src=buffer(p,4); 	p=p+4;
				dhcp_on=buffer(p,4); 	p=p+4;
				modes=buffer(p,4); 	p=p+4;
				dtype=buffer(p,4); 	p=p+4;
				win_size=buffer(p,4); 	p=p+4;
				sfrate=buffer(p,4); 	p=p+4;
				rtp_port=buffer(p,4); 	p=p+4;
				tmp=buffer(p,4); 		p=p+4;
				ar_cam=buffer(p,4); 	p=p+4;

				local subtree_data = subtree_packet:add_le(buffer(header_length,datasize), "Debug BOARD Data ("..datasize..")")

				subtree_data:add_le(mac_src, "mac_src")
				subtree_data:add_le(ip_src, "ip_src="..ip_src:uint())
				subtree_data:add_le(port_src, "port_src="..port_src:uint())
				subtree_data:add_le(dhcp_on, "dhcp_on="..dhcp_on:uint())
				subtree_data:add_le(modes, "modes="..modes:uint())
				subtree_data:add_le(dtype, "dtype="..dtype:uint())
				subtree_data:add_le(win_size, "win_size="..win_size:uint())
				subtree_data:add_le(sfrate, "sfrate="..sfrate:uint())
				subtree_data:add_le(rtp_port, "rtp_port="..rtp_port:uint())
				subtree_data:add_le(tmp, "tmp="..tmp:uint())
				subtree_data:add_le(ar_cam, "ar_cam -> "..string.format("0x%X",ar_cam:uint()))
				
			else
				p=header_length
				mem_start=buffer(p,4); 	p=p+4;
				mem_size=buffer(p,4); p=p+4
				frame_id=buffer(p,2); p=p+4;
				
				is_enable=buffer(p,1); p=p+4;
				im_reso=buffer(p,4);
				width=buffer(p,2); p=p+2;
				height=buffer(p,2); p=p+2;
				
				
				quality=buffer(p,4); p=p+4;
				bg_refresh=buffer(p,4); p=p+4;
				bg_thrs=buffer(p,4); p=p+4;
				cc_minarea=buffer(p,4); p=p+4;
				
				
				
				local subtree_data = subtree_packet:add_le(buffer(header_length,datasize), "Debug Board->Cam Data ("..datasize..")")
				subtree_data:add_le(mem_start, "mem_start="..string.format("0x%X",mem_start:uint()))
				subtree_data:add_le(mem_size, "mem_size="..mem_size:uint())
				subtree_data:add_le(frame_id, "frame_id="..frame_id:uint())
				subtree_data:add_le(is_enable, "is_enable="..is_enable:uint())
				subtree_data:add_le(im_reso, "im_reso="..im_reso:uint().." ("..width:uint().."x"..height:uint()..")")
	
				
				subtree_data:add_le(quality, "quality="..quality:uint())
				subtree_data:add_le(bg_refresh, "bg_refresh="..bg_refresh:uint())
				subtree_data:add_le(bg_thrs, "bg_thrs="..bg_thrs:uint())			
				subtree_data:add_le(cc_minarea, "cc_minarea="..cc_minarea:uint())
			end

		-- METADATA
		elseif cmd == ar_cmd.S7P_CMD_MDATA_FIRST then
			local oobtree = subtree_data:add(buffer(databegin+0,16), "OOB Data (16)")
			oobtree:add(buffer(databegin+0,4), "Time: "..buffer(databegin+0,4):le_uint())
			oobtree:add(buffer(databegin+4,4), "Engine Speed: "..buffer(databegin+4,4):le_uint())
			oobtree:add(buffer(databegin+8,4), "Engine Force: "..buffer(databegin+8,4):le_uint())
			local flagtree = oobtree:add(buffer(databegin+12,4), "Flags: "..buffer(databegin+12,4):le_uint())
			--flaghorn = buffer(12,4):le_uint() & 0x00000001
			--flagtree:add(buffer(12,4), "Horn: "..flaghorn)
			-- todo: all all others
			subtree_data:add(buffer(databegin+16,datasize-16), "Node Data ("..(datasize-16)..")")
		else 
			local subtree_data = subtree_packet:add_le(buffer(header_length,datasize), "S7P Data ("..datasize..")")
		end
	end
end
-- load the udp.port table
udp_table = DissectorTable.get("udp.port")
-- register our protocol to handle udp port 18000
udp_table:add(18000,s7p_proto)

s7pmac_proto = Proto("s7pmac","S7P/MAC","Smart Sevensols Protocol on Mac Layer")
-- create a function to dissect it
function s7pmac_proto.dissector(buffer,pinfo,tree)
 pinfo.cols.protocol = "S7NM"
 zeros = buffer(0,2):uint()
 udp_table = DissectorTable.get("udp.port")
 s7p_dissec = udp_table:get_dissector(18000)
 s7p_dissec(buffer(2,buffer:len()-2),pinfo,tree)
end


eth_table = DissectorTable.get("ethertype")
--print(eth_table)
eth_table:add(61447,s7p_proto)

