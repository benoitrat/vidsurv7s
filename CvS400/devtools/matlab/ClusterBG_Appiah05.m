%% ClusterBG_Appiah05 scripts.
%
% Find the best way to obtain the weight in Appiah2005 GW4 background.
%
%   The formula used is not exactly the same as in Appiah2005, we update
%   the weight with the following formula:
%
%   if(matching_cluster)
%       w(k,t) = (31/32)*w(k,t-1) + 1/32
%   else
%       w(k,t) = (31/32)*w(k,t-1)
%
%
%   And we use a 7 bits fixed point value with 7 bits to the fractional
%   length.
%
%
% Usage: Changes the length of the iteration with length_vec.  
%
% Developped and Tested under MATLAB:
%				- R2007b (Windows).
%				- R2008a (Unix).
%
% Copyright (c) 2009 Benoit Rat
% Seven Solutions S.L.
% www.neub.co.nr | www.sevensols.com
% 
% The above copyright notice and this permission notice shall be included in 
% all copies or substantial portions of the Software.



%% Number of iteration.
length_vec=1000;    

%% Define the step vector
t_vec = 1:length_vec;

%% Define the different weight vector.
w=zeros(1,length_vec);
w_1x=uint8(zeros(1,length_vec));
w_2x=uint8(zeros(1,length_vec));
w_u6 = uint8(zeros(1,length_vec));
tmp_u16=uint16(0);

%% Obtain random match vec
match_vec = (rand(1,length_vec) > 0.5);
if sum(match_vec(1:12)) > 6 
    match_vec = match_vec | (rand(1,length_vec) > 0.5);
elseif sum(match_vec(1:12)) > 4
    match_vec = match_vec & (rand(1,length_vec) > 0.5);   
end

%% Define the behaviour of the fixed point
b_word=14;
b_frac=7;
T = numerictype('Signed',false,'WordLength',b_frac,'FractionLength',b_frac);
T2= numerictype('Signed',false,'WordLength',b_word,'FractionLength',b_frac);
FiMath  = fimath('ProductMode','SpecifyPrecision','ProductWordLength',b_word,'ProductFractionLength',b_frac,...
    'SumMode','SpecifyPrecision','SumWordLength',b_word,'SumFractionLength',b_frac);

%% Define Fixed point variable
fi_1= fi(1,T2,FiMath);
fi_63=fi(31,T2,FiMath);
fi_inv64=fi(1/32,T2,FiMath);
w_fi = fi(w, T, FiMath); 

%% Start updating weight according to the matching of a pixel or not.
for t=2:length_vec
   
    if(match_vec(t)) 
        
        %% Expected loat value
        w(t) = (31*w(t-1) + 1)/32;
        
        %% Simple update
        w_2x(t) = min(64,w_2x(t-1)+2);
        w_1x(t) = min(64,w_1x(t-1)+1);
        
            
        %% Manual fixed point update
        fprintf(['w_(',num2str(t-1,'%02d'),')= ',dec2bin(w_u6(t-1),b_frac)]);
        tmp_u16 = 31*uint16(w_u6(t-1));
        fprintf([' => *31 = ',dec2bin(tmp_u16,b_word)]);    
        tmp_u16 = tmp_u16 + bitshift(1,b_frac);
        fprintf([' => +1 = ',dec2bin(tmp_u16,b_word)]);
        tmp_u16 = bitshift(tmp_u16+16,-5); %Divide by 32 (Add 16 for a rounding to the nearest neighbour)
        fprintf([' => /32 = ',dec2bin(tmp_u16,b_word)]);
        w_u6(t) = min(128,(tmp_u16));
        fprintf([' => w_(',num2str(t,'%02d'),') = .',dec2bin(w_u6(t),b_frac),' = ',num2str(double(w_u6(t))/128),'\n']);
        
        %% Matlab fixed point
        tmp_fi = fi(w_fi(t-1),T2,FiMath);
        tmp_fi = tmp_fi * fi_63;
        tmp_fi = tmp_fi + fi_1;
        result = fi(tmp_fi*fi_inv64,T,FiMath);
        w_fi(t) = fi(result,T,FiMath);

        
    else
        %% Expected loat value
        w(t) = (31/32)*w(t-1);
        
         %% Simple update
        w_2x(t) = w_2x(t-1) - 1;
        w_1x(t) = max(0,(w_1x(t-1)-1));
        
        %% Manual fixed point update
        tmp_u16 = 31*uint16(w_u6(t-1));
        tmp_u16 = tmp_u16+16;
        tmp_u16 = bitshift(tmp_u16,-5);     
        w_u6(t) = min(128,tmp_u16);
        
         %% Matlab fixed point
        tmp_fi = fi(w_fi(t-1),T2,FiMath);
        tmp_fi = tmp_fi * fi_63;
        result = fi(tmp_fi*fi_inv64,T,FiMath);
        w_fi(t) = result;
    end 
end


%% Plot of the different graph.
figure(1)
stem(t_vec,match_vec,'k:*');
hold on
plot(t_vec,w,'r-');
plot(t_vec,double(w_1x)./64,'y-');
plot(t_vec,double(w_2x)./128,'b-');
plot(t_vec,w_fi,'c.');
plot(t_vec,double(w_u6)./128,'g-');
legend('match','float','+/-','2+/-','manual fp','matlab fp');
title(['Match ',num2str(100*sum(match_vec(:))/length_vec,'%.1f'),'%']);
hold off
