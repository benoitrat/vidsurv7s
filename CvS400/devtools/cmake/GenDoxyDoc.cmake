####################################################################
# 				Macro To Generate doxygen documentation
# ------------------------------------------------------------------
# 	See http://www.elpauer.org/stuff/learning_cmake.pdf
#
#	Create make "doc" target using the doxyfile defined by 
#	DOXYGEN_CONFIG_FILE. This configuration must be in the 
#	source directory.
#
# 	Last Modified: 
#		- 27/03/2009: Use a config & run script (Benoit Rat)
#
# ------------------------------------------------------------------

MACRO(GENERATE_DOCUMENTATION DOXYGEN_CONFIG_FILE)

## Check that doxygen is setup on the system
FIND_PACKAGE(Doxygen)

## Check that the Doxyfile configuration exist on your system
SET(DOXYFILE_FOUND false)
IF(EXISTS ${PROJECT_SOURCE_DIR}/${DOXYGEN_CONFIG_FILE})
	SET(DOXYFILE_FOUND true)
ENDIF(EXISTS ${PROJECT_SOURCE_DIR}/${DOXYGEN_CONFIG_FILE})


## If everything OK
IF( DOXYGEN_FOUND )
IF( DOXYFILE_FOUND )

    ## Set Doxygen option
    SET(DOX_OUTPUT_DIRECTORY "doc")
    SET(DOX_PROJECT_NAME ${PROJECT_NAME})
    SET(DOX_PROJECT_NUMBER ${${PROJECT_NAME}_MAJOR_VERSION}.${${PROJECT_NAME}_MINOR_VERSION})
    SET(DOX_STRIP_FROM_PATH "${PROJECT_SOURCE_DIR}" )  
    SET(DOX_INPUT  "\"${PROJECT_SOURCE_DIR}/advisor/\" \"${PROJECT_SOURCE_DIR}/x7s\" \"${PROJECT_SOURCE_DIR}/doc/mainpages\"")
    SET(DOX_EXCLUDE  "\"${PROJECT_SOURCE_DIR}/x7s/3rdparty\"")	#Exclude path from building documentation
    SET(DOX_IMAGE_PATH "${PROJECT_SOURCE_DIR}/doc/images")
    SET(DOX_HTML_HEADER "${PROJECT_SOURCE_DIR}/doc/header.htm")
    SET(DOX_ALIASES "path{1}=\"<a class='internal' href='../../\\1' target='about:blank'>\${${PROJECT_NAME}_PATH}/\\1</a>\" cite{1}=\"<tt>\\1</tt>\"")
    SET(DOX_DOTFILE_DIRS "${PROJECT_SOURCE_DIR}/doc/dotfiles/")
    SET(DOX_PREDEFINED " \"X7S_EXPORT=\" \ \"X7SLIBC=\" \ \"CV_DEFAULT()=\" ")	#Macro expension		

    ## Set Doxygen option for Internal documentation
    IF(INSTALL_INTERNAL_DOC)
        MESSAGE(STATUS "Doxygen - Generation of Internal Documentation")
	SET(DOX_INTERNAL_DOCS "YES")
        SET(DOX_EXTRACT_ALL "YES")
	SET(DOX_EXTRACT_LOCAL_CLASSES "YES")
        SET(DOX_HIDE_IN_BODY_DOCS "YES")
    ELSE()
	SET(DOX_INTERNAL_DOCS "NO")
        SET(DOX_EXTRACT_ALL "NO")
	SET(DOX_EXTRACT_LOCAL_CLASSES "NO")
        SET(DOX_HIDE_IN_BODY_DOCS "NO")
    ENDIF()



    ## Configure doxygen config.
    CONFIGURE_FILE(${PROJECT_SOURCE_DIR}/${DOXYGEN_CONFIG_FILE}
     ${PROJECT_BINARY_DIR}/${DOX_OUTPUT_DIRECTORY}/${PROJECT_NAME}.doxyfile  @ONLY IMMEDIATE)
        
    ## Copy doxygen file.
   	CONFIGURE_FILE(${PROJECT_SOURCE_DIR}/doc/logo.ico ${PROJECT_BINARY_DIR}/${DOX_OUTPUT_DIRECTORY}/logo.ico COPYONLY)
	CONFIGURE_FILE(${PROJECT_SOURCE_DIR}/doc/main.css ${PROJECT_BINARY_DIR}/${DOX_OUTPUT_DIRECTORY}/main.css COPYONLY)
	
	IF(INSTALL_DOC)
	INSTALL(DIRECTORY ${PROJECT_BINARY_DIR}/${DOX_OUTPUT_DIRECTORY} DESTINATION ${CMAKE_INSTALL_PREFIX}/share/x7s PATTERN "*.doxyfile" EXCLUDE)
	ENDIF()
  
	## Create target command
	SET(DOC_TARGET_CMD "${DOXYGEN_EXECUTABLE}" )
	SET(DOC_TARGET_ARGS "${PROJECT_BINARY_DIR}/${DOX_OUTPUT_DIRECTORY}/${PROJECT_NAME}.doxyfile" )
	

## In case we cannot generate the documentation.
ELSE( DOXYFILE_FOUND )
	MESSAGE( STATUS "Doxygen configuration file not found - Documentation will not be generated" )
	SET(DOC_TARGET_CMD ${CMAKE_COMMAND})
	SET(DOC_TARGET_ARGS -E echo DOXYFILE_NOT_FOUND)
ENDIF( DOXYFILE_FOUND )
ELSE(DOXYGEN_FOUND)
	MESSAGE(STATUS "Doxygen not found - Documentation will not be generated")
	SET(DOC_TARGET_CMD ${CMAKE_COMMAND})
	SET(DOC_TARGET_ARGS -E echo DOXYGEN_NOT_FOUND)
ENDIF(DOXYGEN_FOUND)

## Add target
ADD_CUSTOM_TARGET(doc COMMAND ${DOC_TARGET_CMD} ${DOC_TARGET_ARGS})


ENDMACRO(GENERATE_DOCUMENTATION)
