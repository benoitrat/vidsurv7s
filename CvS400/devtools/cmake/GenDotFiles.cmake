####################################################################
# 				Macro To Generate dot file to PNG
# ------------------------------------------------------------------
#		- 27/03/2009: Use a config & run script (Benoit Rat)
#
# ------------------------------------------------------------------

MACRO(GENERATE_PNGGRAPH PATH_DOT_FILES PATH_DOXYGEN_CONFIG_FILE)

## Check that doxygen is setup on the system
FIND_PACKAGE(Doxygen)

## Check that the Doxyfile configuration exist on your system
SET(DOXYFILE_FOUND false)
IF(EXISTS ${PROJECT_SOURCE_DIR}/${DOXYGEN_CONFIG_FILE})
	SET(DOXYFILE_FOUND true)
ENDIF(EXISTS ${PROJECT_SOURCE_DIR}/${DOXYGEN_CONFIG_FILE})


## If everything OK
IF( DOXYGEN_FOUND )
IF( DOXYFILE_FOUND )

    ## Set Doxygen option
    SET(DOX_OUTPUT_DIRECTORY "doc")
    SET(DOX_PROJECT_NAME ${PROJECT_NAME})
    SET(DOX_PROJECT_NUMBER ${${PROJECT_NAME}_MAJOR_VERSION}.${${PROJECT_NAME}_MINOR_VERSION})
    SET(DOX_STRIP_FROM_PATH "${PROJECT_SOURCE_DIR}" )  
    SET(DOX_INPUT  "\"${PROJECT_SOURCE_DIR}/src\" \"${PROJECT_SOURCE_DIR}/include/\" \"${PROJECT_SOURCE_DIR}/x7s\" \"${PROJECT_SOURCE_DIR}/doc/mainpages\"")
    SET(DOX_IMAGE_PATH "${PROJECT_SOURCE_DIR}/doc/images")
    SET(DOX_HTML_HEADER "${PROJECT_SOURCE_DIR}/doc/header.htm")
    SET(DOX_ALIASES "path{1}=\"<a class='internal' href='../../\\1' target='about:blank'>\${${PROJECT_NAME}_PATH}/\\1</a>\" cite{1}=\"<tt>\\1</tt>\"") 

    ## Configure doxygen config.
    CONFIGURE_FILE(${PROJECT_SOURCE_DIR}/${DOXYGEN_CONFIG_FILE}
     ${PROJECT_BINARY_DIR}/${DOX_OUTPUT_DIRECTORY}/${PROJECT_NAME}.doxyfile  @ONLY IMMEDIATE)
        
  
ENDMACRO(GENERATE_PNGGRAPH)