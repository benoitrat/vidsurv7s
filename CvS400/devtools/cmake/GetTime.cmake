MACRO (TODAY RESULT)

    ### Set the system date in TMP
    IF (WIN32)
        ## Execute command like when you go to Start > Execute.
		EXECUTE_PROCESS(COMMAND "cmd" "/C date /T" OUTPUT_VARIABLE TMP)
	ELSEIF(UNIX)
		EXECUTE_PROCESS(COMMAND "date" "+%d/%m/%Y" OUTPUT_VARIABLE TMP)
	ENDIF (WIN32)
	
	#MESSAGE(STATUS "1: TMP=${TMP}|EOL")
	IF(TMP)
        STRING(REGEX REPLACE "(..)/(..)/..(..).*" "\\3\\2\\1" TMP ${TMP})
    	#MESSAGE(STATUS "2: TMP=${TMP}|EOL")
	ELSE(TMP)
		MESSAGE(STATUS "WARNING: Date Not Implemented")
		SET(TMP 000000)
	ENDIF(TMP)
	
	SET(${RESULT} ${TMP})
ENDMACRO (TODAY)
