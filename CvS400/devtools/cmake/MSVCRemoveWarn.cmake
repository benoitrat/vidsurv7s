 # Stop MSVC warning
 if(MSVC)
 
 	OPTION(SDK_MSVC_RMWARN_POSIX 	"C4996: Remove POSIX warning (strcopy, strcmp, ...)" ON )
	OPTION(SDK_MSVC_RMWARN_STDDLL 	"C4251: Remove STD dll interface" ON )
	add_definitions(-D_CRT_SECURE_NO_WARNINGS)
	
	if(NOT MSVC60)
		
		# POSIX function (strcopy, strcmp, ...)
		if(SDK_MSVC_RMWARN_POSIX)
			set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /wd4996")
			set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4996")
		endif(SDK_MSVC_RMWARN_POSIX)
		
		# std containers and dll interface.
		if(SDK_MSVC_RMWARN_STDDLL)
			set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /wd4251")
			set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4251")
		endif(SDK_MSVC_RMWARN_STDDLL)
		
	endif(NOT MSVC60)
 endif()