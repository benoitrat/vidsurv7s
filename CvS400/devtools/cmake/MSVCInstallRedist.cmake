# Stop MSVC warning
if(MSVC)

OPTION(INSTALL_MSVC_REDIST_PKG 	"Install vcredist_ARCH package" ON) 
OPTION(INSTALL_MSVC_REDIST_DLL 	"Install VC DLL and manifest" OFF)

if(MSVC80)
SET(MSVC_VERSION_REDIST "MSVC80")
SET(MSVC_VERSION_PATH "8.0")
SET(MSVC_VERSION_NUM 8)
endif()
if(MSVC90)
SET(MSVC_VERSION_REDIST "MSVC90")
SET(MSVC_VERSION_PATH "9.0")
SET(MSVC_VERSION_NUM 9)
endif()





if(INSTALL_MSVC_REDIST_PKG)
      FIND_PROGRAM(SDK_MSVC_REDIST 
	NAMES 
		vcredist_x86.exe
		vcredist_x64.exe 
		vcredist_IA64.exe	
        PATHS
		"[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Microsoft SDKs\\Windows;CurrentInstallFolder]/"
		"[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\VisualStudio\\${MSVC_VERSION_PATH};InstallDir]/../../SDK/v2.0/"
		"[HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Microsoft\\VisualStudio\\${MSVC_VERSION_PATH};InstallDir]/../../SDK/v2.0/"
	PATH_SUFFIXES 
		BootStrapper/Packages/vcredist_x86
		BootStrapper/Packages/vcredist_x64
		BootStrapper/Packages/vcredist_IA64
        )

message(STATUS "CMAKE_MSVC_ARCH=${CMAKE_MSVC_ARCH}")


if(SDK_MSVC_REDIST)
      GET_FILENAME_COMPONENT(vcredist_name "${SDK_MSVC_REDIST}" NAME)
      INSTALL(PROGRAMS ${SDK_MSVC_REDIST} COMPONENT Third DESTINATION bin)
### /q Option is for quiet
      LIST(APPEND CPACK_NSIS_EXTRA_INSTALL_COMMANDS "ExecWait '\\\"$INSTDIR\\\\bin\\\\${vcredist_name}\\\" /q '") 
endif()


endif(INSTALL_MSVC_REDIST_PKG)

if(INSTALL_MSVC_REDIST_DLL)

message(STATUS "Need to find the DLL in: ${${MSVC_VERSION_REDIST}_REDIST_DIR}")


endif(INSTALL_MSVC_REDIST_DLL)



endif(MSVC)







