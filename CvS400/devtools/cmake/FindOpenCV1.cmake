# - Try to find OpenCV library installation
# See http://sourceforge.net/projects/opencvlibrary/
#
# The following variables are optionally searched for defaults
#  OpenCV_ROOT_DIR:            Base directory of OpenCv tree to use.
#  OpenCV_FIND_REQUIRED_COMPONENTS : FIND_PACKAGE(OpenCV COMPONENTS ..) 
#    compatible interface. typically  CV CXCORE CVAUX HIGHGUI.
#
# The following are set after configuration is done: 
#  OpenCV_FOUND
#  OpenCV_INCLUDE_DIRS
#  OpenCV_LIBRARIES
#  OpenCV_LINK_DIRECTORIES
#
# deprecated:
#  OPENCV_* uppercase replaced by case sensitive OpenCV_*
#  OPENCV_EXE_LINKER_FLAGS
#  OPENCV_INCLUDE_DIR : replaced by plural *_DIRS
# 
# 2004/05 Jan Woetzel, Friso, Daniel Grest 
# 2006/01 complete rewrite by Jan Woetzel
# 2006/09 2nd rewrite introducing ROOT_DIR and PATH_SUFFIXES 
#   to handle multiple installed versions gracefully by Jan Woetzel
# 2009/3 Benoit Rat, Adding NO_DEFAULT_PATH options to handle 
#   multi-version with one in the default path.
#
# tested with:
# -OpenCV 0.97 (beta5a):  MSVS 7.1, gcc 3.3, gcc 4.1
# -OpenCV 0.99 (1.0rc1):  MSVS 7.1
#
# www.mip.informatik.uni-kiel.de/~jw
# --------------------------------

SET(OPENCV_SILENT FALSE)

IF(NOT OPENCV_SILENT) 
MESSAGE(STATUS "OpenCV_ROOT_DIR=${OpenCV_ROOT_DIR}") 
ENDIF()

## required cv components with header and library if COMPONENTS unspecified
IF    (NOT OpenCV_FIND_COMPONENTS)
  # default
  SET(OpenCV_FIND_REQUIRED_COMPONENTS   CV CXCORE CVAUX HIGHGUI )
ENDIF (NOT OpenCV_FIND_COMPONENTS)


IF(NOT OpenCV_FOUND)
IF(NOT OpenCV_ROOT_DIR)

# typical root dirs of installations, exactly one of them is used
SET (OpenCV_POSSIBLE_ROOT_DIRS
"${OpenCV_ROOT_DIR}"
"$ENV{ProgramFiles}/OpenCV"
"/usr/local"
"/usr"
"$ENV{OpenCV_ROOT_DIR}"  
"[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Intel(R) Open Source Computer Vision Library_is1;Inno Setup: App Path]"
)

IF(NOT OPENCV_SILENT)
MESSAGE(STATUS "OpenCV_POSSIBLE_ROOT_DIRS=${OpenCV_POSSIBLE_ROOT_DIRS}")
ENDIF()

#
# select exactly ONE OpenCV base directory/tree 
# to avoid mixing different version headers and libs
#
FIND_PATH(OpenCV_ROOT_DIR_FOUND
  NAMES 
  cv/include/cv.h     # windows
  include/opencv/cv.h # linux, trunk
  include/cv/cv.h 
  include/cv.h 
  PATHS ${OpenCV_POSSIBLE_ROOT_DIRS})

SET(OpenCV_ROOT_DIR ${OpenCV_ROOT_DIR_FOUND} CACHE PATH "Set OpenCV root dir if you need to use one particular"  FORCE)
IF(NOT OPENCV_SILENT)
MESSAGE(STATUS "OpenCV_ROOT_DIR_FOUND=${OpenCV_ROOT_DIR_FOUND} > OpenCV_ROOT_DIR=${OpenCV_ROOT_DIR}")
ENDIF()

ELSE(NOT OpenCV_ROOT_DIR)





IF(NOT OPENCV_SILENT)
MESSAGE(STATUS "OpenCV_FIND_REQUIRED_COMPONENTS=${OpenCV_FIND_REQUIRED_COMPONENTS}")
ENDIF()



# header include dir suffixes appended to OpenCV_ROOT_DIR
SET(OpenCV_INCDIR_SUFFIXES
  include
  include/cv
  include/opencv
  cv/include
  cxcore/include
  cvaux/include
  otherlibs/cvcam/include
  otherlibs/highgui
  otherlibs/highgui/include
  otherlibs/_graphics/include
  )

# library linkdir suffixes appended to OpenCV_ROOT_DIR 
SET(OpenCV_LIBDIR_SUFFIXES
  lib
  OpenCV/lib
  otherlibs/_graphics/lib
  )
#DBG_MSG("OpenCV_LIBDIR_SUFFIXES=${OpenCV_LIBDIR_SUFFIXES}")

IF(NOT OPENCV_SILENT)
MESSAGE(STATUS "OpenCV_INCDIR_SUFFIXES=${OpenCV_INCDIR_SUFFIXES}")
MESSAGE(STATUS "OpenCV_LIBDIR_SUFFIXES=${OpenCV_LIBDIR_SUFFIXES}")
ENDIF()

###############################
# FIND include DIR.
####
FIND_PATH(OpenCV_CV_INCLUDE_DIR
  NAMES cv.h      
  PATHS ${OpenCV_ROOT_DIR} 
  PATH_SUFFIXES ${OpenCV_INCDIR_SUFFIXES}
  NO_DEFAULT_PATH)
## Maybe add this line after if we have probleme to find the library.
#FIND_PATH(OpenCV_CV_INCLUDE_DIR
#  NAMES cv.h      
#  PATH_SUFFIXES ${OpenCV_INCDIR_SUFFIXES})

FIND_PATH(OpenCV_CXCORE_INCLUDE_DIR   
  NAMES cxcore.h
  PATHS ${OpenCV_ROOT_DIR} 
  PATH_SUFFIXES ${OpenCV_INCDIR_SUFFIXES}
  NO_DEFAULT_PATH)
FIND_PATH(OpenCV_CVAUX_INCLUDE_DIR    
  NAMES cvaux.h
  PATHS ${OpenCV_ROOT_DIR} 
  PATH_SUFFIXES ${OpenCV_INCDIR_SUFFIXES}
  NO_DEFAULT_PATH)
FIND_PATH(OpenCV_HIGHGUI_INCLUDE_DIR  
  NAMES highgui.h 
  PATHS ${OpenCV_ROOT_DIR} 
  PATH_SUFFIXES ${OpenCV_INCDIR_SUFFIXES} 
  NO_DEFAULT_PATH)
FIND_PATH(OpenCV_CVCAM_INCLUDE_DIR    
  NAMES cvcam.h 
  PATHS ${OpenCV_ROOT_DIR} 
  PATH_SUFFIXES ${OpenCV_INCDIR_SUFFIXES} 
  NO_DEFAULT_PATH)
  
IF(NOT OPENCV_SILENT)
MESSAGE(STATUS "OpenCV_HIGHGUI_INCLUDE_DIR=${OpenCV_HIGHGUI_INCLUDE_DIR} ${OpenCV_ROOT_DIR}")
ENDIF()

###############################
## FIND Libraries.
##

## First set the name of each library
SET(OpenCV_CV_LIBNAME cv)
SET(OpenCV_CXCORE_LIBNAME cxcore)
SET(OpenCV_CVAUX_LIBNAME cvaux)
SET(OpenCV_HIGHGUI_LIBNAME highgui)
SET(OpenCV_ML_LIBNAME ml)
SET(OpenCV_CXTS_LIBNAME cxts)
SET(OpenCV_CVHAARTRAINING_LIBNAME cvhaartraining)
SET(OpenCV_CVCAM_LIBNAME cvcam)
SET(OpenCV_TRS_LIBRARY trs)

## find absolute path to all libraries 
## some are optionally, some may not exist on Linux

FOREACH(NAME ${OpenCV_FIND_REQUIRED_COMPONENTS} )

IF(MINGW)
SET(OpenCV_${NAME}_LIBNAMES lib${OpenCV_${NAME}_LIBNAME}200 lib${OpenCV_${NAME}_LIBNAME}210 lib${OpenCV_${NAME}_LIBNAME}110 lib${OpenCV_${NAME}_LIBNAME})
ELSE(MINGW)
SET(OpenCV_${NAME}_LIBNAMES ${OpenCV_${NAME}_LIBNAME}200 ${OpenCV_${NAME}_LIBNAME}210 ${OpenCV_${NAME}_LIBNAME}110 ${OpenCV_${NAME}_LIBNAME})
ENDIF(MINGW)

MESSAGE(STATUS "OpenCV_${NAME}_LIBNAME=${OpenCV_${NAME}_LIBNAME} > ${OpenCV_${NAME}_LIBNAMES}")

FIND_LIBRARY(OpenCV_${NAME}_LIBRARY   
  NAMES ${OpenCV_${NAME}_LIBNAMES}
  PATHS ${OpenCV_ROOT_DIR}  
  PATH_SUFFIXES  ${OpenCV_LIBDIR_SUFFIXES}
  DOC "${OpenCV_${NAME}_LIBNAME} part of OpenCV library" 
  NO_DEFAULT_PATH
)
ENDFOREACH(NAME)


###############################
## Set OpenCV has found
##

IF(OpenCV_CXCORE_LIBRARY AND OpenCV_CV_LIBRARY AND OpenCV_CVAUX_LIBRARY AND  OpenCV_HIGHGUI_LIBRARY)
SET(OpenCV_FOUND ON)
ENDIF()


# get the link directory for rpath to be used with LINK_DIRECTORIES: 
IF    (OpenCV_CV_LIBRARY)
  GET_FILENAME_COMPONENT(OpenCV_LINK_DIRECTORIES ${OpenCV_CV_LIBRARY} PATH)
ENDIF (OpenCV_CV_LIBRARY)

MARK_AS_ADVANCED(
  OpenCV_FOUND
  OpenCV_ROOT_DIR 
)
SET(OpenCV_FOUND ${OpenCV_FOUND} CACHE BOOL "Tag If the openCV library has been find" FORCE)
MESSAGE(STATUS "OpenCV_FOUND")


ENDIF(NOT OpenCV_ROOT_DIR)
ENDIF(NOT OpenCV_FOUND)

###############################################################################

IF(OpenCV_FOUND) #Fill OpenCV_LIBRARIES and OpenCV_INCLUDE_DIRS variable.
 
#List all cache variable and fill the non cache one.
FOREACH(NAME ${OpenCV_FIND_REQUIRED_COMPONENTS} )
  # only good if header and library both found   
  IF    (OpenCV_${NAME}_INCLUDE_DIR AND OpenCV_${NAME}_LIBRARY)
    LIST(APPEND OpenCV_INCLUDE_DIRS ${OpenCV_${NAME}_INCLUDE_DIR} )
    LIST(APPEND OpenCV_LIBRARIES    ${OpenCV_${NAME}_LIBRARY} )
    #DBG_MSG("appending for NAME=${NAME} ${OpenCV_${NAME}_INCLUDE_DIR} and ${OpenCV_${NAME}_LIBRARY}" )
  ELSE  (OpenCV_${NAME}_INCLUDE_DIR AND OpenCV_${NAME}_LIBRARY)
    MESSAGE(STATUS "OpenCV component NAME=${NAME} not found! "
      "\nOpenCV_${NAME}_INCLUDE_DIR=${OpenCV_${NAME}_INCLUDE_DIR} "
      "\nOpenCV_${NAME}_LIBRARY=${OpenCV_${NAME}_LIBRARY} ")
    SET(OpenCV_FOUND OFF)
  ENDIF (OpenCV_${NAME}_INCLUDE_DIR AND OpenCV_${NAME}_LIBRARY)
ENDFOREACH(NAME)
LIST(REMOVE_DUPLICATES OpenCV_LIBRARIES)
LIST(REMOVE_DUPLICATES OpenCV_INCLUDE_DIRS)

## be backward compatible:
SET(OPENCV_LIBRARIES   ${OpenCV_LIBRARIES} )
SET(OPENCV_INCLUDE_DIR ${OpenCV_INCLUDE_DIRS} )
SET(OPENCV_FOUND       ${OpenCV_FOUND})

## Disable warning
IF(MSVC)
SET(OpenCV_DISABLE_CRT_WARNING TRUE CACHE BOOL "Disable MSVC CRT warning (stricmp, strdup, ...)")
IF(OpenCV_DISABLE_CRT_WARNING)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -D_CRT_SECURE_NO_DEPRECATE=0")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D_CRT_SECURE_NO_DEPRECATE=0")  
ENDIF()
ENDIF()


IF(NOT OPENCV_SILENT)
MESSAGE(STATUS "OpenCV_INCLUDE_DIRS=${OpenCV_INCLUDE_DIRS}")
ENDIF()
IF(NOT OPENCV_SILENT)
MESSAGE(STATUS "OpenCV_LIBRARIES=${OpenCV_LIBRARIES}")
ENDIF()

ELSE(OpenCV_FOUND) # display warning or error message if it is not found.
  # make FIND_PACKAGE friendly
  IF(NOT OpenCV_FIND_QUIETLY)
    IF(OpenCV_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR
        "OpenCV required but some headers or libs not found. Please specify it's location with OpenCV_ROOT_DIR env. variable.")
    ELSE(OpenCV_FIND_REQUIRED)
      MESSAGE(STATUS 
        "WARNING: OpenCV was not found.")
    ENDIF(OpenCV_FIND_REQUIRED)
  ENDIF(NOT OpenCV_FIND_QUIETLY)
ENDIF(OpenCV_FOUND)
