####################################################################
# 		              Find the SQL plugins in Qt4
# ------------------------------------------------------------------
#		- 18/12/2009: Creation of the script works under MinGW
#
# ------------------------------------------------------------------


SET(QT_SQL_PLUGINS PSQL MYSQL)

SET(QT_PSQL_LIBNAME_RELEASE  qsqlpsql4 libqsqlpsql4)
SET(QT_PSQL_LIBNAME_DEBUG  qsqlpsqld4 libqsqlpsqld4)
SET(QT_MYSQL_LIBNAME_RELEASE qsqlmysql4 libqsqlmysql4)
SET(QT_MYSQL_LIBNAME_DEBUG qsqlmysqld4 libqsqlmysqld4)


FOREACH(MODULE ${QT_SQL_PLUGINS})

FIND_LIBRARY(QT_${MODULE}_LIBRARY_RELEASE NAMES &{QT_${MODULE}_LIBNAME_RELEASE} PATHS ${QT_PLUGINS_DIR}/sqldrivers)
FIND_LIBRARY(QT_${MODULE}_LIBRARY_DEBUG NAMES &{QT_${MODULE}_LIBNAME_DEBUG} PATHS ${QT_PLUGINS_DIR}/sqldrivers)
IF(QT_${MODULE}_LIBRARY_RELEASE AND QT_${MODULE}_LIBRARY_DEBUG)
SET(QT_${MODULE}_LIBRARY optimized;${QT_${MODULE}_LIBRARY_RELEASE};debug;${QT_${MODULE}_LIBRARY_DEBUG})
SET(QT_LIBRARIES ${QT_LIBRARIES} ${QT_${MODULE}_LIBRARY})
ENDIF()
ENDFOREACH(MODULE)

