####################################################################
# 		      Macro To Generate RC file for application Icon
# ------------------------------------------------------------------
#		- 18/12/2009: Compil RC file under MinGW work.
#
# ------------------------------------------------------------------

MACRO(MAKE_WINDOWS_PATH pathname)
  # An extra \\ escape is necessary to get a \ through CMake's processing.
  STRING(REPLACE "/" "\\\\" ${pathname} "${${pathname}}")
  # Enclose with UNESCAPED quotes.  This means we need to escape our
  # quotes once here, i.e. with \"
  SET(${pathname} \"${${pathname}}\")
ENDMACRO(MAKE_WINDOWS_PATH)


MACRO(GET_WINDOWS_PATH winpathname originalpath)
  # An extra \\ escape is necessary to get a \ through CMake's processing.
  STRING(REPLACE "/" "\\\\" ${winpathname} "${originalpath}")
  # Enclose with UNESCAPED quotes.  This means we need to escape our
  # quotes once here, i.e. with \"
  SET(${winpathname} \"${${winpathname}}\")
ENDMACRO(GET_WINDOWS_PATH)


MACRO(GENERATE_RCICONFILE ICON_SRCS ICON_FILE)

GET_FILENAME_COMPONENT(ICON_FPATH ${CMAKE_CURRENT_SOURCE_DIR}/${ICON_FILE} ABSOLUTE)

if(EXISTS ${ICON_FPATH})

## Only work under windows platform.
if(NOT UNIX)
SET(ICON_RC_FILE "${CMAKE_CURRENT_BINARY_DIR}/appicon.rc")
#CONFIGURE_FILE(${ICON_FPATH} ${CMAKE_CURRENT_BINARY_DIR}/app.ico COPYONLY)
#FILE(WRITE ${ICON_RC_FILE} "IDI_ICON1 ICON DISCARDABLE \"app.ico\"")
SET(ICON_WINFPATH ${ICON_FPATH})
MAKE_WINDOWS_PATH(ICON_WINFPATH)
FILE(WRITE ${ICON_RC_FILE} "IDI_ICON1 ICON DISCARDABLE ${ICON_WINFPATH}")


## Check that we are using MINGW
if(MINGW)
	GET_FILENAME_COMPONENT(MINGW_DIR ${CMAKE_C_COMPILER} PATH)
	SET(MINGW_WINDRES ${MINGW_DIR}/windres.exe)
	GET_WINDOWS_PATH(ICON_RC_FILE_WIN ${ICON_RC_FILE})
	
	# resource compilation for mingw
	ADD_CUSTOM_COMMAND(OUTPUT ${ICON_RC_FILE}.o
		COMMAND ${MINGW_WINDRES} 
		ARGS -i ${ICON_RC_FILE_WIN} -o ${ICON_RC_FILE}.o
		DEPENDS ${ICON_FPATH} 
	)
	SET(ICON_SRCS ${ICON_RC_FILE}.o)
else(MINGW)
	SET(ICON_SRCS ${ICON_RC_FILE})
endif(MINGW)

MESSAGE(STATUS "ICO_FILE=${ICON_FILE} ${ICON_RC_FILE} (${ICON_RC_FILE_WIN})")


endif(NOT UNIX)
else(EXISTS ${ICON_FPATH})
MESSAGE(STATUS "Icon file ${ICON_FPATH} not found!")
    
endif(EXISTS ${ICON_FPATH})    
  
ENDMACRO(GENERATE_RCICONFILE)
