####################################################################
# 		      Macro To Generate RC file for application Icon
# ------------------------------------------------------------------
#		- 18/12/2009: Generate RC file under MinGW work.
#
#@@@ Variable that must be define to generate file from template.
#     - LIBNAME : The name of the library.
#


#
# ------------------------------------------------------------------

MACRO(GET_WINDOWS_PATH winpathname originalpath)
  # An extra \\ escape is necessary to get a \ through CMake's processing.
  STRING(REPLACE "/" "\\\\" ${winpathname} "${originalpath}")
  # Enclose with UNESCAPED quotes.  This means we need to escape our
  # quotes once here, i.e. with \"
  SET(${winpathname} \"${${winpathname}}\")
ENDMACRO(GET_WINDOWS_PATH)


MACRO(GENERATE_RCDLLFILE RC_SRC VERSION_HDR RC_TEMPLATE)

if(EXISTS ${RC_TEMPLATE})

## Only work under windows platform.
if(NOT UNIX)
SET(RC_FILE ${PROJECT_BINARY_DIR}/${LIBNAME}.rc)
CONFIGURE_FILE(${RC_TEMPLATE} ${RC_FILE} @ONLY IMMEDIATE)

## Check that we are using MINGW
if(MINGW)
	GET_FILENAME_COMPONENT(MINGW_DIR ${CMAKE_C_COMPILER} PATH)
	SET(MINGW_WINDRES ${MINGW_DIR}/windres.exe)
	GET_WINDOWS_PATH(RC_FILE_WIN ${RC_FILE})
	
	# resource compilation for mingw
	ADD_CUSTOM_COMMAND(OUTPUT ${RC_FILE}.o
		COMMAND ${MINGW_WINDRES} 
		ARGS -i ${RC_FILE_WIN} -o ${RC_FILE}.o
		DEPENDS ${VERSION_HDR} ${RC_FILE})
	MESSAGE(STATUS "RC_SRC=${RC_FILE}.o (${RC_FILE_WIN} ${VERSION_HDR} ${MINGW_WINDRES} ) ")
	
	SET(RC_SRC ${RC_FILE}.o)
else(MINGW)
	SET(RC_SRC ${RC_FILE})
	MESSAGE(STATUS "RC_SRC=${RC_FILE}")
endif(MINGW)



else(NOT UNIX)
#	set_target_properties(${LIBNAME} PROPERTIES VERSION ${LIB_VERSION} SOVERSION ${LIB_SOVERSION})
endif(NOT UNIX)
else(EXISTS ${RC_TEMPLATE})
MESSAGE(STATUS "TEMPLATE file ${RC_TEMPLATE} not found!")
endif(EXISTS ${RC_TEMPLATE})    
  
ENDMACRO(GENERATE_RCDLLFILE)
