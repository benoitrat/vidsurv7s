############################################################
#   Add the standard include directory because Eclipse 
#        auto-completition can not find it
#
#
# The following variable are set and include after configuration is done: 
#  Eclipse_C_INCLUDE_DIR
#  Eclipse_CXX_INCLUDE_DIR
#
# 
# 2009/03 Creation of the script, Benoit RAT.
# tested with:
# -GCC (x.x.x), MinGW (x.x.x), MSVC2008 
#
# 2009/06 Correction of strange behaviour using the 
# Cache. Tested with:
# -GCC 4.3.2
#
# Copyright (C) 2009 by Benoit RAT
# benoit-at-sevensols-dot-com
# 
############################################################

SET(Eclipse_CXX_SUFFIXES "") 

## Obtain version of CXX to set the path for c++ headers files
IF    (${CMAKE_COMPILER_IS_GNUCXX})
	EXEC_PROGRAM(${CMAKE_CXX_COMPILER} ARGS --version OUTPUT_VARIABLE CXX_COMPILER_VERSION)  
	string(REGEX REPLACE "(.*)([3,4]\\.[0-9]\\.[0-9])(.*)" "\\2" CXX_COMPILER_VERSION_SHORT ${CXX_COMPILER_VERSION})
	MESSAGE(STATUS "CXX_VERSION_SHORT=${CXX_COMPILER_VERSION_SHORT}")
	SET(Eclipse_CXX_VERSION "${CXX_COMPILER_VERSION_SHORT}")
ENDIF(${CMAKE_COMPILER_IS_GNUCXX})



## Check if we are using MicroSoft Visual Compiler
IF(MSVC)
	MESSAGE(STATUS "NMake found") 
	SET(MSVC_SDK_PATH "$ENV{PROGRAMFILES}/Microsoft SDKs/Windows/v6.1" CACHE PATH "Path of Microsoft SDK")
	string(REGEX REPLACE "(.*)/bin/.*\\.exe" "\\1/include" CMAKE_INCLUDE_PATH ${CMAKE_C_COMPILER})
	SET(Eclipse_C_INCLUDE_DIR ${CMAKE_INCLUDE_PATH})
	MESSAGE(STATUS "CMAKE_INCLUDE_PATH=${CMAKE_INCLUDE_PATH}")
	MESSAGE(STATUS "Eclipse_C_INCLUDE_DIR=${Eclipse_C_INCLUDE_DIR}")
	MESSAGE(STATUS "Eclipse_CXX_INCLUDE_DIR=${Eclipse_CXX_INCLUDE_DIR}")
ELSE()

IF(MINGW) 
	GET_FILENAME_COMPONENT(MINGW_BIN_DIR "${CMAKE_C_COMPILER}" PATH)
	GET_FILENAME_COMPONENT(MINGW_ROOT_DIR "${MINGW_BIN_DIR}/../" PATH)
	FIND_PATH(Eclipse_C_INCLUDE_DIR NAMES stdio.h PATHS ${MINGW_BIN_DIR}/.. PATH_SUFFIXES "/include/")
	FIND_PATH(Eclipse_CXX_INCLUDE_DIR NAMES vector PATHS ${MINGW_BIN_DIR}/.. PATH_SUFFIXES "/lib/gcc/mingw32/${Eclipse_CXX_VERSION}/include/c++/")
	MESSAGE(STATUS "MINGW_ROOT_DIR=${MINGW_ROOT_DIR}")
	MESSAGE(STATUS "Eclipse_C_INCLUDE_DIR=${Eclipse_C_INCLUDE_DIR}")
	MESSAGE(STATUS "Eclipse_CXX_INCLUDE_DIR=${Eclipse_CXX_INCLUDE_DIR}")
	SET(CMAKE_ECLIPSE_C_SYSTEM_INCLUDE_DIR ${Eclipse_C_INCLUDE_DIR} CACHE INTERNAL "C compiler system include directories")
	SET(CMAKE_ECLIPSE_CXX_SYSTEM_INCLUDE_DIR ${Eclipse_CXX_INCLUDE_DIR} CACHE INTERNAL "CXX compiler system include directories")
ELSE(MINGW) # We are under linux using GCC.
	SET(GCC_POSSIBLE_INC_PATHS "/usr/include" "/usr/share/include")
	FIND_PATH(Eclipse_C_INCLUDE_DIR NAMES stdio.h PATHS ${GCC_POSSIBLE_INC_PATHS})
	FIND_PATH(Eclipse_CXX_INCLUDE_DIR NAMES vector PATHS ${GCC_POSSIBLE_INC_PATHS} PATH_SUFFIXES "/c++/${Eclipse_CXX_VERSION}")
	MESSAGE(STATUS "Eclipse_C_INCLUDE_DIR=${Eclipse_C_INCLUDE_DIR}")
	MESSAGE(STATUS "Eclipse_CXX_INCLUDE_DIR=${Eclipse_CXX_INCLUDE_DIR}")
	SET(CMAKE_ECLIPSE_C_SYSTEM_INCLUDE_DIR ${Eclipse_C_INCLUDE_DIR} CACHE INTERNAL "C compiler system include directories")
	SET(CMAKE_ECLIPSE_CXX_SYSTEM_INCLUDE_DIR ${Eclipse_CXX_INCLUDE_DIR} CACHE INTERNAL "CXX compiler system include directories")

ENDIF(MINGW)

	
ENDIF()


IF(CMAKE_ECLIPSE_C_SYSTEM_INCLUDE_DIR)
	INCLUDE_DIRECTORIES(${CMAKE_ECLIPSE_C_SYSTEM_INCLUDE_DIR})
ENDIF()
IF(CMAKE_ECLIPSE_CXX_SYSTEM_INCLUDE_DIR)
	INCLUDE_DIRECTORIES(${CMAKE_ECLIPSE_CXX_SYSTEM_INCLUDE_DIR})
ENDIF()
