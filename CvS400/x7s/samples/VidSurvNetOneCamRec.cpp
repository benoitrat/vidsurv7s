/**
* @file
* @brief Obtain one image using the X7SIO library and then process it using the VS pipeline.
* @author Benoit RAT
* @date 22-may-2009
*/

//Extra library used
#include <x7sio.h>
#include <x7svidsurv.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

int main(int argc,const char *argv[]) {

	int nFrames=0;
	X7sFrameReader *freader = NULL;
	X7sFrameWriter *fwriter = NULL;
	X7sVidSurvPipeline *vidsurv=NULL;
	IplImage *imI = NULL, *imFG=NULL;

	uint32_t ip_remote=X7sNet_GetIP("192.168.7.254");;
	uint16_t port_remote=18000;
	uint16_t port_local=50000;

	cout << "ip_remote=" <<   ip_remote << endl;
	cout << "port_remote=" << port_remote << endl;
	cout << "port_local=" << port_local << endl;

	//Connect the session
	X7sNetSession *netsession = new X7sNetSession();
	netsession->Connect(ip_remote,port_remote,port_local);
	netsession->SetParam(S7P_PRM_BRD_SSRCID,0xABCDEF01,CAMID_ALL);

	//Create the frame reader
	freader = new X7sNetReader(netsession,CAMID_1);
	netsession->Start();

	//Create the frame writer
	fwriter = X7sCreateFrameWriter("output.avi");

	//Create the surveillance Pipeline.
	vidsurv = x7sCreateVidSurvPipelineFG();

	//Create a windows
	cvNamedWindow("CAMID_1");

	//Then loop over the frame.
	while(freader->GrabFrameError()) {

		imI  = freader->RetrieveRFrame();
		imFG = freader->GetRFGMask();
		if(imFG) cvThreshold(imFG,imFG,0,255,CV_THRESH_BINARY);

		vidsurv->Process(imI,imFG);

		//Show Images !!!
		if(imI)
		{
			vidsurv->Draw(imI);
			cvShowImage("CAMID_1",imI);
			if(fwriter) fwriter->WriteFrame(imI);
		}

		//Wait to display
		//if(nFrames && nFrames%50==0) freader->KeyListener(0);
		//else freader->KeyListener(2);
		freader->KeyListener(2);
		nFrames++;

	}

	if(netsession) delete netsession;
	if(vidsurv) delete vidsurv;
	if(freader) delete freader;
	if(fwriter) delete fwriter;

	return X7S_IO_RET_OK;
}
