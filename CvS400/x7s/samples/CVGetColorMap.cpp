//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------


/**
* @file
* @brief Write JPEG + Blob Position + Alarm in an "improved" MJPEG video.
*
* The JPEG, BlobPosition and Alarm has been previously obtained using the
* VidSurvSaveBinData samples.
* There are loaded through X7sBinReader class.
* The file created is named test_00.mjpeg.
*
* @see VidSurvSaveBinData.cpp
* @author Benoit RAT
* @date 17-jul-2009
*/

//Extra library used
#include <x7sio.h>
#include <x7snet.h>
#include <x7scv.h>
#include <x7svidsurv.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <time.h>

using namespace std;

int main(int argc,const char *argv[]) {

	std::string fpImg;

	//Check the arguments
	if(argc<2 || argc> 4){
		printf("Usage (Image should be 256x?):\n");
		printf("%s <image.png> \n\7",argv[0]);
		exit(EXIT_FAILURE);
	}
	else {
		if(argc>=2)  fpImg=argv[1];
	}


	cv::Mat img = cv::imread(fpImg);
	uint8_t *pImg = img.ptr<uint8_t>(0); //Take the first line

	uint32_t colMap[256];
	uint8_t *pColMap = (uint8_t*)&colMap;

	printf("uint32_t colmap_heatX[%d]={",256);
	for(int x=0;x<img.cols;x++)
	{
		if(x%8==0) printf("\n");
		printf("0x%02X%02X%02X%02X, ",pImg[0],pImg[1],pImg[2],0);
		pColMap[0]=0;
		pColMap[1]=pImg[2];
		pColMap[2]=pImg[1];
		pColMap[3]=pImg[0];

		pImg+=3;
		pColMap+=4;
	}
	printf("};\n");

	IplImage *img1C=NULL;
	IplImage *img3C=NULL;
	x7sPatternImage(&img1C,X7S_CV_GRADIENT_X);
	x7sReallocColorImage(img1C,&img3C,X7S_CV_HEAT2BGR);
	x7sCvtColorMap2BGR(img1C,img3C,(uint32_t*)&colMap);
	cvShowImage("heatMap",img3C);
	cvWaitKey(0);



}
