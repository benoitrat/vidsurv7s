/**
* @file
* @brief Read some frames, process them and write them to a media.
*
* @note This software take as argument two xml files:
* 		-# The media file 		@path{bin/media_confsample.xml}
* 		-# The general file		@path{bin/general_confsample.xml}
*
* The standard content of these files is printed when no argument is given.
*
* @ingroup samples
* @author Benoit RAT
* @date 12-may-2009
*/

//Extra library used
#include <x7sio.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

#define BUFF_SIZE 50

int main(int argc,const char *argv[]) {

        const char *fp_media,*fp_gen;
        X7sFrameReader *freader = NULL;
        X7sFrameWriter *fwriter = NULL;
        TiXmlDocument *xml_media=NULL,*xml_gen=NULL;
        IplImage *imI = NULL;

        int fnum;
        float fps;
        char buf[BUFF_SIZE];

        //Check the arguments
        if(argc<2 || argc >3){
                printf("Usage:\n");
                printf("%s <media_config.xml>\n\7",argv[0]);
                printf("%s <media_config.xml> <general_config.xml>\n\7",argv[0]);
                exit(EXIT_FAILURE);
        }
        else if(argc==2) {
                fp_gen="general_config.xml";
                fp_media=argv[1];
        }
        else {
                fp_media=argv[1];
                fp_gen=argv[2];
        }

        x7sXmlLog_SetLevel(X7S_LOGLEVEL_HIGH);
        x7sIoLog_SetLevel(X7S_LOGLEVEL_HIGH);


        //Create the XML documents
        xml_media = new TiXmlDocument();
        xml_gen = new TiXmlDocument();

        if(!xml_media->LoadFile(fp_media) || !xml_gen->LoadFile(fp_gen)) {
				printf("Could not load media=%s or general=%s XML files",fp_media,fp_gen);
                return X7S_IO_RET_ERR;
        }

        //Create the frame reader.
        freader = X7sCreateFrameReader(xml_media->RootElement(),xml_gen->RootElement());
        if(freader==NULL) return X7S_IO_RET_ERR;

        fwriter = X7sCreateFrameWriter(xml_media->RootElement(),xml_gen->RootElement());

        //Create a windows
        cvNamedWindow("frame");

        //Grab the first frame
        freader->GrabFirstFrame();

        //Then loop over the frame.
        do {
                imI = freader->RetrieveRFrame();
                if(imI) {
						fps = freader->GetFrameRate();
						snprintf(buf,BUFF_SIZE,"fps %04.1f",fps);
						x7sDrawLegend(imI,buf,cvScalarAll(0),0);

						fnum = freader->GetFrameId();
						snprintf(buf,BUFF_SIZE,"frame #%03d",fnum);
						x7sDrawLegend(imI,buf,cvScalarAll(0),2);

                        if(fwriter) fwriter->WriteFrame(imI);
                        cvShowImage("frame",imI);
                        freader->KeyListener(20);
                }

        } while(freader->GrabFrameError());


        delete freader;
        delete fwriter;
        delete xml_media;
        delete xml_gen;

        return X7S_IO_RET_OK;
}


