/**
* @file
* @brief Obtain an image using the X7SIO library and then process FG detection
* using the X7SCV library.
* @see RunIOFile.cpp
* @author Benoit RAT
* @date 12-may-2009
*/

//Extra library used
#include <x7sio.h>
#include <x7scv.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

int main(int argc,const char *argv[]) {

	X7sFrameReader *freader = NULL;
	TiXmlDocument *xml_media,*xml_gen;
	IplImage *imI = NULL;
	IplImage *multi_im=NULL;
	CvBGStatModel *bgModel;


	//Check arguments
	if(argc<1) { printf("Usage: %s <media_config.xml> [<general_config.xml>] [bg_type]",argv[0]); exit(EXIT_FAILURE); }
    const char* fp_media = ((argc > 1)?  argv[1] : "media_config.xml");
    const char* fp_gen = ((argc > 2)? argv[2] : "general_config.xml");
    int bg_type = ((argc > 3)? atoi(argv[3]) : -1);


	//Create the XML documents
	xml_media = new TiXmlDocument();
	xml_gen = new TiXmlDocument();

	if(!xml_media->LoadFile(fp_media) || !xml_gen->LoadFile(fp_gen)) {
		return X7S_RET_ERR;
	}

	//Create the frame reader.
	freader = X7sCreateFrameReader(xml_media->RootElement(),xml_gen->RootElement());
	if(freader==NULL) return X7S_RET_ERR;

	x7sCvLog_SetLevel(X7S_LOGLEVEL_HIGH);


	if(!(0<= bg_type && bg_type <= X7S_BG_MODEL_MID))
	{
		cout << "Which type a background do you want:" << endl;
		cout << "FGD=0,MOG=1,FGD_SIMPLE=2,MOC=10,HW_MOC=11,BCD=12,MID=13" << endl;
		cin >> bg_type;
	}


	//Create a windows
	cvNamedWindow("frame");

	//Grab the first frame
	if(!freader->GrabFirstFrame()) return X7S_RET_ERR;
	imI = freader->RetrieveRFrame();


	//Setup the background model
	bgModel = x7sCreateBGModelType(imI,bg_type);
	if(!bgModel) return X7S_RET_ERR;

	//Then loop over the frame.
	do {
		imI = freader->RetrieveRFrame();
		if(imI) {
			//Process background
			cvUpdateBGStatModel(imI,bgModel);

			//Draw the multi image.
			x7sDrawMultiImages(&multi_im,3,imI,bgModel->background,bgModel->foreground);

			freader->KeyListener(2);
			cvShowImage("frame",multi_im);
		}

	} while(freader->GrabFrameError());

	return X7S_RET_OK;
}


