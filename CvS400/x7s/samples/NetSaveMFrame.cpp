/**
 * @file
 * @brief Save frame data send from the board to a directory.
 *
 *
 *
 * @warning The directory 'frame' must be created before running this program
 * @author Benoit RAT
 * @date 05-may-2009
 */

//Extra library used
#include <x7snet.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;



int main(int argc,const char *argv[]) {

	uint32_t ip_remote=X7sNet_GetIP("192.168.7.254");;
	uint16_t port_remote=18000;
	uint16_t port_local=50000;

	cout << "ip_remote=" <<   ip_remote << endl;
	cout << "port_remote=" << port_remote << endl;
	cout << "port_local=" << port_local << endl;


	FILE *pFile;

	//Connect the session
	X7sNetSession netsession;
	netsession.Connect(ip_remote,port_remote,port_local);

	//Set the parameters
	netsession.SetParam(S7P_PRM_BRD_SSRCID,0x012345AB,CAMID_ALL);
	netsession.SetParam(S7P_PRM_CAM_ENABLE,1,CAMID_1);
	netsession.SetParam(S7P_PRM_CAM_ENABLE,0,CAMID_2);
	netsession.SetParam(S7P_PRM_CAM_ENABLE,0,CAMID_3);
	netsession.SetParam(S7P_PRM_CAM_ENABLE,0,CAMID_4);

	int i=0;
	if(netsession.Start()==X7S_NET_RET_OK) {
		while(i< 500) {
			for(int c=0;c<1;c++) {
				if(netsession.IsNewFrame(c)) {
					X7sNetFrame *frame = netsession.CaptureMetaFrame(c);
					if(frame) {
						stringstream sstr;
						sstr << "frames/C" << frame->camID+1 << "_f" << setw(5) << setfill('0')  << i << ".jpg";
						cout << "Write CAMID_"<< frame->camID+1 << ", frame_id=" << frame->ID << " in " << sstr.str() << endl << endl;
						pFile=fopen(sstr.str().c_str(),"wb");
						if(pFile) {
							fwrite(frame->data->data,sizeof(uint8_t),frame->data->length,pFile);
							fclose(pFile);
						}
						else {
							cerr << "Couldn't open the file : " << sstr.str() << endl;
						}
					}
					else {
						cout << "TestX7sNet::main() >> NewFrame is true but frame is NULL" << endl;
					}
				}
			}
			i++;
		}
		netsession.Stop();
	}
}
