/**
* @file
* @brief Obtain an image using the X7SIO library and then save it in a MJPEG format.
* @see RunIOFile.cpp
* @author Benoit RAT
* @date 17-jul-2009
*/

//Extra library used
#include <x7sio.h>
#include <x7snet.h>
#include <x7scv.h>
#include <x7svidsurv.h>
#include <highgui.h>
#include <cxcore.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <time.h>

using namespace std;

int main(int argc,const char *argv[]) {

	int ret;
	int n_frame = 0;	//The number value of the current frame
	int search_frame = 0;
	IplImage *im = NULL;
	FILE *mjpegFile=NULL;
	X7sMJPEGFrame mjpegFrame;
	X7sMJPEGFrameRate mjpegFPS;
	X7sByteArray *mjpegFrameData = x7sCreateByteArray(X7S_CV_MJPEG_MAXFRAMESIZE);
	X7sByteArray *pHeader = x7sCreateByteArray(X7S_VS_ALRMAN_DATA_SIZE);
	X7sBlobTrajGenerator *pTrajGenClient = new X7sBlobTrajGenerator();
	X7sVSAlrManager *pAlrManClient = NULL;


	if(argc<2){
		printf("Usage:\n");
		printf("%s mjpeg_path year mon mday hour min sec\n\7",argv[0]);
		exit(EXIT_FAILURE);
	}

	//Create a windows
	cvNamedWindow("ReadMJPEG");

	x7sCvLog_SetLevel(X7S_LOGLEVEL_HIGH);

	const char *filepath=argv[1];
	mjpegFile = x7sOpenMJPEG(filepath,pHeader,1);
	if(mjpegFile==NULL)
	{
		printf("Could not find the MJPEG file %s\n",filepath);
		return X7S_RET_ERR;
	}

	if(argc>=6)
	{
		time_t rawtime;
		int i_arg=2;
		struct tm* timeinfo;
		time(&rawtime);
		timeinfo = localtime (&rawtime );
		timeinfo->tm_year = atoi(argv[i_arg++])-1900;
		timeinfo->tm_mon = atoi(argv[i_arg++])-1;
		timeinfo->tm_mday = atoi(argv[i_arg++]);
		timeinfo->tm_hour = atoi(argv[i_arg++]);
		timeinfo->tm_min = atoi(argv[i_arg++]);
		timeinfo->tm_sec = atoi(argv[i_arg++]);
		rawtime = mktime (timeinfo) -1;


		ret= x7sFindTimeMJPEG(mjpegFrameData,mjpegFile,rawtime);
		x7sFindTagMJPEG(mjpegFrameData,mjpegFile,X7S_CV_MJPEG_FRAMETAG_REFFR);
		if(ret== X7S_RET_ERR) {
			printf("Could not find the time %s in file %s\n",x7sCTime(&rawtime),filepath);
			return ret;
		}
	}
	else
	{
		ret = x7sReadFileMJPEG(mjpegFrameData,mjpegFile,0);
		if(ret == X7S_RET_ERR) {
			printf("Could not read the fist frame");
			return ret;
		}
	}

	pAlrManClient = new X7sVSAlrManager(NULL,pHeader);
	printf("%s\n",pAlrManClient->toString().c_str());


	mjpegFrame.pAlarm = x7sCreateByteArray(X7S_VS_ALRMAN_DATA_SIZE);
	mjpegFrame.pBlobs = x7sCreateByteArray(X7S_VS_BLOBTRAJ_DATA_SIZE);
	mjpegFrame.pJpeg = x7sCreateByteArray(X7S_NET_RTP_MSIZEIB_JPG);

	//Init the structure for the first fps computation.
	mjpegFPS.first_t0=true;
	mjpegFPS.t0=0;
	mjpegFPS.fps=24.f;
	mjpegFPS.t0_id=-1;

	while(!feof(mjpegFile))
	{
		n_frame++;
		search_frame = 0;// next frame= 0

		x7sExtractMJPEG(&mjpegFrame, mjpegFrameData);

		CvMat * buf = cvCreateMatHeader(1,mjpegFrame.pJpeg->length,CV_8UC1);
		buf->data.ptr=(uchar*)mjpegFrame.pJpeg->data;
		im = cvDecodeImage(buf);

		if(im==NULL)
		{
			printf ("WARNING: Could not decode image %d\n",n_frame);
			fflush(stdout);
			continue;
		}


		//Process on client side
		if(mjpegFrame.pBlobs)
		{
			pTrajGenClient->SetData(mjpegFrame.pBlobs,n_frame);
			pTrajGenClient->Draw(im);
		}

		if(mjpegFrame.pAlarm)
		{
			pAlrManClient->SetData(mjpegFrame.pAlarm,n_frame);
			pAlrManClient->Draw(im);
		}

		int wait_ms = x7sGetFpsMJPEG(&mjpegFrame,&mjpegFPS);

		cvShowImage("ReadMJPEG",im);

		//if(mjpegFrame.tag==X7S_CV_MJPEG_FRAMETAG_REFFR) cvWaitKey(0);
		cvWaitKey(wait_ms);
		cvReleaseImage(&im);

		//Read next frame
		ret = x7sReadFileMJPEG(mjpegFrameData,mjpegFile, search_frame);
		if(ret == X7S_RET_ERR) break;

	}
	x7sReleaseMJPEG(&mjpegFile);
	x7sReleaseByteArray(&mjpegFrameData);
	X7S_DELETE_PTR(pAlrManClient);
	X7S_DELETE_PTR(pTrajGenClient);


	return X7S_RET_OK;
}


