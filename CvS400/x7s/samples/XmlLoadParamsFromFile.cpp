/**
 * @file
 * @brief Load Parameters from XML files using @ref x7sxml library.
 *
 * At the first execution, this test programs generates an xml files, and close.
 *
 * For the next opening, it read the created file, modify some values and write it in the same file.
 * The parameters count increase each time to keep in memory the number
 * of time this program was executed.
 *
 * @note This software can be executed without printing any arguments.
 *
 * @ingroup samples
 * @author Benoit RAT
 * @date 05-may-2009
 */

//Extra library used
#include <x7sxml.h>
#include <s7p_hdr.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

int main(int argc,const char *argv[]) {

	//First set the highest log level to display all the process
	x7sXmlLog_SetLevel(X7S_LOGLEVEL_HIGHEST);

	//Create an empty XML parameters list
	X7sXmlParamList list("board");

	//Add parameters with their default value to the list.
	list.Insert(X7sParam("ssrc_id",S7P_PRM_BRD_SSRCID,123456747));
	list.Insert(X7sParam("mode",S7P_PRM_BRD_MODES,1054854.0546877454f));
	list.Insert(X7sParam("wsize",S7P_PRM_BRD_WSIZE,250,X7STYPETAG_U8));
	list.Insert(X7sParam("ip",S7P_PRM_BRD_IP,(int)0xFFFFFFFF,X7STYPETAG_U32));
	list.Insert(X7sParam("frame_rate",S7P_PRM_BRD_FRAMERATE,-1));
	list.Insert(X7sParam("count",0x1545,0,X7STYPETAG_U8));
	list.Insert(X7sParam("name",X7S_XML_ANYID,"CAM_1"));
	list.Insert(X7sParam("description",X7S_XML_ANYID,"A small description of the application"));

	//Link count param in the list to count variable.
	uint8_t count=0;
	X7sParam *count_p = list["count"];
	if(count_p) {count_p->LinkParamValue(&count); }

	//Create the XMl Document to be manipulated.
	TiXmlDocument doc("params.xml");

	//Then try to read
	if(!doc.LoadFile()) { //If it is not possible just create it with default parameters.
		TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );
		TiXmlNode *root=list.GetXmlNode();
		doc.LinkEndChild(decl);
		doc.LinkEndChild(root);
		doc.SaveFile();

		cout << "Generation of the file:" << doc.Value() << " succeed" << endl;
		return EXIT_SUCCESS;
	}


	//Otherwise read it from the original value
	TiXmlNode *root = doc.RootElement();	//Obtain the root element.
	list.ReadFromXML(root);					//Read the values from XML.

	cout << endl << "Print all the read parameters:" << endl;
	cout << list.toString() << endl;

	cout << endl <<"Modification of some values:" << endl;
	list["name"]->SetValue("CAM ID 4");
	list["ssrc_id"]->SetValue("147258369");
	list.Insert(X7sParam("state",0x0448,0xF0,X7STYPETAG_U8));
	count++;

	cout << "Write in some files:" << endl;
	list.WriteToXML(root);
	doc.SaveFile();

}

