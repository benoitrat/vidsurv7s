/**
 * @file
 * @brief Show image and metadata from file using the X7SIO library.
 * @author Benoit RAT
 * @date 12-may-2009
 */

//Extra library used
#include <x7scv.h>
#include <x7sio.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;



int main(int argc,const char *argv[]) {

	uint32_t ip_remote=X7sNet_GetIP("192.168.7.254");;
	uint16_t port_remote=18000;
	uint16_t port_local=50000;

	int nFrames=0;

	IplImage *imI=NULL,*imFG=NULL;
	IplImage *multimg=NULL;

	cout << "ip_remote=" <<   ip_remote << endl;
	cout << "port_remote=" << port_remote << endl;
	cout << "port_local=" << port_local << endl;

	cvNamedWindow("CAMID_1");

	//Set Debug level
	x7sCvLog_SetLevel(X7S_LOGLEVEL_HIGH);
	x7sIoLog_SetLevel(X7S_LOGLEVEL_HIGH);
	x7sNetLog_SetLevel(X7S_LOGLEVEL_HIGH);

	//Connect the session
	X7sNetSession *netsession = new X7sNetSession();
	netsession->Connect(ip_remote,port_remote,port_local);

	netsession->SetParam(S7P_PRM_BRD_SSRCID,0xABCDEF01,CAMID_ALL);
	X7sFrameReader *freader = new X7sNetReader(netsession,CAMID_1);
	netsession->Start();

	while(freader->GrabFrameError()) {
		imI=freader->RetrieveRFrame();
		imFG=freader->GetRFGMask();
		if(imFG) {
			X7S_CV_SET_CMODEL(imFG,X7S_CV_HNZ2BGR);//Set the color model to display correctly the image.
			x7sDrawMultiImages(&multimg,2,imI,imFG);
			cvShowImage("CAMID_1",multimg);
		}
		else {
			cvShowImage("CAMID_1",imI);
		}
		freader->KeyListener(2);
		nFrames++;
	}

	netsession->Stop();
	delete netsession;
	delete freader;

	printf("End IORunNet");
	fflush(stdout);
}
