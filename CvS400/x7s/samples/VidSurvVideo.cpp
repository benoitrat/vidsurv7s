/**
* @file
* @brief Simple Program to process a video trough the VS pipeline.
* @author Benoit RAT
* @date 22-may-2009
*/

//Extra library used
#include <x7svidsurv.h>

//OpenCV library
#include <cv.h>
#include <highgui.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

int main(int argc,const char *argv[]) {

	char key;
	X7sVidSurvPipeline *vidsurv=NULL;
	IplImage *imI=NULL, *imFGMask=NULL , *multimg=NULL;

	//Check the arguments
	if(argc<2){
		printf("Usage:\n");
		printf("%s <video_fname>\n\7",argv[0]);
		exit(EXIT_FAILURE);
	}

	//Open a Video Capture with the first argument.
	CvCapture* capture	= cvCaptureFromFile(argv[1]);
	if(capture==NULL) {
		printf("Can not open video =%s\n",argv[1]);
		exit(EXIT_FAILURE);
	}

	x7sVSLog_SetLevel(X7S_LOGLEVEL_HIGH);

	//Create the standard Video Surveillance Pipeline,
	vidsurv = x7sCreateVidSurvPipelineFG();

	//Create a windows
	cvNamedWindow("CAMID_1");

	//Then loop over the frame.
	while(cvGrabFrame(capture)) {

		//Obtain the image
		imI  = cvRetrieveFrame(capture);

		if(imI)
		{
			//Process the images
			vidsurv->Process(imI,NULL);
			vidsurv->Draw(imI);

			//Retrieve the FGMask
			imFGMask = vidsurv->GetFGMask();

			//Then draw the images.
			x7sDrawMultiImages(&multimg,2,imI,imFGMask);
			cvShowImage("CAMID_1",multimg);

			//Listen to escape key to finish application
			key = cvWaitKey(2);
			if(key == 27) break;
		}
	}

	//Delete the memory reserved.
	if(vidsurv) delete vidsurv;
	if(capture) cvReleaseCapture(&capture);

	return X7S_RET_OK;
}

