/**
* @file
* @brief Show image from file using the X7sIO library.
* @author Benoit RAT
* @date 12-may-2009
*/

//Extra library used
#include <x7sio.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

int main(int argc,const char *argv[]) {

        const char *fp_media,*fp_gen;
        X7sFrameReader *freader = NULL;
        TiXmlDocument *xml_media,*xml_gen;
        IplImage *imI = NULL;

        //Check the arguments
        if(argc<2 || argc >3){
                printf("Usage:\n");
                printf("%s <media_config.xml>\n\7",argv[0]);
                printf("%s <media_config.xml> <general_config.xml>\n\7",argv[0]);
                exit(EXIT_FAILURE);
        }
        else if(argc==2) {
                fp_gen="general_config.xml";
                fp_media=argv[1];
        }
        else {
                fp_media=argv[1];
                fp_gen=argv[2];
        }

        //Create the XML documents
        xml_media = new TiXmlDocument();
        xml_gen = new TiXmlDocument();

        if(!xml_media->LoadFile(fp_media) || !xml_gen->LoadFile(fp_gen)) {
				printf("Could not load media=%s or general=%s XML files",fp_media,fp_gen);
                return X7S_IO_RET_ERR;
        }

        //Create the frame reader.
        freader = X7sCreateFrameReader(xml_media->RootElement(),xml_gen->RootElement());
        if(freader==NULL) return X7S_IO_RET_ERR;

        //Create a windows
        cvNamedWindow("frame");

        //Grab the first frame
        freader->GrabFirstFrame();

        //Then loop over the frame.
        do {
                imI = freader->RetrieveRFrame();
                if(imI) {
                        cvShowImage("frame",imI);
                        freader->KeyListener(5);
                }

        } while(freader->GrabFrameError());


        delete freader;
        delete xml_media;
        delete xml_gen;
        return X7S_IO_RET_OK;
}
