/**
 * @file
 * @brief Show meta-frame sent by the board.
 * @author Benoit RAT
 * @date 05-may-2009
 */

//Extra library used
#include <x7snet.h>
#include <x7scv.h>
#include <highgui.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

int main(int argc,const char *argv[]) {

	char key;
	uint32_t ip_remote=X7sNet_GetIP("192.168.7.254");;
	uint16_t port_remote=18000;
	uint16_t port_local=50000;

	IplImage *jpeg=NULL; //Encoded images.
	IplImage *im=NULL;	// Decoded images
	bool life=true;

	cout << "ip_remote=" <<   ip_remote << endl;
	cout << "port_remote=" << port_remote << endl;
	cout << "port_local=" << port_local << endl;

	cvNamedWindow("CAMID_1");

	//Connect the session
	X7sNetSession netsession;
	netsession.Connect(ip_remote,port_remote,port_local);

	//Set the parameters
	netsession.SetParam(S7P_PRM_BRD_SSRCID,0x012345AB);
	netsession.SetParam(S7P_PRM_CAM_ENABLE,1,CAMID_1);
	netsession.SetParam(S7P_PRM_CAM_ENABLE,0,CAMID_2);
	netsession.SetParam(S7P_PRM_CAM_ENABLE,0,CAMID_3);
	netsession.SetParam(S7P_PRM_CAM_ENABLE,0,CAMID_4);

	if(netsession.Start()==X7S_NET_RET_OK) {
		while(life) {
			if(netsession.IsNewFrame(CAMID_1)) {

				X7sNetFrame *frame = netsession.CaptureMetaFrame(CAMID_1);
				if(frame) {

					if(im==NULL) {
						im=cvCreateImage(cvSize(frame->width,frame->height),IPL_DEPTH_8U,3);
						jpeg=cvCreateImageHeader(cvSize(frame->width,frame->height),IPL_DEPTH_8U,0);
						jpeg->nChannels=0;
					}

					jpeg->imageData=(char*)frame->data->data;
					jpeg->imageSize=(int)frame->data->length;
					x7sCvtColor(jpeg,im,X7S_CV_JPEG2BGR);
					cvShowImage("CAMID_1",im);

				}
				else {
					cout << "TestX7sNet::main() >> NewFrame is true but frame is NULL" << endl;
				}
			}	//End of IsNewFrame

			//Listen to escape key
			key = cvWaitKey(2);
			if(key == 27) life=false;


		}	//End of life
		netsession.Stop();
	} //End of start
}
