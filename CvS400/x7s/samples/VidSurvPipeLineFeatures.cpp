/**
* @file
* @brief Obtain image using the X7SIO library and then process it using the VS pipeline.
* @author Benoit RAT
* @date 22-may-2009
*/

#ifdef _VLD
#include <vld.h>
#endif


//Extra library used
#include <x7sio.h>
#include <x7svidsurv.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

#define BUFF_SIZE 50

TiXmlElement *g_RootMedia=NULL;
int showRoot(char key)
{
	if(g_RootMedia)
	{
		g_RootMedia->Print((FILE*)stdout,0);
		return X7S_RET_OK;
	}
	else return X7S_RET_ERR;
}



int main(int argc,const char *argv[]) {

	int nFrames=0;
	X7sFrameReader *freader = NULL;
	X7sFrameWriter *fwriter = NULL;
	X7sVidSurvPipeline *vidsurv=NULL;
	TiXmlDocument *xml_media,*xml_gen;
	char buff[BUFF_SIZE];
	IplImage *imI = NULL, *imFG=NULL, *imFGMask , *multimg=NULL;
	std::string str_media, str_gen;


	if(argc==3)
	{
		str_media=argv[1];
		str_gen=argv[2];
	}
	else
	{
		char * x7sXmlDir = getenv("X7SXMLDIR");
		if(x7sXmlDir)
		{
			str_gen=x7sXmlDir;
			str_media=x7sXmlDir;
			str_media+="\\media_conf.xml";
			str_gen+="\\general_conf.xml";
		}
		else
		{
			str_media="F:/TRABAJO/X7S/mvc2008_build/bin/media_confsample.xml";
			str_gen="F:/TRABAJO/X7S/mvc2008_build/bin/general_confsample.xml";
		}
	}


	x7sXmlLog_SetLevel(X7S_LOGLEVEL_HIGH);
	x7sVSLog_SetLevel(X7S_LOGLEVEL_HIGH);
	x7sNetLog_SetLevel(X7S_LOGLEVEL_HIGHEST);

	cout << "VidSurvPipeline.cpp"<< endl;

	//Create the XML documents
	xml_media = new TiXmlDocument();
	xml_gen = new TiXmlDocument();

	if(!xml_media->LoadFile(str_media.c_str()) || !xml_gen->LoadFile(str_gen.c_str())) {
		cerr << "Could not load " << str_media << " or " << str_gen << endl;
		return X7S_IO_RET_ERR;
	}

	g_RootMedia=xml_media->RootElement();

	//Create the frame reader.
	freader = X7sCreateFrameReader(xml_media->RootElement(),xml_gen->RootElement());
	if(freader==NULL) {
		cerr << "Frame Reader is NULL " << endl;
		return X7S_IO_RET_ERR;
	}

	freader->AddKeyFunction('s',&showRoot);

	//Create the frame writer
	fwriter = X7sCreateFrameWriter(xml_media->RootElement(),xml_gen->RootElement());


	TiXmlElement *vidsurvnode=xml_media->RootElement()->FirstChildElement(X7S_VS_TAGNAME_PIPELINE);
	vidsurv = x7sCreateVidSurvPipelineFeatures();
	vidsurv->ReadFromXML(vidsurvnode);
	vidsurv->SetParam("enable","1");
	//vidsurv->SetParam("dbgWnd","1");

	//vidsurv->GetAlarmManager()->Add(x7sCreateVSAlarmGlobal(cvSize(356,288),0.7));


	//Write to the node the value loaded by default
	vidsurv->WriteToXML(vidsurvnode,true);
	freader->WriteToXML(g_RootMedia->FirstChildElement(freader->GetTagName()));
	if(fwriter) fwriter->WriteToXML(g_RootMedia->FirstChildElement(fwriter->GetTagName()));
	//xml_media->SaveFile("F:/TRABAJO/X7S/mvc2008_build/bin/output.xml");

	//vidsurv->SetNoInterest(freader->GetNoInterest());


	//Create a windows
	cvNamedWindow("CAMID_1");

	//Then loop over the frame.
	while(freader->GrabFrameError()) {

		imI  = freader->RetrieveRFrame();

		if(imI)
		{
			imFG = freader->GetRFGMask();
			if(imFG) cvThreshold(imFG,imFG,0,255,CV_THRESH_BINARY);

			vidsurv->Process(imI,imFG);
			vidsurv->Draw(imI);

			imFGMask = vidsurv->GetFGMask();

			//if(imFGMask) x7sAddWeightedScalar(imI,imFGMask,imI,cvScalar(0,0,210,0),imFGMask);

			//Draw FPS.
			float fps = freader->GetFrameRate();
			snprintf(buff,BUFF_SIZE,"%03.1f fps ",fps);
			x7sDrawLegend(imI,buff,cvScalarAll(100),0);

			//Show Images !!!
			x7sDrawMultiImages(&multimg,2,imI,imFGMask);

			cvShowImage("CAMID_1",multimg);
			//cout << "Frame #"<<m_nFrames << endl;

			if(fwriter) fwriter->WriteFrame(imI);
			if(nFrames==0) cvSaveImage("output.jpg",imI);

		}

		//Wait to display
		//if(m_nFrames && m_nFrames%50==0) freader->KeyListener(0);
		//else freader->KeyListener(2);
		freader->KeyListener(1);
		nFrames++;
		//printf("VAMOS POR EL FRAME %d\n", nFrames);

	}


	printf("erase all\n"); fflush(stdout);
	if(vidsurv) delete vidsurv;
	printf("vidsurv\n"); fflush(stdout);
	if(freader) delete freader;
	if(fwriter) delete fwriter;
	printf("freader\n"); fflush(stdout);
	if(xml_gen) delete xml_gen;
	printf("xml_gen\n"); fflush(stdout);
	if(xml_media) delete xml_media;
	printf("xml_media\n"); fflush(stdout);


	return X7S_IO_RET_OK;
}

