/**
* @file
* @brief Obtain image using the X7SIO library and then process it using the VS pipeline.
* @author Benoit RAT
* @date 22-may-2009
*/

//Extra library used
#include <x7sio.h>
#include <x7svidsurv.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

int main(int argc,const char *argv[]) {

	int nFrames=0;
	const char *fp_media,*fp_gen;
	X7sFrameReader *freader = NULL;
	CvBlobTrackerAuto *vidsurv=NULL;
	TiXmlDocument *xml_media,*xml_gen;
	IplImage *imI = NULL, *imFG=NULL, *multimg=NULL;
	int n;

	//Check the arguments
	if(argc<2 || argc >3){
		printf("Usage:\n");
		printf("%s <media_config.xml>\n\7",argv[0]);
		printf("%s <media_config.xml> <general_config.xml>\n\7",argv[0]);
		exit(EXIT_FAILURE);
	}
	else if(argc==2) {
		fp_gen="general_config.xml";
		fp_media=argv[1];
	}
	else {
		fp_media=argv[1];
		fp_gen=argv[2];
	}

	cout << "VidSurvBTAuto.cpp"<< endl;

	//Create the XML documents
	xml_media = new TiXmlDocument();
	xml_gen = new TiXmlDocument();

	if(!xml_media->LoadFile(fp_media) || !xml_gen->LoadFile(fp_gen)) {
		cerr << "Could not load " << fp_media << " or " << fp_gen << endl;
		return X7S_IO_RET_ERR;
	}

	//Create the frame reader.
	freader = X7sCreateFrameReader(xml_media->RootElement(),xml_gen->RootElement());
	if(freader==NULL) {
		cerr << "Frame Reader is NULL " << endl;
		return X7S_IO_RET_ERR;
	}

	CvBlobTrackerAutoParam1 params;
	params.FGTrainFrames=30;
	params.UsePPData=0;
	params.pFG		= cvCreateFGDetectorBase(CV_BG_MODEL_MOG,NULL);
	params.pBD 		= x7sCreateBlobDetector(X7S_VS_BD_CC);
	params.pBT 		= x7sCreateBlobTracker(X7S_VS_BT_CC);
	params.pBTPP	= x7sCreateVSModBTPostProc(X7S_VS_BTPP_KF);// -->	NULL;//
	params.pBTGen=NULL;
	params.pBTA=NULL;


	vidsurv = cvCreateBlobTrackerAutoCopy1(&params);

	//Create a windows
	cvNamedWindow("CAMID_1");

	//Then loop over the frame.
	while(freader->GrabFrameError()) {

		imI  = freader->RetrieveFrame();

		if(imI)
		{
			vidsurv->Process(imI,NULL);

			//Show Images !!!
			imFG = vidsurv->GetFGMask();


			if(imFG) {
				X7S_CV_SET_CMODEL(imFG,CV_GRAY2BGR);
				n=2;
			}
			else n=1;
			x7sDrawMultiImages(&multimg,n,imI,imFG);
			cvShowImage("CAMID_1",multimg);
			cout << "Main::Frame #"<<nFrames << endl;
		}

		//Wait to display
		//if(nFrames && nFrames%50==0) freader->KeyListener(0);
		//else freader->KeyListener(2);
		if(nFrames==57) {
			freader->KeyListener(10);
		}
		freader->KeyListener(2);
		nFrames++;

	}

	if(vidsurv) delete vidsurv;
	if(freader) delete freader;
	if(xml_gen) delete xml_gen;
	if(xml_media) delete xml_media;
	if(imI) cvReleaseImage(&imI);
	if(imFG) cvReleaseImage(&imFG);

	return X7S_IO_RET_OK;
}

