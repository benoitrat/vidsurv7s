/**
* @file
* @brief Obtain image using the X7SIO library and then process it using the VS pipeline to detect fallin object
* @author Benoit RAT
* @date 22-may-2009
*/

#ifdef _MSC_VER
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif


//Extra library used
#include <x7sio.h>
#include <x7svidsurv.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

#define BUFF_SIZE 50

TiXmlElement *g_RootMedia=NULL;
int showRoot(char key)
{
	if(g_RootMedia)
	{
		g_RootMedia->Print((FILE*)stdout,0);
		return X7S_RET_OK;
	}
	else return X7S_RET_ERR;
}

IplImage *imI = NULL, *imFG=NULL, *imFGMask , *multimg=NULL;
IplImage *rawImageCopy = 0, *mask = 0;
int select_object = 0, track_object = 0;
CvRect selection;
CvPoint origin, min_p, max_p;


void on_mouse( int event, int x, int y, int flags, void* param )
{
    if( !imI )
        return;

    if( imI->origin )
        y = imI->height - y;

    if( select_object )
    {
        selection.x = MIN(x,origin.x);
        selection.y = MIN(y,origin.y);
        selection.width = selection.x + CV_IABS(x - origin.x);
        selection.height = selection.y + CV_IABS(y - origin.y);

        selection.x = MAX( selection.x, 0 );
        selection.y = MAX( selection.y, 0 );
        selection.width = MIN( selection.width, imI->width );
        selection.height = MIN( selection.height, imI->height );
        selection.width -= selection.x;
        selection.height -= selection.y;
    }

    switch( event )
    {
    case CV_EVENT_LBUTTONDOWN:
        origin = cvPoint(x,y);
        selection = cvRect(x,y,0,0);
        select_object = 1;
		track_object = 0;
        break;
    case CV_EVENT_LBUTTONUP:
        select_object = 0;
		if( selection.width > 0 && selection.height > 0 ){
            track_object = 1;
			min_p.x = selection.x;
			min_p.y = selection.y;
			max_p.x = selection.x + selection.width;
			max_p.y = selection.y + selection.height;

			cvSet(mask, cvScalar(0), 0);
			cvRectangle(mask, min_p, max_p, cvScalar(255,0), -1, 8, 0);
		}
        break;
    }
}


int main(int argc,const char *argv[]) {

	int nFrames=0;
	const char *fp_media,*fp_gen;
	X7sFrameReader *freader = NULL;
	X7sFrameWriter *fwriter = NULL;
	X7sVidSurvPipeline *vidsurv=NULL;
	TiXmlDocument *xml_media,*xml_gen;
	char buff[BUFF_SIZE];
	CvVideoWriter *writer = 0;


#ifdef _MSC_VER
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );
#endif


	if(argc==3)
	{
		fp_media=argv[1];
		fp_gen=argv[2];
	}
	else
	{
		fp_media="F:/TRABAJO/X7S/mvc2008_build/bin/media_confsample.xml";
		fp_gen="F:/TRABAJO/X7S/mvc2008_build/bin/general_confsample.xml";
		//fp_media="C:/Users/Ben/project/builds/CvS400/MSVC9/bin/files/media_confsample.xml";
		//fp_gen="C:/Users/Ben/project/builds/CvS400/MSVC9/bin/files/general_confsample.xml";
		//	fp_media="C:/rafa/OPENCV/proyectos/X7S/mvc2008_build/bin/media_confsample.xml";
		//	fp_gen="C:/rafa/OPENCV/proyectos/X7S/mvc2008_build/bin/general_confsample.xml";
	}


	x7sXmlLog_SetLevel(X7S_LOGLEVEL_HIGH);
	x7sVSLog_SetLevel(X7S_LOGLEVEL_HIGH);

	cout << "VidSurvPipeline.cpp"<< endl;

	//Create the XML documents
	xml_media = new TiXmlDocument();
	xml_gen = new TiXmlDocument();

	if(!xml_media->LoadFile(fp_media) || !xml_gen->LoadFile(fp_gen)) {
		cerr << "Could not load " << fp_media << " or " << fp_gen << endl;
		return X7S_IO_RET_ERR;
	}

	g_RootMedia=xml_media->RootElement();

	//Create the frame reader.
	freader = X7sCreateFrameReader(xml_media->RootElement(),xml_gen->RootElement());
	if(freader==NULL) {
		cerr << "Frame Reader is NULL " << endl;
		return X7S_IO_RET_ERR;
	}

	freader->AddKeyFunction('s',&showRoot);

	//Create the frame writer
	fwriter = X7sCreateFrameWriter(xml_media->RootElement(),xml_gen->RootElement());


	TiXmlElement *vidsurvnode=xml_media->RootElement()->FirstChildElement(X7S_VS_TAGNAME_PIPELINE);
	vidsurv = x7sCreateVidSurvPipelineFalling();
	vidsurv->ReadFromXML(vidsurvnode);
	vidsurv->SetParam("enable","1");
	//vidsurv->SetParam("dbgWnd","1");

	//vidsurv->GetAlarmManager()->Add(x7sCreateVSAlarmGlobal(cvSize(356,288),0.7));


	//Write to the node the value loaded by default
	vidsurv->WriteToXML(vidsurvnode,true);
	freader->WriteToXML(g_RootMedia->FirstChildElement(freader->GetTagName()));
	if(fwriter) fwriter->WriteToXML(g_RootMedia->FirstChildElement(fwriter->GetTagName()));
	//xml_media->SaveFile("F:/TRABAJO/X7S/mvc2008_build/bin/output.xml");

	//vidsurv->SetNoInterest(freader->GetNoInterest());


	//Create a windows
	cvNamedWindow("CAMID_1");

	cvNamedWindow( "Original",1);
	cvSetMouseCallback( "Original", on_mouse, 0 );

	//Then loop over the frame.
	while(freader->GrabFrameError()) {

		imI  = freader->RetrieveRFrame();
		if(nFrames == 0){
			mask = cvCreateImage(cvGetSize(imI), 8, 1);
			cvSet(mask, cvScalar(0), 0);
			int frameW  = imI->width; 
			int frameH  = imI->height;
			int isColor = 1;
			int fps     = 15;  
			writer=cvCreateVideoWriter("C:/OBJETOS/6_interest.avi",CV_FOURCC('D', 'I', 'V', '3'),fps,cvSize(frameW,frameH),isColor);
		}

		if(rawImageCopy)
			cvReleaseImage(&rawImageCopy);
		rawImageCopy = cvCloneImage(imI);

		if( select_object && selection.width > 0 && selection.height > 0 ){
            cvSetImageROI( rawImageCopy, selection );
            cvXorS( rawImageCopy, cvScalarAll(255), rawImageCopy, 0 );
            cvResetImageROI( rawImageCopy );
        }
        cvShowImage( "Original",rawImageCopy);
  		//cvWriteFrame(writer, rawImageCopy);  
		
		vidsurv->SetNoInterest(mask);

		if(imI)
		{
			imFG = freader->GetRFGMask();
			if(imFG) cvThreshold(imFG,imFG,0,255,CV_THRESH_BINARY);

			vidsurv->Process(imI,imFG);
			vidsurv->Draw(imI);

			imFGMask = vidsurv->GetFGMask();

			//if(imFGMask) x7sAddWeightedScalar(imI,imFGMask,imI,cvScalar(0,0,210,0),imFGMask);

			//Draw FPS.
			float fps = freader->GetFrameRate();
			snprintf(buff,BUFF_SIZE,"%03.1f fps ",fps);
			x7sDrawLegend(imI,buff,cvScalarAll(100),0);

			//Show Images !!!
			//x7sDrawMultiImages(&multimg,2,imI,imFGMask);
			//cvShowImage("CAMID_1",multimg);
			cvShowImage("CAMID_1",imI);
  		    cvWriteFrame(writer, imI);  

			//cout << "Frame #"<<m_nFrames << endl;

			if(fwriter) fwriter->WriteFrame(imI);
			if(nFrames==0) cvSaveImage("output.jpg",imI);

		}

		//Wait to display
		//if(m_nFrames && m_nFrames%50==0) freader->KeyListener(0);
		//else freader->KeyListener(2);
		freader->KeyListener(2);
		nFrames++;
		printf("VAMOS POR EL FRAME %d\n", nFrames);

	}
	cvReleaseVideoWriter(&writer);


	printf("erase all\n"); fflush(stdout);
	if(vidsurv) delete vidsurv;
	printf("vidsurv\n"); fflush(stdout);
	if(freader) delete freader;
	if(fwriter) delete fwriter;
	printf("freader\n"); fflush(stdout);
	if(xml_gen) delete xml_gen;
	printf("xml_gen\n"); fflush(stdout);
	if(xml_media) delete xml_media;
	printf("xml_media\n"); fflush(stdout);

#ifdef _MSC_VER
	_CrtDumpMemoryLeaks();
#endif

	return X7S_IO_RET_OK;
}

