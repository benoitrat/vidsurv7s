/**
 * @file
 * @brief Write a video using the @ref x7sio library.
 *
 * @note This executable can take the filepath of the output video or nothing.
 * In case nothing is given it will write a "video_out.avi" file in the executable directory
 *
 * @ingroup samples
 * @author Benoit RAT
 * @date 08-jun-2009
 */


//Extra library used
#include <x7scv.h>
#include <x7sio.h>

//Standard library used
#include <cstdio>
#include <cstdlib>
#include <string>

using namespace std;

#define BUFF_SIZE 50

int main(int argc,const char *argv[]) {



	const char *fname;
	X7sFrameWriter *fwriter = NULL;
	IplImage *imI = NULL;
	char buf[BUFF_SIZE];

	//Check the arguments
	if(argc<2)
	{
		printf("Usage:\n");
		printf("%s <output_fname>\n",argv[0]);
		printf("%s <video_out.avi>\n",argv[0]);
		fname="video_out.avi";
	}
	else
	{
		fname=argv[1];
	}

	//Load the filewriter
	fwriter=X7sCreateFrameWriter(fname);
	if(fwriter==NULL) exit(EXIT_FAILURE);

	//Create an image
	imI=cvCreateImage(cvSize(360,288),IPL_DEPTH_8U,3);
	cvNamedWindow("Output",CV_WINDOW_AUTOSIZE);


	for(int i=0;i<1000;i++)
	{
		cvSet(imI,cvScalarAll(0));
		snprintf(buf,BUFF_SIZE,"frame #%03d",i);
		x7sDrawLegend(imI,buf,x7sCvtHue2BGRScalar(i,1000,12),0);
		cvShowImage("Output",imI);
		cvWaitKey(20);
		fwriter->WriteFrame(imI);
	}

	delete fwriter;


}
