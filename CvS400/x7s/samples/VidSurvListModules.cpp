/**
* @file
* @brief  List all the Video Surveillance Modules (CvVSModule).
* @author Benoit RAT
* @date 27-may-2009
*/

#include <cvaux.h>
#include <cstdio>
#include <vector>


int main(int argc,const char *argv[]) {

	std::vector<CvVSModule*> vecModule;
	const char *type, *name, *nick;


	vecModule.push_back(cvCreateFGDetectorBase(CV_BG_MODEL_FGD,NULL));
	vecModule.push_back(cvCreateFGDetectorBase(CV_BG_MODEL_FGD_SIMPLE,NULL));
	vecModule.push_back(cvCreateFGDetectorBase(CV_BG_MODEL_MOG,NULL));

	 //Blob tracking
	vecModule.push_back(cvCreateBlobTrackerCCMSPF());
	vecModule.push_back(cvCreateBlobTrackerCC());
	vecModule.push_back(cvCreateBlobTrackerMS());
	vecModule.push_back(cvCreateBlobTrackerMSFG());
	vecModule.push_back(cvCreateBlobTrackerMSPF());

    //Trajectory post-processing module
    vecModule.push_back(cvCreateModuleBlobTrackPostProcKalman());
    vecModule.push_back(cvCreateModuleBlobTrackPostProcTimeAverExp());
    vecModule.push_back(cvCreateModuleBlobTrackPostProcTimeAverRect());

	//Trajectory generation modules
	vecModule.push_back(cvCreateModuleBlobTrackGen1());
	vecModule.push_back(cvCreateModuleBlobTrackGenYML());

	//Trajectory Analyzer modules.
	vecModule.push_back(cvCreateModuleBlobTrackAnalysisHistP());
	vecModule.push_back(cvCreateModuleBlobTrackAnalysisHistPV());
	vecModule.push_back(cvCreateModuleBlobTrackAnalysisHistPVS());
	vecModule.push_back(cvCreateModuleBlobTrackAnalysisHistSS());
	vecModule.push_back(cvCreateModuleBlobTrackAnalysisTrackDist());
	vecModule.push_back(cvCreateModuleBlobTrackAnalysisIOR());


	for(int i=0;i<(int)vecModule.size();i++) {
		if(vecModule[i])
		{
			type=vecModule[i]->GetTypeName();
			name=vecModule[i]->GetModuleName();
			nick=vecModule[i]->GetNickName();

			printf("Type:%-20s, Name:%-20s, Nick:%-20s\n",type,name,nick);
		}
	}
}

