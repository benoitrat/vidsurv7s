/**
 * @file
 * @brief Show meta-frame sent by the board.
 * @author Benoit RAT
 * @date 05-may-2009
 */

//Extra library used
#include <x7snet.h>
#include <x7scv.h>
#include <highgui.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

#define BUFF_SIZE 100

int main(int argc,const char *argv[]) {

	char key, buff[BUFF_SIZE];
	uint32_t ip_remote=X7sNet_GetIP("192.168.7.254");;
	uint16_t port_remote=18000;
	uint16_t port_local=50000;
	string enable_code_str;
	bool cam_enable[4]={true,false,false,false};
	int i=0;

	IplImage *multimg=NULL;
	IplImage *im=NULL,*fgmask=NULL;	// Decoded images

	cout << "ip_remote=" <<   ip_remote << endl;
	cout << "port_remote=" << port_remote << endl;
	cout << "port_local=" << port_local << endl;

	cout << "Set camera enable code:"<< endl;
	cout << "1111 (All enable), 100 (CAMID_3 enable only), 1 (CAMID_1 enable only)" << endl << endl;
	cout << "code = ";
	cin >> enable_code_str;

	for(i=0;i<(int)enable_code_str.length();i++) {
		char c = enable_code_str[i];
		cam_enable[i] = (c!='0');
	}

	x7sCvLog_SetLevel(X7S_LOGLEVEL_LOWEST);

	if(cam_enable[CAMID_1]) cvNamedWindow("CAMID_1");
	if(cam_enable[CAMID_2]) cvNamedWindow("CAMID_2");
	if(cam_enable[CAMID_3]) cvNamedWindow("CAMID_3");
	if(cam_enable[CAMID_4]) cvNamedWindow("CAMID_4");

	//Connect the session
	X7sNetSession netsession;
	netsession.Connect(ip_remote,port_remote,port_local);

	//Set the parameters
	netsession.SetParam(S7P_PRM_BRD_SSRCID,0x012345AB);
	netsession.SetParam(S7P_PRM_CAM_ENABLE,(uint32_t)cam_enable[CAMID_1],CAMID_1);
	netsession.SetParam(S7P_PRM_CAM_ENABLE,(uint32_t)cam_enable[CAMID_2],CAMID_2);
	netsession.SetParam(S7P_PRM_CAM_ENABLE,(uint32_t)cam_enable[CAMID_3],CAMID_3);
	netsession.SetParam(S7P_PRM_CAM_ENABLE,(uint32_t)cam_enable[CAMID_4],CAMID_4);

	i=0;
	bool life=true;
	if(netsession.Start()==X7S_NET_RET_OK) {
		while(life && netsession.IsAlive(1)) {
			for(int c=0;c<4;c++) {
				if(netsession.IsNewFrame(c)) {
					X7sNetFrame *frame = netsession.CaptureMetaFrame(c);
					if(frame) {

						if(im==NULL) {
							im=cvCreateImage(cvSize(frame->width,frame->height),IPL_DEPTH_8U,3);
							fgmask=cvCreateImage(cvSize(frame->width,frame->height),IPL_DEPTH_8U,1);
						}
						snprintf(buff,BUFF_SIZE,"CAMID_%d",c+1);
						if(i%24==0) cout << "Show "<< buff << ", frame #"<<frame->counter << " (ID=" << frame->ID << ")"<<endl;

						IplImage jpeg=x7sByteArrayToIplImage(frame->data);
						IplImage lrle=x7sByteArrayToIplImage(frame->mdata);
						x7sCvtColor(&jpeg,im,X7S_CV_JPEG2BGR);
						x7sCvtColor(&lrle,fgmask,X7S_CV_LRLE2GREY);
						X7S_CV_SET_CMODEL(fgmask,X7S_CV_HNZ2BGRLEGEND)

						x7sDrawMultiImages(&multimg,2,im,fgmask);

						cvShowImage(buff,multimg);
						key = cvWaitKey(2);
						if(key == 27) {
							life=false;
							cout << "QUIT! the key is " << (int)key << endl;
							break;
						}


					}
					else {
						cout << "TestX7sNet::main() >> NewFrame is true but frame is NULL" << endl;
					}
				}
			}
			i++;
		}
		netsession.Stop();
	}
}
