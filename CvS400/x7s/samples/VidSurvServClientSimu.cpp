/**
* @file
* @brief Simulate the server-client by using setData() and getData()
* @author Benoit RAT
* @date 05-may-2009
*/

//Extra library used
#include <x7snet.h>
#include <x7sio.h>
#include <x7svidsurv.h>
#include <tinyxml.h>


//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

#define BUFF_SIZE 50

int main(int argc,const char *argv[]) {

	int nFrames=0;
	const char *fp_media,*fp_gen;
	X7sFrameReader *freader = NULL;
	X7sVidSurvPipeline *vidsurv=NULL;
	TiXmlDocument *xml_media,*xml_gen;
	char buff[BUFF_SIZE];
	IplImage *imI = NULL, *imFG=NULL;

	X7sMJPEGFrame metaJpegFrame;	//!< Structure with JPEG and its metadata.


	bool fulldata=false;

	//Declare the client variable
	IplImage *imClient=NULL;
	X7sBlobTrajGenerator *pTrajGenClient = NULL;
	X7sVSAlrManager *pAlrManClient = NULL;
	TiXmlElement *g_RootMedia=NULL;

	int mjpeg_restart=30;


	//Check the arguments
	if(argc<2 || argc >3){
		printf("Usage:\n");
		printf("%s <media_config.xml>\n\7",argv[0]);
		printf("%s <media_config.xml> <general_config.xml>\n\7",argv[0]);
		exit(EXIT_FAILURE);
	}
	else if(argc==2) {
		fp_gen="general_config.xml";
		fp_media=argv[1];
	}
	else {
		fp_media=argv[1];
		fp_gen=argv[2];
	}

	x7sXmlLog_SetLevel(X7S_LOGLEVEL_HIGH);
	x7sVSLog_SetLevel(X7S_LOGLEVEL_HIGH);

	cout << "VidSurvServClient.cpp"<< endl;

	//Create the XML documents
	xml_media = new TiXmlDocument();
	xml_gen = new TiXmlDocument();

	if(!xml_media->LoadFile(fp_media) || !xml_gen->LoadFile(fp_gen)) {
		cerr << "Could not load " << fp_media << " or " << fp_gen << endl;
		return X7S_IO_RET_ERR;
	}

	g_RootMedia=xml_media->RootElement();

	//Create the frame reader.
	freader = X7sCreateFrameReader(xml_media->RootElement(),xml_gen->RootElement());
	if(freader==NULL) {
		cerr << "Frame Reader is NULL " << endl;
		return X7S_IO_RET_ERR;
	}


	//Create the VidsurvPipeline from XML
	TiXmlElement *vidsurvnode=xml_media->RootElement()->FirstChildElement(X7S_VS_TAGNAME_PIPELINE);
	vidsurv = x7sCreateVidSurvPipelineFG();
	vidsurv->ReadFromXML(vidsurvnode);
	vidsurv->SetNoInterest(freader->GetNoInterest());


	//Obtain the configuration of the Alarm manager of the server
	pAlrManClient = new X7sVSAlrManager(NULL);
	pAlrManClient->ReadFromXMLObject(vidsurv->GetAlarmManager());


	//Obtain the configuration of the Trajectory generator of the server
	pTrajGenClient = new X7sBlobTrajGenerator();
	pTrajGenClient->ReadFromXMLObject(vidsurv->GetTrajGenerator());


	//Write the vidsurv and freader configuration to ouput.xml
	vidsurv->WriteToXML(vidsurvnode,true);
	freader->WriteToXML(g_RootMedia->FirstChildElement(freader->GetTagName()));
	xml_media->SaveFile("output.xml");


	//Create a windows
	cvNamedWindow("CAM_Serv");
	cvNamedWindow("CAM_Cli");

	//Then loop over the frame.
	while(freader->GrabFrameError()) {

		imI  = freader->RetrieveRFrame();

		if(imI)
		{
			metaJpegFrame.id=freader->GetFrameId();

			if(imClient) cvReleaseImage(&imClient);
			imClient = cvCloneImage(imI);

			imFG = freader->GetRFGMask();
			if(imFG) cvThreshold(imFG,imFG,0,255,CV_THRESH_BINARY);

			vidsurv->Process(imI,imFG);
			vidsurv->Draw(imI);

			//Draw FPS.
			float fps = freader->GetFrameRate();
			snprintf(buff,BUFF_SIZE,"%03.1f fps ",fps);
			x7sDrawLegend(imI,buff,cvScalarAll(255),0);


			//Ask if we need to save full data or not.
			fulldata = ((metaJpegFrame.id%mjpeg_restart)==0);

			//Obtain blob position/trajectory data
			X7sBlobTrajGenerator *trajGen = vidsurv->GetTrajGenerator();
			if(trajGen) pTrajGenClient->SetData(trajGen->GetData(fulldata),metaJpegFrame.id);

			//Obtain alarms event and counter data
			X7sVSAlrManager *pAlrMan = vidsurv->GetAlarmManager();
			if(pAlrMan) pAlrManClient->SetData(pAlrMan->GetData(fulldata),metaJpegFrame.id);


			//Then set the tag for the frame
			X7S_CV_TAG_RST(metaJpegFrame.tag);
			X7S_CV_TAG_SET(metaJpegFrame.tag,X7S_CV_MJPEG_FRAMETAG_REFFR,fulldata);
			X7S_CV_TAG_SET(metaJpegFrame.tag,X7S_CV_MJPEG_FRAMETAG_ALARM,metaJpegFrame.pAlarm);


			//Indicate that we have movement
			if(imI)
			{
				CvPoint TL = cvPoint(350,5);
				CvPoint BR = cvPoint(355,10);
				if(metaJpegFrame.tag==X7S_CV_MJPEG_FRAMETAG_REFFR)
				{
					cvRectangle(imClient,TL,BR,cvScalar(0,0,255,255),CV_FILLED);
				}
				if(metaJpegFrame.tag==X7S_CV_MJPEG_FRAMETAG_REFALR)
					cvRectangle(imClient,TL,BR,cvScalar(0,255,0,255),CV_FILLED);
			}


			pAlrManClient->Draw(imClient);
			pTrajGenClient->Draw(imClient);

			cvShowImage("CAM_Serv",imI);
			cvShowImage("CAM_Cli",imClient);



			if(nFrames==0) cvSaveImage("output.jpg",imI);

		}

		//Wait to display
		//if(nFrames && nFrames%50==0) freader->KeyListener(0);
		//else freader->KeyListener(2);
		freader->KeyListener(2);
		nFrames++;

	}


	printf("erase all\n"); fflush(stdout);
	if(vidsurv) delete vidsurv;
	printf("vidsurv\n"); fflush(stdout);
	if(freader) delete freader;
	printf("freader\n"); fflush(stdout);
	if(xml_gen) delete xml_gen;
	printf("xml_gen\n"); fflush(stdout);
	if(xml_media) delete xml_media;
	printf("xml_media\n"); fflush(stdout);


	return X7S_IO_RET_OK;
}
