/**
* @file
* @brief Save meta-frame send from the board to a directory.
* @warning The directory ./frames/ must be previously created
* @author Benoit RAT
* @date 05-may-2009
*/

//Extra library used
#include <x7snet.h>
#include <x7sio.h>
#include <x7svidsurv.h>
#include <tinyxml.h>


//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;



int main(int argc,const char *argv[]) {

	uint32_t ip_remote=X7sNet_GetIP("192.168.7.254");;
	uint16_t port_remote=18000;
	uint16_t port_local=50000;

	cout << "ip_remote=" <<   ip_remote << endl;
	cout << "port_remote=" << port_remote << endl;
	cout << "port_local=" << port_local << endl;



	FILE *pFile=NULL;
	IplImage* imI=NULL, *imFG=NULL;
	X7sNetReader *freader=NULL;
	X7sNetFrame *metaFrame=NULL;
	X7sByteArray *pJPEG=NULL;
	const X7sByteArray  *pBlobs=NULL, *pAlarms=NULL;
	bool fulldata=false;

	//Declare the client variable
	IplImage *imClient=NULL;
	X7sBlobTrajGenerator *pTrajGenClient = new X7sBlobTrajGenerator();
	X7sVSAlrManager *pAlrManClient = new X7sVSAlrManager(NULL);

	//Setup the vidsurv
	X7sVidSurvPipeline *vidsurv = x7sCreateVidSurvPipelineFG();
	vidsurv->X7sXmlObject::SetParam("enable","1");
	X7sVSAlrManager *pAlrMan = vidsurv->GetAlarmManager();
	x7sCreateVSAlarmLine(pAlrMan,242,219,135,274);
	x7sCreateVSAlarmLine(pAlrMan,264,203,320,184);
	pAlrMan->GetLast()->SetParam("color","0 255 255 0");

	//Obtain the configuration of the Alarm manager of the server
	TiXmlElement node("alarms");
	pAlrMan->WriteToXML(&node,true);
	pFile=fopen("frames/config.dat","wb");
	if(pFile)
	{
		TiXmlPrinter printer;
		printer.SetIndent("");
		node.Accept(&printer);
		fprintf(pFile,"%s",printer.CStr());
		fclose(pFile);
	}

	//Configure the alarm manager on the client with the configuration
	pAlrManClient->ReadFromXML(&node);


	//Connect the session
	X7sNetSession *netsession = new X7sNetSession();
	netsession->Connect(ip_remote,port_remote,port_local);

	//Set the parameters
	netsession->SetParam(S7P_PRM_BRD_SSRCID,0x012345AB);

	//Obtain the netReader
	int camID=CAMID_1;
	cvNamedWindow("Server");
	cvNamedWindow("Client");
	freader = new X7sNetReader(netsession,camID);

	int nFrame=0;
	if(netsession->Start()==X7S_NET_RET_OK) {
		while(freader->GrabFrameError()) {

			//Obtain the Image and the FG
			imI=freader->RetrieveRFrame();
			if(imI==NULL) continue;
			imFG = freader->GetRFGMask();
			imClient = (IplImage*)cvClone(imI);

			//Process video surveillance
			vidsurv->Process(imI,imFG);
			vidsurv->Draw(imI);

			//Show Image
			cvShowImage("Server",imI);
			freader->KeyListener(2);

			fulldata = (nFrame%20==0);

			//Obtain the data
			metaFrame = freader->RetrieveData();
			if(metaFrame) pJPEG=metaFrame->data;
			else pJPEG=NULL;
			pBlobs=vidsurv->GetTrajGenerator()->GetData(fulldata);
			pAlarms=pAlrMan->GetData();


			//Write JPEG
			if(pJPEG)
			{
				stringstream sstr_jpeg;
				sstr_jpeg << "frames/C" << camID+1 << "_f" << setw(5) << setfill('0')  << nFrame << ".jpg";
				pFile=fopen(sstr_jpeg.str().c_str(),"wb");
				if(pFile) {
					fwrite(pJPEG->data,sizeof(uint8_t),pJPEG->length,pFile);
					fclose(pFile);
				}
				else {
					cerr << "Couldn't open the file : " << sstr_jpeg.str() << endl;
				}
			}

			//Write Blob position
			if(pBlobs)
			{
				stringstream sstr_blobs;
				sstr_blobs << "frames/C" << camID+1 << "_f" << setw(5) << setfill('0')  << nFrame << ".blobs";
				pFile=fopen(sstr_blobs.str().c_str(),"wb");
				if(pFile) {
					fwrite(pBlobs->data,sizeof(uint8_t),pBlobs->length,pFile);
					fclose(pFile);
				}
				else {
					cerr << "Couldn't open the file : " << sstr_blobs.str() << endl;
				}
			}
			//Write alarms
			if(pAlarms)
			{
				stringstream sstr_alarms;
				sstr_alarms << "frames/C" << camID+1 << "_f" << setw(5) << setfill('0')  << nFrame << ".alarms";
				pFile=fopen(sstr_alarms.str().c_str(),"wb");
				if(pFile) {
					fwrite(pAlarms->data,sizeof(uint8_t),pAlarms->length,pFile);
					fclose(pFile);
				}
				else {
					cerr << "Couldn't open the file : " << sstr_alarms.str() << endl;
				}
			}

			//Process on client side
			if(pBlobs)
			{
				pTrajGenClient->SetData(pBlobs,nFrame);
				pTrajGenClient->Draw(imClient);
			}
			if(pAlarms)
			{
				pAlrManClient->SetData(pAlarms,nFrame);
				pAlrManClient->Draw(imClient);
			}


			//Display the client
			cvShowImage("Client",imClient);
			cvReleaseImage(&imClient);


			nFrame++;
			if(nFrame>1000) break;
		}
		netsession->Stop();
	}

	X7S_DELETE_PTR(netsession);
	X7S_DELETE_PTR(freader);
	X7S_DELETE_PTR(vidsurv);

}
