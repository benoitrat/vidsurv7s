/**
* @file
* @brief Obtain image using the X7SIO library and then process it using the VS pipeline.
* @author Benoit RAT
* @date 22-may-2009
*/

//Extra library used
#include <x7sio.h>
#include <x7svidsurv.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

#define BUFF_SIZE 50

TiXmlElement *g_RootMedia=NULL;
int dbgLevel;

int showRoot(char key)
{
	if(g_RootMedia)
	{
		g_RootMedia->Print((FILE*)stdout,0);
		return X7S_RET_OK;
	}
	else return X7S_RET_ERR;
}


int showDebug(char key)
{
	if(islower(key)) dbgLevel++;
	else dbgLevel--;

	dbgLevel = X7S_INLOOP(dbgLevel,X7S_LOGLEVEL_MED,X7S_LOGLEVEL_HIGHEST);
	x7sVSLog_SetLevel(dbgLevel);
	cout << dbgLevel << endl;

	return X7S_RET_OK;

}



int switchImage(char key)
{
	std::cout << "Not implemented" << std::endl;
	return X7S_RET_ERR;
}




void on_mouse(int event, int x, int y, int flags, void *param)
{
	if(event==CV_EVENT_LBUTTONDOWN)
		cout << "pts=" << x << " " << y << endl;
}

int main(int argc,const char *argv[]) {

	int nFrames=0;
	const char *fp_media,*fp_gen;
	X7sFrameReader *freader = NULL;
	X7sFrameWriter *fwriter = NULL;
	X7sVidSurvPipeline *vidsurv=NULL;
	TiXmlDocument *xml_media,*xml_gen;
	char buff[BUFF_SIZE];
	IplImage *imI = NULL, *imFG=NULL, *imFGMask , *multimg=NULL;

	//Check the arguments
	if(argc<2 || argc >3){
		printf("Usage:\n");
		printf("%s <media_config.xml>\n\7",argv[0]);
		printf("%s <media_config.xml> <general_config.xml>\n\7",argv[0]);
		exit(EXIT_FAILURE);
	}
	else if(argc==2) {
		fp_gen="general_config.xml";
		fp_media=argv[1];
	}
	else {
		fp_media=argv[1];
		fp_gen=argv[2];
	}

	x7sXmlLog_SetLevel(X7S_LOGLEVEL_HIGH);
	x7sVSLog_SetLevel(X7S_LOGLEVEL_HIGHEST);
	x7sCvLog_SetLevel(X7S_LOGLEVEL_LOWEST);

	cout << "VidSurvPipeline.cpp"<< endl;

	//Create the XML documents
	xml_media = new TiXmlDocument();
	xml_gen = new TiXmlDocument();

	if(!xml_media->LoadFile(fp_media) || !xml_gen->LoadFile(fp_gen)) {
		cerr << "Could not load " << fp_media << " or " << fp_gen << endl;
		return X7S_IO_RET_ERR;
	}

	g_RootMedia=xml_media->RootElement();

	//Create the frame reader.
	freader = X7sCreateFrameReader(xml_media->RootElement(),xml_gen->RootElement());
	if(freader==NULL) {
		cerr << "Frame Reader is NULL " << endl;
		return X7S_IO_RET_ERR;
	}



	//Create the frame writer
	fwriter = X7sCreateFrameWriter(xml_media->RootElement(),xml_gen->RootElement());


	TiXmlElement *vidsurvnode=xml_media->RootElement()->FirstChildElement(X7S_VS_TAGNAME_PIPELINE);
	vidsurv = x7sCreateVidSurvPipeline(NULL,vidsurvnode);
	vidsurv->SetParam("enable","1");

	//vidsurv->GetAlarmManager()->Add(x7sCreateVSAlarmGlobal(cvSize(356,288),0.7));

	//Write to the node the value loaded by default
	vidsurv->WriteToXML(vidsurvnode,true);
	freader->WriteToXML(g_RootMedia->FirstChildElement(freader->GetTagName()));
	if(fwriter) fwriter->WriteToXML(g_RootMedia->FirstChildElement(fwriter->GetTagName()));
	xml_media->SaveFile("output.xml");

	vidsurv->SetNoInterest(freader->GetNoInterest());


	//freader->AddKeyFunction('s',&showRoot);
	freader->AddKeyFunction('d',&showDebug);
	freader->AddKeyParam('p',(X7sParam*)vidsurv->GetParam("drawPolys"));
	freader->AddKeyParam('c',(X7sParam*)vidsurv->GetParam("cvtThermal"));
	freader->AddKeyParam('t',(X7sParam*)vidsurv->GetParam("thresholdFG"));
	//freader->AddKeyFunction('i',&switchImage);


	//Create a windows
	cvNamedWindow("CAMID_1");
	cvSetMouseCallback("CAMID_1",on_mouse,NULL);

	//Then loop over the frame.
	while(freader->GrabFrameError()) {

		imI  = freader->RetrieveRFrame();

		if(imI)
		{
			imFG = freader->GetRFGMask();
			if(imFG) cvThreshold(imFG,imFG,0,255,CV_THRESH_BINARY);

			vidsurv->Process(imI,imFG);
			vidsurv->Draw(imI);

			imFGMask = vidsurv->GetFGMask();

			//if(imFGMask) x7sAddWeightedScalar(imI,imFGMask,imI,cvScalar(0,0,210,0),imFGMask);
			//if(imFGMask) x7sPixelate(imI,imI,imFGMask,5,5);

			//Draw FPS.
//			float fps = freader->GetFrameRate();
//			snprintf(buff,BUFF_SIZE,"%03.1f fps ",fps);
//			x7sDrawLegend(imI,buff,cvScalarAll(255),0);

			//Show Images !!!
			x7sDrawMultiImages(&multimg,2,imI,imFGMask);
			cvShowImage("CAMID_1",multimg);
			//cout << "Frame #"<<nFrames << endl;

			if(fwriter) fwriter->WriteFrame(imI);
			if(nFrames==0) cvSaveImage("output.jpg",imI);

		}

		//Wait to display
		//if(nFrames && nFrames%50==0) freader->KeyListener(0);
		//else freader->KeyListener(2);
		freader->KeyListener(2);
		nFrames++;

	}


	vidsurv->WriteToXML(vidsurvnode,true);

	printf("erase all\n"); fflush(stdout);
	if(vidsurv) delete vidsurv;
	printf("vidsurv\n"); fflush(stdout);
	if(freader) delete freader;
	if(fwriter) delete fwriter;
	printf("freader\n"); fflush(stdout);
	if(xml_gen) delete xml_gen;
	printf("xml_gen\n"); fflush(stdout);
	if(xml_media) delete xml_media;
	printf("xml_media\n"); fflush(stdout);


	return X7S_IO_RET_OK;
}

