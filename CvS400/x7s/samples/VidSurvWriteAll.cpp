/**
* @file
* @brief Write the parameters for all CvVsModule in a xml-file.
* @see RunIOFile.cpp
* @author Benoit RAT
* @date 12-may-2009
*/
#include <x7svidsurv.h>
using namespace std;


int main(int argc,const char *argv[]) {

	const char *fp_name;

	//Check the arguments
	if(argc>=2 ) fp_name=argv[1];
	else fp_name="vsmodules.xml";

	//Create the XML document
	TiXmlDocument xml;

	//Create the header and attach to the XML document.
	TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );
	TiXmlElement *root=new TiXmlElement("vsmodules");
	xml.LinkEndChild(decl);
	xml.LinkEndChild(root);

	//Write to the root node all the modules parameters.
	x7sVSModulesWriteAllToXML(root);

	//And save file.
	xml.SaveFile(fp_name);
}
