/**
 * @file
 * @brief Show meta-frame sent by the board.
 * @author Benoit RAT
 * @date 05-may-2009
 */

//Extra library used
#include <x7snet.h>
#include <x7scv.h>
#include <highgui.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

#define BUFF_SIZE 100

int main(int argc,const char *argv[])
{

	x7sNetLog_SetLevel(X7S_LOGLEVEL_HIGHEST);

	char key, buff[BUFF_SIZE];
		uint32_t ip_remote, port_remote;
		uint16_t port_local=50000;
		int i=0, ret;

		IplImage *jpeg=NULL, *lrle=NULL; //Encoded images.
		IplImage *multimg=NULL;
		IplImage *im=NULL,*fgmask=NULL;	// Decoded images

		string ip_str, port_str;

		cout << "Set board IP (Write 0 to use default value 192.168.7.253)"<< endl;
		cout << "> "; cin >> ip_str;
		cout << "Set board port (Write 0 to use default value 18000)" << endl;
		cout << "> "; cin >> port_remote;
		cout << "Set local port (Write 0 to use default value 50000)" << endl;
		cout << "> "; cin >> port_local;


		if(ip_str=="0") ip_str="192.168.7.253";
		if(port_remote==0) port_remote=18000;
		if(port_local==0) port_local=50000;
		ip_remote=X7sNet_GetIP(ip_str.c_str());


		cout << "ip_remote=" <<   ip_remote << endl;
		cout << "port_remote=" << port_remote << endl;
		cout << "port_local=" << port_local << endl;
		cout << endl;

		//=================================================

		string check;
		cout << "Check that the board is connect [Y/n]" << endl;
		cout << "> "; cin >> check;
		if(check == "n") return 0;

		//Connect the session
		X7sNetSession netsession;
		ret = netsession.Connect(ip_remote,port_remote,port_local);
		if(ret!=X7S_RET_OK) return 0;

		//Set the parameters
		netsession.SetParam(S7P_PRM_BRD_SSRCID,0x012345AB);
		netsession.SetParam(S7P_PRM_CAM_ENABLE,1,CAMID_1);
		netsession.SetParam(S7P_PRM_CAM_ENABLE,0,CAMID_2);
		netsession.SetParam(S7P_PRM_CAM_ENABLE,0,CAMID_3);
		netsession.SetParam(S7P_PRM_CAM_ENABLE,0,CAMID_4);


		cout << "Disconnect the board [Y/n]" << endl;
		cout << "> "; cin >> check;
		if(check == "n") return 0;

		cout << "Trying ping...isAlive(2)" << endl;
		netsession.IsAlive(2);

		cout << "Trying ping...isAlive(-3)" << endl;
		netsession.IsAlive(-3);

		//Trying 100 simple ping
		cout << "Trying 100 isAlive(1) (With no wait)" << endl;
		int n=0;
		while(n<100) {
			n++;
			if(netsession.IsAlive(1)) break;
		}


}
