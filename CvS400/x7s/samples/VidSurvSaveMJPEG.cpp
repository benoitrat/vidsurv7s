/**
* @file
* @brief Save meta-frame send from the board to a directory.
* @author Benoit RAT
* @date 07-aug-2009
*/

//Extra library used
#include <x7snet.h>
#include <x7sio.h>
#include <x7svidsurv.h>
#include <tinyxml.h>


//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;



int main(int argc,const char *argv[]) {

	uint32_t ip_remote=X7sNet_GetIP("192.168.7.254");
	uint16_t port_remote=18000;
	uint16_t port_local=50000;

	cout << "ip_remote=" <<   ip_remote << endl;
	cout << "port_remote=" << port_remote << endl;
	cout << "port_local=" << port_local << endl;

	IplImage* imI=NULL, *imFG=NULL;
	X7sNetReader *freader=NULL;
	X7sNetFrame *metaFrame=NULL;
	X7sByteArray *mjpegFrameData = NULL;
	FILE *mjpegFile=NULL;
	X7sMJPEGFrame mjpegFrame;
	bool fulldata=false;

	x7sVSLog_SetLevel(X7S_LOGLEVEL_HIGH);

	//Setup the vidsurv
	X7sVidSurvPipeline *vidsurv = x7sCreateVidSurvPipelineFG();
	vidsurv->X7sXmlObject::SetParam("enable","1");
	X7sVSAlrManager *pAlrMan = vidsurv->GetAlarmManager();
	x7sCreateVSAlarmLine(pAlrMan,242,219,135,274);
	x7sCreateVSAlarmLine(pAlrMan,264,203,320,184);
	pAlrMan->GetLast()->SetParam("color","0 255 255 0");

	//Connect the session
	X7sNetSession *netsession = new X7sNetSession();
	netsession->Connect(ip_remote,port_remote,port_local);

	//Set the parameters
	netsession->SetParam(S7P_PRM_BRD_SSRCID,0x012345AB);

	//Obtain the netReader
	int camID=CAMID_1;
	cvNamedWindow("Server");
	freader = new X7sNetReader(netsession,camID);

	X7sByteArray *alrDefData = (X7sByteArray*)pAlrMan->GetDefinition();
	mjpegFile = x7sOpenMJPEG("output.mjpeg",alrDefData, 0);
	mjpegFrameData = x7sCreateByteArray(X7S_CV_MJPEG_MAXFRAMESIZE);

	int nFrame=0;
	if(netsession->Start()==X7S_NET_RET_OK) {
		while(freader->GrabFrameError()) {

			//Obtain the Image and the FG
			imI=freader->RetrieveRFrame();
			if(imI==NULL) continue;
			imFG = freader->GetRFGMask();

			//Process video surveillance
			vidsurv->Process(imI,imFG);
			vidsurv->Draw(imI);


			fulldata = (nFrame%20==0);

			//Obtain the data
			metaFrame = freader->RetrieveData();
			if(metaFrame==NULL) continue;
			mjpegFrame.pJpeg=metaFrame->data;
			mjpegFrame.pBlobs=(X7sByteArray*)vidsurv->GetTrajGenerator()->GetData(fulldata);
			mjpegFrame.pAlarm=(X7sByteArray*)pAlrMan->GetData(fulldata);
			mjpegFrame.id=freader->GetFrameId();
			if(fulldata) mjpegFrame.tag=X7S_CV_MJPEG_FRAMETAG_REFFR;
			else mjpegFrame.tag=X7S_CV_MJPEG_FRAMETAG_NORMAL;
			time ( &(mjpegFrame.time) );

			//Fill the mjpeg frame data.
			x7sFillMJPEG(&mjpegFrame,mjpegFrameData);

			//And write it in a file.
			x7sWriteMJPEG(mjpegFrameData,mjpegFile);

			//Show Image
			cvShowImage("Server",imI);
			freader->KeyListener(2);

			nFrame++;
			if(nFrame>1000) break;
		}
		netsession->Stop();
	}

	if(mjpegFrameData) x7sReleaseByteArray(&mjpegFrameData);
	if(mjpegFile) x7sReleaseMJPEG(&mjpegFile);

	X7S_DELETE_PTR(netsession);
	X7S_DELETE_PTR(freader);
	X7S_DELETE_PTR(vidsurv);

}
