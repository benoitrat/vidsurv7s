/**
 * @file
 * @brief Simple application to show the behaviour of the X7sParam and X7sParamList.
 *
 * @note This software can be executed without printing any arguments.
 *
 * @ingroup samples
 * @author Benoit RAT
 * @date 05-may-2009
 */

//Extra library used
#include <x7sxml.h>
#include <s7p_hdr.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

const char* ret2str(bool test)
{
	if(test) return " (Ok) ";
	else return " (Failed) ";
}

#define VAL 	0
#define MIN 	1
#define MAX 	2
#define STEP 	3



int main(int argc,const char *argv[]) {

	//Create an empty parameters list
	X7sParamList list;
	int nTest=0;

	//Set the parameters
	list.Insert(X7sParam("ssrc_id",S7P_PRM_BRD_SSRCID,9.06666666));
	list.Insert(X7sParam("mode",S7P_PRM_BRD_MODES,1054854.0546877454f));
	list.Insert(X7sParam("wsize",S7P_PRM_BRD_WSIZE,250,X7STYPETAG_U8));
	list.Insert(X7sParam("ip",S7P_PRM_BRD_IP,(int)0xFFFFFFFF,X7STYPETAG_U32));
	list.Insert(X7sParam("frame_rate",S7P_PRM_BRD_FRAMERATE,-1));
	list.Insert(X7sParam("name",X7S_XML_ANYID,"CAM_1"));
	list.Insert(X7sParam("description",X7S_XML_ANYID,"This sample show how to manipulate parameters"));
	list.Insert(X7sParam("vector",X7S_XML_ANYID,"123.2  1242.064 1298.066    154  0x4456 ABCD -12.1e10 ",X7STYPETAG_STRVEC));

	//Display all parameters
	cout << "Print all parameters:" << endl;
	cout << list.toString() << endl;

	//Browse by name of by ID
	cout << "Get param by ID: S7P_PRM_BRD_MODES= " <<  list[S7P_PRM_BRD_MODES]->toString() << endl;
	cout << "Get param by NAME: wsize=" << list["wsize"]->toString() << endl;

	//Create a value
	uint8_t val_u8=12;
	cout << "val_u8 #1=" << (int)val_u8 << endl;

	//Link this value to the parameters
	list[S7P_PRM_BRD_WSIZE]->LinkParamValue(&val_u8);
	cout << "val_u8 #2 (linked to S7P_PRM_BRD_MODES) =" << (int)val_u8 << endl;

	//Setup the value in the parameters from stream
	stringstream sstr;
	cout << "Setup value from stream \"25\" >> list[\"wsize\"] " << endl;
	sstr << "25";
	sstr >> list["wsize"];
	cout << "Get param by ID: list[S7P_PRM_BRD_WSIZE]= " << list[S7P_PRM_BRD_WSIZE] << endl;
	cout << "val_u8 #3=" << (int)val_u8 << endl;

	//Setup the value in the parameters from the internal value.
	val_u8=5;
	cout << "Set val_u8=5" << endl;
	cout << "Get param by NAME: list[\"wsize\"]=" << list["wsize"]->toString() << endl;
	cout << "val_u8 #4=" << (int)val_u8 << endl;

	//Setup the value in the parameters from string.
	cout << endl << "Print the final list using GetNextParam():" << endl;
	X7sParam *param = NULL;
	while((param=list.GetNextParam(param))!=NULL) {
		cout << "\t" << param << endl;
	}

	//Ask a non existent parameter
	cout << " cout << list[\"non_existant_param\"] " << endl;
	cout << list["non_existant_param"] << endl;


	//list["modes"]++;	//Increment the parameters.

	double val=-1;
	cout << "cout << list[\"vector\"] << endl;" << endl;
	cout << "print:" << list["vector"] << endl;
	cout << "length:" << list["vector"]->GetLength() << endl;
	for(int i=0;i<list["vector"]->GetLength();i++)
	{
		if(list["vector"]->GetRealValue(i,&val)) cout<< "#"<<i<<" : "<< val << endl;
	}

	//------------------------ OUTOFRANGE TEST

	int oor_max[3] = { 15423,-2,1500};
	int *tmpi;
	tmpi=oor_max;
	list.Insert(X7sParam("oor_max",X7S_XML_ANYID,tmpi[VAL],X7STYPETAG_FLOAT,tmpi[MIN],tmpi[MAX]));


	int oor_max2[4] = {0,0,0,0};
	tmpi=oor_max2;
	bool oor_ret = list["oor_max"]->GetValue(&tmpi[VAL],&tmpi[MIN],&tmpi[MAX],&tmpi[STEP]);

	cout << endl;
	cout << "TEST #" << nTest++ << ": OutOfRange (Max)" << endl;
	cout << "Param=" << list["oor_max"]->Print() << endl;
	cout << oor_max2[VAL] << ret2str(oor_max[MAX]==oor_max2[VAL]) << " ret=" << oor_ret << endl;



	//------------------------ RU8 TEST

	int ru8_val=175;
	int ru8_min=100;
	int ru8_max=250;
	int ru8_step=20;

	list.Insert(X7sParam("range_u8",X7S_XML_ANYID,ru8_val,X7STYPETAG_U8,ru8_min,ru8_max));
	list["range_u8"]->SetStep(ru8_step);

	int ru8_val2,ru8_max2,ru8_min2,ru8_step2;
	double ru8_val2d,ru8_max2d,ru8_min2d,ru8_step2d;

	bool ru8_ret2 = list["range_u8"]->GetValue(&ru8_val2,&ru8_min2,&ru8_max2,&ru8_step2);
	bool ru8_ret2d = list["range_u8"]->GetValue(&ru8_val2d,&ru8_min2d,&ru8_max2d,&ru8_step2d);

	cout << endl;
	cout << "TEST #" << nTest++ << ": RANGE_U8 (Min,Max,Step)" << endl;
	cout << "Param=" << list["range_u8"]->Print() << endl;
	cout << "test_i" << ret2str(ru8_ret2) << ":";
	cout << ru8_val2 << ret2str(ru8_val==ru8_val2) << ", ";
	cout << ru8_min2 << ret2str(ru8_min==ru8_min2) << ", ";
	cout << ru8_max2 << ret2str(ru8_max==ru8_max2) << ", ";
	cout << ru8_step2 << ret2str(ru8_step==ru8_step2) << endl;
	cout << "test_d" << ret2str(ru8_ret2d) << ":";
	cout << ru8_val2d << ret2str(ru8_val==ru8_val2d) << ", ";
	cout << ru8_min2d << ret2str(ru8_min==ru8_min2d) << ", ";
	cout << ru8_max2d << ret2str(ru8_max==ru8_max2d) << ", ";
	cout << ru8_step2d << ret2str(ru8_step==ru8_step2d) << endl;

	cout << "before=" << ru8_val << " ";
	int tmp= X7S_INRANGE(ru8_val,ru8_min,ru8_max);
	cout << "inrange=" << tmp << " ";
	cout << "after range=" << ru8_val << endl;



	//------------------------ R TEST










}
