/**
* @file
* @brief Write JPEG + Blob Position + Alarm in an "improved" MJPEG video.
*
* The JPEG, BlobPosition and Alarm has been previously obtained using the
* VidSurvSaveBinData samples.
* There are loaded through X7sBinReader class.
* The file created is named test_00.mjpeg.
*
* @see VidSurvSaveBinData.cpp
* @author Benoit RAT
* @date 17-jul-2009
*/

//Extra library used
#include <x7sio.h>
#include <x7snet.h>
#include <x7scv.h>
#include <x7svidsurv.h>

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <time.h>

using namespace std;

int main(int argc,const char *argv[]) {

	const char *fp_media=NULL,*fp_blobs=NULL, *fp_alarm=NULL;
	X7sFrameReader *fr_media = NULL, *fr_blobs=NULL, *fr_alrEvt=NULL;
	IplImage *imI = NULL, *imB=NULL, *imA=NULL;
	FILE *mjpegFILE=NULL;
	X7sByteArray *pMjpegFrameData = NULL;
	X7sByteArray jpegData = {0};
	X7sByteArray blobsData = {0};
	X7sByteArray alrEvtData = {0};
	X7sMJPEGFrame mjpegFrame = {0};


	//Check the arguments
	if(argc<2 || argc> 4){
		printf("Usage:\n");
		printf("%s <media_config.xml> <blob_config.xml> <alarm_config.xml>\n\7",argv[0]);
		exit(EXIT_FAILURE);
	}
	else {
		if(argc>=2)  fp_media=argv[1];
		if(argc>=3)  fp_blobs=argv[2];
		if(argc>=4)  fp_alarm=argv[3];
	}


	//Create the XML documents
	TiXmlDocument* xml_media = new TiXmlDocument();
	TiXmlDocument* xml_blobs = new TiXmlDocument();
	TiXmlDocument* xml_alarm = new TiXmlDocument();

	//Load the XML file and create a FrameReader
	if(fp_media && xml_media->LoadFile(fp_media)) fr_media = X7sCreateFrameReader(xml_media->RootElement());
	if(fp_blobs && xml_blobs->LoadFile(fp_blobs)) fr_blobs = X7sCreateFrameReader(xml_blobs->RootElement());
	if(fp_alarm && xml_alarm->LoadFile(fp_alarm)) fr_alrEvt = X7sCreateFrameReader(xml_alarm->RootElement());

	if(fr_media==NULL) return X7S_RET_ERR;

	//Create a windows
	cvNamedWindow("frame");

	//Create the alarm definition
	X7sVSAlrManager *pAlrMan = new X7sVSAlrManager(NULL);
	x7sCreateVSAlarmLine(pAlrMan,242,219,135,274);
	x7sCreateVSAlarmLine(pAlrMan,264,203,320,184);
	pAlrMan->GetLast()->SetParam("color","0 255 255 0");
	TiXmlElement node("alarms");
	pAlrMan->WriteToXML(&node,true);
	TiXmlPrinter printer;
	printer.SetIndent("");
	node.Accept(&printer);
	const char *alrDef = printer.CStr();
	X7sByteArray *alrDefData = x7sCreateByteArray(strlen(alrDef));
	alrDefData->length=strlen(alrDef);
	memcpy(alrDefData->data,alrDef,alrDefData->length);


	//Reserved the temporary memory for the special JPEG frame
	pMjpegFrameData = x7sCreateByteArray(65536);	//!< JPEG data with metadata inside.

	mjpegFILE = x7sOpenMJPEG("test_00.mjpeg",alrDefData, 0);
//	mjpegFILE = x7sOpenMJPEG("test_00.mjpeg", 0);
	if(!mjpegFILE) return X7S_RET_ERR;


	//Then loop over the frame.
	while(fr_media->GrabFrameError()) {

		imI = fr_media->RetrieveRFrame();
		if(imI) {
			//Obtain the data from a files
			if(fr_blobs) imB=fr_blobs->QueryRFrame();
			if(fr_alrEvt) imA=fr_alrEvt->QueryRFrame();

			jpegData=x7sByteArray(imI); // The JPEG data
			mjpegFrame.id=fr_media->GetFrameId();
			mjpegFrame.pJpeg=&jpegData;

			if(imB)
			{
				blobsData=x7sByteArray(imB);  	// The Blob data
				mjpegFrame.pBlobs=&blobsData;
			}
			if(imA)
			{
				alrEvtData=x7sByteArray(imA);	// The alarm event data
				mjpegFrame.pAlarm=&alrEvtData;
			}

			//Obtain the time.
			time ( &(mjpegFrame.time) );

//			x7sFillMJPEG(mjpegFrame.pJpeg,mjpegFrame.pBlobs,mjpegFrame.pAlarm,pMjpegFrameData,mjpegFrame.id, mjpegFrame.time);
			x7sFillMJPEG(&mjpegFrame,pMjpegFrameData);
			x7sWriteMJPEG(pMjpegFrameData, mjpegFILE);//prueba de la correcta grabacion

			fr_media->KeyListener(2);
		}
		if(mjpegFILE==NULL) break;
	}
	x7sReleaseMJPEG(&mjpegFILE);
	x7sReleaseByteArray(&pMjpegFrameData);
	x7sReleaseByteArray(&alrDefData);

	X7S_DELETE_PTR(pAlrMan);
	X7S_DELETE_PTR(xml_media);
	X7S_DELETE_PTR(xml_blobs);
	X7S_DELETE_PTR(xml_alarm);


	return X7S_RET_OK;
}


