/**
 *  @file
 *  @brief Library with computer vision functions.
 *  @date 04-may-2009
 *  @author Benoit RAT
 *	@ingroup x7scv
 */


#ifndef _X7SCV_H_
#define _X7SCV_H_


#include <cv.h>
#include <cvaux.h>
#include <ctype.h>
#include <stdio.h>
#include <time.h>

#include "x7sdef.h"


#define X7S_CV_TRUEPIX 255				//!< Code value which mean @true for a pixel
#define X7S_CV_FALSEPIX 0				//!< Code value which mean @false for a pixel

#define X7S_CV_RET_OK 	X7S_RET_OK		//!< Specific OK return code for x7scv library
#define X7S_CV_RET_ERR  X7S_RET_ERR		//!< Specific Error return code for x7scv library

//! Check the field of this image and if it has the correct number of channel.
#define X7S_IS_IMAGENC(img,nChan) \
	( CV_IS_IMAGE_HDR(img) && ( ((IplImage*)img)->nChannels == nChan ) )

//! Check if image size is equal
#define X7S_ARE_IMSIZEEQ(im1, im2) \
    ( ((im1)->height == (im2)->height) && ((im1)->width == (im2)->width) )

//! Check if size structure is equal
#define X7S_ARE_SIZESEQ(size1, size2) \
( ((size1).height == (size2).height) && ((size1).width == (size2).width) )

//! Check if image is equal (same size, type, nChannels)
#define X7S_ARE_IMEQ(im1, im2) \
    ( ((im1)->height == (im2)->height) && ((im1)->width == (im2)->width) && ((im1)->nChannels == (im2)->nChannels) && ((im1)->depth == (im2)->depth))

//! Check if image is same size and type but with different nChannel.
#define X7S_ACHECK_IMG8U(im1, im2, nChan1, nChan2) \
    ((im1)->height == (im2)->height && (im1)->width == (im2)->width && (im1)->nChannels == nChan1 && (im2)->nChannels == nChan2 && (im1)->depth == 8 && (im2)->depth == 8)

//! Function to set the color model of an image
#define X7S_CV_SET_CMODEL(im,code) \
	im->colorModel[0]='X'; 	im->colorModel[1]='7'; 	im->colorModel[2]='S'; 	im->colorModel[3]=code & 0xFF;

//! Function to get the color model of an image.
#define X7S_CV_GET_CMODEL(im,code) \
	if(im->colorModel[0]=='X' && im->colorModel[1]=='7' && im->colorModel[2]=='S') code=(int)im->colorModel[3]; else code=0;

// Tag Management and Binary Logic Bit Operations


#define x7sCreateImage(im,nChan) \
	cvCreateImage(cvGetSize(im),(im)->depth,nChan)

// Core structure and Memory managment
//----------------------------------------------------------------

X7SLIBC X7sByteArray* x7sCreateByteArray(int size_max);
X7SLIBC void x7sReleaseByteArray(X7sByteArray** pByteArray);
X7SLIBC int x7sCopyByteArray(const X7sByteArray* src,X7sByteArray *dst);
X7SLIBC X7sByteArray  x7sByteArray(IplImage *pImg);
X7SLIBC CvMat x7sByteArrayToCvMat(X7sByteArray* byteArray);
X7SLIBC IplImage x7sByteArrayToIplImage(X7sByteArray* pByteArray);
X7SLIBC int x7sReleaseFILE(FILE **ppFile);
X7SLIBC uint8_t* x7sMemWrite(uint8_t* destination,const uint8_t *end_buff,const void* source, size_t num);
X7SLIBC uint8_t* x7sMemRead(uint8_t* destination,const uint8_t *end_buff,void* source, size_t num);


// Connected Component
//----------------------------------------------------------------

/**
 * @brief Represent a connected component as a bounding box.
 *
 * A connected component is a group of connected pixel that are connected together.
 * This class give the center of mass of this connected component, its bounding box and finally its area.
 * @ingroup x7scv
 */
typedef struct {

	//Bounding Box position
	int min_x;	//!< Left side of the bounding box
	int max_x; 	//!< Right side of the bounding box
	int min_y;	//!< Top side of the bounding box
	int max_y;	//!< Bottom side of the bounding box

	int x; //!< Center of mass in x axis (left->right)
	int y; //!< Center of mass in y axis (top->bottom)

	int area; //!< Number of pixel=@ref X7S_CV_TRUEPIX in the bounding box.

} X7sConCompo;


/**
 * @brief This class represent the list of the connected component of a bw image.
 * @note The number of maximum component is given by the FPGA.
 *
 * The standard procedure to use the ConCompoList:
 * 		-# Obtain a binary image in IPL_DEPTH_8U with value equal to 0 or 255 using a FGBG model.
 * 		-# Create the blobs images with x7sMakeBlobs().
 * 		-# Initiate or reset the list of Connexed Component using x7sResetConCompoList().
 * 		-# Fill the values using  x7sFillConCompoList().
 * 		-# ...
 * 		-# process tracking or something else
 * 		-# ...
 * 		-# Release the array of connected components x7sReleaseConCompoList().
 *
 *
 * Another function has been created to ease the procedure: x7sUpdateConCompoList(). It deal with :
 * 		- The creation of blob image
 * 		- The reset the list.
 * 		- How to fill the list.
 * 		- And also to draw the connexed component on the black white image.
 * @ingroup x7scv
 */
typedef struct {

	X7sConCompo *vec;
	int size;
	int max_size;

} X7sConCompoList;


X7SLIBC X7sConCompoList* x7sCreateConCompoList(int max_size);
X7SLIBC void x7sReleaseConCompoList(X7sConCompoList **ccList);
X7SLIBC void x7sResetConCompoList(X7sConCompoList* ccList);
X7SLIBC void x7sFillConCompoList(X7sConCompoList *ccList,const IplImage *im);
X7SLIBC int x7sUpdateConCompoList(X7sConCompoList *ccList, IplImage *imBW, bool draw);
X7SLIBC int x7sSortConCompoList(X7sConCompoList *ccList, bool asc_order);
X7SLIBC void x7sPrintConCompo(X7sConCompo *cc);



// List Features
//----------------------------------------------------------------

/**
 * @brief 
 */


typedef struct X7sFeaturesBox{
	
	int id;
	//Bounding Box position
	int min_x;	//!< Left side of the bounding box
	int max_x; 	//!< Right side of the bounding box
	int min_y;	//!< Top side of the bounding box
	int max_y;	//!< Bottom side of the bounding box

	int x; //!< Center of mass in x axis (left->right)
	int y; //!< Center of mass in y axis (top->bottom)

	int area; //!< Number of pixel=@ref X7S_CV_TRUEPIX in the bounding box.
	
	float features_mean_diffx; 
	float features_mean_diffy; 
	float features_desv_diffx; 
	float features_desv_diffy; 
	int num_features_box;
	int dim_x;
	int dim_y;
	CvHistogram *hist_ini;
	int flag_hist;

} X7sFeaturesBox;


typedef struct X7sFeaturesHeaderBox{
	
	X7sFeaturesBox* box_list;
	struct X7sFeaturesHeaderBox* next_HeaderBox;
	struct X7sFeaturesHeaderBox* prev_HeaderBox;

	int last_index;
	int ok;

} X7sFeaturesHeaderBox;


/**
 * @brief 
 */
typedef struct {

	int num_features;
	int max_num_features;

	int num_boxfeatures;
	int max_num_boxfeatures;
	int last_IDbox;
	int count_features;
	int win_size;

	int size;
	int min_area;
	int min_area_delete;
	float interval[2];
	int max_count_history;
	int min_movement;
	int max_area_person;
	int max_dim_xy;
	int min_dim_xy;

	X7sFeaturesHeaderBox *vec;
	int num_nbox;

	int count_header;


	CvSize size_img;

	double quality;
	double min_distance;

	IplImage *pImg_clone, *eig, *grey, *prev_grey, *pyramid, *prev_pyramid, *temp, *swap_temp, 
		*mask_tracker, *rect_ini, *rect_fin, *mask_hist, *yuv, *y_plane_i, *u_plane_i, *v_plane_i,
		*y_plane_f, *u_plane_f, *v_plane_f, *backproject, *backprojectGRAY, *back_image;

	IplImage *pImg_diff, *pImg_diff2;

	IplImage *planes_i[3];
	IplImage *planes_f[3];


	int hist_size[3];
	float range[2];
	float* ranges[3];

	CvHistogram *hist_fin;

	CvPoint2D32f* points[2];
	float *points_difx;
	float *points_dify;

	int *points_IDbox;
	int *lost_points;
	int *valid_ID;
	char* status;
	int flags_pyramid;

	int temp_flags;

} X7sFeaturesList;


X7SLIBC X7sFeaturesList* x7sCreateFeaturesList(const CvSize *size CV_DEFAULT(NULL));
X7SLIBC X7sFeaturesHeaderBox * x7sCreateHeaderBox(const X7sFeaturesList* pFeatList,X7sFeaturesHeaderBox* pFeatPrev);
X7SLIBC void x7sReleaseFeaturesList(X7sFeaturesList **featList);
X7SLIBC bool x7sBoxOverlapping(X7sConCompo *ccbox, X7sFeaturesBox *fbox);
X7SLIBC void x7sDelHeaderBox(X7sFeaturesHeaderBox **first, X7sFeaturesHeaderBox **f, int nbox);
X7SLIBC int x7sDelBoxTrackOverlapping(X7sFeaturesBox *fbox1, X7sFeaturesBox *fbox2);



// Color Conversion
//----------------------------------------------------------------

/**
 * @brief The Color Space define outside OpenCV
 * @anchor X7S_CV_xxx
 */
enum {
	X7S_CV_CVTCOPY=CV_COLORCVT_MAX,	//!< Copy image in the same format.

	X7S_CV_YUV4222BGR,	//!< Convert from YUV 4:2:2 to BGR
	X7S_CV_YUV4222RGB,	//!< Convert from YUV 4:2:2 to RGB
	X7S_CV_BGR2YUV422,	//!< Convert from BGR to YUV 4:2:2
	X7S_CV_RGB2YUV422, //!< Convert from RGB to YUV 4:2:2

	X7S_CV_YUV4222YUV, //!< Convert from YUV 4:2:2 to YUV
	X7S_CV_YUV2YUV422, //!< Convert from YUV to YUV 4:2:2

	X7S_CV_H2BGR,		//!< Convert an image from Hue to BGR.
	X7S_CV_H2BGRLEGEND,	//!< Convert an image from Hue to BGR adding a legend of the color.
	X7S_CV_HNZ2BGR,		//!< Convert an image from Hue (non zeros) to BGR.
	X7S_CV_HNZ2BGRLEGEND,//!< Convert an image from Hue (non zeros) to BGR adding legend for the color.

	X7S_CV_JPEG2BGR,	//!< Convert from JPEG buffer 2 BGR image
	X7S_CV_JPEG2RGB,	//!< Convert from JPEG buffer 2 BGR image
	X7S_CV_JPEG2BGRA,	//!< Convert from JPEG buffer 2 BGRA image
	X7S_CV_JPEG2RGBA,	//!< Convert from JPEG buffer 2 RGBA image

	X7S_CV_LRLE2GREY,	//!< Convert from Line-RLE code 2 Grey image.

	X7S_CV_HEAT2BGR,	//!< Convert thermal (heat) image to BGR (Using Dark->Cold->Warm colorMap).
	X7S_CV_HEATW2BGR, //!< Convert thermal (heat) image to BGR (using Dark->Warm colorMap).
	X7S_CV_HEATC2BGR, //!< Convert thermal (heat) image to BGR (using Cold->Warm colorMap).

	X7S_CV_BGRA2BGRA255,//!< Fill alpha pixel to 255.
};

X7SLIBC int x7sReallocImage(CvSize size, int depth, int channels, IplImage **pDst);
X7SLIBC int x7sReallocColorImage(const IplImage *src, IplImage **pDst, int code);
X7SLIBC int x7sCvtColor(const IplImage *src, IplImage *dst, int code);
X7SLIBC int x7sCvtColorRealloc(const IplImage *src, IplImage **pDst, int code);
X7SLIBC int x7sCvtColorGetChannel(int code, bool is_dst);
X7SLIBC IplImage* x7sCreateCvtImage(const IplImage *src, int code);
X7SLIBC CvScalar x7sCvtHue2BGRScalar(int i, int max, int mod);
X7SLIBC CvScalar x7sCvtHue2RGBScalar(int i, int max, int mod);
X7SLIBC int x7sCvtColorMap2BGR(const IplImage *src, IplImage *dst,uint32_t* colorMap);


// Filtering
//----------------------------------------------------------------


/* x7sFilters.c */
X7SLIBC int x7sPseudoMed(const IplImage *src, IplImage *dst, int n_pix);
X7SLIBC int x7sPixelate(const IplImage *src, IplImage *dst, IplImage *mask, int pixw, int pixh);

/* x7sMorphological.c */
X7SLIBC int x7sErodeFast(IplImage *im,int size);
X7SLIBC int x7sDilateFast(IplImage *im, int size);

/* x7sFill.c */
X7SLIBC int x7sFillHoles(IplImage *im);
X7SLIBC int x7sFillLineHoles(IplImage *im);
X7SLIBC int x7sFillEdges(IplImage *edge, const IplImage *mask);


// Misc Image processing
//----------------------------------------------------------------


/* x7sMakeBlobs.c */
X7SLIBC int x7sMakeBlobs(IplImage *mask);

/* x7sFindMax.c */
X7SLIBC double x7sFindMax(const IplImage *im);

/* x7sDownSample.c */

/**
 * @brief the type of downsample we need to perform
 * @anchor X7S_CV_DOWNSAMP_xxx
 */
enum {
	X7S_CV_DOWNSAMP_YUV422_MEAN2,//!< Downsample taking the mean on all channel (4 pixels -> 2 pixels).
	X7S_CV_DOWNSAMP_YUV422_MEAN2Y,//!< Downsample taking the mean only on Y channel (4 pixels -> 2 pixels).
};

X7SLIBC int x7sDownSample(const IplImage *src,IplImage *dst, int type);

// Draw & Display
//----------------------------------------------------------------


/* x7sAdd.c */
X7SLIBC void x7sAddWeightedScalar(const CvArr *src1, const CvArr *src2, CvArr *dst,CvScalar value,const IplImage *mask CV_DEFAULT(NULL));
X7SLIBC void x7sAddAlphaLayer(const CvArr *src1, const CvArr *src2, CvArr *dst);

/* x7sDraw.c */
X7SLIBC int x7sDrawLegend(IplImage *im, const char *text, CvScalar color, int n_line);
X7SLIBC int x7sDrawCross(IplImage *im, CvPoint center, CvScalar color,  int size);
X7SLIBC int x7sDrawConCompoList(IplImage *im,const X7sConCompoList* ccList);
X7SLIBC int x7sDrawMultiImages(IplImage **pMultiIm, int nImgs, ...);

/* x7sDisplay.c */
X7SLIBC void x7sShowImage(const char *name, const IplImage *image);
X7SLIBC void x7sShowImageRescale(const char *name, const IplImage *image);
X7SLIBC void x7sShowImageRescaleMax(const char *name, const IplImage *image);
X7SLIBC void x7sShowHistogram(const IplImage *im);


// MJPEG and Special JPEG functions
//----------------------------------------------------------------

#define X7S_CV_MJPEG_MAXFILESIZE 1073741824	//!< Maximum file size is 1Gb (2^30 bytes).
#define X7S_CV_MJPEG_MAXFRAMESIZE 	  327680	//!< Maximum frame size is 320 Kb (Define in net)
#define X7S_CV_MJPEG_FRAMEDATA_TYPE X7S_BYTEARRAY_OTHERS 	//!< Type of the MJPEG frame data

/**
 * @brief The different tag used for the X7sMJPEGFrame.
 * @anchor X7S_CV_MJPEG_FRAMETAG_xxx
 */
enum {
	X7S_CV_MJPEG_FRAMETAG_NORMAL= 0x00,	//!< Normal frame.
	X7S_CV_MJPEG_FRAMETAG_REFFR = 0x01,	//!< Reference frame where data are full
	X7S_CV_MJPEG_FRAMETAG_ALARM = 0x02,	//!< Frame with an alarm event
	X7S_CV_MJPEG_FRAMETAG_REFALR= 0x03,	//!< Combination of previous flags.
};

/**
 * @brief Combine a simple JPEG with some relevant metadata.
 *
 *	This structure is used to save/hide metadata in a JPEG image.
 *  This image is then written in a MJPEG file as a video+metadata stream.
 */
typedef struct
{
	int id;						//!< ID of the MJPEG Frame
	uint8_t tag;				//!< Tag of this frame (Alarm Frame, Reference Frame, etc, ...)
	time_t time;				//!< Time in seconds since the Unix time (01/01/1970).
	X7sByteArray *pJpeg;		//!< Pointer on a memory with the JPEG data.
	X7sByteArray *pBlobs;		//!< Pointer on a memory with the blobs position and trajectory data.
	X7sByteArray *pAlarm;		//!< Pointer on a memory with the alarms events data.
} X7sMJPEGFrame;

/**
 * @brief Structure to compute the FPS.
 */
typedef struct
{
	time_t t0;			//!< Time of the first frame in a second.
	int	t0_id;			//!< ID of the frame at t0
	uint8_t first_t0;	//!< If it is the first time we set t0.
	float fps;			//!< Frame per seconds.
} X7sMJPEGFrameRate;

#define x7sReleaseMJPEG x7sReleaseFILE
X7SLIBC FILE *x7sOpenMJPEG(const char *filepath, X7sByteArray *mjpegFrameData , bool read);
X7SLIBC int x7sFillMJPEG(const X7sMJPEGFrame *frame, X7sByteArray *mjpegFrameData);
X7SLIBC int x7sExtractMJPEG(X7sMJPEGFrame *frame,const X7sByteArray *mjpegFrameData);
X7SLIBC int x7sWriteMJPEG(const X7sByteArray *mjpegFrameData, FILE *pFile);
X7SLIBC int x7sReadFileMJPEG(X7sByteArray *mjpegFrameData, FILE *pFile, int num_frame);
X7SLIBC int x7sFindTimeMJPEG(X7sByteArray *mjpegFrameData, FILE *pFile,time_t search_time);
X7SLIBC int x7sFindTagMJPEG(X7sByteArray *mjpegFrameData, FILE *pFile, uint8_t search_tag);
X7SLIBC int x7sGetFpsMJPEG(const X7sMJPEGFrame *mjpegFrame, X7sMJPEGFrameRate *mjpegFPS);
X7SLIBC const char *x7sCTime(const time_t* time);


// Tools
//----------------------------------------------------------------

/* x7sLog.c */
X7SLIBC int x7sCvLog_SetLevel(int log_level);
X7SLIBC int x7sCvLog_Config(int log_level, FILE *f_out X7S_DEFAULT(NULL), FILE *f_err X7S_DEFAULT(NULL), int no_error X7S_DEFAULT(X7S_LOG_NOERROR));



/* x7sBit.c */

#define X7S_BIT_MLMASK_8   0x01					//!< Mask for the MLB of a 8-bit integer
#define X7S_BIT_MRMASK_8   0x80					//!< Mask for the MRB of a 8-bit integer
#define X7S_BIT_MLMASK_16 0x0001				//!< Mask for the MLB of a 16-bit integer
#define X7S_BIT_MRMASK_16 0x8000				//!< Mask for the MRB of a 16-bit integer
#define X7S_BIT_MLMASK_32 0x80000000 	//!< Mask for the MLB of a 32-bit integer
#define X7S_BIT_MRMASK_32 0x00000001 	//!< Mask for the MRB of a 32-bit integer


//!< Reset val to zeros.
#define X7S_CV_TAG_RST(val) \
		(val = 0)

//!< Set the bits of val to one where bitmask
#define X7S_CV_TAG_ADD(val,bitmask) \
		(val |= bitmask)

//!< Set the bits of val to zeros where bitmask
#define X7S_CV_TAG_DEL(val,bitmask) \
	(val &= (~bitmask))

//!< Toggle the bits of val where bitmask
#define X7S_CV_TAG_TOGGLE(val,bitmask) \
	(val ^= bitmask)

//!< Return true is the bits of val are equal to 1 where bitmask
#define X7S_CV_TAG_IS(val,bitmask) \
	((val & bitmask) == bitmask)

//!< Set the bits of value where bitmask to the value of enable.
#define X7S_CV_TAG_SET(val,bitmask,enable) \
	if(enable) X7S_CV_TAG_ADD(val,bitmask); else X7S_CV_TAG_DEL(val,bitmask)

X7SLIBC void x7sBitToByteArray8(uint8_t val, uint8_t *barray);
X7SLIBC void x7sBitToByteArray32(uint32_t val, uint8_t *barray);
X7SLIBC void x7sBitToCString8(uint8_t val, char *sz);
X7SLIBC void x7sBitToCString32(uint32_t val, char *sz);
X7SLIBC uint8_t x7sBitPushBack32(uint32_t *word, uint8_t push_val);
X7SLIBC uint8_t x7sBitCount32(uint32_t word);
X7SLIBC uint8_t x7sBitCountConsec32(uint32_t word, int val);
X7SLIBC uint8_t x7sBitFastCount32(uint32_t word);
X7SLIBC const uint8_t* x7sBitFastCountGenLUT256();



/* x7sTools.c */

X7SLIBC double x7sLog2(double val);

X7SLIBC uint32_t x7sReadUIntByName(const CvFileStorage* fs, const CvFileNode* map,
        const char* name, uint32_t default_value);
X7SLIBC void x7sIplPrintInfos(const IplImage *img);
X7SLIBC void x7sIplPrintValues(const IplImage *img);
X7SLIBC void x7sIplPrintValuesFull(const IplImage *img, int start_x, int start_y, int nof_x, int nof_y);
X7SLIBC void x7sPrintMat(CvMat *A);


enum {
	X7S_CV_GRADIENT_X=0x00,
	X7S_CV_GRADIENT_Y,
	X7S_CV_BW_COLUMN,
	X7S_CV_GRADIENT_BGR=0xF0,

};

X7SLIBC int x7sPatternImage(IplImage **pDst, int type);


//Background/Foreground Model
//----------------------------------------------------------------


/**
 * @brief Redefine the type of background modeling and refreshing that we use.
 * @anchor X7S_BG_MODEL_xxx
 */
enum {
	X7S_BG_MODEL_FGD=CV_BG_MODEL_FGD,					//!< Foreground detection implemented in OpenCV 1.
	X7S_BG_MODEL_MOG=CV_BG_MODEL_MOG,		 			//!< Mixture of Gaussian implemented in OpenCV 1.1
	X7S_BG_MODEL_CBK_INTER = 15,						//!< CodeBook Intervals background 
	X7S_BG_MODEL_CBK=7,									//!< CodeBook background modeling
	X7S_BG_MODEL_FGD_SIMPLE=CV_BG_MODEL_FGD_SIMPLE,		//!< Same as X7S_BG_MODEL_FGD
	X7S_BG_MODEL_MOC=10, 		//!< Background model according to @cite{Appiah2005b} using float.
	X7S_BG_MODEL_MOC_HW, 		//!< Background model according to @cite{Appiah2005b}.
	X7S_BG_MODEL_BCD,				//!< Brightness/Color Distortion background model using float.
	X7S_BG_MODEL_MID,				//!< Mosaic Image Difference foreground @cite{Li2008}
};




/* Cluster background */
/**
 * @brief Parameters for the Cluster Background Model.
 * @ingroup x7scv
 */
typedef struct {
	int K;	//!< Number maximum of cluster (typically 3)
	int T;	//!< Dimensionality Threshold (See @cite{Grimson2005})
	int dB;	//!< distance from the center (Big)
	int dS;	//!< distance from the center (Small)
	int insDelayLog;	//!< Insertion delay. @see CV7_BG_INS_MEDIUM.
	int is_dispBG;	 	//!< If true, update bg_model->background to be displayable (BGR format)
}
X7sClusterBGStatModelParams;

/**
 * @brief Structure of a cluster (CenterValue,Weight).
 */
typedef struct {
	unsigned int C:11;	//!< Center Value of background cluster.
	unsigned int w:7;	//!< Weight of background cluster.
	unsigned int zeros:14;
	float fpC;
	float fpW;
}
X7sCluster;

/**
 * @brief Structure of the Cluster Background model.
 * @ingroup x7scv
 */
typedef struct
{
    CV_BG_STAT_MODEL_FIELDS();	//!< Define that declare the same fields for all the BGStatModel.
	IplImage *imK;				//!< Give the index of the K which match previously
	IplImage *imPrev;			//!< Previous image.
	X7sCluster* data;			//!< An array composed by (imageSize * K) Cluster.
    X7sClusterBGStatModelParams params; //!< Parameters
}
X7sClusterBGStatModel;


X7SLIBC CvBGStatModel* x7sCreateClusterBGModel( IplImage* first_frame,
                X7sClusterBGStatModelParams* parameters);





/* CodeBook intervals background */
/**
 * @brief Parameters for the CodeBook intervals Background Model.
 * @ingroup x7scv
 */
typedef struct {
    int cbBounds[3];
    int modMin[3];
    int modMax[3];
	int window;
	int num_codewords_elements;
	int nframesToLearnBG;
}
X7sCBookInterBGStatModelParams;


/**
 * @brief Structure of a codebook element.
 */
typedef struct{
	uchar f;		//valid codeword 
	float weight;	//weight 
	uchar p;		//first access time
	uchar q;		//last access time
	float mean[3];
    uchar boxMin[3];
    uchar boxMax[3];
    uchar learnMin[3];
    uchar learnMax[3];
}
X7SBGCodeBookElem;

/**
 * @brief Structure of the Cluster Background model.
 * @ingroup x7scv
 */
typedef struct
{
    CV_BG_STAT_MODEL_FIELDS();	//!< Define that declare the same fields for all the BGStatModel.
    X7SBGCodeBookElem** cbmap;  // set of CodeBooks
	int nframes;
	int width; 
	int height;

    X7sCBookInterBGStatModelParams params; //!< Parameters

}
X7sCBookInterBGStatModel;


X7SLIBC CvBGStatModel* x7sCreateCBookInterBGModel(IplImage* first_frame,
                X7sCBookInterBGStatModelParams* parameters);



/* Brightness and chromacity distortions (BCD) background */
/**
 * @brief Parameters for BCDist Background Model.
 * @ingroup x7scv
 */
typedef struct {
	int refreshDelay;	//!< Is the nFrames a FG pixel stay as background until start to insert into BG.
	int insDelayLog;	//!< Is the log2(nFrames) needed for a FG pixel to be inserted as BG. (nFrames=2^insDelayLog).
	int threshold;		//!< Threshold on color distortion to considerate pixel as foreground.
	int pseudomed_1;	//!< Size of the kernel for a pseudo-median filter on foreground (1st pass)
	int pseudomed_2;	//!< Size of the kernel for a pseudo-median filter on foreground (2nd pass)
	int dilate_k;		//!< Dilate filter on foreground (Not performed if value equal to 1)
	int is_dispBG;		//!< If true, update bg_model->background to be displayable (BGR format)
} X7sBCDistBGStatModelParams;


/**
 * @brief Structure of the Brightness/Color Distortions Background Model.
 *
 * This function internally use frames in YUV422 format.
 * @ingroup x7scv
 */
typedef struct {
    CV_BG_STAT_MODEL_FIELDS();	//!< Define that declare the same fields for all the BGStatModel.
	IplImage *count_delay;				//!< Special count Mask
	IplImage *mean_YUV422;				//!< Mean image (maybe the same as BG).
	IplImage *bg_YUV422;		//!< Background in YUV422.
	bool if_reset;				//!< Reset mean image into I.
	X7sBCDistBGStatModelParams params;	//!< Parameters

} X7sBCDistBGModel;

X7SLIBC CvBGStatModel* x7sCreateBCDistBGModel( IplImage* first_frame,
                X7sBCDistBGStatModelParams* parameters);



/* Multi-Image Difference BG Model */
/**
 * @brief Parameters for MIDiff Background Model.
 * @ingroup x7scv
 */
typedef struct {
	int blockSize;			//!< Is the nFrames a FG pixel stay as background until start to insert into BG.
	int subSeqLogSize;	//!< size of the subSeq (Must be 4).
	int nSubSeq;			//!< Number of subsequence (Time observed: nSubSeq*2^subSeqLogSize / fps).
	uint8_t threshold;		//!< Threshold on frame difference to considerate pixel as foreground.
	int runModulo;			//!< this parameter give us a way to compute the algo each runModulo frames.

} X7sMIDiffBGStatModelParams;


/**
 * @brief Structure of the Mosaic Image Difference Background Model.
 *
 * @ingroup x7scv
 */
typedef struct {
    CV_BG_STAT_MODEL_FIELDS();	//!< Define that declare the same fields for all the BGStatModel.
	int nFrames;							//!< Number of frames.
	IplImage *mb_prev;				//!< Mosaic block image M_{t-1}
	IplImage *mb_act;				//!< Mosaic bloc image M_{t}
	IplImage *midPeriod;				//!< The mid features during a period of time (Do not use as Image)
	X7sMIDiffBGStatModelParams params;	//!< Parameters

} X7sMIDiffBGModel;

X7SLIBC CvBGStatModel* x7sCreateMIDiffBGModel( IplImage* first_frame,
                X7sMIDiffBGStatModelParams* parameters);



/* Other implementation */

/**
 * @brief Structure of the CodeBook Background Model.
 *
 * @note No parameters are defined for this model
 *
 * @ingroup x7scv
 */
typedef struct {
	int test;
} x7sCBookBGStatModelParams;

/* General structure for parameters of background modeling */

/**
 * @brief General structure for the parameters of BackGround modeling.
 * @ingroup x7scv
 */
typedef struct {
	int type;
	X7sClusterBGStatModelParams *cluster;
	X7sBCDistBGStatModelParams  *bcd;
	X7sMIDiffBGStatModelParams  *mid;
	x7sCBookBGStatModelParams *cbk;
	X7sCBookInterBGStatModelParams *cbk_inter;
	CvGaussBGStatModelParams *gauss;
	CvFGDStatModelParams *fgd;

} X7sBGStatModelParams;

X7SLIBC X7sBGStatModelParams* x7sCreateBGStatModelParams(int type);
X7SLIBC void x7sReleaseBGStatModelParams(X7sBGStatModelParams** p_gen_params);
X7SLIBC CvBGStatModel* x7sCreateBGModelType(IplImage *first_frame ,int type);
X7SLIBC CvBGStatModel* x7sCreateBGModel(IplImage *first_frame ,X7sBGStatModelParams *parameters);



#endif /* _X7SCV_H_ */
