/**
*  @file
*  @brief The Video Surveillance Library of X7S.
*
*  This library is based on various library:
*  	- The VideoSurveillance of OpenCV (cvvidsurv.hpp).
*  	- The X7S Computer Vision (x7scv.h)
*  	- The X7S XML library to load the parameters (x7sxml.h).
*
*  @date 15-may-2009
*  @author Benoit RAT
*/

#ifndef _X7SVIDSURV_H_
#define _X7SVIDSURV_H_

#include "x7sdef.h"
#include "x7scv.h"
#include "x7sxml.h"

#include <cvaux.h>
#include <highgui.h>	//Save and load images.
#include <cvaux.hpp>
#include <cvver.h>
#include <cvvidsurv.hpp>

#include <cstddef>
#include <vector>

using std::vector;

#define X7S_VS_RET_ERR X7S_RET_ERR;
#define X7S_VS_RET_OK X7S_RET_OK;
#define X7S_VS_NUMFRAMES_TRAIN 30;		//!< Number of frame the background must process before creating the FGMask.
#define X7S_VS_TAGNAME_PIPELINE "vidsurv_pipeline"
#define X7S_VS_TAGNAME_TRAJGEN "trajgen"



/*
 * Depending on the version of OpenCV some code has changed to correct warning during compilation:
 * 	- x7scharA:
 * 		- SetParam((x7scharA*)"...",xxx)
 * 		- AddParam((x7scharA*)"...",xxx)
 * 		- SetNickName((x7scharA*)"...")
 * 	- x7scharB:
 * 		- x7scharB* GetStateDesc()
 * 		- AddParam(const char* name,x7scharB* value)
 */
#if CV_MAJOR_VERSION < 1
#error Only support openCV after version 1.0.0
#endif
#if   CV_MAJOR_VERSION==1 && CV_MINOR_VERSION==0 //version 1.0.x
typedef char x7scharA;	//!< C-string in OpenCV (version == 1.0.x)
typedef char x7scharB; //!< C-string in OpenCV (version >= 1.0.x)
#elif CV_MAJOR_VERSION==1 && CV_MINOR_VERSION==1 && CV_SUBMINOR_VERSION==0 //version 1.1.0
typedef const char x7scharA; //!< C-string in OpenCV (version == 1.1.0)
typedef char x7scharB; 		//!< C-string in OpenCV (version == 1.1.0)
#else
typedef const char x7scharA;	//!< C-string in OpenCV (version >= 1.1.1)
typedef const char x7scharB;	//!< C-string in OpenCV (version >= 1.1.1)
#endif

//Pre-declaration of the variable.
class X7sVSAlrDetector;
class X7sVSAlrManager;
class X7sBlobTrajectory;
class X7sBlobTrajGenerator;


//========================================================================
//-- Video Surveillance Pipeline.

enum {
	X7S_VS_PIPELINE_FG=0,				//!< Try with ForeGround
	X7S_VS_PIPELINE_FEATURES=1,		//!< Try with good features to track and KLT.
	X7S_VS_PIPELINE_DESCRIPTOR=2,	//!< Try with SURF, YAPE descriptors.
	X7S_VS_PIPELINE_QUEUE=3,				//!< Compute for queue length
	X7S_VS_PIPELINE_FALLING=4,			//!< Detect falling object
	X7S_VS_END_PIPELINE
};


/**
* @brief The Video Surveillance Pipeline.
*
* 	It performs video surveillance and trajectory analysis, like the one from OpenCV.
*
* @ingroup x7svidsurv
* @see X7sVidSurvPipelineFG.
*/
class X7S_EXPORT X7sVidSurvPipeline : public X7sXmlObject {
public:
	X7sVidSurvPipeline(X7sXmlObject *parent,int type,int ID);
	virtual ~X7sVidSurvPipeline();
	virtual void        Process(IplImage* pImg, IplImage* pMask = NULL)=0;
	virtual void 		process(const cv::Mat& src, const cv::Mat& fg=cv::Mat()){ }; //!< For next compatibility.
	virtual void 		Draw(IplImage* pImg);
	virtual void SetNoInterest(IplImage *pImg);
	virtual CvBlob*     GetBlob(int index);
	virtual CvBlob*     GetBlobByID(int ID);
	virtual int         GetBlobNum();
	virtual IplImage*   GetFGMask();
	virtual IplImage* 	GetBGImg();
	virtual float       GetState(int BlobID);
	virtual x7scharB* 	GetStateDesc(int BlobID);
	virtual X7sBlobTrajGenerator* GetTrajGenerator();
	virtual X7sVSAlrManager* GetAlarmManager();
	virtual void    	Release();

protected:
	static void LoadParameters(CvVSModule *module, TiXmlNode * node);
	virtual void ProcessBTGen(IplImage* pImg, IplImage* pFG) {};
	virtual void ProcessAlarm();

    X7sBlobTrajGenerator*   m_pBTGen;			//!< Blob Trajectory Generator
    X7sVSAlrManager* 		m_pAlarmMan;			//!< The Alarm manager module
    int						m_FrameCount;		//!< Count the number of frame for each process.
    CvBlobSeq*              m_pBlobList;				//!< A pointer on the list of the blob tracked.
    IplImage* 					m_pBGImg;				//!< The background image
    IplImage*               	m_pFGMask;				//!< The ForeGroung mask obtained.
    bool 						m_FGMaskDel;			//!< Delete ForeGround mask if it is created.
    IplImage*					m_pNIMask;				//!< The NoInterest Mask image
    bool 						m_NIMaskDel;			//!< Delete NoInterest Mask if it is created.

	bool						m_enable;				//!< Enable the VidSurvPipeline
	bool						m_dbgWnd;				//!< Debug windows.
	bool						m_draw;					//!< Enable the general draw.

private:
	

};

X7S_EXPORT X7sVidSurvPipeline* x7sCreateVidSurvPipeline(X7sXmlObject* parent, int type);
X7S_EXPORT X7sVidSurvPipeline* x7sCreateVidSurvPipeline(X7sXmlObject* parent,const TiXmlElement *cnode);

X7S_EXPORT X7sVidSurvPipeline* x7sCreateVidSurvPipelineFG(X7sXmlObject *parent=NULL);
X7S_EXPORT X7sVidSurvPipeline* x7sCreateVidSurvPipelineFeatures(X7sXmlObject *parent=NULL);
X7S_EXPORT X7sVidSurvPipeline* x7sCreateVidSurvPipelineFalling(X7sXmlObject *parent=NULL);
X7S_EXPORT X7sVidSurvPipeline* x7sCreateVidSurvPipelineQueue(X7sXmlObject *parent=NULL);
X7S_EXPORT CvBlobTrackerAuto* cvCreateBlobTrackerAutoCopy1(CvBlobTrackerAutoParam1* param=NULL);
X7S_EXPORT CvBlobTrackerAuto* cvCreateBlobTrackerAuto2(CvBlobTrackerAutoParam1* param=NULL);


//========================================================================
//-- Foreground Detector Module. (X7sFGDetector.cpp)


/**
* @brief Generic and abstract class for the X7s Foreground Detector.
*/
class X7S_EXPORT X7sFGDetector: public X7sXmlObject {
public:
	X7sFGDetector(X7sXmlObject *parent,const std::string& name,int type,int ID): X7sXmlObject(parent,name,type,ID) {};
	virtual IplImage* GetMask()=0;
	virtual IplImage* GetBackGround()=0;
	virtual void Process(IplImage *pImg)=0;
	virtual void SetNoInterest(IplImage *pImg)=0;
	virtual void Release()=0;

};

X7S_EXPORT X7sFGDetector* x7sCreateFGDetector(X7sXmlObject *parent,int type=-1);
X7S_EXPORT X7sFGDetector* x7sCreateFGDetector(X7sXmlObject *parent,const TiXmlElement * cnode);


//========================================================================
//-- Blob Detector Module (X7sBlobDetector.cpp)

/**
* @brief The type of Blob Detector.
*/
enum {
	X7S_VS_BD_SIMPLE,	//!< Simple Connected component with low history.
	X7S_VS_BD_CC,		//!< Connected Component using Find Contours.
	X7S_VS_BD_CV,		//!< Similar to the one from OpenCV with small improvement.
	X7S_VS_BD_CCCM,		//!< Connected Component with cluster merging.
	X7S_VS_BD_LRLE,		//!< Parse data in Line-Run-Length-Encoding.
	X7S_VS_BD_END,
};


X7S_EXPORT CvBlobDetector* x7sCreateBlobDetector(int type=X7S_VS_BD_CCCM);
X7S_EXPORT CvBlobDetector* x7sCreateBlobDetectorCV();
X7S_EXPORT CvBlobDetector* x7sCreateBlobDetectorCCCM();
X7S_EXPORT CvBlobDetector* x7sCreateBlobDetectorLRLE();
X7S_EXPORT void x7sDrawCvBlobSeq(IplImage *pImg, CvBlobSeq *pBlobList, int hue_color=-1, bool is_legend=true);



//========================================================================
//-- Other Modules (X7sVSPipeline.cpp)

/**
* @brief The Type of Blob Tracker.
*/
enum {
	X7S_VS_BT_CC,		//!< Connected Component tracking
	X7S_VS_BT_CC2,		//!< Connected Component tracking (with collision resolution)
	X7S_VS_BT_CCMSPF,	//!< Connected Component and Mean-Shift Particle Filtering during collision.
	X7S_VS_BT_MS,		//!< Mean-Shift only (No Background model need).
	X7S_VS_BT_MSPF,		//!< Mean-Shift Particle Filtering using Bhattacharya coefficient.
	X7S_VS_BT_MSFG,		//!< Mean-Shift on ForeGround image (Connected component).
	X7S_VS_BT_MSFGS,	//!< Mean-Shift on ForeGround with Scaling factor.
	X7S_VS_BT_KM,		//!< Tracking using only Kalman filter.
	X7S_VS_BT_LHR,		//!< Likelihood Histogram tracker (Not Implemented Yet)
	X7S_VS_BT_LHRS,		//!< Likelihood Histogram tracker with Scaling factor (Not Implemented Yet).
	X7S_VS_BT_END,
};

X7S_EXPORT CvBlobTracker* x7sCreateBlobTracker(int type=X7S_VS_BT_CC, void *param=NULL);
X7S_EXPORT CvBlobTracker* x7sCreateBlobTrackerCC();
X7S_EXPORT CvBlobTracker* x7sCreateBlobTrackerCC2();



/**
* @brief The Type of Blob Tracker trajectory Post Processing.
*/
enum {
	X7S_VS_BTPP_KF,		//!< Post-Processing with Kalman Filter.
	X7S_VS_BTPP_TAE,	//!< Post-Processing using a Linear Time Average with Exponential Kernel.
	X7S_VS_BTPP_TAR,	//!< Post-Processing using a Linear Time Average with Rectangular Kernel.
	X7S_VS_BTPP_END,
};

X7S_EXPORT CvBlobTrackPostProc* x7sCreateVSModBTPostProc(int type=X7S_VS_BTPP_KF);
X7S_EXPORT CvBlobTrackPredictor* x7sCreateModuleBlobTrackPredictKalman();


/**
* @brief The type of Blob Tracker Trajectory File Generator.
* @anchor X7S_VS_BTGEN_xxx
*/
enum {
	X7S_VS_BTGEN_TXT,  	//!< Write trajectory in a text file (Each line is a blob)
	X7S_VS_BTGEN_YML,	//!< Write trajectory in YML format (Structured text file)
	X7S_VS_BTGEN_END,
};

X7S_EXPORT CvBlobTrackGen *x7sCreateVSModBTGen(int type=X7S_VS_BTGEN_TXT);


//========================================================================
//-- Blob Trajectory Module (X7sVSAlrManager.cpp)

#define X7S_VS_BLOBTRAJ_DATA_SIZE   24576	//!< Number of byte initially used for data size (24 kB).
#define X7S_VS_BLOBTRAJ_DATA_NPTS 20			//!< Default number of points for the data.
#define X7S_VS_BLOBTRAJ_DRAW_LENGTH 50 		//!< Number of points in the trajectory draw.
#define X7S_VS_BLOBTRAJ_DRAW_STAY	20		//!< Number of frame with no update during we still draw the blob
#define X7S_VS_BLOBTRAJ_MEMORY 		100 	//!< Number of frame with no update of trajectory to delete a blob.

/**
*	@brief The Trajectory of a blob

* 	This structure can be cast as a simple blob due to the variable X7sBlobTrajectory::blob.
*  Therefore we can easily access to its ID and first position
* @code
*  CvBlob pBlob = (CvBlob*)new X7sBlobTrajectory;
*  @endcode
*
*	@ingroup x7svidsurv
*/
class X7S_EXPORT X7sBlobTrajectory : public CvBlob  {

public:
	X7sBlobTrajectory(const CvBlob& blob);
	X7sBlobTrajectory(const X7sByteArray* data);
	X7sBlobTrajectory(const X7sBlobTrajectory& copy);
	X7sBlobTrajectory& operator=(const X7sBlobTrajectory&);
	~X7sBlobTrajectory() { this->Release(); }	//!< Default detructor

	bool AddBlob(CvBlob* pBlob, int frameID);
	void Release();
	void Smooth(int filter_size);
	void Hide(bool enable=true);
	void Draw(IplImage *pImg,int length=-1, int hue_color=-1, int legend=-1,int thickness=1);
	const CvBlob *GetLastBlob();
	const CvBlob *GetBlob(int index);
	const X7sByteArray* GetData(int maxNbPoints,int maxLength);
	int GetBlobNum() { return m_blobSeq.GetBlobNum(); };
	const CvBlobSeq * GetBlobSeq() const { return &m_blobSeq; }
	int GetFirstFrame() const {return m_frameBegin; }
	int GetLastFrame() const { return m_frameLast; }
	bool isSimilar(const X7sBlobTrajectory* pBlobOther, int history=3, bool olderOnly=true) const;
	bool IsToDelete(int nframe=0);
	bool isHidden() const { return m_frameHidden>0; }
	std::string toString();

private:
	CvBlobSeq  	m_blobSeq;			//!< The time sequence of a blob.
	int         m_frameBegin;		//!< At which frame this begin
	int         m_frameLast;		//!< At which frame this will end.
	int 			m_frameHidden;	//!< At which frame it was set to hidden.
	int 		m_minDist;			//!< Minimum distance between 2 points to be added to the trajectory.
	int 		m_smoothSize;		//!< Size of the smooth filter.
	bool 		m_isExternalHidden;			//!< Status if the has been hidden externally or not.
	X7sByteArray *m_pData;
};


/**
* @brief Manage and generate the list of X7sBlobTrajectory
* @ingroup x7svidsurv
*/
class X7S_EXPORT X7sBlobTrajGenerator : public X7sXmlObject {
private:	//This file contains a pointer on a byteArray and therefore can not be copied implicitly.
	X7sBlobTrajGenerator(const X7sBlobTrajGenerator& copy);				//!< declared private, but not defined, to prevent copying
	X7sBlobTrajGenerator& operator=(const X7sBlobTrajGenerator& copy);	//!< declared private, but not defined, to prevent copying

public:
	X7sBlobTrajGenerator(X7sXmlObject *parent=NULL);
	virtual ~X7sBlobTrajGenerator();
	virtual void	AddBlob(CvBlob* pBlob);
	virtual const X7sByteArray* GetData(bool full_data=false);
	virtual bool SetData(const X7sByteArray* data, int frameID);
	virtual void	Process(IplImage* /*pImg*/ = NULL, IplImage* /*pFG*/ = NULL);
	virtual X7sBlobTrajectory* GetBlobTrajByID(int BlobID);
	virtual X7sBlobTrajectory* GetBlobTraj(int index);
	virtual int GetBlobTrajNum() { return m_pTrajVec.size(); }	//!< Return the number of blob trajectory.
	virtual bool DelBlobTraj(int index);
	virtual bool DelBlobTrajByID(int BlobID);

	virtual void Draw(IplImage *pImg,int length=-1, int hue_color=-1, int legend=-1,int thickness=1);
	virtual void 	Release() {  delete this; }
	virtual std::string toString(bool full=false);

protected:
	virtual bool HideSimilarTraj();
	virtual bool IsActual(int BlobID);
	virtual void DeleteAll();

	std::vector<X7sBlobTrajectory*> m_pTrajVec;
	// CvBlobSeq   m_TrajList;
	bool 		m_SetByData;
	bool 		m_draw;
	int         m_Frame;
	X7sByteArray *m_pData;
};


//========================================================================
//-- Alarm Manager Module (X7sVSAlrManager.cpp)

#define X7S_VS_ALRMAN_DATA_SIZE   8192 	//!< Maximum number of byte use to receive the image (8 kB).

//-- Alarms with VideoSurveillance
/**
* @brief Type for the different video surveillance alarm detector.
* @anchor X7S_VS_ALR_TYPE_xxx
*/
enum {
	X7S_VS_ALR_TYPE_GLOB=0,		//!< Alert when a blob enter/exit/hide in the video.
			X7S_VS_ALR_TYPE_LINE,  			//!< Alert when a blob cross a line.
			X7S_VS_ALR_TYPE_POLY,  			//!< Alert when a blob enter/exit/stay in a polygon.
			X7S_VS_ALR_TYPE_WRGDIR,		//!< Alert when a trajectory is not in the correct direction
			X7S_VS_ALR_TYPE_PROWL, 		//!< Alert when a trajectory is prowling around something.
			X7S_VS_ALR_TYPE_AND,   //!< X7S_VS_ALR_TYPE_AND
			X7S_VS_ALR_TYPE_END    //!< X7S_VS_ALR_TYPE_END
};



/**
* @brief Represent an event generated by an X7sVSAlrDetector.
*/
struct X7S_EXPORT X7sVSAlarmEvt {
	int blob_ID;		//!< ID of the blob
	uint8_t alarm_ID; 	//!< ID of the alarm
	uint8_t warn;		//!< @true when we want to display a warning
	int8_t 	val;		//!< A specific value
	uint8_t reserved;	//!< Reserved value (should not be used).
};


/**
* @brief The Alarm Event value.
* @anchor X7S_VS_ALR_EVT_xxx
* @see X7sVSAlarmEvt::val.
*/
enum
{
	X7S_VS_ALR_EVT_NONE= 0,			//!< No event occurs.
			X7S_VS_ALR_EVT_IN=-1,		//!< A blob enter an area, zone.
			X7S_VS_ALR_EVT_OUT=1,		//!< A blob exit an area, zone.
			X7S_VS_ALR_EVT_HIDE=-2,	//!< A blob dissapears.
			X7S_VS_ALR_EVT_SHOW=2,	//!< A blob appears.
			X7S_VS_ALR_EVT_TOUT=10	//!< A blob generate a timeout.
};


/**
*	@brief The X7s Video Surveillance Alarms Manager control the various alarm detector,
*	and produce event data.
*	@ingroup x7svidsurv
*/
class X7S_EXPORT X7sVSAlrManager: public X7sXmlObject {
private:	//This file contains a pointer on a byteArray and therefore can not be copied implicitly.
	X7sVSAlrManager(const X7sVSAlrManager& copy);				//!< declared private, but not defined, to prevent copying
	X7sVSAlrManager& operator=(const X7sVSAlrManager& copy);	//!< declared private, but not defined, to prevent copying

public:
	X7sVSAlrManager(X7sXmlObject *parent,const TiXmlNode *cnode=NULL);
	X7sVSAlrManager(X7sXmlObject *parent,X7sByteArray* alrDef);
	virtual ~X7sVSAlrManager();
	virtual void Draw(IplImage *pImg);
	virtual bool Process(int frame_id, X7sBlobTrajectory *pTraj);
	virtual const X7sByteArray* GetData(bool full_data=false);
	virtual const X7sByteArray* GetDefinition();
	virtual bool SetData(const X7sByteArray* data, int frameID);
	bool AddAlrDector(X7sVSAlrDetector *pAlrDtr);
	X7sVSAlrDetector* Get(int index) const { return m_vecAlarm[index]; }
	X7sVSAlrDetector* GetByID(int ID) const;
	X7sVSAlrDetector* GetLast() const;
	const vector<X7sVSAlarmEvt>& GetAlarmEvents() const { return m_vecAlrEvt; };
	void Delete(int ID);
	int Size(int alarmType=-1) const;
	std::string toString(bool recursive=false) const;

	bool HasEvent() const { return m_vecAlrEvt.size() >0; }
	bool HasAlarm() const { return m_hasAlarm; }

protected:
	void Init();
	void DeleteAll();


private:
	bool CreateChildFromXML(const TiXmlElement *child);
	vector<X7sVSAlrDetector *> m_vecAlarm;	//!< Vector on each Alarm Detector
	vector<X7sVSAlarmEvt> m_vecAlrEvt;			//!< Vector on the Alarm Event for each frame.
	int numTypeAlarm[X7S_VS_ALR_TYPE_END];		//!< Contains for each type of alarm detector the number of this type.
	int m_lastID;	//!< Last ID of the alarmDetector
	X7sByteArray* m_pData, *m_pDefData;
	int m_frameID;
	bool m_hasAlarm;
	bool m_draw;
};





/**
*	@brief The generic (abstract) class for any alarm detector.
*	@ingroup x7svidsurv
*/
class X7S_EXPORT X7sVSAlrDetector: public X7sXmlObject {
public:
	X7sVSAlrDetector(X7sVSAlrManager *parent, uint8_t type);
	virtual ~X7sVSAlrDetector();
	virtual bool Process(int frame_id, const X7sBlobTrajectory *pTraj)=0;
	virtual bool Process(int frame_id, const X7sVSAlarmEvt& evt)=0;
	virtual bool ReadFromXML(const TiXmlNode *cnode);
	virtual void Draw(IplImage *pImg,int hue=-1, int thickness=1)=0;
	virtual const X7sByteArray* GetData();
	virtual bool SetData(const X7sByteArray *pData);
	virtual const X7sVSAlarmEvt& GetAlrEvt() const { return alrEvt; };	//!< Return the alarm event generated by Process()
	int GetStartFrame() const { return m_start; } 						//!< Return the frame when the event start. @deprecated
	int GetEndFrame();
	static const char* GetTypeName(int type);
	virtual void DelTrajectory(int ID);
	virtual void Reload();

protected:
	void ResetAlrEvt(int blobID);
	X7sVSAlrManager *alrMan;
	int m_frame;
	int m_start;
	X7sByteArray* m_pData;
	X7sVSAlarmEvt alrEvt;
};


X7S_EXPORT X7sVSAlrDetector* X7sCreateVSAlarm(X7sVSAlrManager *parent,const TiXmlNode *cnode);
X7S_EXPORT X7sVSAlrDetector* x7sCreateVSAlarmGlobal(X7sVSAlrManager *parent,int width, int height,float ratio=.7f, const char *name=NULL);
X7S_EXPORT X7sVSAlrDetector *x7sCreateVSAlarmLine(X7sVSAlrManager *parent,int x0, int y0, int x1, int y1, const char *name=NULL);
X7S_EXPORT X7sVSAlrDetector *x7sCreateVSAlarmPoly(X7sVSAlrManager *parent,int n_pts, int *x_pts, int *y_pts, const char *name=NULL);


//========================================================================
//-- Tools for X7SVS (X7sVSModules.cpp)

X7S_EXPORT void X7sXmlParamGetVector(CvScalar *val,const X7sParam* pParam);
X7S_EXPORT void X7sXmlParamGetVector(CvPoint2D32f *val, const X7sParam* pParam);
X7S_EXPORT void X7sXmlParamGetVector(CvPoint *val, const X7sParam* pParam);
X7S_EXPORT int x7sVSModuleReadFromXML(CvVSModule *module, const TiXmlElement *parent);
X7S_EXPORT int x7sVSModuleWriteToXML(const CvVSModule *module,TiXmlElement *parent);
X7S_EXPORT int x7sVSModulesWriteAllToXML(TiXmlElement *parent);
X7S_EXPORT int x7sVSModulePrint(const CvVSModule *module);
X7S_EXPORT int x7sVSLog_SetLevel(int log_level);
X7S_EXPORT int x7sVSLog_Config(int log_level, FILE *f_out=stdout, FILE *f_err=stderr, int no_error=X7S_LOG_NOERROR);


X7S_EXPORT FILE* x7sMakeTmp();
X7S_EXPORT void x7sCloseTmp(FILE **pFile);

#endif /* X7SVIDSURV_H_ */
