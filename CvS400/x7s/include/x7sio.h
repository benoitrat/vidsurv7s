/*
 * x7sio.h
 *
 *  Created on: 11-may-2009
 *      Author: Benoit RAT.
 */

#ifndef _X7SIO_H_
#define _X7SIO_H_

#include <string>
#include <iostream>
#include <cstdio>
#include <cv.h>
#include <highgui.h>	//Save and load images.
#include <limits.h>

#include "x7sdef.h"
#include "x7sxml.h"
#include "x7snet.h"
#include "x7scv.h"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Defines and enum.
//----------------------------------------------------------------------------------------

#define X7S_IO_KEY_ESC 27
#define X7S_IO_KEY_SPACE 32
#define X7S_IO_KEY_LARR 81
#define X7S_IO_KEY_UARR 82
#define X7S_IO_KEY_RARR 83
#define X7S_IO_KEY_DARR 84
#define X7S_IO_KEY_LSHIFT -31
#define X7S_IO_KEY_RSHIFT -30



#define X7S_IO_RET_OK 0			//!< General OK return code for cv7core library
#define X7S_IO_RET_ERR -1		//!< General Error return code for cv7core library


enum {
	X7S_IO_TYPE_SEQ=1,		//!< The frames are given by a sequence pattern (i.e., frame_%04d.jpg).
	X7S_IO_TYPE_LIST,		//!< The frames are given by a list in a text file.
	X7S_IO_TYPE_VID,		//!< The frames are given by a video name.
	X7S_IO_TYPE_CAM,		//!< The frames are given by a camera ID.
	X7S_IO_TYPE_NET,		//!< The frames are given from the network using X7sNet.
	X7S_IO_TYPE_BIN,		//!< The frames are given by a sequence in binary.
};


#define X7S_IO_TAGNAME_READER "io_reader"
#define X7S_IO_TAGNAME_WRITER "io_writer"



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Abtract and generic class.
//----------------------------------------------------------------------------------------


/**
 *	@brief Abstract class for reading frame from various media.
 *
 *	The way to load the parameters is the following for the frame reader
 *	is the following:
 *
 *		- general_conf.xml:
@code
<?xml version="1.0"?>
<opencv_storage>
	<input>
		<fpath_prefix>D:\DATABASE\</fpath_prefix>
	</input>
	...
</opencv_storage>
@endcode
 *
 *		- media_conf.xml:
 @code
 <?xml version="1.0"?>
<opencv_storage>
	<input>
		<type>3</type>  <!-- SEQ=1,FILE=2,VIDEO=3,CAMERA=4 -->
		<fpath>"PETS2001\DS01_Te_cam1.avi"</fpath>
		<f_start>20</f_start>
		<f_end>-1</f_end>
		<width>360</width>
		<height>288</height>
		<nMaxErrors>10</nMaxErrors>
	</input>
	...
</opencv_storage>
@endcode
 *
 *
 *  @note If you don't want to use relative path in media_conf.xml you can
 *  or override the @tt{fpath_prefix} with nothing. Otherwise, you can load only
 *  media_conf.xml.
 *  @note The filepath (full or relative) should be between "" to ensure path that path with
 *  special characters (space,digit,...)
 *	@ingroup x7sio
 */
class X7S_EXPORT X7sFrameReader: public X7sXmlObject {

	friend std::ostream &operator<<( std::ostream &out, X7sFrameReader &freader );


public:
	X7sFrameReader(int type);
	virtual ~X7sFrameReader();
	virtual bool Setup()=0;

	virtual bool GrabFirstFrame();
	virtual bool GrabFrame()=0;
	virtual bool GrabFrameError();
	virtual IplImage* RetrieveFrame()=0;
	virtual IplImage* RetrieveRFrame();
	virtual IplImage* QueryFrame();
	virtual IplImage* QueryRFrame();

	int KeyListener(int millisec);
	float GetFrameRate();

	int	GetFrameId() { return frame_id; };
	const CvSize& GetFrameSize();

	IplImage *GetNoInterest();
	IplImage *GetRNoInterest();

	IplImage *GetFGMask();
	IplImage *GetRFGMask();

	void AddKeyParam(char key,X7sParam * pParam);
	void AddKeyFunction(char key,int (*pFunction)(char));


protected:
	bool CheckInputFrame();
	virtual int KeyEvent(char key);
	virtual void PrintHelp();


	//The global parameters.
	int nErrors, frame_id, f_end;
	int nChannels;
	int m_wait;	//!< The number of milliseconds to wait (can be configure through XML or keyboard)
	bool m_isLooping;

	//Value keep to compute the frame rate
	float m_fps;		//!< The value of frame per seconds saved during one seconds.
	clock_t m_start_ts;	//!< The time that we can reset to compute the fps.
	int m_start_fid;	//!< The frame id when we reset the start timestamp.

	//Parameters linked by XML
	CvSize resize;
	int nMaxErrors;

	//The output frame
	IplImage* im;		//!< The original frame loaded.
	IplImage* imr;		//!< The frame resized.
	IplImage* tmp;		//!< Temporary image for transformation (Same size as resized)
	IplImage* mask;		//!< Mask for the original image.
	IplImage* maskr;	//!< Mask for the resized image.
	IplImage* fgmask;	//!< Foreground image image (only valid with network).
	IplImage* fgmaskr;	//!< Foreground image resized.

	//State of the FrameReader.
	bool is_1stframe;	//!< Flag set to true until the first frame is grabbed.
	bool is_setup;		//!< Flag set to true when the FrameReader has been setup.
	bool is_flipped;	//!< Flag set to true if image need to be flipped (windows)
	bool is_exit;		//!< Flag set to true when the frame reader need to exit.

	void* m_mapParam;		//!< Pointer on an array of function
	int (*ptFunc)(char);
	char charFunc;


private:
	void Init();

};

X7S_EXPORT X7sFrameReader* X7sCreateFrameReader(const TiXmlNode* root,const TiXmlNode* root_gen=NULL);

//================================================================================================

/**
 * @brief Class for reading video from a network connection using
 * the x7sNet library.
 */
class X7S_EXPORT X7sNetReader : public X7sFrameReader {
public:
	X7sNetReader(X7sNetSession *netsession, int cam_id);
	X7sNetReader(const TiXmlNode *node);
	virtual ~X7sNetReader();
	bool Setup();
	bool GrabFrame();
	IplImage* RetrieveFrame();
	X7sNetFrame* RetrieveData() { return metaFrame; }

protected:
	virtual int KeyEvent(char key);
	virtual void PrintHelp();

private:
	void Init();
	X7sNetSession *netsession;	//!< A pointer on the netsession.
	int cam_id;					//!< The corresponding camera ID for this netsession.
	int nConsecErrors;	//!< Number of consecutive error
	bool is_internal;			//!< Tell is the netsession has been created internally
	IplImage *jpeg_buff, *meta_buff, *im_frame;
	X7sNetFrame *metaFrame;
};

//================================================================================================


/**
 *	@brief Abstract class for writing frames in various format.
 *	@ingroup x7sio
 */
class X7S_EXPORT X7sFrameWriter: public X7sXmlObject {

	friend std::ostream &operator<<( std::ostream &out, X7sFrameWriter &fwriter );

public:
	X7sFrameWriter(int type);
	virtual ~X7sFrameWriter();
	virtual bool Setup()=0;						//!< Setup, the variable and the path of everything
	virtual int WriteFrame(IplImage *pImg)=0;	//!< Write a frame in the output.

protected:
	virtual void DrawLogo(IplImage *pImg);
	IplImage *pLogo;
	CvPoint logo_pos;
	bool m_isSetup;
};


X7S_EXPORT X7sFrameWriter* X7sCreateFrameWriter(const char *fname);
X7S_EXPORT X7sFrameWriter* X7sCreateFrameWriter(const TiXmlNode* root,const TiXmlNode* root_gen=NULL);

//================================================================================================


X7S_EXPORT int x7sIoLog_SetLevel(int log_level);
X7S_EXPORT int x7sIoLog_Config(int log_level, FILE *f_out=stdout, FILE *f_err=stderr, int no_error=X7S_LOG_NOERROR);
#endif /* _X7SIO_H_ */
