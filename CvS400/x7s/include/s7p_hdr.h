/**
 *  @file
 *  @brief Contains the class s7p_header.h
 *  @date 09-ene-2009
 *  @author neub
 *  @see @ref page_s7p
 *  @page page_s7p S7P: Smart/Small Sevensols Protocol.
 *
 * The first thing is to understand how a the @ref s7p_hdr_t  is composed.
 *
 * @code
 * ||----------------------------------------------------------------------------------------------------------||
 * || 00 01 02 03 04 05 06 07 || 08 09 10 11 12 13 14 15 || 16 17 18 19 20 21 22 23 || 24 25 26 27 28 29 30 31 ||
 * ||-------------------------||-------------------------||-------------------------||-------------------------||
 * ||     s7p_hdr_t::cmd      ||    s7p_hdr_t::camID     ||                  s7p_hdr_t::fpID                   ||
 * ||-------------------------||-------------------------||-------------------------||-------------------------||
 * ||                                              s7p_hdr_t::val                                              ||
 * ||----------------------------------------------------------------------------------------------------------||
 * @endcode
 *
 *  @br
 * 	Then we are going to explain the following step in the S7P.
 *
 * 		- @ref page_s7p_ping.
 * 		- @ref page_s7p_setup.
 * 		- @ref page_s7p_rtp.
 * 		- @ref page_s7p_dflow.
 * 		- @ref page_s7p_lastmodif.
 *
 *
 * @br
 * @section page_s7p_ping Ping the boards.
 *
 * 	To know if the board is present we just do the following communication:
 * 		-	PC		->	BOARD	: @ref S7P_CMD_BOARD_ASK
 * 		- 	BOARD	->	PC		: @ref S7P_CMD_BOARD_ACK
 *
 *  Once we know that the board is available we need to make the setup.
 *
 * @sepline
 * @section page_s7p_setup Setup the parameters.
 *
 * This part give us some important value about the communication type, the datatype, the number of camera available, ...
 *
 * The PC can send to the board the following commands (PC->BOARD):
 *		- @ref S7P_CMD_PARAM_GET	(To obtain a parameters)
 *		- @ref S7P_CMD_PARAM_APPLY	(To apply temporary a parameters)
 *		- @ref S7P_CMD_PARAM_SET	(To apply a parameters and write it in the flash memory of the board).
 * 		- @ref S7P_CMD_PARAM_RST	(To reset all the parameters with their default value if the flash memory).
 *
 * Then the board can reply with two different commands (BOARD->PC):
 * 		- @ref S7P_CMD_PARAM_ACK	(Return cmd if the previous command has been correctly proceed)
 * 		- @ref S7P_CMD_PARAM_ERR	(Return cmd if an error occurs with the previous packets)
 *
 * @subsection page_s7p_setup_hdr Packets header during the setup.
 *
 *  For all these commands the header of the S7P header has the same meaning.
 *
 * 		- s7p_hdr_t::cmd = S7P_CMD_PARAM_xxx
 * 		- s7p_hdr_t::camID = @ref CAMID_ALL or a specific one.
 * 		- s7p_hdr_t::fpID = The parameter Id: @ref S7P_PRM_xxx
 * 		- s7p_hdr_t::val = The value of the paramater (u32) or the error code (int).
 *
 * Normally the s7p_hdr_t::camID should be CAMID_ALL, however some parameters are specific
 * to one camera (See @ref page_s7p_setup_params), therefore we set the camID to the corresponding one.
 * @note The value of the parameters are always on 32 bits, even if they use only 8 bits.
 *
 *
 * @subsection page_s7p_setup_params The parameters.
 *
 * The parameters are defined using ID in @ref S7P_PRM_xxx. They can be divided in 4 main family.
 * 		- 	Read only parameters of the board (e.i. \ref  S7P_PRM_RO_NOFCAM)
 * 		- 	General parameters of the board (e.i. \ref S7P_PRM_BRD_IP)
 * 		-	Parameters specific to a camera (e.i. \ref S7P_PRM_CAM_ENABLE)
 * 		-	Parameters specific to firewire camera (e.i. \ref S7P_PRM_IIDC_WHITE_BALANCE)
 *
 *
 * For the camera parameters, if one wants to set the same parameters to all the camera available,
 * It should use the camID=CAMID_ALL to set the same parameters on all the camera, otherwise it should use
 * the specific ID.
 *
 * @note Not all the parameters are available on a specific boards.
 * For example on the S400, if one uses the Firewire parameters, it will always receive
 * an error message that the parameters is not found.
 *
 *
 *
 * @subsection page_s7p_setup_error Error during Setup the parameters.
 *
 * During the setup some errors can occur: for example, we want to get a parameters
 * that doesn't exist for this board or we want to write a parameters that is only used
 * for reading its value.
 *
 * Therefore the S7P can return some packets with the
 * cmd=@ref S7P_CMD_PARAM_ERR and the associated value equal to:
 *
 * 			-	@ref S7P_ERR_PRM_NOTDEF.
 * 			-	@ref S7P_ERR_PRM_READONLY.
 * 			-	@ref S7P_ERR_PRM_OUTRANGE
 * 			-	@ref S7P_ERR_CAMID_NOTDEF
 *  		-	@ref S7P_ERR_CMD_NOTDEF
 * 			-	@ref S7P_ERR_UNKOWN.
 *
 *
 * @sepline
 * @section page_s7p_rtp  Start/Stop the RTP data flow.
 *
 * Once the configuration has been done we can let the board start sending us using
 * TP protocol. RTP need a SSRC and a ip port, therefore we use the start command to set them.
 * To start the flow the header of the S7P packet is therefore:
 *
 * 		- s7p_hdr_t::cmd = @ref S7P_CMD_SEND_START_RTP
 * 		- s7p_hdr_t::camID = @ref CAMID_ALL.
 * 		- s7p_hdr_t::val = SSRC.
 * 		- s7p_hdr_t::fpID = IP Port (of the PC).
 *
 * To stop the RTP when can use the classic command @ref S7P_CMD_SEND_STOP
 *
 * @see @ref S7P_CMD_SEND_START_RTP, @ref S7P_CMD_SEND_STOP.
 *
 *
 *

 *
 * @sepline
 * @section page_s7p_dflow  Start/Stop the S7P data flow.
 *
 * In the case we don't want to use RTP we can use the Simple Sevensols
 * protocol to start/stop a dataflow. We first need to send the following commands
 * to the Board:
 *
 *		-	Start the flow 	: 	@ref S7P_CMD_SEND_START
 *		- 	Stop the flow	:	@ref S7P_CMD_SEND_STOP
 *
 *
 * @subsection page_s7p_dflow_frame Reception of the frame.
 *
 * @subsubsection page_s7p_dflow_frame_setup Setup (BOARD	->	PC)
 *
 * During the setup phase the s7p_hdr_t contains the following values:
 *
 * 			-	s7p_hdr_t::cmd:		@ref S7P_CMD_FRAME_SETUP.
 * 			-	s7p_hdr_t::camID:	Give the corresponding camera ID for this frame.
 * 			-	s7p_hdr_t::fpID:	Send the actual frame ID of the camera.
 * 			-	s7p_hdr_t::val:		Give the image resolution or the memory size of the frame.
 *
 * We first need to ask to the board the datatype that it use to organize its memory.
 * 		- If the number of layer  is upper than 0 (S7P_DTYPE_xxx > 255), it means that we
 * have an image. Therefore the s7p_hdr_t::val gives the images resolution
 * (width << 16 | height & 0xFFFF).
 *		- Otherwise we have only a memory block that can't be represented as an image, so the
 * s7p_hdr_t::val represent the size of this memory that is going to be written in a binary file.
 *
 *	Once the memory as been correctly reserved or checked to be sufficient,
 *	we copy the payload data at the position 0 of the reserved memory, and the frame id
 *	for all the following packets.
 *
 * @subsubsection page_s7p_dflow_frame_block Block (BOARD -> PC)
 * @code if(s7p_hdr.cmd == S7P_CMD_FRAME_BLOCK) @endcode
 *
 * At this step, we check if the packet sequence correspond correctly to the frame id.
 * Then if it is okay we copy the payload data in our frame buffer with the memory
 * offset given by s7p_hdr_t::val.
 *
 * @subsubsection page_s7p_dflow_frame_last Last (BOARD -> PC)
 * @code if(s7p_hdr.cmd == S7P_CMD_FRAME_LAST) @endcode
 *
 *	We first do the operation of @ref page_s7p_dflow_frame_block. Then we check if the
 *	number of packet is okay.
 *		-	If @TRUE we send a packet (PC -> BOARD) with cmd=@ref S7P_CMD_FRAME_ACKS and the correct camID and fpID.
 *
 *	Once we have done this we save the image in a file or we display it depending of the value
 *	of the processor.
 *
 *
 *
 */

#ifndef S7P_HEADER_H_
#define S7P_HEADER_H_

//////////////////// ============  LAST MODIFICATION ============ ////////////////////
/**
 * @file
 * @page page_s7p
 * @sepline
 * @section page_s7p_lastmodif Last Modification.
 * @note The last modifications are:
 *
 *		-	27/01/10: Add @ref S7P_ERR_CAM_NOTSELECTED
 *		-	16/11/09: Add @ref S7P_PRM_CAM_SWITCH_LED	and modify the order of other parameters.
 *		-	26/05/09:	Add @ref S7P_PRM_CAM_FG_REFRESH and modify the order of other parameters.
 *		- 	21/04/09:	Add @ref S7P_PRM_CAM_TMP1, @ref S7P_PRM_CAM_TMP2, @ref S7P_PRM_CAM_TMP2
 *		-	15/04/09:	Add @ref S7P_PRM_CAM_DTYPE, @ref S7P_PRM_CAM_MTYPE.
 *		-	28/03/09: 	Add @ref S7P_ERR_PRM_NOTDEF, @ref S7P_ERR_CAMID_NOTDEF and @ref S7P_ERR_CMD_NOTDEF.
 * 		-	27/03/09:	Add @ref S7P_PRM_BRD_SSRCID and @ref S7P_PRM_CAM_BG_INSERT.
 * 		- 	02/03/09:	Add @ref S7P_DTYPE_BWBIN.
 *
 */
//////////////////// ============================================ ////////////////////

/**
 * @brief The enumerator for camera ID.
 * @anchor CAMID_xxx
 */
enum {
	CAMID_1 =0,			//!< First Camera
	CAMID_2,			//!< Second Camera
	CAMID_3,			//!< Third Camera
	CAMID_4,			//!< Fourth Camera
	CAMID_NONE=0xF0,	//!< No camera
	CAMID_ALL = 0xFF,	//!< All Camera
};

/**
 * @brief Type of error in the S7P protocol.
 * @anchor S7P_ERR_xxx
 */
enum {
	S7P_ERR_UNKOWN=-1,				//!< Error code: Unkown error.
	S7P_ERR_PRM_NOTDEF=-2,			//!< Error code: This parameters ID does not correspond to any define parameters on this board.
	S7P_ERR_PRM_READONLY=-3,		//!< Error code: The parameters can't be written or applied.
	S7P_ERR_PRM_OUTRANGE=-4,		//!< Error code: The parameters is out of its range.
	S7P_ERR_CAMID_NOTDEF=-5,		//!< Error code: The camID is not in the range or is Unknown.
	S7P_ERR_CMD_NOTDEF=-6,			//!< Error code: The cmd is not define.
	S7P_ERR_CAM_NOTSELECTED=-7,		//!< Error code: There aren't any camera selected for start.
};


/**
 * @brief The different type of command we receive.
 * @anchor S7P_CMD_xxx
 */
enum {
	S7P_CMD_UNKOWN=0x00,
	S7P_CMD_DEBUG=0xFF,

	/* board  0x1? */
	S7P_CMD_BOARD_ASK=0x10,		//!< Ping the board and ask if it is available.
	S7P_CMD_BOARD_ACK=0x11,		//!< The board reply that it is available (connected).

	/* parameters 0x2? */
	S7P_CMD_PARAM_RST=0x20, 	//!< Ask to reset to the default parameter @anchor S7P_CMD_PARAM_xxx.
	S7P_CMD_PARAM_APPLY,		//!< Ask to apply a parameter
	S7P_CMD_PARAM_SET,			//!< Ask to write the parameter
	S7P_CMD_PARAM_GET,			//!< Ask to get the parameter
	S7P_CMD_PARAM_ACK,			//!< Announce the acknowledge of the parameter(s) set.
	S7P_CMD_PARAM_ERR,			//!< Announce an error while getting or setting the parameters.

	/* network 0x8? */
	S7P_CMD_SEND_SLOW=0x80,		//!<
	S7P_CMD_SEND_FAST,
	S7P_CMD_SEND_STOP,			//!< Ask the board to stop sending on the next round.
	S7P_CMD_SEND_START,			//!< Ask the board to send a frame.
	S7P_CMD_SEND_START_RTP,		//!< Ask the board to send frames using RTP.
	S7P_CMD_SEND_ACK,			//!< Acknowledge that the send command has been received.

	/* frames 0xA? */
	S7P_CMD_FRAME_SETUP=0xA0,	//!< Announce the beginning of a frame and setup the buffer.
	S7P_CMD_FRAME_BLOCK,		//!< Announce that a frame block is sent.
	S7P_CMD_FRAME_LAST,			//!< Announce that is the last block sent
	S7P_CMD_FRAME_ACKS,			//!< Acknowledge the good reception of the complete frame.

	/* Metadata 0xB? */
	S7P_CMD_MDATA_FIRST=0xB0,		//!< Announce that the first block of metadata is sent.
	S7P_CMD_MDATA_BLOCK,			//!< Announce that a block of metadata is sent.
	S7P_CMD_MDATA_LAST,				//!< Announce that the last block of metadata is sent.
	S7P_CMD_MDATA_ACK,				//!< Acknowledge the goog reception of the metadats
};

/**
 * @brief The different type of parameters that we can set or get.
 * @anchor S7P_PRM_xxx
 */
enum {

	/* Read-Only parameters */
	S7P_PRM_RO_VFW=0x8000,	//!< Version of the firmware. 	(Read only)
	S7P_PRM_RO_VS7P,		//!< Version of the S7P Protocol.	(Read only)
	S7P_PRM_RO_NOFCAM,		//!< The Number of camera (Read only)
	S7P_PRM_RO_MAC_PREFIX,	//!< Prefix of the mac address (Xilinx - Seven Solution).
	S7P_PRM_RO_MAC_ID,		//!< MAC ID of the board (should be on 16 bits).
	S7P_END_PRM_RO,	//!< Parameters not used (Only to set the limit) @br

	/* BoaRD Parameters */
	S7P_PRM_BRD_IP=0x8010,	//!< IP address.
	S7P_PRM_BRD_PORT_S7P,	//!< Port used for S7P.
	S7P_PRM_BRD_DHCP_ON,	//!< Set if we want the IP to be automatically set.
	S7P_PRM_BRD_MODES,		//!< Modes (RTP, SEQ, FRATE)
	S7P_PRM_BRD_DTYPE,		//!< @deprecated Represent the data type.
	S7P_PRM_BRD_WSIZE,		//!< The windows size of ACK	(Only with mode=SEQ)
	S7P_PRM_BRD_FRAMERATE,	//!< Sending frame rate (Only with mode={RTP,FRATE}).
	S7P_PRM_BRD_PORT_RTP,	//!< Port used by RTP to send to the Server.
	S7P_PRM_BRD_SSRCID,		//!< Source ID of the board (for RTP).
	S7P_END_PRM_BRD,		//!< Parameters not used (Only to set the limit) @br

	/*	-------------------------------------------------------------	*
	 *     Be careful to separate correctly camera parameters			*
	 *    from the others because camera parameters used the camID.		*
	 *	-------------------------------------------------------------	*/

	/* Camera Parameters */
	S7P_PRM_CAM_ENABLE=0x8020,	//!< Tell if camera is enable or not (1 bit).
	S7P_PRM_CAM_RESO,			//!< Resolution of image (IM_WIDTH|IM_HEIGHT)
	S7P_PRM_CAM_DTYPE,			//!< Type of data for this camera (See @ref S7P_DTYPE_xxx) (DEPTHCODE | NOF_CHANNEL | COLSPACE )
	S7P_PRM_CAM_MTYPE,			//!< Type of metadata for this camera (See @ref S7P_MTYPE_xxx)
	S7P_PRM_CAM_QUALITY,		//!< Quality of image.
	S7P_PRM_CAM_SWITCH_LED,	//!< Switch the led that correspond to the camera to on/off.
	S7P_PRM_CAM_BG_THRS,		//!< Threshold for background (8 bits)
	S7P_PRM_CAM_BG_INSERT,		//!< Insertion in background in number of frame (in [1,8]).
	S7P_PRM_CAM_BG_REFRESH,		//!< Refresh background in number of frame (8 bits)
	S7P_PRM_CAM_FG_REFRESH,		//!< Refresh background in number of frame (8 bits)
	S7P_PRM_CAM_CC_MINAREA,		//!< Minimum area of a connected component.
	S7P_PRM_CAM_TMP1,			//!< A temporary value for debug.
	S7P_PRM_CAM_TMP2,			//!< A temporary value for debug.
	S7P_PRM_CAM_TMP3,			//!< A temporary value for debug.
	S7P_END_PRM_CAM,			//!< Parameters not used (Only to set the limit) @br


	/* Firewire IEEE1934 IIDC camera standard parameters */
	S7P_PRM_IIDC_BRIGHTNESS=0x8030,	//!< Brightness of the image
	S7P_PRM_IIDC_AUTO_EXPOSURE,		//!< Exposure of the camara
	S7P_PRM_IIDC_SHARPNESS,			//!< Sharpeness of the image
	S7P_PRM_IIDC_WHITE_BALANCE,		//!< White balance (should be set in manual)
	S7P_PRM_IIDC_HUE,				//!< Hue of the image
	S7P_PRM_IIDC_SATURATION,		//!< Saturation of the image
	S7P_PRM_IIDC_GAMMA,				//!< Gamma correction of the image
	S7P_PRM_IIDC_SHUTTER,			//!< Shutter speed of the image
	S7P_PRM_IIDC_GAIN,				//!< Gain of the image.
	S7P_PRM_IIDC_IRIS,				//!< Iris ???
	S7P_PRM_IIDC_FOCUS,				//!< Change the focus digitally.
	S7P_PRM_IIDC_TEMPERATURE,		//!< Temperature (white balance)
	S7P_PRM_IIDC_TRIGGER,	//Param 12 @br

	S7P_PRM_IIDC_ZOOM=0x8050,		//!< Zoom of the moving camera.
	S7P_PRM_IIDC_PAN,				//!< Pan of the moving camera.
	S7P_PRM_IIDC_TILT,				//!< TILT of the moving camera.
	S7P_PRM_IIDC_FILTER,			//!< FILTER
	S7P_END_PRM_IIDC,				//!< Parameters not used (Only to set the limit) @br

	S7P_PRM_UNKNOWN=0x8060,	//!< The ID that correspond to the unkown ID.@br
};

/**
 * @brief The different type of data we can send.
 *
 * This data are encoded by Number of channel and color space.
 *
 * @anchor S7P_DTYPE_xxx
 */
enum {
	/* 0 channel data */
	S7P_DTYPE_BIN= (0 << 8),	//!< We send binary information with just a count (*.bin).
	S7P_DTYPE_RTP,				//!< We have RTP data (We need to find the payload to open it).
	S7P_DTYPE_JPEG,				//!< Same mode as bin image except that the file is written in (*.jpg).
	S7P_DTYPE_STREAM,			//!< We don't know the total size of data, only when its the last one. (DON'T USE IT)

	/* 1 channel images */
	S7P_DTYPE_RAW_RG=(1 << 8),	//!< Bayer RG with 8 bits. (See @cv{CvtColor}).
	S7P_DTYPE_RAW_GR,			//!< Bayer RG with 8 bits. (See @cv{CvtColor}).
	S7P_DTYPE_RAW_BG,			//!< Bayer RG with 8 bits. (See @cv{CvtColor}).
	S7P_DTYPE_RAW_GB,			//!< Bayer BG with 8 bits. (See @cv{CvtColor}).
	S7P_DTYPE_GREY,				//!< The pixel are send as grey @br
	S7P_DTYPE_HRL,				//!< The pixel are send in Hue Run-Length.
	S7P_DTYPE_BWBIN,			//!< The pixel are send as Black-White Binary 0 or 1.

	/* 2 channel images */
	S7P_DTYPE_YUV422=(2 << 8),	//!< The pixel are send as YUV422 (2 channels) @br

	/* 3 channels images */
	S7P_DTYPE_RGB=(3 << 8),		//!< The pixel are send as interleaved RGB.
	S7P_DTYPE_BGR,				//!> The pixel are send as interleaved BGR.
	S7P_DTYPE_YCrCb,			//!< The pixel are send as YCrCb (See @cv{CvtColor}).
	S7P_DTYPE_YCbCr,			//!< The pixel are send as YCrCb (See @cv{CvtColor}).
	S7P_DTYPE_HSV,				//!< Pixel are in HSV color.

	/* 4 channels images */
	S7P_DTYPE_RGBA=(4 << 8),	//!< The pixel are send as interleaved RGBA.
};


/**
 * @brief The different type of metadata we can send.
 * @anchor S7P_MTYPE_xxx
 */
enum {
	S7P_MTYPE_NONE=0,				//!< No Metadata are found.
	S7P_MTYPE_CCL,				//!< Metadata are encoded as  Connected Component List
	S7P_MTYPE_RLE,				//!< Metadata are encoded by Run-Length Encoding algorithm. (ID, S|E|R)
};


/**
 * @brief The header of the S7P packet for network (Big endian).
 * @note the fpID and the val can change according to the cmd.
 */
typedef struct {
	unsigned int cmd:8;		//!< The command we need to execute. @see @ref S7P_CMD_xxx
	unsigned int camID:8;	//!< Camera ID.	@see @ref CAMID_xxx.
	unsigned int fpID:16;	//!< frame or parameter ID (@see @ref S7P_PRM_xxx, @ref page_s7p_setup,  @ref page_s7p_dflow_frame).
	unsigned int val:32;	//!< The value of the parameters or the offset.
} s7p_hdr_be_t;

/**
 * @brief The header of the S7P packet for little endian computer (x86).
 * @note The fpID, camID, cmd are swapped, because we re-swap them during the endiannes
 * conversion (ntoh/hton). This is maybe not the most efficient way to operate but it is
 * an easy one.
 * @warning This header should be use only on little endian processor (x86) that swap
 * each 32-bits of the header before sending/receiving them!
 * @see s7p_hdr_be_t.
 */
typedef struct {
	unsigned int fpID:16;	//!< frame or parameter ID (@see @ref S7P_PRM_xxx, @ref page_s7p_setup,  @ref page_s7p_dflow_frame).
	unsigned int camID:8;	//!< Camera ID.	@see @ref CAMID_xxx.
	unsigned int cmd:8;		//!< The command we need to execute. @see @ref S7P_CMD_xxx
	unsigned int val:32;	//!< The value of the parameters or the offset.
} s7p_hdr_le_t;


#endif /* S7P_HEADER_H_ */
