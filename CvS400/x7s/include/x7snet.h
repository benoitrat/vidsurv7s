/**
 *  @file
 *  @brief External interface (API) for x7snet library.
 *  @date 21-abr-2009
 *  @author Benoit RAT
 *  @ingroup x7snet
 */


#ifndef _X7S_NET_H_
#define _X7S_NET_H_

#include <ctime>
#include <cstdio>
#include "x7sdef.h"
#include "s7p_hdr.h"

#define X7S_NET_GETIP(a,b,c,d) \
	(((a & 0xFF) << 24) + ((b & 0xFF) << 16) + ((c & 0xFF) << 8) + ((d & 0xFF) << 0))

#define X7S_NET_IP_STRLENGTH 16						//!< Minimum number of char need to write an IP in a string.
#define X7S_NET_SESSION_CTRL_TOUT_MS 3000	//!< Timeout in milliseconds of a control packet (maximum RTT).
#define X7S_NET_SESSION_DRCVR_TOUT 500	//!< RTP have a timeout of 500 ms before saying that it doesn't receive any frame.
#define X7S_NET_SESSION_PING_TOUT_S	30		//!< Check ping each 30s (otherwise board is going to reset)
#define X7S_NET_PING_NOFTRIES 		3		//!< Number of tries to ping the board until establishing a connection.
#define X7S_NET_RTP_MSIZEIB_JPG 262144		//!< Maximum Size In Byte for RTP/JPG Data image (Also define in x7scv for MJPEG).
#define X7S_NET_RTP_MSIZEIB_META 65536		//!< Maximum Size In Byte for RTP Metadata image (Also define in x7scv for MJPEG).
#define X7S_NET_NTYPES_ERROR 10

#ifndef NULL
#define NULL 0
#endif

/*
 * This class are not defined externally because it is not in the interface of the user.
 */
class X7sDataRcvr;		//Forward declaration: this object is included in the .cpp file.
class S7PController;		//Forward declaration: this object is included in the .cpp file.
class X7sNetBoard;		//Forward declaration: this object is included in the .cpp file.
class X7sNetErrors;		//Forward declaration: this object is included in the .cpp file.

/**
 * @brief The type returned by the function of Xs7Net library.
 * @ingroup x7snet
 * @anchor X7S_NET_RET_xxx
 */

enum {
        X7S_NET_RET_OK=X7S_RET_OK,     //!< Return code when a function has been correctly processed
        X7S_NET_RET_UNKOWN=-1,          //!< An error occurs during this function.
        X7S_NET_RET_SOCKETCLOSED=-2,    //!< The socket is not open.
        X7S_NET_RET_TIMEOUT=-3,         //!< Timeout for network command.
        X7S_NET_RET_NOTFOUND=-4,        //!< Parameters or command not found.
        X7S_NET_RET_READONLY=-5,        //!< Can not write on this values.
        X7S_NET_RET_OUTRANGE=-6,        //!< Value is out of its possible range.
		X7S_NET_RET_LOSTPKT=-7,			//!< Some packet are lost.
        X7S_NET_RET_ONSENDING=-8,		//!< Error during sending of packet.
        X7S_NET_RET_BADMDATA=-9			//!< Bad metadata type.
};




/**
 * @brief A buffer of X7sNet library.
 *
 * It contains the image data and its corresponding metadata.
 * @ingroup x7snet
 */
struct X7S_EXPORT X7sNetFrame {
	uint32_t ID;			//!< ID of the frame (for RTP it is equal to TS).
	int counter;			//!< Increment each time a new frame is received correctly
	int camID;				//!< ID of the camera for this frame

	int width;				//!< Width (if it's an image).
	int height;				//!< Height (if it's an image).
	int nChannels;			//!< Number of channel (if it's an image).

	int dtype;				//!< Type of the data. See @ref S7P_DTYPE_xxx
	X7sByteArray *data;		//!< Pointer on a buffer of data.

	int mdtype;				//!< Type of metadata. See @ref S7P_MTYPE_xxx
	X7sByteArray *mdata;	//!< Pointer on the memory of the data.
};
X7S_EXPORT void x7sReleaseNetFrame(X7sNetFrame** pNetFrame);
X7S_EXPORT X7sNetFrame* x7sCreateNetFrame(int ID, int dtype, size_t dsize, int mdtype, size_t mdsize);


/**
 *	@brief Create a Network session with a board.
 *
 *	This class can be configured in several ways:
 *		- The control of the session is done using the
 *			- S7P protocol
 *		- The reception of data can be performed with
 *			- S7P
 *			- RTP/JPEG
 *		- These protocols can be implemented at different layers
 *			- UDP layer
 *			- MAC layer
 *	@ingroup x7snet
 *	@note It is therefore important to use carefully the X7sNetSession::Connect() function in order
 * to configure properly your session.
 */
class X7S_EXPORT X7sNetSession {
public:

	X7sNetSession();
	virtual ~X7sNetSession();

	int Connect(uint32_t ip_remote, uint16_t port_remote,uint16_t& port_local);
	int Connect(const char *eha_remote, const char* eha_local);

	int Start();
	int Stop();
	int Close();
	int SetParam(int ID, const uint32_t& val, uint8_t cam_id=CAMID_ALL);
	int GetParam(int ID, uint32_t& val, uint8_t cam_id=CAMID_ALL);
	X7sNetFrame* CaptureMetaFrame(int camID);
	int SendMask(uint8_t *buffer, int nbytes);
	bool IsAlive(int nof_tries=X7S_NET_PING_NOFTRIES);
	bool IsNewFrame(int camID);
	static const char* GetErrorMsg(int code);
	int GetNumErrors(int type=0, bool reset=false);
	int SetReceiverTimeOut(int nof_milli=X7S_NET_SESSION_DRCVR_TOUT);

protected:
	S7PController *ctrlr;	//!< Controler using S7P protocol.
	X7sDataRcvr *drcvr;		//!< DataReceiver of X7SNET.
	X7sNetBoard *board;		//!< Keep parameters of the board
	X7sNetErrors* errors;	//!< Specific class to to keep the errors.
	int SetupRTP();
	int SetupS7P();

private:
	int session_type;		//!< The type of session used according to the Connect().
	bool is_socketok;		//!< Tell if the socket are OK
	bool is_setup;			//!< Tell that the board is setup.
	bool is_alive;			//!< If the board is connected or not.
	time_t t_alive;			//!< The last time we check is connected.
};

//X7sNetTools.cpp
X7S_EXPORT int x7sNetLog_SetLevel(int log_level=X7S_LOGLEVEL_MED);
X7S_EXPORT int x7sNetLog_Config(int log_level, FILE *f_out=stdout, FILE *f_err=stderr, int no_error=X7S_LOG_NOERROR);

//These functions can be find in X7sSocketUDP.cpp
X7S_EXPORT uint32_t X7sNet_GetIP(const char *ip_str);
X7S_EXPORT void X7sNet_SetIP(uint32_t ip, char *ip_str);
X7S_EXPORT void X7sNet_SetIP(uint32_t ip, uint8_t* ip_tab);

X7S_EXPORT void x7sPrintU8Array(const char *format,const uint8_t *buff, int length,
		const char* in_fmt="%X ", int mod_endl=0,FILE *stream=stdout);

#endif /* X7S_NET_H_ */
