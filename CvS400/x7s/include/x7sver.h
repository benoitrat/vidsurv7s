/**
*  @file
*  @brief definition of the current version of X7S LibraryOpenCV
*  @date 04-may-2009
*  @author Benoit RAT
*	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
*/
/*

  Usefull to test in user programs
*/

#ifndef _X7SVERSION_H_
#define _X7SVERSION_H_

#define X7S_MAJOR_VERSION    0
#define X7S_MINOR_VERSION    9
#define X7S_PATCH_VERSION    2

#define X7S_STR_EXP(__A)  #__A
#define X7S_STR(__A)      X7S_STR_EXP(__A)
#define X7S_VERSION       X7S_STR(X7S_MAJOR_VERSION) "." X7S_STR(X7S_MINOR_VERSION) "." X7S_STR(X7S_PATCH_VERSION)

#endif /*_X7SVERSION_H_*/
