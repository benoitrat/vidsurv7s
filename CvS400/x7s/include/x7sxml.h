/**
 *  @file
 *  @brief External interface (API) for x7sxml library.
 *  @date 07-may-2009
 *  @author Benoit RAT
 *  @ingroup x7sxml
 */

#ifndef _X7SXML_HPP_
#define _X7SXML_HPP_

#ifndef TIXML_USE_STL
#define TIXML_USE_STL
#endif
#define X7S_XML_ANYID (int)-1			//!< Default ID of an X7sParam
#define X7S_XML_ANYTYPE (int)-1			//!< Default Type used for X7sXmlParamList
#define X7S_XML_NULLID 0x48151623	//!< ID of the NULL X7sParam (Have you seen LOST ?)

#include "x7sdef.h"
#include <tinyxml.h>

#include <climits>
#include <cfloat>
#include <cstdarg> 	//!< Use va_list(),..
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <map>
#include <vector>

#if !(defined(INT32_MIN)) ||  !(defined(INT32_MAX))
	#define INT32_MAX 2147483647
	#define INT32_MIN (-INT_MAX-1)
#endif

#if !(defined(UINT8_MAX))
# 	define UINT8_MAX		(255)
#endif

#if !(defined(UINT16_MAX))
# 	define UINT16_MAX		(65535)
#endif

#if !(defined(UINT32_MAX))
# 	define UINT32_MAX		(4294967295U)
#endif

#if !(defined(FLT_MAX))
#	define FLT_MAX 1E+37
#endif

#if !(defined(DBL_MAX))
#	define DBL_MAX 1E+37
#endif


#ifdef x7sxml_EXPORTS
	#define X7SXML_DLL X7S_EXPORT
#else
	#define X7SXML_DLL X7S_IMPORT
#endif


#define X7S_XML_ISEQNODENAME(node,name) \
	(node && (strcmp(((TiXmlNode*)node)->Value(),name)==0))


/**
 * @brief Just to avoid multi-definition of a typedef.
 */
#ifndef _X7SRANGE_H_
struct X7sRangeGeneric;
#endif


/**
 * @brief Generic class to save parameters.
 *
 * A X7sParam contains a:
 * 		- name @anchor X7sParam_name.
 * 		- ID @anchor X7sParam_ID (which can be set as @ref X7S_XML_ANYID if you don't want to use it).
 *
 */
class X7SXML_DLL X7sParam {
public:
	X7sParam();
	X7sParam(const char* tag,int ID, int val, int type=X7STYPETAG_INT,
		int min=INT_MIN, int max=INT_MAX);
	X7sParam(const char* tag,int ID,  double val, int type=X7STYPETAG_DOUBLE,
		double min=-DBL_MAX, double max=DBL_MAX);
	X7sParam(const char* tag,int ID, const char *str, int type=X7STYPETAG_STRING);
	X7sParam(const X7sParam& copy); // declared private, but not defined, to prevent copying
	X7sParam& operator=(const X7sParam&);  // declared private, but not defined, to prevent copying

	virtual ~X7sParam();

	//Set Method.
	void ResetIsChanged();
	bool setComment(const std::string& str);
	bool SetStep(double step);
	bool SetValue(const char *szStr);
	bool SetValue(const std::string& str);
	bool SetValues(const char *szFormat,...);
	bool SetValues(const char *szFormat,va_list args);
	bool Increment(bool positive=true);
	bool LinkParamValue(uint8_t *pVal);
	bool LinkParamValue(uint32_t *pVal);
	bool LinkParamValue(int *pVal);
	bool LinkParamValue(float *pVal);
	bool LinkParamValue(bool *pVal);
	bool LinkParamValue(double *pVal);
	bool LinkParamValue(std::string *pVal);
	friend X7SXML_DLL std::istream& operator >>(std::istream& stream, X7sParam const*pParam);

	//Get Method
	const std::string& GetName() const;
	const std::string& getComment() const;
	int GetType() const;
	int GetID() const;
	std::string toString() const;
	int toIntValue(bool *ok=NULL) const;
	double toRealValue(bool *ok=NULL) const;
	int GetLength() const;
	bool GetValue(double *val, double *min=NULL, double *max=NULL, double *step=NULL) const;
	bool GetValue(int *val, int *min=NULL, int *max=NULL, int *step=NULL) const;
	bool GetValue(std::string *val) const;
	bool GetRealValue(int pos, double *ptr) const;

	bool IsOk() const;
	bool IsChanged() const;
	bool IsInteger() const;
	bool IsReal() const;
	bool IsString() const;
	bool IsNull() const { return (ID==X7S_XML_NULLID); }

	friend X7SXML_DLL std::ostream& operator <<(std::ostream& stream, const X7sParam* pParam);

	//Debug method
	std::string Print() const;

	static const X7sParam& GetNULL();

private:
	std::string name;			//!< The name of the parameters.
	std::string comment;		//!< Comment of the parameters
	int ID;						//!< The ID of the parameters.
	X7sRangeGeneric *m_range;	//!< A pointer on the generic and abstract class Range
	char m_buff[256];			//!< Buffer to create and keep the parameters value when it is a string
};


/**
 * @brief Class that implement the function of the map list.
 */
class X7SXML_DLL X7sParamList {
private:
	X7sParamList(const X7sParamList& copy); 		// declared private, but not defined, to prevent copying
	X7sParamList& operator=(const X7sParamList&);  // declared private, but not defined, to prevent copying

public:
	X7sParamList();
	virtual ~X7sParamList();

	//Redefine the map function
	X7sParam* Insert(const X7sParam* pParam);
	X7sParam* Insert(const X7sParam& param);

	int Size() const;
	void ResetIsChanged();

	bool Exist(const std::string& name);
	X7sParam* operator[] (const int& ID);
	X7sParam* operator[] (const char *name);
	X7sParam* operator[] (const std::string& name);
	X7sParam* GetNextParam(X7sParam * param);
	X7sParam* last();
	const X7sParam* last() const;

	//For debugging.
	virtual std::string toString();


protected:

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
	std::map<int,X7sParam*> m_mapID;
	std::map<std::string,X7sParam*> m_mapName;
	std::map<std::string,X7sParam*>::iterator m_it;
#pragma warning(pop)
#else
	std::map<int,X7sParam*> m_mapID;
	std::map<std::string,X7sParam*> m_mapName;
	std::map<std::string,X7sParam*>::iterator m_it;
#endif
	X7sParam paramNULL; //!< Correspond to the NULL parameter returned when a search has been unsuccessful (@ref X7sParamNULL).
};

/**
 * @brief Derived class to manipulate a list of parameters from XML file.
 *
 * This class can load value of parameters from an XML file.
 * This class can write parameters to an XML file.
 * This class does not load parameters that have not be inserted previously.
 */
class X7SXML_DLL X7sXmlParamList: public X7sParamList {
public:
	X7sXmlParamList(const std::string& name, int id=X7S_XML_ANYID, int type=X7S_XML_ANYTYPE);
	virtual ~X7sXmlParamList();
	bool ReadFromXML(const TiXmlNode* parent);
	bool WriteToXML(TiXmlNode* parent) const;
	TiXmlNode* GetXmlNode();
	const char* GetName() const;
	int GetID() const { return m_id; };
	bool SetID(int ID);
	int GetType() const { return m_type; };


	//For debugging.
	virtual std::string toString();

protected:
	TiXmlNode* CreateXmlNode();
	bool ReadParamFromXML(const TiXmlElement *el_param);
	bool WriteParamToXML(TiXmlElement *el_param);
	TiXmlNode* CreateParamXmlNode(const X7sParam *pParam) const;
	static bool SetNodeText(TiXmlElement *node,const std::string& str);

	std::string m_name;		//!< The name of the XML list.
	int m_id;				//!< The ID of the XML list (by default its -1).
	int m_type;
};





class X7sXmlObject;

/**
 * @brief Generic interface to use public methods with an internal X7sXmlParamList.
 *
 * The public method let set or get a parameter.
 *
 */
class X7SXML_DLL X7sXmlObject {

public:
	X7sXmlObject(X7sXmlObject *parent, const std::string& name,int type=X7S_XML_ANYTYPE,int id=X7S_XML_ANYID);
	X7sXmlObject(X7sXmlObject *parent, const std::string& name,bool createChildren, int type=X7S_XML_ANYTYPE,int id=X7S_XML_ANYID);
	virtual ~X7sXmlObject();
	bool ReadFromXMLObject(const X7sXmlObject *obj);
	virtual bool ReadFromXML(const TiXmlNode *cnode);
	virtual bool WriteToXML(TiXmlNode *cnode, bool clear=true) const;
	virtual bool Apply(int depth=-1);
	virtual bool SetParam(const char *name, const char *value, ...);
	virtual bool SetParam(const char *name, const std::string& value);
	virtual const X7sParam* GetParam(const char* name);
	virtual const X7sParam* GetNextParam(const X7sParam* pParam);
	virtual int GetID() const { return m_params.GetID(); };
	virtual int GetType() const { return m_params.GetType(); };
	virtual const char* GetTagName() const { return m_params.GetName(); };
	virtual const X7sXmlObject* GetParent() const { return m_parent; };
	virtual X7sXmlObject* GetNextChild(X7sXmlObject *child);
	virtual bool IsTagName(const char *name) const;
	virtual bool IsValid() const { return m_isValid; };
	virtual std::string toString();
	void Print(FILE *cfile=stdout,int depth=-1);

	static int ReadTypeFromXML(const TiXmlNode *cnode, bool *ok=NULL);
	static int ReadIdFromXML(const TiXmlNode *cnode, bool *ok=NULL);

protected:
	bool AddAsChild(X7sXmlObject *child);	//!< Normally called during construction.
	virtual bool RemoveChild(X7sXmlObject *child);
	virtual bool SetID(int ID) { return m_params.SetID(ID); };
	bool IsExistChild(const char *name, int ID) const;
	bool IsFirstChild() const { return m_isFirstChild; }; //!< Return @true, if we are actually creating the first child.
	bool IsLastChild()	const { return m_isLastChild; };  //!< Return @true, if we are actually creating the last child.
	X7sXmlParamList m_params;		//!< Handle the list of parameters with XML.

	std::vector<X7sXmlObject*> m_children;
	std::vector<X7sXmlObject*>::iterator m_itChild;

private:
	virtual X7sXmlObject* ChildTypeReadFromXML(const TiXmlElement* childNode, X7sXmlObject *childObj);
	virtual bool CreateChildFromXML(const TiXmlElement *child);
	void Init();

	X7sXmlObject *m_parent;		//!< Pointer on the parent of this XMLNode.
	bool m_isFirstChild;		//!< @true when creating the first child,  @false otherwise.
	bool m_isLastChild;			//!< @true when creating the last child,  @false otherwise.
	bool m_isValid;				//!< @true when the adding as a child has been correctly performed.
	bool m_createChildren;		//!< if @true, the function CreateChildFromXML() is called.


};

X7S_EXPORT int x7sXmlLog_SetLevel(int log_level);
X7S_EXPORT int x7sXmlLog_Config(int log_level, FILE *f_out=stdout, FILE *f_err=stderr, int no_error=X7S_LOG_NOERROR);




#endif /* X7SPARAM_HPP_ */
