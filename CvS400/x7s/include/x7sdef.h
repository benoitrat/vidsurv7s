/**
*  @file
*  @brief Contains the class x7sdef
*  @date 04-may-2009
*  @author Benoit RAT
*	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
*/


#ifndef _X7SDEF_H_
#define _X7SDEF_H_


//------------------------------------------------------------------------------------------------------ Windows/Linux specific.

//Include the POSIX C99 library stdint.h
#ifdef _MSC_VER
#include "msc_stdint.h"
#else
#include <stdint.h>
#endif

#if ( defined(WIN32) || defined(WIN64) )
#ifndef WINDOWS
#define WINDOWS 	//!< General define to say that we are on WINDOWS OS.
#endif
#endif


#if defined(WINDOWS) // Windows
#define X7S_PATH_SEP          '\\'	//!< Char use to separate two folder level (Windows)
#elif defined(__MAC__)	//Mac OSX
#define X7S_PATH_SEP          ':'	//!< Char use to separate two folder level (MAC OSX)
#else   // CYGWIN also uses UNIX settings
#define X7S_PATH_SEP          '/'	//!< Char use to separate two folder level (Unix)
#endif

#include <stdio.h>
#ifdef _MSC_VER
#define snprintf(buff,count,format,...) _snprintf(buff,count,format,##__VA_ARGS__)
#endif


//------------------------------------------------------------------------------------------------------ Libraries export/import.

#ifndef X7S_EXTERN_C
#ifdef __cplusplus
#define X7S_EXTERN_C extern "C"
#define X7S_DEFAULT(val) = val	//!< default value used when C code is used in C++
#else
#define X7S_EXTERN_C
#define X7S_DEFAULT(val)
#endif
#endif

//Set dllexport to compile library under MSVC.
#if (defined WIN32 || defined WIN64 || defined _WIN64)
#define X7S_EXPORT __declspec(dllexport)	//!< Export to a DLL (under windows)
#define X7S_IMPORT __declspec(dllimport)	//!< Import from a DLL (under windows)
#define X7S_CDECL __cdecl
#define X7S_FUNC __stdcall //Faster under windows (but don't work with vargs)
#define X7S_FUNC_VARG __cdecl
#else
#define X7S_EXPORT
#define X7S_IMPORT
#define X7S_CDECL
#define X7S_FUNC __cdecl   //Sous linux
#define X7S_FUNC_VARG __cdecl

#endif

//Must be put before each function in a c library
#ifndef X7SLIBC
#define X7SLIBC X7S_EXTERN_C X7S_EXPORT //!< Must be put before each function in a c library
#endif

#ifdef _MSC_VER
#define X7S_UNUSED
#else 
#ifndef X7S_UNUSED
#define X7S_UNUSED __attribute__ ((unused))
#endif
#endif


//------------------------------------------------------------------------------------------------------ Types and family types.

/**
* @brief The X7STYPETAG is a tag for a given type.
*
* @ref X7STYPETAG_UNKOWN,	@ref X7STYPETAG_U8, @ref X7STYPETAG_U16, @ref X7STYPETAG_U32,
* @ref X7STYPETAG_CHAR, @ref X7STYPETAG_SHORT, @ref X7STYPETAG_INT, @ref X7STYPETAG_LONG
* @ref X7STYPETAG_BOOL, @ref X7STYPETAG_FLOAT, @ref X7STYPETAG_DOUBLE, @ref X7STYPETAG_STRING
* @anchor X7STYPETAG_xxx
*/
#ifndef _X7STYPETAGS_DEFINED
#define X7STYPETAG_UNKOWN	 0 	//!< Represent the unknown type.
#define X7STYPETAG_U8 		 10	//!< Represent the type: unsigned char or uint8_t.
#define X7STYPETAG_U16 		 11 //!< Represent the type: unsigned short or uint16_t.
#define X7STYPETAG_U32 		 12 //!< Represent the type: unsigned int or uint32_t.
#define X7STYPETAG_CHAR 	 13	//!< Represent the type: char or int8_t.
#define X7STYPETAG_SHORT 	 14 //!< Represent the type: short or int16_t.
#define X7STYPETAG_INT 		 15 //!< Represent the type: int or int32_t.
#define X7STYPETAG_LONG 	 16 //!< Represent the type: long.
#define X7STYPETAG_BOOL		 17 //!< Represent the type: bool or uint8_t.
#define X7STYPETAG_FLOAT	 20 //!< Represent the type: float.
#define X7STYPETAG_DOUBLE 	 21 //!< Represent the type: double
#define X7STYPETAG_STRING 	 30 //!< Represent the type: std::string
#define X7STYPETAG_STRVEC	 31	//!< Represent the type: std::string with real number separated by space.

#define X7STYPEFAMILY_UNKOWN 	0	//!< Represent all unknown type.
#define X7STYPEFAMILY_INTEGER 	1	//!< Represent all integer types (types=[1*10,1*10+9])
#define X7STYPEFAMILY_REAL 		2	//!< Represent all real types 	 (types=[2*10,2*10+9])
#define X7STYPEFAMILY_STRING 	3	//!< Represent all string types  (types=[3*10,3*10+9])

#define _X7STYPETAGS_DEFINED	//!< @internal
#endif

//------------------------------------------------------------------------------------------------------ Return type and delete PTR.


#ifndef __cplusplus
#define X7S_BOOL_ISDEFINED
typedef int bool;
#else
//! If a pointer exist, delete a pointer and set-it to NULL
#define X7S_DELETE_PTR(ptr) \
		if(ptr) { delete ptr; ptr=NULL; }
#endif

#ifndef TRUE
#define TRUE 1	//!< Value assigned for @true in C.
#endif

#ifndef FALSE
#define FALSE 0	//!< Value assigned for @false in C
#endif

/**
 * @brief X7S return code
 * @anchor X7S_RET_xxx
 */
#define X7S_RET_ERR -1	//!< General OK return code for all x7s libraries
#define X7S_RET_OK 0	//!< General Error return code for all x7s libraries

#define X7S_NBYTES_KIB 1024
#define X7S_NBYTES_MIB 1048576
#define X7S_NBYTES_GIB 1073741824

/**
 * @brief Compare two C-string, return @ref TRUE if they are equals, @ref FALSE otherwise.
 */
#define X7S_IS_STREQ(szStringA,szStringB) \
	(szStringA && szStringB && (strcmp(szStringA,szStringB)==0))?TRUE:FALSE

//------------------------------------------------------------------------------------------------------ Range (Min/Max)

//! Return @true if the val is between [min_in, max_out[
#define X7S_CHECK_RANGE(val, min, max) \
		(min <= val && val < max)

//! For unsigned variable (>0)
#define X7S_CHECK_URANGE(v, max_out) \
		(v < max_out)

//! Return maximum value between a and b
#define X7S_MAX(a,b) \
		(a>b?a:b)

//! Return minimum value between a and b
#define X7S_MIN(a,b) \
		(a<b?a:b)

//! Return the variable or its value clipped to the range defined by [min,max]
#define X7S_INRANGE(val,min,max) \
		(val<min)?min:((val>max)?max:val)

//!Return a value inside [min,max], when val<min: val=max or when val > max : val=min.
#define X7S_INLOOP(val,min,max) \
	(val<min)?max:((val>max)?min:val)


//------------------------------------------------------------------------------------------------------ Log Level

/**
* @brief The different Level for Log message.
* @anchor X7S_LOGLEVEL_xxx
*/
enum {
	X7S_LOGLEVEL_LOWEST=1,	//!< Always show the Log message (ERROR)
	X7S_LOGLEVEL_LOW,		//!< Low Log Level: Important message which are always import to see for Logging (WARN).
	X7S_LOGLEVEL_MED,		//!< Medium Log Level: This type is the most used (INFO).
	X7S_LOGLEVEL_HIGH,		//!< High Log Level: Message that print a lot of information but do not slow down (DEBUG)
	X7S_LOGLEVEL_HIGHEST,	//!< Higher Log Level: Message that typically slow down the system (loop,threads) (DUMP)
	X7S_LOGLEVEL_END,		//!< Not use log LEVEL
};

#define X7S_LOG_NOERROR X7S_LOGLEVEL_MED
#define X7S_LOGNAMES { "Fatal", "Error", "Warn ", "Info ", "Debug", "DebuG"}
#define X7S_FUNCNAME( Name )  static char funcName[] = Name

//------------------------------------------------------------------------------------------------------ X7sByteArray structure

enum {
	X7S_BYTEARRAY_UNKNOWN=0,	//!< Unkown type for the byteArray.
	X7S_BYTEARRAY_MULTIMIXED,		//!< Multipart/Mixed byteArray inside this ByteArray.
	X7S_BYTEARRAY_MIME,					//!< File that have a MIME type .
	X7S_BYTEARRAY_IPLIMAGE,			//!< IplImage transformed to byteArray (OpenCV).
	X7S_BYTEARRAY_CVMAT,				//!< CvMatrix transformed to byteArray (OpenCV).
	X7S_BYTEARRAY_CVSEQ,				//!< CvSeq transformed to byteArray (OpenCV).
	X7S_BYTEARRAY_S7PDATA,			//!< Data received from the S7P Protocol.
	X7S_BYTEARRAY_QIMAGE,				//!< Data encoded has QImage (Qt library).
	X7S_BYTEARRAY_OTHERS=0xFF,	//!< Other types not registered.
};

#define X7S_BYTEARRAY_MULTIBARRAY_MAX 7

/**
* @brief A simple structure that represent a array of byte in the memory.
*
* This byte array should be initialized with x7sCreateByteArray(),
* and deleted with x7sReleaseByteArray();
*/
typedef struct X7S_EXPORT  {
	int type;				//!< The first 4-bytes determine the type of data (0 is un.
	uint8_t *data;		//!< A pointer to the data stored in this byte array
	int length;			//!< The number of bytes in this byte array.
	int capacity;		//!< The maximum number of bytes that can be stored in the byte array without forcing a reallocation.
} X7sByteArray;



/**
* @brief Structure to mix
*
* This byte array should be initialized with x7sCreateByteArray(),
* and deleted with x7sReleaseByteArray();
*/
typedef struct X7S_EXPORT  {
	int n;
	X7sByteArray byteArrays[X7S_BYTEARRAY_MULTIBARRAY_MAX];
} X7sMultiByteArrayHeader;


#endif /* X7SDEF_H_ */
