#include <winresrc.h>

VS_VERSION_INFO VERSIONINFO
  FILEVERSION @X7S_VERSION_MAJOR@,@X7S_VERSION_MINOR@,@X7S_VERSION_PATCH@,0
  PRODUCTVERSION @X7S_VERSION_MAJOR@,@X7S_VERSION_MINOR@,@X7S_VERSION_PATCH@,0
  FILEFLAGSMASK VS_FFI_FILEFLAGSMASK
#ifdef DEBUG
  FILEFLAGS 0x1L
#else
  FILEFLAGS 0x0L
#endif
  FILEOS VOS_NT_WINDOWS32
  BEGIN
    BLOCK "StringFileInfo"
    BEGIN
      BLOCK "04090000"
      BEGIN
	    VALUE "CompanyName", "Seven Solutions"
		VALUE "Comments", "www.sevensols.com"
        VALUE "FileDescription", "eXtend Seven Solutions Library"
        VALUE "FileVersion", "@X7S_SOVERSION@"
        VALUE "InternalName", "@LIBNAME@"
        VALUE "LegalCopyright", "Seven Solutions - Copyright 2010"
        VALUE "ProductVersion", "@X7S_VERSION@"
      END
    END
    BLOCK "VarFileInfo"
    BEGIN
      VALUE "Translation", 0x409, 1200
    END
  END
