#include "X7sNetTools.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------

#include <cstdarg> 	//!< Use va_list(),...
#include <cstdio> 	//!< Use vprintf(),...
#include <cstdlib>


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Initialisation of static (global) variable.
//---------------------------------------------------------------------------------------

const char* X7sNetLog::names[X7S_LOGLEVEL_END] = X7S_LOGNAMES;
int X7sNetLog::level = X7S_LOGLEVEL_MED;
int X7sNetLog::noError=X7S_LOG_NOERROR;
FILE* X7sNetLog::ferr=stderr;
FILE* X7sNetLog::fout=stdout;


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public interfaces
//----------------------------------------------------------------------------------------



/**
* @brief Public interface to set the logLevel of the X7SCV library.
* @return @ref X7S_RET_OK if the level has been applied, otherwise @ref X7S_RET_ERR.
* @see X7sNetLog::SetLevel().
*/
int x7sNetLog_SetLevel(int log_level) {
	if(X7S_LOGLEVEL_LOWEST <= log_level && log_level < X7S_LOGLEVEL_END) {
		X7sNetLog::SetLevel(log_level);
		return X7S_RET_OK;
	}
	return X7S_RET_ERR;
}

/**
 * @brief Configure the LOG object
 *
 * This is the public interface to configure the LOG object:
 *	its level, the log files, the threshold between error and normal.
 *
 * @param log_level Set the level for the current library.
 * @param f_out Set the output log file (by default it's stdout)
 * @param f_err Set the error log file (by default it's stderr)
 * @param no_error Set the level to switch between error and output log message.
 *
 * @return @ref X7S_RET_OK if the level has been applied, otherwise @ref X7S_RET_ERR.
 */
int x7sNetLog_Config(int log_level, FILE *f_out, FILE *f_err, int no_error)
{
	int ret=x7sNetLog_SetLevel(log_level);
	X7sNetLog::noError=no_error;

	//Check if the file is valid.
	if(f_out==NULL) f_out=stdout;
	if(f_err==NULL) f_err=stderr;

	//Set the new one
	X7sNetLog::fout=f_out;
	X7sNetLog::ferr=f_err;

	return ret;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Static public functions
//----------------------------------------------------------------------------------------

/**
* @brief Set the level of print for this static class.
* @param log_level The level given by @ref X7S_LOGLEVEL_xxx
* @return @true if the operation performs correctly.
*/
bool X7sNetLog::SetLevel(int log_level)
{
	if(X7S_LOGLEVEL_LOWEST <= log_level && log_level < X7S_LOGLEVEL_END) {
		X7sNetLog::level=log_level;
		return true;
	}
	return false;
}

/**
* @brief If the given level is going to be display.
*
* The given level is compared with the level set with SetLevel()
* @note SetLevel(), x7sNetLog_SetLevel()
*/
bool X7sNetLog::IsDisplay(int level)
{
	return (0<= level && level <= X7sNetLog::level);
}

/**
* @brief Print Log message with a specific level
*
* @param funcname The name of the function that we want to be displayed
* @param file	The name of the file that call this function.
* @param line	The line of the file where the call of this function was produced.
* @param level The log level (e.i.@ref X7S_LOGLEVEL_xxx)
* @param szFormat The format of the output.
* @param ... The variable argument list (va_list).
*
*/
void X7sNetLog::PrintLevel(const char *funcname,const char *file, int line, int level, const char* szFormat, ...) {

	if(X7S_LOGLEVEL_LOWEST <= level && level < X7S_LOGLEVEL_END)
	{
		va_list args;
		va_start(args,szFormat);
		FILE *output;
		if(level < noError) output=ferr;
		else output=fout;
		Print(true,names[level],funcname,file,line,output,szFormat,args);

#ifdef DEBUG
		if(level < noError)
		{
			if(output!=stderr) Print(true,names[level],funcname,file,line,(FILE*)stderr,szFormat,args);
		}
		else
		{
			if(output!=stdout) Print(true,names[level],funcname,file,line,(FILE*)stdout,szFormat,args);
		}
#endif
	}
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Static private functions
//----------------------------------------------------------------------------------------

/**
* @brief Print a message with the following options:
*
* @param with_time	Add the time to the print.
* @param prefix	Add a prefix to the print (Debug, Warn, ...)
* @param funcname	Add the funcname to the print (if @NULL nothing is added)
* @param filepath 	Add the filepath at the end of the print (if @NULL nothing is added)
* @param line		Add the line number at the end of the print (if @NULL nothing is added).
* @param stream	In which stream to print the message (stdout, stderr, ...).
* @param szFormat	The format of the message
* @param args		The variable argument list.
*/
void X7sNetLog::Print(bool with_time, const char *prefix, const char *funcname,const char *filepath, int line, FILE *stream, const char *szFormat, va_list args)
{
	char buffer [80];
	const char *filename;
	if(with_time)
	{
		time_t rawtime;
		struct tm * timeinfo;
		time ( &rawtime );
		timeinfo = localtime ( &rawtime );
		strftime(buffer,80,"[%H:%M:%S]",timeinfo);
	}
	if(funcname==NULL) fprintf(stream,"%s %s: ",buffer,prefix);
	else fprintf(stream,"%s %s > %s: ",buffer,prefix,funcname);
	if(szFormat) vfprintf(stream,szFormat,args);
	if(filepath)
	{
		filename = strrchr(filepath,X7S_PATH_SEP);
		if(filename) filename++;
		else filename=filepath;
		fprintf(stream," (%s:%d)",filename,line);
	}
	fprintf(stream,"\n");
	fflush(stream);
	va_end(args);
}



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Function for X7sNetChrono.
//---------------------------------------------------------------------------------------

/**
 * @brief Construct an Chrono object and call X7sNetChrono::Reset()
 */
X7sNetChrono::X7sNetChrono() {
	Reset();
}

/**
 * @brief Get the time "spend" since last reset or construction.
 */
long X7sNetChrono::Time() {
	return (long)((clock() - t0)*(double)(CLOCKS_PER_SEC/1000.0));
}

/**
 * @brief return a real value in seconds.
 * this operation is a shortcut to Time()/1000
 */
float X7sNetChrono::Seconds() {
	return ((float)Time())/1000.0f;
}

/**
 * @brief Reset the chrono.
 */
long X7sNetChrono::Reset() {
	t0 = clock();
	return t0;
}



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Function for X7sNetChrono.
//---------------------------------------------------------------------------------------

X7sNetErrors::X7sNetErrors()
{
	GetNumErrors(0,true);
}

/**
 * @brief Obtain the number of error for a specific type
 * @param type (If type is 0, we check for all type)
 * @param reset (Reset the number of error to zeros if true)
 * @return
 */
int X7sNetErrors::GetNumErrors(int type, bool reset)
{
	int ret=-1;
	type = abs(type);

	if(type == 0)
	{
		for(int i=0;i<NTYPES;++i)
		{
			ret+=errors[i];
			if(reset) errors[i]=0;
		}
	}
	else if( X7S_CHECK_RANGE(type,1,NTYPES) )
	{
		ret+=errors[type];
		if(reset) errors[type]=0;
	}


	return ret;
}



void X7sNetErrors::IncrementNumError(int type)
{
	type=abs(type); //Take absolute value
	 if( X7S_CHECK_RANGE(type,1,NTYPES) )
	 {
		 errors[type]++;
	 }

}




//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Private functions
//----------------------------------------------------------------------------------------


/**
 * @brief Print a buffer in the specific format
 * @note The size of the buffer is not more than 512 character.
 * @param format The text where that need to be printed where the tag '%s' is used to
 * setup where the array will be printed.
 * @param buff Point on the beginning of a buffer.
 * @param length of the array in bytes.
 * @param in_fmt format to print the uint8_t value. (by default it's "%x ")
 * @param mod_endl Number of value before inserting a break line.
 * @param stream The File where we are going to write this message.
 *
 *
 */
void x7sPrintU8Array(const char *format,const uint8_t *buff, int length, const char* in_fmt, int mod_endl,FILE *stream) {
	char ret[1024];
	char tmp[50];
	int i=0;
	strcpy (ret,"");

	for(i=0;i<length;i++) {
		if(mod_endl>0 && i%mod_endl==0) snprintf(ret,1024,"%s\n",ret);
		snprintf(tmp,50,in_fmt,buff[i]);	//Print the inside format
		strcat(ret,tmp);				//Concatenate it to the real string
	}
	i = strlen(ret)-1; 		//Search the position of last character
	ret[i]='\0';			//Remove it
	fprintf(stream,format,ret);		//Then format with the real string
}
