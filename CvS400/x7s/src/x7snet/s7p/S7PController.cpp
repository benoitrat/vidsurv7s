#include "s7p/S7PController.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sSocket.hpp"
#include "X7sNetTools.hpp"	//!< For Log Message
#include <cstdlib>			//!< For random


#define IX7S_S7PCTRL_CHECKSOCKET(funcName,mysock) \
	X7S_CHECK_WARN(funcName,(mysock && mysock->IsOk()),X7S_NET_RET_SOCKETCLOSED,"Check Socket %s",X7sNetSession::GetErrorMsg(X7S_NET_RET_SOCKETCLOSED));


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor and Initialization.
//---------------------------------------------------------------------------------------

/**
* @brief Create a S7PControler to send parameters and command through the S7P Protocol.
* @param sock A Pointer on a X7sSocket open previously.
* @param max_rtt_ms The maximum Round-Trip-Time before timeout (send and reply time).
* @warning The socket should not be deleted twice, therefore it should be deleted only here.
*
*/
S7PController::S7PController(X7sSocket* sock, int max_rtt_ms)
:sock(sock),max_rtt_ms(max_rtt_ms)
 {
	mypkt  = new s7p_hdr_t();
	xilpkt = new s7p_hdr_t();
	sock->SetRcvTimeOut(max_rtt_ms);

	X7S_PRINT_INFO("S7PController::S7PController()",sock->toString().c_str());
 }

/**
* @brief Destructor which kill the memory reserver for the S7P pkt header.
*/
S7PController::~S7PController() {
	X7S_PRINT_DEBUG("S7PController()::~S7PController()");

	//Send stop command and don't wait reply
	mypkt->cmd=S7P_CMD_SEND_STOP;
	mypkt->camID=CAMID_ALL;
	mypkt->fpID=0x0123;

	s7p_hdr_be_t pkt_tmp;		//!< Temporary s7p header (in big endian (network) for debugging).

	//Swap from little endian (x86) to big endian (network).
	X7sSocket::buff_htonl((uint8_t*)(&pkt_tmp),(uint8_t*)(mypkt),sizeof(s7p_hdr_t));

	//Send the packet
	sock->SendPkt((uint8_t*)(&pkt_tmp),sizeof(s7p_hdr_t));


	if(mypkt) delete(mypkt);
	mypkt=NULL;

	if(xilpkt) delete(xilpkt);
	xilpkt=NULL;

	if(sock) delete(sock);	//IMPROVE: Maybe change the way to delete the socket. it should not be here.
	sock=NULL;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Runing commands.
//---------------------------------------------------------------------------------------

/**
* @brief Send a ping to the board and wait ::main_rtt_ms time for a reply.
*/
int S7PController::Ping() {

	X7S_FUNCNAME("S7PController::Ping()");

	//Check socket
	IX7S_S7PCTRL_CHECKSOCKET(funcName,sock);

	//Create a random ping number.
	int ran_id = rand();
	ran_id=(int)0x01234567;

	//Prepare the packet
	mypkt->cmd=S7P_CMD_BOARD_ASK;
	mypkt->camID=CAMID_ALL;
	mypkt->fpID=0xC1A0;	//Ciao, como stai.....
	mypkt->val=ran_id;

	//Send command and wait the reply.
	int ret=this->SendCmdWaitReply(S7P_CMD_BOARD_ACK,mypkt->fpID);

	//Print an error message
	if(ret!=X7S_NET_RET_OK)
	{
		X7S_PRINT_WARN(funcName,"%s (%d) from %s",X7sNetSession::GetErrorMsg(ret),ret,sock->GetRemoteAddr().c_str());
	}

	//Return the OK or an error
	return ret;


}

/**
* @brief Start sending the data.
* @note The type of data must be configured before using this command.
* @return X7S_NET_RET_OK if everything is fine or the error code of int.
*/
int S7PController::Start() {

	X7S_FUNCNAME("S7PController::Start()");

	//Check socket
	IX7S_S7PCTRL_CHECKSOCKET(funcName,sock);

	//Prepare the packet
	mypkt->cmd=S7P_CMD_SEND_START;
	mypkt->camID=CAMID_ALL;
	mypkt->fpID=0x0123;

	//Send command and wait the reply.
	int ret=this->SendCmdWaitReply(S7P_CMD_SEND_ACK,mypkt->fpID);

	//Print an error message
	if(ret!=X7S_NET_RET_OK)
	{
		X7S_PRINT_WARN(funcName,"%s (%d) from %s",X7sNetSession::GetErrorMsg(ret),ret,sock->GetRemoteAddr().c_str());
	}

	//Return the OK or an error
	return ret;
}

/**
* @brief Ask the board to stop sending data.
* @return X7S_NET_RET_OK if everything is fine or the error code of int.
*/
int S7PController::Stop() {

	X7S_FUNCNAME("S7PController::Stop()");

	//Check socket
	IX7S_S7PCTRL_CHECKSOCKET(funcName,sock);

	//Prepare the packet
	mypkt->cmd=S7P_CMD_SEND_STOP;
	mypkt->camID=CAMID_ALL;
	mypkt->fpID=0x3210;

	//Send command and wait the reply.
	int ret=this->SendCmdWaitReply(S7P_CMD_SEND_ACK,mypkt->fpID);

	//Print an error message
	if(ret!=X7S_NET_RET_OK)
	{
		X7S_PRINT_WARN(funcName,"%s (%d) from %s",X7sNetSession::GetErrorMsg(ret),ret,sock->GetRemoteAddr().c_str());
	}

	//Return the OK or an error
	return ret;
}

/**
* @brief Reset the sending of the board.
* @note This function is only equivalent as Stop()/Start().
* @todo Parameters written should be restore.
* @return X7S_NET_RET_OK if everything is fine or the error code of int.
*/
int S7PController::Reset() {
	int ret;

	ret = this->Stop();
	if(ret == X7S_NET_RET_OK) {
		ret = this->Start();
	}
	return ret;

}

/**
* @brief Set,Get or Apply the parameters to the board.
*/
int S7PController::SetGetApplyParam(uint8_t cmd,uint16_t ptype, uint32_t& param, uint8_t cam_id) {

	X7S_FUNCNAME("S7PController::SetGetApplyParam()");

	//Check socket
	IX7S_S7PCTRL_CHECKSOCKET(funcName,sock);

	std::stringstream sstr;
	sstr << "#" << (cmd-S7P_CMD_PARAM_RST) << " >> ";
	if(cam_id != CAMID_ALL && cam_id != CAMID_NONE) {
		sstr << "CAMID_" << (cam_id+1) << ": ";
	}

	//Prepare the packet
	mypkt->cmd=cmd;
	mypkt->val=param;
	mypkt->camID=cam_id;
	mypkt->fpID=ptype;

	//Send command and wait the reply.
	int ret=this->SendCmdWaitReply(S7P_CMD_PARAM_ACK,ptype);

	if(ret==X7S_NET_RET_OK)
	{
		sstr << GetParamName(ptype) << " (0x" << std::hex << ptype << ") = ";

		if(cmd==S7P_CMD_PARAM_GET) {
			param=xilpkt->val; //Write the parameters in case of get
		}
		if(param < 0xFFFF) sstr << std::dec << param;
		else sstr <<  std::hex << "0x" << param;
		X7S_PRINT_INFO(funcName,sstr.str().c_str());
	}
	else if(xilpkt->cmd==S7P_CMD_PARAM_ERR)
	{
		std::string err_str=GetErrorName(xilpkt->val);
		X7S_PRINT_WARN(funcName,"%s %s (%s #%d) from %s",
				sstr.str().c_str(),err_str.c_str(),
				X7sNetSession::GetErrorMsg(ret),ret,sock->GetRemoteAddr().c_str());
	}


	return ret;
}

std::string S7PController::GetRemoteAddr() const {
	if(sock) return sock->GetRemoteAddr();
	else return std::string("NULL");
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected functions.
//---------------------------------------------------------------------------------------


/**
* @brief Send a command and wait the reply using min_rtt_ms time.
* @note In this function packet are swapped between little<->big endian.
* @deprecated
*
*/
int S7PController::SendCmdWaitReply() {
	int ret=0;

	s7p_hdr_be_t pkt_tmp;		//!< Temporary s7p header (in big endian (network) for debugging).

	//Swap from little endian (x86) to big endian (network).
	X7sSocket::buff_htonl((uint8_t*)(&pkt_tmp),(uint8_t*)(mypkt),sizeof(s7p_hdr_t));

	//First flush the socket to ensure that it is empty
	sock->Flush();

	//Send the packet
	sock->SendPkt((uint8_t*)(&pkt_tmp),sizeof(s7p_hdr_t));


	//Then look if we have received a packet.
	ret = sock->RecvPktChk((uint8_t*)(&pkt_tmp),sizeof(s7p_hdr_t));


	//Swap from big endian (network) to little endian (PC)
	if(ret>0) {
		X7sSocket::buff_ntohl((uint8_t*)(xilpkt),(uint8_t*)(&pkt_tmp),sizeof(s7p_hdr_t));
	}
	return ret;
}


/**
* @brief Send a command and wait the reply using min_rtt_ms time.
* @note In this function packet are swapped between little<->big endian.
* @deprecated
*
*/
int S7PController::SendCmdWaitReply(uint8_t expected_cmd, uint16_t expected_fpID) {

	int rcv_bytes=0;
	s7p_hdr_be_t pkt_tmp;		//!< Temporary s7p header (in big endian (network) for debugging).

	//Swap from little endian (x86) to big endian (network).
	X7sSocket::buff_htonl((uint8_t*)(&pkt_tmp),(uint8_t*)(mypkt),sizeof(s7p_hdr_t));

	//First flush the socket to ensure that it is empty
	sock->Flush();

	//Send the packet
	int ret=sock->SendPktChk((uint8_t*)(&pkt_tmp),sizeof(s7p_hdr_t));
	if(ret<=0) return X7S_NET_RET_ONSENDING;

	//Then look if we have received a packet.
	while(TRUE)
	{
		rcv_bytes = sock->RecvPktChk((uint8_t*)(&pkt_tmp),sizeof(s7p_hdr_t));

		//Swap from big endian (network) to little endian (PC)
		if(rcv_bytes>0)
		{
			X7sSocket::buff_ntohl((uint8_t*)(xilpkt),(uint8_t*)(&pkt_tmp),sizeof(s7p_hdr_t));

			//Parse normal cmd
			if(xilpkt->cmd==expected_cmd && xilpkt->fpID==expected_fpID)
			{
				return X7S_NET_RET_OK;
			}
			//Parse parameters cmd error
			else if(xilpkt->cmd==S7P_CMD_PARAM_ERR && expected_cmd==S7P_CMD_PARAM_ACK)
			{
				return GetX7sCtrlError(xilpkt->val);
			}
			else
			{
				X7S_PRINT_WARN("S7PController::SendCmdWaitReply()",
						"Receive bad reply cmd=0x%x,fpID=0x%x (Expected 0x%x,0x%x)",
						xilpkt->cmd,xilpkt->fpID,expected_cmd,expected_fpID);
				continue; //Continue to loop until timeout.
			}
		}
		else
		{
			return X7S_NET_RET_TIMEOUT;	//Receive a timeout
		}
	}
	return X7S_NET_RET_UNKOWN;
}

/**
* @brief Get the int type giving an error from the S7P Protocol.
* @param s7p_error_id The S7P error ID (See @ref S7P_ERR_xxx).
* @note All error of type @tt{xxx_NOTDEF} are replaced by X7S_NET_RET_NOTFOUND.
*/
int S7PController::GetX7sCtrlError(int s7p_error_id) {
	switch(s7p_error_id) {
	case S7P_ERR_PRM_READONLY:
		return X7S_NET_RET_READONLY;
	case S7P_ERR_PRM_OUTRANGE:
		return X7S_NET_RET_OUTRANGE;
	case S7P_ERR_PRM_NOTDEF:
	case S7P_ERR_CAMID_NOTDEF:
	case S7P_ERR_CMD_NOTDEF:
		return X7S_NET_RET_NOTFOUND;
	default:
		return X7S_NET_RET_UNKOWN;
	}
}

/**
* @brief Get the name of an error from the S7P Protocol.
* @param s7p_error_id The S7P error ID (See @ref S7P_ERR_xxx).
*/
std::string S7PController::GetErrorName(int s7p_error_id) {
	std::string ret;
	switch(s7p_error_id) {
	case S7P_ERR_UNKOWN: ret="S7P_ERR_UNKOWN"; break;
	case S7P_ERR_PRM_NOTDEF: ret="S7P_ERR_PRM_NOTDEF"; break;
	case S7P_ERR_PRM_READONLY: ret="S7P_ERR_PRM_READONLY"; break;
	case S7P_ERR_PRM_OUTRANGE: ret="S7P_ERR_PRM_OUTRANGE"; break;
	case S7P_ERR_CAMID_NOTDEF: ret="S7P_ERR_CAMID_NOTDEF"; break;
	case S7P_ERR_CMD_NOTDEF: ret="S7P_ERR_CMD_NOTDEF"; break;
	default:	ret="UNKNOWN"; break;
	}
	return ret;
}

/**
* @brief Get the name of a parameters from the S7P Protocol.
* @param ptype Parameters type form S7P (See @ref S7P_PRM_xxx).
*/
std::string S7PController::GetParamName(uint16_t ptype) {
	std::string ret;

	switch(ptype) {
	case S7P_PRM_RO_VFW: ret="S7P_PRM_RO_VFW"; break;
	case S7P_PRM_RO_VS7P: ret="S7P_PRM_RO_VS7P"; break;
	case S7P_PRM_RO_NOFCAM: ret="S7P_PRM_RO_NOFCAM"; break;
	case S7P_END_PRM_RO: ret="S7P_END_PRM_RO"; break;

	/* BoaRD Parameters */
	case S7P_PRM_BRD_IP: ret="S7P_PRM_BRD_IP"; break;
	case S7P_PRM_BRD_PORT_S7P: ret="S7P_PRM_BRD_PORT_S7P"; break;
	case S7P_PRM_BRD_DHCP_ON: ret="S7P_PRM_BRD_DHCP_ON"; break;
	case S7P_PRM_BRD_MODES: ret="S7P_PRM_BRD_MODES"; break;
	case S7P_PRM_BRD_DTYPE: ret="S7P_PRM_BRD_DTYPE"; break;
	case S7P_PRM_BRD_WSIZE: ret="S7P_PRM_BRD_WSIZE"; break;
	case S7P_PRM_BRD_FRAMERATE: ret="S7P_PRM_BRD_FRAMERATE"; break;
	case S7P_PRM_BRD_PORT_RTP: ret="S7P_PRM_BRD_PORT_RTP"; break;
	case S7P_PRM_BRD_SSRCID: ret="S7P_PRM_BRD_SSRCID"; break;
	case S7P_END_PRM_BRD: ret="S7P_END_PRM_BRD"; break;

	/* Camera Parameters */
	case S7P_PRM_CAM_ENABLE: ret="S7P_PRM_CAM_ENABLE"; break;
	case S7P_PRM_CAM_RESO: ret="S7P_PRM_CAM_RESO"; break;
	case S7P_PRM_CAM_DTYPE: ret="S7P_PRM_CAM_DTYPE"; break;
	case S7P_PRM_CAM_MTYPE: ret="S7P_PRM_CAM_MTYPE"; break;
	case S7P_PRM_CAM_QUALITY: ret="S7P_PRM_CAM_QUALITY"; break;
	case S7P_PRM_CAM_BG_THRS: ret="S7P_PRM_CAM_BG_THRS"; break;
	case S7P_PRM_CAM_BG_INSERT: ret="S7P_PRM_CAM_BG_INSERT"; break;
	case S7P_PRM_CAM_BG_REFRESH: ret="S7P_PRM_CAM_BG_REFRESH"; break;
	case S7P_PRM_CAM_FG_REFRESH: ret="S7P_PRM_CAM_FG_REFRESH"; break;
	case S7P_PRM_CAM_CC_MINAREA: ret="S7P_PRM_CAM_CC_MINAREA"; break;
	case S7P_PRM_CAM_TMP1: ret="S7P_PRM_CAM_TMP1"; break;
	case S7P_PRM_CAM_TMP2: ret="S7P_PRM_CAM_TMP2"; break;
	case S7P_PRM_CAM_TMP3: ret="S7P_PRM_CAM_TMP3"; break;
	case S7P_END_PRM_CAM: ret="S7P_END_PRM_CAM"; break;

	/* Firewire IEEE1934 IIDC camera standard parameters */
	case S7P_PRM_IIDC_BRIGHTNESS: ret="S7P_PRM_IIDC_BRIGHTNESS"; break;
	case S7P_PRM_IIDC_AUTO_EXPOSURE: ret="S7P_PRM_IIDC_AUTO_EXPOSURE"; break;
	case S7P_PRM_IIDC_SHARPNESS: ret="S7P_PRM_IIDC_SHARPNESS"; break;
	case S7P_PRM_IIDC_WHITE_BALANCE: ret="S7P_PRM_IIDC_WHITE_BALANCE"; break;
	case S7P_PRM_IIDC_HUE: ret="S7P_PRM_IIDC_HUE"; break;
	case S7P_PRM_IIDC_SATURATION: ret="S7P_PRM_IIDC_SATURATION"; break;
	case S7P_PRM_IIDC_GAMMA: ret="S7P_PRM_IIDC_GAMMA"; break;
	case S7P_PRM_IIDC_SHUTTER: ret="S7P_PRM_IIDC_SHUTTER"; break;
	case S7P_PRM_IIDC_GAIN: ret="S7P_PRM_IIDC_GAIN"; break;
	case S7P_PRM_IIDC_IRIS: ret="S7P_PRM_IIDC_IRIS"; break;
	case S7P_PRM_IIDC_FOCUS: ret="S7P_PRM_IIDC_FOCUS"; break;
	case S7P_PRM_IIDC_TEMPERATURE: ret="S7P_PRM_IIDC_TEMPERATURE"; break;
	case S7P_PRM_IIDC_TRIGGER: ret="S7P_PRM_IIDC_TRIGGER"; break;
	case S7P_PRM_IIDC_ZOOM: ret="S7P_PRM_IIDC_ZOOM"; break;
	case S7P_PRM_IIDC_PAN: ret="S7P_PRM_IIDC_PAN"; break;
	case S7P_PRM_IIDC_TILT: ret="S7P_PRM_IIDC_TILT"; break;
	case S7P_PRM_IIDC_FILTER: ret="S7P_PRM_IIDC_FILTER"; break;
	case S7P_END_PRM_IIDC: ret="S7P_END_PRM_IIDC"; break;
	case S7P_PRM_UNKNOWN: ret="S7P_PRM_UNKNOWN"; break;
	default:	ret="UNKNOWN"; break;
	}

	return ret;
}



/**
* @brief Set the maximum RTT (Round-Trip Time) that a packet can have to send a command and receive a response.
* @note If the RTT of a command is bigger than this value, it will return X7S_NET_RET_TIMEOUT.
* @param rtt_ms The value of the RTT in milliseconds: ~[300ms-2000ms].
*/
void S7PController::SetMaximumRTT(int rtt_ms) {
	this->max_rtt_ms=rtt_ms;
	sock->SetRcvTimeOut(max_rtt_ms);
}


