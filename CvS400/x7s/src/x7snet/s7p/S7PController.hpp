/**
 *  @file
 *  @brief Contains the class S7PController.hpp
 *  @date 12-feb-2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef S7PCONTROLER_HPP_
#define S7PCONTROLER_HPP_

#include <string>
#include "x7snet.h"

class X7sSocket;	//Forward declaration: this object is included in the .cpp file.


typedef s7p_hdr_le_t s7p_hdr_t; //!< The S7P header is in little endian.


/**
 *	@brief The Controler of dataflow and parameters using the S7P Protocol.
 *	@ingroup net
 */
class S7PController {
public:
	S7PController(X7sSocket* sock, int rtt_ms=3000);
	virtual ~S7PController();

	int Ping();
	int Start();
	int Stop();
	int Reset();
	int SetGetApplyParam(uint8_t cmd,uint16_t ptype, uint32_t& param, uint8_t cam_id);

	void SetMaximumRTT(int rtt_ms);
	int GetMaximumRTT() const;
	const X7sSocket* GetSoscket() const { return sock; };
	std::string GetRemoteAddr() const;

	static std::string GetParamName(uint16_t ptype);
	static std::string GetErrorName(int s7p_error_id);
	static int GetX7sCtrlError(int s7p_error_id);

protected:
	int SendCmdWaitReply();
	int SendCmdWaitReply(uint8_t expected_cmd, uint16_t expected_fpID);
	s7p_hdr_t *mypkt;	//!< Data reserved to send to the Socket.
	s7p_hdr_t *xilpkt;	//!< Data reserved to received in the Socket.
	X7sSocket* sock;	//!< A pointer on the socket (Must be opened before using it)
	int max_rtt_ms;		//!< The maximum value of the RTT in milliseconds (wait in Sockets).
};



/**
 * @brief Return the value of the maximum RTT (Round-Trip Time).
 * @return The value of the RTT in milliseconds.
 * @see SetMaximumRTT().
 */
inline int S7PController::GetMaximumRTT(void) const {
	return this->max_rtt_ms;
}


#endif /* S7PCONTROLER_HPP_ */
