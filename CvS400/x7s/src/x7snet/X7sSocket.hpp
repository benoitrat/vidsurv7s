/**
 *  @file
 *  @brief Contains the class X7sSocket.hpp
 *  @date 12-nov-2008
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef _X7S_NET_SOCKET_HPP_
#define _X7S_NET_SOCKET_HPP_

#include "x7snet.h" //For uint32_t, uint8_t,...

#include <string>	//For toPrint().
#include <sstream>  //For toPrint().
#include <cstdio>	//For print...
#include <cstring>	//For standard library

/**
 * @brief The type of error that a X7sSocket can return.
 * @anchor X7S_NET_SOCKERR_xxx
 *
 */
enum {
	X7S_NET_SOCKERR_MISC=-100,
	X7S_NET_SOCKERR_ONSND,		//!< An error occurs while trying to send a packet.
	X7S_NET_SOCKERR_ONRCV,		//!< An error occurs while trying to received a packet.
	X7S_NET_SOCKERR_TIMEOUT,		//!< An error of timeout occurs while using SendWaitReply().
	X7S_NET_SOCKERR_ONOPEN,		//!< An error occurs when the OS try to open the socket.
	X7S_NET_SOCKERR_ONBIND,		//!< An error occurs when the OS try to set the socket in listening mode
	X7S_NET_SOCKERR_NOTDEF,		//!< This operation is not defined by the socket.
	X7S_NET_SOCKERR_NOERROR=1	//!< No errors occurs.
};

/**
 * @brief The state a X7sSocket can have.
 * @anchor X7S_NET_SOCKSTATE_xxx
 */
enum {
	X7S_NET_SOCKSTATE_NULL=0,	//!< No direction, interface or port has been given.
	X7S_NET_SOCKSTATE_INIT,
	X7S_NET_SOCKSTATE_CLOSED,
	X7S_NET_SOCKSTATE_OPENED,
	X7S_NET_SOCKSTATE_CONNECTED,
	X7S_NET_SOCKSTATE_BINDED,
};

/**
 * @brief The type of X7sSocket we can use.
 * @anchor X7S_NET_SOCKTYPE_xxx
 */
enum {
	X7S_NET_SOCKTYPE_UDP,
	X7S_NET_SOCKTYPE_TCP,
	X7S_NET_SOCKTYPE_MAC,
};


enum {
	X7S_NET_SOCKFLAG_TIMEDOUT,
	X7S_NET_SOCKFLAG_WAITALL,
	X7S_NET_SOCKFLAG_NOWAIT,
};

/**
 * @brief An abstract socket class that permit to send or receive packets.
 *
 * Different type of socket can be use for packet communication:
 * 		- As a standard UDP socket
 * 		- As a standard TCP (If S4NSocketTCP is defined).
 * 		- As a special MAC socket using libpcap  (if S4NSocketMAC is defined).
 *
 * The socket are by default configure in blocking mode, you can change
 * this parameters by playing with the SetRcvTimeOut() function.
 *
 * As not all the functions works in the same way or are not defined the
 * enum X7sSocketError has been defined to identify the error.
 *
 *	@ingroup net
 */
class X7sSocket {
public:

	X7sSocket(int sockType);
	virtual ~X7sSocket();


	virtual int Bind()=0;	//!< Open the socket and listen.
	virtual int Open()=0;	//!< Open the socket.
	virtual int Close()=0;	//!< Close the socket.
	virtual int Discard();
	virtual int SendPkt(uint8_t *buffer, const int& nbBytes) = 0;	//!< Abstract Method: Send a packet.
	virtual int RecvPkt(uint8_t *buffer, const int& nbBytes) = 0;	//!< Abstract Method: Receive a packet.
	virtual int Flush()=0;	//!< Abstract method: Flush the data that are already on the socket.


	virtual bool IsOk();	//!< Tell if the socket is Ok to receive or send.
	virtual int SendPktChk(uint8_t *buffer, const int& nbBytes);
	virtual int RecvPktChk(uint8_t *buffer, const int& nbBytes);
	virtual int SendWaitReply(uint8_t *buff_snd,const int& nbB_snd, uint8_t *buff_rcv, const int& nbB_rcv, int timeout=1000, int nof_tries=1);
	virtual bool SetRcvTimeOut(int nof_milli);
	virtual bool SetRcvBufferSize(int buffsize)=0;
	virtual int GetRcvBufferSize()=0;

	virtual void SetFlags(int flags)=0;
	int GetType();
	int GetState();


	/* Function for conversion between little and big endian */
	static uint32_t pbyte_ntohl(const uint8_t* p);
	static uint32_t pbyte_to32(const uint8_t *p);
	static void buff_ntohl(uint8_t *p_le,const uint8_t *p_be, int bsize);
	static void buff_htonl(uint8_t *p_be, const uint8_t *p_le, int bsize);

	virtual std::string GetRemoteAddr() const { return "NULL"; };
	virtual std::string toString();

protected:
	int socketType;			//!< The type of the socket. See ::X7sSocketType.
	int state;				//!< The state of the socket See ::X7sSocketState.
	int timeout_rcv;		//!< The timeout for the reception in ms (by default blocking=-1).
	int timeout_snd;		//!< The timeout for the send in ms (by default blocking=-1).
};


inline int X7sSocket::GetType() {
	return socketType;
}

inline int X7sSocket::GetState() {
	return state;
}



#endif /* X7S_NET_SOCKET_HPP_ */
