#include "x7snet.h"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sNetTools.hpp"	//X7S_PRINT_xxx, ...
#include "X7sNetBoard.hpp"
#include "X7sSocketUDP.hpp"
#include "rtp/RTPDataRcvr.hpp"
#include "s7p/S7PController.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Internal define and Prototype
//----------------------------------------------------------------------------------------

#define IXSNET_TYPE_UNKOWN 0
#define IXSNET_TYPE_RTPS7P 1
#define IXSNET_TYPE_S7P 2


int g_killwait=FALSE;
void wait ( int seconds );

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Desctructor/Connect.
//----------------------------------------------------------------------------------------

/**
* @brief Default empty constructor.
* @see X7sNetSession::Connect().
*/
X7sNetSession::X7sNetSession()
:ctrlr(NULL), drcvr(NULL), board(NULL),errors(NULL),
 session_type(IXSNET_TYPE_UNKOWN),
 is_socketok(false), is_setup(false),
 is_alive(false), t_alive(-1)
 {
	errors = new X7sNetErrors();
 }

/**
* @brief Default Destructor
*
* This method performs the following steps:
* 		-# Send Stop Command To The Board.
* 		-# Close the socket.
* 		-# Close log files.
*/
X7sNetSession::~X7sNetSession()
{
	X7S_PRINT_DEBUG("X7sNetSession::~X7sNetSession()");
	this->Close();
	X7S_DELETE_PTR(errors);
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods.
//----------------------------------------------------------------------------------------

/**
* @brief Connect a full S7P session over MAC layer.
* @warning Winpcap/Libpcap need to be installed on the PC
* and the symbols USE_PCAP need to be added during compilation.
*
*/
int X7sNetSession::Connect(const char *eha_remote, const char* eha_local)
{
#ifdef USE_PCAP

#else
	X7S_PRINT_ERROR("X7sNetSession::Connect()","Error: Can't use S7P on MAC layer because pcap.lib is not found");
#endif
	return X7S_NET_RET_UNKOWN;
}


/**
* @brief Connect a RTP/S7P session over UDP layer.
*
* This function performs the following steps:
* 		-# Create the control socket (UDP/S7P).
* 		-# Bind the receiving UDP/RTP socket on the first available port start at port_local.
* 		-# Then, check if the board is connected.
* 		-# Finally, setup the X7sByteArray.
* @param ip_remote The IP of the board we want to communicates (default=192.168.7.254).
* @param port_remote The port of the board which listen to the S7P protocol (default=18000).
* @param port_local The port on which we listen to receive the X7sNetFrame
* @return the return value give if an error occurs during the connection (See @ref X7S_NET_RET_xxx).
* @note If the connection failed, you can retry it. The internal value created will
* not be created twice. Once the session has established a connection you can setup the parameters
* with X7sNetSession::SetParam() or X7sNetSession::GetParam(),
* and finally start the sending of the X7sNetFrame using X7sNetSession::Start().
*/
int X7sNetSession::Connect(uint32_t ip_remote, uint16_t port_remote,uint16_t& port_local)
{
	X7S_FUNCNAME("X7sNetSession::Connect");

	int ret=X7S_NET_RET_UNKOWN;
	X7sSocket *rtp_sock, *s7p_sock;
	s7p_sock=NULL;
	rtp_sock=NULL;


	if(!is_socketok) {
		//Create the board
		board = new X7sNetBoard();
		X7sNet_SetIP(ip_remote,board->ip_str);

		X7S_PRINT_INFO(funcName,"Connecting... (%s:%d)",board->ip_str,port_remote);

		//Create the S7P Socket
		s7p_sock=new X7sSocketUDP(ip_remote,port_remote);
		if(s7p_sock->Open()!=X7S_NET_SOCKERR_NOERROR) return X7S_NET_RET_SOCKETCLOSED;

		//Connect the S7P controller
		ctrlr=new S7PController(s7p_sock,X7S_NET_SESSION_CTRL_TOUT_MS);

		//Then bind RTP socket on the first available port_local.
		do {
			//Check if rtpsock has been opened before.
			if(rtp_sock) {
				delete rtp_sock;	//close rtp_socket
				port_local+=2;		//Set port local to the next one
				if(port_local >= 65534) break;
			}
			rtp_sock = new X7sSocketUDP(port_local,ip_remote,X7S_NET_SOCK_ANYPORT);
		} while(rtp_sock->Bind()!=X7S_NET_SOCKERR_NOERROR);
		if(!rtp_sock->IsOk()) return X7S_NET_RET_SOCKETCLOSED;

		//Increase the size of the socket buffer to handle various image in the same socket.
		rtp_sock->SetRcvBufferSize(128000);	//The total size is 128Ko and an image is about 20Ko.
		rtp_sock->SetRcvTimeOut(X7S_NET_SESSION_DRCVR_TOUT);

		drcvr = (X7sDataRcvr*)(new RTPDataRcvr(rtp_sock,board));
		board->pval[S7P_PRM_BRD_PORT_RTP]=port_local;


		is_socketok=true;	//Avoid to reopen the socket if the init failed for other reason.
		session_type=IXSNET_TYPE_RTPS7P;
	}

	//Then, check if it is really a session of RTP/S7P type.
	if(session_type!=IXSNET_TYPE_RTPS7P) {
		return X7S_NET_RET_UNKOWN;
	}

	//Then check if the board is connected
	if(!IsAlive(1)) {
		return X7S_NET_RET_TIMEOUT;
	}

	//Send stop message to stop communication first.
	ret = ctrlr->Stop();
	X7S_CHECK_WARN(funcName,(ret==X7S_NET_RET_OK),ret,"Error during stop %s (%d)",GetErrorMsg(ret),ret);

	//Finally set/get the necessary parameters to create the buffer.
	ret = this->SetupRTP();

	if(ret==X7S_NET_RET_OK) 
		X7S_PRINT_INFO(funcName,"OK (%s:%d is connected)",board->ip_str,port_remote);

	return ret;

}

/**
* @brief Close all the connection and socket.
*
* After using close, you must use the Connect() function to restart a connection
*
* @note This function can be use in parallel to another function.
* @return the same return as X7sNetSession::Stop()
*/
int X7sNetSession::Close()
{
	X7S_PRINT_DEBUG("X7sNetSession::Close()");
	int ret;
	g_killwait=TRUE;
	ret=this->Stop();
	X7S_DELETE_PTR(ctrlr);
	X7S_DELETE_PTR(drcvr);
	X7S_DELETE_PTR(board);
	session_type=IXSNET_TYPE_UNKOWN;
	is_socketok=false;
	is_setup=false;
	is_alive=false;
	t_alive=-1;
	return ret;
}


/**
* @brief Send start command to the board.
*
* This function check if the network session is still alive
* and correctly setup. Then it send a the start command to the board
* and wait to be acknowledge that the board correctly start to send
* the frame and its metadata to the listening port.
*
* @return @ref X7S_NET_RET_OK if the start command was correctly handle.
* Otherwise it return an error code (See @ref X7S_NET_RET_xxx).
*/
int X7sNetSession::Start()
{
	if(IsAlive()) {
		if(!is_setup) {
			X7S_PRINT_WARN("X7sNetSession::Start()", "Setup is not correct, retry calling Init()");
			return X7S_NET_RET_UNKOWN;
		}
		//Finally reset the parameters of datarecv
		drcvr->Reset();
		return ctrlr->Start();
	}
	return X7S_NET_RET_UNKOWN;
}

/**
* @brief Send stop command to the board.
* @note This function permits to stop the transmission from the board
* to our socket and therefore liberate the bandwidth of our network.
* @return @ref X7S_NET_RET_OK if the start command was correctly handle.
* Otherwise it return an error code (See @ref X7S_NET_RET_xxx).
*/
int X7sNetSession::Stop()
{
	if(IsAlive(0)) {	//!< Check if the previous state was Alive or not?
		return ctrlr->Stop();
	}
	return X7S_NET_RET_OK;
}

/**
* @brief Check if the NetSession is Alive().
* @param nof_tries Number of Tries before returning that the board is unreachable.
* The default value is given by @ref X7S_NET_PING_NOFTRIES.
* The maximum number of tries is 10 and the wait time between each try is X7S_NET_SESSION_CTRL_TOUT_MS.
* 	-	If the value is equal to zeros, then the function only check if the last connection was alive since
* X7S_NET_SESSION_PING_TOUT_MS.
* 	-	If the value is negative, the method doesn't check the previous state of a the netsession.
* @see X7S_NET_SESSION_PING_TOUT_MS
*/
bool X7sNetSession::IsAlive(int nof_tries)
{
	if(ctrlr) {

		//Check when was the last time to check the ping message.
		if(is_alive && nof_tries>=0)
		{
			if(time(NULL)-t_alive < X7S_NET_SESSION_PING_TOUT_S) {
				return true;
			}
		}
		if(nof_tries==0) return false;
		if(nof_tries<0) nof_tries=abs(nof_tries);

		//int ar_wait_s[10] = {0,2,5,10,15,35,50,85,125,200};
		nof_tries=nof_tries<10?nof_tries:10;	//Nof of tries is at maximum 10
		X7S_PRINT_INFO("X7sNetSession::IsAlive()","number of tries %d. Starting... (%s)",nof_tries,board->ip_str);
		is_alive=false;
		for(int i=0;i<nof_tries;i++) {
			//if(ar_wait_s[i]>0) {
			//	X7S_PRINT_INFO("X7sNetSession::IsAlive()","Timeout. Wait %d seconds for try %d/%d...",
			//			ar_wait_s[i],i+1,nof_tries);
			//	wait(ar_wait_s[i]);	//Wait in seconds
			//}
			if(i>0) {
				X7S_PRINT_INFO("X7sNetSession::IsAlive()","Try %d/%d... (%s)",i+1,nof_tries,board->ip_str);
			}
			if(ctrlr && ctrlr->Ping()==X7S_NET_RET_OK) {
				is_alive=true;
				t_alive=time(NULL);
				break;
			}
		}
		if(!is_alive) {
			X7S_PRINT_WARN("X7sNetSession::IsAlive()","ERROR! %s Is Unreachable...",board->ip_str);
		}
		else {
			X7S_PRINT_INFO("X7sNetSession::IsAlive()","OK! (%s)",board->ip_str);
		}
		return is_alive;
	}
	return false;
}

/**
* @brief Set a parameters in the board.
*
* The parameters set are keep until the end of the execution, therefore, it will not resend
* a value of a parameters if this one did not change.
*
* @param ID 		The ID of the parameters (See @ref S7P_PRM_xxx)
* @param val_tmp 		The value of this parameters on 32-bits.
* @param cam_id	The camera ID (by default it is @ref CAMID_ALL), however if you want to
* set a parameters only for a specific camera you should use another @ref CAMID_xxx.
* @return See X7sNetSession::GetParam() except that we can have two more errors:
* 		- @ref X7S_NET_RET_READONLY If the parameters can not be set.
* 		- @ref X7S_NET_RET_OUTRANGE If the range of the value we wanted to put is not correct.
*
*/
int X7sNetSession::SetParam(int ID, const uint32_t& val_tmp, uint8_t cam_id)
{
	int ret=X7S_NET_RET_TIMEOUT;
	uint32_t val=val_tmp;
	if(ctrlr && IsAlive()) {
		ret=X7S_NET_RET_NOTFOUND;

		//Check if its a board param
		if((S7P_PRM_BRD_IP <= ID && ID <S7P_END_PRM_BRD) && board) {
			if(board->pval[ID]==val) ret=X7S_NET_RET_OK;	//Parameters already set.
			else
			{
				if(ctrlr) ret = ctrlr->SetGetApplyParam(S7P_CMD_PARAM_SET,ID,val,CAMID_ALL);
				if(ret==X7S_NET_RET_OK) 	board->pval[ID]=val;	//Keep a temporary value to avoid resending
			}
			return ret;
		}

		//Check if it is a camera parameters
		if(S7P_PRM_CAM_ENABLE <= ID && ID <S7P_END_PRM_CAM) {
			if((cam_id <= CAMID_4) && board && board->cam[cam_id]) {
				if(board->cam[cam_id]->pval[ID]==val) ret=X7S_NET_RET_OK;	//Parameters already set.
				else
				{
					if(ctrlr) ret=ctrlr->SetGetApplyParam(S7P_CMD_PARAM_SET,ID,val,cam_id);
					if(ret==X7S_NET_RET_OK) board->cam[cam_id]->pval[ID]=val; //Keep a temporary value to avoid resending
				}
			}
			return ret;
		}
	}
	return ret;
}

/**
* @brief Get a parameters from the board.
* @param ID 		The ID of the parameters (See @ref S7P_PRM_xxx)
* @param val 		A reference on the value of the parameter we want to receive.
* @param cam_id	The camera ID (by default it is @ref CAMID_ALL), however if you want to
* get a parameters only for a specific camera you should use another @ref CAMID_xxx.
* @return @ref X7S_NET_RET_OK if parameters correctly setup, Otherwise it return an error:
* 		- @ref X7S_NET_RET_TIMEOUT if the board didn't reply.
* 		- @ref X7S_NET_RET_NOTFOUND if ID doesn't correspond to a declared parameters.
* 		- @ref X7S_NET_RET_SOCKETCLOSED if the socket is closed.
* 		- @ref X7S_NET_RET_UNKOWN if another error appears.
*/
int X7sNetSession::GetParam(int ID, uint32_t& val, uint8_t cam_id)
{


	int ret=X7S_NET_RET_TIMEOUT;
	if(ctrlr && IsAlive()) {
		ret=X7S_NET_RET_NOTFOUND;

		//Check if it is a board parameter.
		if((S7P_PRM_RO_VFW <= ID && ID < S7P_END_PRM_RO) ||
				(S7P_PRM_BRD_IP <= ID && ID <S7P_END_PRM_BRD)) {
			ret=ctrlr->SetGetApplyParam(S7P_CMD_PARAM_GET,ID,val,CAMID_ALL);
		}

		//Check if it is a camera parameters
		if(S7P_PRM_CAM_ENABLE <= ID && ID <S7P_END_PRM_CAM) {
			if(cam_id <= CAMID_4) {
				ret=ctrlr->SetGetApplyParam(S7P_CMD_PARAM_GET,ID,val,cam_id);
			}
		}
	}
	return ret;
}

/**
* @brief Capture the actual frame.
*
* This function just return a frame if it exists for this camera ID.
*
* @note If this function is called another time we obtain the next "ITAC" frame (In Time And Camera).
*
* @return If IsNewFrame() is @true for this camera, we return a new frame.
* Otherwise it return @NULL.
*/
X7sNetFrame * X7sNetSession::CaptureMetaFrame(int camID)
{
	if(drcvr==NULL) return NULL;
	return drcvr->CaptureMetaFrame(camID);
}

/**
* @brief Check if there is a new frame.
*
* First flush all the buffer into the X7sNetCamera buffer
* than check if the actual frame is new or not.
*
* @note This function use the socket from X7sDataReceiver.
* @return true if the actual frame is new. Otherwise it returns false
* because this frame has been capture using the CaptureMetaFrame() function.
*/
bool X7sNetSession::IsNewFrame(int camID)
{
	int ret=X7S_NET_RET_UNKOWN;
	if(drcvr)
	{
		ret=drcvr->IsNewFrame(camID);
		if(ret!=X7S_NET_RET_OK) errors->IncrementNumError(ret);
		else return true;
	}
	return false;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods.
//----------------------------------------------------------------------------------------

/**
* @brief Setup the buffer for RTP data transmission.
*
* This parameters are necessary to make the board works.
* 		- By default the camera are disable therefore, this procedure enable at least one.
* 		- The RTP Port is given by the socket and can't be change by user program.
* 		- The SSCRID is select randomly by the X7sNetSession, however the application
* can change it, before calling to Start()
*/
int X7sNetSession::SetupRTP() {
	int ret=X7S_NET_RET_OK;
	if(IsAlive(0)) {

		if(ctrlr) ret = ctrlr->SetGetApplyParam(S7P_CMD_PARAM_SET,S7P_PRM_BRD_PORT_RTP,board->pval[S7P_PRM_BRD_PORT_RTP],CAMID_ALL);
		if(ret!=X7S_NET_RET_OK) return ret;

		//Send the SSRCID
		if(ctrlr) ret = ctrlr->SetGetApplyParam(S7P_CMD_PARAM_SET,S7P_PRM_BRD_SSRCID, board->pval[S7P_PRM_BRD_SSRCID],CAMID_ALL);
		if(ret!=X7S_NET_RET_OK) return ret;

		//By default enable camera 1 and disable others
		if(board)
		{
			board->cam[CAMID_1]->pval[S7P_PRM_CAM_ENABLE]=1;
			board->cam[CAMID_2]->pval[S7P_PRM_CAM_ENABLE]=0;
			board->cam[CAMID_3]->pval[S7P_PRM_CAM_ENABLE]=0;
			board->cam[CAMID_4]->pval[S7P_PRM_CAM_ENABLE]=0;
		}

		//Get Metadatype
		for(int i=0;i<X7S_NET_BOARD_NCAMERA;i++) {
			if(ctrlr && board) ret = ctrlr->SetGetApplyParam(S7P_CMD_PARAM_SET,S7P_PRM_CAM_ENABLE, board->cam[i]->pval[S7P_PRM_CAM_ENABLE],i);
			if(ret!=X7S_NET_RET_OK) return ret;
			if(ctrlr && board) ret = ctrlr->SetGetApplyParam(S7P_CMD_PARAM_GET,S7P_PRM_CAM_MTYPE, board->cam[i]->pval[S7P_PRM_CAM_MTYPE],i);
			if(ret!=X7S_NET_RET_OK) return ret;
		}

		is_setup=true;
		return X7S_NET_RET_OK;
	}
	return X7S_NET_RET_UNKOWN;
}


/**
* @brief Setup the buffer and datatype for S7P data transmission
*/
int X7sNetSession::SetupS7P() {
	return X7S_NET_RET_OK;
}

/**
* @brief Return the C-String that correspond to the error code.
*/
const char* X7sNetSession::GetErrorMsg(int code)
{
	switch(code)
	{
	case X7S_NET_RET_OK: return "OK";
	case X7S_NET_RET_UNKOWN: return "UNKOWN";
	case X7S_NET_RET_SOCKETCLOSED: return "SOCKET_CLOSED";
	case X7S_NET_RET_TIMEOUT: return "TIMEOUT";
	case X7S_NET_RET_NOTFOUND: return "NOT_FOUND";
	case X7S_NET_RET_READONLY: return "READ_ONLY";
	case X7S_NET_RET_OUTRANGE: return "OUT_OF_RANGE";
	case X7S_NET_RET_LOSTPKT: return "LOST_PKT";
	case X7S_NET_RET_ONSENDING: return "ON_SENDING";
	case X7S_NET_RET_BADMDATA: return "BAD_MDATA";
	default: return "BAD_ERROR_CODE";
	}
}



int X7sNetSession::GetNumErrors(int type, bool reset)
{
	return errors->GetNumErrors(type,reset);
}


int X7sNetSession::SetReceiverTimeOut(int nof_milli)
{

	if(drcvr==NULL) return X7S_NET_RET_NOTFOUND;
	if(nof_milli > 10000) return X7S_NET_RET_OUTRANGE;
	if(drcvr->SetSocketTimeOut(nof_milli)==false) return X7S_NET_RET_UNKOWN;
	else
	{
		return X7S_NET_RET_OK;
	}
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Private Function.
//----------------------------------------------------------------------------------------

/**
* @brief Stay in a loop during n seconds.
* @warning This function must not be used, it is actually just for testing purpose.
* @deprecated since version 0.9.1
*/
void wait ( int seconds )
{
	g_killwait=FALSE;
	clock_t endwait;
	endwait = clock () + seconds * CLOCKS_PER_SEC ;
	while (clock() < endwait)
	{
		if(g_killwait) break;
	}
}


