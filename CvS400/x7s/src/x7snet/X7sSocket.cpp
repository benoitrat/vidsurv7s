#include "X7sSocket.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sNetTools.hpp"	//X7S_PRINT_xxx,...


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor and Initialization.
//---------------------------------------------------------------------------------------

/**
 * @brief Default constructor
 */
X7sSocket::X7sSocket(int socketType)
: socketType(socketType), state(X7S_NET_SOCKSTATE_NULL), timeout_rcv(-1), timeout_snd(-1)
{
}

/*
 * @brief Default destructor.
 */
X7sSocket::~X7sSocket()
{
	X7S_PRINT_DEBUG("X7sSocket::~X7sSocket()","");
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Set/Get State function
//----------------------------------------------------------------------------------------

/**
 * @brief Return if the Socket is Ok, this mean not close or not NULL.
 */
bool X7sSocket::IsOk() {
	return (state!=X7S_NET_SOCKSTATE_CLOSED && state!=X7S_NET_SOCKSTATE_NULL);
}

/**
 * @brief This function set a timeout for the socket received message.
 * @note By default this function return false, this mean that no timeout has been configured.
 */
bool X7sSocket::SetRcvTimeOut(int nof_milli) {
	return false;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Recv/Send function
//----------------------------------------------------------------------------------------

/**
 * @brief Discard all bytes in the incoming queue.
 * @return the number of discarded bytes.
 */
int X7sSocket::Discard() {
	int bcount=0, brcv;
	uint8_t buff[1500];
	do {
		brcv=this->RecvPkt(buff,1500);
		bcount+=brcv;
	}
	while(brcv>0);
	return bcount;
}


int X7sSocket::SendPktChk(uint8_t *buffer, const int& nBytes) {
	if(buffer == NULL || IsOk()==false) {
		X7S_PRINT_ERROR("X7sSocket::SendPktChk()");
		return X7S_NET_SOCKERR_ONSND;
	}
	return SendPkt(buffer,nBytes);
}

/**
 * @brief Receive a packet through a socket and check errors.
 *
 *
 * @see S4NSocketUDP::RecvPkt()
 */
int X7sSocket::RecvPktChk(uint8_t *buffer, const int& nBytes) {
	if(buffer == NULL || IsOk()==false) {
		X7S_PRINT_ERROR("X7sSocket::RecvPktChk()","buffer=%X or IsOk=%d",buffer,IsOk());
		return X7S_NET_SOCKERR_ONRCV;
	}
	return RecvPkt(buffer,nBytes);
}


/**
 * @brief First send a packet, and then wait until receiving a reply or a timeout.
 *
 * This function is typically used with short message such as in ICMP protocol, DHCP protocol,...
 * In our case we use it to all the message used by the Real Time Stream Protocol (RTSP).
 *
 *	@param buff_snd A pointer on the packet's data we need to send.
 *	@param nB_snd The number of byte sent.
 *	@param buff_rcv A pointer on the memory where we are going to write the data of the received packet.
 *	@param nB_rcv The number of byte that we expect to write.
 *	@param timeout The time we wait for the reception of a packet.
 *	@param nof_tries The number of tries we perform this protocol, by default its only one.
 *	@return The size of the message received if there is no error, otherwise an X7sSocketError.
 *
 */
int X7sSocket::SendWaitReply(uint8_t *buff_snd,const int& nB_snd, uint8_t *buff_rcv, const int& nB_rcv, int timeout, int nof_tries) {
	bool is_t_out_ok;
	int ret, size, old_t_out_rcv;

	size =0;
	ret=X7S_NET_SOCKERR_TIMEOUT;
	old_t_out_rcv = timeout_rcv;
	is_t_out_ok = this->SetRcvTimeOut(timeout);

	for(int i=0;i<nof_tries;i++) {
		size = this->SendPktChk(buff_snd,nB_snd);
		if(size > 0) { //If more then 0 bytes were sent.

			size = this->RecvPktChk(buff_rcv,nB_rcv);
			if(size > 0) {	//If more than 0 byte are received
				return size; //Return with the size of the packet received...
			}
			else {
				X7S_PRINT_INFO("X7sSocket::SendWaitReply()","Timeout (%d): Try #%d...",timeout,i);
			}
		}
		else {
			ret=X7S_NET_SOCKERR_ONSND;
		}
	}
	return ret;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Debug message.
//----------------------------------------------------------------------------------------


/**
 * @brief The debug message of the class.
 * @note This message can be used only when the symbol
 * __WXX7S_LOGLEVEL__ is defined during compilation.
 */
std::string X7sSocket::toString() {
	std::stringstream sstr;
	std::string str_type;
	std::string str_state;
	switch(socketType) {
	case X7S_NET_SOCKTYPE_UDP: 	str_type="UDP"; break;
	case X7S_NET_SOCKTYPE_TCP:	str_type="TCP"; break;
	case X7S_NET_SOCKTYPE_MAC:	str_type="MAC"; break;
	}
	switch(state) {
	case X7S_NET_SOCKSTATE_INIT:		str_state="Init"; 		break;
	case X7S_NET_SOCKSTATE_OPENED:	str_state="Opened"; 	break;
	case X7S_NET_SOCKSTATE_BINDED:	str_state="Binded"; 	break;
	case X7S_NET_SOCKSTATE_CLOSED:	str_state="Close"; 		break;
	case X7S_NET_SOCKSTATE_CONNECTED:str_state="Connected"; 	break;
	case X7S_NET_SOCKSTATE_NULL:		str_state="Unknown"; 	break;
	}

	sstr << "type=" << str_type << ",state=" << str_state << " ";
	sstr << "timeout [snd,rcv]=[" << timeout_snd << "," << timeout_rcv << "] ";
	return sstr.str();
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Static function (tools)
//----------------------------------------------------------------------------------------

/**
 * @brief Convert 4 network bytes into an unsigned int in little endian.
 * @param p A pointer on 4 network bytes.
 * @return a 32-bit in little endian (x86).
 */
uint32_t X7sSocket::pbyte_ntohl(const uint8_t* p) {
        uint32_t ret=0;
        ret+= (p[0] << 24);
        ret+= (p[1] << 16);
        ret+= (p[2] << 8);
        ret+= p[3];
        return ret;
}

/**
 * This function maybe not usefull.
 */
uint32_t X7sSocket::pbyte_to32(const uint8_t *p) {
        uint32_t ret=0;
        ret+= (p[3] << 24);
        ret+= (p[2] << 16);
        ret+= (p[1] << 8);
        ret+= p[0];
     //   printf("0x%02X 0x%02X 0x%02X 0x%02X >> 0x%08X\n",p[0],p[1],p[2],p[3],ret);
        return ret;
}

/**
 * @brief Convert a frame buffer in 32-bits big endian to a buffer in 32-bits little endian.
 *
 * This operation can be performed on the same memory because
 * the byte swapping is done with a temporary memory.
 *
 * @warning If value are encoded in one bytes, reversing the endianess
 * change the order of the bytes value. This function may be used only with
 * 32-bits value.
 *
 * @param p_be Pointer on a constant big endian buffer (network)
 * @param p_le Pointer on little endian buffer (PC/x86)
 * @param bsize The size of the buffer in bytes. If this value is not multiple
 * of 4, we removed the last bytes.
 *
 */
void X7sSocket::buff_ntohl(uint8_t *p_le, const uint8_t *p_be, int bsize) {
        uint32_t tmp;
        bsize -= bsize%4; //Remove the bytes that can't make a 32 words.
        const uint8_t* end = p_be + bsize;
        while(p_be< end) {
                tmp = pbyte_ntohl(p_be);
                memcpy(p_le,&tmp,4);
                p_be+=4;
                p_le+=4;
        }
}

/**
 * @brief Convert from a buffer in 32-bits little endian to a buffer in 32-bits big endian.
 *
 *
 * @param p_le Pointer on a constant little endian buffer (PC/x86)
 * @param p_be Pointer on big endian buffer (network)
 * @param bsize The size of the buffer in bytes. If this value is not multiple
 * of 4, we removed the last bytes.
 *
 * @see X7sSocket::buff_ntohl
 */
void X7sSocket::buff_htonl(uint8_t *p_be, const uint8_t *p_le, int bsize) {
	X7sSocket::buff_ntohl(p_be,p_le,bsize);
}
