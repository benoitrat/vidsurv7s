/**
 *  @file
 *  @brief Contains the class X7sSocketMAC.hpp
 *  @date 12-nov-2008
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7S_NET_SOCKETMAC_HPP_
#define X7S_NET_SOCKETMAC_HPP_

#include "X7sSocket.hpp"

#ifndef _BITTYPES_H
#define _BITTYPES_H

#ifndef HAVE_U_INT8_T

#if SIZEOF_CHAR == 1
typedef unsigned char u_int8_t;
#endif
#define HAVE_U_INT8_T 1
#define HAVE_INT8_T 1

#endif /* HAVE_U_INT8_T */

#ifndef HAVE_U_INT16_T

#if SIZEOF_SHORT == 2
typedef unsigned short u_int16_t;
#elif SIZEOF_INT == 2
typedef unsigned int u_int16_t;
#elif SIZEOF_CHAR == 2
typedef unsigned char u_int16_t;
#else  /* XXXX */
#error "there's no appropriate type for u_int16_t"
#endif
#define HAVE_U_INT16_T 1
#define HAVE_INT16_T 1

#endif /* HAVE_U_INT16_T */

#ifndef HAVE_U_INT32_T

#if SIZEOF_INT == 4
typedef unsigned int u_int32_t;
#elif SIZEOF_LONG == 4
typedef unsigned long u_int32_t;
#elif SIZEOF_SHORT == 4
typedef unsigned short u_int32_t;
#else  /* XXXX */
#error "there's no appropriate type for u_int32_t"
#endif
#define HAVE_U_INT32_T 1
#define HAVE_INT32_T 1

#endif /* HAVE_U_INT32_T */

#ifndef HAVE_U_INT64_T
#if SIZEOF_LONG_LONG == 8
typedef unsigned long long u_int64_t;
#elif defined(_MSC_EXTENSIONS)
typedef unsigned _int64 u_int64_t;
#elif SIZEOF_INT == 8
typedef unsigned int u_int64_t;
#elif SIZEOF_LONG == 8
typedef unsigned long u_int64_t;
#elif SIZEOF_SHORT == 8
typedef unsigned short u_int64_t;
#else  /* XXXX */
#error "there's no appropriate type for u_int64_t"
#endif

#endif /* HAVE_U_INT64_T */

#ifndef PRId64
#ifdef _MSC_EXTENSIONS
#define PRId64	"I64d"
#else /* _MSC_EXTENSIONS */
#define PRId64	"lld"
#endif /* _MSC_EXTENSIONS */
#endif /* PRId64 */

#ifndef PRIo64
#ifdef _MSC_EXTENSIONS
#define PRIo64	"I64o"
#else /* _MSC_EXTENSIONS */
#define PRIo64	"llo"
#endif /* _MSC_EXTENSIONS */
#endif /* PRIo64 */

#ifndef PRIx64
#ifdef _MSC_EXTENSIONS
#define PRIx64	"I64x"
#else /* _MSC_EXTENSIONS */
#define PRIx64	"llx"
#endif /* _MSC_EXTENSIONS */
#endif /* PRIx64 */

#ifndef PRIu64
#ifdef _MSC_EXTENSIONS
#define PRIu64	"I64u"
#else /* _MSC_EXTENSIONS */
#define PRIu64	"llu"
#endif /* _MSC_EXTENSIONS */
#endif /* PRIu64 */

#endif /* _BITTYPES_H */


#include <pcap.h>
#include <cstdio>
#include <cstring>
#include <cstdlib>

#ifdef WIN32 /* If you use windows */
#include <windows.h>
#include <iphlpapi.h>
#elif defined (linux) /* if you are under linux */
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#endif


#ifndef PCAP_OPENFLAG_PROMISCUOUS
#define PCAP_OPENFLAG_PROMISCUOUS 1
#endif

#define X7S_NET_MAC_S7PPROTO 18000
#define X7S_NET_MAC_EHASIZE 6			//!< Size in bytes of EHA
#define X7S_NET_MAC_MTU	1500			//!< Size in bytes of
#define X7S_NET_MAC_HDRSIZE 16			//!< Size in byte of MAC header
#define X7S_NET_MAC_PAYLOAD_MSIZEIB		X7S_NET_MAC_MTU - X7S_NET_MAC_HDRSIZE

/**
 *	@brief The MAC socket class used in X7sSocket connection.
 *
 *  This class needs an remote IP and port to connect to a board.
 *  It may also works in listening mode if we give only the local port.
 *
 *
 *	@ingroup net
 */
class X7sSocketMAC: public X7sSocket {
public:

	X7sSocketMAC(const uint8_t *eha_local, const uint8_t *eha_remote, uint16_t proto=X7S_NET_MAC_S7PPROTO);
	virtual ~X7sSocketMAC();

	virtual int Bind();
	virtual int Open();
	virtual int Close();
	virtual int SendPkt(uint8_t *buffer, const int& nBytes);
	virtual int RecvPkt(uint8_t *buffer, const int& nBytes);
	virtual int RecvPktChk(uint8_t *buffer, const int& nBytes);
	virtual bool SetRcvTimeOut(int nof_milli);
	virtual bool SetRcvBufferSize(int buffsize);
	virtual int GetRcvBufferSize();

	virtual std::string toString();


protected:

	static int count_socket;
	static std::string GetErrNoDesc();


private:

	int m_flags;
	char errbuf[PCAP_ERRBUF_SIZE];
	struct pcap_pkthdr *header;
	struct bpf_program fp;      /* hold compiled program     */
	pcap_t *adhandle;
	int timeout;	//If 0 we are in blocking mode.

	const uint8_t *pktbuff;

	uint8_t *eha_remote;	//!< Remote Ethernet Hardware Address (pointing on packet buffer)
	uint8_t *eha_local;		//!< Local Ethernet Hardware Address (pointing on packet buffer)
	uint16_t *proto;		//!< Value of the ethernet protocol
	uint8_t *p_payload;		//!< Point on starting data.


	//For using select.
	fd_set m_readfs;
	struct timeval m_tvto;	//!< Timeval for timeout.

};

#endif /* X7S_NET_SOCKETMAC_HPP_ */
