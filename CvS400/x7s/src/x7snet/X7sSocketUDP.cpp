#include "X7sSocketUDP.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sNetTools.hpp"	//X7S_PRINT_xxx,...
#include "x7snet.h"			//To use X7sNet_GetIP()

//Function statically and privately
SOCKADDR_IN GetSockAddr(uint32_t ip, uint32_t port);
std::string SockAddrToStr(const SOCKADDR_IN& sin);


#ifdef WIN32
#define INIT_M_FLAGS 0
WSADATA wsa;
#else
#define INIT_M_FLAGS MSG_DONTWAIT
#endif

#define IX7S_NET_PKT_MTU 1500

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Static variable Initialization.
//---------------------------------------------------------------------------------------
bool X7sSocketUDP::is_initwsa=false;
int X7sSocketUDP::count_socket=0;


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor and Initialization.
//---------------------------------------------------------------------------------------

/**
* @brief The constructor of a UDP Socket.
*
* By default the socket are initiate in blocking mode (rcvtimeout=-1,sndtimeout=-1)
*
* @note In case you want to receive information only the port of the local IP is important,
* the other field will be erased.
*/
X7sSocketUDP::X7sSocketUDP(uint32_t ip_remote, uint16_t port_remote)
:X7sSocket(X7S_NET_SOCKTYPE_UDP), m_flags(INIT_M_FLAGS)
 {
	sin_local=GetSockAddr(X7S_NET_SOCK_ANYIP,X7S_NET_SOCK_ANYPORT);
	sin_remote=GetSockAddr(ip_remote, port_remote);
	state = X7S_NET_SOCKSTATE_INIT;

 }



X7sSocketUDP::X7sSocketUDP(uint16_t port_local, uint32_t ip_remote, uint16_t port_remote)
:X7sSocket(X7S_NET_SOCKTYPE_UDP), m_flags(INIT_M_FLAGS)
 {
	sin_local=GetSockAddr(X7S_NET_SOCK_ANYIP,port_local);
	sin_remote=GetSockAddr(ip_remote, port_remote);
	state = X7S_NET_SOCKSTATE_INIT;
 }

/**
* @brief Try to close the socket and them delete the object.
*/
X7sSocketUDP::~X7sSocketUDP() {
	X7S_PRINT_DEBUG("~X7sSocketUDP()","%s",toString().c_str());
	//First try to close the socket.
	if(state!=X7S_NET_SOCKSTATE_CLOSED && state!=X7S_NET_SOCKSTATE_INIT) {
		this->Close();
	}
}

/**
* @brief Call and initiate the DLL to use Windows Sockets API (WSA).
* @note This functions does nothing on UNIX system.
*/
void X7sSocketUDP::initWSA() {
#ifdef WIN32
	if(!X7sSocketUDP::is_initwsa) {
		int err = WSAStartup(MAKEWORD(2, 2), &wsa);
		if(err < 0)
		{
			perror("WSAStartup failed !");
			exit(EXIT_FAILURE);
		}
		X7sSocketUDP::count_socket=0;
		X7sSocketUDP::is_initwsa=true;
	}
	X7sSocketUDP::count_socket++;
#endif
}

/**
* @brief Cleanup the WSA dll when the sockets are no longer usefull.
*
* The number of socket opened is count to call this function when the
* last socket is destroy.
*
* @note This functions does nothing on UNIX system.
*/
void X7sSocketUDP::endWSA() {
#ifdef WIN32
	if(X7sSocketUDP::is_initwsa && X7sSocketUDP::count_socket<=1) {
		WSACleanup();
		X7sSocketUDP::is_initwsa=false;
	}
	X7sSocketUDP::count_socket--;
#endif
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Functions.
//---------------------------------------------------------------------------------------


/**
* @brief Open the UDP Socket connection.
*
* This function need to be call for Client and Server.
*
* @return X7S_NET_SOCKERR_NOERROR or the type of error.
*/
int X7sSocketUDP::Open() {
	X7sSocketUDP::initWSA();
	m_socket = socket(AF_INET, SOCK_DGRAM, 0);	//Socket Diagram for UDP.
	if(m_socket == INVALID_SOCKET)
	{
		X7S_PRINT_ERROR("X7sSocketUDP::Open()");
		return X7S_NET_SOCKERR_ONOPEN;
	};
	X7S_PRINT_DEBUG("X7sSocketUDP::Open()");
	state=X7S_NET_SOCKSTATE_OPENED;
	return X7S_NET_SOCKERR_NOERROR;
}

/**
* @brief Open and Listen on a UDP Socket.
*
* This socket is mainly in server mode
* (waiting to receive some data from another computer).
*
* @return X7S_NET_SOCKERR_NOERROR or the type of error.
*/
int X7sSocketUDP::Bind() {

	//First open the socket.
	int ret= this->Open();

	if(ret!=X7S_NET_SOCKERR_NOERROR) return ret;

	//Then bind to a specific port (Defined by the sin_local address).
	if(bind(m_socket, (SOCKADDR *) &(sin_local),sizeof(sin_local)) == SOCKET_ERROR)  {
		X7S_PRINT_ERROR("X7sSocketUDP::Bind()","%s (sin_local=%s)",GetErrNoDesc().c_str(),SockAddrToStr(sin_local).c_str());
		return X7S_NET_SOCKERR_ONBIND;
	}

	state=X7S_NET_SOCKSTATE_BINDED;
	return X7S_NET_SOCKERR_NOERROR;
}

/**
* @brief Close the UDP Socket connection.
*/
int X7sSocketUDP::Close() {
	int ret = closesocket(m_socket);
	if(ret==0) {
		state=X7S_NET_SOCKSTATE_CLOSED;
		X7S_PRINT_INFO("X7sSocketUDP::Close()", "ret=%d, %s",ret,toString().c_str());
		this->endWSA(); //Try to close the WSA for the last socket.
		return X7S_NET_SOCKERR_NOERROR;
	}
	else {
		X7S_PRINT_ERROR("X7sSocketUDP:Close",GetErrNoDesc().c_str());
		return X7S_NET_SOCKERR_MISC;
	}
}

/**
* @brief Flush all the packet already received on this socket before sending one.
*/
int X7sSocketUDP::Flush()
{
	int ret=X7S_NET_RET_SOCKETCLOSED;

	if(m_socket)
	{
		int nBytes=IX7S_NET_PKT_MTU;
		uint8_t buffer[IX7S_NET_PKT_MTU];
		socklen_t size = sizeof(sin_remote);
		int nBytesRead;
		do
		{
			nBytesRead=recvfrom(m_socket,(char*)buffer, nBytes, MSG_DONTWAIT,(SOCKADDR *)&(sin_remote),&size);
		} while(nBytesRead>0);

		return nBytesRead;
	}
	return ret;
}


/**
* @brief Send a packet through a UDP socket..
* The buffer and the nBytes should be correctly assigned.
*
* @param buffer The pointer on the beginning of the buffer memory.
* @param nBytes The size in bytes of the data expected to be written in the packet.
* @return Upon successful completion, sendto() shall return the number of bytes sent.
* Otherwise, -1 shall be returned and errno set to indicate the error.
*/
int X7sSocketUDP::SendPkt(uint8_t *buffer, const int& nBytes)  {
	return sendto(m_socket,(const char*)buffer, nBytes,0,(SOCKADDR *)&(sin_remote),sizeof(sin_remote));
}

/**
* @brief Send a packet through a UDP socket (And check arguments and error)
*
* This function take and returns the same argument of @ref X7sSocketUDP::SendPkt(),
* however if an error occurs a lot of error message are displayed.
*
* @see X7sSocketUDP::SendPkt()
*/
int X7sSocketUDP::SendPktChk(uint8_t *buffer, const int& nBytes)  {
	//Check if everything is okay before sending
	if(buffer == NULL || IsOk()==false || nBytes <= 0)
	{
		X7S_PRINT_ERROR("X7sSocket::SendPktChk()","buffer=%s, nBytes=%d,IsOk=%d",buffer,nBytes,IsOk());
		return X7S_NET_SOCKERR_ONSND;
	}

	//Send the packet
	int snd_bytes=SendPkt(buffer,nBytes);

	//Check if sending as been successful
	if(snd_bytes <= 0)
	{
		X7S_PRINT_WARN("X7sSocketUDP::SendPkt()","%s",GetErrNoDesc().c_str());
	}

	//Return the number of bytes send
	return snd_bytes;
}


/**
* @brief Receive a packet through a UDP socket.
* The buffer and the nBytes should be correctly assigned.
*
* @param buffer The pointer on the beginning of the buffer memory.
* @param nBytes The size in bytes of the encapsulated data in the S4NPacket.
* @return The size in byte of the packet received. Otherwise we can return:
* 		-  0 if no packet have being read in blocking mode.
* 		- -1 if a timeout occurs in blocking mode.
*/
int X7sSocketUDP::RecvPkt(uint8_t *buffer, const int& nBytes) {


	//On linux the timeval is undefined after a call of select so we redefine it here.
	struct timeval *p_tvto;
	if(timeout_rcv>=0)
	{
		struct timeval tvto;
		tvto.tv_sec=m_tvto.tv_sec;
		tvto.tv_usec=m_tvto.tv_usec;
		p_tvto=&tvto;
	}
	else p_tvto=NULL;


	FD_ZERO(&m_readfs);
	FD_SET(m_socket, &m_readfs);
	int ret=select(m_socket + 1, &m_readfs, NULL, NULL, p_tvto);

	if(ret>0) {
		if(FD_ISSET(m_socket,&m_readfs))
		{
			socklen_t size = sizeof(sin_remote);
			return recvfrom(m_socket,(char*)buffer, nBytes, m_flags,(SOCKADDR *)&(sin_remote),&size);
		}
		X7S_PRINT_ERROR("X7sSocketUDP::RecvPkt()","FD_ISSET is false");
	}
	return -1;
}

/**
* @brief Receive packet through a UDP socket and check errors.
* @see X7sSocketUDP::RecvPkt() and X7sSocket::RecvPktChk().
*/
int X7sSocketUDP::RecvPktChk(uint8_t *buffer, const int& nBytes) {
	X7S_FUNCNAME("X7sSocketUDP::RecvPktChk()");

	X7sNetChrono chrono;
	int ret = X7sSocket::RecvPktChk(buffer,nBytes);
	if(ret < 0) {
#ifdef WIN32
		X7S_PRINT_DEBUG(funcName,"%d > EAGAIN (Timeout: Try again) = 11 (elapsed %d ms)",ret,chrono.Time());
#else
		X7S_PRINT_DEBUG(funcName,"%d > %s (elapsed %d ms)",ret,X7sSocketUDP::GetErrNoDesc().c_str(),chrono.Time());
#endif
	}
	return ret;
}

/**
* @brief Set timeout of the socket.
*
* The socket can have different behaviour according to the value of the timeout.
* 	- nof_milli=[1,INT_MAX] -> When the socket enter use recv() or recvfrom().
* 	- nof_milli=0			-> The socket don't wait and directly return from the calling function.
* 	- nof_milli=-1			-> The socket WAITALL packet until a message arrive.
*
* @param nof_milli time of timeout in milliseconds.
* @return @true if the timeout has been set correctly, @false otherwise.
* @warning In windows it seems that the SO_RCVTIMEO options is not working.
* Therefore we have decided to use a select() method in the
* X7sSocketUDP::RecvPkt() function.
*
*/
bool X7sSocketUDP::SetRcvTimeOut(int nof_milli) {

	//We set the default timeval structure, also used by select().
	m_tvto.tv_sec = (int)(((double)nof_milli)/1000.0);
	m_tvto.tv_usec = (nof_milli%1000)*1000;
	timeout_rcv = nof_milli;	//Change value of rcv timeout.

	//Set the socket in non-blocking!
#ifdef WIN32
	// Set the socket I/O mode; iMode = 0 for blocking; iMode != 0 for non-blocking
	u_long iMode = 1;
	ioctlsocket(m_socket, FIONBIO,&iMode);
	m_flags = 0;
#else
	m_flags=MSG_DONTWAIT;
#endif

	return true;
}

/**
* @brief Obtain the size of the receiving buffer for a socket.
* @note In linux, the value returned by the system is normally the double than
* the one setted by the system. In this function we have decided to return the same value
* under windows and linux. Therefore the value of buffsize in unix, is divide by two before
* being returned.
* @see General documentation on UDP buffer: http://www.29west.com/docs/THPM/udp-buffer-sizing.html
* @see http://msdn.microsoft.com/en-us/library/ms738544(VS.85).aspx
*
*/
int X7sSocketUDP::GetRcvBufferSize() {
	int buffsize=0;
#if WIN32
	int s_t=sizeof(buffsize);
#else
	socklen_t s_t = sizeof(buffsize);
#endif

	if(getsockopt(m_socket,SOL_SOCKET,SO_RCVBUF,(char*)(&buffsize),&s_t)==-1) {
		X7S_PRINT_ERROR("X7sSocketUDP::GetRcvBufferSize()",GetErrNoDesc().c_str());
		return -1;
	}
#ifndef WIN32
	buffsize/=2;	//Divide buffsize by two for linux system
#endif
	return buffsize;
}

/**
* @brief Set the size of the receiving buffer for a socket.
* @note By default the size of the receiving buffer depends on the system:
* 		- In windows:		8Kb (8192b)
* 		- In unix,linux: 	55Kb (56320b)
*/
bool X7sSocketUDP::SetRcvBufferSize(int buffsize) {
	if(setsockopt(m_socket,SOL_SOCKET,SO_RCVBUF,(char*)(&buffsize),sizeof(buffsize))==-1) {
		X7S_PRINT_ERROR("X7sSocketUDP::SetRcvBufferSize()",GetErrNoDesc().c_str());
		return false;
	}
	return true;
}



/**
* @brief Set the flag of the function recvfrom() and sendto().
*
* With the winsocket API, the value of flags can take only value
* MSG_PEEK and/or MSG_OOB. These behavior are bad and should not be
* used, so we set m_flags=0 for windows API.
*
* @see http://tangentsoft.net/wskfaq/newbie.html
* @see http://msdn.microsoft.com/en-us/library/ms740120(VS.85).aspx
*/
void X7sSocketUDP::SetFlags(int flag) {

	//	u_long iMode=0;
	//
	//
	//	switch(flag) {
	//	//Timeout socket and blocking socket.
	//	case X7S_NET_SOCKFLAG_TIMEDOUT:
	//	case X7S_NET_SOCKFLAG_WAITALL:
	//#ifdef WIN32
	//		// Set the socket I/O mode; iMode = 0 for blocking; iMode != 0 for non-blocking
	//		iMode = 0;
	//		ioctlsocket(m_socket, FIONBIO,&iMode);
	//		m_flags=0;
	//#else
	//		m_flags=MSG_WAITALL;
	//		iMode=0;
	//#endif
	//		break;
	//
	//		//Non blocking socket...
	//	case X7S_NET_SOCKFLAG_NOWAIT:
	//#ifdef WIN32
	//		// Set the socket I/O mode; iMode = 0 for blocking; iMode != 0 for non-blocking
	//		iMode = 1;
	//		ioctlsocket(m_socket, FIONBIO,&iMode);
	//		m_flags = 0;
	//#else
	//		m_flags = MSG_DONTWAIT;
	//		iMode=0;
	//#endif
	//		break;
	//
	//
	//		//Waiting socket (Like RTP).
	//	}
}

std::string X7sSocketUDP::GetRemoteAddr() const {
	return SockAddrToStr(sin_remote);
}



/**
* @brief The debug message of the class.
* @note This message can be used only when the symbol
* __WXX7S_LOGLEVEL__ is defined during compilation.
*/
std::string X7sSocketUDP::toString() {
	std::stringstream sstr;
	sstr << X7sSocket::toString();
	sstr << ">>";
	if(this->IsOk()) {
		sstr << " (rcv_buff=" << this->GetRcvBufferSize() << "), ";
	}
	sstr << SockAddrToStr(sin_local);

	if(this->state==X7S_NET_SOCKSTATE_BINDED) sstr << " <- ";
	else sstr << " -> ";

	sstr << SockAddrToStr(sin_remote);
	//printf("%s\n",sstr.str().c_str());
	return sstr.str();
}

/**
* @brief Return the last error of the socket as a string.
*/
std::string X7sSocketUDP::GetErrNoDesc() {
	std::stringstream sstr;
#ifdef WIN32
	switch(WSAGetLastError())
	{
	case WSANOTINITIALISED:	sstr << "WSANOTINITIALISED (A successful WSAStartup call must occur before using this function.)"; break;
	case WSAENETDOWN:  sstr <<"WSAENETDOWN (The network subsystem has failed.)"; break;
	case WSAEACCES: sstr << "WSAEACCES (Attempt to connect datagram socket to broadcast address failed because setsockopt option SO_BROADCAST is not enabled)."; break;
	case WSAEADDRINUSE: sstr << "WSAEADDRINUSE (A process on the computer is already bound to this address)";break;
	case WSAEADDRNOTAVAIL: sstr << "WSAEADDRNOTAVAIL (The specified address is not a valid address for this computer.)";break;
	case WSAEFAULT: sstr << "WSAEFAULT (The name or namelen parameter is not a valid part of the user address space, the namelen parameter is too small, the name parameter contains an incorrect address format for the associated address family, or the first two bytes of the memory block specified by name does not match the address family associated with the socket descriptor s."; break;
	case WSAEINPROGRESS: sstr << "WSAEINPROGRESS (A blocking Windows Sockets 1.1 call is in progress, or the service provider is still processing a callback function."; break;
	case WSAEINVAL: sstr << "WSAEINVAL (The socket is already bound to an address.)"; break;
	case WSAENOBUFS: sstr << "WSAENOBUFS (Not enough buffers available, too many connections.)"; break;
	case WSAENOTSOCK: sstr << "WSAENOTSOCK (The descriptor is not a socket.)"; break;
	default: sstr << "UNKOWN =" << WSAGetLastError() << "(Unkown error code)"; break;
	}
#else
	switch(errno)
	{
	case EPERM: sstr << "EPERM (Operation not permitted)"; break;
	case ENOENT: sstr << "ENOENT (No such file or directory)"; break;
	case ESRCH: sstr << "ESRCH (No such process)"; break;
	case EINTR: sstr << "EINTR (Interrupted system call)"; break;
	case EIO: sstr << "EIO (I/O error)"; break;
	case ENXIO: sstr << "ENXIO (No such device or address)"; break;
	case E2BIG: sstr << "E2BIG (Argument list too long)"; break;
	case ENOEXEC: sstr << "ENOEXEC (Exec format error)"; break;
	case EBADF: sstr << "EBADF (Bad file number)"; break;
	case ECHILD: sstr << "ECHILD (No child processes)"; break;
	case EAGAIN: sstr << "EAGAIN (Timeout: Try again)"; break;
	case ENOMEM: sstr << "ENOMEM (Out of memory)"; break;
	case EACCES: sstr << "EACCES (Permission denied)"; break;
	case EFAULT: sstr << "EFAULT (Bad address)"; break;
	case EBUSY: sstr << "EBUSY (Device or resource busy)"; break;
	case EEXIST: sstr << "EEXIST (File exists)"; break;
	case EXDEV: sstr << "EXDEV (Cross-device link)"; break;
	case ENODEV: sstr << "ENODEV (No such device)"; break;
	case ENOTDIR: sstr << "ENOTDIR (Not a directory)"; break;
	case EISDIR: sstr << "EISDIR (Is a directory)"; break;
	case EINVAL: sstr << "EINVAL (Invalid argument)"; break;
	case ENFILE: sstr << "ENFILE (File table overflow)"; break;
	case EMFILE: sstr << "EMFILE (Too many open files)"; break;
	case ENOTTY: sstr << "ENOTTY (Not a typewriter)"; break;
	case EFBIG: sstr << "EFBIG (File too large)"; break;
	case ENOSPC: sstr << "ENOSPC (No space left on device)"; break;
	case ESPIPE: sstr << "ESPIPE (Illegal seek)"; break;
	case EROFS: sstr << "EROFS (Read-only file system)"; break;
	case EMLINK: sstr << "EMLINK (Too many links)"; break;
	case EPIPE: sstr << "EPIPE (Broken pipe)"; break;
	case EDOM: sstr << "EDOM (Math argument out of domain of func)"; break;
	case ERANGE: sstr << "ERANGE (Math result not representable)"; break;
	case ENOTBLK: sstr << "ENOTBLK (Block device required)"; break;
	case ETXTBSY: sstr << "ETXTBSY (Text file busy)"; break;
	default: sstr << "UNKOWN =" << errno << " (Unkown error code)"; break;
	}
#endif
	sstr << " ";
	return sstr.str();
}


//========================================================================================


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	IPv4 Functions.
//---------------------------------------------------------------------------------------
SOCKADDR_IN GetSockAddr(uint32_t ip, uint32_t port){
	SOCKADDR_IN saddr;
	saddr.sin_family=AF_INET;
	saddr.sin_port=htons(port);
#ifdef WIN32
	saddr.sin_addr.S_un.S_addr=htonl(ip); //host to network (long) bytes conversion
#else
	saddr.sin_addr.s_addr=htonl(ip);
#endif

	return saddr;

}

std::string SockAddrToStr(const SOCKADDR_IN& sin) {
	uint16_t port;
	uint32_t ip;


	port = ntohs(sin.sin_port);
#ifdef WIN32
	ip = ntohl(sin.sin_addr.S_un.S_addr); //host to network (long) bytes conversion
#else
	ip = ntohl(sin.sin_addr.s_addr);
#endif

	char tmp_ip[X7S_NET_IP_STRLENGTH];
	X7sNet_SetIP(ip,tmp_ip);

	char tmp[30];
	if(port==0) snprintf(tmp,30,"%s:ANY",tmp_ip);
	else snprintf(tmp,30,"%s:%d",tmp_ip,port);
	return std::string(tmp);
}

/**
* @brief Write a Unsigned 32 bits IP in a C-string
* @param ip The value of IP.
* @param ip_str A C-String previously reserved with at least @ref X7S_NET_IP_STRLENGTH characters.
*/
void X7sNet_SetIP(uint32_t ip,char *ip_str)
{
	int ret=0;
	if(ip_str) {
		uint8_t *tmp=(uint8_t*)&ip;
		ret=snprintf(ip_str,(size_t)X7S_NET_IP_STRLENGTH,"%d.%d.%d.%d",tmp[3],tmp[2],tmp[1],tmp[0]);
		X7S_CHECK_WARN("X7sNet_SetIP()",ret < (int)X7S_NET_IP_STRLENGTH,,
			"Buffer overflow (%d,%d) (snprintf), text is corrupted",ret,X7S_NET_IP_STRLENGTH);
	}
}

/**
* @brief Write a Unsigned 32 bits IP in an array of 4 bytes.
* @param ip The value of IP.
* @param ip_array An array of 4 uint8_t cells previously reserved.
*/
void X7sNet_SetIP(uint32_t ip,uint8_t *ip_array)
{
	if(ip_array) {
		uint8_t *tmp=(uint8_t*)&ip;
		ip_array[0] = tmp[3];
		ip_array[1] = tmp[2];
		ip_array[2] = tmp[1];
		ip_array[3] = tmp[0];
	}
}


/**
* @brief return the IP on 32-bits given a string such as "192.154.125.1".
* @note this function should use inet_ntop() but it is not available under WindowsXP.
*/
uint32_t X7sNet_GetIP(const char *ip_str) {
	uint32_t ip=0;
	int ip8=0;

	std::string tmp=ip_str;
	std::string sub;
	int pos=0;
	for(int i=0;i<4;i++) {
		pos=tmp.find_first_of('.'); //To look more than 4 char
		sub=tmp.substr(0,pos);
		tmp=tmp.substr(pos+1,tmp.size());	//And remove the old char.
		ip8 = atoi(sub.c_str());
		if(ip8<0 || 255<ip8) {
			X7S_PRINT_WARN("sub = %s; tmp=%s ",sub.c_str(),tmp.c_str());
		}
		ip = (ip << 8) + (ip8 & 0xFF);
	}
	return ip;
}
