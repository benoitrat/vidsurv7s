#include "X7sNetBoard.hpp"
#include <ctime>	//To give to random seed.
#include <cstdlib>	//To use random seed.
#include <X7sNetTools.hpp>


X7sNetCamera::X7sNetCamera()
{

	frame=NULL;
	is_new=false;

	//first set the pval
	pval=((uint32_t*)ar_param)-S7P_PRM_CAM_ENABLE;

	//Initialize all the value in pvalue with 0.
	memset(ar_param,0,sizeof(ar_param));

	//Then set the default (non-zeros) value for each parameters.
	pval[S7P_PRM_CAM_ENABLE]=false;
	pval[S7P_PRM_CAM_MTYPE]=S7P_MTYPE_NONE;
	pval[S7P_PRM_CAM_BG_THRS]=50;

}

/**
 * @brief Destroy the X7SNetFrame
 */
X7sNetCamera::~X7sNetCamera()
{
	X7S_PRINT_DEBUG("X7sNetCamera::~X7sNetCamera()","");
	x7sReleaseNetFrame(&frame);
}




/**
 *	@brief Structure to keep parameters of one board and its camera parameters.
 *	@see X7sNetCamera.
 */
X7sNetBoard::X7sNetBoard()
{
	//first set the pval
	pval=((uint32_t*)ar_param)-S7P_PRM_BRD_IP;

	//Init the seed for a valid random ID.
	srand((uint32_t)time(NULL)*(uint32_t)this);

	//Initialize all the value in pvalue with 0.
	memset(ar_param,0,sizeof(ar_param));

	//Then set the default value for each parameters.
	pval[S7P_PRM_BRD_IP]=0xFFFFFFF;
	pval[S7P_PRM_BRD_PORT_S7P]=18000;
	pval[S7P_PRM_BRD_DHCP_ON]=false;
	pval[S7P_PRM_BRD_MODES]=0;
	pval[S7P_PRM_BRD_DTYPE]=0;
	pval[S7P_PRM_BRD_PORT_RTP]=50000;
	pval[S7P_PRM_BRD_SSRCID]=(uint32_t)rand();
	pval[S7P_PRM_BRD_WSIZE]=6;
	pval[S7P_PRM_BRD_FRAMERATE]=0;

	//Setup the IP string
	X7sNet_SetIP(pval[S7P_PRM_BRD_IP],ip_str);

	for(int i=0;i<X7S_NET_BOARD_NCAMERA;i++) {
		cam[i]=new X7sNetCamera;
		cam[i]->ID=i;
	}

	//Set camera_1 enable
	cam[CAMID_1]->pval[S7P_PRM_CAM_ENABLE]=true;

};

/**
 * @brief Call the destructor of its corresponding camera.
 */
X7sNetBoard::~X7sNetBoard()
{
	uint32_t ip=pval[S7P_PRM_BRD_IP];
	X7S_PRINT_DEBUG("X7sNetBoard::~X7sNetBoard()","IP=%u.%u.%u,%u",
			(ip>>24) & 0xff,
			(ip>>16) & 0xff,
			(ip>>8) & 0xff,
			ip & 0xff);

	for(int i=0;i<X7S_NET_BOARD_NCAMERA;i++)
	{
		if(cam[i]) delete cam[i];
		cam[i]=NULL;
	}
}
