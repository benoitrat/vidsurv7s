/**
 *  @file
 *  @brief Contains the class X7sSocketUDP.hpp
 *  @date 12-nov-2008
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7S_NET_SOCKETUDP_HPP_
#define X7S_NET_SOCKETUDP_HPP_

#include "X7sSocket.hpp"

#define X7S_NET_SOCK_ANYIP 0
#define X7S_NET_SOCK_ANYPORT 0

#include <stdio.h>


#ifdef WIN32 /* If you use windows */

#include <Ws2tcpip.h> //Include InetPton()
#include <Winsock2.h>
#include <windows.h>
#ifndef errno
#define errno WSAGetLastError()
#endif
typedef int socklen_t;
typedef char* pbyte;
#define MSG_DONTWAIT 0
#ifndef MSG_WAITALL
#define MSG_WAITALL 0
#endif


#else

#include <errno.h>			//variable errno
#include <string.h>			//function memset
#include <stdlib.h>			//exit(),free(),...
#include <unistd.h> 		//close(),...
#include <netdb.h> 			//gethostbyname(),...
#include <asm/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket(s) close(s)
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;
typedef unsigned char* pbyte;

#endif



/**
 *	@brief The UDP socket class used in X7sSocket connection.
 *
 *  This class needs an remote IP and port to connect to a board.
 *  It may also works in listening mode if we give only the local port.
 *
 *  @see The creation of this socket is similar to this tutorial:
 *  http://broux.developpez.com/articles/c/sockets/?page=client-serveur#L5-4
 *  @see http://www.windowsecurity.com/whitepapers/Writing_UDPSOCK_DGRAM_applications.html
 *
 *	@ingroup net
 */
class X7sSocketUDP: public X7sSocket {
public:

	X7sSocketUDP(uint32_t ip_remote, uint16_t port_remote);
	X7sSocketUDP(uint16_t port_local, uint32_t ip_remote=X7S_NET_SOCK_ANYIP, uint16_t port_remote=X7S_NET_SOCK_ANYIP);
	virtual ~X7sSocketUDP();

	virtual int Bind();
	virtual int Open();
	virtual int Close();
	virtual int Flush();
	virtual int SendPkt(uint8_t *buffer, const int& nBytes);
	virtual int SendPktChk(uint8_t *buffer, const int& nBytes);
	virtual int RecvPkt(uint8_t *buffer, const int& nBytes);
	virtual int RecvPktChk(uint8_t *buffer, const int& nBytes);
	virtual bool SetRcvTimeOut(int nof_milli);
	virtual bool SetRcvBufferSize(int buffsize);
	virtual int GetRcvBufferSize();

	virtual std::string toString();
	virtual std::string GetRemoteAddr() const;

protected:

	static bool is_initwsa;
	static int count_socket;
	static std::string GetErrNoDesc();


private:
	static void initWSA();
	static void endWSA();
	void SetFlags(int flags);

	int m_flags;
	SOCKET m_socket;
	SOCKADDR_IN sin_local, sin_remote;

	//For using select.
	fd_set m_readfs;
	struct timeval m_tvto;	//!< Timeval for timeout.

};

#endif /* X7S_NET_SOCKETUDP_HPP_ */
