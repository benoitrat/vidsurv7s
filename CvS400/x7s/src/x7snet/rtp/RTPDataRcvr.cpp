#include "rtp/RTPDataRcvr.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------

#include "X7sNetTools.hpp"	//X7S_PRINT_xxx,...
#include "X7sSocket.hpp"
#include "X7sNetBoard.hpp"
#include "RTPJPEGData.hpp"
#include "RTPMetaData.hpp"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Prototype for internal private function.
//---------------------------------------------------------------------------------------
std::string printRTPHeader(int level,rtp_hdr_t *rtp_hdr);
int CopyRTPBuffer2Camera(RTPBufferMFrame *buff, X7sNetCamera *cam);

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor and Initialization.
//---------------------------------------------------------------------------------------

/**
* @brief Constructor of RTPRcvr.
*
* The constructor may also set the correct size to the X7sByteArray for
* each camera in X7sNetBoard.
*/
RTPDataRcvr::RTPDataRcvr(X7sSocket *sock,X7sNetBoard *board)
:X7sDataRcvr(X7S_NET_DRCVR_RTP,sock,board)
 {

	rtp_hdr= new rtp_hdr_t;
	rtp_hdr->ts=0;
	this->rtpmframe=NULL;

	//Then create the temporary memory to receive the frame.
	for(int i=0;i<X7S_NET_BOARD_NCAMERA;i++) {
		ar_rtpmframe[i]=new RTPBufferMFrame;
		ar_rtpmframe[i]->ID=i;
		ar_rtpmframe[i]->jpeg= new RTPJPEGData(X7S_NET_RTP_MSIZEIB_JPG);
		ar_rtpmframe[i]->mdata=new RTPMetaData(X7S_NET_RTP_MSIZEIB_META);
		ar_rtpmframe[i]->state=X7S_NET_RTPFRAME_PARTIAL;
		ar_rtpmframe[i]->need_mdata=false;
		ar_rtpmframe[i]->ts=0;
	}
 }

RTPDataRcvr::~RTPDataRcvr() {
	X7S_PRINT_DEBUG("RTPDataRcvr::~RTPDataRcvr()");

	//Then close The RTPBufferMFrame
	for(int i=0;i<X7S_NET_BOARD_NCAMERA;i++) {
		//Delete temporary frame
		if(ar_rtpmframe[i])
		{
			if(ar_rtpmframe[i]->jpeg) delete ar_rtpmframe[i]->jpeg;
			if(ar_rtpmframe[i]->mdata) delete ar_rtpmframe[i]->mdata;
			delete ar_rtpmframe[i];
		}
		//Delete the camera X7sNetFrame.
		if(board && board->cam[i] && board->cam[i]->frame)
		{
			x7sReleaseNetFrame(&(board->cam[i]->frame));
		}
	}

	//And finally close the rtpheader.
	delete(rtp_hdr);
}

/**
* @brief Reset internal value before starting sending a new RTP flow.
*
* The value of SSRCID is given by the X7sNetBoard parameter @ref S7P_PRM_BRD_SSRCID.
*/
int RTPDataRcvr::Reset() {

	X7S_FUNCNAME("RTPDataRcvr::Reset()");
	uint32_t ssrc_cam, ssrc_cam0;
	ssrc_cam0 = board->pval[S7P_PRM_BRD_SSRCID]+1;

	X7sDataRcvr::Reset();	//Reset flushcam and actual_camid.

	X7S_PRINT_INFO(funcName,sock->toString().c_str());
	std::stringstream sstr;
	sstr << "SSRCID: ";

	//If the data has not been reserved during setup we can reserve it
	for(int i=0;i<X7S_NET_BOARD_NCAMERA;i++) {

		//Create the memory for each camera!
		if(board->cam[i]->frame==NULL) {
			board->cam[i]->frame=x7sCreateNetFrame(i,
					S7P_DTYPE_JPEG,X7S_NET_RTP_MSIZEIB_JPG,
					S7P_MTYPE_NONE,X7S_NET_RTP_MSIZEIB_META);
		}

		//Then set the associative tab between SSRC and X7sNetCamera.
		ssrc_cam = ssrc_cam0+i;

		//Set the correct SSRC for the RTPData.
		ssrcRtpMframe[ssrc_cam]=ar_rtpmframe[i];
		ar_rtpmframe[i]->jpeg->SetSSRC(ssrc_cam);

		sstr << "CAMID_" << (i+1) << " <=> 0x" << std::hex << ssrc_cam << " ";

		if(board->cam[i]->pval[S7P_PRM_CAM_MTYPE]!=S7P_MTYPE_NONE) {
			ssrcRtpMframe[ssrc_cam+X7S_NET_BOARD_NCAMERA]=ar_rtpmframe[i];
			ar_rtpmframe[i]->mdata->SetSSRC(ssrc_cam+X7S_NET_BOARD_NCAMERA);
			ar_rtpmframe[i]->need_mdata=true;
			sstr << "& 0x" << ssrc_cam+X7S_NET_BOARD_NCAMERA << " ";
		}

	}
	X7S_PRINT_DEBUG(funcName,"%s",sstr.str().c_str());

	//Reset the value of last timestamp.


	//Flush socket buffer to trash
	sock->Flush();

	return X7S_NET_RET_OK;
}


/**
* @brief Release the frame created by the RTP Receive Data.
*/
void ix7sReleaseRTPNetFrame(X7sNetFrame** pNetFrame)
{
	X7sNetFrame* netFrame=*(pNetFrame);
	if(netFrame)
	{
		X7S_PRINT_DEBUG("ix7sReleaseRTPNetFrame()","camID=%d",netFrame->camID);

		if(netFrame->data) delete (RTPJPEGData*)netFrame->data;
		netFrame->data=NULL;

		if(netFrame->mdata) delete (RTPMetaData*)netFrame->mdata;
		netFrame->mdata=NULL;

		memset(netFrame,sizeof(netFrame),0);
		delete netFrame;
	}
	*(pNetFrame)=NULL;
}

/**
* @brief Flush one metaframe from socket buffer to our receiving buffer.
*
* This function is quite complicated because if everything works fine we know
* when we our metaframe is complete. However, if the
* last packet is lost we will know only that this last packet is missing by receiving
* a packet for a new metaframe when the last metaframe is still not complete.
*
* @return X7S_NET_RET_OK if a frame has been flushed completely.
*
*/
int RTPDataRcvr::FlushMetaFrame() {
	int ret;
	X7sNetChrono chrono;
	X7S_FUNCNAME("RTPDataRcvr::FlushMetaFrame()");

	uint32_t timestamp;

	//First receive a packet and set its timestamp
	ret= this->RecvPkt();
	if(ret!=X7S_NET_RET_OK) {
		X7S_PRINT_INFO(funcName,"TIMEOUT chrono=%f s (%s)",chrono.Seconds(),board->ip_str);
		return ret;
	}

	//Set the timestamp of the while loop.
	timestamp=rtp_hdr->ts;

	//Loop over the same timestamp until the frame is full
	while(rtp_hdr->ts==timestamp) {

		//And use the SSRCID to find the X7sNetFrame buffer.
		rtpmframe=ssrcRtpMframe[rtp_hdr->ssrc];

		if(rtpmframe==NULL) {
			X7S_PRINT_WARN(funcName,
					"No X7sNetCamera find for SSRCID 0x%X",rtp_hdr->ssrc);
			return X7S_NET_RET_NOTFOUND;
		}

		//Then parse the packet to fill the metaframe.
		bool completed=false;
		ret = this->ParsePkt(&completed);
		if(ret!=X7S_NET_RET_OK) {
			X7S_PRINT_INFO(funcName,"ParsePkt() Error=%d",ret);
			return ret;
		}


		if(completed==false)
		{
			//Continue receiving data
			ret= this->RecvPkt();
			if(ret!=X7S_NET_RET_OK) {
				X7S_PRINT_DUMP(funcName,"CAMID_%d >> RecvPkt() Timeout (TS=0x%0x)",rtpmframe->ID+1,timestamp);
				return X7S_NET_RET_TIMEOUT;
			}
		}
		else
		{

			//Check if we do not have the time to read the frame before receiving a new one.
			bool isOverwritten = (board->cam[rtpmframe->ID]->is_new);

			//If it is the last packet and the frame is complete then, we copy into new buffer.
			CopyRTPBuffer2Camera(rtpmframe,board->cam[rtpmframe->ID]);

			//And print a message
			if(isOverwritten)
			{
				X7S_PRINT_INFO(funcName,"CAMID_%d > Full Frame (TS=%d, %d ms) Overwritten"
						,rtpmframe->ID+1,rtpmframe->ts,chrono.Time());
			}
			else
			{
				X7S_PRINT_DEBUG(funcName,"CAMID_%d > Full Frame (TS=%d, %d ms) Copied"
						,rtpmframe->ID+1,rtpmframe->ts,chrono.Time());
			}
			return X7S_NET_RET_OK;
		}
	}

	X7S_PRINT_DEBUG(funcName,"CAMID_%d >> RTP Buffer State %d (TS: pkt=%d, frame=%d)",rtpmframe->ID+1,rtpmframe->state,rtp_hdr->ts,timestamp);
	if(rtpmframe->state<0) X7S_PRINT_INFO(funcName,"RTP Frame error: %d (-1 miss mdata, -2 miss data)",rtpmframe->state);

	printRTPHeader(X7S_LOGLEVEL_HIGH,rtp_hdr);

	//And use the SSRCID to find the X7sNetFrame buffer.
	rtpmframe=ssrcRtpMframe[rtp_hdr->ssrc];

	if(rtpmframe==NULL) {
		X7S_PRINT_WARN(funcName,
				"No X7sNetCamera find for SSRCID 0x%X",rtp_hdr->ssrc);
		return X7S_NET_RET_NOTFOUND;
	}

	//Then parse the packet to fill the metaframe.
	this->ParsePkt();


	return X7S_NET_RET_LOSTPKT;
}

/**
* @brief Receive a packet.
* This function:
* 	- Ask a RTPDataRcvr::sock to receive a packet in the RTPDataRcvr::pkt_buff.
* 	- Set the RTPDataRcvr::size_pkt of the packet in RTPDataRcvr.
* 	- And finally parse the RTPDataRcvr::rtp_hdr and set the  RTPDataRcvr::size_rtp_hdr of the header.
*
* @return the X7S_NET_RET_xxx error code.
*
*/
int RTPDataRcvr::RecvPkt() {
	size_pkt=sock->RecvPkt(pkt_buff,X7S_NET_RTP_MSIZEIB_PKT);
	if(size_pkt <= 0) return X7S_NET_RET_TIMEOUT; //No packet are received.

	//Then parse the RTP header
	size_rtp_hdr=RTPData::ParseRTPHeader(pkt_buff,rtp_hdr);
	//printRTPHeader(X7S_LOGLEVEL_HIGHEST,rtp_hdr);

	return X7S_NET_RET_OK;
}



/**
* @brief Parse the sub-RTP header and fill the metaframe with the given packet.
* @return @true if the last packet of the metaframe has been parsed, @false otherwise:
* If @false you should look the state of the RTP frame with the RTPBufferFrame::state
*/
int RTPDataRcvr::ParsePkt(bool *completed) {
	std::stringstream sstr;
	bool var;
	if(completed==NULL) completed=&var;
	*completed=false;
	RTPData *tmp=NULL;
	bool new_rtpdata=false;

	//Check Arguments to be sure we have the correct class
	if(rtp_hdr==NULL || rtpmframe==NULL) return X7S_NET_RET_UNKOWN;

	//Check the payload type
	if(rtp_hdr->pt==rtpmframe->jpeg->GetPayloadType()) {
		tmp=rtpmframe->jpeg;
	}
	else if(rtp_hdr->pt==rtpmframe->mdata->GetPayloadType()){
		tmp=rtpmframe->mdata;
	}
	else {
		//Print error message if the payload type is unkown.
		X7S_PRINT_ERROR("RTPDataRcvr::ParsePkt()","RTP type is not correct: %d (expected %d or %d)",
				rtp_hdr->pt,rtpmframe->jpeg->GetPayloadType(),rtpmframe->mdata->GetPayloadType());
		return X7S_NET_RET_UNKOWN;
	}

	//Finaly, check the Synchronisation source
	if(rtp_hdr->ssrc!=tmp->GetSSRC()) {
		X7S_PRINT_ERROR("RTPDataRcvr::ParsePkt()","SSRC ID is not correct: 0x%X (expected 0x%X)",
				rtp_hdr->ssrc,tmp->GetSSRC());
		return X7S_NET_RET_UNKOWN;
	}


	//Check if the packet receive correspond to same timestamp than the RTPBufferMFrame.
	if(rtp_hdr->ts!=tmp->GetTS()) {
		new_rtpdata=true;

		if(rtp_hdr->ts!=rtpmframe->ts) {
			rtpmframe->ts=rtp_hdr->ts;
			rtpmframe->state=X7S_NET_RTPFRAME_PARTIAL;

			X7S_PRINT_DEBUG("RTPDataRcvr::ParsePkt()","CAMID_%d New RTPNetFrame (TS=%d)"
					,rtpmframe->ID+1,rtpmframe->ts);
		}
	}

	//Then we parse the packet to the RTPData.
	tmp->ParseData((uint8_t*)pkt_buff,size_pkt,rtp_hdr,new_rtpdata);

	//And finally we check if the parsed packet was the last one for this RTPData.
	if(tmp->IsComplete())  {
		if(tmp==rtpmframe->jpeg) {
			if(rtpmframe->need_mdata) {	//!< If metadata are need
				rtpmframe->state=X7S_NET_RTPFRAME_MISSMDATA;
				//return X7S_NET_RET_BADMDATA;
			}
			else {
				rtpmframe->state=X7S_NET_RTPFRAME_FULL;
				*completed=true;
			}
		}
		else if(tmp==rtpmframe->mdata){
			if(rtpmframe->state==X7S_NET_RTPFRAME_MISSMDATA) {
				rtpmframe->state=X7S_NET_RTPFRAME_FULL;
				*completed=true;
			}
		}
	}
	return X7S_NET_RET_OK;
}

int CopyRTPBuffer2Camera(RTPBufferMFrame *buff, X7sNetCamera *cam) {
	int ret=X7S_NET_RET_OK;

	if(buff!=NULL && cam!=NULL && cam->frame !=NULL)
	{
		//Set frame properties
		cam->frame->camID=buff->ID;
		cam->frame->width=buff->jpeg->GetWidth();
		cam->frame->height=buff->jpeg->GetHeight();
		cam->frame->ID=buff->ts;
		cam->frame->counter++;
		if(cam->frame->data->capacity >= buff->jpeg->length)
		{
			memcpy(cam->frame->data->data,buff->jpeg->data,buff->jpeg->length);
			cam->frame->data->length=buff->jpeg->length;
		}
		else ret=X7S_NET_RET_UNKOWN;

		//If metadatos are set during the process than create the following one.
		if(buff->mdata) {
			//Copy metadatos
			if(cam->frame->mdata->capacity >= buff->mdata->length)
			{
				memcpy(cam->frame->mdata->data,buff->mdata->data,buff->mdata->length);
				cam->frame->mdata->length=buff->mdata->length;
				cam->frame->mdtype=(int)cam->pval[S7P_PRM_CAM_MTYPE];
			}
			else ret=X7S_NET_RET_UNKOWN;
		}


		cam->is_new=true;
	}
	else ret=X7S_NET_RET_UNKOWN;
	return ret;
}


std::string printRTPHeader(int level, rtp_hdr_t *rtp_hdr) {
	std::stringstream sstr;
#ifdef DEBUG
	sstr << "V="<< rtp_hdr->version << "; P=" << rtp_hdr->p <<"; X="<<rtp_hdr->x<<"; ";
	sstr << "CCS="<< rtp_hdr->cc<<"; M="<<rtp_hdr->m<<"; PT="<<rtp_hdr->pt<<"; ";
	sstr << "SEQ_NUM="<<rtp_hdr->seq<<"; " << "TS="<< rtp_hdr->ts << "; SSCR=0x" << rtp_hdr->ssrc;
	X7S_LOG("",level,"RTP=%s",sstr.str().c_str());
#endif
	return sstr.str();
}





