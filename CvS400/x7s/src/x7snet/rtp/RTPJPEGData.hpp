/**
 *  @file
 *  @brief Contains the class RTPJPEGData
 *  @date 15-dic-2008
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef RTPJPEGDATA_HPP_
#define RTPJPEGDATA_HPP_

#include "RTPData.hpp"


/**
 * @brief Structure for the JPEG/RTP Header
 *
 * @warning  The byte in the structure are swapped for Little Endian representation.
 * See @ref page_developers_endianness
 */
struct jpeghdr_s {
        unsigned int off:24;	//!< fragment byte offset
        unsigned int tspec:8;	//!< type-specific field
        unsigned int height:8;	//!< frame height in 8 pixel blocks
        unsigned int width:8;	//!< frame width in 8 pixel blocks
        unsigned int q:8;		//!< quantization factor (or table id)
        unsigned int type:8;	//!< id of jpeg decoder params
};

/**
 * @brief Structure for the Reset JPEG/RTP Header.
 *
 * @warning  The byte in the structure are swapped for Little Endian representation.
 * See @ref page_developers_endianness
 */
struct jpeghdr_rst_s {
        unsigned int count:14;	//!< The count of the number of block.
        unsigned int l:1;		//!< If the last byte of the restart block is in this packet.
        unsigned int f:1;		//!< If the first byte of the restart block is in this packet.
        unsigned int dri:16;	//!< Size of the restart block interval
};


/**
 * @brief Structure for the Quantization table if the JPEG/RTP protocol give a dynamic one.
 *
 * @warning  The byte in the structure are swapped for Little Endian representation.
 * See @ref page_developers_endianness
 */
struct jpeghdr_qtable_s {
        unsigned int mbz:8;			//!< Must Be Zero (reserved network protocol field)
        unsigned int precision:8;	//!< Precision field bit for each tables (rightmost bit if the first table)
        unsigned int length:16;		//!< Length of quantification table or 0 if this one has been given before.
};



/**
 *	@brief The RTPJPEGData.
 *	@see Most of the code come from: http://tools.ietf.org/html/rfc2435 (Appendix C)
 *	@ingroup net
 */
class RTPJPEGData: public RTPData {
public:
	RTPJPEGData();
	RTPJPEGData(int size_buffer);
	void Init();
	virtual ~RTPJPEGData();

	virtual std::string DebugMsg();
	int GetWidth();
	int GetHeight();


protected:
	int ParseData(const uint8_t *pkt,const int& size_pkt,const rtp_hdr_t *p_rtp_hdr, const bool& new_frame);


private:
	bool CheckJPEGHdr(jpeghdr_s *jpeghdr);
	int ParseHeader(const uint8_t *rtp_data);
	bool SelectQTables(uint8_t Q);
	int MakeHeaders(uint8_t *p, int type, int w, int h, uint8_t *lqt,
			uint8_t *cqt, uint16_t dri);

	/* VARIABLES */

	uint8_t lum_qtable[64];					//!< Memory to create the luminance quantification table.
	uint8_t chr_qtable[64];					//!< Memory to create the chrominance quantification table

	jpeghdr_s *jpeghdr; 				//!< Pointer on the packet header.
	jpeghdr_rst_s *jpeghdr_rst;			//!< Pointer on the packet header.
	jpeghdr_qtable_s *jpeghdr_qtable;	//!< Pointer on the packet header.

	int size_rtp_jpeg_hdr;	//!< Size of the JPEG/RTP header (main + reset + quanti).
	int size_jpg_hdr;		//!< Size of the JPEG header created in the buffer @see MakeHeaders().
	int size_pl;			//!< Size of the jpeg payload (only data).

	uint8_t Q, type;		//!< Index of the Quantification table, and type of JPEG
	int width, height;		//!< Width,height of the JPEG image.
	uint16_t dri;			//!< Value of the DRI marker in JFIF format (Restart length)

	int seq_startframe;		//!< Sequence number of the packet when the frame start.
	int seq_endframe;		//!< Sequence number of the packet when the frame end
};

//! Return width of the JPEG image
inline int RTPJPEGData::GetWidth() { return width; }

//! Return height of the JPEG image
inline int RTPJPEGData::GetHeight() { return height; }

#endif /* RTPJPEGDATA_HPP_ */
