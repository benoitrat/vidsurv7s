/**
 *  @file
 *  @brief Contains the class RTPDataRcvr
 *  @date 23-abr-2009
 *  @author Benoit RAT
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef RTPDATARCVR_HPP_
#define RTPDATARCVR_HPP_

#include "X7sDataRcvr.hpp"
#include "RTPData.hpp"
#include <map>


#define X7S_NET_RTP_MSIZEIB_PKT 1500		//!< Maximum Size In Byte for a RTP packet.
#define X7S_NET_RTP_DEFAULT_PORT 50000	//!< Default port for RTP data.


enum {
	X7S_NET_RTPFRAME_MISSDATA=-2,	//!< This mean that the frame has received its last packet and miss some data.
	X7S_NET_RTPFRAME_MISSMDATA=-1,	//!< This mean that the frame has received its last packet and miss some mdata.
	X7S_NET_RTPFRAME_PARTIAL=0,		//!< This mean that the frame need more packet to be complete
	X7S_NET_RTPFRAME_FULL=1,			//!< This mean that the metaframe is complete
};


class RTPJPEGData;

/**
 * @brief A structure which represents a RTPBuffer for a metaframe.
 */
struct RTPBufferMFrame {
	int ID;
	uint32_t ts;
	int state;
	bool need_mdata;
	RTPJPEGData *jpeg;
	RTPData *mdata;
};



/**
 *	@brief RTPDataRcvr class
 * 	This class works in the following way:
 *
 * We first have the first step to initiate all the variable.
 * 		- It first create 4 RTPBufferMFrames, each containing:
 * 			- A Camera ID.
 * 			- A timestamp.
 * 			- A JPEG Frame.
 * 			- A Metadata Frame.
 *
 * 		- Then we associated the different SSRC setup previously to this RTPBufferMFrames.
 *
 * Then, we loop over each packet until a RTPBufferMFrames is complete or until no packet
 * are found in the socket. During one loop we performs the following operation:
 *
 * 			- First, parse the rtp_header of the packet and find if this last one correspond to
 * a RTPBufferMFrame.
 * 			- Then we check if we have a new frame by comparing the timestamp of rtp_header
 * and RTPBufferMFrame, if true, we reset the RTPData.
 * 			- Then we check if it is the first or the last packet by looking at offset or M tag.
 * 			- Then, we check if the RTPData is complete by comparing the sequence number between last
 * and first packet and the number of packet received.
 * 			- And finally we check if both RTPJPEGData and RTPMetaData are RTPData::IsComplete() to copy
 * them into the X7sNetFrame of the X7sNetCamera, and set this frame as new.
 */
class RTPDataRcvr: public X7sDataRcvr {
public:
	RTPDataRcvr(X7sSocket *sock,X7sNetBoard *board);
	virtual ~RTPDataRcvr();
	virtual int Reset();
	X7sNetFrame* CaptureFrame(bool is_waiting=true);
	int GetRTPBufferState() { return rtpmframe?rtpmframe->state:RTPDATA_NULL; }

protected:
	int FlushMetaFrame();
	int ParsePkt(bool* completed=NULL);
	int RecvPkt();

private:
	RTPBufferMFrame* rtpmframe;
	RTPBufferMFrame* ar_rtpmframe[X7S_NET_BOARD_NCAMERA];
	std::map<uint32_t,RTPBufferMFrame* > ssrcRtpMframe;			//!< Map a ID RTP Meta frame.
	uint8_t pkt_buff[X7S_NET_RTP_MSIZEIB_PKT];				//!< The memory used to receive the packet.
	rtp_hdr_t *rtp_hdr; 									//!< A pointer on the RTP header of the actual packet.
	int size_rtp_hdr;										//!< Size in byte of RTP header.
	int size_pkt;											//!< Size in byte of the packet received.
};

#endif /* RTPDATARCVR_HPP_ */
