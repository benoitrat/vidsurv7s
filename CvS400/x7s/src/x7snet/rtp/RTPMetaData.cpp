#include "RTPMetaData.hpp"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------

#include "X7sNetTools.hpp"	//X7S_PRINT_xxx,...
#include "X7sSocket.hpp"	//X7sNetLog::ntohl(),...



RTPMetaData::RTPMetaData()
:RTPData(RTP_PTYPE_X7SMD)
{
	Init();
}

RTPMetaData::RTPMetaData(int size_buffer)
:RTPData(RTP_PTYPE_X7SMD,size_buffer)
{
	Init();
}

/**
* @brief Init the memory for the pointers ::rtpmetahdr
*/
void RTPMetaData::Init() {
	rtpmetahdr = new rtpmetahdr_s;
	size_pl=0;
	seq_startframe=-1;
	seq_endframe=-1;
	width=-1;
	height=-1;
}

/**
* @brief Free the memory for various pointers created while creating this object.
*/
RTPMetaData::~RTPMetaData() {
	delete rtpmetahdr;
}

/**
* @brief Parse the header of X7SMETA/RTP.
*
* This method set the correct value to ::rtpmetahdr,
*
* @param rtp_data A pointer on the RTP data which is the beginning of the X7SMeta/Header
* @return the size of the X7SMeta/RTP header (@ref RTP_META_HDR_SIZEIB).
*/
int RTPMetaData::ParseHeader(const uint8_t *rtp_data) {

	//Parse main header (Convert BE -> LE the first 2*32-bits of RTP/JPEG header).
	X7sSocket::buff_ntohl((uint8_t*)rtpmetahdr,rtp_data,sizeof(rtpmetahdr_s));
	return RTP_META_HDR_SIZEIB;
}


int RTPMetaData::ParseData(const uint8_t *pkt,const int& size_pkt,
		const rtp_hdr_t *p_rtp_hdr,const bool& new_frame) {

	uint8_t *p_pkt= (uint8_t*)pkt; //Make a copy of the rtp packet.
	int nof_pkt=0;

	uint8_t *p_rtpdata=data;	// A pointer on the RTPdata

	size_rtp_hdr=(RTP_HEAD_MIN_SIZEIB+p_rtp_hdr->cc*4);
	p_pkt+=size_rtp_hdr; //Set at the beginning of JPEG header

	p_pkt+=this->ParseHeader(p_pkt);	//Set at the beginning of JPEG payload.


	//Set and/or reset the field when the first packet of a frame arrive.
	//===================================================================================
	if(new_frame) {
		this->SetNewData(p_rtp_hdr);
		if(!CheckMetaHdr(rtpmetahdr)) {
			this->type = rtpmetahdr->type;
			this->width= rtpmetahdr->width << 3;	//Convert from block size to pixel size.
			this->height=rtpmetahdr->height << 3;	//Convert from block size to pixel size.
		}

		count=0;			//Re-initiate the counter.
		nof_pkt=0;			//Initiate internal variable to correct display

		seq_startframe= -1;		//Reset the start seq as INVALID
		seq_endframe=-1;		//Reset the end seq as INVALID

		X7S_PRINT_DEBUG("RTPMetaData::ParseData()","New MDATA frame (ssrc=%X, ts=%d, seq=%d)",
				p_rtp_hdr->ssrc,p_rtp_hdr->ts,p_rtp_hdr->seq);

	}
	else {
		//Check that is the same attribute.
		if(!CheckMetaHdr(rtpmetahdr) ) {
			X7S_PRINT_WARN("RTPMetaData::ParseData()","The type of X7SMETA/RTP has changed"
					"type=%d,width=%d,height=%d",type,width,height);
		}
	}



	//Copy payload data at correct position
	//===================================================================================

	//Obtain the size of payload data (whole packet - headers).
	size_pl = size_pkt-(RTP_META_HDR_SIZEIB+size_rtp_hdr);
	this->length=rtpmetahdr->off+size_pl;	//Add this pkt payload to the size of RTPData buffer.
	if(CheckSize()) {
		memcpy(p_rtpdata+rtpmetahdr->off,p_pkt,size_pl);	//The offset is given by the packet, p_pkt point on payload data.
	}


	//Count if the number of packet is correct
	//===================================================================================

	//If it is the first packet we receive set the seq number of the starting frame.
	if(this->rtpmetahdr->off==0) seq_startframe=(int)p_rtp_hdr->seq;	//We have the first packet of the frame

	//If it is the last frame keep its sequence number.
	if(p_rtp_hdr->m==1) seq_endframe=(int)p_rtp_hdr->seq;	//For JPEG/RTP, marker bit equal to 1 mean end of frame (last packet).

	//Check if the start and end sequence number are corrects.
	if(seq_startframe>=0 && seq_endframe >=0) {
		nof_pkt = (seq_endframe - seq_startframe);
		if(nof_pkt<0) {
			nof_pkt =  seq_endframe + (0xFFFF-seq_startframe) +1;
		}

		if(nof_pkt != count) {
			X7S_PRINT_INFO("RTPMetaData::ParseData()","PACKET IS MISSING=[(seq:%d-%d)=%d] != count:%d",
					seq_endframe,seq_startframe,nof_pkt,count);
			count=seq_endframe-seq_startframe; //Put count value to the correct value.
		}
		else {
			state = RTPDATA_ENDOK;
		}
	}


	//Print debug message
	//===================================================================================

#ifdef DEBUG
	std::stringstream sstrd_dbg;
	sstrd_dbg << this->toString() << " | ";
	sstrd_dbg << "seq:("<<p_rtp_hdr->seq << "-" << seq_startframe<<")="<<nof_pkt << " | ";
	sstrd_dbg << "Offset=" << rtpmetahdr->off;
	if(p_rtp_hdr->m==1) sstrd_dbg << " << LAST;";
	X7S_PRINT_DUMP("%s",sstrd_dbg.str().c_str());
#endif


	//Add one count in the packet
	count++;

	return state;
}

bool RTPMetaData::CheckMetaHdr(rtpmetahdr_s *rtpmetahdr) {
	bool is_ok=true;
	is_ok = is_ok && (width == (int)(rtpmetahdr->width << 3));
	is_ok = is_ok && (height == (int)(rtpmetahdr->height << 3));
	is_ok = is_ok && (type == rtpmetahdr->type);
	return is_ok;
}




std::string RTPMetaData::DebugMsg() {
	stringstream sstr;
#ifdef DEBUG
	sstr << RTPData::toString();
	sstr << ">> MData [" << width << "x" << height <<"]: T="<<type;
#endif
	return sstr.str();
}
