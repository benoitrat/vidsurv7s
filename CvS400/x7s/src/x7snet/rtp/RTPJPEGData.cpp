#include "RTPJPEGData.hpp"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------

#include "X7sNetTools.hpp"	//X7S_PRINT_xxx,...
#include "X7sSocket.hpp"	//X7sNetLog::ntohl(),...


/*
 * Table K.1 from JPEG spec.
 */
static const int jpeg_luma_quantizer[64] = {
		16, 11, 10, 16, 24, 40, 51, 61,
		12, 12, 14, 19, 26, 58, 60, 55,
		14, 13, 16, 24, 40, 57, 69, 56,
		14, 17, 22, 29, 51, 87, 80, 62,
		18, 22, 37, 56, 68, 109, 103, 77,
		24, 35, 55, 64, 81, 104, 113, 92,
		49, 64, 78, 87, 103, 121, 120, 101,
		72, 92, 95, 98, 112, 100, 103, 99
};

/*
 * Table K.2 from JPEG spec.
 */
static const int jpeg_chroma_quantizer[64] = {
		17, 18, 24, 47, 99, 99, 99, 99,
		18, 21, 26, 66, 99, 99, 99, 99,
		24, 26, 56, 99, 99, 99, 99, 99,
		47, 66, 99, 99, 99, 99, 99, 99,
		99, 99, 99, 99, 99, 99, 99, 99,
		99, 99, 99, 99, 99, 99, 99, 99,
		99, 99, 99, 99, 99, 99, 99, 99,
		99, 99, 99, 99, 99, 99, 99, 99
};



//Luminance quantification table (High Quality).
static const uint8_t lqt_high[64] = {
		0x05,0x03,0x03,0x04,0x03,0x03,0x05,0x04,
		0x04,0x04,0x05,0x05,0x05,0x06,0x07,0x0D,
		0x08,0x07,0x07,0x07,0x07,0x10,0x0B,0x0C,
		0x09,0x0D,0x13,0x10,0x14,0x13,0x12,0x10,
		0x12,0x12,0x15,0x17,0x1E,0x19,0x15,0x16,
		0x1C,0x16,0x12,0x12,0x1A,0x23,0x1A,0x1C,
		0x1F,0x20,0x21,0x22,0x21,0x14,0x19,0x25,
		0x27,0x24,0x20,0x27,0x1E,0x21,0x21,0x20,
};
//Chrominance quantification table (High Quality).
static const uint8_t cqt_high[64] = {
		0x05,0x05,0x05,0x07,0x06,0x07,0x0F,0x08,
		0x08,0x0F,0x20,0x15,0x12,0x15,0x15,0x20,
		0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,
		0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,
		0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,
		0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,
		0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,
		0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,
};

//Luminance quantification table (Medium quality).
static const uint8_t lqt_med[64] = {
		0x10,0x0B,0x0C,0x0E,0x0C,0x0A,0x10,0x0E,
		0x0D,0x0E,0x12,0x11,0x10,0x13,0x18,0x28,
		0x1A,0x18,0x16,0x16,0x18,0x31,0x23,0x25,
		0x1D,0x28,0x3A,0x33,0x3D,0x3C,0x39,0x33,
		0x38,0x37,0x40,0x48,0x5C,0x4E,0x40,0x44,
		0x57,0x45,0x37,0x38,0x50,0x6D,0x51,0x57,
		0x5F,0x62,0x67,0x68,0x67,0x3E,0x4D,0x71,
		0x79,0x70,0x64,0x78,0x5C,0x65,0x67,0x63,
};
//Chrominance quantification table (Medium quality).
static const uint8_t cqt_med[64] = {
		0x11,0x12,0x12,0x18,0x15,0x18,0x2F,0x1A,
		0x1A,0x2F,0x63,0x42,0x38,0x42,0x63,0x63,
		0x63,0x63,0x63,0x63,0x63,0x63,0x63,0x63,
		0x63,0x63,0x63,0x63,0x63,0x63,0x63,0x63,
		0x63,0x63,0x63,0x63,0x63,0x63,0x63,0x63,
		0x63,0x63,0x63,0x63,0x63,0x63,0x63,0x63,
		0x63,0x63,0x63,0x63,0x63,0x63,0x63,0x63,
		0x63,0x63,0x63,0x63,0x63,0x63,0x63,0x63
};


//Luminance quantification table (Low quality).
static const uint8_t lqt_low[64] = {
		0x2D,0x1F,0x22,0x27,0x22,0x1C,0x2D,0x27,
		0x24,0x27,0x33,0x30,0x2D,0x35,0x44,0x71,
		0x49,0x44,0x3E,0x3E,0x44,0x8B,0x63,0x69,
		0x52,0x71,0xA4,0x90,0xAD,0xAA,0xA1,0x90,
		0x9E,0x9C,0xB5,0xCC,0xFF,0xDD,0xB5,0xC1,
		0xF7,0xC3,0x9C,0x9E,0xE3,0xFF,0xE5,0xF7,
		0xFF,0xFF,0xFF,0xFF,0xFF,0xB0,0xDA,0xFF,
		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
};
//Chrominance quantification table (Low quality).
static const uint8_t cqt_low[64] = {
		0x30,0x33,0x33,0x44,0x3B,0x44,0x85,0x49,
		0x49,0x85,0xFF,0xBB,0x9E,0xBB,0xBB,0xFF,
		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
		0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF
};


//================= ViSmart


//quantification table (Vismart 100%).
static const uint8_t qt_vismart100[64] = {
		0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
		0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
		0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
		0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
		0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
		0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
		0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
		0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01
};


//quantification table (Vismart 85%).
static const uint8_t qt_vismart85[64] = {
		0x05,0x03,0x04,0x04,
		0x04,0x03,0x05,0x04,
		0x04,0x04,0x05,0x05,
		0x05,0x06,0x07,0x0C,
		0x08,0x07,0x07,0x07,
		0x07,0x0F,0x0B,0x0B,
		0x09,0x0C,0x11,0x0F,
		0x12,0x12,0x11,0x0F,
		0x11,0x11,0x13,0x16,
		0x1C,0x17,0x13,0x14,
		0x1A,0x15,0x11,0x11,
		0x18,0x21,0x18,0x1A,
		0x1D,0x1D,0x1F,0x1F,
		0x1F,0x13,0x17,0x22,
		0x24,0x22,0x1E,0x24,
		0x1C,0x1E,0x1F,0x1E
};


//quantification table (Vismart 50%).
static const uint8_t qt_vismart75[64] = {
		0x08,0x06,0x06,0x07,
		0x06,0x05,0x08,0x07,0x07,
		0x07,0x09,0x09,0x08,0x0A,
		0x0C,0x14,0x0D,0x0C,0x0B,
		0x0B,0x0C,0x19,0x12,0x13,
		0x0F,0x14,0x1D,0x1A,0x1F,
		0x1E,0x1D,0x1A,0x1C,0x1C,
		0x20,0x24,0x2E,0x27,0x20,
		0x22,0x2C,0x23,0x1C,0x1C,
		0x28,0x37,0x29,0x2C,0x30,
		0x31,0x34,0x34,0x34,0x1F,
		0x27,0x39,0x3D,0x38,0x32,
		0x3C,0x2E,0x33,0x34,0x32
};


//quantification table (Vismart 50%)
static const uint8_t qt_vismart50[64] = {
		0x10,0x0B,0x0C,0x0E,0x0C,0x0A,0x10,0x0E,
		0x0D,0x0E,0x12,0x11,0x10,0x13,0x18,0x28,
		0x1A,0x18,0x16,0x16,0x18,0x31,0x23,0x25,
		0x1D,0x28,0x3A,0x33,0x3D,0x3C,0x39,0x33,
		0x38,0x37,0x40,0x48,0x5C,0x4E,0x40,0x44,
		0x57,0x45,0x37,0x38,0x50,0x6D,0x51,0x57,
		0x5F,0x62,0x67,0x68,0x67,0x3E,0x4D,0x71,
		0x79,0x70,0x64,0x78,0x5C,0x65,0x67,0x63
};

//quantification table (Vismart 15%).
static const uint8_t qt_vismart15[64] = {
		0x35,0x25,0x28,0x2F,
		0x28,0x21,0x35,0x2F,
		0x2B,0x2F,0x3C,0x39,
		0x35,0x3F,0x50,0x85,
		0x57,0x50,0x49,0x49,
		0x50,0xA3,0x75,0x7B,
		0x61,0x85,0xC1,0xAA,
		0xCB,0xC8,0xBE,0xAA,
		0xBA,0xB7,0xD5,0xF0,
		0xFF,0xFF,0xD5,0xE2,
		0xFF,0xE6,0xB7,0xBA,
		0xFF,0xFF,0xFF,0xFF,
		0xFF,0xFF,0xFF,0xFF,
		0xFF,0xCE,0xFF,0xFF,
		0xFF,0xFF,0xFF,0xFF,
		0xFF,0xFF,0xFF,0xFF
};






uint8_t lum_dc_codelens[] = {
		0, 1, 5, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0,
};

uint8_t lum_dc_symbols[] = {
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
};

uint8_t lum_ac_codelens[] = {
		0, 2, 1, 3, 3, 2, 4, 3, 5, 5, 4, 4, 0, 0, 1, 0x7d,
};

uint8_t lum_ac_symbols[] = {
		0x01, 0x02, 0x03, 0x00, 0x04, 0x11, 0x05, 0x12,
		0x21, 0x31, 0x41, 0x06, 0x13, 0x51, 0x61, 0x07,
		0x22, 0x71, 0x14, 0x32, 0x81, 0x91, 0xa1, 0x08,
		0x23, 0x42, 0xb1, 0xc1, 0x15, 0x52, 0xd1, 0xf0,
		0x24, 0x33, 0x62, 0x72, 0x82, 0x09, 0x0a, 0x16,
		0x17, 0x18, 0x19, 0x1a, 0x25, 0x26, 0x27, 0x28,
		0x29, 0x2a, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
		0x3a, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49,
		0x4a, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59,
		0x5a, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69,
		0x6a, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79,
		0x7a, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89,
		0x8a, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98,
		0x99, 0x9a, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7,
		0xa8, 0xa9, 0xaa, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6,
		0xb7, 0xb8, 0xb9, 0xba, 0xc2, 0xc3, 0xc4, 0xc5,
		0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xd2, 0xd3, 0xd4,
		0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xe1, 0xe2,
		0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea,
		0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8,
		0xf9, 0xfa,
};

uint8_t chm_dc_codelens[] = {0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,};
uint8_t chm_dc_symbols[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,};
uint8_t chm_ac_codelens[] = {0, 2, 1, 2, 4, 4, 3, 4, 7, 5, 4, 4, 0, 1, 2, 0x77,};

uint8_t chm_ac_symbols[] = {
		0x00, 0x01, 0x02, 0x03, 0x11, 0x04, 0x05, 0x21,
		0x31, 0x06, 0x12, 0x41, 0x51, 0x07, 0x61, 0x71,
		0x13, 0x22, 0x32, 0x81, 0x08, 0x14, 0x42, 0x91,
		0xa1, 0xb1, 0xc1, 0x09, 0x23, 0x33, 0x52, 0xf0,
		0x15, 0x62, 0x72, 0xd1, 0x0a, 0x16, 0x24, 0x34,
		0xe1, 0x25, 0xf1, 0x17, 0x18, 0x19, 0x1a, 0x26,
		0x27, 0x28, 0x29, 0x2a, 0x35, 0x36, 0x37, 0x38,
		0x39, 0x3a, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48,
		0x49, 0x4a, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58,
		0x59, 0x5a, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68,
		0x69, 0x6a, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78,
		0x79, 0x7a, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87,
		0x88, 0x89, 0x8a, 0x92, 0x93, 0x94, 0x95, 0x96,
		0x97, 0x98, 0x99, 0x9a, 0xa2, 0xa3, 0xa4, 0xa5,
		0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xb2, 0xb3, 0xb4,
		0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xc2, 0xc3,
		0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xd2,
		0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda,
		0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9,
		0xea, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8,
		0xf9, 0xfa,
};

uint8_t *MakeQuantHeader(uint8_t *p, uint8_t *qt, int tableNo)
{
	*p++ = 0xff;
	*p++ = 0xdb;            /* DQT */
	*p++ = 0;               /* length msb */
	*p++ = 67;              /* length lsb */
	*p++ = tableNo;
	memcpy(p, qt, 64);
	return (p + 64);
}

uint8_t *MakeHuffmanHeader(uint8_t *p, uint8_t *codelens, int ncodes,
		uint8_t *symbols, int nsymbols, int tableNo,
		int tableClass)
{
	*p++ = 0xff;
	*p++ = 0xc4;            /* DHT */
	*p++ = 0;               /* length msb */
	*p++ = 3 + ncodes + nsymbols; /* length lsb */
	*p++ = (tableClass << 4) | tableNo;
	memcpy(p, codelens, ncodes);
	p += ncodes;
	memcpy(p, symbols, nsymbols);
	p += nsymbols;
	return (p);
}

uint8_t *MakeDRIHeader(uint8_t *p, uint16_t dri) {
	*p++ = 0xff;
	*p++ = 0xdd;            /* DRI */
	*p++ = 0x0;             /* length msb */
	*p++ = 4;               /* length lsb */
	*p++ = dri >> 8;        /* dri msb */
	*p++ = dri & 0xff;      /* dri lsb */
	return (p);
}


/**
* @brief Call MakeTables with the Q factor and two uint8_t[64] return arrays.
* @see  ISO DIS 10918-1. Digital Compression and Coding of Continuous-tone Still Images (JPEG), CCITT Recommendation T.81.
* @see Tom Lane et. al., The Independent JPEG Group software JPEG codec.
*
*
*/
void MakeTables(int q, uint8_t *lqt, uint8_t *cqt) {
	int i;
	int factor = q;

	if (q < 1) factor = 1;
	if (q > 99) factor = 99;
	if (q < 50)
		q = 5000 / factor;
	else
		q = 200 - factor*2;


	for (i=0; i < 64; i++) {
		int lq = (jpeg_luma_quantizer[i] * q + 50) / 100;
		int cq = (jpeg_chroma_quantizer[i] * q + 50) / 100;

		/* Limit the quantizers to 1 <= q <= 255 */
		if (lq < 1) lq = 1;
		else if (lq > 255) lq = 255;
		lqt[i] = lq;

		if (cq < 1) cq = 1;
		else if (cq > 255) cq = 255;
		cqt[i] = cq;
	}
}



RTPJPEGData::RTPJPEGData()
:RTPData(RTP_PTYPE_JPEG)
 {
	Init();
 }

RTPJPEGData::RTPJPEGData(int size_buffer)
:RTPData(RTP_PTYPE_JPEG,size_buffer)
 {
	Init();
 }

/**
* @brief Init the memory for the pointers ::jpeghdr, ::jpeghdr_rst, ::jpeghdr_qtable.
*/
void RTPJPEGData::Init() {
	jpeghdr = new jpeghdr_s;
	jpeghdr_rst = new jpeghdr_rst_s;
	jpeghdr_qtable = new jpeghdr_qtable_s;

	size_rtp_jpeg_hdr=0;
	size_jpg_hdr=0;
	size_pl=0;

	seq_startframe=-1;
	seq_endframe=-1;

	width=-1;
	height=-1;
	Q=0;
	dri=0;
	type=0;
}

/**
* @brief Free the memory for various pointers created while creating this object.
*/
RTPJPEGData::~RTPJPEGData() {
	delete jpeghdr;
	delete jpeghdr_rst;
	delete jpeghdr_qtable;
}

/**
* @brief Parse the header of JPEG/RTP.
*
* This method set the correct value to ::jpeghdr, ::jpeghdr_rst and ::jpeghdr_qtable
*
* @param rtp_data A pointer on the RTP data which is the beginning of the JPEG/Header
* @return the size of the JPEG/RTP header.
*/
int RTPJPEGData::ParseHeader(const uint8_t *rtp_data) {
	int size=sizeof(jpeghdr_s);

	//Parse main header (Convert BE -> LE the first 2*32-bits of RTP/JPEG header).
	X7sSocket::buff_ntohl((uint8_t*)jpeghdr,rtp_data,size);

	//Parse restart header
	if(64 <= jpeghdr->type && jpeghdr-> type < 128) {
		X7sSocket::buff_ntohl((uint8_t*)jpeghdr_rst,rtp_data+size,sizeof(jpeghdr_rst_s));
		size+=sizeof(jpeghdr_rst_s); //Add the 4 bytes of jpeg_hdr_rst.
	}

	//Parse quantification header
	if(jpeghdr->q >= 128 ) {
		X7sSocket::buff_ntohl((uint8_t*)jpeghdr_qtable,rtp_data+size,sizeof(jpeghdr_qtable_s));
		size+=sizeof(jpeghdr_qtable_s);
		size+=jpeghdr_qtable->length;
	}
	return size;
}


int RTPJPEGData::ParseData(const uint8_t *pkt,const int& size_pkt,
		const rtp_hdr_t *p_rtp_hdr,const bool& new_frame) {

	uint8_t *p_pkt= (uint8_t*)pkt; //Make a copy of the rtp packet.
	int nof_pkt=0;

	uint8_t *p_rtpdata=data;	// A pointer on the RTPdata

	size_rtp_hdr=(RTP_HEAD_MIN_SIZEIB+p_rtp_hdr->cc*4);
	p_pkt+=size_rtp_hdr; //Set at the beginning of JPEG header

	size_rtp_jpeg_hdr=this->ParseHeader(p_pkt);
	p_pkt+=size_rtp_jpeg_hdr;	//Set at the beginning of JPEG payload.


	//Set and/or reset the field when the first packet of a frame arrive.
	//===================================================================================

	if(new_frame) {
		this->SetNewData(p_rtp_hdr);

		//If the JPEG header has changed find the new Q-Tables and MakeHeaders.
		if(!CheckJPEGHdr(jpeghdr)) {
			this->type = jpeghdr->type;
			this->width= jpeghdr->width << 3;	//Convert from block size to pixel size.
			this->height=jpeghdr->height << 3;	//Convert from block size to pixel size.
			if(64 <= jpeghdr->type && jpeghdr-> type < 128) dri = jpeghdr_rst->dri;
			else dri=0;
			this->SelectQTables(jpeghdr->q); //Modify Q, lum_qtable, chr_qtable.

			uint8_t* chr_qtable_tmp=NULL;
			if(jpeghdr->q<110) chr_qtable_tmp=(uint8_t*)chr_qtable; //If there is two table.

			size_jpg_hdr = this->MakeHeaders(p_rtpdata,
					type,jpeghdr->width,jpeghdr->height,
					(uint8_t*)lum_qtable,chr_qtable_tmp,dri);
		}

		p_rtpdata +=size_jpg_hdr;	//Point to the beginning of the JPEG payload.

		count=0;			//Re-initiate the counter.
		nof_pkt=0;			//Initiate internal variable to correct display

		seq_startframe= -1;		//Reset the start seq as INVALID
		seq_endframe=-1;		//Reset the end seq as INVALID


		X7S_PRINT_DEBUG("RTPJPEGData::ParseData()","New JPEG frame (ssrc=%X, ts=%d, seq=%d)",
				p_rtp_hdr->ssrc,p_rtp_hdr->ts,p_rtp_hdr->seq);

	}
	else {
		//Check that is the same attribute.
		if(!CheckJPEGHdr(jpeghdr) ) {
			X7S_PRINT_WARN("RTPJPEGData::ParseData()","The type of JPEG/RTP has changed: "
					"type=%d,width=%d,height=%d,Q=%d",type,width,height,Q);
		}

		//Set the pointer after JPEG header.
		p_rtpdata +=size_jpg_hdr;	//Point to the beginning of the JPEG payload.
	}

	//Copy payload data at correct position
	//===================================================================================

	//Obtain the size of payload data (whole packet - headers).
	size_pl = size_pkt-(size_rtp_jpeg_hdr+size_rtp_hdr);
	this->length=size_jpg_hdr+(jpeghdr->off+size_pl);	//Add this pkt payload to the size of RTPData buffer.
	if(CheckSize()) {
		memcpy(p_rtpdata+jpeghdr->off,p_pkt,size_pl);	//The offset is given by the packet, p_pkt point on payload data.
	}


	//Count if the number of packet is correct
	//===================================================================================

	//If it is the first packet we receive set the seq number of the starting frame.
	if(jpeghdr->off==0) seq_startframe=(int)p_rtp_hdr->seq;	//We have the first packet of the frame

	//If it is the last frame keep its sequence number.
	if(p_rtp_hdr->m==1) seq_endframe=(int)p_rtp_hdr->seq;	//For JPEG/RTP, marker bit equal to 1 mean end of frame (last packet).

	//Check if the start and end sequence number are corrects.
	if(seq_startframe>=0 && seq_endframe >=0) {
		nof_pkt = (seq_endframe - seq_startframe);
		if(nof_pkt<0) {
			nof_pkt =  seq_endframe + (0xFFFF-seq_startframe) +1;
		}

		if(nof_pkt != count) {
			X7S_PRINT_INFO("RTPJPEGData::ParseData()","PACKET IS MISSING=[(seq:%d-%d)=%d] != count:%d",
					seq_endframe,seq_startframe,nof_pkt,count);
			count=seq_endframe-seq_startframe; //Put count value to the correct value.
		}
		else {
			state = RTPDATA_ENDOK;
		}
	}


	//Print debug message
	//===================================================================================

#if DEBUG
	std::stringstream sstrd_dbg;
	sstrd_dbg << this->toString() << " | ";
	sstrd_dbg << "seq:("<<p_rtp_hdr->seq << "-" << seq_startframe<<")="<<nof_pkt << " | ";
	sstrd_dbg << "Offset=" << jpeghdr->off;
	if(p_rtp_hdr->m==1) sstrd_dbg << " << LAST;";
	X7S_PRINT_DUMP("%s",sstrd_dbg.str().c_str());
#endif


	//Add one count in the packet
	count++;

	return state;
}

bool RTPJPEGData::CheckJPEGHdr(jpeghdr_s *jpeghdr) {
	bool is_ok=true;
	is_ok = is_ok && (width == (int)(jpeghdr->width << 3));
	is_ok = is_ok && (height == (int)(jpeghdr->height << 3));
	is_ok = is_ok && (type == jpeghdr->type);
	is_ok = is_ok && (Q == jpeghdr->q);
	return is_ok;
}

/**
* @brief Select or create the quantification tables.
*
* If the Q is the same then the previous one the values in the q tables don't change.
* Otherwise if the Q is different or equal to 0xFF we need to change the values of the Q tables.
*
* @param Q The Q index of the actual packet.
* @return if the Q table has changed or is still the same.
* @warning This algorithm is not standard for Q>=100, Q is never set dynamically.
*/
bool RTPJPEGData::SelectQTables(uint8_t q) {
	if(this->Q==q && this->Q!=255) return false;	//The new Q is the same as the one previously selected.
	else {
		if(q<100) MakeTables(q, lum_qtable, chr_qtable); //From 0 to 99 table can be created with the MakeTables functions.
		else {
			switch(q)
			{
			case 100:
				memcpy(lum_qtable,lqt_low,64);
				memcpy(chr_qtable,cqt_low,64);
				break;
			case 101:
				memcpy(lum_qtable,lqt_med,64);
				memcpy(chr_qtable,cqt_med,64);
				break;
			case 102:
				memcpy(lum_qtable,lqt_high,64);
				memcpy(chr_qtable,cqt_high,64);
				break;
			case 110:
				memcpy(lum_qtable,qt_vismart15,64);
				memcpy(chr_qtable,qt_vismart15,64);
				break;
			case 111:
				memcpy(lum_qtable,qt_vismart50,64);
				memcpy(chr_qtable,qt_vismart50,64);
				break;
			case 112:
				memcpy(lum_qtable,qt_vismart75,64);
				memcpy(chr_qtable,qt_vismart75,64);
				break;
			case 113:
				memcpy(lum_qtable,qt_vismart85,64);
				memcpy(chr_qtable,qt_vismart85,64);
				break;
			case 114:
				memcpy(lum_qtable,qt_vismart100,64);
				memcpy(chr_qtable,qt_vismart100,64);
				break;
			default:
				memcpy(lum_qtable,jpeg_luma_quantizer,64);
				memcpy(chr_qtable,jpeg_chroma_quantizer,64);
			}
		}
		this->Q=q;
		return true;
	}
}

/**
* @brief Create a JPEG header for a received packet.
*
*
*  Generate a frame and scan headers that can be prepended to the
*  RTP/JPEG data payload to produce a JPEG compressed image in
*  interchange format (except for possible trailing garbage and
*  absence of an EOI marker to terminate the scan).
*
*  @param p		A pointer to the jpeg buffer memory
*  @param type 	The type for color space encoding YUV
*  @param width	Width in MCUs of the JPEG image (Width/8).
*  @param height	Height in MCUs of the JPEG image (Height/8)
*  @param lqt		Luminance quantification table.
*  @param cqt		Chrominance quantification table.
*  @param dri		Restart interval in MCUs, or 0 if no restarts.
*
*  @note quantization tables as either derived from the Q field
*  using MakeTables() or as specified in section 4.2 of .
*
*  @return The length of the generated headers.
*
*/
int RTPJPEGData::MakeHeaders(uint8_t *p, int type, int w, int h, uint8_t *lqt,
		uint8_t *cqt, uint16_t dri)
{
	uint8_t *start = p;


	/* convert from blocks to pixels */
	w <<= 3;
	h <<= 3;

	*p++ = 0xff;
	*p++ = 0xd8;            /* SOI */

	/** JFIF: Don't know if needed **/

	//JFIF header
	uint8_t JFIF_hdr[18] = {0xFF,0xE0,
			0x00,0x10,0x4A,0x46,
			0x49,0x46,0x00,0x01,
			0x02,0x00,0x00,0x01,
			0x00,0x01,0x00,0x00};


	memcpy(p, JFIF_hdr, sizeof(JFIF_hdr));
	p+=sizeof(JFIF_hdr);


	if(lqt) p = MakeQuantHeader(p, lqt, 0);
	if(cqt) p = MakeQuantHeader(p, cqt, 1);

	if (dri != 0)
		p = MakeDRIHeader(p, dri);

	*p++ = 0xff;
	*p++ = 0xc0;            /* SOF */
	*p++ = 0;               /* length msb */
	*p++ = 17;              /* length lsb */
	*p++ = 8;               /* 8-bit precision */
	*p++ = h >> 8;          /* height msb */
	*p++ = h;               /* height lsb */
	*p++ = w >> 8;          /* width msb */
	*p++ = w;               /* wudth lsb */
	*p++ = 3;               /* number of components */

	if(type==128) //Code OpenCore (Vismart)
	{
		*p++ = 01;               /* comp Y */
		*p++ = 0x11;    		/* hsamp = 1, vsamp = 1 */
		*p++ = 0;               /* quant table 0 */

		*p++ = 02;               /* comp U */
		*p++ = 0x11;            /* hsamp = 1, vsamp = 1 */
		*p++ = 0;               /* quant table 0 */

		*p++ = 03;               /* comp V */
		*p++ = 0x11;            /* hsamp = 1, vsamp = 1 */
		*p++ = 0;               /* quant table 0 */

		p = MakeHuffmanHeader(p, lum_dc_codelens,
				sizeof(lum_dc_codelens),
				lum_dc_symbols,
				sizeof(lum_dc_symbols), 0, 0);
		p = MakeHuffmanHeader(p, lum_ac_codelens,
				sizeof(lum_ac_codelens),
				lum_ac_symbols,
				sizeof(lum_ac_symbols), 0, 1);


		*p++ = 0xff;
		*p++ = 0xda;            /* SOS */
		*p++ = 0;               /* length msb */
		*p++ = 12;              /* length lsb */
		*p++ = 3;               /* 3 components */
		*p++ = 1;               /* comp Y */
		*p++ = 0;               /* huffman table 0 */
		*p++ = 2;               /* comp U */
		*p++ = 0;            /* huffman table 0 */
		*p++ = 3;               /* comp V */
		*p++ = 0;            /* huffman table 0 */
		*p++ = 0;               /* first DCT coeff */
		*p++ = 63;              /* last DCT coeff */
		*p++ = 0;               /* sucessive approx. */

	}
	else //Code official
	{
		*p++ = 0;               /* comp 0 (Y) */
		if (type == 0)
			*p++ = 0x21;    /* hsamp = 2, vsamp = 1 */
		else
			*p++ = 0x22;    /* hsamp = 2, vsamp = 2 */
		*p++ = 0;               /* quant table 0 */

		*p++ = 1;               /* comp 1 (U) */
		*p++ = 0x11;            /* hsamp = 1, vsamp = 1 */
		*p++ = 1;               /* quant table 1 */

		*p++ = 2;               /* comp 2 (V) */
		*p++ = 0x11;            /* hsamp = 1, vsamp = 1 */
		*p++ = 1;               /* quant table 1 */


		p = MakeHuffmanHeader(p, lum_dc_codelens,
				sizeof(lum_dc_codelens),
				lum_dc_symbols,
				sizeof(lum_dc_symbols), 0, 0);
		p = MakeHuffmanHeader(p, lum_ac_codelens,
				sizeof(lum_ac_codelens),
				lum_ac_symbols,
				sizeof(lum_ac_symbols), 0, 1);
		p = MakeHuffmanHeader(p, chm_dc_codelens,
				sizeof(chm_dc_codelens),
				chm_dc_symbols,
				sizeof(chm_dc_symbols), 1, 0);
		p = MakeHuffmanHeader(p, chm_ac_codelens,
				sizeof(chm_ac_codelens),
				chm_ac_symbols,
				sizeof(chm_ac_symbols), 1, 1);


		*p++ = 0xff;
		*p++ = 0xda;            /* SOS */
		*p++ = 0;               /* length msb */
		*p++ = 12;              /* length lsb */
		*p++ = 3;               /* 3 components */
		*p++ = 0;               /* comp 0 */
		*p++ = 0;               /* huffman table 0 */
		*p++ = 1;               /* comp 1 */
		*p++ = 0x11;            /* huffman table 1 */
		*p++ = 2;               /* comp 2 */
		*p++ = 0x11;            /* huffman table 1 */
		*p++ = 0;               /* first DCT coeff */
		*p++ = 63;              /* last DCT coeff */
		*p++ = 0;               /* sucessive approx. */

	}








	return (p - start);
};


std::string RTPJPEGData::DebugMsg() {
	stringstream sstr;
#ifdef DEBUG
	sstr << RTPData::toString();
	sstr << ">> JPEG [" << width << "x" << height <<"]: T="<<type<<",Q="<<Q;
#endif
	return sstr.str();
}
