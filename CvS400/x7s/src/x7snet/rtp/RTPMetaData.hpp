/**
 *  @file
 *  @brief Contains the class RTPMetaData
 *  @date 30-apr-2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef RTPMETADATA_HPP_
#define RTPMETADATA_HPP_

#include "RTPData.hpp"

#define RTP_META_HDR_SIZEIB 8	//!< Size of the Meta/RTP header in bytes.

/**
 * @brief Structure for the Metadatos/RTP Header
 *
 * @warning  The byte in the structure are swapped for Little Endian representation.
 * See @ref page_developers_endianness
 */
struct rtpmetahdr_s {
        unsigned int off:24;		//!< fragment byte offset
        unsigned int reserved1:8;	//!< Reserved value
        unsigned int height:8;		//!< frame height in 8 pixel blocks
        unsigned int width:8;		//!< frame width in 8 pixel blocks
        unsigned int reserved2:8;	//!< Reserved value.
        unsigned int type:8;		//!< Type of metadato
};




/**
 *	@brief The RTPMetaData.
 *	@ingroup net
 */
class RTPMetaData: public RTPData {
public:
	RTPMetaData();
	RTPMetaData(int size_buffer);
	void Init();
	virtual ~RTPMetaData();

	virtual std::string DebugMsg();
	int GetWidth();
	int GetHeight();


protected:
	int ParseData(const uint8_t *pkt,const int& size_pkt,const rtp_hdr_t *p_rtp_hdr, const bool& new_frame);


private:
	bool CheckMetaHdr(rtpmetahdr_s *metahdr);
	int ParseHeader(const uint8_t *rtp_data);


	/* VARIABLES */

	rtpmetahdr_s *rtpmetahdr; 	//!< Pointer on the packet header.
	int size_pl;				//!< Size of the jpeg payload (only data).
	uint8_t type;				//!< Index of the Quantification table, and type of JPEG
	int width, height;			//!< Width,height of the JPEG image.
	int seq_startframe;		//!< Sequence number of the packet when the frame start.
	int seq_endframe;		//!< Sequence number of the packet when the frame end

};

//! Return width of the MetaData image
inline int RTPMetaData::GetWidth() { return width; }

//! Return height of the MetaData image
inline int RTPMetaData::GetHeight() { return height; }

#endif /* RTPMETADATA_HPP_ */
