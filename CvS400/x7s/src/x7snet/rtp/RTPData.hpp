/**
 *  @file
 *  @brief Contains the class RTPData
 *  @date 17-dic-2008
 *  @author Benoit RAT
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef RTPDATA_HPP_
#define RTPDATA_HPP_

#include "x7snet.h"
#include <string>

/**
 * @brief Identifies the format of the RTP payload and determines its interpretation by the application.
 * @see @ref page_netp and http://www.networksorcery.com/enp/rfc/rfc3551.txt
 */
enum {
	RTP_PTYPE_PCMU = 0,
	RTP_PTYPE_GSM = 3,
	RTP_PTYPE_G723 = 4,
	RTP_PTYPE_DVI4_8KHZ = 5,
	RTP_PTYPE_DVI4_16KHZ = 6,
	RTP_PTYPE_LPC = 7,
	RTP_PTYPE_PCMA = 8,
	RTP_PTYPE_G722 = 9,
	RTP_PTYPE_L16_2C = 10,
	RTP_PTYPE_L16_1C = 11,
	RTP_PTYPE_QCELP = 12,
	RTP_PTYPE_CN = 13,
	RTP_PTYPE_MPA = 14,
	RTP_PTYPE_G728 = 15,
	RTP_PTYPE_DVI4_11KHZ = 16,
	RTP_PTYPE_DVI4_22KHZ = 17,
	RTP_PTYPE_G729 = 18,
	RTP_PTYPE_CelB = 25,
	RTP_PTYPE_JPEG = 26,		//!< The JPEG used in our program.
	RTP_PTYPE_nv = 28,
	RTP_PTYPE_H261 = 31,
	RTP_PTYPE_MPV = 32,
	RTP_PTYPE_MP2T = 33,
	RTP_PTYPE_H263 = 34,

	RTP_PTYPE_X7SMD = 77,		//!< The Xtend 7even Solutions Meta Datos
};

/**
 * @brief The different state for an RTPData
 */
enum {
	RTPDATA_NULL=-2,		//!< The RTPData don't have its buffer created.
	RTPDATA_ENDPARTIAL=-1,	//!< The RTPData receive its last frame but the packet count missing
	RTPDATA_START,			//!< The RTPData is new and packet need more packet
	RTPDATA_ENDOK,			//!< The RTPData is complete. End it OK.
};


#define RTP_HEAD_MIN_SIZEIB 12 //3*32-bits = 12 bytes.

/**
 * @brief A structure that represent the RTP header.
 * @see http://tools.ietf.org/html/rfc2435 (Appendix C)
 *
 * @warning  The byte in the structure are swapped for Little Endian representation.
 * See @ref page_developers_endianness
 *
 */
struct rtp_hdr_t {

		/* Write the 32 bits in the reverse order */
		unsigned int seq:16;		//!< sequence number
		unsigned int pt:7;			//!< payload type
		unsigned int m:1;			//!< marker bit
		unsigned int cc:4;			//!< CSRC count
		unsigned int x:1;			//!< header extension flag
		unsigned int p:1;			//!< padding flag
		unsigned int version:2;		//!< protocol version
		/* End of write the 32 bits in reverse order */

        unsigned int ts;			//!< timestamp
        unsigned int ssrc;			//!< synchronization source
        unsigned int csrc[16];		//!< optional CSRC list (Normally set to zeros)
};


/**
 *	@brief Abstract class which represent the generic type of RTP data.
 *
 *	The RTPData Object represent a complete frame of data. It can have different type
 *	such as Audio, Video, Metadata...
 *
 *  This object pass through different states:
 *
 * 		-# @ref RTPDATA_NULL.
 *		-# @ref RTPDATA_START.
 *		-# @ref RTPDATA_ENDPARTIAL (optional state).
 * 		-# @ref RTPDATA_ENDOK.
 *		-# ... Go back to @ref RTPDATA_START.
 *
 *
 *
 *	@ingroup net
 *	@see Implementations: RTPJPEGData
 */
class RTPData: public X7sByteArray {
public:
	RTPData(int payload_type);
	RTPData(int payload_type,int size_buffer);
	bool Init(int size_buffer);
	virtual ~RTPData();

	bool CheckSetHeader(const rtp_hdr_t *rtp_hdr);
	bool CheckFFields(const rtp_hdr_t *rtp_hdr) const;
	virtual int ParseData(const uint8_t *pkt,const int& size_pkt,
			const rtp_hdr_t *p_rtp_hdr, const bool& new_frame)=0; //!< abstract function which must be defined in a daughter class.


	static int ParseRTPHeader(const uint8_t* pkt_data,rtp_hdr_t* rtp_hdr);

	void SetSSRC(uint32_t ssrc);
	uint32_t GetSSRC() const;
	uint32_t GetTS() const;
	uint8_t GetPayloadType() const;
	bool IsComplete() const;
	virtual std::string toString();


protected:
	void SetNewData(const rtp_hdr_t *rtp_hdr);
	bool CheckSize();

	const rtp_hdr_t *p_rtp_hdr;
	int size_rtp_hdr;

	int state;			//!< Set the state of the RTPData.
	int count;			//!< This count the number of packet arrived.

	uint8_t ptype;			//!< The type of payload.
	uint32_t tstamp;	//!< The timestamp of the data.
	uint32_t ssrc; 		//!< The synchronization source of the data.
};

//! Set the SSRC for a specific type of RTPData.
inline void RTPData::SetSSRC(uint32_t ssrc) {
	this->ssrc=ssrc;
}

//! Return the Synchronous source
inline uint32_t RTPData::GetSSRC() const {
	return ssrc;
}

//! Return the timestamp of the frame
inline uint32_t RTPData::GetTS() const {
	return tstamp;
}

//! Return the payload type such as  @ref RTP_PTYPE_JPEG.
inline uint8_t RTPData::GetPayloadType() const {
	return ptype;
}

//! Return if the frame is complete or not.
inline bool RTPData::IsComplete() const { return state==RTPDATA_ENDOK; }

#endif /* RTPDATA_HPP_ */
