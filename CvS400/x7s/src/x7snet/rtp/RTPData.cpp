#include "RTPData.hpp"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------


#include "X7sNetTools.hpp"	//X7S_PRINT_xxx,...
#include "X7sSocket.hpp"
#include <iomanip>

#define SIZEIB_RTP_HEAD_MIN 12 //3*32-bits = 12 bytes.

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor and Initialization.
//---------------------------------------------------------------------------------------


/**
 * @brief Empty constructor.
 *
 * You must call ::Init() is you want to use this class.
 */
RTPData::RTPData(int payload_type)
:state(RTPDATA_NULL),count(0),
ptype(payload_type), tstamp(0), ssrc(0)
{
	data=NULL;
	length=0;
	capacity=0;
}

/**
 * @brief Constructor of generic RTP class.
 *
 * @see ::Init() for the parameters.
 */
RTPData::RTPData(int payload_type,int size_buffer)
:state(RTPDATA_NULL),count(0),
ptype(payload_type), tstamp(0), ssrc(0)
{
	data=NULL;
	length=0;
	capacity=0;
	Init(size_buffer);
}

/**
 * @brief Init the general RTP data.
 *
 * @param size_buffer The size of the memory reserved to save the RTP data. In some case
 * this memory is not exactly know (e.g. depending on JPEG compression). Therefore one need
 * to set a "big" memory to handle the biggest type of frame.
 *
 */
bool RTPData::Init(int size_buffer) {
	length=0;
	capacity=size_buffer;
	data = new uint8_t[size_buffer];
	if(data==NULL) return false;
	state=RTPDATA_ENDPARTIAL;	//Simulate the we can start a new frame but old one is not correct.
	return true;
}

/**
 * @brief Default destructor.
 *
 * This destructor release the memory reserved for the RTPData.
 */
RTPData::~RTPData() {
	X7S_PRINT_DEBUG("RTPData::~RTPData()");
	delete [] data;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methodes.
//---------------------------------------------------------------------------------------


/**
 * @brief Check if the value of an RTPData frame fit the RTP header of the received packet.
 *
 * @param rtp_hdr A pointer on the RTP header of the received packet.
 * @return @true if the ssrc, timestamp and payload type are the same, @false otherwise.
 */
bool RTPData::CheckFFields(const rtp_hdr_t *rtp_hdr) const {
	if(this->ssrc == rtp_hdr->ssrc
			&& this->tstamp == rtp_hdr->ts
			&& this->ptype == rtp_hdr->pt) {
		return true;
	}
	else return false;
}

/**
 * @brief Parse the header of an RTP packet and set the field of the RTP data.
 *
 * We first call ::CheckFFields(). If this function returns:
 * 		- @true, the ::p_rtp_hdr is set to the value of the header parsed.
 * 		- @false, the ::p_rtp_hdr is set to false.
 *
 * @param rtp_hdr A pointer on the RTP header of the received packet.
 * @return @true if the ssrc, timestamp and payload type are the same, @false otherwise.
 */
bool RTPData::CheckSetHeader(const rtp_hdr_t *rtp_hdr) {
	if(CheckFFields(rtp_hdr)) {
		p_rtp_hdr=rtp_hdr;
		return true;
	}
	else {
		p_rtp_hdr=NULL;
		return false;
	}
}

/**
 * @brief Print a resume of the field in RTPData.
 */
std::string RTPData::toString() {
	std::stringstream sstr;
	sstr << "RTP: ssrc=0x" << std::hex << ssrc << ", ts=" << std::dec <<  tstamp;
	sstr << ", length=" << std::setw(7) << length << ", count=" << count;
	if(state==RTPDATA_ENDOK) sstr << " ENDOK";
	sstr << ";";
	return sstr.str();
}

//---------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Static Methodes.
//---------------------------------------------------------------------------------------


/**
 * @brief Parse the header of RTP to find which type of RTPData we have.
 *
 * This method read the value in the packet to correctly set
 * a structure of rtp_hdr_t.
 *
 * @note This method is static because
 *
 * @param 	pkt_data 	A pointer on the RTP packet received.
 * @param	rtp_hdr		A pointer on the structure of the RTP header.
 * @return 	the size of the RTP header.
 */
int RTPData::ParseRTPHeader(const uint8_t *pkt_data, rtp_hdr_t* rtp_hdr) {


	X7sSocket::buff_ntohl((uint8_t*)rtp_hdr,pkt_data,SIZEIB_RTP_HEAD_MIN); //Convert from BE to LE the 3*32-bits of the RTP header.
	if(rtp_hdr->cc > 0) X7sSocket::buff_ntohl((uint8_t*)rtp_hdr->csrc,pkt_data,rtp_hdr->cc);

	return (uint32_t)(SIZEIB_RTP_HEAD_MIN+rtp_hdr->cc*4);
}


//---------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Private/Protected Methodes.
//---------------------------------------------------------------------------------------


/**
 * @brief Check if the size of the buffer is sufficient to copy all the packet.
 */
bool RTPData::CheckSize() {
	if(this->length>this->capacity) {
		X7S_PRINT_WARN("RTPData::CheckSize()", "buff=%d < total=%d",
				capacity,length);
		return false;
	}
	else return true;
}

/**
 * @brief Set the field of an RTPData frame with the RTP header of the received packet.
 *
 *	This function is typically called when the first packet of an RTP frame arrive. Therefore
 *	the value ::count and ::size_buff are reset to zero. The pointer ::p, go back to
 *	the beginning of its memory.
 *
 * @param rtp_hdr A pointer on the RTP header of the received packet.
 */
void RTPData::SetNewData(const rtp_hdr_t *rtp_hdr) {
	this->ssrc = rtp_hdr->ssrc;
	this->tstamp = rtp_hdr->ts;
	this->ptype = rtp_hdr->pt;
	this->count = 0;
	this->length=0;
	this->state=RTPDATA_START;

	X7S_PRINT_DUMP("RTPData::SetNewData()","New Frame with TS=%d",this->tstamp);
}
