#include "X7sSocketMAC.hpp"

#define IX7S_NET_MAC_HDRSIZE 16


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sNetTools.hpp"	//X7S_PRINT_xxx,...



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor and Initialization.
//---------------------------------------------------------------------------------------

/**
 * @brief The constructor of a MAC Socket.
 *
 * By default the socket are initiate in blocking mode (rcvtimeout=-1,sndtimeout=-1)
 *
 * @note In case you want to receive information only the port of the local IP is important,
 * the other field will be erased.
 */
X7sSocketMAC::X7sSocketMAC(const uint8_t* eha_local, const uint8_t* eha_remote, uint16_t proto)
:X7sSocket(X7S_NET_SOCKTYPE_MAC), pktbuff(new uint8_t[X7S_NET_MAC_MTU])
{

	//Set pointer on etherner header.
	p_payload=(uint8_t*)pktbuff;
	eha_remote= p_payload;

	p_payload+=X7S_NET_MAC_EHASIZE;
	eha_local= p_payload;

	p_payload+=X7S_NET_MAC_EHASIZE;
	//proto=(uint16_t*)p_payload;

	//Set two zeros to align on 32-bits.
	p_payload+=2;
	p_payload[0]=0;
	p_payload[1]=0;

	//And finally point on the beginning of the data.
	p_payload=(uint8_t*)pktbuff+X7S_NET_MAC_HDRSIZE;

	//Then copy the data to their correct position
	memcpy(this->eha_local,eha_local,X7S_NET_MAC_EHASIZE);
	memcpy(this->eha_remote,eha_remote,X7S_NET_MAC_EHASIZE);
	memcpy(this->proto,&proto,2);

	state = X7S_NET_SOCKSTATE_INIT;
}



/**
 * @brief Try to close the socket and them delete the object.
 */
X7sSocketMAC::~X7sSocketMAC() {
	X7S_PRINT_DEBUG("~X7sSocketMAC(): %s ",toString().c_str());
	//First try to close the socket.
	if(state!=X7S_NET_SOCKSTATE_CLOSED && state!=X7S_NET_SOCKSTATE_INIT) {
		this->Close();
	}
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Functions.
//---------------------------------------------------------------------------------------


/**
 * @brief Open the MAC Socket connection.
 *
 * This function need to be call for Client and Server.
 *
 * @return X7S_NET_SOCKERR_NOERROR or the type of error.
 */
int X7sSocketMAC::Open() {

	state=X7S_NET_SOCKSTATE_OPENED;
	return X7S_NET_SOCKERR_NOERROR;
}

/**
 * @brief Open and Listen on a MAC Socket.
 *
 * This socket is mainly in server mode
 * (waiting to receive some data from another computer).
 *
 * @return X7S_NET_SOCKERR_NOERROR or the type of error.
 */
int X7sSocketMAC::Bind() {

	//First open the socket.
	int ret= this->Open();

	if(ret!=X7S_NET_SOCKERR_NOERROR) return ret;

	state=X7S_NET_SOCKSTATE_BINDED;
	return X7S_NET_SOCKERR_NOERROR;
}

/**
 * @brief Close the MAC Socket connection.
 */
int X7sSocketMAC::Close() {
	pcap_close(adhandle);
	state=X7S_NET_SOCKSTATE_CLOSED;
	X7S_PRINT_INFO("X7sSocketMAC::Close(): %s ",toString().c_str());
	return X7S_NET_SOCKERR_NOERROR;
}

/**
 * @brief Send a packet through a MAC socket..
 * The buffer and the nBytes should be correctly assigned.
 *
 * @param buffer The pointer on the beginning of the data memory.
 * @param nBytes The size in bytes of the data that need to be send.
 * This size need to be less than X7S_NET_MAC_PAYLOAD_MSIZEIB.
 * @return returns 0 on success and -1 on failure.
 */
int X7sSocketMAC::SendPkt(uint8_t *buffer, const int& nBytes)  {

	int ret;
	memcpy((void*)pktbuff,buffer,nBytes);	//Copy data buffer to the packet buffer.
	ret = pcap_sendpacket(adhandle,(const uint8_t*)pktbuff, X7S_NET_MAC_HDRSIZE+nBytes);
	return ret;
}

/**
 * @brief Receive a packet through a MAC socket.
 * The buffer and the nBytes should be correctly assigned.
 *
 * @param buffer The pointer on the beginning of the buffer memory.
 * @param nBytes The size in bytes of the encapsulated data in the S4NPacket.
 * @return The size in byte of the packet received. Otherwise we can return:
 * 		-  0 if no packet have being read in blocking mode.
 * 		- -1 if a timeout occurs in blocking mode.
 */
int X7sSocketMAC::RecvPkt(uint8_t *buffer, const int& nBytes) {
//#ifdef WIN32
//	if(timeout_rcv>0) {
//		FD_ZERO(&m_readfs);
//		FD_SET(m_socket, &m_readfs);
//		if(select(m_socket + 1, &m_readfs, NULL, NULL, &m_tvto) == -1) {
//			X7S_PRINT_ERROR("X7sSocketMAC::RecvPkt()","Error in X7sSocketMAC::SetRcvTimeOut: Select %s",GetErrNoDesc().c_str());
//			return -1;
//		}
//		if(!FD_ISSET(m_socket, &m_readfs)) { return -1; }
//	}
//#endif
//	socklen_t size = sizeof(sin_remote);
//	return recvfrom(m_socket,(char*)buffer, nBytes, m_flags,(SOCKADDR *)&(sin_remote),&size);
	return -1;
}

/**
 * @brief Receive packet through a MAC socket and check errors.
 * @see X7sSocketMAC::RecvPkt() and X7sSocket::RecvPktChk().
 */
int X7sSocketMAC::RecvPktChk(uint8_t *buffer, const int& nBytes) {
	X7sNetChrono chrono;
	int ret = X7sSocket::RecvPktChk(buffer,nBytes);
	if(ret < 0) {
#ifdef WIN32
		X7S_PRINT_DEBUG("X7sSocket::RecvPkt() %d > EAGAIN (Timeout: Try again) = 11 (elapsed %d ms)",ret,chrono.Time());
#else
		X7S_PRINT_DEBUG("X7sSocket::RecvPkt() %d > %s (elapsed %d ms)",ret,X7sSocketMAC::GetErrNoDesc().c_str(),chrono.Time());
#endif
	}
	return ret;
}

/**
 * @brief Set timeout of the socket.
 *
 * The socket can have different behaviour according to the value of the timeout.
 * 	- nof_milli=[1,INT_MAX] -> When the socket enter use recv() or recvfrom().
 * 	- nof_milli=0			-> The socket don't wait and directly return from the calling function.
 * 	- nof_milli=-1			-> The socket WAITALL packet until a message arrive.
 *
 * @param nof_milli time of timeout in milliseconds.
 * @return @true if the timeout has been set correctly, @false otherwise.
 * @warning In windows it seems that the SO_RCVTIMEO options is not working.
 * Therefore we have decided to use a select() method in the
 * X7sSocketMAC::RecvPkt() function.
 *
 */
bool X7sSocketMAC::SetRcvTimeOut(int nof_milli) {
	//We set the default timeval structure, also used by select().
	m_tvto.tv_sec = (int)(((double)nof_milli)/1000.0);
	m_tvto.tv_usec = (nof_milli%1000)*1000;
	int result;

#if(WIN32)
	char *p_t = (char *)&m_tvto;
	int s_t = sizeof(struct timeval);
#else
	timeval *p_t = &m_tvto;
	socklen_t s_t = sizeof(struct timeval);
#endif


	if(nof_milli>0) {
		if (result < 0) {
			X7S_PRINT_ERROR("X7sSocketMAC::SetRcvTimeOut()","Error in X7sSocketMAC::SetRcvTimeOut: %s",GetErrNoDesc().c_str());
			return false;
		}

		SetFlags(X7S_NET_SOCKFLAG_TIMEDOUT);
	}
	else {
		if(nof_milli==0) SetFlags(X7S_NET_SOCKFLAG_NOWAIT);
		else SetFlags(X7S_NET_SOCKFLAG_WAITALL);
	}
	timeout_rcv = nof_milli;	//Change value of rcv timeout.
	return true;
}

/**
 * @brief Obtain the size of the receiving buffer for a socket.
 * @note In linux, the value returned by the system is normally the double than
 * the one setted by the system. In this function we have decided to return the same value
 * under windows and linux. Therefore the value of buffsize in unix, is divide by two before
 * being returned.
 * @see General documentation on MAC buffer: http://www.29west.com/docs/THPM/MAC-buffer-sizing.html
 * @see http://msdn.microsoft.com/en-us/library/ms738544(VS.85).aspx
 *
 */
int X7sSocketMAC::GetRcvBufferSize() {
	int buffsize=0;
#if WIN32
	int s_t=sizeof(buffsize);
#else
	socklen_t s_t = sizeof(buffsize);
#endif

//	if(getsockopt(m_socket,SOL_SOCKET,SO_RCVBUF,(char*)(&buffsize),&s_t)==-1) {
//		X7S_PRINT_ERROR("X7sSocketMAC::GetRcvBufferSize()",GetErrNoDesc().c_str());
//		return -1;
//	}
#ifndef WIN32
	buffsize/=2;	//Divide buffsize by two for linux system
#endif
	return buffsize;
}

/**
 * @brief Set the size of the receiving buffer for a socket.
 * @note By default the size of the receiving buffer depends on the system:
 * 		- In windows:		8Kb (8192b)
 * 		- In unix,linux: 	55Kb (56320b)
 */
bool X7sSocketMAC::SetRcvBufferSize(int buffsize) {
//	if(setsockopt(m_socket,SOL_SOCKET,SO_RCVBUF,(char*)(&buffsize),sizeof(buffsize))==-1) {
//		X7S_PRINT_ERROR("X7sSocketMAC::SetRcvBufferSize()",GetErrNoDesc().c_str());
//		return false;
//	}
	return true;
}


/**
 * @brief Return the last error of the socket as a string.
 */
std::string X7sSocketMAC::GetErrNoDesc() {
	return "";
}


/**
 * @brief The debug message of the class.
 * @note This message can be used only when the symbol
 * __WXX7S_LOGLEVEL__ is defined during compilation.
 */
std::string X7sSocketMAC::toString() {
	std::stringstream sstr;
	sstr << X7sSocket::toString();
	sstr << ">>";
	if(this->IsOk()) {
		sstr << " (rcv_buff=" << this->GetRcvBufferSize() << "), ";
	}
	//sstr << SockAddrToStr(sin_local);

	if(this->state==X7S_NET_SOCKSTATE_BINDED) sstr << " <- ";
	else sstr << " -> ";

	//sstr << SockAddrToStr(sin_remote);
	//printf("%s\n",sstr.str().c_str());
	return sstr.str();
}

