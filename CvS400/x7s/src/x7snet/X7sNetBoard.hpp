/**
 *  @file
 *  @brief Contains the structure X7sNetBoard and X7sNetCamera.
 *  @date 21-abr-2009
 *  @author Benoit RAT
 */

#ifndef X7SNETBOARD_HPP_
#define X7SNETBOARD_HPP_

#include "x7snet.h"


#define X7S_NET_BOARD_NCAMERA 4
#define INCR_CAMID(cam_id)  \
    ( cam_id = ((cam_id + 1) % X7S_NET_BOARD_NCAMERA) )



/**
 *	@brief Structure to keep the parameters of each
 */
class X7sNetCamera {
public:
	X7sNetCamera();
	~X7sNetCamera();
	int ID;
	bool is_new;
	uint32_t ar_param[S7P_END_PRM_CAM-S7P_PRM_CAM_ENABLE];	//define an array with all the parameters
	uint32_t *pval;		//!< A pointer that can be use to access in an easier way to the parameters.
	X7sNetFrame *frame;	//!< Pointer on a NetFrame (ImageData & Data).
};



/**
 *	@brief Structure to keep parameters of one board and its camera parameters.
 *	@see X7sNetCamera.
 */
class X7sNetBoard {
public:
	X7sNetBoard();
	~X7sNetBoard();
	char ip_str[X7S_NET_IP_STRLENGTH];
	uint32_t ar_param[S7P_END_PRM_BRD-S7P_PRM_BRD_IP];
	uint32_t *pval; //A pointer that can be use to access in an easier way to the parameters.
	X7sNetCamera* cam[X7S_NET_BOARD_NCAMERA];
};

#endif /* X7SNETBOARD_HPP_ */
