/**
 *  @file
 *  @brief Contains the class X7sDataRcvr
 *  @date 23-abr-2009
 *  @author Benoit RAT
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SDATARCVR_HPP_
#define X7SDATARCVR_HPP_

#include "x7snet.h"
#include "X7sNetBoard.hpp"

//Prototype
X7sByteArray* x7sCreateByteArray(int size_max);
void x7sReleaseByteArray(X7sByteArray** pByteArray);

enum {
	X7S_NET_DRCVR_RTP,
	X7S_NET_DRCVR_S7P,
};

class X7sSocket;	//Forward declaration: this object is included in the .cpp file.


/**
 *	@brief Generic Class to handle reception of data through a socket.
 */
class X7sDataRcvr {
public:
	X7sDataRcvr(int proto, X7sSocket *sock,X7sNetBoard *board);
	virtual ~X7sDataRcvr();

	bool SetSocketTimeOut(int nof_milli);
	int IsNewFrame(int camID);
	X7sNetFrame* CaptureMetaFrame(int camID);
	virtual int Reset();

protected:
	virtual int FlushMetaFrame()=0;
	int proto;
	X7sSocket *sock;
	X7sNetBoard *board;
};




#endif /* X7SDATARCVR_HPP_ */
