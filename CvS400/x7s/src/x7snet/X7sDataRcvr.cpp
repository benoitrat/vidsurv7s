#include "X7sDataRcvr.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sNetTools.hpp"	//X7S_PRINT_xxx,...
#include "X7sNetBoard.hpp"
#include "X7sSocket.hpp"
#include <cstdlib>

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor and Initialization.
//---------------------------------------------------------------------------------------

X7sDataRcvr::X7sDataRcvr(int proto, X7sSocket *sock,X7sNetBoard *board)
:proto(proto), sock(sock), board(board)
 {
	if(sock==NULL || board==NULL) {
		X7S_PRINT_ERROR("X7sDataRcvr::X7sDataRcvr()","sock=%X, board=%X",sock,board);
	}

 }

/**
* @brief Close and destroy the socket used to construct the X7sDataRcvr.
*/
X7sDataRcvr::~X7sDataRcvr() {
	X7S_PRINT_DEBUG("X7sDataRcvr::~X7sDataRcvr()");
	if(sock) delete sock;	//Delete the socket used to construct the object.
}


int X7sDataRcvr::Reset() {
	return X7S_NET_RET_OK;
}


/**
* @brief Capture the actual frame.
*
* This function just return a frame if it exists for this camera ID.
*
* @note If this function is called another time we obtain the next "ITAC" frame (In Time And Camera).
*
* @return If IsNewFrame() is @true for this camera, we return a new frame.
* Otherwise it return @NULL.
*/
X7sNetFrame* X7sDataRcvr::CaptureMetaFrame(int camID) {
	if(camID > CAMID_4) { return NULL; }
	X7sNetCamera *cam = board->cam[camID];
	if(!cam->pval[S7P_PRM_CAM_ENABLE]) { return NULL; }

	if(cam->is_new) {
		cam->is_new=false;
		return cam->frame;
	}
	//If frame is not complete
	return NULL;
}



/**
* @brief Check if there is a new frame.
*
* First flush all the buffer into the X7sNetCamera buffer
* than check if the actual frame is new or not.
*
* @return X7S_NET_RET_OK if the actual frame is new. Otherwise it returns a 
* @ref X7S_NET_RET_XXX error code because this frame has been capture using the CaptureMetaFrame() function.
*/
int X7sDataRcvr::IsNewFrame(int camID) {
	int ret=X7S_NET_RET_UNKOWN;
	X7S_FUNCNAME("X7sDataRcvr::IsNewFrame()");


	//Check if camera ID is correct and if the corresponding camera is available.
	if(camID <= CAMID_4) {
		if(board->cam[camID]->pval[S7P_PRM_CAM_ENABLE]) {

			X7S_PRINT_DEBUG(funcName,"CAMID_%d =================",camID+1);

			//If the camera has yet a new frame, return it directly.
			if(board->cam[camID]->is_new) return X7S_NET_RET_OK;

			//Then flush until having the camera with new or an error occurs while flushing.
			while(!board->cam[camID]->is_new)
			{
				ret=FlushMetaFrame();
				if(ret!=X7S_NET_RET_OK) {
					if(ret==X7S_NET_RET_LOSTPKT)
					{
						X7S_PRINT_INFO(funcName,"FlushFrame() returns %s (%s)",X7sNetSession::GetErrorMsg(ret),board->ip_str);
					}
					else
					{
						X7S_PRINT_DEBUG(funcName,"FlushFrame() returns %s (%s)",X7sNetSession::GetErrorMsg(ret),board->ip_str);
					}
					break;
				}
			}
			//If the camera has obtain a new frame, we return OK
			if(board->cam[camID]->is_new) return X7S_NET_RET_OK;
		}
		else
		{
			//Camera is not enable.
		}
	} else
	{
		X7S_PRINT_INFO(funcName,"Bad cam_id %d",camID);
	}
	return ret;
}

bool X7sDataRcvr::SetSocketTimeOut(int nof_milli)
{
	 return sock?sock->SetRcvTimeOut(nof_milli):false;
}



/**
* @brief Create an simple array of byte.
* The size given is the maximum size reserved in the memory,
* however the size of usefull data is given by length.
* @note This function use malloc() and free() because it must be compatible
* with C code.
*/
X7sByteArray* x7sCreateByteArray(int size_max)
		{
	X7sByteArray* pByteArray = (X7sByteArray*)malloc(sizeof(X7sByteArray));
	pByteArray->type=X7S_BYTEARRAY_UNKNOWN;
	pByteArray->length=0;
	pByteArray->capacity=0;
	pByteArray->data=NULL;
	if(size_max>0)
	{
		pByteArray->capacity=size_max;
		pByteArray->data = (uint8_t*)calloc(sizeof(uint8_t),pByteArray->capacity);
	}
	return pByteArray;
		}



/**
* @brief Release the memory reserved for this buffer.
* @note This function use malloc() and free() because it must be compatible
* with C code.
* @see x7sCreateByteArray().
*/
void x7sReleaseByteArray(X7sByteArray** pByteArray)
{
	X7sByteArray* byteArray=*(pByteArray);
	if(byteArray)
	{
		X7S_PRINT_DEBUG("x7sReleaseNetBuffer()");
		byteArray->length=0;
		byteArray->capacity=0;
		free(byteArray->data);
		byteArray->data=NULL;
		free(byteArray);
	}
	*(pByteArray)=NULL;
}


/**
* @brief Create a X7sNetFrame.
* Reserve the memory and initiate all the parameters
* to create a X7sNetFrame also call a meta-frame.
*
* @param ID ID of the Net frame (correspond to camera ID)
* @param dtype	type of data (JPEG, Xvid Frame, ...)
* @param dsize	maximum size allowed for the data
* @param mdtype type of metadata (can be set to S7P_MTYPE_NONE and therefore will be asked)
* @param mdsize maximum size allowed for the metadata.
* @return A X7sNetFrame with its memory correctly reserved.
*/
X7sNetFrame* x7sCreateNetFrame(int ID, int dtype, size_t dsize, int mdtype, size_t mdsize) {
	X7sNetFrame *frame=NULL;

	X7S_PRINT_DEBUG("x7sCreateNetFrame()");

	frame=new X7sNetFrame;


	frame->camID=ID;
	frame->height=-1;
	frame->width=-1;
	frame->nChannels=0;

	frame->ID=0;
	frame->counter=0;

	frame->dtype=dtype;
	frame->data=x7sCreateByteArray(dsize);

	frame->mdtype=mdtype;
	frame->mdata=x7sCreateByteArray(mdsize);

	return frame;
}


/**
* @brief Release the memory reserved for the buffer (image and metadata)
* reserved in a NetFrame.
*/
void x7sReleaseNetFrame(X7sNetFrame** pNetFrame)
{
	X7sNetFrame* netFrame=*(pNetFrame);
	if(netFrame)
	{
		X7S_PRINT_DEBUG("x7sReleaseNetFrame()");
		x7sReleaseByteArray(&(netFrame->data));
		x7sReleaseByteArray(&(netFrame->mdata));
		memset(netFrame,sizeof(netFrame),0);
		delete netFrame;
	}
	*(pNetFrame)=NULL;
}

