/**
 *  @file
 *  @brief Contains the class X7sIoLog
 *  @date 21-abr-2009
 *  @author Benoit RAT
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SIOLOG_HPP_
#define X7SIOLOG_HPP_


#include <ctime>	//!< Use clock(),...
#include <string>	//!< Use string during debbuggin
#include <sstream>	//!< Use string stream
#include <cstdarg> 	//!< Use va_list(),...
#include <cstdio> 	//!< Use vprintf(),...
#include "x7sdef.h"	//!< Definition of the X7S_LOGLEVEL_xxx
#include "x7sio.h"	//!< To link with the interface functions


/**
 *	@brief A class to print in message / error in log files-
 */
class X7sIoLog {
public:
	static void PrintLevel(const char *funcname,const char *file, int line, int level, const char* szFormat=NULL, ...);
	static bool SetLevel(int log_level);
	static bool IsDisplay(int level);
	static FILE* ferr;
	static FILE* fout;
	static int noError;
private:
	static void Print(bool with_time, const char *prefix, const char *funcname,const char *file, int line, FILE *stream, const char *szFormat, va_list args);
	static int level;
	static const char * names[X7S_LOGLEVEL_END];
};

#define   X7S_LOG(funcname,lvl,...)      if (X7sIoLog::IsDisplay(lvl)) X7sIoLog::PrintLevel(funcname,__FILE__, __LINE__,lvl,##__VA_ARGS__)

#define   X7S_PRINT_ERROR(funcname,...)     X7S_LOG(funcname,X7S_LOGLEVEL_LOWEST,##__VA_ARGS__)
#define   X7S_PRINT_WARN(funcname,...)      X7S_LOG(funcname,X7S_LOGLEVEL_LOW,##__VA_ARGS__)
#define   X7S_PRINT_INFO(funcname,...)      X7S_LOG(funcname,X7S_LOGLEVEL_MED,##__VA_ARGS__)
#ifdef DEBUG
#define   X7S_PRINT_DEBUG(funcname,...)		X7S_LOG(funcname,X7S_LOGLEVEL_HIGH,##__VA_ARGS__)
#define   X7S_PRINT_DUMP(funcname,...) 		X7S_LOG(funcname,X7S_LOGLEVEL_HIGHEST,##__VA_ARGS__)
#else
#define X7S_PRINT_DEBUG(funcname,...)
#define X7S_PRINT_DUMP(funcname,...)
#endif



#define X7S_CHECK_WARN(funcName,condition,ret,...) \
		if(!(condition)) \
		{ \
			 X7S_LOG(funcName,X7S_LOGLEVEL_LOW,##__VA_ARGS__); \
			 return ret; \
		}

#define X7S_CHECK_ERROR(funcName,condition,ret,...) \
		if(!(condition)) \
		{ \
			 X7S_LOG(funcName,X7S_LOGLEVEL_LOWEST,##__VA_ARGS__); \
			 return ret; \
		}




#endif /* X7SIOLOG_HPP_ */
