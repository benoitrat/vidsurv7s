#include "X7sBinReader.hpp"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------

#define IX7S_IO_BIN_READER_MAX_SIZE 52428800 //!< Maximum size of a file that can be load is 50Mb


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
X7sBinReader::X7sBinReader()
:X7sFrameReader(X7S_IO_TYPE_BIN), pFile(NULL)
{
}

X7sBinReader::~X7sBinReader()
{
	if(pFile) fclose(pFile);
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------
bool X7sBinReader::Setup() {
	//The first time we are here
	frame_id = m_params["f_start"]->toIntValue();
	ffpath=m_params["fpath_prefix"]->toString();
	ffpath+=m_params["im_path"]->toString();
	is_setup=true;
	return true;
}

/**
* @brief Grabs frame from several image files.
* @return @true if the frame is correctly grabbed, false otherwise.
* @see RetrieveFrame()
*/
bool X7sBinReader::GrabFrame() {

	long lSize;
	size_t result;

	if(is_exit) return false;
	if(pFile) fclose(pFile);	//Close the previous file.


	//Get the new name location.
	snprintf(fname,X7S_IO_FNAME_MAXLENGTH,ffpath.c_str(),frame_id);
	frame_id++; //Go to the next frame

	//Open the file with the given name
	pFile = fopen (fname, "rb" );
	if (pFile==NULL) {
		printf("%s can not be loaded\n",fname);
		return false;
	}

	// obtain file size and check if it is OK
	fseek (pFile , 0 , SEEK_END);
	lSize = ftell (pFile);
	rewind (pFile);
	if(lSize > IX7S_IO_BIN_READER_MAX_SIZE)
	{
		printf("Filesize of %s is too big (%.3f Mb > %.3f Mb)\n",fname,
				((double)lSize)/(1024.0*1024.0),((double)IX7S_IO_BIN_READER_MAX_SIZE)/(1024.0*1024.0));
		return false;
	}


	//Release the old images.
	if(im != NULL) cvReleaseImage(&im);

	//Create the memory for new one with the given filesize
	im = cvCreateImage(cvSize(lSize,1),IPL_DEPTH_8U,1);


	//Finally read the file
	result = fread(im->imageData,1,lSize,pFile);
	if (result != (size_t)lSize) {
		fputs ("Reading error",stderr);
		return false;
	}


	return true;
}


/**
* @brief Gets the image grabbed with GrabFrame().
* @note The frame as the original size not the size define by user
* @return A pointer on a IplImage if the frame is correctly grabbed, @NULL otherwise.
* @see GrabFrame(), RetrieveRFrame().
*/
IplImage* X7sBinReader::RetrieveFrame()
{
	return im;
}

/**
* @brief For this function it is the same as RetriveFrame().
*/
IplImage* X7sBinReader::RetrieveRFrame()
{
	return im;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------
