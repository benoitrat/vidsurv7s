/**
 *  @file
 *  @brief Contains the class X7sNetReader.hpp
 *  @date 08-jun-2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SNETREADER_HPP_
#define X7SNETREADER_HPP_

#include "x7sio.h"


#endif /* X7SNETREADER_HPP_ */
