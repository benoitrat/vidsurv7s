/**
 *  @file
 *  @brief Contains the class X7sSeqReader.hpp
 *  @date 08-jun-2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SSEQREADER_HPP_
#define X7SSEQREADER_HPP_

#include "x7sio.h"

/**
 *	@brief Class for reading frame from a sequence of images.
 *	@ingroup x7sio
 */
class X7sSeqReader : public X7sFrameReader {
public:
	X7sSeqReader();
	virtual ~X7sSeqReader();

	bool Setup();
	bool GrabFrame();
	IplImage* RetrieveFrame();

private:
	std::string ffpath;		//!< Full file path (prefix+filepath+seq_format.ext)
	char fname[256];		//!< buffer to write the true fname of each file.
};


#endif /* X7SSEQREADER_HPP_ */
