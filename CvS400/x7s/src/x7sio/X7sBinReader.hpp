/**
 *  @file
 *  @brief Contains the class X7sBinReader.hpp
 *  @date Jul 20, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SFILESREADER_HPP_
#define X7SFILESREADER_HPP_

#include "x7sio.h"

#define X7S_IO_FNAME_MAXLENGTH 256


/**
 *	@brief The X7sBinReader.
 *	@ingroup x7sio
 */
class X7sBinReader: public X7sFrameReader {
public:
	X7sBinReader();
	virtual ~X7sBinReader();

	bool Setup();
	bool GrabFrame();
	IplImage* RetrieveFrame();
	IplImage* RetrieveRFrame();


private:
	FILE *pFile;				//!< Keep a pointer on a file.
	std::string ffpath;		//!< Full file path (prefix+filepath+seq_format.ext)
	char fname[256];		//!< buffer to write the true fname of each file.
};


#endif /* X7SFILESREADER_HPP_ */
