/**
 *  @file
 *  @brief Contains the class X7sVidWriter.hpp
 *  @date 08-jun-2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SVIDEOWRITER_HPP_
#define X7SVIDEOWRITER_HPP_

/**
 *	@brief The X7sVidWriter write frame in videos.
 *	@ingroup x7sio
 */
#include "x7sio.h"

class X7sVidWriter: public X7sFrameWriter {
public:
	X7sVidWriter(const char* filename, int fourcc, double fps, CvSize frame_size, int is_color=1);
	X7sVidWriter(const char* filename);
	X7sVidWriter(const TiXmlNode *node);
	virtual ~X7sVidWriter();
	virtual bool Setup();
	int WriteFrame(IplImage *pImg);

protected:
	void Init();

private:
	CvVideoWriter* m_writer;
	bool m_isToFlip;

	//Value configure by XML
	CvSize m_frameSize;
	int m_fourcc;
	double m_fps;
	int m_isColor;



};

#endif /* X7SVIDEOWRITER_HPP_ */
