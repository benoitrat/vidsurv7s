#include "X7sNetReader.hpp"
#include "x7sxml.h"
#include "x7snet.h"
#include "X7sIoLog.hpp"
#include <cctype> //Tolower(), islower()...


using std::cout;
using std::endl;

/**
* @brief Constructor of the X7sNetReader.
* @param netsession A pointer on a net-session.
* @param cam_id The corresponding camera ID for this reader.
*/
X7sNetReader::X7sNetReader(X7sNetSession *netsession, int cam_id)
:X7sFrameReader(X7S_IO_TYPE_NET), netsession(netsession), cam_id(cam_id), is_internal(false)
 {
	Init();
	Setup();
 }

/**
* @brief Create a NetReader Using XML configuration to open the netsession.
*
@code
<param name="ip_remote">192.168.1.7</param>
<param name="port_remote">18000</param>
<param name="port_local">50000</param>
<param name="ssrcid">7777777</param>
@endcode
*
* @note This function is a simple way to load a frame from a board through Internet.
* @param node
* @return
*/
X7sNetReader::X7sNetReader(const TiXmlNode *node)
:X7sFrameReader(X7S_IO_TYPE_NET), netsession(NULL), cam_id(CAMID_1), is_internal(true)
 {
	//Declare variable
	uint32_t ip_remote;
	uint16_t port_local,port_remote;

	//Create the parameters list.
	m_params.Insert(X7sParam("ip_remote",X7S_XML_ANYID,""));
	m_params.Insert(X7sParam("port_remote",X7S_XML_ANYID,18000,X7STYPETAG_U16));
	m_params.Insert(X7sParam("port_local",X7S_XML_ANYID,50000,X7STYPETAG_U16));
	m_params.Insert(X7sParam("ssrcid",X7S_XML_ANYID,(int)0xABCDEF01,X7STYPETAG_U32));
	m_params.Insert(X7sParam("cam_id",X7S_XML_ANYID,cam_id,X7STYPETAG_INT,0,3));
	m_params["cam_id"]->LinkParamValue(&cam_id);

	Init();

	//Obtain the parameters from XmlNode
	this->ReadFromXML(node);

	//Set them to a file.
	ip_remote=X7sNet_GetIP(m_params["ip_remote"]->toString().c_str());
	port_remote=(uint16_t)m_params["port_remote"]->toIntValue();
	port_local=(uint16_t)m_params["port_local"]->toIntValue();


	netsession = new X7sNetSession();
	int ret=netsession->Connect(ip_remote,port_remote,port_local);
	if(ret==X7S_NET_RET_OK) {
		netsession->SetParam(S7P_PRM_BRD_SSRCID,(uint32_t)m_params["ssrcid"]->toIntValue(),CAMID_ALL);
		this->Setup();
		netsession->Start();
	}
 }



/**
* @brief Default destructor.
*
* Destroy the netsession created by the
* constructor X7sNetReader(const TiXmlNode *node).
*/
X7sNetReader::~X7sNetReader()
{
	if(is_internal && netsession) {
		netsession->Stop();
		delete netsession;
	}
}


/**
* @brief Init the parameters send to the board.
*/
void X7sNetReader::Init()
{
	metaFrame=NULL;
	im_frame=NULL;
	jpeg_buff=NULL;
	meta_buff=NULL;
	nConsecErrors=5;

	//Create the list of parameters
	m_params.Insert(X7sParam("enable",S7P_PRM_CAM_ENABLE,true,X7STYPETAG_INT,0,1));
	m_params.Insert(X7sParam("mtype",S7P_PRM_CAM_MTYPE,S7P_MTYPE_NONE,X7STYPETAG_U32));
	m_params.Insert(X7sParam("led_switch",S7P_PRM_CAM_SWITCH_LED,false,X7STYPETAG_BOOL));
	m_params.Insert(X7sParam("bg_threshold",S7P_PRM_CAM_BG_THRS,30,X7STYPETAG_U32,5,255));
	m_params.Insert(X7sParam("bg_insDelayLog",S7P_PRM_CAM_BG_INSERT,8,X7STYPETAG_U32,1,8));
	m_params.Insert(X7sParam("bg_refFGDelay",S7P_PRM_CAM_BG_REFRESH,256,X7STYPETAG_U32,1,1024));
	m_params.Insert(X7sParam("bg_refBGDelay",S7P_PRM_CAM_FG_REFRESH,8,X7STYPETAG_U32,1,1024));
	m_params.Insert(X7sParam("bg_ccMinArea",S7P_PRM_CAM_CC_MINAREA,64,X7STYPETAG_U32));
	m_params.Insert(X7sParam("bg_tmp1",S7P_PRM_CAM_TMP1,8,X7STYPETAG_U32));
	m_params.Insert(X7sParam("bg_tmp2",S7P_PRM_CAM_TMP2,8,X7STYPETAG_U32));
	m_params.Insert(X7sParam("bg_tmp3",S7P_PRM_CAM_TMP3,8,X7STYPETAG_U32));

	//Setup the step.
	m_params["bg_threshold"]->SetStep(10);
	m_params["bg_insDelayLog"]->SetStep(10);
	m_params["bg_refFGDelay"]->SetStep(10);
	m_params["bg_refBGDelay"]->SetStep(10);
	m_params["bg_ccMinArea"]->SetStep(5);
}


/**
* @brief Setup the connection and enable the corresponding camera.
* @return @true if everything work fine.
*/
bool X7sNetReader::Setup()
{
	netsession->SetReceiverTimeOut(5);

	//Setup the camera parameters.
	netsession->SetParam(S7P_PRM_CAM_ENABLE,0,CAMID_1);
	netsession->SetParam(S7P_PRM_CAM_ENABLE,1,cam_id);
	netsession->SetParam(S7P_PRM_BRD_SSRCID,0xABCDEF01,CAMID_ALL);

	X7sParam *param=NULL;
	uint32_t val;
	while((param=m_params.GetNextParam(param)))
	{
		if(S7P_PRM_CAM_ENABLE <= param->GetID() && param->GetID() < S7P_END_PRM_CAM)
		{
			val=(uint32_t)param->toIntValue();
			netsession->SetParam(param->GetID(),val,cam_id);
		}
	}


	//Set up that the frame ID will be this size.
	frame_id=0;


	is_setup=true;
	return true;
}

/**
* @brief Grabs a meta-frame from the network session.
* @return @true if a frame was grabbed, false otherwise.
* @see RetrieveFrame()
*/
bool X7sNetReader::GrabFrame()
{
	if(is_exit) {
		return false;
	}

	bool isNewFrame=false;
	//Check if a frame has arrived in the socket buffer.
	for(int i=0;i<nConsecErrors;i++)
		if(netsession->IsNewFrame(cam_id))
		{
			isNewFrame=true;
			break;
		}

	if(isNewFrame) {
		//Capture this frame
		metaFrame = netsession->CaptureMetaFrame(cam_id);
		if(metaFrame) {
			//If it is the first time the image is still empty.
			if(im_frame==NULL) {
				im_frame=cvCreateImage(cvSize(metaFrame->width,metaFrame->height),IPL_DEPTH_8U,3);
				jpeg_buff=cvCreateImageHeader(cvSize(metaFrame->width,metaFrame->height),IPL_DEPTH_8U,0);
				jpeg_buff->nChannels=0;

				if(metaFrame->mdtype!=S7P_MTYPE_NONE) {
					fgmask=cvCreateImage(cvSize(metaFrame->width,metaFrame->height),IPL_DEPTH_8U,1);
					cvSet(fgmask,cvScalarAll(0),NULL);
					meta_buff=cvCreateImageHeader(cvSize(metaFrame->width,metaFrame->height),IPL_DEPTH_8U,0);
					meta_buff->nChannels=0;
				}
			}
			//Transform JPEG into an image
			jpeg_buff->imageData=(char*)metaFrame->data->data;
			jpeg_buff->imageDataOrigin=jpeg_buff->imageData;
			jpeg_buff->imageSize=(int)metaFrame->data->length;
			x7sCvtColor(jpeg_buff,im_frame,X7S_CV_JPEG2BGR);

			//Transform metadata into image
			if(metaFrame->mdtype!=S7P_MTYPE_NONE) {
				meta_buff->imageData=(char*)metaFrame->mdata->data;
				meta_buff->imageSize=(int)metaFrame->mdata->length;
				if(metaFrame->mdtype==S7P_MTYPE_RLE) {
					X7S_CV_SET_CMODEL(fgmask,X7S_CV_HNZ2BGR);
					if(fgmaskr) {
						X7S_CV_SET_CMODEL(fgmaskr,X7S_CV_HNZ2BGR);
					}
					x7sCvtColor(meta_buff,fgmask,X7S_CV_LRLE2GREY);
				}
				if(meta_buff->imageSize==0) {
					X7S_PRINT_INFO("X7sNetReader::GrabFrame()","Meta data exist with type %d and size=0",metaFrame->mdtype);
				}
				//x7sPrintU8Array("Metadata:\n%s\n",mframe->mdata->data,180,"%2X ",8);
			}

			netsession->IsAlive(3);	//Check ping each 30 seconds and try at least 3 pings if there is no response.

			frame_id++;
			X7S_PRINT_DEBUG("X7sNetReader::GrabFrame()","frame_id=%d, frame_ts=%u",frame_id,metaFrame->ID);
			return true;
		}
	}
	return false;
}

/**
* @brief
* @return
*/
IplImage* X7sNetReader::RetrieveFrame()
{
	return im_frame;
}


/**
* @brief Wait X millisec and seek in the video (next/prev/pause/quit)
* according to the key pressed.
*/
int X7sNetReader::KeyEvent(char key) {

	int ID=-1;
	int ret;
	bool isIncre=false;
	X7S_PRINT_INFO("X7sNetReader()::KeyEvent()","%c (0x%02X)",key,key);

	if(isalpha(key))
	{
		isIncre=(isupper(key)!=0);
		switch(tolower(key)) {
		case 't':
			ID=S7P_PRM_CAM_BG_THRS;
			break;
		case 'i':
			ID=S7P_PRM_CAM_BG_INSERT;
			break;
		case 'r':
			ID=S7P_PRM_CAM_BG_REFRESH;
			break;
		case 'b':
			ID=S7P_PRM_CAM_FG_REFRESH;
			break;
		case 'a':
			ID=S7P_PRM_CAM_CC_MINAREA;
			break;
		case 'l':
			ID=S7P_PRM_CAM_SWITCH_LED;
			break;
		default:
			return X7sFrameReader::KeyEvent(key);
		}
	}
	else {
		switch(tolower(key)) {
		case X7S_IO_KEY_LARR:
		case X7S_IO_KEY_RARR:
			ID=S7P_PRM_BRD_FRAMERATE;
			isIncre=(key==X7S_IO_KEY_RARR);
			break;
		default:
			return X7sFrameReader::KeyEvent(key);
		}
	}

	if(ID>0 && m_params[ID]) {
		if(m_params[ID]->Increment(isIncre)) {	//If we can increment the value
			ret=netsession->SetParam(ID,(uint32_t)m_params[ID]->toIntValue(),cam_id);	//We send it.
			if(ret!= X7S_NET_RET_OK) {	//In case a problem occurs during communication
				m_params[ID]->Increment(!isIncre);	//We decrement it.
				return X7S_IO_RET_ERR;
			}
		}
		else {
			X7S_PRINT_WARN("X7sNetReader()::KeyEvent()","Value is at Maximum/Minimum",key);
		}
	}
	return X7S_IO_RET_OK;
}


void X7sNetReader::PrintHelp()
{
	const char *sep="\t- ";
	X7sFrameReader::PrintHelp();
	cout << sep << "t/T 	> Change background threshold (-/+)" << endl;
	cout << sep << "i/I 	> Change  insertion time (-/+)" << endl;
	cout << sep << "r/R 	> Change BG refresh time (-/+)" << endl;
	cout << sep << "b/B 	> Change FG refresh time (-/+)" << endl;
	cout << sep << "a/A 	> Change minimum area	 (-/+)" << endl;
}
