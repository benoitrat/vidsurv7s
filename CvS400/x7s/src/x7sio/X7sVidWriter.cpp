//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------

#include "X7sVidWriter.hpp"
#include "X7sIoLog.hpp"
#include <string>
using namespace std;

#ifndef CV_FOURCC_PROMPT
#define CV_FOURCC_PROMPT -1
#endif

#ifndef CV_FOURCC_DEFAULT
#define CV_FOURCC_DEFAULT CV_FOURCC('I','Y','U','V')
#endif

#define IX7S_CV_FOURCC_PROMPT "PROMPT"
#ifdef WIN32
#define IX7S_CV_FOURCC_CODE CV_FOURCC_PROMPT
#define IX7S_CV_FOURCC_NAME IX7S_CV_FOURCC_PROMPT
#define IX7S_IO_FLIPVIDEO false
#else
#define IX7S_CV_FOURCC_CODE CV_FOURCC_DEFAULT
#define IX7S_CV_FOURCC_NAME "IYUV"
#define IX7S_IO_FLIPVIDEO false
#endif


int ix7sGetCVFourCCFromStr(const std::string& fourcc_name);



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
X7sVidWriter::X7sVidWriter(const char* filename, int fourcc, double fps, CvSize frame_size, int is_color)
:X7sFrameWriter(X7S_IO_TYPE_VID),m_fourcc(IX7S_CV_FOURCC_CODE)
{
	Init();
	if(filename) m_params["fname"]->SetValue(filename);
	else m_params["enable"]->SetValue("0");

	//Set-up the linked value
	m_fourcc=fourcc;
	m_fps=fps;
	m_frameSize=frame_size;
	m_isColor=is_color;
}

X7sVidWriter::X7sVidWriter(const char* filename)
:X7sFrameWriter(X7S_IO_TYPE_VID), m_fourcc(IX7S_CV_FOURCC_CODE)
{
	Init();
	if(filename) m_params["fname"]->SetValue(filename);
	else m_params["enable"]->SetValue("0");

}

/**
 * @brief Create the video writer from the XML node.
 */
X7sVidWriter::X7sVidWriter(const TiXmlNode *node)
:X7sFrameWriter(X7S_IO_TYPE_VID), m_fourcc(IX7S_CV_FOURCC_CODE)
{
	//Init the parameters list
	Init();

	//Obtain the parameters from XmlNode
	ReadFromXML(node);

	if(m_params["fname"]->toString().empty())
		m_params["enable"]->SetValue("0");
}

/**
 * @brief Setup the default parameters.
 */
void X7sVidWriter::Init()
{
	m_writer=NULL;

	m_params.Insert(X7sParam("fname",X7S_XML_ANYID,""));
	m_params.Insert(X7sParam("fourcc",X7S_XML_ANYID,IX7S_CV_FOURCC_NAME));
	m_params.Insert(X7sParam("enable",X7S_XML_ANYID,1,X7STYPETAG_BOOL,0,1));

	m_params.Insert(X7sParam("fps",X7S_XML_ANYID,24.0,X7STYPETAG_DOUBLE,0.0,50.0));
	m_params["fps"]->LinkParamValue(&m_fps);

	m_params.Insert(X7sParam("isColor",X7S_XML_ANYID,1,X7STYPETAG_INT,0,1));
	m_params["isColor"]->LinkParamValue(&m_isColor);

	m_params.Insert(X7sParam("width",X7S_XML_ANYID,-1));
	m_params["width"]->LinkParamValue(&(m_frameSize.width));

	m_params.Insert(X7sParam("height",X7S_XML_ANYID,-1));
	m_params["height"]->LinkParamValue(&(m_frameSize.height));

	m_isToFlip=IX7S_IO_FLIPVIDEO;
}

/**
 * @brief Setup the VideoWriter.
 * @return
 */
bool X7sVidWriter::Setup()
{
	m_isSetup=true;
	return m_isSetup;
}


X7sVidWriter::~X7sVidWriter()
{
	X7S_PRINT_DEBUG("X7sVidWriter::~X7sVidWriter()");
	if(m_writer) cvReleaseVideoWriter(&(m_writer));
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

/**
 * Write an IplImage to a video stream
 * @param pImg	The image must be in 3CU8 or 1CU8
 * @return X7S_IO_RET_OK if the image was successfully written
 */
int X7sVidWriter::WriteFrame(IplImage *pImg)
{

	IplImage *pImOut=NULL;

	if(m_params["enable"]->toIntValue()==false) return X7S_RET_ERR;


	//Create the writer if it doesn't exist.
	if(m_writer==NULL)
	{
		if(m_frameSize.height==-1 && m_frameSize.width==-1)
		{
			m_frameSize=cvGetSize(pImg);
		}

		m_fourcc=ix7sGetCVFourCCFromStr(m_params["fourcc"]->toString());

		//TODO: catch an error here.
		m_writer = cvCreateVideoWriter(m_params["fname"]->toString().c_str(),m_fourcc,m_fps,m_frameSize,m_isColor);

		//If it's still NULL return an error.
		if(m_writer==NULL) return X7S_IO_RET_ERR;
	}

	//Write the logo.
	DrawLogo(pImg);

	//Flip image (OS depending)
	if(m_isToFlip)
	{
		pImOut = x7sCreateCvtImage(pImg,X7S_CV_CVTCOPY);
		cvConvertImage(pImg,pImOut,CV_CVTIMG_FLIP);
	}
	else {
		pImOut=pImg;
	}

	//Then write the frame.
	cvWriteFrame(m_writer,pImOut);

	//Release image if we have created one.
	if(pImOut && pImOut!=pImg) cvReleaseImage(&pImOut);

	return X7S_IO_RET_OK;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

int ix7sGetCVFourCCFromStr(const std::string& fourcc_name)
{
	int fourcc_code;
	if(fourcc_name.compare(std::string(IX7S_CV_FOURCC_PROMPT))==0) fourcc_code=CV_FOURCC_PROMPT;
	else 	{
		if(fourcc_name.length()!=4)
		{
			X7S_PRINT_ERROR("X7sVidWriter::FourCC()","fourcc length is not equal 4 ('%s' not valid) > Using default one",
					fourcc_name.c_str());
			fourcc_code=CV_FOURCC_DEFAULT;
		}
		else fourcc_code=CV_FOURCC(fourcc_name[0],fourcc_name[1],fourcc_name[2],fourcc_name[3]);
	}
	return fourcc_code;
}
