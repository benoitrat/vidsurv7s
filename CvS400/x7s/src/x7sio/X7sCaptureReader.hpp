/**
 *  @file
 *  @brief Contains the class X7sCaptureReader.hpp
 *  @date 08-jun-2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SCAPTUREREADER_HPP_
#define X7SCAPTUREREADER_HPP_

#include "x7sio.h"

/**
 * @brief Abstract class for reading frame from various capture type.
 * @see X7sVidReader
 */
class X7sCaptureReader: public X7sFrameReader {
public:
	X7sCaptureReader(int type);
	virtual ~X7sCaptureReader();

	virtual bool Setup()=0;
	virtual bool reload() { return false; }
	bool GrabFrame();
	IplImage* RetrieveFrame();

protected:
	CvCapture *m_pCapture;
	 cv::VideoCapture m_capture;
	 cv::Mat m_image;
	 IplImage m_iplim; //Temporary structure to (cv1.0 -> cv2.0)
	bool isRetrieved;
};



#endif


