#include "X7sVidReader.hpp"

#include "X7sIoLog.hpp"

X7sVidReader::X7sVidReader()
:X7sCaptureReader(X7S_IO_TYPE_VID)
 {

 }

X7sVidReader::~X7sVidReader()
{
	X7S_PRINT_DEBUG("X7sVidReader::~X7sVidReader()");
}

/**
* @brief Setup the media to grab frames (Check file path, or dimension).
*/
bool X7sVidReader::Setup() {

	X7S_FUNCNAME("X7sVidReader::Setup()");

	std::string ffpath;
	bool ret=false;

	ffpath=m_params["fpath_prefix"]->toString();
	ffpath+=m_params["im_path"]->toString();

	int f_start=m_params["f_start"]->toIntValue();

#ifdef WIN32
	is_flipped=true;	//Flip for videos and maybe webcam ??? need to try it !!!
#else
	is_flipped=false;
#endif

	try
	{
		m_pCapture=cvCaptureFromFile(ffpath.c_str());
		ret=(m_pCapture!=NULL);
	}
	catch ( const std::exception & e )
	{
		X7S_PRINT_WARN(funcName,"%s",e.what());
		ret=false;
	}
	catch ( ... ) // traite toutes les autres exceptions
	{
		X7S_PRINT_WARN(funcName,"Unknown error ...");
		ret=false;
	}

	if(!ret) {
		X7S_PRINT_ERROR(funcName,"Can not open video =%s (%d)",ffpath.c_str(),f_start);
		return ret;
	}

	frame_id=0;	//Reset the frame id.

	//Set the first frame position if videos (May not work properly)
	if(cvSetCaptureProperty(m_pCapture, CV_CAP_PROP_POS_FRAMES,f_start)) {
		X7S_PRINT_INFO(funcName,"Start at frame %d ... ",f_start);
		frame_id=f_start;
		is_setup=true;
		return is_setup;
	}

	if(frame_id<f_start) {
		X7S_PRINT_INFO(funcName,"Wait until reaching start position %d ... ",f_start);
		for(;frame_id<f_start;frame_id++) {
			cvGrabFrame(m_pCapture);
			printf("%d ",frame_id);
		}
		printf(" OK\n");
	}

	is_setup=true;
	return is_setup;
}


/**
 * @brief Reload the video if we can loop and we are at the end of the video
 * @return
 */
bool X7sVidReader::reload()
{
	if(m_isLooping)
	{
		cvReleaseCapture(&m_pCapture); //Close the old video stream
		if(this->Setup() && cvGrabFrame(m_pCapture)) return true;
	}
	return false;
}

///**
// * @brief Grab a frame in the X7sFrameReader::im field.
// *
// * @see GetImage().
// */
//bool X7sVidReader::GrabFrame() {
//	bool ret;
//
//	//Check if the capture has been setup.
//	if(capture==NULL) return false;
//
//	//Grab a frame and retrieve it.
//	if(cvGrabFrame(capture)==false) nErrors++;
//	im=cvRetrieveFrame(capture);
//	if(im!=NULL) frame_id++;
//
//	//Wait at each frame.
//	if(waitN>=0) cvWaitKey(waitN);
//
//	//Check the input frames.
//	return CheckInputFrame();
//}


