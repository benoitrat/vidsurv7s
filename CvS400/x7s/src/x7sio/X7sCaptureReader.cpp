#include "X7sCaptureReader.hpp"
#include "X7sIoLog.hpp"


X7sCaptureReader::X7sCaptureReader(int type)
:X7sFrameReader(type)
{
}

X7sCaptureReader::~X7sCaptureReader()
{
	X7S_PRINT_DEBUG("X7sCaptureReader::~X7sCaptureReader()");
    if(m_pCapture!=NULL) cvReleaseCapture(&m_pCapture);

}

/**
 * @brief Grabs frame from camera or file.
 * This function store internally the frame, exactly like in openCV.
 * @return @true if the frame is correctly grabbed, false otherwise.
 * @see @cv{GrabFrame}, RetrieveFrame().
 */
bool X7sCaptureReader::GrabFrame() {

	if(is_exit) return false;

	X7S_CHECK_WARN("X7sCaptureReader::GrabFrame()",m_pCapture,false,"capture is not open");

	bool ret=(cvGrabFrame(m_pCapture)==1);
	isRetrieved=false;

	//Check if frame is bigger than index
	if(frame_id >= f_end) ret=false;

	//End of the video and reload is false.
	if(ret==false)
		{
		if(reload()==false) return false;
		}

	//Move the index of frame to the next one.
	frame_id++;
	return true;
}

/**
 * @brief Gets the image grabbed with GrabFrame().
 * This function retrieve the frame like OpenCV.
 * @note The frame as the original size not the size define by user
 * @return A pointer on a IplImage if the frame is correctly grabbed, @NULL otherwise.
 * @see GrabFrame(), RetrieveRFrame().
 */
IplImage* X7sCaptureReader::RetrieveFrame() {

	im=cvRetrieveFrame(m_pCapture);

	if(im && is_flipped && !isRetrieved) {
		cvConvertImage(im,im,CV_CVTIMG_FLIP); //Just test it.
		isRetrieved=true;
	}
	return im;
}
