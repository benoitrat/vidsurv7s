#include "x7sio.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sIoLog.hpp"
#include "X7sVidWriter.hpp"
#include <string>

//TODO: These should be replaced by Log functions.
using std::cerr;
using std::endl;
using std::cout;

#define IX7S_IO_FWRITER_TAG "io_writer"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Define/Prototype
//----------------------------------------------------------------------------------------

int ix7sGetIoType(const char *fname);


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
X7sFrameWriter::X7sFrameWriter(int type)
:X7sXmlObject(NULL,IX7S_IO_FWRITER_TAG,type),
pLogo(NULL), logo_pos(cvPoint(-5,5))
{
	X7S_PRINT_DEBUG("X7sFrameWriter::X7sFrameWriter()");
	m_params.Insert(X7sParam("logo_fname",X7S_XML_ANYID,"rpout"));
	m_params.Insert(X7sParam("logo_pos_x",X7S_XML_ANYID,logo_pos.x));
	m_params.Insert(X7sParam("logo_pos_y",X7S_XML_ANYID,logo_pos.y));
	m_params["logo_pos_x"]->LinkParamValue(&(logo_pos.x));
	m_params["logo_pos_y"]->LinkParamValue(&(logo_pos.y));

}

X7sFrameWriter::~X7sFrameWriter()
{
	X7S_PRINT_DEBUG("X7sFrameWriter::~X7sFrameWriter()");
	if(pLogo) cvReleaseImage(&pLogo);
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

void X7sFrameWriter::DrawLogo(IplImage *pImg)
{
	if(pImg==NULL) return;

	if(pLogo==NULL) {
		std::string logo_fname=m_params["logo_fname"]->toString();
		if(!(logo_fname.empty())) {
			pLogo=cvLoadImage(logo_fname.c_str(),CV_LOAD_IMAGE_COLOR);
			if(pLogo==NULL)
			{
				X7S_PRINT_ERROR("X7sFrameWriter::DrawLogo()",
						"Couldn't load filename : %s",
						logo_fname.c_str());
				m_params["logo_fname"]->SetValue("");	//Set string to empty.
				return;
			}
			//TODO: Check pLogo size
		}
	}
	else {
		CvPoint offset;
		if(logo_pos.x >= 0)  offset.x=logo_pos.x;
		else offset.x=pImg->width+logo_pos.x-pLogo->width;

		if(logo_pos.y >= 0)  offset.y=logo_pos.y;
		else offset.y=pImg->height+logo_pos.y-pLogo->height;
		cvSetImageROI(pImg, cvRect(offset.x, offset.y, pLogo->width, pLogo->height));
		cvResize(pLogo, pImg,CV_INTER_LINEAR);
		cvResetImageROI(pImg);
	}
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Friend functions
//----------------------------------------------------------------------------------------

std::ostream &operator<<(std::ostream& out, X7sFrameWriter &fwriter ) {
	out << "Frame Writer" << endl;
	out << "\ttype:" << fwriter.GetType() << endl;
	out << (fwriter.m_params).toString() << endl;
	return out;
}



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Static Functions (Interface)
//----------------------------------------------------------------------------------------
X7sFrameWriter* X7sCreateFrameWriter(const char *fname)
{
	int type=0;
	X7sFrameWriter *fwriter=NULL;

	//Obtain the type of writer given the filename.
	type=ix7sGetIoType(fname);

	//Create the child object.
	switch(type) {
	case X7S_IO_TYPE_VID:
		fwriter = (X7sFrameWriter*)new X7sVidWriter(fname);
		break;
	case X7S_IO_TYPE_SEQ:
		//fwriter = (X7sFrameWriter*)new X7sSeqWriter();
		//break;
	case X7S_IO_TYPE_BIN:
		//fwriter = (X7sFrameWriter*)new X7sBinWriter();
		//break;
	default:
		X7S_PRINT_ERROR("X7sCreateFrameWriter()","Unknown type=%d",type);
		return NULL;
	}

	return fwriter;

}

/**
*
* @param root
* @param root_gen
* @todo Remove the type and find only with filename.
* @return
*/
X7sFrameWriter* X7sCreateFrameWriter(const TiXmlNode* root,const TiXmlNode* root_gen)
{
	int type=0;
	X7sFrameWriter *fwriter=NULL;
	const TiXmlElement *cnode=NULL,*cnode_gen=NULL;	//child node (<io_reader></io_reader>).

	if(root==NULL) {
		X7S_PRINT_ERROR("X7sCreateFrameWriter()","<root></root> node not found");
		return NULL;
	}

	//Go inside the filenode <input></input>
	cnode = root->FirstChildElement(IX7S_IO_FWRITER_TAG);
	if(cnode==NULL) {
		X7S_PRINT_ERROR("X7sCreateFrameWriter()",
				"<%s></%s> tag not found",
				IX7S_IO_FWRITER_TAG,IX7S_IO_FWRITER_TAG);
		return NULL;
	}

	//Get the type parameters
	cnode->Attribute("type",&type);

	//Create the child object.
	switch(type) {
	case X7S_IO_TYPE_VID:
		fwriter = (X7sFrameWriter*)new X7sVidWriter(cnode);
		break;
	case X7S_IO_TYPE_SEQ:
		//fwriter = (X7sFrameWriter*)new X7sSeqWriter();
		//break;
	case X7S_IO_TYPE_BIN:
		//fwriter = (X7sFrameWriter*)new X7sBinWriter();
		//break;
	default:
		X7S_PRINT_ERROR("X7sCreateFrameWriter()","Can't create FrameWriter for type=%d\n",type);
		return NULL;
	}

	//If main xml config is setup try to load it.
	if(root_gen) {
		if(root_gen->FirstChild(IX7S_IO_FWRITER_TAG)) {
			cnode_gen = root_gen->FirstChildElement(IX7S_IO_FWRITER_TAG);
			if(cnode_gen) {
				fwriter->ReadFromXML(cnode_gen);
			}
		}
	}

	//Then load and "overwrite" parameters with the media config.
	fwriter->ReadFromXML(cnode);
	cout << "\n" << (*fwriter) << endl;
	fwriter->Setup();

	return fwriter;
}





int ix7sGetIoType(const char *fname)
{

	if(fname==NULL)
	{
		X7S_PRINT_ERROR("ix7sGetIoType()","Filename is NULL");
		return X7S_IO_RET_ERR;
	}

	const char *ext = strrchr(fname,'.');
	if(ext==NULL) {
		X7S_PRINT_ERROR("ix7sGetIoType()","Filename '%s' without extension!",
				fname);
		return X7S_IO_RET_ERR;
	}

	if(strcmp(ext,".avi")==0) {
		return X7S_IO_TYPE_VID;
	} else if(strcmp(ext,".bin")==0) {
		return X7S_IO_TYPE_BIN;
	} else {
		return X7S_IO_TYPE_SEQ;
	}

	return X7S_IO_RET_ERR;
}
