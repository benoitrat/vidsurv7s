/**
 *  @file
 *  @brief Contains the class X7sVidReader.hpp
 *  @date 08-jun-2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SVIDREADER_HPP_
#define X7SVIDREADER_HPP_


#include "X7sCaptureReader.hpp"

/**
 *	@brief Class for reading frame from video file.
 *	@ingroup x7sio
 */
class X7sVidReader : public X7sCaptureReader {
public:
	X7sVidReader();
	virtual ~X7sVidReader();
	bool Setup();
	bool reload();
};


#endif /* X7SVIDREADER_HPP_ */
