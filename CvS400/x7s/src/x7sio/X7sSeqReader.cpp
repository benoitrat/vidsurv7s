#include "X7sSeqReader.hpp"

X7sSeqReader::X7sSeqReader()
:X7sFrameReader(X7S_IO_TYPE_SEQ)
{
}

X7sSeqReader::~X7sSeqReader()
{
}


bool X7sSeqReader::Setup() {
	//The first time we are here
	frame_id = m_params["f_start"]->toIntValue();
	ffpath=m_params["fpath_prefix"]->toString();
	ffpath+=m_params["im_path"]->toString();
	is_setup=true;
	return true;
}

/**
 * @brief Grabs frame from several image files.
 * @return @true if the frame is correctly grabbed, false otherwise.
 * @see RetrieveFrame()
 */
bool X7sSeqReader::GrabFrame() {

	if(is_exit) return false;

	//Release the old images.
	if(im != NULL) cvReleaseImage(&im);

	//Get the new location.
	snprintf(fname,sizeof(fname),ffpath.c_str(),frame_id);
	im = cvLoadImage(fname,CV_LOAD_IMAGE_COLOR);
	frame_id++; //Go to the next frame

	if(im==NULL) {
		printf("%s can not be loaded\n",fname);
		return false;
	}
	return true;
}


/**
 * @brief Gets the image grabbed with GrabFrame().
 * @note The frame as the original size not the size define by user
 * @return A pointer on a IplImage if the frame is correctly grabbed, @NULL otherwise.
 * @see GrabFrame(), RetrieveRFrame().
 */
IplImage* X7sSeqReader::RetrieveFrame() {
	return im;
}


