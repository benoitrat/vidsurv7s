#include "x7sio.h"
#include "X7sIoLog.hpp"

#include "X7sVidReader.hpp"
#include "X7sSeqReader.hpp"
#include "X7sNetReader.hpp"
#include "X7sBinReader.hpp"
#include <ctime>
#include <map>

typedef std::map<char,X7sParam*> MapParam;

#ifdef WIN32
#define ISFLIPPED 1
#else
#define ISFLIPPED 0
#endif


//These should be replaced by Log functions.
using std::cerr;
using std::endl;
using std::cout;
using std::map;


//------ Public Methods

/**
* @brief Constructor for X7sFrameReader.
* @note As this class is abstract this constructor can not be used to create an object.
* @see CvSeqReader
*/
X7sFrameReader::X7sFrameReader(int type)
:X7sXmlObject(NULL,X7S_IO_TAGNAME_READER,type)
{
	X7S_PRINT_DEBUG("X7sFrameReader::X7sFrameReader()");
	Init();
}

/**
* @brief Destroy the created pointers (IplImage)
*/
X7sFrameReader::~X7sFrameReader()
{

	X7S_PRINT_DEBUG("X7sFrameReader::~X7sFrameReader()");

	//if(im!=NULL) cvReleaseImage(&im);
	//if(imr!=NULL) cvReleaseImage(&imr);
	MapParam *mapFunc = (MapParam*)m_mapParam;
	X7S_DELETE_PTR(mapFunc);


}

/**
* @brief Gets the image grabbed in the user defined resolution.
* The returned image should not be deleted, nor released.
* @return a pointer on a IplImage if the frame can be retrieve, otherwise it return NULL.
* @see @cv{RetrieveImage}, RetrieveFrame(), GrabFrame().
*/
IplImage* X7sFrameReader::RetrieveRFrame() {
	//First Retrieve the original Image
	this->RetrieveFrame();

	//Check the input frames.
	if(!CheckInputFrame()) return NULL;

	if(im!=NULL && imr!=NULL) {
		//Resize frame to the wanted size.
		cvResize(im,imr,CV_INTER_LINEAR);
		return imr;
	}
	return NULL;
}

/**
* @brief Grabs and returns a frame in the original size.
* The returned image should not be deleted, nor released.
* @return a pointer on a IplImage if the frame can be retrieve, otherwise it return NULL.
* @warning Errors are not handle by this function.
* @see @cv{RetrieveImage}, RetrieveFrame(), GrabFrame().
*/
IplImage* X7sFrameReader::QueryFrame() {
	if(this->GrabFrame()) {
		return this->RetrieveFrame();
	}
	return NULL;
}

/**
* @brief Grabs and returns a frame in the user defined size.
* The returned image should not be deleted, nor released.
* @return a pointer on a IplImage if the frame can be retrieve, otherwise it return NULL.
* @warning Errors are not handle by this function.
* @see @cv{RetrieveImage}, RetrieveFrame(), GrabFrame().
*/
IplImage* X7sFrameReader::QueryRFrame() {
	if(this->GrabFrame()) {
		return this->RetrieveRFrame();
	}
	return NULL;
}

/**
* @brief Grab a frame handling error at loading frame.
*/
bool X7sFrameReader::GrabFrameError() {

	if(is_exit) return false;

	if(this->GrabFrame()) {	//Grab the frame
		im=this->RetrieveFrame();	//Then internally set it in im pointer.
	}
	else {
		im=NULL;
	}

	//Wait at least two milliseconds.
	if(m_wait<=1) cvWaitKey(2);

	//Check the input frames.
	return CheckInputFrame();
}

/**
* @brief Grab the first frame (Handling error).
* @see GrabFrame().
* @return true when if a frame could be grabbed.
*/
bool X7sFrameReader::GrabFirstFrame() {

	//Loop until the image is not NULL.
	while(nErrors <= nMaxErrors) {

		//Try to grab the first frame.
		if(GrabFrame()) {
			return true;
		}
		nErrors++;
	}
	cerr << "GrabFirstFrame(): nof errors is " << nErrors << " > " << nMaxErrors << endl;
	return false;
}

/**
* @brief this function must be done after the setup.
*/
const CvSize& X7sFrameReader::GetFrameSize() {
	if(resize.height != -1 || resize.width != -1) {
		return resize;
	}
	else {
		//Find the first frame available.
		while(GrabFrameError()) {
			if(im) {
				resize = cvGetSize(im);
				return resize;
			}
		}
	}
	return resize;
}

/**
* @brief Return the FPS of the X7sFrameReader
*/
float X7sFrameReader::GetFrameRate()
{
	clock_t elap_time = clock() - m_start_ts;
	if(elap_time >= CLOCKS_PER_SEC)
	{
		m_fps=((float)(GetFrameId()-m_start_fid))*((float)CLOCKS_PER_SEC/(float)elap_time);

		//And finally reset the time.
		m_start_fid=GetFrameId();
		m_start_ts=clock();
	}
	return m_fps;
}


/**
* @brief Wait X millisec and seek in the video (next/prev/pause/quit)
* according to the key pressed.
*/
int X7sFrameReader::KeyListener(int millisec) {
	char key, keyLow;
	if(m_wait>0)
	{
		if(m_wait==10) millisec=0;
		else millisec = millisec+(int)pow(2.0,(m_wait+2));
	}
	key= cvWaitKey(millisec);
	if(key==-1) return X7S_IO_RET_OK;
	else {
		if(ptFunc!=NULL && charFunc==key)
		{
			return ptFunc(key);
		}

		MapParam *mapFunc = (MapParam*)m_mapParam;
		keyLow=tolower(key);
		if(mapFunc->find(keyLow)==mapFunc->end())
		{
			return KeyEvent(key);
		}
		else
		{
		X7sParam *pParam = (*mapFunc)[keyLow];
		bool ret= pParam->Increment((isupper(key)!=0));
		if(ret) return X7S_IO_RET_OK;
		else return X7S_IO_RET_ERR;
		}
	}
	return X7S_IO_RET_ERR;
}

/**
* @brief Obtain a mask image from the sequence.
*
* The path for the NoInterest Image is given by the X7sParam @tt{mask_path},
* the given image must be >0 on the Region Of No Interest. (RONI).
* A binary threshold can be used to improve the separation, therefore a pixel is considered
* as not interesting if pixel>@tt{mask_thrs}.
* If the @tt{mask_thrs} is negative, the loaded image is inverted and then we apply the threshold
* with its absolute value.
*
* @note This function may be used afters GrabFirstFrame().
* @return An image where the pixel of no interest have the value one.
*/
IplImage* X7sFrameReader::GetNoInterest() {
	if(mask==NULL) {
		if(!m_params["mask_path"]->toString().empty()) {
			mask = cvLoadImage(m_params["mask_path"]->toString().c_str(),CV_LOAD_IMAGE_GRAYSCALE);
			if(mask==NULL) {
				X7S_PRINT_WARN("X7sFrameReader::GetMask()",
						"Mask filename: %s could not be loaded",
						m_params["mask_path"]->toString().c_str());
			}
			std::cout << "cout "<<  m_params["mask_path"] << std::endl;
		}
		if(resize.width != -1 && resize.height != -1) {
			maskr = cvCreateImage(resize,IPL_DEPTH_8U,1);
		}
		if(mask==NULL) {
			if(im == NULL) 	return NULL;
			else {
				//Send an error message.

				//If mask could not be loaded
				mask = cvCreateImage(cvGetSize(im),IPL_DEPTH_8U,1);
				//Set Mask with all the pixel as white.
				cvSet(mask,cvScalarAll(0),NULL);
			}
		}
		else {
			X7S_PRINT_WARN("X7sFrameReader::GetMask()",
					"Threshold value is = %s",
					m_params["mask_thrs"]->toString().c_str());

			if(m_params["mask_thrs"]->toRealValue()<0) {
				cvNot(mask,mask);
			}

			//!Threshold the image.
			cvThreshold(mask,mask,abs(m_params["mask_thrs"]->toRealValue()),255,CV_THRESH_BINARY);
		}
		//Finally resize mask.
		if(maskr) cvResize(mask,maskr,CV_INTER_LINEAR);
	}
	return mask;
}

/**
* @brief Return the Resized mask.
* (1 channels, 8 bits depths).
*/
IplImage* X7sFrameReader::GetRNoInterest() {
	if(maskr==NULL) this->GetNoInterest();
	return maskr;
}

/**
* @brief Obtain a foreground mask for the image sequence.
* @note This function may be used afters GrabFirstFrame().
*/
IplImage* X7sFrameReader::GetFGMask() {
	if(fgmask!=NULL) {
		if(resize.width != -1 && resize.height != -1) {
			fgmaskr = cvCreateImage(resize,IPL_DEPTH_8U,1);
		}
	}
	return fgmask;
}


/**
* @brief Return the resize Foreground mask.
* (1 channels, 8 bits depths).
*/
IplImage* X7sFrameReader::GetRFGMask() {
	if(fgmaskr==NULL) return this->GetFGMask();
	else {
		if(fgmask==NULL) return NULL;
		cvResize(fgmask,fgmaskr,CV_INTER_LINEAR);
		return fgmaskr;
	}
}

/**
* @brief Assign a key to a parameter that we want to modify.
* @param key An alpha key (uppercase <=> increment, lowercase <=> decrement)
* @param pParam The parameter (The increment step need to be set otherwise it will be one).
*/
void X7sFrameReader::AddKeyParam(char key,X7sParam * pParam)
{
	if(pParam==NULL) return;
	MapParam* pMap=(MapParam*)m_mapParam;

	if(isalpha(key)) {
		(*pMap)[tolower(key)]=pParam; //Save the param with the lower key if we give P (we save with p)
	}
	else
		X7S_PRINT_WARN("X7sFrameReader::AddKeyParam()","key=%c (%02X) is not alpha",key,key);
}

/**
* @brief Assign a function to a key value
*
* The function must be in the following form:
* @code
* int funcName(char key) {
* 	...
* 	if(ok) return X7S_RET_OK;
* 	else return X7S_RET_ERR;
* }
* @endcode
*
* @param key
* @param pFunction
*/
void X7sFrameReader::AddKeyFunction(char key,int (*pFunction)(char))
{
	ptFunc=pFunction;
	charFunc=key;
}


//------ Protected & Private Methods


/**
* @brief Init all the default parameters for the X7sFrameReader.
*/
void X7sFrameReader::Init() {

	//Variable that can be set by XML.
	resize.height=-1;
	resize.width=-1;
	m_wait=0;
	m_isLooping=false;

	//Fill the parameters list with default value and range.
	m_params.Insert(X7sParam("im_path",X7S_XML_ANYID,""));
	m_params.Insert(X7sParam("mask_path",X7S_XML_ANYID,""));
	m_params.Insert(X7sParam("fg_path",X7S_XML_ANYID,""));
	m_params.Insert(X7sParam("fpath_prefix",X7S_XML_ANYID,""));
	m_params.Insert(X7sParam("nMaxErrors",X7S_XML_ANYID,3,X7STYPETAG_INT,0,INT_MAX));
	m_params.Insert(X7sParam("f_start",X7S_XML_ANYID,0	,X7STYPETAG_INT,0,INT_MAX));
	m_params.Insert(X7sParam("f_end",X7S_XML_ANYID,INT_MAX,X7STYPETAG_INT,1,INT_MAX));
	m_params.Insert(X7sParam("width",X7S_XML_ANYID,-1	,X7STYPETAG_INT,-1,2048));
	m_params.Insert(X7sParam("height",X7S_XML_ANYID,-1	,X7STYPETAG_INT,-1,2048));
	m_params.Insert(X7sParam("mask_thrs",X7S_XML_ANYID,1	,X7STYPETAG_INT,-255,255));
	m_params.Insert(X7sParam("waitLevel",X7S_XML_ANYID,m_wait,X7STYPETAG_INT,0,10));
	m_params.Insert(X7sParam("isLooping",X7S_XML_ANYID,false,X7STYPETAG_BOOL));

	//Link the parameters to the some value that are often used.
	m_params["width"]->LinkParamValue(&(resize.width));
	m_params["height"]->LinkParamValue(&(resize.height));
	m_params["nMaxErrors"]->LinkParamValue(&nMaxErrors);
	m_params["f_end"]->LinkParamValue(&f_end);
	m_params["waitLevel"]->LinkParamValue(&m_wait);
	m_params["isLooping"]->LinkParamValue(&m_isLooping);


	//Variable internal to the class.
	frame_id=0;
	nErrors=0;
	is_exit=false;
	is_setup=false;
	is_flipped=false;
	is_1stframe=true;
	im=NULL;
	imr=NULL;
	mask=NULL;
	maskr=NULL;
	fgmask=NULL;
	fgmaskr=NULL;
	m_fps=0;
	m_start_ts=clock();
	m_start_fid=0;

	m_mapParam=	(void*)new MapParam();
	ptFunc=NULL;
	charFunc=0x0;
}

/**
* @brief Check if the input frame exist and if the resize frame is necessary.
* @return @false when the number of errors at reading the input frame
*/
bool X7sFrameReader::CheckInputFrame() {

	//Check if a frame has been loaded.
	if(im == NULL) {
		nErrors++;
		if(nErrors > nMaxErrors) {
			cerr << "nof errors is " << nErrors << " > " << nMaxErrors << endl;
			frame_id=f_end;
		}
	}

	//Check if the frame_id is not the last one.
	if(frame_id>=f_end) {
		cerr << "frame_id > f_end (" << f_end << ")" << endl;
		return false;
	}


	//Check if our resize frame has not been created.
	if(imr == NULL) {
		if(resize.width <=0) resize.width=im->width;
		if(resize.height <=0) resize.height=im->height;
		imr = cvCreateImage(resize,IPL_DEPTH_8U,3);
		frame_id=m_params["f_start"]->toIntValue();
	}
	return true;
}


/**
* @brief Perform an action when a key is pressed.
*/
int X7sFrameReader::KeyEvent(char key) {
	int tmp;
	int ret=X7S_IO_RET_OK;
	switch(key) {
	case X7S_IO_KEY_ESC:	//Quit
		cout << "Escape key" << endl;
		is_exit=true;
		break;
	case X7S_IO_KEY_SPACE:	//Pause
		cout << "Pause, press any key to continue..." << endl;
		cvWaitKey(0);
		break;
	case X7S_IO_KEY_LARR:	//Go back 10 frames
		cout << "Not implemented" << endl;
		break;
	case X7S_IO_KEY_RARR:	//Go to the next 48 frames (2sec).
		tmp=frame_id+48;
		cout << "Jump from " << frame_id << " to " << tmp << endl;
		for(;frame_id<tmp;frame_id++) this->GrabFrameError();
		break;
	case X7S_IO_KEY_UARR:
		if(m_wait<10) m_wait++;
		cout << "Increase wait:" << m_wait << endl;
		break;
	case X7S_IO_KEY_DARR:
		if(m_wait>0) m_wait--;
		cout << "Decrease wait:" << m_wait << endl;
		break;
	case 'h':
	case 'H':
		this->PrintHelp();
		break;
	case X7S_IO_KEY_LSHIFT:
	case X7S_IO_KEY_RSHIFT:
		break;
	default:
		printf("Unknown key '%c' with code: %d\n",key,key);
		ret=X7S_IO_RET_ERR;
	}
	fflush(stdout);
	return ret;
}


void X7sFrameReader::PrintHelp()
{
	const char *sep="\t- ";
	cout << "Key Help" << endl;
	cout << sep << "h/H 	> Show This Help" << endl;
	cout << sep << "esc 	> Quit the software" << endl;
	cout << sep << "space	> Pause" << endl;
	cout << sep << "left	> Go to prev frames (-2 secs)" << endl;
	cout << sep << "right	> Go to next frames (+2 secs)" << endl;
	cout << sep << "up   	> Increase wait time between frame" << endl;
	cout << sep << "down	> Decrease wait time between frame" << endl;
}

//------ Friend Methods


std::ostream &operator<<(std::ostream& out, X7sFrameReader &freader) {
	out << "Frame Reader:" << endl;
	out << "\ttype:" << freader.GetType() << endl;
	out << (freader.m_params).toString() << endl;
	return out;
}

//----- Outside Methods.

/**
* @brief Read the @tt{\<io_reader>\</io_reader>} tag in the XML and return a frame Reader correctly setup.
*
* This function also setup the media.
*
* The way to load the parameters is the following for the frame reader is the following:
*
*		- general_conf.xml:
@code
<?xml version="1.0"?>
<root>
<io_reader>
<fpath_prefix>D:\DATABASE\</fpath_prefix>
</io_reader>
...
</root>
@endcode
*
*		- media_conf.xml:
@code
<?xml version="1.0"?>
<root>
<io_reader type="1"> <!-- SEQ=1,FILE=2,VIDEO=3,CAMERA=4 -->
<im_path>"PETS2001\DS01_Te_cam1.avi"</im_path>
<f_start>20</f_start>
<f_end>-1</f_end>
<width>360</width>
<height>288</height>
<nMaxErrors>10</nMaxErrors>
</io_reader>
...
</root>
@endcode
*
* @warning This function can also create a FrameReader with network communication,
* however this frame reader will only work using one camera due to fact that
* the X7sNetSession is created internally.
*
*  @note If you don't want to use relative path in media_conf.xml you can
*  or override the @tt{fpath_prefix} with nothing. Otherwise, you can load only
*  media_conf.xml.
*
* @param root The parent node (by default it is NULL)
* @param root_gen The parent node for general configuration.
*
*/
X7sFrameReader* X7sCreateFrameReader(const TiXmlNode* root,const TiXmlNode* root_gen) {

	int type=0;
	X7sFrameReader *freader;
	const TiXmlElement *cnode=NULL,*cnode_gen=NULL;	//child node (<io_reader></io_reader>).
	const char* tagname="io_reader";

	if(root==NULL) {
		X7S_PRINT_ERROR("X7sCreateFrameReader()","<root></root> node not found");
		return NULL;
	}


	//Go inside the filenode <input></input>
	cnode = root->FirstChild(tagname)->ToElement();
	if(cnode==NULL) {
		X7S_PRINT_ERROR("X7sCreateFrameReader()","<%s></%s> tag not found",tagname,tagname);
		return NULL;
	}

	//Get the type parameters
	cnode->Attribute("type",&type);

	//Create the child object.
	switch(type) {
	case X7S_IO_TYPE_SEQ:
		freader = new X7sSeqReader();
		break;
	case X7S_IO_TYPE_VID:
		freader = new X7sVidReader();
		break;
	case X7S_IO_TYPE_BIN:
		freader = new X7sBinReader();
		break;
	case X7S_IO_TYPE_NET:
		freader = new X7sNetReader(cnode);
		break;
	default:
		X7S_PRINT_ERROR("X7sCreateFrameReader()","Can't create FrameReader for type=%d",type);
		return NULL;
	}

	//If main xml config is setup try to load it.
	if(root_gen) {
		cnode_gen = root_gen->FirstChild(tagname)->ToElement();
		if(cnode_gen) {
			freader->ReadFromXML(cnode_gen);
		}
	}

	bool ret=true;
	//Then load and "overwrite" parameters with freader.
	ret=freader->ReadFromXML(cnode);
	if(ret) 
	{
		cout << "\n" << (*freader) << endl;
		ret=freader->Setup();
	}

	if(ret==false)
	{
		X7S_DELETE_PTR(freader);
	}
	return freader;
}
