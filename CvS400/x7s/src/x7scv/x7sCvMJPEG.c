#include "x7scv.h"
/**
* @file
*
*   The format of MJPEG frame is the following:

@code
*    |---------------------------|
*    |   MJPEG header (0x2D2D)   | (56B)	//--myboundary\r\nContent-Type: image/jpeg\r\nContent-Length: 
*    |   MJPEG Footer (0x0D0A)   | (4B)
*    |---------------------------|
*    |   JPEG Header1 (0xFFD8)   |
*    |---------------------------|
*    |   MDATA Data1 (0xFFFE)    | 
*    |   MDATA Data2 (0xFFFE)    |
*    |---------------------------|
*    |   JPEG Header2 (0xFFC0)   |
*    |---------------------------|
*    |   JPEG Data    (0xFFDA)   |
*    |---------------------------|
*    |   JPEG Footer  (0xFFD9)   |
*    |---------------------------|
*    |   MJPEG Footer (0x0D0A)   | (2B)
*    |---------------------------|
@endcode


*/



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Defines/Enums/Includes
//----------------------------------------------------------------------------------------

#include "x7sdef.h"
#include "x7sCvLog.h"
#include <time.h>

typedef struct tm tm_t;	//!< A simple typedef to the timeinfo structure.

static const uint8_t mjpeg_FILE_hdr[12] = {
		0x18, 0x02, 0x00, 0x00, 0x4D, 0x4A, 0x50, 0x47,
		0x03, 0x00, 0x00,
};

#define IX7S_SIZEIB_MJPEG_HDR 	65	//!< Size of MJPEG header (since --myboundary to 0x0D0A0D0A included)
#define IX7S_SIZEIB_JPEG_HDR0	158	//!< Size of JPEG header (since 0xFFD8FFE0 to 0xFFFE excluded)
#define IX7S_SIZEIB_JPEG_MIN	760	//!< Minimum size of a JPEG frame (with all headers)
#define IX7S_SIZEIB_STRTIME 	20	//!< Size in byte of the time.
#define IX7S_STRTIME_FORMAT 	"%02d/%02d/%04d %02d:%02d:%02d"		//!< Print/Read format of a time string

/**
* @brief Header (begin) to seperate two JPEG in a MJPEG file.
* @note The length of the frame is not put in this header.
* @see IX7S_SIZEIB_MJPEG_HDR
*/
static const uint8_t mjpeg_header[56] = {
		0x2D, 0x2D, 0x6D, 0x79, 0x62, 0x6F, 0x75, 0x6E,
		0x64, 0x61, 0x72, 0x79, 0x0D, 0x0A, 0x43, 0x6F,
		0x6E, 0x74, 0x65, 0x6E, 0x74, 0x2D, 0x54, 0x79,
		0x70, 0x65, 0x3A, 0x20, 0x69, 0x6D, 0x61, 0x67,
		0x65, 0x2F, 0x6A, 0x70, 0x65, 0x67, 0x0D, 0x0A,
		0x43, 0x6F, 0x6E, 0x74, 0x65, 0x6E, 0x74, 0x2D,
		0x4C, 0x65, 0x6E, 0x67, 0x74, 0x68, 0x3A, 0x20
};

/**
* @brief Header (end) to seperate two JPEG in a MJPEG file.
*/
static const uint8_t mjpeg_end_header[4] = {
		0x0D, 0x0A, 0x0D, 0x0A
};


typedef struct
{
	uint32_t id;
	uint32_t tag;
} iX7sMJPEGIdTag;


typedef struct
{
	uint16_t f1_cmt;
	uint16_t f1_length;
	uint32_t id;
	uint32_t tag;
	uint16_t f2_cmt;
	uint16_t f2_length;
	const char strtime[IX7S_SIZEIB_STRTIME];

} iX7sMJPEGFrameHdr;


static char g_strtime[IX7S_SIZEIB_STRTIME];	//!< Memory reserved to display the string.

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Private Prototypes
//----------------------------------------------------------------------------------------



/**
* @brief Swap the endianness of a 16 bytes value (big endian <-> small endian).
*/
uint16_t swapEndian16(uint16_t val) {
	uint16_t ret=0;
	uint8_t* p = (uint8_t*)& val;
	ret+= (p[0] << 8);
	ret+= p[1];
	return ret;
}

/**
* @brief Swap the endianness of a 32 bytes value (big endian <-> small endian).
*/
uint32_t swapEndian32(uint32_t val) {
	uint32_t ret=0;
	uint8_t* p = (uint8_t*)& val;
	ret+= (p[0] << 24);
	ret+= (p[1] << 16);
	ret+= (p[2] << 8);
	ret+= p[3];
	return ret;
}

/**
* @brief Basic function which counts the number of byte,which value is 0xFF, we have
* in an array of bytes.
* @param raw_data Input array of bytes which contains the information we have to check.
* @return Integer which contains the number of 0xFF we have.
*/
int num_FF(X7sByteArray *raw_data){
	int n = 0, i = 0;
	if(raw_data && raw_data->data)
	{
		for(i=0;i< raw_data->length;i++)
			if (raw_data->data[i] == 0xFF) n++;
	} else { X7S_PRINT_ERROR("num_FF()", "Error opening raw_data"); }
	return n;
}

/**
* @brief this function is created to include a byte of 0x00 every time that we have a 0xFF in our data
* in order to adapt a array of data to a JPEG code. JPEG code's bytes, which value is 0xFF, are JPEG header code
*  so we have to include 0x00 in other to indicate that these bytes are 0xFF data and not header.
* @param raw_data An input array of bytes which contains the data we want to convert
* @param new_data An output array of bytes which contains the data converted to JPEG code
* @return integer which indicates if the function ends correctly: 0 correct; -1 incorrect.
*/
int codeFFdata2JPEG(X7sByteArray *raw_data, X7sByteArray *new_data){

	int n = 0,j= 0,i=0;
	if (raw_data->data==NULL){ X7S_PRINT_ERROR("codeFFdata2JPEG", "Error opening raw_data");
	}else{
		n = num_FF(raw_data);

		if (new_data->capacity < (raw_data->length + n)){X7S_PRINT_ERROR("codeFFdata2JPEG", "Error length"); return X7S_RET_ERR;}
		new_data->length = raw_data->length + n;
		for(i=0;i<raw_data->length;i++)
		{
			if (raw_data->data[i] == 0xFF)
			{
				new_data->data[i+j] = 0xFF;
				j++;
				new_data->data[i+j] = 0x00;
			}else new_data->data[i+j] = raw_data->data[i];
		}
	}
	return X7S_RET_OK;
}

/**
* @brief this function is created to remove a byte of 0x00 every time the previous byte is 0xFF. This means that
* we are converting a JPEG data code of an array of data with only true data. JPEG code bytes, which value is
* 0xFF, are JPEG header code so we have to remove 0x00 in other to indicate that these were just 0xFF data.
* @param raw_data An input array of byte which contains the data we want to convert(JPEG data code).
* @param new_data An output array of byte which contains the data with just the true data.
* @return integer which indicates if the function ends correctly: 0 correct; -1 incorrect.
*/
int codeFFJPEG2data(X7sByteArray *raw_data, X7sByteArray *new_data){

	int n = 0,j= 0, i = 0;
	if (raw_data->data==NULL){ X7S_PRINT_ERROR("codeFFJPEG2data", "Error opening raw_data");
	}else{
		n = num_FF(raw_data);

		if (new_data->capacity < raw_data->length - n)
		{
			X7S_PRINT_ERROR("codeFFJPEG2data()", "Error length new_data->capacity=%d, raw_data->length=%d",
					new_data->capacity,raw_data->length - n);
			return X7S_RET_ERR;
		}
		for(i=0;i<raw_data->length;i++)
		{
			if (raw_data->data[i] == 0xFF)
			{
				new_data->data[i-j] = 0xFF;
				i++;
				j++;
				if (raw_data->data[i] != 0x00){ X7S_PRINT_ERROR("codeFFJPEG2data", "0xFF found is not data but code."); return X7S_RET_ERR;}
			}else new_data->data[i-j] = raw_data->data[i];
		}
		new_data->length = raw_data->length - n;
	}
	return X7S_RET_OK;


}

/**
* @brief Fing the previous JPEG frame and place us using the pFile.
*
* @note In order to find the previous JPEG we look every previous byte until find the JPEG header code which starts
*  a frame: 0xFFD8
* @param pFile A pointer on the File which contains several numbers of JPEG frame (up to 1Gb or 1 hour of information)
* @return integer which indicates if the function ends correctly: 0 correct; -1 incorrect(the beginning
*  of the File comes before find a previous frame). The return of this function is a pointer on the File where
*  it's the frame wanted
*/
int findPrevJpegInit(FILE *pFile)
{
	int c1= 0, c2= 0, n= 0;
	long size;
	size= ftell(pFile);
	while (size > 60){
		c1 = fgetc (pFile);
		if (c1 == 0xFF){
			c2 = fgetc (pFile);
			//printf ("second: %d. %x%x \n", n, c1, c2);
			if (c2 == 0xD8){//printf ("jumps: %d. \n", n);
				return X7S_RET_OK;}
			else  fseek( pFile , -2 , SEEK_CUR);
		}
		fseek( pFile , -2 , SEEK_CUR);
		n++;
		size= ftell(pFile);
	}

	return X7S_RET_ERR;
}

/**
* @brief Fing the next JPEG frame and place us using the pFile.
* @note In order to find the next JPEG we look every next byte until find the JPEG header code which starts
*  a frame: 0xFFD8. If the number of jumps is up to 80 the function creates a warning but it's still looking
*  for the next frame.
* @param pFile A pointer on the File which contains several numbers of JPEG frame (up to 1Gb or 1 hour of information)
* @return integer which indicates if the function finishes correctly: 0 correct; -1 incorrect(the File ends
*  before find a next frame). The return of this function is a pointer on the File where it's the frame wanted.
*/
int findNextJpegInit(FILE *pFile)
{
	int c1 = 0, c2 = 0, n = 0;
	do
	{
		c1 = fgetc(pFile);
		if (c1 == 0xFF){
			c2 = fgetc(pFile);
			//printf ("second: %d. %x%x \n", n, c1, c2);
			if (c2 == 0xD8) {//printf ("jumps: %d. \n", n);
				return X7S_RET_OK;}
		}
		n++;
		if (n == 80)  X7S_PRINT_DEBUG("findNextJpegInit()","The file was pointing to a wrong position...Looking for the next frame.");
	} while (!feof(pFile));//} while (c1 != EOF && c2 != EOF);
	return X7S_RET_ERR;
}

/**
* @brief Find next JPEG comment inside a whole JPEG frame.
*
* @note In order to find the next JPEG comment we look every next byte until
* find the JPEG header code which indicated it: 0xFFFE.
* @param mjpegFrameData Input array of bytes which contains the whole JPEG frame: with header, comments and data.
* @param act_pos Integer which indicates the position where we have to start looking and, at the end, it points to the next
* JPEG comment is. This integer points just after the code which indicated the JPEG comment(0xFFFE)
* @return integer which indicates if the function finishes correctly: 0 correct; -1 incorrect(the array of byte ends
*  before find a next JPEG comment).
*/
int findNextJpegComment(const X7sByteArray *mjpegFrameData,int *act_pos)
{
	int n = *act_pos;

	//printf ("after jump: %d. \n", n);
	do {
		if (mjpegFrameData->data[n] == 0xFF){
			if (mjpegFrameData->data[n+1] == 0xFE) {
				//printf ("jumps before comment: %d. \n", (n - *act_pos));
				*act_pos = n+2;
				return X7S_RET_OK;
			}
		}
		n++;
	} while (n != mjpegFrameData->length);
	return X7S_RET_ERR;
}

/**
* @brief Read time into a time structure and return the time in seconds since unix epoch.
* @param strtime A buffer with the time in string format (See @ref IX7S_STRTIME_FORMAT)
* @param timeinfo a structure with the timeinfo (If it is NULL the structure is created internally).
* @return X7S_RET_ERR if an error occurs, otherwise it return the time in seconds since linux epoch.
*/
time_t ix7sReadTime(const char *strtime,tm_t *timeinfo)
{
	time_t time=X7S_RET_ERR;
	sscanf(strtime,
			IX7S_STRTIME_FORMAT,
			&(timeinfo->tm_mday),
			&(timeinfo->tm_mon),
			&(timeinfo->tm_year),
			&(timeinfo->tm_hour),
			&(timeinfo->tm_min),
			&(timeinfo->tm_sec)
	);

	timeinfo->tm_isdst=-1;		//Take the default value for Daylight Saving Time (DST).
	timeinfo->tm_mon--;			//Months are in the range [0-11]
	timeinfo->tm_year-=1900;	//Year since 1900

	time = mktime(timeinfo);
	return time;
}

/**
* @brief Format in a string the time.
* @param strtime	buffer of at least @ref IX7S_SIZEIB_STRTIME.
* @param time	Time in seconds since linux epoch.
* @return The @ref X7S_RET_xxx code.
* @see String is written with the following format @ref IX7S_STRTIME_FORMAT
*  */
int ix7sStrfTime(const char *strtime, time_t time)
{
	tm_t *timeinfo;
	timeinfo = localtime(&time);
	X7S_CHECK_WARN("ix7sStrfTime()",timeinfo,X7S_RET_ERR,"timeinfo=NULL (time=%d)",time);
	X7S_CHECK_WARN("ix7sStrfTime()",strtime,X7S_RET_ERR,"strtime=NULL");
	snprintf((char*)strtime,IX7S_SIZEIB_STRTIME,IX7S_STRTIME_FORMAT,
			(timeinfo->tm_mday),
			(timeinfo->tm_mon)+1,
			(timeinfo->tm_year)+1900,
			(timeinfo->tm_hour),
			(timeinfo->tm_min),
			(timeinfo->tm_sec));
	return X7S_RET_OK;
}

/**
* @brief Convert the time value in seconds since unix epoch to a human-readable version.
*
* The returned string has the following format dd/mm/yyyy hh:mm:ss
*
* @param time The time value in seconds since unix epoch.
* @return A C string containing the date and time information. This string is statically allocated, therefore
* each time this funcion is called the content of this array is overwritten.
*/
const char *x7sCTime(const time_t* time)
{
	int ret = ix7sStrfTime(g_strtime,*time);
	if(ret==X7S_RET_OK) return g_strtime;
	else return "NULL";
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public functions
//----------------------------------------------------------------------------------------



/**
* @brief Open a file in write/read mode at the given filepath.
*
* This function has two mode of use according to the value of read.
* 	-	In read mode, it an MJPEG header is found we fill mjpegHdrData (memory must be reserved)
*  	-	In write mode, we can write the mjpegHdrData or not.
*
* This function @b must be followed by the x7sReleaseMJPEG() function.
*
* @param filepath A pointer on the File we want to read or write.
* @param mjpegHdrData Array of bytes that correspond to the header of the MJPEG File header.
* @param read boolean which indicates if the function is going to read or write: 1 read; 0 write.
* @return A pointer on a FILE used to write inside each JPEG+Mdata frame or read each JPEG+Mdata frame.
* @see x7sReleaseMJPEG()
*/
FILE *x7sOpenMJPEG(const char *filepath, X7sByteArray *mjpegHdrData , bool read)
{
	CV_FUNCNAME("x7sOpenMJPEG()");

	FILE *pFile;
	uint32_t length_temp;
	int size1 =0, size2 =0, size_t_hdr =0;
	int ret_code;
	X7sByteArray *temp_X7Mjpeg;
	uint8_t tmp[sizeof(mjpeg_FILE_hdr)+1];

	//Open file at the given filepath
	if(read) pFile = fopen (filepath , "rb");
	else		pFile = fopen (filepath , "wb");

	X7S_CHECK_ERROR(cvFuncName,pFile,NULL,"Error opening file %s",filepath);

	//Read the MJPEG File header
	if(read)
	{
		//check that we have the correct a X7S MJPEG file
		fread(tmp,sizeof(mjpeg_FILE_hdr)+1,1,pFile);
		tmp[sizeof(mjpeg_FILE_hdr)]=0;
		ret_code = memcmp(tmp,mjpeg_FILE_hdr,sizeof(mjpeg_FILE_hdr));
		X7S_CHECK_ERROR(cvFuncName,(ret_code == 0),NULL,"File %s doesn't correspond to MJPEG file: %s",filepath,tmp);


		// extract type of alarm from the file
		fseek(pFile, 14, SEEK_SET);
		size1 = getc(pFile);
		size2 = getc(pFile);
		size_t_hdr = size1*256 + size2 - 2;
		X7S_PRINT_DEBUG("x7sOpenMJPEG","Whole data of an MJPEG header.Size of: %d. It contains the alarm type active in this frames.", size_t_hdr);
		if (size_t_hdr == 0)
		{
			mjpegHdrData = NULL;
		}
		else
		{
			mjpegHdrData->length = size_t_hdr;
			fread(mjpegHdrData->data,1,size_t_hdr,pFile);
			ret_code = codeFFJPEG2data(mjpegHdrData, mjpegHdrData);
			X7S_CHECK_ERROR(cvFuncName,ret_code == X7S_CV_RET_OK,NULL,"Failed to read file header (alarm definition)");

		}
	}
	//Write the MJPEG File header.
	else
	{
		//Write MJPEG header, each FILE
		fwrite(mjpeg_FILE_hdr, sizeof(uint8_t),sizeof(mjpeg_FILE_hdr),pFile);
		if(mjpegHdrData)
		{
			temp_X7Mjpeg = x7sCreateByteArray(2*mjpegHdrData->length);
			ret_code = codeFFdata2JPEG(mjpegHdrData,temp_X7Mjpeg);
			X7S_CHECK_WARN(cvFuncName,ret_code == X7S_CV_RET_OK,NULL,"Failed to write file header (alarm definition)");
			length_temp =  ((swapEndian16(temp_X7Mjpeg ->length +2) << 16) & 0xFFFF0000) + 0x80FF;
			fwrite(&length_temp, sizeof(uint32_t),1,pFile);
			fwrite(temp_X7Mjpeg->data,sizeof(uint8_t),temp_X7Mjpeg->length,pFile);
			x7sReleaseByteArray(&temp_X7Mjpeg);

		}
		else
		{
			length_temp =  ((0x2 << 16) & 0xFFFF0000) + 0x80FF;
			fwrite(&length_temp, sizeof(uint32_t),1,pFile);
		}

		//Flush the data written into the file.
		fflush(pFile);
	}
	return pFile;
}



/**
* @brief Write a mjepgFrameData in a file.
*
* This function also check that the size of the MJPEG file does not exceed @ref X7S_CV_MJPEG_MAXFILESIZE bytes
*
* @param mjpegFrameData Input array of bytes which we are going to insert in the file. This array contains header,
*  comments and JPEG data.
* @param pFile A pointer on the File which contains JPEG frames (up to 1Gb or 1 hour of information)
* @return @ref X7S_RET_ERR is returned when the file is goind to exceed @ref X7S_CV_MJPEG_MAXFILESIZE,
* otherwise it return @ref X7S_RET_OK.
*/
int x7sWriteMJPEG(const X7sByteArray *mjpegFrameData, FILE *pFile)
{
	long endPos=0;
	int full_size;

	CV_FUNCNAME("x7sWriteMJPEG");
	//Check arguments.
	X7S_CHECK_WARN(cvFuncName,mjpegFrameData,X7S_RET_ERR,"x7sFillMJPEG(): Mjpeg_frame is NULL");
	X7S_CHECK_WARN(cvFuncName,pFile,X7S_RET_ERR,"x7sFillMJPEG(): pFile is NULL");

	//Get full size
	full_size = mjpegFrameData->length;

	//Check if the file will not be bigger than 1Gb
	//Obtain the size of the FILE
	endPos = ftell(pFile);
	if(endPos+full_size>X7S_CV_MJPEG_MAXFILESIZE)
	{
		X7S_PRINT_INFO(cvFuncName, "x7sFillMJPEG(): File was close because (%d+%d) bytes will exceed maximum size %d",
				full_size,endPos,X7S_CV_MJPEG_MAXFILESIZE);
		x7sReleaseMJPEG(&pFile);
		return X7S_RET_ERR;
	}

	fwrite(mjpegFrameData->data,sizeof(uint8_t),mjpegFrameData->length,pFile);

	//Flush the data written into the file.
	fflush(pFile);

	return X7S_RET_OK;
}

/**
* @brief Mix metadata and JPEG into a special JPEG data buffer.
*
* 	Inside each frame of JPEG data we hide the whole  X7sMJPEGFrame structure:
* 	(JPEG, ID, Tag, Time, Blobs, Alarm).
*
* @param frame Input construction which contains the Info we have to add on the mjpegFrameData. The fields of this
*  construction are JPEG data, ID, Tag, Time, MData and Alarm.
* @param mjpegFrameData Output array of bytes which contains the filled frame.Without the first 65 bytes (type and
* length of the data) is a normal JPEG frame with some comments that we don't show.
* @return integer that contains 0 when the function ends successfully.
*/
int x7sFillMJPEG(const X7sMJPEGFrame *frame, X7sByteArray *mjpegFrameData)
{
	X7S_FUNCNAME("x7sFillMJPEG");

	int jpeg_hdr_pos=0;
	uint32_t full_size = 0;
	uint32_t CommentJPEG_hdr, length_temp;
	uint16_t end_MJPEG;
	X7sByteArray *mdata, *alarm;
	char buf_length[10];
	char time_buf[20];
	iX7sMJPEGIdTag Tag_struct;

	CommentJPEG_hdr=0xFEFF;

	//Seach end where FFC0 start
	jpeg_hdr_pos=158;

	//---- Start array ------//
	X7S_CHECK_WARN(funcName,frame->pJpeg,X7S_RET_ERR,"pJpeg is NULL");
	X7S_CHECK_WARN(funcName,frame->pAlarm,X7S_RET_ERR,"pAlarm is NULL");
	X7S_CHECK_WARN(funcName,(frame->pJpeg->length+frame->pAlarm->length+frame->pBlobs->length) < mjpegFrameData->capacity,X7S_RET_ERR,
			"X7sMJPEGFrame size is too big: jpeg:%d + alarm:%d + mdata:%d = %d",frame->pJpeg->length,frame->pAlarm->length,frame->pBlobs->length,frame->pJpeg->length+frame->pAlarm->length+frame->pBlobs->length);



	//Write MJPEG header, each frame
	memcpy(mjpegFrameData->data,mjpeg_header,sizeof(mjpeg_header));
	full_size = sizeof(mjpeg_header) + 5; // MJPEG header "--myboundary Type..." and length data(We write it at the end on ascii character)
	memcpy((mjpegFrameData->data+full_size),mjpeg_end_header,sizeof(mjpeg_end_header));
	full_size += sizeof(mjpeg_end_header); //MJPEG end header


	//Write first part of JPEG
	memcpy((mjpegFrameData->data+full_size),frame->pJpeg->data,jpeg_hdr_pos);
	full_size += jpeg_hdr_pos; //ID data

	//Write MJPEG ID + Tag(identification of referent frame MJPEG + bits of fields)
	Tag_struct.id = frame->id;
	Tag_struct.tag = frame->tag;
	length_temp = 0x0A00FEFF;// it has the same size every frame
	memcpy((mjpegFrameData->data+full_size),&length_temp,sizeof(uint32_t));
	full_size += sizeof(uint32_t);
	memcpy((mjpegFrameData->data+full_size),&Tag_struct,sizeof(Tag_struct));
	full_size += sizeof(Tag_struct);


	//Write actual time
	length_temp = 0x1600FEFF;// it has the same size every frame
	memcpy((mjpegFrameData->data+full_size),&length_temp,sizeof(uint32_t));
	full_size += sizeof(uint32_t);
	ix7sStrfTime(time_buf,frame->time);
	memcpy((mjpegFrameData->data+full_size),time_buf,IX7S_SIZEIB_STRTIME);
	full_size += IX7S_SIZEIB_STRTIME; //number of characters of time data. 20 = x16


	//Write Mdata in JPEG
	if(frame->pBlobs)
	{
		mdata = x7sCreateByteArray(2*frame->pBlobs->length);
		codeFFdata2JPEG(frame->pBlobs, mdata);
		length_temp =  ((swapEndian16(mdata ->length +2) << 16) & 0xFFFF0000) + CommentJPEG_hdr;// The second part of the uint32_t is the mdata length
		memcpy((mjpegFrameData->data+full_size),&length_temp,sizeof(uint32_t));
		full_size+= sizeof(uint32_t);
		X7S_CHECK_WARN(funcName,(uint32_t)(mdata->length) < (mjpegFrameData->capacity-full_size),X7S_RET_ERR,
				"X7sMJPEGFrame.mdata size is too big: %d",mdata->length);
		memcpy((mjpegFrameData->data+full_size),mdata->data,mdata ->length);
		full_size+=mdata->length;
		x7sReleaseByteArray(&mdata);
	}

	//Write Alarm in JPEG
	if(frame->pAlarm)
	{
		alarm = x7sCreateByteArray(2*frame->pAlarm->length);
		codeFFdata2JPEG(frame->pAlarm, alarm);
		length_temp =  ((swapEndian16(alarm ->length +2) << 16) & 0xFFFF0000) + CommentJPEG_hdr; // The second part of the uint32_t is the alarm length
		memcpy((mjpegFrameData->data+full_size),&length_temp,sizeof(uint32_t));
		full_size += sizeof(uint32_t); //ID data
		X7S_CHECK_WARN(funcName,(uint32_t)(alarm->length) < (mjpegFrameData->capacity-full_size),X7S_RET_ERR,
					"X7sMJPEGFrame.alarm size is too big: %d",alarm->length);
		memcpy((mjpegFrameData->data+full_size),alarm->data,alarm->length);
		full_size+=alarm->length;
		x7sReleaseByteArray(&alarm);
	}

	//Write the other part of JPEG.
	X7S_CHECK_WARN(funcName,(uint32_t)(frame->pJpeg->length-jpeg_hdr_pos) < (mjpegFrameData->capacity-full_size),X7S_RET_ERR,
			"X7sMJPEGFrame.jpeg size is too big: %d",frame->pJpeg->length-jpeg_hdr_pos);
	memcpy((mjpegFrameData->data+full_size),(frame->pJpeg->data+jpeg_hdr_pos),(frame->pJpeg->length-jpeg_hdr_pos));
	full_size +=(frame->pJpeg->length-jpeg_hdr_pos);

	//End of MJPEG.
	end_MJPEG=0x0A0D;
	memcpy((mjpegFrameData->data+full_size),&end_MJPEG,sizeof(uint16_t));
	full_size += sizeof(uint16_t); //ID data


	//Get full size
	mjpegFrameData->length= full_size;

	if(full_size > 99999) {
		X7S_PRINT_ERROR("x7sFillMJPEG()","length=%d (jpeg=%d,blob=%d,alr=%d)",
				(int)full_size,frame->pJpeg->length,frame->pBlobs->length,frame->pAlarm->length);
	}

	snprintf(buf_length,sizeof(buf_length),"%05d", full_size);// write it on ascii character instead of hex
	//write_length = swapEndian32(full_size);
	memcpy((mjpegFrameData->data+sizeof(mjpeg_header)),buf_length,5);

	mjpegFrameData->type=X7S_CV_MJPEG_FRAMEDATA_TYPE;

	return X7S_RET_OK;
}

/**
* @brief Extract a frameData from the MJPEG file given the position in a file and the number of frame to jump.
*
* @warning If this function returns correctly, pFile is pointing on the next frame from the one we just read in mjpegFrameData.
* In other words, if in mjpegFrameData we have the data of frame #7, the pointer on pFile is pointing on the
* beginning of the next frame (the frame #8). Therefore, if we want to read consecutively frame by frame
* the value @tt{jump_frame=0} read pFile frame by frame without any "jump".
*
* @param mjpegFrameData Output array of bytes where we are going to insert the select frame. This array contains header,
*  comments and JPEG data.
* @param pFile A pointer of File where we starts looking for this specific frame.
* @param jump_frame The number of frames we want to jump in the MJPEG file. It can be negative/positive number
* if we respectively want previous/next frames.
* @return @ref X7S_RET_OK if it finishes correctly, @ref X7S_RET_ERR otherwise
* (the array of byte ends before find the specific frame).
*
**/
int x7sReadFileMJPEG(X7sByteArray *mjpegFrameData, FILE *pFile, int jump_frame)
{
	//char buf_length[5];
	int full_size = 0, n = 0;
	//    fpos_t pos;

	if (pFile==NULL)
	{
		X7S_PRINT_ERROR("x7sReadFileMJPEG","Error opening file");
	} else
	{
		//     fgetpos (pFile,&pos);// save the position we are because we need to end in the same position
		if (jump_frame >= 0){
			do {
				fseek( pFile , full_size , SEEK_CUR); //the file jump to the init of the next frame, just when the current frame is not the one we are looking for
				if (findNextJpegInit(pFile) == X7S_RET_ERR)
				{
					X7S_PRINT_WARN("x7sReadFileMJPEG()", "END of File before find next frame");
					return X7S_RET_ERR;
				}
				n++;
				fseek ( pFile , -11 , SEEK_CUR);
				fscanf (pFile, "%d", &full_size); // It's the same as the two lines above.
				fseek( pFile , -61 , SEEK_CUR);//56 positions of mjpeg's header and 5 positions of length's frame
			} while(jump_frame >= n);
		} else
		{
			do {
				if (findPrevJpegInit(pFile) == X7S_RET_ERR)
				{
					X7S_PRINT_WARN("x7sReadFileMJPEG()", "Init of File before find previous frame");
					return X7S_RET_ERR;
				}
				n--;
				fseek ( pFile , -11 , SEEK_CUR);
				fscanf (pFile, "%d", &full_size); // It's the same as the two lines above.
				fseek( pFile , -61 , SEEK_CUR);//56 positions of mjpeg's header and 5 positions of length's frame
			} while(jump_frame < n);
		}
		mjpegFrameData->length = full_size;
		fread(mjpegFrameData->data,1,full_size,pFile);

	}

	//	fsetpos (pFile,&pos);// set the position where we started the function
	return X7S_RET_OK;
}

/**
* @brief Extract a X7sMJPEGFrame from an array of bytes.
*
*
*  It extracts the features from a specific MJPEG frame:MJPEG header, type and length of whole frame,
*  JPEG header ,comment of JPEG and JPEG data.It returns on a struct(X7SMJPEGFrame) the Info of each frame(ID,
*  Tag, Time, MData, Alarm) and the JPEG data from a whole MJPEG frame given.
*
* @param frame Output structure where we are going to add the JPEG data, ID, Tag, Time, Blobs and Alarm.
* Each param is add on each argument.
* @note The frame structure must be created outside, and the space for each X7sByteArray must be reserved previously.
* @param mjpegFrameData Input array of bytes which contains the whole MJPEG frame.
* @return @ref X7S_RET_OK when the function ends correctly and @ref X7S_RET_ERR if one of the comments we are looking
*  for is omitted.
*/
int x7sExtractMJPEG(X7sMJPEGFrame *frame,const X7sByteArray *mjpegFrameData)
{
	int ret_code;
	int act_pos;
	uint16_t conv = 0;
	uint16_t comment_length;
	int year, month, day, hour, min, sec;
	char time_fc[20];
	X7sByteArray tmpData;
	iX7sMJPEGIdTag Tag_struct;
	struct tm * timeinfo;


	CV_FUNCNAME("x7sExtractMJPEG()");


	X7S_CHECK_WARN(cvFuncName,frame,X7S_RET_ERR,"frame is NULL");
	X7S_CHECK_WARN(cvFuncName,mjpegFrameData,X7S_RET_ERR,"frame is NULL");
	X7S_CHECK_WARN(cvFuncName,mjpegFrameData->length > 0,X7S_RET_ERR,"mjpegFrameData->length <= 0");
	X7S_CHECK_WARN(cvFuncName,mjpegFrameData->length < X7S_CV_MJPEG_MAXFRAMESIZE,X7S_RET_ERR,"mjpegFrameData->length=%d is too big",mjpegFrameData->length);
	X7S_CHECK_WARN(cvFuncName,mjpegFrameData->type==X7S_CV_MJPEG_FRAMEDATA_TYPE,X7S_RET_ERR,"mjpegFrameData->type=%d is not valid",mjpegFrameData->type);
	X7S_CHECK_WARN(cvFuncName,frame->pJpeg,X7S_RET_ERR,"frame->pJpeg is NULL");
	X7S_CHECK_WARN(cvFuncName,frame->pBlobs,X7S_RET_ERR,"frame->pBlobs is NULL");
	X7S_CHECK_WARN(cvFuncName,frame->pAlarm,X7S_RET_ERR,"frame->pAlarm is NULL");


	// extract image : JPEG
	X7S_CHECK_WARN(cvFuncName,(mjpegFrameData->length-IX7S_SIZEIB_MJPEG_HDR) < frame->pJpeg->capacity,X7S_RET_ERR,
			"mjpegFrameData->length is too big: %d",mjpegFrameData->length);
	frame->pJpeg->length = (mjpegFrameData->length - IX7S_SIZEIB_MJPEG_HDR);
	if(abs(mjpegFrameData->length-IX7S_SIZEIB_MJPEG_HDR) > 65535/*X7S_NET_RTP_MSIZEIB_JPG*/){
		X7S_PRINT_WARN(cvFuncName,"mjpegFrameData->length-IX7S_SIZEIB_MJPEG_HDR > X7S_NET_RTP_MSIZEIB_JPG");
		return X7S_RET_ERR;
	}
	memcpy(frame->pJpeg->data,(mjpegFrameData->data + IX7S_SIZEIB_MJPEG_HDR),abs(mjpegFrameData->length-IX7S_SIZEIB_MJPEG_HDR));
	act_pos = IX7S_SIZEIB_MJPEG_HDR + 1;

	// extract image : ID + Tags
	if (findNextJpegComment(mjpegFrameData, &act_pos) == X7S_RET_ERR)
	{
		X7S_PRINT_WARN(cvFuncName,"END of frame before find next JPEG comment");
		return X7S_RET_ERR;
	}
	memcpy(&conv,(mjpegFrameData->data + act_pos),sizeof(uint16_t));
	comment_length = swapEndian16(conv);
	if(abs(comment_length - 2) > sizeof(iX7sMJPEGIdTag)){
		X7S_PRINT_WARN(cvFuncName,"comment_length - 2 > sizeof(iX7sMJPEGIdTag)");
		return X7S_RET_ERR;
	}
	memcpy(&Tag_struct,(mjpegFrameData->data + act_pos + 2),abs(comment_length - 2));
	frame->id = Tag_struct.id;
	frame->tag = Tag_struct.tag;

	//Correct bug when ID contains 0x...FFFE... hexa decimal code.
	act_pos+=(comment_length-1);

	// extract image : Time
	if (findNextJpegComment(mjpegFrameData, &act_pos) == X7S_RET_ERR)
	{
		X7S_PRINT_ERROR("x7sExtractMJPEG","END of frame before find next JPEG comment");
		return X7S_RET_ERR;
	}
	memcpy(&conv,(mjpegFrameData->data + act_pos),sizeof(uint16_t));
	comment_length = swapEndian16(conv);

	if(abs(comment_length - 2) > 20){
		X7S_PRINT_WARN(cvFuncName,"comment_length - 2 > 20");
		return X7S_RET_ERR;
	}
	memcpy(time_fc,(mjpegFrameData->data + act_pos + 2),abs(comment_length - 2));
	sscanf (time_fc,"%d %*c %d %*c %d %d %*c %d %*c %d ", &day,&month,&year,&hour, &min, &sec);
	if(&frame->time == NULL){
		X7S_PRINT_WARN(cvFuncName,"&frame->time == NULL");
		return X7S_RET_ERR;
	}

	time ( &(frame->time));

	timeinfo = localtime ( &(frame->time) );
	timeinfo->tm_year = year - 1900;
	timeinfo->tm_mon = month-1;
	timeinfo->tm_mday = day;
	timeinfo->tm_hour = hour;
	timeinfo->tm_min = min;
	timeinfo->tm_sec = sec;
	frame->time = mktime(timeinfo);


	// extract image : Blobs
	if (findNextJpegComment(mjpegFrameData, &act_pos) == X7S_RET_ERR)
	{
		X7S_PRINT_WARN(cvFuncName,"END of frame before find the next comment, no blobs & alarm in this frame");
		frame->pBlobs->length=0;
		frame->pAlarm->length=0;
		return X7S_RET_ERR;
	}

	memcpy(&conv,(mjpegFrameData->data + act_pos),sizeof(uint16_t));
	comment_length = swapEndian16(conv);
	tmpData.length = comment_length-2;
	tmpData.data = mjpegFrameData->data + act_pos + 2;
	ret_code = codeFFJPEG2data(&tmpData, frame->pBlobs);
	X7S_CHECK_WARN(cvFuncName,ret_code == X7S_CV_RET_OK,X7S_CV_RET_ERR,"Error with blobs data");

	// extract image : Alarms
	if (findNextJpegComment(mjpegFrameData, &act_pos) == X7S_RET_ERR)
	{
		X7S_PRINT_WARN(cvFuncName,"END of frame before find the next comment, no alarm in this frame");
		frame->pAlarm->length=0;
		return X7S_RET_ERR;
	}
	memcpy(&conv,(mjpegFrameData->data + act_pos),sizeof(uint16_t));
	comment_length = swapEndian16(conv);
	tmpData.length = comment_length - 2;
	tmpData.data = (mjpegFrameData->data + act_pos + 2);
	ret_code = codeFFJPEG2data(&tmpData, frame->pAlarm);
	X7S_CHECK_WARN(cvFuncName,ret_code == X7S_CV_RET_OK,X7S_CV_RET_ERR,"Error with alarm data");


	X7S_PRINT_DEBUG(cvFuncName,"ID: %d, tag=0x%x, time: %s",frame->id,frame->tag,x7sCTime(&(frame->time)));
	return X7S_RET_OK;
}

/**
* @brief Find a specific frame time inside the File.
*
* If this File doesn't contain this time the function return error. This function is faster
*  than exact because we choose a random frame  which has this second(there can be more than 10).
* @param mjpegFrameData Output array of bytes which contains the whole MJPEG frame wanted.
* @param pFile A pointer on the file where we starts looking for this specific frame.
* @param wanted_time time we are looking for (in seconds since unix epoch).
* @return integer which indicates if the function finishes correctly: 0 correct; -1 incorrect(the array of byte ends
*  before find the specific frame).
*/
int x7sFindTimeMJPEG (X7sByteArray *mjpegFrameData, FILE *pFile,time_t wanted_time)
{
	int n = 0, num_frame = 0, act_pos = 0, conv = 0, comment_length = 0;
	char read_strtime[IX7S_SIZEIB_STRTIME+1];
	char wanted_strtime[IX7S_SIZEIB_STRTIME+1];
	long size_act = 0;
	long size_save = 0; 
	long size_slice = 0;
	double dtime;	//Delta time (between read and wanted)
	tm_t read_timeinfo;
	time_t read_time;
	X7S_FUNCNAME("x7sFindTimeMPJEG()");

	read_strtime[IX7S_SIZEIB_STRTIME]=0;	// Put the escape char
	wanted_strtime[IX7S_SIZEIB_STRTIME]=0;	//Put the escape char


	if (pFile==NULL)
	{
		X7S_PRINT_ERROR(funcName,"Error opening file");
	}
	else
	{
		//take the time that we are looking for

		//Get the file half size of the file
		fseek(pFile, 0, SEEK_END);
		size_slice = ftell(pFile)/2;
		size_act = size_slice;


		do{
			X7S_PRINT_DUMP(funcName,"size_act: %d", size_act);

			fseek(pFile, size_act, SEEK_SET);
			//find the time of file's frame
			num_frame = 0;
			if (x7sReadFileMJPEG(mjpegFrameData,pFile,num_frame) == X7S_RET_ERR)
			{
				num_frame = -1;
				x7sReadFileMJPEG(mjpegFrameData,pFile,num_frame);
			}
			// extract image : ID
			act_pos = IX7S_SIZEIB_MJPEG_HDR + 1;

			if (findNextJpegComment(mjpegFrameData, &act_pos) == X7S_RET_ERR)
			{
				X7S_PRINT_ERROR(funcName,"END of Array before find next JPEG comment");
				return X7S_RET_ERR;
			}
			// extract image : Time
			if (findNextJpegComment(mjpegFrameData, &act_pos) == X7S_RET_ERR)
			{
				X7S_PRINT_ERROR(funcName,"END of Array before find next JPEG comment");
				return X7S_RET_ERR;
			}
			memcpy(&conv,(mjpegFrameData->data + act_pos),sizeof(uint16_t));
			comment_length = swapEndian16(conv);
			if(comment_length - 2 > IX7S_SIZEIB_STRTIME+1){
				X7S_PRINT_WARN(funcName,"comment_length - 2 > IX7S_SIZEIB_STRTIME+1");
				return X7S_RET_ERR;
			}
			memcpy(read_strtime,(mjpegFrameData->data + act_pos + 2),(comment_length - 2));

			read_time = ix7sReadTime(read_strtime,&read_timeinfo);
			dtime =  difftime(wanted_time,read_time);

			X7S_PRINT_DUMP(funcName,"difftime=%f (w:%d-r:%d) %s",dtime,wanted_time,read_time,read_strtime);

			if (fabs(dtime)>3600)
			{
				X7S_PRINT_ERROR(funcName,"Error: Looking at wrong file: different hour, day, month or year.");
				return X7S_RET_ERR;
			}

			// We try to find the faster method to find the time into the file : dichotomy method
			if (dtime==0)
			{
				X7S_PRINT_INFO(funcName,"We've needed %d iteration to find the frame wanted.", n);
				localtime(&wanted_time);
				return X7S_RET_OK;
			}
			else {
				if(dtime>0.0)
					size_act = size_act + size_slice/2; //Forward in the file
				else
					size_act = size_act - size_slice/2;		//Reward in the file
			}
			n++;
			size_slice = size_slice/2;
			if (size_act == size_save || size_slice < IX7S_SIZEIB_JPEG_MIN) break;//If the time wanted is not in the File, we break the file before 100000 iterations or if the size of the slice is less the minimum JPEG.
			else size_save = size_act; // save the position because if in the next iteration the position is the same, the time is lock and we need to break the while
		} while( n < 100000 );
	}

	ix7sStrfTime(wanted_strtime,wanted_time);
	X7S_PRINT_INFO(funcName,"Last read frame of the file: %s; and time wanted: %s.", read_strtime, wanted_strtime);
	X7S_PRINT_ERROR(funcName,"The function didn't find the time we are looking: %d iterations", n);
	return X7S_RET_ERR;
}

/**
* @brief Find the first mjpegFrameData with a specific tag.
* @param mjpegFrameData	 Output array of bytes which contains the whole MJPEG frame wanted.
* @param pFile  A pointer on the file where we starts looking for this specific frame.
* @param search_tag The specific tag we want to search, typically the reference frame.
* @see X7sMJPEGFrame.tag, @ref X7S_CV_MJPEG_FRAMETAG_xxx.
* @return @ref X7S_RET_OK if the frame was found, Otherwise it return @ref X7S_RET_ERR.
*/
int x7sFindTagMJPEG(X7sByteArray *mjpegFrameData, FILE *pFile, uint8_t search_tag)
{
	iX7sMJPEGFrameHdr jpegFrameHdr;
	uint8_t tag;
	int act_pos = 0;
	int ret=X7S_RET_ERR;

	CV_FUNCNAME("x7sFindTagMJPEG()");

	X7S_CHECK_WARN(cvFuncName,pFile,X7S_RET_ERR,"pFile is NULL");
	X7S_CHECK_WARN(cvFuncName,mjpegFrameData,X7S_RET_ERR,"mjpegFrameData is NULL");

	while(!feof(pFile))
	{
		act_pos=0;

		//Go from frame to frame
		ret = x7sReadFileMJPEG(mjpegFrameData,pFile,0);
		if(ret==X7S_RET_ERR) break;

		//Set a structure that fit the header.
		if (findNextJpegComment(mjpegFrameData, &act_pos) == X7S_RET_ERR)
		{
			X7S_PRINT_ERROR(cvFuncName,"END of Array before find next JPEG comment");
			return X7S_RET_ERR;
		}

		memcpy(&(jpegFrameHdr),(mjpegFrameData->data + act_pos-2),sizeof(jpegFrameHdr));
		tag=(uint8_t)jpegFrameHdr.tag;

		//Check if the tag is OK.
		if(tag==search_tag) return X7S_RET_OK;
	}

	X7S_PRINT_WARN(cvFuncName,"Tag %0x was not found",search_tag);
	return X7S_RET_ERR;
}

/**
* @brief Compute the FPS of a MJPEG sequence, and return the time to wait between each frame.
*
* @param mjpegFrame The MJPEG frame structure.
* @param mjpegFPS	The Structure to compute the frameRate.
* @return The time to wait between each frame (in milliseconds).
*/
int x7sGetFpsMJPEG(const X7sMJPEGFrame *mjpegFrame, X7sMJPEGFrameRate *mjpegFPS)
{
	double diff;

	//Compute the FPS.
	if(mjpegFPS->t0==0) {	//We are at a new seconds
		mjpegFPS->t0=mjpegFrame->time;
		mjpegFPS->t0_id=mjpegFrame->id;
	}
	//Then check if we are in a new second
	diff=difftime(mjpegFrame->time,mjpegFPS->t0);
	if(diff>0)
	{
		if(mjpegFPS->first_t0)
		{
			mjpegFPS->first_t0=FALSE;
			mjpegFPS->fps=15;
		}
		else
		{
			mjpegFPS->fps=(float)((mjpegFrame->id-mjpegFPS->t0_id)+1);
		}
		mjpegFPS->t0=0;
	}
	return (int)((1000/mjpegFPS->fps)-3);	//The time lost to check both time is about 3ms
}
