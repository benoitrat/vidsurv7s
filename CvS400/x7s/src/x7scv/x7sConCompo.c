#include "x7scv.h"
#include "x7sCvLog.h"

//================================================================================
//-- Private Prototypes

// =============================================================================== */
//-- Functions

/**
 * @brief Allocate the memory and init an array of connected component.
 * @see x7sResetConCompoList.
 */
X7sConCompoList* x7sCreateConCompoList(int max_size) {

	X7sConCompoList *ccList = calloc(1,sizeof(X7sConCompoList));
	ccList->vec = calloc(max_size,sizeof(X7sConCompo));
	ccList->max_size=max_size;
	ccList->size=max_size;

	x7sResetConCompoList(ccList);

	return ccList;
}

/**
 * @brief Reset the connexed component list.
 * @note Only the "size" first CC are reset, if we want to have them all reset
 * X7sConCompoList::size should be equal to X7sConCompoList::max_size.
 * @param ccList A pointer on a CC list.
 * After this call each "X7sConCompoList::size" first X7sConCompo is initiate to its default
 * value and the size value is reset to zeros.
 */
void x7sResetConCompoList(X7sConCompoList* ccList) {
	int i;
	for(i=0;i<ccList->size;i++) {

		//Init the minimum value
		ccList->vec[i].min_x=INT32_MAX;
		ccList->vec[i].min_y=INT32_MAX;

		//Init The maximum value
		ccList->vec[i].max_x=INT32_MIN;
		ccList->vec[i].max_y=INT32_MIN;

		//Init the area
		ccList->vec[i].area=0;

		//Init the position
		ccList->vec[i].x=0;
		ccList->vec[i].y=0;
	}
	ccList->size=0;
}

/**
 * @brief Deallocate the memory used for this X7sConCompoList.
 * @param ccList is a pointer on the pointer of bgModel
 * @note Using a pointer on a pointer let us reset the pointer to NULL,
 * and therefore be sure that it will be not used outside this function.
 */
void x7sReleaseConCompoList(X7sConCompoList **ccList) {
	free((*ccList)->vec);
	free((*ccList));
	(*ccList)=NULL;
}

/**
 * @brief Generate a list of Connexed Component from an Black/White image.
 *
 *		-# First, convert the BW  {0,1} to labeled image [1-255].
 *		-# Reset the list.
 *		-# Get the size, position of each blobs.
 *		-# Finally, draw the bounding box on the image.
 *
 *  @return @ref X7S_CV_RET_OK if no error occurs, and @ref X7S_CV_RET_ERR if for the maximum number
 *  of cc in the list is smaller then the number of label in the image.
 */
int x7sUpdateConCompoList(X7sConCompoList *ccList, IplImage *imBW, bool draw) {

	int ret=X7S_CV_RET_OK;

	//First reset the list of CC.
	x7sResetConCompoList(ccList);

	//Then label the image.
	ccList->size = x7sMakeBlobs(imBW);

	//Check maximum size
	if(ccList->size > ccList->max_size) {
		ccList->size = ccList->max_size;
		ret=X7S_CV_RET_ERR;
	}

	//Then Fill the list of CC.
	x7sFillConCompoList(ccList,imBW);

	//Then Draw the function.
	if(draw) {
		x7sDrawConCompoList(imBW,(const X7sConCompoList*)ccList);
	}
	return ret;
}


/**
 * @brief Sort the Connexed Component list based on the X7sConCompo::area.
 * @param ccList A pointer on a connected component list.
 * @param asc_order If order>0, sort in ascending order. Otherwise sort in descending order.
 */
int x7sSortConCompoList(X7sConCompoList *ccList, int asc_order)
{
	int i,j;
	int sign;
	X7sConCompo temp;
	X7sConCompo *pP, *pN;

	CV_FUNCNAME("x7sSortConCompoList()");

	if(ccList==NULL)
	{
		X7S_PRINT_ERROR(cvFuncName,"ccList is NULL");
		return X7S_CV_RET_ERR;
	}

	if(asc_order) sign=+1;
	else sign=-1;

	for(i=1; i<ccList->size; ++i)
	{
		for(j=i; j>0; --j)
		{
			pP = &ccList->vec[j-1];
			pN = &ccList->vec[j];
			if((pN->area - pP->area)*sign>0) break;
			CV_SWAP(pN[0],pP[0],temp);
		}
	}
	return X7S_CV_RET_OK;
}


/**
 * @brief Fill the Connexed Component List based on a labeled image.
 * @param ccList A pointer on a CC list with X7sConCompoList::size set by the maximum value of im.
 * @param im A blob labeled image with label between [1-255].
 */
void x7sFillConCompoList(X7sConCompoList *ccList, const IplImage *im) {
	int x,y;
	uchar *line, id; 		//Pointer on the image

	CV_FUNCNAME("x7sFillConCompoList()");

	if (im->nChannels!=1 || im->depth!=IPL_DEPTH_8U) {
		X7S_PRINT_ERROR(cvFuncName,"Image input is not valid");
		return;
	}

	//Itarate over the whole image
	for (y=0; y<im->height; y++) {
		line=(uchar*)(im->imageData+y*im->widthStep);
		for (x=0; x<im->width; x++) {

			//If pixel is not black
			if (line[x]) {
				//The labels start from 1 but the array of blobs from 0
				id=line[x]-1;
				if(id < ccList->max_size) {

					//Set Max Values (Bounding Box)
					if(ccList->vec[id].max_x < x) ccList->vec[id].max_x = x;
					if(ccList->vec[id].max_y < y) ccList->vec[id].max_y = y;
					//Set Min Values (Bouding Box)
					if(ccList->vec[id].min_x > x) ccList->vec[id].min_x = x;
					if(ccList->vec[id].min_y > y) ccList->vec[id].min_y = y;
					//Set Center Position
					ccList->vec[id].x+=x;
					ccList->vec[id].y+=y;
					//Set The area
					ccList->vec[id].area++;
				}
			}
		}
	}

	for(id=0;id<ccList->size;id++) {
		ccList->vec[id].x/=ccList->vec[id].area;
		ccList->vec[id].y/=ccList->vec[id].area;
	}
}

/**
 * @brief Print the value of one X7sConCompo for debugging.
 */
void x7sPrintConCompo(X7sConCompo* cc) {
	X7S_PRINT_INFO(NULL,"{Center=[%d,%d]; Lim(x,y)=([%d,%d],[%d,%d]); Area[%d]} ",cc->x,cc->y,cc->min_x,cc->max_x,cc->min_y,cc->max_y,cc->area);
}



