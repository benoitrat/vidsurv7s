#include "x7scv.h"
#include "x7sCvLog.h"

//------------------ Private prototypes.

int x7sErodeFast_1C8U(IplImage *im, int size);
int x7sDilateFast_1C8U(IplImage *im, int size);

//------------------ Public function (Check arguments)

/**
* @brief This function is a Fast square Erode.
* @param im The input image in @ref CV_8U1C format.
* @param size represent the number of width of the square (square is @tt{size}x@tt{size}).
* @return @x7scvret
*/
int x7sErodeFast(IplImage *im,int size) {

	CV_FUNCNAME("x7sErodeFast()");

	if(im->nChannels==1 && im->depth==IPL_DEPTH_8U) {
		return x7sErodeFast_1C8U(im,size);
	}
	else {
		X7S_PRINT_ERROR(cvFuncName,"Images are not compatible");
		return X7S_CV_RET_ERR;
	}

}


/**
* @brief This function is a Fast square Dilate.
* @param im The input image in @ref CV_8U1C format.
* @param size correspond to the width of the square (square is @tt{size} x @tt{size}).
* @return @x7scvret
*/
int x7sDilateFast(IplImage *im, int size) {

	CV_FUNCNAME("x7sErodeFast()");

	if(im->nChannels==1 && im->depth==IPL_DEPTH_8U) {
		return x7sDilateFast_1C8U(im,size);
	}
	else {
		X7S_PRINT_ERROR(cvFuncName,"Images are not compatible");
		return X7S_CV_RET_ERR;
	}
}

//------------------ Private function

/**
 * @brief @ref x7sErodeFast() function for @ref CV_8U1C image.
 * @see x7sErodeFast().
 */
int x7sErodeFast_1C8U(IplImage *im, int size) {

	int i;
	uchar *m, *mend, *pend, *line, *s;
	uchar res, size1, size2;

	//Temporary line (keep the pixel value for the previous line)
	line=(uchar*)malloc(im->width);


	//Compute the size of the dilation
	size1 = size/2;
	size2 = (size+1)/2;
	size = size1; 	//To not erode a pixel we need to have at least n=size1 white pixels above
	res = 0;		//To not erode a pixel we need to have at least n=size1 white pixels on the left

	m = (uchar*)(im->imageData);	//first pixel of the image
	mend = m + im->imageSize;		//(last+1) pixel of the image
	pend = m + im->width;		//next line


	//Set the temporary line to the value size
	for(i=0;i<im->width;i++) line[i] = size;
	s = (uchar*)(line);


	while (m < mend) {
		//If pixel is black (p != [1,255])
		if(!*m) {
			res = size;
			*s = size;
		}
		//If pixel is white
		else {
			//if the pixel is the first size1 it will be eroded.(need to have at least size1 pixel to not be eroded)
			if(res) {
				res--; //Reduce the limit of minimum number of white pixel on the left
				*m = 0; //Erode pixel
				*s = size;
			}
			//if the pixel on the line above was black or eroded.
			else if (*s) {
				*m = 0; //Erode pixel
				(*s)--; //Reduce the limit of minimum number of white pixel above
			}
		}

		//Go to the next pixel
		m++;
		s++;

		//Go to the next row
		if (m == pend){
			pend = m + im->width;
			res = size;
			s = (uchar*)(line);
		}
	}

	/* ---------------------------------------------
	Now restart the same operation in the other side
	(from bottom-right to top-left) and with size2
	 --------------------------------------------*/
	size = size2;   //To not erode a pixel, we need to have at least n=size1 white pixels below (on the previous line)
	res = size;		//To not erode a pixel, we need to have at least n=size1 white pixels on the right

	mend = (uchar*)(im->imageData) - 1; //(first-1) pixel in the image
	m = mend + im->imageSize;			//last pixel in the image
	pend = m - im->width;				//previous line


	for(i=0;i<im->width;i++) line[i] = size;
	s = (uchar*)(line);

	while (m > mend) {
		if(!*m) {
			res = size;
			*s = size;
		} else {
			if(res) {
				res--;
				*m = 0;
				*s = size;
			} else {
				if (*s) {
					*m = 0;
					(*s)--;
				}
			}
		}
		m--;
		s++;
		if (m == pend){
			pend = m - im->width;
			res = size;
			s = (uchar*)(line);
		}
	}

	free(line);

	return X7S_CV_RET_OK;

}

/**
 * @brief @ref x7sDilateFast() function for @ref CV_8U1C image.
 * @see x7sDilateFast().
 */
int x7sDilateFast_1C8U(IplImage *im, int size) {

	int i;
	uchar *m, *mend, *pend, *line, *s;
	uchar res, size1, size2;



	//Temporary line (keep the pixel value for the previous line)
	line=(uchar*)malloc(im->width);


	//Compute the size of the dilation
	size1 = size/2;
	size2 = (size+1)/2;
	size = size1; 	//To not dilate a pixel we need to have at least n=size1 black pixels above
	res = 0;		//To not dilate a pixel we need to have at least n=size1 black pixels on the left

	m = (uchar*)(im->imageData);	//first pixel of the image
	mend = m + im->imageSize;		//(last+1) pixel of the image
	pend = m + im->width;		//next line


	//Set the temporary line to zeros
	for(i=0;i<im->width;i++) line[i] = 0;
	s = (uchar*)(line);



	while (m < mend) {
		//If pixel is white (p == [1,255])
		if(*m) {
			res = size;
			*s = size;
			*m = UINT8_MAX;
		} else { //If pixel is white
			//Look pixels on the left: if it is one of the first size1-one it will be dilated.
			//(need to have at least size1 black pixels on the left to not be dilated)
			if(res) {
				res--; //Reduce the limit of minimum number of black pixel on the left
				*m = UINT8_MAX;
				*s = size;
			}
			//Look pixels on the top: if it is one of the first size1-one it will be dilated.
			//(need to have at least size1 black pixels on the top to not be dilated)
			else if (*s) {
				*m = UINT8_MAX;
				(*s)--; //Reduce the limit of minimum number of black pixel above
			}
		}

		//Go to next pixel
		m++;
		s++;

		//Go to the next row
		if (m == pend){
			pend = m + im->width;
			res = 0;
			s = (uchar*)(line);
		}
	}



	/* ---------------------------------------------
	Now restart the same operation in the other side
	(from bottom-right to top-left) and with size2
	 --------------------------------------------*/
	size = size2;   //To not dilate a pixel we need to have at least n=size1 black pixels below (on the previous line)
	res = 0;		//To not dilate a pixel we need to have at least n=size1 black pixels on the right

	mend = (uchar*)(im->imageData) - 1; //(first-1) pixel in the image
	m = mend + im->imageSize;			//last pixel in the image
	pend = m - im->width;				//previous line


	for(i=0;i<im->width;i++) line[i] = 0;
	s = (uchar*)(line);


	while (m > mend) {
		if(*m) {
			res = size;
			*s = size;
		} else {
			if(res) {
				res--;
				*m = UINT8_MAX;
				*s = size;
			} else {
				if (*s) {
					*m = UINT8_MAX;
					*s -= 1;
				}
			}
		}
		m--;
		s++;
		if (m == pend){
			pend = m - im->width;
			res = 0;
			s = (uchar*)(line);
		}
	}
	free(line);

	return X7S_CV_RET_OK;
}

