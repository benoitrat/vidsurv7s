#include "x7scv.h"

/*
 * Includes done in cpp file to accelerate compilation.
 */


//------------------ Private prototypes.
int x7sDownSample_YUV422Mean2(const IplImage *src,IplImage *dst);

//------------------ Public function (Check arguments)

/**
* @brief Perform specific downsampling according to type (reducing size of the image by 2).
* @param src An image in YUV422 format
* @param dst An image in YUV422 but with dst->width=src->width/2 and dst->height=dst->height/2.
* @param type The type of downsampling (See  @ref X7S_CV_DOWNSAMP_xxx).
* @return @x7scvret
*/
int x7sDownSample(const IplImage *src,IplImage *dst, int type) {

	//For YUV422
	if(src->nChannels==2 && dst->nChannels==2)
	{
		switch(type)
		{
		case X7S_CV_DOWNSAMP_YUV422_MEAN2 : return x7sDownSample_YUV422Mean2(src,dst);
		case X7S_CV_DOWNSAMP_YUV422_MEAN2Y: return x7sDownSample_YUV422Mean2Y(src,dst);
		}
	}
	return X7S_CV_RET_ERR;
}

//------------------ Private function

/**
* @brief @ref x7sDownSample() function when type equal to @ref X7S_CV_DOWNSAMP_YUV422_MEAN2Y
*
* Obtain two means of Y value, with 4 Y from source. The U,V value are obtain taking the second pixel.
*
* @code
* //src image (four pixels = 8 bits).
* | U01 | Y0 | V01 | Y1 | U23 | Y2 | V23 | Y3 |
*
* //dst image (two pixels = 4 bits)
* | U23 | (Y0 + Y2)/2 | V23 | (Y1 + Y3)/2 |
*
* @endcode
*
* @see x7sDownSample()
*/
int x7sDownSample_YUV422Mean2Y(const IplImage *src,IplImage *dst) {

	int y;
	uint8_t *p_i, *p_o,*endl;
	float tmp_U,tmp_Y1,tmp_V,tmp_Y2;

	//Set the starting point of the pointers
	p_i = (uchar*)src->imageData;
	p_o = (uchar*)dst->imageData;
	endl=(p_o+dst->widthStep-4);

	//printf("v7DownSample_YUV422Mean2 \n"); fflush(stdout);

	//Loop on each line of output
	for(y=0;y<dst->height;y++) {
		//Loop on each 2-by-2 pixels of output.
		while(p_o<endl) {

			tmp_U = (float)(p_i[4]);
			tmp_Y1 = (float)(p_i[1] + p_i[5])/2.f;
			tmp_Y2 = (float)(p_i[3] + p_i[7])/2.f;
			tmp_V = (float)(p_i[6]);


			p_o[0] = (uint8_t)(tmp_U  + 0.5);
			p_o[1] = (uint8_t)(tmp_Y1 + 0.5);
			p_o[2] = (uint8_t)(tmp_V  + 0.5);
			p_o[3] = (uint8_t)(tmp_Y2 + 0.5);

			//Jump 4-by-4 pixels (8 bytes) on input and 2-by-2 pixels (4 bytes) on output.
			p_o+=4;
			p_i+=8;
		}
		endl+=dst->widthStep;	//Set next end of line
		p_i+=src->widthStep;	//Jump one line in src image.
	}
	return X7S_CV_RET_OK;
}

/**
* @brief @ref x7sDownSample() function when type equal to @ref X7S_CV_DOWNSAMP_YUV422_MEAN2
*
* Make the mean of each channel of the four pixels, to obtain two pixels.
*
* @code
* //src image (four pixels = 8 bits).
* | U01 | Y0 | V01 | Y1 | U23 | Y2 | V23 | Y3 |
*
* //dst image (two pixels = 4 bits)
* (| (U01 + U23) | (Y0 + Y2) | (V01 + V23) | (Y1 + Y3) |)*0.5
*
* @endcode
*
* @see x7sDownSample()
*
*/
int x7sDownSample_YUV422Mean2(const IplImage *src,IplImage *dst) {

	int y;
	uint8_t *p_i, *p_o,*endl;
	float tmp_U,tmp_Y1,tmp_V,tmp_Y2;

	//Set the starting point of the pointers
	p_i = (uchar*)src->imageData;
	p_o = (uchar*)dst->imageData;
	endl=(p_o+dst->widthStep-4);

	//printf("v7DownSample_YUV422Mean2 \n"); fflush(stdout);

	//Loop on each line of output
	for(y=0;y<dst->height;y++) {
		//Loop on each 2-by-2 pixels of output.
		while(p_o<endl) {

			tmp_U  = (float)(p_i[0] + p_i[4])/2.f;
			tmp_Y1 = (float)(p_i[1] + p_i[5])/2.f;
			tmp_V  = (float)(p_i[2] + p_i[6])/2.f;
			tmp_Y2 = (float)(p_i[3] + p_i[7])/2.f;

			p_o[0] = (uint8_t)(tmp_U  + 0.5);
			p_o[1] = (uint8_t)(tmp_Y1 + 0.5);
			p_o[2] = (uint8_t)(tmp_V  + 0.5);
			p_o[3] = (uint8_t)(tmp_Y2 + 0.5);

			//Jump 4-by-4 pixels (8 bytes) on input and 2-by-2 pixels (4 bytes) on output.
			p_o+=4;
			p_i+=8;
		}
		endl+=dst->widthStep;	//Set next end of line
		p_i+=src->widthStep;	//Jump one line in src image.
	}
	return X7S_CV_RET_OK;
}

/**
* @brief Split YUV422 image in Y,U,V channels.
* @note It is important to notice that:
* 		- U->width=V->width=0.5*Y->width;
* 		- U->height=V->height=0.5*Y->height;
* 		- Y->size = src->size.
* @param src The source image (normal size, CV_3C8U)
* @param Y the luminance channel (normal size)
* @param U the U channel (half size)
* @param V the V channel (half size)
* @param is_even (Not used here)
* @return @x7scvret
*/
int x7sSplitYUV422(const IplImage *src, IplImage *Y, IplImage *U, IplImage *V, bool is_even) {

	uint8_t *p_YUV, *p_Y, *p_U, *p_V,*end;

	//Set the starting point of the pointers
	p_YUV = (uchar*)src->imageData;
	end=(p_YUV+src->imageSize);

	p_Y = (uchar*)Y->imageData;
	p_U = (uchar*)U->imageData;
	p_V = (uchar*)V->imageData;


	while(p_YUV<end) {
		*(p_U++)=*(p_YUV++);
		*(p_Y++)=*(p_YUV++);
		*(p_V++)=*(p_YUV++);
		*(p_Y++)=*(p_YUV++);
	}
	return X7S_CV_RET_OK;
}



/**
* @brief Merge Y,U,V separate image in a YUV422 image.
* @note It is important to notice that:
* 		- U->width=V->width=0.5*Y->width;
* 		- U->height=V->height=0.5*Y->height;
* 		- Y->size = src->size.
*
* @param Y the luminance channel (normal size)
* @param U the U channel (half size)
* @param V the V channel (half size)
* @param dst The destination image (normal size, CV_3C8U)
* @param is_par Not used.
* @return @x7scvret
*/
int x7sMergeYUV422(const IplImage *Y, const IplImage *U, const IplImage *V, IplImage *dst, bool is_par) {


	uint8_t *p_YUV, *p_Y, *p_U, *p_V,*end;


	//Set the starting point of the pointers
	p_YUV = (uchar*)dst->imageData;
	end=(p_YUV+dst->imageSize);

	p_Y = (uchar*)Y->imageData;
	p_U = (uchar*)U->imageData;
	p_V = (uchar*)V->imageData;


	while(p_YUV<end) {
		*(p_YUV++)=*(p_U++);
		*(p_YUV++)=*(p_Y++);
		*(p_YUV++)=*(p_V++);
		*(p_YUV++)=*(p_Y++);
	}
	return X7S_CV_RET_OK;
}

/**
 * @brief Downsampling taking only even or odd line.
 * @param src The source image
 * @param dst	the destination image
 * @param is_even if @true we keep only even line, otherwise only odd line.
 * @return @x7scvret
 */
int x7sDownSampleLine(const IplImage *src, IplImage *dst, bool is_even) {
	int y;
	uint8_t *p_src,*endl;
	uint8_t *p_dst;


	if(is_even) {
		y=0;
		p_src = (uchar*)src->imageData;	//Go to beginning of line 0
	}
	else {
		y=1;
		p_src=(uchar*)src->imageData + src->widthStep;	//Go to beginning of line 1
	}

	p_dst= (uchar*)dst->imageData;


	for(y=0;y<dst->height;y+=2) {
		//Loop on each 2-by-2 pixels of output.
		endl=(p_src+src->widthStep);
		while(p_src<endl) {
			*(p_dst++)=*(p_src++);
		}
		//Jump one line
		p_src+=src->widthStep;

	}
	return X7S_CV_RET_OK;
}



