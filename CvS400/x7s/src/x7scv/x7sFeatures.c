#include "x7scv.h"
#include "x7sCvLog.h"
#include "cvaux.h"
#include "cv.h"

//================================================================================
//-- Private Prototypes
#define IX7S_CV_CIF_W 352
#define IX7S_CV_CIF_H 288


// =============================================================================== */
//-- Functions

/**
* @brief Allocate the memory and init an array of connected component.
* @param The size of the image, if NULL we use a default CIF-PAL resolution (352x288).
* @see x7sResetConCompoList.
*/
X7sFeaturesList* x7sCreateFeaturesList(const CvSize *size) {

	X7sFeaturesList *featList = calloc(1,sizeof(X7sFeaturesList));

	featList->max_num_features = 500;
	featList->max_num_boxfeatures = 100;

	featList->vec = 0; //calloc(featList->max_num_boxfeatures, sizeof(X7sFeaturesBox));
	featList->num_nbox = 10;

	featList->quality = 0.001;
	featList->min_distance = 10;
	featList->count_features = 100;
	featList->win_size = 5;
	featList->last_IDbox = 0;
	featList->min_area = 2500;
	featList->min_area_delete = 1000;
	featList->interval[0] = 0.5;
	featList->interval[1] = 2.0;
	featList->max_count_history = 5;
	featList->min_movement = 1;
	featList->max_area_person = 13000;
	featList->max_dim_xy = 200;
	featList->min_dim_xy = 20;

	featList->size = 0;
	featList->num_features = 0;
	featList->num_boxfeatures = 0;

	if(size) 	featList->size_img=*size;
	else featList->size_img= cvSize(IX7S_CV_CIF_W, IX7S_CV_CIF_H);

	featList->pImg_clone = cvCreateImage(featList->size_img, 8, 3 );
	featList->eig  = cvCreateImage(featList->size_img, 32, 1 );
	featList->grey  = cvCreateImage(featList->size_img, 8, 1 );
	featList->prev_grey = cvCreateImage(featList->size_img, 8, 1 );
	featList->temp  = cvCreateImage(featList->size_img, 32, 1 );
	featList->mask_tracker = cvCreateImage(featList->size_img, 8, 1 );
	featList->pyramid = cvCreateImage( featList->size_img, 8, 1 );
	featList->prev_pyramid = cvCreateImage( featList->size_img, 8, 1 );

	featList->hist_size[0] = 32;
	featList->hist_size[1] = 16;
	featList->hist_size[2] = 16;
	featList->range[0] = 0;
	featList->range[1] = 255;

	featList->ranges[0] = featList->range;
	featList->ranges[1] = featList->range;
	featList->ranges[2] = featList->range;

	featList->rect_ini = cvCreateImage(featList->size_img, 8, 3 );
	featList->rect_fin = cvCreateImage(featList->size_img, 8, 3 );
	featList->yuv = cvCreateImage(featList->size_img, 8, 3 );
	featList->mask_hist = cvCreateImage(featList->size_img, 8, 1);
	featList->pImg_diff = cvCreateImage(featList->size_img, 8, 1);
	featList->pImg_diff2 = cvCreateImage(featList->size_img, 8, 1);

	featList->backproject = cvCreateImage(featList->size_img, 8, 1);
	featList->backprojectGRAY = cvCreateImage(featList->size_img, 8, 1);
	featList->back_image = cvCreateImage(featList->size_img, 8, 3);

	featList->hist_fin = cvCreateHist( 3, featList->hist_size, CV_HIST_ARRAY, featList->ranges, 1 );

	featList->y_plane_i = cvCreateImage(featList->size_img,8,1);
	featList->u_plane_i = cvCreateImage(featList->size_img,8,1);
	featList->v_plane_i = cvCreateImage(featList->size_img,8,1);
	featList->planes_i[0]=featList->y_plane_i;
	featList->planes_i[1]=featList->u_plane_i;
	featList->planes_i[2]=featList->v_plane_i;

	featList->y_plane_f = cvCreateImage(featList->size_img,8,1);
	featList->u_plane_f = cvCreateImage(featList->size_img,8,1);
	featList->v_plane_f = cvCreateImage(featList->size_img,8,1);
	featList->planes_f[0]=featList->y_plane_f;
	featList->planes_f[1]=featList->u_plane_f;
	featList->planes_f[2]=featList->v_plane_f;


	featList->points[0] = (CvPoint2D32f*)cvAlloc(featList->max_num_features*sizeof(featList->points[0][0]));
	featList->points[1] = (CvPoint2D32f*)cvAlloc(featList->max_num_features*sizeof(featList->points[0][0]));
	featList->points_IDbox = (int*)calloc(featList->max_num_features, sizeof(int));
	featList->lost_points = (int*)calloc(featList->max_num_features, sizeof(int));
	featList->valid_ID = (int*)calloc(featList->max_num_features, sizeof(int));
	featList->points_difx = (float*)calloc(featList->max_num_features, sizeof(float));
	featList->points_dify = (float*)calloc(featList->max_num_features, sizeof(float));

	featList->status = (char*)cvAlloc(featList->max_num_features);
	featList->flags_pyramid = 0;
	featList->temp_flags = 1;


	return featList;
}

bool x7sBoxOverlapping(X7sConCompo *ccbox, X7sFeaturesBox *fbox){

	float centx_ccbox, centy_ccbox, centx_fbox, centy_fbox;

	if(ccbox->min_x <= fbox->min_x && fbox->min_x <= ccbox->max_x){
		if(ccbox->min_y <= fbox->min_y && fbox->min_y <= ccbox->max_y){
			return TRUE;
		}
	}
	if(ccbox->min_x <= fbox->max_x && fbox->max_x <= ccbox->max_x){
		if(ccbox->min_y <= fbox->min_y && fbox->min_y <= ccbox->max_y){
			return TRUE;
		}
	}

	if(ccbox->min_x <= fbox->min_x && fbox->min_x <= ccbox->max_x){
		if(ccbox->min_y <= fbox->max_y && fbox->max_y <= ccbox->max_y){
			return TRUE;
		}
	}

	if(ccbox->min_x <= fbox->max_x && fbox->max_x <= ccbox->max_x){
		if(ccbox->min_y <= fbox->max_y && fbox->max_y <= ccbox->max_y){
			return TRUE;
		}
	}

	//-------------------------
	if(fbox->min_x <= ccbox->min_x && ccbox->min_x <= fbox->max_x){
		if(fbox->min_y <= ccbox->min_y && ccbox->min_y <= fbox->max_y){
			return TRUE;
		}
	}
	if(fbox->min_x <= ccbox->max_x && ccbox->max_x <= fbox->max_x){
		if(fbox->min_y <= ccbox->min_y && ccbox->min_y <= fbox->max_y){
			return TRUE;
		}
	}

	if(fbox->min_x <= ccbox->min_x && ccbox->min_x <= fbox->max_x){
		if(fbox->min_y <= ccbox->max_y && ccbox->max_y <= fbox->max_y){
			return TRUE;
		}
	}

	if(fbox->min_x <= ccbox->max_x && ccbox->max_x <= fbox->max_x){
		if(fbox->min_y <= ccbox->max_y && ccbox->max_y <= fbox->max_y){
			return TRUE;
		}
	}

	centx_ccbox = ccbox->min_x + ((float)ccbox->max_x - (float)ccbox->min_x)/2.0;
	centy_ccbox = ccbox->min_y + ((float)ccbox->max_y - (float)ccbox->min_y)/2.0;
	centx_fbox = fbox->min_x + ((float)fbox->max_x - (float)fbox->min_x)/2.0;
	centy_fbox = fbox->min_y + ((float)fbox->max_y - (float)fbox->min_y)/2.0;


	if(ccbox->min_x <= centx_fbox && centx_fbox <= ccbox->max_x){
		if(ccbox->min_y <= centy_fbox && centy_fbox <= ccbox->max_y){
			return TRUE;
		}
	}

	if(fbox->min_x <= centx_ccbox && centx_ccbox <= fbox->max_x){
		if(fbox->min_y <= centy_ccbox && centy_ccbox <= fbox->max_y){
			return TRUE;
		}
	}


	return FALSE;

}


X7sFeaturesHeaderBox * x7sCreateHeaderBox(const X7sFeaturesList* pFeatList,X7sFeaturesHeaderBox* pFeatPrev)
{
	X7sFeaturesHeaderBox *pFeat=NULL;
	int m=0;
	if(pFeatList)
	{
		pFeat = (X7sFeaturesHeaderBox *)malloc(sizeof(X7sFeaturesHeaderBox));
		pFeat->next_HeaderBox = NULL;
		pFeat->prev_HeaderBox = pFeatPrev;
		pFeat->last_index = 0;
		pFeat->ok=FALSE;

		//Create the box list
		pFeat->box_list = (X7sFeaturesBox *)malloc(pFeatList->num_nbox*sizeof(X7sFeaturesBox));
		for(m = 0; m < pFeatList->num_nbox; m++) pFeat->box_list[m].hist_ini = NULL;

		pFeat->box_list[0].hist_ini = cvCreateHist(3,(int*)pFeatList->hist_size, CV_HIST_ARRAY, (float**)pFeatList->ranges, 1);
		pFeat->box_list[0].flag_hist = 0;

		if(pFeatPrev) pFeatPrev->next_HeaderBox=pFeat;

	}

	return pFeat;
}



void x7sDelHeaderBox(X7sFeaturesHeaderBox **first, X7sFeaturesHeaderBox **f, int nbox){

	X7sFeaturesHeaderBox *next=NULL, *prev=NULL;
	int i=0;

	X7S_CHECK_WARN("x7sDelHeaderBox()",(*f),,"*f or *first are NULL");

	//Borrar historial de histogramas.
	for(i=0;i<nbox;i++)
	{
		if((*f)->box_list[i].hist_ini!=NULL)
			cvReleaseHist(&((*f)->box_list[i].hist_ini));
	}
	free((*f)->box_list);


	prev = (*f)->prev_HeaderBox;
	next = (*f)->next_HeaderBox;


	//If first element
	if(prev == NULL){
		if(next == NULL){
			free((*f));
			(*f) = NULL;
			(*first) = NULL;
		}
		else{
			next->prev_HeaderBox = NULL;
			free((*f)); // Delete actual
			(*f) = next; //actual is next
			(*first) = next;
		}
	}
	else{
		//Last element
		if(next == NULL){
			prev->next_HeaderBox = NULL;
			free((*f));
			(*f) = prev;
		}
		//In the middle
		else{
			//Jump the link to actual one
			prev->next_HeaderBox= next;
			next->prev_HeaderBox= prev;

			//Remove the actual one
			free((*f));

			//Go to the previous one
			(*f) = prev;
		}
	}
}


int x7sDelBoxTrackOverlapping(X7sFeaturesBox *fbox1, X7sFeaturesBox *fbox2){

	//float centx_ccbox, centy_ccbox, centx_fbox, centy_fbox;
	float fbox1_min_x, fbox1_max_x, fbox1_min_y, fbox1_max_y, fbox1_area1, fbox1_area2; 
	float fbox2_min_x, fbox2_max_x, fbox2_min_y, fbox2_max_y, fbox2_area1, fbox2_area2; 


	if(fbox1->min_x <= fbox2->min_x && fbox2->min_x <= fbox1->max_x){
		if(fbox1->min_y <= fbox2->min_y && fbox2->min_y <= fbox1->max_y){
			fbox1_max_x = fbox2->min_x;
			fbox1_area1 = (fbox1_max_x - fbox1->min_x) * (fbox1->max_y - fbox1->min_y);
			fbox1_max_y = fbox2->min_y;
			fbox1_area2 = (fbox1->max_x - fbox1->min_x) * (fbox1_max_y - fbox1->min_y);

			fbox2_min_x = fbox1->max_x;
			fbox2_area1 = (fbox2->max_x - fbox2_min_x) * (fbox2->max_y - fbox2->min_y);
			fbox2_min_y = fbox1->max_y;
			fbox2_area2 = (fbox2->max_x - fbox2->min_x) * (fbox2->max_y - fbox2_min_y);

			if (fbox1_area1 > fbox1_area2)
				fbox1->max_x = fbox1_max_x;
			else
				fbox1->max_y = fbox1_max_y;

			if (fbox2_area1 > fbox2_area2)
				fbox2->min_x = fbox2_min_x;
			else
				fbox2->min_y = fbox2_min_y;

			return TRUE;
		}
	}
	if(fbox1->min_x <= fbox2->max_x && fbox2->max_x <= fbox1->max_x){
		if(fbox1->min_y <= fbox2->min_y && fbox2->min_y <= fbox1->max_y){
			fbox1_min_x = fbox2->max_x;
			fbox1_area1 = (fbox1->max_x - fbox1_min_x) * (fbox1->max_y - fbox1->min_y);
			fbox1_max_y = fbox2->min_y;
			fbox1_area2 = (fbox1->max_x - fbox1->min_x) * (fbox1_max_y - fbox1->min_y);

			fbox2_max_x = fbox1->min_x;
			fbox2_area1 = (fbox2_max_x - fbox2->min_x) * (fbox2->max_y - fbox2->min_y);
			fbox2_min_y = fbox1->max_y;
			fbox2_area2 = (fbox2->max_x - fbox2->min_x) * (fbox2->max_y - fbox2_min_y);

			if (fbox1_area1 > fbox1_area2)
				fbox1->min_x = fbox1_min_x;
			else
				fbox1->max_y = fbox1_max_y;

			if (fbox2_area1 > fbox2_area2)
				fbox2->max_x = fbox2_max_x;
			else
				fbox2->min_y = fbox2_min_y;

			return TRUE;
		}
	}

	if(fbox1->min_x <= fbox2->min_x && fbox2->min_x <= fbox1->max_x){
		if(fbox1->min_y <= fbox2->max_y && fbox2->max_y <= fbox1->max_y){
			fbox1_max_x = fbox2->min_x;
			fbox1_area1 = (fbox1_max_x - fbox1->min_x) * (fbox1->max_y - fbox1->min_y);
			fbox1_min_y = fbox2->max_y;
			fbox1_area2 = (fbox1->max_x - fbox1->min_x) * (fbox1->max_y - fbox1_min_y);

			fbox2_min_x = fbox1->max_x;
			fbox2_area1 = (fbox2->max_x - fbox2_min_x) * (fbox2->max_y - fbox2->min_y);
			fbox2_max_y = fbox1->min_y;
			fbox2_area2 = (fbox2->max_x - fbox2->min_x) * (fbox2_max_y - fbox2->min_y);

			if (fbox1_area1 > fbox1_area2)
				fbox1->max_x = fbox1_max_x;
			else
				fbox1->min_y = fbox1_min_y;

			if (fbox2_area1 > fbox2_area2)
				fbox2->min_x = fbox2_min_x;
			else
				fbox2->max_y = fbox2_max_y;

			return TRUE;
		}
	}

	if(fbox1->min_x <= fbox2->max_x && fbox2->max_x <= fbox1->max_x){
		if(fbox1->min_y <= fbox2->max_y && fbox2->max_y <= fbox1->max_y){
			fbox1_min_x = fbox2->max_x;
			fbox1_area1 = (fbox1->max_x - fbox1_min_x) * (fbox1->max_y - fbox1->min_y);
			fbox1_min_y = fbox2->max_y;
			fbox1_area2 = (fbox1->max_x - fbox1->min_x) * (fbox1->max_y - fbox1_min_y);

			fbox2_max_x = fbox1->min_x;
			fbox2_area1 = (fbox2_max_x - fbox2->min_x) * (fbox2->max_y - fbox2->min_y);
			fbox2_max_y = fbox1->min_y;
			fbox2_area2 = (fbox2->max_x - fbox2->min_x) * (fbox2_max_y - fbox2->min_y);

			if (fbox1_area1 > fbox1_area2)
				fbox1->min_x = fbox1_min_x;
			else
				fbox1->min_y = fbox1_min_y;

			if (fbox2_area1 > fbox2_area2)
				fbox2->max_x = fbox2_max_x;
			else
				fbox2->max_y = fbox2_max_y;

			return TRUE;
		}
	}



	//-------------------------

	if(fbox2->min_x <= fbox1->min_x && fbox1->min_x <= fbox2->max_x){
		if(fbox2->min_y <= fbox1->min_y && fbox1->min_y <= fbox2->max_y){
			fbox2_max_x = fbox1->min_x;
			fbox2_area2 = (fbox2_max_x - fbox2->min_x) * (fbox2->max_y - fbox2->min_y);
			fbox2_max_y = fbox1->min_y;
			fbox2_area1 = (fbox2->max_x - fbox2->min_x) * (fbox2_max_y - fbox2->min_y);

			fbox1_min_x = fbox2->max_x;
			fbox1_area2 = (fbox1->max_x - fbox1_min_x) * (fbox1->max_y - fbox1->min_y);
			fbox1_min_y = fbox2->max_y;
			fbox1_area1 = (fbox1->max_x - fbox1->min_x) * (fbox1->max_y - fbox1_min_y);

			if (fbox2_area2 > fbox2_area1)
				fbox2->max_x = fbox2_max_x;
			else
				fbox2->max_y = fbox2_max_y;

			if (fbox1_area2 > fbox1_area1)
				fbox1->min_x = fbox1_min_x;
			else
				fbox1->min_y = fbox1_min_y;

			return TRUE;
		}
	}
	if(fbox2->min_x <= fbox1->max_x && fbox1->max_x <= fbox2->max_x){
		if(fbox2->min_y <= fbox1->min_y && fbox1->min_y <= fbox2->max_y){
			fbox2_min_x = fbox1->max_x;
			fbox2_area2 = (fbox2->max_x - fbox2_min_x) * (fbox2->max_y - fbox2->min_y);
			fbox2_max_y = fbox1->min_y;
			fbox2_area1 = (fbox2->max_x - fbox2->min_x) * (fbox2_max_y - fbox2->min_y);

			fbox1_max_x = fbox2->min_x;
			fbox1_area2 = (fbox1_max_x - fbox1->min_x) * (fbox1->max_y - fbox1->min_y);
			fbox1_min_y = fbox2->max_y;
			fbox1_area1 = (fbox1->max_x - fbox1->min_x) * (fbox1->max_y - fbox1_min_y);

			if (fbox2_area2 > fbox2_area1)
				fbox2->min_x = fbox2_min_x;
			else
				fbox2->max_y = fbox2_max_y;

			if (fbox1_area2 > fbox1_area1)
				fbox1->max_x = fbox1_max_x;
			else
				fbox1->min_y = fbox1_min_y;

			return TRUE;
		}
	}

	if(fbox2->min_x <= fbox1->min_x && fbox1->min_x <= fbox2->max_x){
		if(fbox2->min_y <= fbox1->max_y && fbox1->max_y <= fbox2->max_y){
			fbox2_max_x = fbox1->min_x;
			fbox2_area2 = (fbox2_max_x - fbox2->min_x) * (fbox2->max_y - fbox2->min_y);
			fbox2_min_y = fbox1->max_y;
			fbox2_area1 = (fbox2->max_x - fbox2->min_x) * (fbox2->max_y - fbox2_min_y);

			fbox1_min_x = fbox2->max_x;
			fbox1_area2 = (fbox1->max_x - fbox1_min_x) * (fbox1->max_y - fbox1->min_y);
			fbox1_max_y = fbox2->min_y;
			fbox1_area1 = (fbox1->max_x - fbox1->min_x) * (fbox1_max_y - fbox1->min_y);

			if (fbox2_area2 > fbox2_area1)
				fbox2->max_x = fbox2_max_x;
			else
				fbox2->min_y = fbox2_min_y;

			if (fbox1_area2 > fbox1_area1)
				fbox1->min_x = fbox1_min_x;
			else
				fbox1->max_y = fbox1_max_y;

			return TRUE;
		}
	}

	if(fbox2->min_x <= fbox1->max_x && fbox1->max_x <= fbox2->max_x){
		if(fbox2->min_y <= fbox1->max_y && fbox1->max_y <= fbox2->max_y){
			fbox2_min_x = fbox1->max_x;
			fbox2_area2 = (fbox2->max_x - fbox2_min_x) * (fbox2->max_y - fbox2->min_y);
			fbox2_min_y = fbox1->max_y;
			fbox2_area1 = (fbox2->max_x - fbox2->min_x) * (fbox2->max_y - fbox2_min_y);

			fbox1_max_x = fbox2->min_x;
			fbox1_area2 = (fbox1_max_x - fbox1->min_x) * (fbox1->max_y - fbox1->min_y);
			fbox1_max_y = fbox2->min_y;
			fbox1_area1 = (fbox1->max_x - fbox1->min_x) * (fbox1_max_y - fbox1->min_y);

			if (fbox2_area2 > fbox2_area1)
				fbox2->min_x = fbox2_min_x;
			else
				fbox2->min_y = fbox2_min_y;

			if (fbox1_area2 > fbox1_area1)
				fbox1->max_x = fbox1_max_x;
			else
				fbox1->max_y = fbox1_max_y;

			return TRUE;
		}
	}

	/*

	centx_ccbox = ccbox->min_x + ((float)ccbox->max_x - (float)ccbox->min_x)/2.0;
	centy_ccbox = ccbox->min_y + ((float)ccbox->max_y - (float)ccbox->min_y)/2.0;
	centx_fbox = fbox->min_x + ((float)fbox->max_x - (float)fbox->min_x)/2.0;
	centy_fbox = fbox->min_y + ((float)fbox->max_y - (float)fbox->min_y)/2.0;


	if(ccbox->min_x <= centx_fbox && centx_fbox <= ccbox->max_x){
		if(ccbox->min_y <= centy_fbox && centy_fbox <= ccbox->max_y){
			return TRUE;
		}
	}

	if(fbox->min_x <= centx_ccbox && centx_ccbox <= fbox->max_x){
		if(fbox->min_y <= centy_ccbox && centy_ccbox <= fbox->max_y){
			return TRUE;
		}
	}
	 */

	return FALSE;

}



void x7sReleaseFeaturesList(X7sFeaturesList **ppFeatList) {

	X7sFeaturesList* pFeaturesList=*(ppFeatList);
	if(pFeaturesList)
	{
		if(pFeaturesList->pImg_clone) cvReleaseImage(&pFeaturesList->pImg_clone);
		if(pFeaturesList->eig) cvReleaseImage(&pFeaturesList->eig);
		if(pFeaturesList->grey) cvReleaseImage(&pFeaturesList->grey);
		if(pFeaturesList->prev_grey) cvReleaseImage(&pFeaturesList->prev_grey);
		if(pFeaturesList->temp) cvReleaseImage(&pFeaturesList->temp);
		if(pFeaturesList->mask_tracker) cvReleaseImage(&pFeaturesList->mask_tracker);
		if(pFeaturesList->pyramid) cvReleaseImage(&pFeaturesList->pyramid);
		if(pFeaturesList->prev_pyramid) cvReleaseImage(&pFeaturesList->prev_pyramid);

		if(pFeaturesList->rect_ini) cvReleaseImage(&pFeaturesList->rect_ini);
		if(pFeaturesList->rect_fin) cvReleaseImage(&pFeaturesList->rect_fin);
		if(pFeaturesList->yuv) cvReleaseImage(&pFeaturesList->yuv);
		if(pFeaturesList->mask_hist) cvReleaseImage(&pFeaturesList->mask_hist);
		if(pFeaturesList->pImg_diff) cvReleaseImage(&pFeaturesList->pImg_diff);
		if(pFeaturesList->pImg_diff2) cvReleaseImage(&pFeaturesList->pImg_diff2);

		if(pFeaturesList->backproject) cvReleaseImage(&pFeaturesList->backproject);
		if(pFeaturesList->backprojectGRAY) cvReleaseImage(&pFeaturesList->backprojectGRAY);
		if(pFeaturesList->back_image) cvReleaseImage(&pFeaturesList->back_image);

		if(pFeaturesList->hist_fin) cvReleaseHist(&pFeaturesList->hist_fin);

		if(pFeaturesList->y_plane_i) cvReleaseImage(&pFeaturesList->y_plane_i);
		if(pFeaturesList->u_plane_i) cvReleaseImage(&pFeaturesList->u_plane_i);
		if(pFeaturesList->v_plane_i) cvReleaseImage(&pFeaturesList->v_plane_i);

		if(pFeaturesList->y_plane_f) cvReleaseImage(&pFeaturesList->y_plane_f);
		if(pFeaturesList->u_plane_f) cvReleaseImage(&pFeaturesList->u_plane_f);
		if(pFeaturesList->v_plane_f) cvReleaseImage(&pFeaturesList->v_plane_f);

		if(pFeaturesList->points[0]) cvFree(&pFeaturesList->points[0]);
		if(pFeaturesList->points[1]) cvFree(&pFeaturesList->points[1]);
		if(pFeaturesList->points_IDbox) free(pFeaturesList->points_IDbox);
		if(pFeaturesList->lost_points) free(pFeaturesList->lost_points);
		if(pFeaturesList->valid_ID) free(pFeaturesList->valid_ID);
		if(pFeaturesList->points_difx) free(pFeaturesList->points_difx);
		if(pFeaturesList->points_dify) free(pFeaturesList->points_dify);

		if(pFeaturesList->status) cvFree(&pFeaturesList->status);

		free(pFeaturesList->vec);
		free(pFeaturesList);

		X7S_PRINT_DEBUG("x7sReleaseFeaturesList()","");

	}

	//Set the pointer to NULL.
	(*ppFeatList)=NULL;
}


