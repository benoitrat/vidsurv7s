#include "x7scv.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Defines/Enums/Includes
//----------------------------------------------------------------------------------------
#include "x7sCvLog.h"

#define START_X 0
#define START_Y 0
#define NOF_X 5
#define NOF_Y 15


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Private Prototypes
//----------------------------------------------------------------------------------------
void fprint_8U (FILE *stream, const void *pva);
void fprint_16U(FILE *stream, const void *pva);
void fprint_16S(FILE *stream, const void *pva);
void fprint_32S(FILE *stream, const void *pva);
void fprint_32F(FILE *stream, const void *pva);
void fprint_64F(FILE *stream, const void *pva);
void x7sIplPrintValues_A(const IplImage *img, int start_x, int start_y, int nof_x, int nof_y,void (*fct)(FILE *stream, const void *));
char *getIplDepthCode2Str(const int depth);
int getIplDepthCode2ByteSize(const int depth);



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public functions
//----------------------------------------------------------------------------------------

double x7sLog2(double val)
{
	return log(val)/log(2);
}




/**
* @brief Similar to @cv{cvReadIntByName} function except that this function return the
* default value if a negative value (i.e:-1) has been written in the XML file.
*/
uint32_t x7sReadUIntByName(const CvFileStorage* fs, const CvFileNode* map,
		const char* name, uint32_t default_value) {
	int tmp;
	tmp=cvReadIntByName(fs,map,name,default_value);
	if(tmp < 0) return default_value;
	return (uint32_t)tmp;
}

/**
* @brief Print important info of an image (width,height,channels,depth_code,...)
* @param img A pointer in an image.
*/
void x7sIplPrintInfos(const IplImage *img) {

	int height,width,channels;
	char *depth_code;

	// get the image data
	height    = img->height;
	width     = img->width;
	channels  = img->nChannels;
	depth_code	= getIplDepthCode2Str(img->depth);

	printf("Processing a %dx%d image with %d channels (%s)\n",width,height,channels,depth_code);
	if(img->dataOrder == 1) printf("separate color channels\n");
	else printf("interleaved color channels\n");
	if(img->origin == 1) printf("WARNING : bottom-left origin (Windows bitmaps style)\n");


}

/**
* @brief Print some values of an image (at the beggining).
* @param img A pointer in an image.
* @see  x7sIplPrintValuesFull().
*/
void x7sIplPrintValues(const IplImage *img) {
	x7sIplPrintValuesFull(img,START_X,START_Y,NOF_X,NOF_Y);
}

/**
* @brief Print a desired part of an image.
* @param img A pointer in an image.
* @param start_x The start poisition in X.
* @param start_y The start position in Y.
* @param nof_x	The number of pixels to display in the x-axis.
* @param nof_y The number of pixels to display in the y-axis.
*/
void x7sIplPrintValuesFull(const IplImage *img, int start_x, int start_y, int nof_x, int nof_y) {

	switch(img->depth) {
	case IPL_DEPTH_8U:
		x7sIplPrintValues_A(img,start_x,start_y,nof_x,nof_y,fprint_8U);
		break;
	case IPL_DEPTH_16U:
		x7sIplPrintValues_A(img,start_x,start_y,nof_x,nof_y,fprint_16U);
		break;
	case IPL_DEPTH_16S:
		x7sIplPrintValues_A(img,start_x,start_y,nof_x,nof_y,fprint_16S);
		break;
	case IPL_DEPTH_32S:
		x7sIplPrintValues_A(img,start_x,start_y,nof_x,nof_y,fprint_32S);
		break;
	case IPL_DEPTH_32F:
		x7sIplPrintValues_A(img,start_x,start_y,nof_x,nof_y,fprint_32F);
		break;
	case IPL_DEPTH_64F:
		x7sIplPrintValues_A(img,start_x,start_y,nof_x,nof_y,fprint_64F);
		break;
	default:
		printf("Function is nor implemented for this type of data\n");
	}

}


/**
* @brief Print a matrix on the stdout.
* @param A
*/
void x7sPrintMat(CvMat *A)
{
	int i, j;
	for (i = 0; i < A->rows; i++)
	{
		printf("\n");
		switch (CV_MAT_DEPTH(A->type))
		{
		case CV_32F:
		case CV_64F:
			for (j = 0; j < A->cols; j++)
				printf ("%8.3f ", (float)cvGetReal2D(A, i, j));
			break;
		case CV_8S:
		case CV_8U:
		case CV_16U:
		case CV_32S:
			for(j = 0; j < A->cols; j++)
				printf ("%6d",(int)cvGetReal2D(A, i, j));
			break;
		default:
			break;
		}
	}
	printf("\n");
}



int x7sPatternImage(IplImage **pDst, int type)
{

	X7S_FUNCNAME("x7sPatternImage()");
	CvSize size;
	uint8_t *pLine, val;
	int x,y;

	if(pDst && pDst[0]) size=cvGetSize(pDst[0]);
	else size=cvSize(256,256);


	if(type<0xF0)
	{
		x7sReallocImage(size,IPL_DEPTH_8U,1,pDst);
		pLine = (uint8_t*)pDst[0]->imageData;
		val=0;

		if(type==X7S_CV_GRADIENT_X)
		{
			for(y=0;y<pDst[0]->height;y++)
			{
				for(x=0;x<pDst[0]->width;x++)
				{
					pLine[x]=val++;
				}
				pLine+=pDst[0]->widthStep;
				val=0; //Reset value for next line
			}
			return X7S_RET_OK;
		}
		else if(type==X7S_CV_GRADIENT_Y)
		{
			for(y=0;y<pDst[0]->height;y++)
			{
				for(x=0;x<pDst[0]->width;x++)
				{
					pLine[x]=val;
				}
				pLine+=pDst[0]->widthStep;
				val++; //Increment value for next line
			}
			return X7S_RET_OK;
		}
		return X7S_RET_OK;
	}


	X7S_PRINT_WARN(funcName,"type of pattern %d is not defined",type);
	return X7S_RET_OK;


}




//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Private functions.
//----------------------------------------------------------------------------------------

void x7sIplPrintValues_A(const IplImage *img, int start_x, int start_y, int nof_x, int nof_y,void (*fct)(FILE *stream, const void *)) {

	uint8_t *line, *pixel;
	int nofB = getIplDepthCode2ByteSize(img->depth);
	int widthStep = img->widthStep/nofB;
	int x,y,end_y, end_x;
	int nofC,c;

	nofC = img->nChannels;
	end_x = MIN(start_x+nof_x,img->width);
	end_y = MIN(start_y+nof_y,img->height);




	//Multiband channels
	if(nofC > 1 && img->dataOrder  ==1 )  {


	}
	else {
		for(y=start_y;y<end_y;y++) {
			line = (uint8_t*)img->imageDataOrigin + y*widthStep;
			for(x=start_x;x<end_x;x++) {
				printf("[%d,%d]=(",x,y);
				pixel = line+ x*nofC*nofB;
				for(c=0;c<nofC;c++) {
					if(c!=0) printf(",");
					//printf("%p",pixel + c*nofB);
					fct(stdout,pixel + c*nofB);
				}
				printf(") ");
			}
			printf("\n"); //End line
		}
	}
}

void fprint_8U(FILE *stream, const void *pva)
{
	const uchar *pa = pva;
	printf("%d",*pa);
}

void fprint_16U(FILE *stream, const void *pva)
{
	const ushort *pa = pva;
	printf("%d",*pa);
}

void fprint_16S(FILE *stream, const void *pva)
{
	const short *pa = pva;
	printf("%d",*pa);
}

void fprint_32S(FILE *stream, const void *pva)
{
	const int *pa = pva;
	printf("%d",*pa);
}

void fprint_32F(FILE *stream, const void *pva)
{
	const float *pa = pva;
	printf("%f",*pa);
}

void fprint_64F(FILE *stream, const void *pva)
{
	const double *pa = pva;
	printf("%f",*pa);
}


char *getIplDepthCode2Str(const int depth) {
	switch(depth) {
	case IPL_DEPTH_8U: 	return "IPL_DEPTH_8U";
	case IPL_DEPTH_8S:	return "IPL_DEPTH_8S";
	case IPL_DEPTH_16U:	return "IPL_DEPTH_16U";
	case IPL_DEPTH_16S:	return "IPL_DEPTH_16S";
	case IPL_DEPTH_32S:	return "IPL_DEPTH_32S";
	case IPL_DEPTH_32F:	return "IPL_DEPTH_32F";
	case IPL_DEPTH_64F:	return "IPL_DEPTH_64F";
	default: 			return "IPL_CODE_???";
	}
}


int getIplDepthCode2ByteSize(const int depth) {
	switch(depth) {
	case IPL_DEPTH_8U: 	return sizeof(uchar);
	case IPL_DEPTH_8S:	return sizeof(char);
	case IPL_DEPTH_16U:	return sizeof(ushort);
	case IPL_DEPTH_16S:	return sizeof(short);
	case IPL_DEPTH_32S:	return sizeof(int);
	case IPL_DEPTH_32F:	return sizeof(float);
	case IPL_DEPTH_64F:	return sizeof(double);
	default: 			return 0;
	}
}

