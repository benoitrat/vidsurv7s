#include "x7scv.h"
#include "x7sCvLog.h"

#define ICV7_TRUEPIX 255

#include "cxcore.h"
#include "cv.h"
#include "highgui.h"

//------------------ Private prototypes.

int x7sPseudoMed_1C8U(const IplImage *src, IplImage *dst, int n_pix);
int x7sPixelate_8U(const IplImage *src, IplImage *dst, IplImage *mask, int pixw, int pixh);

//------------------ Public Interfaces (Check arguments)

/**
* @brief Performs a Pseudo Median filtering of an image.
*
* The pdeuso median filtering, set a pixel to @ref X7S_CV_TRUEPIX,
* if there is at least n_pixel > 0 in a 3x3 neighbourhood.
*
* Here follow an example with n_pixels=6
* @code
*
*  //source image
* ------------------------------
* |............................|
* |......xx.xx...xxxx.x...x....|
* |......x.xxx...x.x.x...xx....|
* |.......xx......x.x.x.xxxx...|
* |............................|
* ------------------------------
*
* //destination image
* -----------------------------
* |............................|
* |............................|
* |.......x.x.....xx.....xx....|
* |............................|
* |............................|
* -----------------------------
* @endcode
*
* This algorithm remove 2 pixels on all the border.
* @note This algorithm can works in place (on the same image)
* @param src the source image.
* @param dst the destination image (must be same size, channels and depth than the source)
* @param n_pix	The number of pixel that need to be >0 to set the current pixel to @ref X7S_CV_TRUEPIX.
* @return @ref X7S_CV_RET_OK if no error occurs, otherwise it return @ref X7S_CV_RET_ERR.
*/
int x7sPseudoMed(const IplImage *src, IplImage *dst, int n_pix) {

	CV_FUNCNAME("x7sPseudoMed()");

	if(X7S_ACHECK_IMG8U(src,dst,1,1)) {
		return x7sPseudoMed_1C8U(src,dst,n_pix);
	}
	else {
		X7S_PRINT_ERROR(cvFuncName,"Images are not compatible");
		return X7S_CV_RET_ERR;
	}
}

/**
* @brief Pixelate an image with blocks of size pixw x pixh.
* @param src The source image.
* @param dst the destination image (same size, channels and depth than source)
* @param mask The mask image to know which pixels are going to be pixelate (same size, depth than source, 1 channel).
* @param pixw The width of the block
* @param pixh The heigh of the block.
* @return @ref X7S_CV_RET_OK if no error occurs, otherwise it return @ref X7S_CV_RET_ERR.
*/
int x7sPixelate(const IplImage *src, IplImage *dst, IplImage *mask, int pixw, int pixh)
{
	CV_FUNCNAME("x7sPseudoMed()");

	if(X7S_ARE_IMEQ(src,dst) && X7S_IS_IMAGENC(mask,1)) {
		return x7sPixelate_8U(src,dst,mask,pixw,pixh);
	}
	else {
		X7S_PRINT_ERROR(cvFuncName,"Images are not compatible");
		return X7S_CV_RET_ERR;
	}
}


//------------------ Private function

/**
* @brief @ref x7sPseudoMed() function for 1 channel uint8 depth image.
* @see x7sPseudoMed().
*/
int x7sPseudoMed_1C8U(const IplImage *src, IplImage *dst, int n_pix) {

	int r=0, c=0, width=-1, height=-1, sum=0;
	uint8_t *src_lprev, *src_lactu, *src_lnext;
	uint8_t *dst_lactu;
	uint8_t sum_cprev=0,sum_cactu=0,sum_cnext=0;
	uint8_t sum_thrs=7;
	if(0 < n_pix) sum_thrs=(uint8_t)(n_pix<9?n_pix:9);


	//Reserve memory for two lines
	src_lprev=malloc(sizeof(uint8_t)*src->width);
	src_lactu=malloc(sizeof(uint8_t)*src->width);

	//Remove one pixel on bottom and right borders.
	height=src->height-1;
	width=src->width-1;

	//Then browse on each line -2
	for(r=1;r<height;r++) {

		//Copy actual and previous line of src images.
		memcpy(src_lprev,src->imageData+((r-1)* src->widthStep),src->widthStep);
		memcpy(src_lactu,src->imageData+(  r  * src->widthStep),src->widthStep);

		//Set pointer on the next line of src images
		src_lnext=(uint8_t*)src->imageData+((r+1)* src->widthStep);

		//Set pointer on the actual line of dst images...
		dst_lactu=(uint8_t*)dst->imageData+(  r * dst->widthStep);

		//... and set the first pixel as black (border)
		dst_lactu[0]=0;


		//Find the sum of the 3 first columns.
		sum_cprev=(src_lprev[0]>0)+(src_lactu[0]>0)+(src_lnext[0]>0);
		sum_cactu=(src_lprev[1]>0)+(src_lactu[1]>0)+(src_lnext[1]>0);
		sum_cnext=(src_lprev[2]>0)+(src_lactu[2]>0)+(src_lnext[2]>0);



		//Loop on the others column.
		for(c=1;c<width;c++) {

			//count how many neighbour are white.
			sum=sum_cprev+sum_cactu+sum_cnext;
			if(sum >= sum_thrs) dst_lactu[c]=X7S_CV_TRUEPIX;
			else  dst_lactu[c]=X7S_CV_FALSEPIX;

			//Shift sum of each column and compute the next one.
			sum_cprev=sum_cactu;
			sum_cactu=sum_cnext;
			if(c < (width-1)) sum_cnext=(src_lprev[c+2]>0)+(src_lactu[c+2]>0)+(src_lnext[c+2]>0);

		}
	}

	//Free the memory of temporary lines
	free(src_lprev);
	src_lprev=NULL;
	free(src_lactu);
	src_lactu=NULL;


	return X7S_CV_RET_OK;
}



int x7sPixelate_8U(const IplImage *src, IplImage *dst, IplImage *mask, int pixw, int pixh) {

	IplImage* imSmall;
	int next_r, next_c;
	uint8_t *p_src, *p_dst, *p_mask, *p_pix, *p_pixline;
	int r=0, c=0;
	int small_w=src->width/pixw+1;
	int small_h=src->height/pixh+1;

	//First resize the small image
	imSmall = cvCreateImage(cvSize(small_w,small_h),IPL_DEPTH_8U,src->nChannels);
	cvResize(src,imSmall,CV_INTER_NN);


	p_src=(uint8_t*)src->imageData;
	p_dst=(uint8_t*)dst->imageData;
	p_mask=(uint8_t*)mask->imageData;
	p_pix=(uint8_t*)imSmall->imageData;
	p_pixline=p_pix;


	//Loop each line (row)
	while(r<src->height)
	{


		//Make a for loop on the pixh (height of the pixelate structure)
		for(next_r=X7S_MIN((r+pixh),src->height);r<next_r;r++)
		{
			//Loop each colum (row)
			c=0;
			p_pix=p_pixline;

			while(c<src->width)
			{
				//printf("%x [%3d,%3d,%3d], ",p_pix,p_pix[0],p_pix[1],p_pix[2]);
				//Make a for loop on the pixw (width of the pixelate structure)
				for(next_c=X7S_MIN((c+pixw),src->width);c<next_c;c++)
				{
					//If mask is true take pixelate value
					if(*p_mask)
					{
						memcpy(p_dst,p_pix,dst->nChannels);
					}
					//Otherwise copy from source value
					else
					{
						//Copy the destination channel
						memcpy(p_dst,p_src,dst->nChannels);
					}

					//Jump to the next position.
					p_mask++;
					p_dst+=dst->nChannels;
					p_src+=src->nChannels;
				}
				//Jump to the next position on image structure
				p_pix+=imSmall->nChannels;
			}
			//printf("--\n");
		}
		p_pixline+=imSmall->widthStep;
	}

	cvReleaseImage(&imSmall);
	return X7S_RET_OK;
}
