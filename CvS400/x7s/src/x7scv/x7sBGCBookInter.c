#include "x7scv.h"
#include "cvaux.h"
#include "cxmisc.h"
#include "cxtypes.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

//================================================================================
//-- Internal includes to fasten compilation
#include "x7sCvLog.h"


static uchar vecTab8u[768];
#undef VEC_8U
#define VEC_8U(x) vecTab8u[(x) + 255]

//================================================================================
//-- Private Static Prototypes
static int x7sUpdateCBookInterBGModel(IplImage *curr_frame, X7sCBookInterBGStatModel* bg_model);
static void x7sReleaseCBookInterBGModel(X7sCBookInterBGStatModel ** p_bg_model);

//================================================================================
//-- Private Prototypes
void x7sBGCodeBookConstruction(X7sCBookInterBGStatModel* model, const CvArr* _image);
void x7sBGCodeBookUpdate(X7sCBookInterBGStatModel* model, const CvArr* _image);
void x7sBGCodeBookDiff( const X7sCBookInterBGStatModel* model, const CvArr* _image, CvArr* _fgmask);


static void X7SInitVecTab()
{
	static int initialized = 0;
	int i;
    if( !initialized )
    {
        for( i = 0; i < 768; i++ )
        {
            int v = i - 255;
            vecTab8u[i] = (uchar)(v < 0 ? 0 : v > 255 ? 255 : v);
        }
        initialized = 1;
    }
}

//================================================================================


/**
* @brief Set the default value for parameters of
*  Codebook Intervals background model.
*/
int x7sSetCBookInterBGStatBGModelParams(X7sCBookInterBGStatModelParams *params)
{
	params->cbBounds[0] = params->cbBounds[1] = params->cbBounds[2] = 8;
	params->modMin[0] = params->modMin[1] = params->modMin[2] = 10;
	params->modMax[0] =	params->modMax[1] = params->modMax[2] = 10;

	params->window = 140;
	params->num_codewords_elements = 4;
	params->nframesToLearnBG = 40;


	return X7S_CV_RET_OK;
}


/*********************************************************************************/
/*********************************************************************************/


CvBGStatModel* x7sCreateCBookInterBGModel(IplImage *first_frame ,X7sCBookInterBGStatModelParams *parameters)
{
	int i;
	X7sCBookInterBGStatModel* bg_model;

	CV_FUNCNAME( "x7sCreateClusterBGModel()" );

	//Check input variable.
	if(!CV_IS_IMAGE(first_frame)) {
		X7S_PRINT_ERROR(cvFuncName,"first_frame == NULL");
		return NULL;
	}

	//Reserved memory for structure.
	bg_model = (X7sCBookInterBGStatModel*) malloc( sizeof(X7sCBookInterBGStatModel) );

	bg_model->type = X7S_BG_MODEL_CBK_INTER;
	bg_model->release = (CvReleaseBGStatModel)x7sReleaseCBookInterBGModel;
	bg_model->update = (CvUpdateBGStatModel)x7sUpdateCBookInterBGModel;
	bg_model->nframes = 0;
	bg_model->width = first_frame->width; 
	bg_model->height = first_frame->height;

	if(parameters == NULL)
		x7sSetCBookInterBGStatBGModelParams(&bg_model->params);
	else
		bg_model->params=*parameters;


	
	//Then reserved the memory for the generic background.
	bg_model->background = cvCreateImage(cvGetSize(first_frame),IPL_DEPTH_8U,first_frame->nChannels);
	bg_model->foreground = cvCreateImage(cvGetSize(first_frame),IPL_DEPTH_8U,1);
	bg_model->storage = cvCreateMemStorage(0);
	bg_model->layer_count=0;

	//Then reserved the memory for the temporary image.
	//bg_model->imPrev = cvCreateImage(cvGetSize(first_frame),first_frame->depth,first_frame->nChannels);
	//bg_model->imK = cvCreateImage(cvGetSize(first_frame),first_frame->depth,first_frame->nChannels);

	//Reset the previous image with 255 (Far from cluster initialization).
	//cvSet(bg_model->imPrev,cvScalar(255,255,255,255),NULL);

	bg_model->cbmap = (X7SBGCodeBookElem**) malloc(first_frame->width*first_frame->height*sizeof(X7SBGCodeBookElem *));

	for(i=0; i<first_frame->width*first_frame->height; i++){
		bg_model->cbmap[i] = (X7SBGCodeBookElem*) malloc(bg_model->params.num_codewords_elements*sizeof(X7SBGCodeBookElem));
		memset( bg_model->cbmap[i], 0, 4*sizeof(X7SBGCodeBookElem) );
	}


	return (CvBGStatModel*)bg_model;
}


/*********************************************************************************/

int x7sUpdateCBookInterBGModel(IplImage *curr_frame, X7sCBookInterBGStatModel* bg_model) {


	if( bg_model->nframes-1 < bg_model->params.nframesToLearnBG ){
        x7sBGCodeBookConstruction(bg_model, curr_frame);
		bg_model->nframes++;
	}

	else{
        x7sBGCodeBookUpdate( bg_model, curr_frame );
        // Find foreground by codebook method
		x7sBGCodeBookDiff( bg_model, curr_frame, bg_model->foreground);
    }



	return X7S_CV_RET_OK;

}
/*********************************************************************************/



void x7sReleaseCBookInterBGModel(X7sCBookInterBGStatModel ** p_bg_model){
	int i;
	//Declaration of the variable
	X7sCBookInterBGStatModel* bg_model;

	//Checking arguments
	CV_FUNCNAME( "ix7sReleaseClusterBGModel()" );
	if( !p_bg_model ) X7S_PRINT_ERROR(cvFuncName,"Address of pointer is not valid");
	bg_model = (X7sCBookInterBGStatModel *)(*p_bg_model);
	if(!bg_model) X7S_PRINT_ERROR(cvFuncName,"Pointer is NULL");

	//Remove generic memory
	cvReleaseImage(&(bg_model->foreground));
	cvReleaseMemStorage(&bg_model->storage);

	//Remove specific memory
	//cvReleaseImage(&(bg_model->imPrev));
	//cvReleaseImage(&(bg_model->imK));
	for(i=0; i< bg_model->width*bg_model->height; i++)
		free(bg_model->cbmap[i]);
	free(bg_model);


	//Free the structure memory and set the pointed value unusable.
	(*p_bg_model)=NULL;


}

/*********************************************************************************/


void x7sBGCodeBookConstruction(X7sCBookInterBGStatModel* model, const CvArr* _image){


    CvMat stub, *image;
    int x, y, e, pos_codebook;
    uchar cb0, cb1, cb2;

    X7SInitVecTab();
    image = cvGetMat( _image, &stub, 0, 0);

	cb0 = model->params.cbBounds[0];
    cb1 = model->params.cbBounds[1];
    cb2 = model->params.cbBounds[2];



    for( y = 0; y < image->rows; y++ )
    {
        const uchar* p = image->data.ptr + image->step*y;

        for( x = 0; x < image->cols; x++, p += 3 )
        {
			int found = 0, last = 0;
            uchar p0, p1, p2, l0, l1, l2, h0, h1, h2;

			pos_codebook = y*image->cols + x;


            p0 = p[0]; p1 = p[1]; p2 = p[2];
            l0 = VEC_8U(p0 - cb0); l1 = VEC_8U(p1 - cb1); l2 = VEC_8U(p2 - cb2);
            h0 = VEC_8U(p0 + cb0); h1 = VEC_8U(p1 + cb1); h2 = VEC_8U(p2 + cb2);

	
			found = -1;

			for( e = 0; e < model->params.num_codewords_elements; e++ )
            {
                if( model->cbmap[pos_codebook][e].learnMin[0] <= p0 && p0 <= model->cbmap[pos_codebook][e].learnMax[0] &&
                    model->cbmap[pos_codebook][e].learnMin[1] <= p1 && p1 <= model->cbmap[pos_codebook][e].learnMax[1] &&
                    model->cbmap[pos_codebook][e].learnMin[2] <= p2 && p2 <= model->cbmap[pos_codebook][e].learnMax[2] && 
					found == -1)
                {
                    model->cbmap[pos_codebook][e].boxMin[0] = MIN(model->cbmap[pos_codebook][e].boxMin[0], p0);
                    model->cbmap[pos_codebook][e].boxMax[0] = MAX(model->cbmap[pos_codebook][e].boxMax[0], p0);
                    model->cbmap[pos_codebook][e].boxMin[1] = MIN(model->cbmap[pos_codebook][e].boxMin[1], p1);
                    model->cbmap[pos_codebook][e].boxMax[1] = MAX(model->cbmap[pos_codebook][e].boxMax[1], p1);
                    model->cbmap[pos_codebook][e].boxMin[2] = MIN(model->cbmap[pos_codebook][e].boxMin[2], p2);
                    model->cbmap[pos_codebook][e].boxMax[2] = MAX(model->cbmap[pos_codebook][e].boxMax[2], p2);

                    // no need to use SAT_8U for updated learnMin[i] & learnMax[i] here,
                    // as the bounding li & hi are already within 0..255.
                    if( model->cbmap[pos_codebook][e].learnMin[0] > l0 ) model->cbmap[pos_codebook][e].learnMin[0]--;
                    if( model->cbmap[pos_codebook][e].learnMax[0] < h0 ) model->cbmap[pos_codebook][e].learnMax[0]++;
                    if( model->cbmap[pos_codebook][e].learnMin[1] > l1 ) model->cbmap[pos_codebook][e].learnMin[1]--;
                    if( model->cbmap[pos_codebook][e].learnMax[1] < h1 ) model->cbmap[pos_codebook][e].learnMax[1]++;
                    if( model->cbmap[pos_codebook][e].learnMin[2] > l2 ) model->cbmap[pos_codebook][e].learnMin[2]--;
                    if( model->cbmap[pos_codebook][e].learnMax[2] < h2 ) model->cbmap[pos_codebook][e].learnMax[2]++;

                    found = e;
					if (model->cbmap[pos_codebook][e].p < 255)
						model->cbmap[pos_codebook][e].p = model->cbmap[pos_codebook][e].p + 1;
					model->cbmap[pos_codebook][e].q = model->cbmap[pos_codebook][e].q = 1; 
					model->cbmap[pos_codebook][e].weight = (model->cbmap[pos_codebook][e].weight*(model->params.window - 1) + 1)/model->params.window;
					model->cbmap[pos_codebook][e].mean[0] = (model->cbmap[pos_codebook][e].mean[0]*(model->params.window - 1) + p0)/model->params.window;
					model->cbmap[pos_codebook][e].mean[1] = (model->cbmap[pos_codebook][e].mean[1]*(model->params.window - 1) + p1)/model->params.window;
					model->cbmap[pos_codebook][e].mean[2] = (model->cbmap[pos_codebook][e].mean[2]*(model->params.window - 1) + p2)/model->params.window;
					//break;
                }
				else{
					if (model->cbmap[pos_codebook][e].f == 1){
						if (model->cbmap[pos_codebook][e].p < 255)
							model->cbmap[pos_codebook][e].p = model->cbmap[pos_codebook][e].p + 1;
						if (model->cbmap[pos_codebook][e].q < 255)
							model->cbmap[pos_codebook][e].q = model->cbmap[pos_codebook][e].q + 1;
						model->cbmap[pos_codebook][e].weight = model->cbmap[pos_codebook][e].weight*(model->params.window - 1)/model->params.window;
					}
				}
			}


            if( found == -1){
				last = -1;
				for( e = 0; e < model->params.num_codewords_elements; e++ ){
					if (model->cbmap[pos_codebook][e].f != 0){
						last = e;
					}
				}
				if (last < model->params.num_codewords_elements - 1 ){
					last++;
					model->cbmap[pos_codebook][last].f = 1;
					model->cbmap[pos_codebook][last].p = 50;
					model->cbmap[pos_codebook][last].q = 1;
					model->cbmap[pos_codebook][last].weight = 1/(float)model->params.window;

					model->cbmap[pos_codebook][last].learnMin[0] = l0; model->cbmap[pos_codebook][last].learnMax[0] = h0;
					model->cbmap[pos_codebook][last].learnMin[1] = l1; model->cbmap[pos_codebook][last].learnMax[1] = h1;
					model->cbmap[pos_codebook][last].learnMin[2] = l2; model->cbmap[pos_codebook][last].learnMax[2] = h2;
					model->cbmap[pos_codebook][last].boxMin[0] = model->cbmap[pos_codebook][last].boxMax[0] = p0;
					model->cbmap[pos_codebook][last].boxMin[1] = model->cbmap[pos_codebook][last].boxMax[1] = p1;
					model->cbmap[pos_codebook][last].boxMin[2] = model->cbmap[pos_codebook][last].boxMax[2] = p2;
					model->cbmap[pos_codebook][last].mean[0] = (model->cbmap[pos_codebook][last].mean[0]*(model->params.window - 1) + p0)/model->params.window;
					model->cbmap[pos_codebook][last].mean[1] = (model->cbmap[pos_codebook][last].mean[1]*(model->params.window - 1) + p1)/model->params.window;
					model->cbmap[pos_codebook][last].mean[2] = (model->cbmap[pos_codebook][last].mean[2]*(model->params.window - 1) + p2)/model->params.window;
				}
            }
        }
    }

}



/*********************************************************************************/
/*********************************************************************************/


void x7sBGCodeBookUpdate(X7sCBookInterBGStatModel* model, const CvArr* _image){


    CvMat stub, *image = cvGetMat(_image, &stub, 0, 0);
    int i, x, y, e, pos_codebook, T, pos_interest;
    int nblocks;
    int cb0, cb1, cb2;
	float min_weight;


    X7SInitVecTab();

    cb0 = model->params.cbBounds[0];
    cb1 = model->params.cbBounds[1];
    cb2 = model->params.cbBounds[2];

	pos_interest = 72*image->cols + 85;

    for( y = 0; y < image->rows; y++ )
    {
        const uchar* p = image->data.ptr + image->step*y;

        for( x = 0; x < image->cols; x++, p += 3 )
        {
			int found = 0, last = 0, pos_min_weight = 0, num_codewords;
            uchar p0, p1, p2, l0, l1, l2, h0, h1, h2;
            int negRun;

			pos_codebook = y*image->cols + x;


            p0 = p[0]; p1 = p[1]; p2 = p[2];
            l0 = VEC_8U(p0 - cb0); l1 = VEC_8U(p1 - cb1); l2 = VEC_8U(p2 - cb2);
            h0 = VEC_8U(p0 + cb0); h1 = VEC_8U(p1 + cb1); h2 = VEC_8U(p2 + cb2);

			/*if(pos_interest == pos_codebook){
				printf("p0 = %d; p1 = %d; p2 = %d\n", p0, p1, p2);
				printf("LH0 [%d, %d] ; LH2 [%d, %d] ; LH2 [%d, %d] \n", l0, h0, l1, h1, l2, h2);
			}*/

	
			found = -1;

            for( e = 0; e < model->params.num_codewords_elements; e++ )
            {
				/*if(pos_interest == pos_codebook){
					printf("CodeWord %d\n", e);
				} */       
				if( model->cbmap[pos_codebook][e].learnMin[0] <= p0 && p0 <= model->cbmap[pos_codebook][e].learnMax[0] &&
                    model->cbmap[pos_codebook][e].learnMin[1] <= p1 && p1 <= model->cbmap[pos_codebook][e].learnMax[1] &&
                    model->cbmap[pos_codebook][e].learnMin[2] <= p2 && p2 <= model->cbmap[pos_codebook][e].learnMax[2] && 
					found == -1 && model->cbmap[pos_codebook][e].f == 1)
                {
                    model->cbmap[pos_codebook][e].boxMin[0] = MIN(model->cbmap[pos_codebook][e].boxMin[0], p0);
                    model->cbmap[pos_codebook][e].boxMax[0] = MAX(model->cbmap[pos_codebook][e].boxMax[0], p0);
                    model->cbmap[pos_codebook][e].boxMin[1] = MIN(model->cbmap[pos_codebook][e].boxMin[1], p1);
                    model->cbmap[pos_codebook][e].boxMax[1] = MAX(model->cbmap[pos_codebook][e].boxMax[1], p1);
                    model->cbmap[pos_codebook][e].boxMin[2] = MIN(model->cbmap[pos_codebook][e].boxMin[2], p2);
                    model->cbmap[pos_codebook][e].boxMax[2] = MAX(model->cbmap[pos_codebook][e].boxMax[2], p2);

                    // no need to use SAT_8U for updated learnMin[i] & learnMax[i] here,
                    // as the bounding li & hi are already within 0..255.
                    if( model->cbmap[pos_codebook][e].learnMin[0] > l0 ) model->cbmap[pos_codebook][e].learnMin[0]--;
                    if( model->cbmap[pos_codebook][e].learnMax[0] < h0 ) model->cbmap[pos_codebook][e].learnMax[0]++;
                    if( model->cbmap[pos_codebook][e].learnMin[1] > l1 ) model->cbmap[pos_codebook][e].learnMin[1]--;
                    if( model->cbmap[pos_codebook][e].learnMax[1] < h1 ) model->cbmap[pos_codebook][e].learnMax[1]++;
                    if( model->cbmap[pos_codebook][e].learnMin[2] > l2 ) model->cbmap[pos_codebook][e].learnMin[2]--;
                    if( model->cbmap[pos_codebook][e].learnMax[2] < h2 ) model->cbmap[pos_codebook][e].learnMax[2]++;

                    found = e;
					if (model->cbmap[pos_codebook][e].p < 255)
						model->cbmap[pos_codebook][e].p = model->cbmap[pos_codebook][e].p + 1;
					model->cbmap[pos_codebook][e].q = model->cbmap[pos_codebook][e].q = 1;
					model->cbmap[pos_codebook][e].weight = (model->cbmap[pos_codebook][e].weight*(model->params.window - 1) + 1)/model->params.window;
					model->cbmap[pos_codebook][e].mean[0] = (model->cbmap[pos_codebook][e].mean[0]*(model->params.window - 1) + p0)/model->params.window;
					model->cbmap[pos_codebook][e].mean[1] = (model->cbmap[pos_codebook][e].mean[1]*(model->params.window - 1) + p1)/model->params.window;
					model->cbmap[pos_codebook][e].mean[2] = (model->cbmap[pos_codebook][e].mean[2]*(model->params.window - 1) + p2)/model->params.window;
                    //break;
					/*if(pos_interest == pos_codebook){
						printf("CodeWord SI apto:\n");
					}*/
				}
				else{
					/*if(pos_interest == pos_codebook){
						printf("CodeWord NO apto:\n");
					}*/
					if (model->cbmap[pos_codebook][e].f == 1){
						if (model->cbmap[pos_codebook][e].q > 200/* && model->cbmap[pos_codebook][e].weight < 0.05*/){
							memset( &model->cbmap[pos_codebook][e], 0, sizeof(X7SBGCodeBookElem) );
							//printf("ENTRA");
						}
						else{
							if (model->cbmap[pos_codebook][e].p < 255)
								model->cbmap[pos_codebook][e].p = model->cbmap[pos_codebook][e].p + 1;
							if (model->cbmap[pos_codebook][e].q < 255)
								model->cbmap[pos_codebook][e].q = model->cbmap[pos_codebook][e].q + 1;
							model->cbmap[pos_codebook][e].weight = model->cbmap[pos_codebook][e].weight*(model->params.window - 1)/model->params.window;
						}
					}
				}

				/*if(pos_interest == pos_codebook){
					printf("learnMin[0] = %d; learnMin[1] = %d; learnMin[2] = %d\n", 
					model->cbmap[pos_codebook][e].learnMin[0],
					model->cbmap[pos_codebook][e].learnMin[1],
					model->cbmap[pos_codebook][e].learnMin[2]);

					printf("learnMax[0] = %d; learnMax[1] = %d; learnMax[2] = %d\n", 
					model->cbmap[pos_codebook][e].learnMax[0],
					model->cbmap[pos_codebook][e].learnMax[1],
					model->cbmap[pos_codebook][e].learnMax[2]);

					printf("boxMin[0] = %d; boxMin[1] = %d; boxMin[2] = %d\n", 
					model->cbmap[pos_codebook][e].boxMin[0],
					model->cbmap[pos_codebook][e].boxMin[1],
					model->cbmap[pos_codebook][e].boxMin[2]);

					printf("boxMax[0] = %d; boxMax[1] = %d; boxMax[2] = %d\n", 
					model->cbmap[pos_codebook][e].boxMax[0],
					model->cbmap[pos_codebook][e].boxMax[1],
					model->cbmap[pos_codebook][e].boxMax[2]);

					printf("p = %d; q = %d; weight = %f\n", 
					model->cbmap[pos_codebook][e].p,
					model->cbmap[pos_codebook][e].q,
					model->cbmap[pos_codebook][e].weight);
				}*/
			}
/*
			if(pos_interest == pos_codebook){
				printf("FOUND %d\n\n\n", found);
			}        
*/

			if(found == -1){
				last = -1;
				num_codewords = 0;
				min_weight = model->cbmap[pos_codebook][0].weight;
				for( e = 0; e < model->params.num_codewords_elements; e++ ){
					if (model->cbmap[pos_codebook][e].f != 0){
						last = e;
						num_codewords++;
					}
					if (min_weight > model->cbmap[pos_codebook][e].weight){
						min_weight = model->cbmap[pos_codebook][e].weight;
						pos_min_weight = e;
					}
				}
				if (num_codewords == model->params.num_codewords_elements){
					//printf("ENTRA  %d    %d\n", num_codewords, pos_min_weight);

					model->cbmap[pos_codebook][pos_min_weight].f = 1;
					model->cbmap[pos_codebook][pos_min_weight].p = 1;
					model->cbmap[pos_codebook][pos_min_weight].q = 1;
					model->cbmap[pos_codebook][pos_min_weight].weight = 1/(float)model->params.window;

					model->cbmap[pos_codebook][pos_min_weight].learnMin[0] = l0; model->cbmap[pos_codebook][pos_min_weight].learnMax[0] = h0;
					model->cbmap[pos_codebook][pos_min_weight].learnMin[1] = l1; model->cbmap[pos_codebook][pos_min_weight].learnMax[1] = h1;
					model->cbmap[pos_codebook][pos_min_weight].learnMin[2] = l2; model->cbmap[pos_codebook][pos_min_weight].learnMax[2] = h2;
					model->cbmap[pos_codebook][pos_min_weight].boxMin[0] = model->cbmap[pos_codebook][pos_min_weight].boxMax[0] = p0;
					model->cbmap[pos_codebook][pos_min_weight].boxMin[1] = model->cbmap[pos_codebook][pos_min_weight].boxMax[1] = p1;
					model->cbmap[pos_codebook][pos_min_weight].boxMin[2] = model->cbmap[pos_codebook][pos_min_weight].boxMax[2] = p2;
					model->cbmap[pos_codebook][pos_min_weight].mean[0] = p0;
					model->cbmap[pos_codebook][pos_min_weight].mean[1] = p1;
					model->cbmap[pos_codebook][pos_min_weight].mean[2] = p2;
				
				}
				else{
					if (last < model->params.num_codewords_elements - 1 ){
						last++;
						model->cbmap[pos_codebook][last].f = 1;
						model->cbmap[pos_codebook][last].p = 1;
						model->cbmap[pos_codebook][last].q = 1;
						model->cbmap[pos_codebook][last].weight = 1/(float)model->params.window;

						model->cbmap[pos_codebook][last].learnMin[0] = l0; model->cbmap[pos_codebook][last].learnMax[0] = h0;
						model->cbmap[pos_codebook][last].learnMin[1] = l1; model->cbmap[pos_codebook][last].learnMax[1] = h1;
						model->cbmap[pos_codebook][last].learnMin[2] = l2; model->cbmap[pos_codebook][last].learnMax[2] = h2;
						model->cbmap[pos_codebook][last].boxMin[0] = model->cbmap[pos_codebook][last].boxMax[0] = p0;
						model->cbmap[pos_codebook][last].boxMin[1] = model->cbmap[pos_codebook][last].boxMax[1] = p1;
						model->cbmap[pos_codebook][last].boxMin[2] = model->cbmap[pos_codebook][last].boxMax[2] = p2;

						model->cbmap[pos_codebook][last].mean[0] = p0;
						model->cbmap[pos_codebook][last].mean[1] = p1;
						model->cbmap[pos_codebook][last].mean[2] = p2;

					}
				}
            }


        }
    }

}




/*********************************************************************************/
/*********************************************************************************/

void x7sBGCodeBookDiff( const X7sCBookInterBGStatModel* model, const CvArr* _image, CvArr* _fgmask)
{

    CvMat stub, *image = cvGetMat(_image, &stub, 0, 0);
    CvMat mstub, *mask = cvGetMat(_fgmask, &mstub, 0, 0);
    //CvMat mstub_inter, *mask_inter = cvGetMat( _fgmask_inter, &mstub_inter );

    int x, y, pos_codebook, e;
    uchar m0, m1, m2, M0, M1, M2;
	
	float Xt2, Vi2, XtVi2, P2, delta;

	float numerador_alpha, denominador_alpha, alpha, term_prod1, term_prod2, beta;

	m0 = model->params.modMin[0]; M0 = model->params.modMax[0];
    m1 = model->params.modMin[1]; M1 = model->params.modMax[1];
    m2 = model->params.modMin[2]; M2 = model->params.modMax[2];


    for( y = 0; y < image->rows; y++ ){
        const uchar* p = image->data.ptr + image->step*y;
        uchar* m = mask->data.ptr + mask->step*y;
        //uchar* m_inter = mask_inter->data.ptr + mask_inter->step*y;

		for( x = 0; x < image->cols; x++, p += 3 ){

            uchar p0 = p[0], p1 = p[1], p2 = p[2], p0m, p1m, p2m;
            int l0 = p0 + m0, l1 = p1 + m1, l2 = p2 + m2;
            int h0 = p0 - M0, h1 = p1 - M1, h2 = p2 - M2;
            m[x] = (uchar)255;
            //m_inter[x] = (uchar)255;
			pos_codebook = y*image->cols + x;
            

			for(e = 0; e < model->params.num_codewords_elements; e++ ){
                /*if( model->cbmap[pos_codebook][e].boxMin[0] <= l0 && h0 <= model->cbmap[pos_codebook][e].boxMax[0] &&
                    model->cbmap[pos_codebook][e].boxMin[1] <= l1 && h1 <= model->cbmap[pos_codebook][e].boxMax[1] &&
                    model->cbmap[pos_codebook][e].boxMin[2] <= l2 && h2 <= model->cbmap[pos_codebook][e].boxMax[2] &&
					(model->cbmap[pos_codebook][e].p > model->params.window))
                {
                    m[x] = 0;
                    break;
                }*/
				if (model->cbmap[pos_codebook][e].f == 1 && (model->cbmap[pos_codebook][e].p > 40/*model->params.window*/)/*&&
					model->cbmap[pos_codebook][e].weight > 0.2*/){
					if( model->cbmap[pos_codebook][e].boxMin[0] <= p0 && p0 <= model->cbmap[pos_codebook][e].boxMax[0] &&
						model->cbmap[pos_codebook][e].boxMin[1] <= p1 && p1 <= model->cbmap[pos_codebook][e].boxMax[1] &&
						model->cbmap[pos_codebook][e].boxMin[2] <= p2 && p2 <= model->cbmap[pos_codebook][e].boxMax[2] )
					{
						//m_inter[x] = 0;
						//break;
					}
					if( model->cbmap[pos_codebook][e].boxMin[0] <= p0 && p0 <= model->cbmap[pos_codebook][e].boxMax[0] &&
						model->cbmap[pos_codebook][e].boxMin[1] <= p1 && p1 <= model->cbmap[pos_codebook][e].boxMax[1] &&
						model->cbmap[pos_codebook][e].boxMin[2] <= p2 && p2 <= model->cbmap[pos_codebook][e].boxMax[2] &&
						model->cbmap[pos_codebook][e].weight > 0.3)
					{
						m[x] = 0;
						//break;
					}

					/*p0m = model->cbmap[pos_codebook][e].mean[0];
					p1m = model->cbmap[pos_codebook][e].mean[1];
					p2m = model->cbmap[pos_codebook][e].mean[2];

					numerador_alpha = (float)p0 * (float)p0m + (float)p1 * (float)p1m + (float)p2 * (float)p2m;
					denominador_alpha = (float)p0m * (float)p0m + (float)p1m * (float)p1m + (float)p2m * (float)p2m;
					alpha = numerador_alpha/denominador_alpha;

					term_prod1 = (p1 - alpha * p1m)*(p1 - alpha * p1m);
					term_prod2 = (p2 - alpha * p2m)*(p2 - alpha * p2m);
					beta = (term_prod1 + term_prod2)/4;
					if (beta < 80){
						m[x] = 0;
						//break;
					}*/

				}

            }
        }
    }
}




