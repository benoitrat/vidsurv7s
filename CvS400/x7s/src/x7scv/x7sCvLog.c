#include "x7sCvLog.h"

const char* X7sCvLog_names[X7S_LOGLEVEL_END] = X7S_LOGNAMES;
int X7sCvLog_level=X7S_LOGLEVEL_MED;
int X7sCvLog_noError=X7S_LOG_NOERROR;
FILE* X7sCvLog_fout=NULL;//=stdout;
FILE* X7sCvLog_ferr=NULL;//=stderr;


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Private functions
//----------------------------------------------------------------------------------------
void print_msg(const char *prefix, FILE *stream, const char *szFormat, va_list args) {
	time_t rawtime;
	struct tm * timeinfo;
	char buffer [80];

	time ( &rawtime );
	timeinfo = localtime ( &rawtime );

	strftime(buffer,80,"[%X]",timeinfo);
	fprintf(stream,"%s %s",buffer,prefix);
	vfprintf(stream,szFormat,args);
	fflush(stream);
	va_end(args);
	fprintf(stream,"\n");
}


void ix7sCvLog_Print(bool with_time, const char *prefix, const char *funcname,const char *filepath, int line, FILE *stream, const char *szFormat, va_list args)
{
	char buffer [80];
	const char *filename;
	time_t rawtime;
	struct tm * timeinfo;

	if(with_time)
	{
		time ( &rawtime );
		timeinfo = localtime ( &rawtime );
		strftime(buffer,80,"[%H:%M:%S]",timeinfo);
	}
	if(funcname==NULL) fprintf(stream,"%s %s: ",buffer,prefix);
	else fprintf(stream,"%s %s > %s: ",buffer,prefix,funcname);
	if(szFormat) vfprintf(stream,szFormat,args);
	if(filepath)
	{
		filename = strrchr(filepath,X7S_PATH_SEP);
		if(filename) filename++;
		else filename=filepath;
		fprintf(stream," (%s:%d)",filename,line);
	}
	fprintf(stream,"\n");
	fflush(stream);
	va_end(args);
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public interfaces
//----------------------------------------------------------------------------------------

/**
 * @brief Interface to set the logLevel of the X7SCV library.
 * @param log_level The level of log we want (See @ref X7S_LOGLEVEL_xxx)
 * @return @ref X7S_CV_RET_ERR if log_level is not correct, otherwise @ref X7S_CV_RET_OK.
 */
int x7sCvLog_SetLevel(int log_level) {
		if(X7S_LOGLEVEL_LOWEST <= log_level && log_level < X7S_LOGLEVEL_END) {
			X7sCvLog_level=log_level;
			return X7S_CV_RET_OK;
		}
		return X7S_CV_RET_ERR;
}



/**
 * @brief Configure the LOG object
 *
 * This is the public interface to configure the LOG object:
 *	its level, the log files, the threshold between error and normal.
 *
 * @param log_level Set the level for the current library.
 * @param f_out Set the output log file (by default it's stdout)
 * @param f_err Set the error log file (by default it's stderr)
 * @param no_error Set the level to switch between error and output log message.
 *
 * @return @ref X7S_RET_OK if the level has been applied, otherwise @ref X7S_RET_ERR.
 */
int x7sCvLog_Config(int log_level, FILE *f_out, FILE *f_err, int no_error)
{
	int ret=x7sCvLog_SetLevel(log_level);
	X7sCvLog_noError=no_error;

	//Check if the file is valid.
	if(f_out==NULL) f_out=stdout;
	if(f_err==NULL) f_err=stderr;

	//Set the new one
	X7sCvLog_fout=f_out;
	X7sCvLog_ferr=f_err;

	return ret;
}



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Static functions for X7sNetLog..
//----------------------------------------------------------------------------------------


/**
 * @brief Print Log message in to the general Log Level
 *
 * The general Log level is given by X7sNetLog_Level.
 *
 * @param level The log level (e.i.@ref X7S_LOGLEVEL_xxx)
 * @param szFormat The format of the output.
 * @param ... The variable argument list (va_list).
 *
 */
void X7sCvLog_PrintMsg(int level, const char* szFormat, ...) {
	if(level <= X7sCvLog_level) {
		va_list args;
		va_start(args,szFormat);
		print_msg("Debug: ",stdout,szFormat,args);
	}
}

/**
 * @brief Print Error message into the stderr and maybe a file if specified.
 *
 * @param szFormat The format of the output.
 * @param ... The variable argument list (va_list).
 *
 */
void X7sCvLog_PrintErr(const char* szFormat, ...) {
	va_list args;
	va_start(args,szFormat);
	print_msg("Error: ",stderr,szFormat,args);
}

/**
 * @brief Return if true when the general Log level is greater than the one given
 * @param level The given level.
 */
bool X7sCvLog_IsDisplay(int level) {

	return (level <= X7sCvLog_level);
}

/**
 * @brief Return if true when the general Log level is greater than the one given
 * @param level The given level.
 */
bool ix7sCvLog_IsDisplay(int level)
{
	return (level <= X7sCvLog_level);
}

void ix7sCvLog_PrintLevel(const char *funcname,const char *file, int line, int level, const char* szFormat, ...)
{
	//Convert to have similar function has in C++.
	int true=1;
	int noError=X7sCvLog_noError;
	const char **names=X7sCvLog_names;
	FILE *ferr=(X7sCvLog_ferr)?X7sCvLog_ferr:stderr;
	FILE *fout=(X7sCvLog_fout)?X7sCvLog_fout:stdout;
	FILE *output;

	if(X7S_LOGLEVEL_LOWEST <= level && level < X7S_LOGLEVEL_END)
	{
		va_list args;
		va_start(args,szFormat);
		if(level < noError) output=ferr;
		else output=fout;
		ix7sCvLog_Print(true,names[level],funcname,file,line,output,szFormat,args);

#ifdef DEBUG
		if(level < noError)
		{
			if(output!=stderr) ix7sCvLog_Print(true,names[level],funcname,file,line,(FILE*)stderr,szFormat,args);
		}
		else
		{
			if(output!=stdout) ix7sCvLog_Print(true,names[level],funcname,file,line,(FILE*)stdout,szFormat,args);
		}
#endif
	}
}




