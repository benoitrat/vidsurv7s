#include "x7scv.h"
#include "x7sCvLog.h"


//================================================================================
//-- Defines and Enumerates


#define IX7S_CV_NO_INTEREST 255	//!< Value of a pixel considered as no interest.
#define IX7S_CV_FG_TOINSERT 1		//!< Pixel considered as FG but is inserting inside BG (during insDelayLog).
#define IX7S_CV_BG_TOREFRESH 0		//!< Pixel considered as BG which is always refreshing.

/**
* @brief this enum represent two pixel on the YUV422 format.
* @note YUV 4:2:2 FORMAT is: U  Y1  V  Y2
*/
enum {
	IND_U=0,
	IND_Y1,
	IND_V,
	IND_Y2
};

//================================================================================
//-- Private Prototypes

static void ix7sReleaseBCDistBGModel( X7sBCDistBGModel** bg_model );
static int  ix7sUpdateBCDistBGModel( IplImage* curr_frame, X7sBCDistBGModel*  bg_model );

int ix7sBrightChromaDistortion(IplImage *curr_frame, X7sBCDistBGModel *bg_model);


//================================================================================
//-- Functions

/**
* @brief Set the structure for the parameters of
*  Brightness/Chromacity Distortion background model.
*
* The parameters are:
* 		- threshold: Which is the threshold of the beta difference.
* 		- insDelayLog: 	Is the log2(nFrames) needed for a FG pixel to be inserted as BG.
* (nFrames=2^insDelayLog).
* 		- refreshDelay:
* 		- pseudomed_1:	Is the value of the pseudo median filter for the first iteration.
* 		- pseudomed_2: 	Is the value of the pseudo median filter for the second iteration.
* 		- dilate_k: Is the size of the dilate kernel (Dilate is done with a square shape).
*/
int x7sSetBCDistBGStatModelParams(X7sBCDistBGStatModelParams *params) {

	//Check Memory
	if(params==NULL) { return X7S_CV_RET_ERR; }

	//Set default parameters
	params->threshold=38;
	params->insDelayLog=5;			//The value expected should be between [3-6] <=> (8 frames, 64 frames)
	params->refreshDelay=32;
	params->pseudomed_1=5;
	params->pseudomed_2=7;
	params->dilate_k=3;
	params->is_dispBG=TRUE;

	return X7S_CV_RET_OK;
}

/**
* @brief Create and initiate the Background model with the first frame and the parameters.
* @note For this background model it is preferable to use images in YUV 4:2:2 format, however
* if you send a 3 channels images in BGR the conversion will be done automatically to YUV422
* using x7sCvtColor() function.
* @param first_frame A pointer on the first_frame. The format can be BGR or
* directly in YUV 4:2:2 format.
* @param parameters The parameters for the background model.
* @return A generic CvBGStatModel with its specific type.
*/
CvBGStatModel* x7sCreateBCDistBGModel(IplImage *first_frame ,X7sBCDistBGStatModelParams *parameters) {

	//Declare variable
	X7sBCDistBGModel *bg_model;
	int code;

	CV_FUNCNAME("x7sCreateBCDistBGModel()");

	//Check input variable.
	if(!CV_IS_IMAGE(first_frame) && first_frame->nChannels!=2) {
		X7S_PRINT_ERROR(cvFuncName,"imI==NULL or nChannels!=2");
		return NULL;
	}

	//Reserved memory for structure.
	bg_model = (X7sBCDistBGModel*)calloc(sizeof(*bg_model),1);
	bg_model->type=X7S_BG_MODEL_BCD;
	bg_model->release=(CvReleaseBGStatModel)ix7sReleaseBCDistBGModel;
	bg_model->update=(CvUpdateBGStatModel)ix7sUpdateBCDistBGModel;

	//Init parameters
	if(parameters == NULL)
	{
		x7sSetBCDistBGStatModelParams(&bg_model->params);
	}
	else
	{
		bg_model->params=*parameters;
	}

	//Then reserved the memory for the generic background.
	bg_model->background = cvCreateImage(cvGetSize(first_frame),IPL_DEPTH_8U,first_frame->nChannels);
	bg_model->foreground = cvCreateImage(cvGetSize(first_frame),IPL_DEPTH_8U,1);
	bg_model->storage = cvCreateMemStorage(0);
	bg_model->layer_count=0;

	//Then reserved the specific memory for X7sBGDistModel.
	bg_model->mean_YUV422 = cvCreateImage(cvGetSize(first_frame),IPL_DEPTH_32F,2);
	bg_model->bg_YUV422 = cvCreateImage(cvGetSize(first_frame),IPL_DEPTH_8U,2);
	bg_model->count_delay = cvCreateImage(cvGetSize(first_frame),IPL_DEPTH_8U,1);

	//Reset mean image into I.
	bg_model->if_reset=TRUE;

	//Reset the mean and mask image with 0.
	cvSet(bg_model->count_delay,cvScalarAll(0),NULL);

	//Find if we need to apply a specific transformation.
	if(first_frame->nChannels==3) code=X7S_CV_BGR2YUV422;
	else code=X7S_CV_CVTCOPY;

	//Set background as the first frame
	x7sCvtColor(first_frame,bg_model->bg_YUV422,code);
	cvSet(bg_model->background,cvScalarAll(255),NULL);

	return (CvBGStatModel*)bg_model;
}

/**
* @brief Release the memory of the Brightness/Contrast Distortion background model.
*/
static void ix7sReleaseBCDistBGModel(X7sBCDistBGModel** p_bg_model)
{
	//Declaration
	X7sBCDistBGModel* bg_model;

	//Checking the arguments.
	CV_FUNCNAME( "ix7sReleaseBCDistBGModel()" );
	if( !p_bg_model ) X7S_PRINT_ERROR(cvFuncName,"Address of pointer is not valid",cvFuncName);
	bg_model= *p_bg_model;
	if(!bg_model) X7S_PRINT_ERROR(cvFuncName,"Pointer is NULL",cvFuncName);

	//Remove generic memory
	cvReleaseImage(&(bg_model->background));
	cvReleaseImage(&(bg_model->foreground));
	cvReleaseMemStorage(&bg_model->storage);

	//Remove specific memory
	cvReleaseImage(&(bg_model->bg_YUV422));
	cvReleaseImage(&(bg_model->mean_YUV422));
	cvReleaseImage(&(bg_model->count_delay));

	//Free the structure memory and set the pointed value unusable.
	free(bg_model);
	(*p_bg_model)=NULL;
}


/**
* @brief Update the background model with the current_frame.
* @param first_frame A pointer on the first_frame in YUV 4:2:2 format (2 channels).
* @param params The parameters for the background model.
* @note The computation of this function is done with floating image.
*/
static int ix7sUpdateBCDistBGModel(IplImage *curr_frame, X7sBCDistBGModel* bg_model) {

	uint8_t  *p_I, *p_BG, *p_FG, *p_M, *end, refreshDelay;
	float *p_E, insDelay_mul, insDelay_div;
	X7sBCDistBGStatModelParams* params;
	IplImage *im_yuv422=NULL;

	CV_FUNCNAME("ix7sUpdateBCDistBGModel()");


	//Set the parameters value
	params=(X7sBCDistBGStatModelParams*)&(bg_model->params);
	insDelay_div=(float)(pow(2,params->insDelayLog));
	insDelay_mul=insDelay_div-1.f;
	refreshDelay=params->refreshDelay;


	//Transform input
	if(X7S_IS_IMAGENC(curr_frame,2)) {
		im_yuv422=curr_frame;
	}
	else if(X7S_IS_IMAGENC(curr_frame,3)) {
		im_yuv422 = x7sCreateCvtImage(curr_frame,X7S_CV_BGR2YUV422);
		x7sCvtColor(curr_frame,im_yuv422,X7S_CV_BGR2YUV422);
	}
	else {
		X7S_PRINT_ERROR(cvFuncName,"Wrong curr_frame type");
		return FALSE;
	}

	if(!X7S_IS_IMAGENC(bg_model->bg_YUV422,2) || !X7S_IS_IMAGENC(bg_model->mean_YUV422,2) || !X7S_IS_IMAGENC(bg_model->foreground,1)) {
		X7S_PRINT_ERROR(cvFuncName,"Wrong number of channel in bg_model");
		return FALSE;
	}
	if(bg_model->mean_YUV422->depth!=IPL_DEPTH_32F) {
		X7S_PRINT_ERROR(cvFuncName,"Wrong depth");
		return FALSE;
	}


	//Init the pointers on the images
	p_I = (uint8_t*)im_yuv422->imageData;	//Input image
	end = (uint8_t*)p_I + im_yuv422->imageSize;

	p_E = (float*)bg_model->mean_YUV422->imageData;	//Mean image

	p_BG = (uint8_t*)bg_model->bg_YUV422->imageData;	//BackGround image
	p_FG = (uint8_t*)bg_model->foreground->imageData;	//ForeGround image.
	p_M  = (uint8_t*)bg_model->count_delay->imageData;	//Mask and count.


	//Perform the operation 2-pixels by 2-pixels due to YUV 4:2:2.
	while(p_I < end) {

		//Set the background with the current frame and reset the mean images
		if(bg_model->if_reset) {
			p_BG[0]=p_I[0];
			p_BG[1]=p_I[1];
			p_E[0]=(float)((uint8_t)(p_BG[0]));
			p_E[1]=(float)((uint8_t)(p_BG[1]));
			(*p_M)=IX7S_CV_BG_TOREFRESH;
		}
		else if((*p_M)==IX7S_CV_NO_INTEREST) {
			p_BG[0]=p_I[0];
			p_BG[1]=p_I[1];
		}
		else {

			//In case the pixel is foreground
			if((*p_FG)) {

				//If it was background in the previous iteration
				if((*p_M)==IX7S_CV_BG_TOREFRESH) {
					(*p_M)=refreshDelay;
				}
				//If it is FG and need to be inserted into the background mean.
				else if((*p_M)==IX7S_CV_FG_TOINSERT) {
					p_E[0]=(insDelay_mul*(float)p_E[0] + (float)p_I[0])/insDelay_div;
					p_E[1]=(insDelay_mul*(float)p_E[1] + (float)p_I[1])/insDelay_div;
				}
				//Otherwise decrease refresh count until going to insert.
				else {
					(*p_M)--;
				}
			}
			//Otherwise update the background by converging the current pixel to the mean value.
			else {
				p_E[0]=(insDelay_mul*(float)p_E[0] + (float)p_I[0])/insDelay_div;
				p_E[1]=(insDelay_mul*(float)p_E[1] + (float)p_I[1])/insDelay_div;
				(*p_M)=IX7S_CV_BG_TOREFRESH;
			}

			//Copy E in BG using 8 bits.
			p_BG[0]=(uint8_t)(p_E[0]+0.5);
			p_BG[1]=(uint8_t)(p_E[1]+0.5);
		}

		//Go to the next pixel position (2 bytes for YUV422 and 1 for Grey)
		p_E+=2;
		p_BG+=2;
		p_I+=2;
		p_M++;
		p_FG++;
	}

	bg_model->if_reset=FALSE;
	if(bg_model->params.is_dispBG) {
		x7sCvtColor(bg_model->bg_YUV422,bg_model->background,X7S_CV_YUV4222BGR);
	}

	//Then call the function to compute the distortion after background update.
	ix7sBrightChromaDistortion(im_yuv422,bg_model);


	//If we have created a temporary image we need to release it.
	if(im_yuv422!=curr_frame) cvReleaseImage(&im_yuv422);

	return X7S_CV_RET_OK;
}



/**
 * @brief Compute the Brightness distortion (alpha) and Chromacity distortion (beta)
 * following the papers @cite{Horprasert1999}.
 * @warning curr_frame must be in a YUV422 format.
 */
int ix7sBrightChromaDistortion(IplImage *curr_frame, X7sBCDistBGModel *bg_model) {

        uint8_t  *p_I, *p_BG, *p_FG,*p_M,*end;
        float tmp, alpha1, beta1, alpha2, beta2, thrs;
        bool is_p1ok,is_p2ok;


        //Check input image
        if(curr_frame->nChannels!=2) {
                printf("Wrong number of channel for input image\n");
                return FALSE;
        }

        //(if(CV_ARE_SIZES_EQ())


        //Init the pointers on the images
        p_I = 	(uint8_t*)curr_frame->imageData; //Input image
        p_BG = 	(uint8_t*)bg_model->bg_YUV422->imageData;        //BackGround image
        p_FG = 	(uint8_t*)bg_model->foreground->imageData;        //Foreground image
        p_M = 	(uint8_t*)bg_model->count_delay->imageData;  	//Mask for recursive iteration
        end = p_I + curr_frame->imageSize;

        thrs=(float)((uint8_t)bg_model->params.threshold);

        //Perform the operation 2-pixels by 2-pixels due to YUV 4:2:2.
        while(p_I < end) {


                /**************************************
                 *             STEP 1
                 * Compute Alpha (Luminance distortion).
                 **************************************/

                //TODO: It is maybe better to set the condition of no interest here to avoid unuseful computing.
                is_p1ok = (p_M[0] != IX7S_CV_NO_INTEREST);
                is_p2ok = (p_M[1] != IX7S_CV_NO_INTEREST);


                tmp = 0;
                tmp+= ((float)(p_I[IND_U])  * (float)(p_BG[IND_U]));
                tmp+= ((float)(p_I[IND_V])  * (float)(p_BG[IND_V]));


                //For pixel #n+0 (Use the current frame and the "static" background.
                alpha1  = tmp + ((float)(p_I[IND_Y1]) * (float)(p_BG[IND_Y1]));
                //For pixel #n+1 (Use the current frame and the "static" background.
                alpha2  = tmp + ((float)(p_I[IND_Y2]) * (float)(p_BG[IND_Y2]));

                tmp=0;
                tmp  = ((float)(p_BG[IND_U]) * (float)(p_BG[IND_U]));
                tmp += ((float)(p_BG[IND_V]) * (float)(p_BG[IND_V]));

                alpha1 /= (tmp + ((float)(p_BG[IND_Y1]) * (float)(p_BG[IND_Y1])));
                alpha2 /= (tmp + ((float)(p_BG[IND_Y2]) * (float)(p_BG[IND_Y2])));

                ////Set the value between 0 and 255. (I'm not sure about this)
                //alpha1 = (MAX(MIN(255.0*(alpha1-1.0f)+128.0f,255),0));
                //alpha2 = (MAX(MIN(255.0*(alpha2-1.0f)+128.0f,255),0));


                /**************************************
                 *             STEP 2
                 *   Compute Beta (Color distortion).
                 **************************************/

                beta1  = ((float)(p_I[IND_U]) - alpha1 * (float)p_BG[IND_U]);
                beta1 *= ((float)(p_I[IND_V]) - alpha1 * (float)p_BG[IND_V]);
                beta1 += beta1; //Pow 2

                beta2  = ((float)(p_I[IND_U]) - alpha2 * (float)p_BG[IND_U]);
                beta2 *= ((float)(p_I[IND_V]) - alpha2 * (float)p_BG[IND_V]);
                beta2 += beta2; //Pow 2


                ////Set the value between 0 and 255. (I'm not sure about this)
                //beta1 = (MAX(MIN(beta1,255),0));
                //beta2 = (MAX(MIN(beta2,255),0));


                /**************************************
                 *               STEP 3
                 *      Set the difference image.
                 *
                 *   The mask (p_M) and the difference
                 *    image (p_D) have only 1 channel
                 **************************************/

                //If it's bigger than a threshold or it is set as a not interesting pixel.
                //We use index 0 for pixel #n+0
                if(beta1 > thrs && p_M[0] != IX7S_CV_NO_INTEREST) {
                        p_FG[0] = X7S_CV_TRUEPIX;
                }
                else {
                        p_FG[0] = X7S_CV_FALSEPIX;
                }

                //We use index 1 for pixel #n+1
                if(beta2 > thrs && p_M[1] != IX7S_CV_NO_INTEREST) {
                        p_FG[1] = X7S_CV_TRUEPIX;
                }
                else {
                        p_FG[1] = X7S_CV_FALSEPIX;
                }


                /**************************************
                 *             FINAL STEP
                 * Jump to the next 2-pixels position.
                 **************************************/

                //A jump of 4 when the image is in color.
                p_I+=4;
                p_BG+=4;

                //A jump of 2 when it is in grey.
                p_M+=2;
                p_FG+=2;

        }
        return TRUE;
}




