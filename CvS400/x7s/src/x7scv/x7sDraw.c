#include "x7scv.h"
#include "x7sCvLog.h"
#include <stdarg.h>

/**
 * @brief Draw the bounding box and the center of the x7sConcompoList
 * @param im A pointer on a 1 channel and IPL_DEPTH_8U image.
 * @param ccList A pointer on a list of Connexed Component (CC).
 * @see x7sUpdateConCompoList().
 */
int x7sDrawConCompoList(IplImage *im,const X7sConCompoList* ccList) {
	int i;
	X7sConCompo *cc;
	CV_FUNCNAME("x7sDrawConCompoList()");

	X7S_CHECK_WARN(cvFuncName,X7S_IS_IMAGENC(im,1),X7S_RET_ERR,"Image input is not valid");

	cc = ccList->vec;
	for(i=0;i<ccList->size;i++) {
		if(cc[i].x>=0 && cc[i].y>=0) {
			cvRectangle(im, cvPoint(cc[i].min_x, cc[i].min_y), cvPoint(cc[i].max_x, cc[i].max_y),cvRealScalar(i+1),1,8,0);
			x7sDrawCross(im,cvPoint(cc[i].x,cc[i].y),cvRealScalar((i+ccList->size/2)%ccList->size),5);
		}
	}
	return X7S_CV_RET_OK;
}


/**
 * @brief Draw a cross on a given image.
 */
int x7sDrawCross(IplImage *im, CvPoint center, CvScalar color,  int size) {
	cvLine(im, cvPoint(center.x-size, center.y), cvPoint(center.x+size, center.y),color,1,8,0);
	cvLine(im, cvPoint(center.x, center.y-size), cvPoint(center.x, center.y+size),color,1,8,0);
	return X7S_CV_RET_OK;
}


/**
 * @brief Draw the legend in the Top-right corner.
 * @param im A pointer on an image.
 * @param text The text to put as legend.
 * @param color The color of the text.
 * @param n_line The line position (0 correspond to the first line).
 */
int x7sDrawLegend(IplImage *im, const char *text, CvScalar color, int n_line)
{
	CvFont font;
	CvPoint pos;

	//Init the font.
	cvInitFont(&font,CV_FONT_HERSHEY_PLAIN, 1,1, 0,1,8);

	//Set the position.
	pos.y=15+n_line*10;
	pos.x=im->width-10*(strlen(text));

	//Write the legend text on the image.
	cvPutText(im, text, pos, &font,color);

	return X7S_CV_RET_OK;
}


/**
 * @brief Draw a multi-image to display until 6 array
 * @note The image will have the same size as the first image, therefore if the first
 * image is NULL this function return an error.
 * @param pMultiIm A pointer on a (IplImage*): this image can be change its size, therefore
 * we need to send a pointer on a pointer to be able to handle size changes.
 * @param nImgs The number of images use to draw a the MultiImage. It should be between 1 and 6.
 * @param ... A list of pointers on IplImage.
 * @return @ref X7S_CV_RET_OK if everything if no errors occur, otherwise it returns @ref X7S_CV_RET_ERR.
 */
int x7sDrawMultiImages(IplImage **pMultiIm, int nImgs, ...) {

	int width, height,code;
	int x_offset,y_offset;
	int i, n_w, n_h;
	CvSize bigSize,normSize;
	IplImage *big,*img, *img_resized, *img_bgr;
	int x_border=10,y_border=10; //Border between each images

	CV_FUNCNAME("x7sDrawMultiImages()");


	//First, obtain the list of arguments.
	va_list args;
	va_start(args, nImgs);


	//Find the subplot function.
	switch(nImgs) {
	case 0:
		X7S_PRINT_ERROR(cvFuncName,"Number of images is 0....");
		return X7S_CV_RET_ERR;
	case 1:
	case 2:
	case 3:
		n_w=nImgs;
		n_h=1;
		break;
	case 4:
		n_h=2;
		n_w=2;
		break;
	case 5:
	case 6:
		n_w=3;
		n_h=2;
		break;
	default:
		X7S_PRINT_ERROR(cvFuncName,"Number of images too large....");
		return X7S_CV_RET_ERR;
	}

	//Then, get the Pointer on the first IplImage
	img = va_arg(args, IplImage*);
	if(img==NULL) return X7S_CV_RET_ERR;

	//Get the size of the multi-image
	width=img->width;
	height=img->height;
	normSize = cvSize(width,height);
	bigSize=cvSize(n_w*width+ (n_w+1)*x_border,n_h*height+ (n_h+1)*y_border);
	img_bgr = cvCreateImage(normSize, IPL_DEPTH_8U,3);

	//Then check if we need to modify the multi-image.
	big=(IplImage*)*pMultiIm;

	//Check if the size is correct, otherwise reset the image
	if(big) {
		if(!X7S_ARE_SIZESEQ(cvGetSize(big),bigSize)) {
			cvReleaseImage(&big);
			big=NULL;
		}
	}

	//If image has not been set or is reset.
	if(big==NULL) {
		big = cvCreateImage(bigSize, IPL_DEPTH_8U,3);
		cvSet(big,cvScalarAll(255),NULL);	//Set to white background
	}

	//Set the starting offset
	x_offset=x_border;
	y_offset=y_border;


	for(i=0;i<nImgs;i++) {

		//Check if image is NULL
		if(img==NULL) {
			X7sCvLog_PrintMsg(X7S_LOGLEVEL_MED,"Pointer on image %d is NULL....\n",i);
		}
		else {
			//Check size of the image
			if(X7S_ARE_SIZESEQ(cvGetSize(img),normSize)) {
				img_resized=img;
			}
			else {
				img_resized = cvCreateImage(normSize,img->depth,img->nChannels);
				cvResize(img,img_resized,CV_INTER_NN);
			}

			//Check color model of image
			X7S_CV_GET_CMODEL(img,code);
			if(code==0 && img->nChannels==1) {
				code = CV_GRAY2BGR;
			}

			//Then copy or convert according to color model code.
			if(code==0) cvCopy(img_resized,img_bgr,NULL);
			else x7sCvtColor(img_resized,img_bgr,code);

			//Set ROI
			cvSetImageROI(big, cvRect(x_offset, y_offset, width, height));
			// Resize the input image and copy the it to the Single Big Image
			cvResize(img_bgr, big,CV_INTER_LINEAR);
			// Reset the ROI in order to display the next image
			cvResetImageROI(big);

			//Draw a rectangle on the border to contrast against background...
			cvRectangle(big,
					cvPoint(x_offset-1,y_offset-1),
					cvPoint(x_offset+width,y_offset+height),
					cvScalarAll(0),1,8,0);

			//If the size were not equal we have create a temporary image that need to be release.
			if(!X7S_ARE_SIZESEQ(cvGetSize(img),normSize)) {
				cvReleaseImage(&img_resized);
			}
		}

		//Move to next position
		x_offset+=width+x_border;

		if((i+1)%n_w==0) {
			y_offset+=height+y_border;
			x_offset=x_border;
		}

		//And take next argument in the list.
		img = va_arg(args, IplImage*);
	}

	//And to end set the created image to the one pointed.
	*pMultiIm=big;

	//Finally release reserved memory
	cvReleaseImage(&img_bgr);
	va_end(args);

	//And return that the function proceed.
	return X7S_CV_RET_OK;
}
