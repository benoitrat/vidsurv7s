#include "x7scv.h"

//================================================================================
//-- Internal includes to fasten compilation
#include "x7sCvLog.h"


//================================================================================
//-- Defines and Enumerates
typedef union FIXED11_3tag {
	unsigned short full;
	struct part11_3tag {
		unsigned short fraction: 3;
		unsigned short integer: 13;
	} part;
} FIXED11_3;


//================================================================================
//-- Private Static Prototypes
static void ix7sReleaseClusterBGModel(X7sClusterBGStatModel** bg_model );
static X7S_UNUSED int  ix7sUpdateClusterBGModel( IplImage* curr_frame, X7sClusterBGStatModel*  bg_model );
static int  ix7sUpdateClusterBGModel_F( IplImage* curr_frame, X7sClusterBGStatModel*  bg_model );

//================================================================================
//-- Private Prototypes
bool ix7sIsPixelBelongCluster(FIXED11_3 C_k, FIXED11_3 X_ij, FIXED11_3 dist);
bool ix7sIsPixelBelongCluster_F(float C_k, float X_ij, float dist);
int ix7sClusterSorting(X7sCluster *ar_Cluster, int nof_K, int k_matched);
int ix7sClusterSorting_F(X7sCluster *ar_Cluster, int nof_K, int k_matched);

float getFloatFIXEDX_3(FIXED11_3 fi);
void pixInsertion(FIXED11_3 *C_k, FIXED11_3 *X, uint8_t type);
FIXED11_3 setIntegerFIXEDX_3(uint8_t integer);


//================================================================================
//-- Function

/**
* @brief Set the default value for parameters of
*  Brightness/Chromacity Distortion background model.
*/
int x7sSetClusterBGStatModelParams(X7sClusterBGStatModelParams *params) {
	//Check Memory
	if(params==NULL) { return X7S_CV_RET_ERR; }

	//Set default parameters
	params->K=3;
	params->T=38;
	params->insDelayLog=4;
	params->dB=30;
	params->dS=8;
	params->is_dispBG=TRUE;

	return X7S_CV_RET_OK;
}

/**
* @brief Create and initiate the Background model with the
* first frame and the parameters.
* @param first_frame A pointer on the first_frame.
* @param parameters The parameters for the background model.
* @return A generic BGStatModel with the default type.
*/
CvBGStatModel* x7sCreateClusterBGModel(IplImage *first_frame ,X7sClusterBGStatModelParams *parameters)
{

	//Declare variable
	X7sClusterBGStatModel *bg_model;

	CV_FUNCNAME( "x7sCreateClusterBGModel()" );

	//Check input variable.
	if(!CV_IS_IMAGE(first_frame)) {
		X7S_PRINT_ERROR(cvFuncName,"first_frame == NULL");
		return NULL;
	}

	//Reserved memory for structure.
	bg_model = (X7sClusterBGStatModel*)calloc(sizeof(*bg_model),1);
	bg_model->type=X7S_BG_MODEL_MOC;
	bg_model->release=(CvReleaseBGStatModel)ix7sReleaseClusterBGModel;
	bg_model->update=(CvUpdateBGStatModel)ix7sUpdateClusterBGModel_F;

	//Init parameters
	if(parameters == NULL)
	{
		x7sSetClusterBGStatModelParams(&bg_model->params);
	}
	else
	{
		bg_model->params=*parameters;
	}

	//Then reserved the memory for the generic background.
	bg_model->background = cvCreateImage(cvGetSize(first_frame),IPL_DEPTH_8U,first_frame->nChannels);
	bg_model->foreground = cvCreateImage(cvGetSize(first_frame),IPL_DEPTH_8U,1);
	bg_model->storage = cvCreateMemStorage(0);
	bg_model->layer_count=0;

	//Then reserved the memory for all the clusters (calloc set the memory with zeros).
	bg_model->data = (X7sCluster*)calloc(first_frame->imageSize,bg_model->params.K*sizeof(X7sCluster));

	//Then reserved the memory for the temporary image.
	bg_model->imPrev = cvCreateImage(cvGetSize(first_frame),first_frame->depth,first_frame->nChannels);
	bg_model->imK = cvCreateImage(cvGetSize(first_frame),first_frame->depth,first_frame->nChannels);

	//Reset the previous image with 255 (Far from cluster initialization).
	cvSet(bg_model->imPrev,cvScalar(255,255,255,255),NULL);

	return (CvBGStatModel*)bg_model;
}


/**
* @brief Release the memory of the Cluster background model.
*/
void ix7sReleaseClusterBGModel(X7sClusterBGStatModel ** p_bg_model)
{

	//Declaration of the variable
	X7sClusterBGStatModel* bg_model;

	//Checking arguments
	CV_FUNCNAME( "ix7sReleaseClusterBGModel()" );
	if( !p_bg_model ) X7S_PRINT_ERROR(cvFuncName,"Address of pointer is not valid");
	bg_model = (X7sClusterBGStatModel *)*p_bg_model;
	if(!bg_model) X7S_PRINT_ERROR(cvFuncName,"Pointer is NULL");

	//Remove generic memory
	cvReleaseImage(&(bg_model->background));
	cvReleaseImage(&(bg_model->foreground));
	cvReleaseMemStorage(&bg_model->storage);

	//Remove specific memory
	cvReleaseImage(&(bg_model->imPrev));
	cvReleaseImage(&(bg_model->imK));
	free(bg_model->data);

	//Free the structure memory and set the pointed value unusable.
	free(bg_model);
	(*p_bg_model)=NULL;

}


/**
* @brief Update at each t the bg_model to obtain a new bg_model->foreground.
*
* This update is similar as the Gaussian mixture model excepted that we have have
* a fixed cluster width instead of a standard variation.
*
* Some adaptation are made from the original papers to perform the operation on hardware:
* 		- The weight are not normalized between [0-1], we prefer to use the scale between [0-64].
*
*/

int ix7sUpdateClusterBGModel(IplImage* curr_frame,X7sClusterBGStatModel* bg_model)
{
	/*
	//Declare variable
	int x,y,c,line,posRGB,k, k_matched;
	X7sClusterBGStatModelParams* params;
	X7sCluster *ar_Cluster;
	uint16_t w_k;

	float fpWk, fpCk, fpX,fpXprev, fpDistBig, fpDistSmall;

	int vec3[3];


	FIXED11_3 C_k, X, Xprev, distBig,distSmall;


	//Set the parameters value
	params=(X7sClusterBGStatModelParams*)(&bg_model->params);
	distBig = setIntegerFIXEDX_3(params->dB);
	distSmall = setIntegerFIXEDX_3(params->dS);

	fpDistBig = (float)((uint8_t)params->dB);
	fpDistSmall = (float)((uint8_t)params->dS);


	//Loop on the image and on the cluster.
	for(y=0;y<curr_frame->height;y++) {
		line=y*curr_frame->width;
		for(x=0;x<curr_frame->width;x++) {

			//Reset FG value to zeros.
			bg_model->foreground->imageData[line+x]=0;

			for(c=0;c<curr_frame->nChannels;c++) {

				//Compute the position
				posRGB=(line+x)*curr_frame->nChannels + c;

				//Get the value and the cluster.
				X = setIntegerFIXEDX_3(curr_frame->imageData[posRGB]);
				Xprev = setIntegerFIXEDX_3(bg_model->imPrev->imageData[posRGB]);

				fpX=(uint8_t)curr_frame->imageData[posRGB];
				fpXprev=(uint8_t)bg_model->imPrev->imageData[posRGB];

				ar_Cluster = &(bg_model->data[posRGB*params->K]);
				k_matched=-1;


				//----------> Begin Search Cluster
				//TOCHECK: If operation are correct with bit size.
				for(k=0;k<params->K;k++) {

					C_k.full = ar_Cluster[k].C;
					fpCk=ar_Cluster[k].fpC;
					if(ix7sIsPixelBelongCluster_F(fpCk,fpX,fpDistBig) && k_matched<0) {
						fpWk = (ar_Cluster[k].fpW*63.f + 1.f)/64.f;
						fpCk = (fpCk*15.f + fpX)/16.f;
						k_matched=k;
					}
					else {
						fpWk = (ar_Cluster[k].fpW*63.f)/64.f;
					}
					ar_Cluster[k].fpC=fpCk;
					ar_Cluster[k].fpW=fpWk;


					//Check if X belong to the cluster
					if(ix7sIsPixelBelongCluster(C_k,X, distBig) && k_matched<0) {
						//(k,t-1)
						w_k = ar_Cluster[k].w;
						w_k *= 31;
						w_k += (1 << 7);
						w_k = (w_k +16) >> 5;	// Divide by 32 (Add 16 to round better)

						pixInsertion(&(C_k),&(X), params->insDelayLog);

						//(k,t)
						ar_Cluster[k].C = MIN(0x7FF,C_k.full); //Set it on 11 bits.
						ar_Cluster[k].w = MIN(128,w_k);

						//k_matched=k;
					}
					else {
						//w_{k,t} = (63/64)(w_{k,t-1}).
						w_k = ar_Cluster[k].w;
						w_k *= 31;
						w_k = (w_k +16) >> 5;	// Divide by 32 (Add 16 to round better)
						ar_Cluster[k].w = MIN(128,w_k);
					}
				} //<---------- End Search Cluster


				//TOCHECK: Look if the algorithm is correct!!!!!!!
				//Now sort the clusters in O(n^2).
				k_matched = ix7sClusterSorting(ar_Cluster,params->K,k_matched);

				//Set the last image and which K has matched
				bg_model->imPrev->imageData[posRGB] = (uint8_t)X.part.integer;


				//Keep vector
				vec3[c] = k_matched;

				//If no cluster is matched (Add to the smallest weight)
				if(k_matched < 0) {
					k=params->K-1;	//Index of the smallest weight.
					ar_Cluster[k].C=MIN(0x7FF,X.full);	//C_k is replaced with X.
					ar_Cluster[k].w=0;	//Weight is reset to zeros.

					ar_Cluster[k].fpC=fpX;
					ar_Cluster[k].fpW=0;

					//And set the pixel has foreground.
					bg_model->foreground->imageData[line+x]=255;
					break;
				}
				else {

					C_k.full = ar_Cluster[k_matched].C;
					fpCk=ar_Cluster[k_matched].fpC;

					//In case the X values are outside the 40% mid-range for the last two consecutive frame.
					//					if(!x7sIsPixelBelongCluster(C_k,X, distSmall) &&
					//							!x7sIsPixelBelongCluster(C_k,Xprev, distSmall)) {
					//						//Set the pixel has foreground.
					//						bgModel->imFG->imageData[line+x]=255;
					//						break;
					//					}
					if(!(ix7sIsPixelBelongCluster_F(fpCk,fpX,fpDistSmall)) &&
							!(ix7sIsPixelBelongCluster_F(fpCk,fpXprev,fpDistSmall))) {
						//Set the pixel has foreground.
						bg_model->foreground->imageData[line+x]=255;
						break;
					}



				}
			}	//<---- End loop channel

			if(X7sCvLog_IsDisplay(X7S_LOGLEVEL_HIGHEST)) {
				if(x==122 && y==66) {
					printf("%d\t",(bg_model->foreground->imageData[line+x]!=0));

					for(c=0;c<curr_frame->nChannels;c++) {

						//Compute the position
						posRGB=(line+x)*curr_frame->nChannels + c;

						//Get the value and the cluster.
						X = setIntegerFIXEDX_3(curr_frame->imageData[posRGB]);
						ar_Cluster = &(bg_model->data[posRGB*params->K]);
						printf("%03d %+d ",X.part.integer,vec3[c]);


						for(k=0;k<params->K;k++) {
							C_k.full = ar_Cluster[k].C;
							printf("%d %3.3f %06.2f ",k,((float)ar_Cluster[k].w)/128,getFloatFIXEDX_3(C_k));
							printf(" %3.3f %06.2f  ",ar_Cluster[k].fpW,ar_Cluster[k].fpC);
						}
						printf("\t");
					}
					printf("\n");
				}
			}
		}
	}
	*/
	return X7S_CV_RET_OK;
}


/**
* @brief Update a each t the bg_model to gather a new bg_model->foreground.
* @note This function is similar to ix7sUpdateClusterBGModel() except that we use
* float value here.
* @param current_frame The current frame in RGB/YCrCb format.
* @param bgModel	A pointer on the bg_model
*/
int ix7sUpdateClusterBGModel_F(IplImage *curr_frame,X7sClusterBGStatModel* bg_model) {

	//Declare variable
	int x,y,c,line,pos3C,pos1C,k,k_matched;
	X7sClusterBGStatModelParams* params;
	X7sCluster *ar_Cluster;

	float fpWk, fpCk, fpX,fpXprev, fpDistBig, fpDistSmall,fpSum,fpT;
	int B;



	//Set the parameters value
	params=(X7sClusterBGStatModelParams*)(&bg_model->params);
	fpDistBig = (float)((uint8_t)params->dB);
	fpDistSmall = (float)((uint8_t)params->dS);
	fpT=(float)(params->T)/32.f;


	//Loop on the image and on the cluster.
	for(y=0;y<curr_frame->height;y++) {
		line=y*curr_frame->width;
		for(x=0;x<curr_frame->width;x++) {

			//Reset FG value to zeros.
			pos1C=(line+x);
			bg_model->foreground->imageData[pos1C]=0;


			for(c=0;c<curr_frame->nChannels;c++) {

				//Compute the position
				pos3C=(pos1C)*curr_frame->nChannels + c;

				//Get the value and the cluster.
				fpX=(uint8_t)curr_frame->imageData[pos3C];
				fpXprev=(uint8_t)bg_model->imPrev->imageData[pos3C];
				ar_Cluster = &(bg_model->data[pos3C*(params->K)]);
				k_matched=-1;


				//----------> Begin Search Cluster
				for(k=0;k<params->K;k++) {

					fpCk=ar_Cluster[k].fpC;
					if(ix7sIsPixelBelongCluster_F(fpCk,fpX,fpDistBig) && k_matched<0) {
						fpWk = (ar_Cluster[k].fpW*63.f + 1.f)/64.f;
						fpCk = (fpCk*7.f + fpX)/8.f;
						k_matched=k;
					}
					else {
						fpWk = (ar_Cluster[k].fpW*63.f)/64.f;
					}
					ar_Cluster[k].fpC=fpCk;
					ar_Cluster[k].fpW=fpWk;
				} //<---------- End Search Cluster


				//Now sort the clusters in O(n^2).
				k_matched = ix7sClusterSorting_F(ar_Cluster,params->K,k_matched);

				//If no matching cluster is found, we update the least weighted cluster.
				if(k_matched < 0) {
					k=(params->K-1);	//Index of the lightest cluster.
					ar_Cluster[k].fpC=fpX;
					ar_Cluster[k].fpW=0;
				}


				//Set the last image
				bg_model->imPrev->imageData[pos3C] = (uint8_t)fpX;
				bg_model->imK->imageData[pos3C] = (char)k_matched;

				if(params->is_dispBG) bg_model->background->imageData[pos3C]=(uint8_t)(ar_Cluster[0].fpC);

				//Check if the number of K
				fpSum=0;
				B=params->K;
				for(k=0;k<params->K;k++) {
					fpSum+=ar_Cluster[k].fpW;
					if(fpSum>fpT) {
						B=k+1;
						if(k_matched>k) {
							k_matched=-1;
						}
						break;
					}
				}


				//If the intensity match none of the pixel
				if(k_matched < 0) {
					//set the pixel has foreground.
					bg_model->foreground->imageData[pos1C]=255;
				}
				else {
					fpCk=ar_Cluster[k_matched].fpC;
					if(!(ix7sIsPixelBelongCluster_F(fpCk,fpX,fpDistSmall)) &&
							!(ix7sIsPixelBelongCluster_F(fpCk,fpXprev,fpDistSmall))) {
						//Set the pixel has foreground.
						//bgModel->imFG->imageData[pos1C]=255;
					}
					else {
						if(params->is_dispBG) bg_model->background->imageData[pos3C]=(uint8_t)(ar_Cluster[k_matched].fpC);
					}
				}
			}	//<---- End loop channel


			if(X7sCvLog_IsDisplay(X7S_LOGLEVEL_HIGHEST)) {
				if(x==180 && y==122) {
					printf("%d\t",(bg_model->foreground->imageData[pos1C]!=0));

					for(c=0;c<curr_frame->nChannels;c++) {

						//Compute the position
						pos3C=(line+x)*curr_frame->nChannels + c;

						//Get the value and the cluster.
						ar_Cluster = &(bg_model->data[pos3C*params->K]);
						printf("%03d %+d ",(uint8_t)curr_frame->imageData[pos3C],bg_model->imK->imageData[pos3C]);
						for(k=0;k<params->K;k++) {
							printf(" %3.3f %06.2f  ",ar_Cluster[k].fpW,ar_Cluster[k].fpC);
						}
						printf("\t");
					}
					printf("\n");
				}
			}
		}
	}
	return X7S_CV_RET_OK;
}

//-----------------------------------------------------------------
// -- Internal functions.

/**
* @brief Sort an array of cluster
* @param ar_Cluster
* @param nof_K
* @param k_matched
* @return
*/
int ix7sClusterSorting_F(X7sCluster *ar_Cluster, int nof_K, int k_matched) {
	X7sCluster tmpCluster;
	int i,k;

	for(i=0;i<nof_K;i++) {
		for(k=i+1;k<nof_K;k++) {
			if(ar_Cluster[k].fpW > ar_Cluster[i].fpW) {
				memcpy(&(tmpCluster),&(ar_Cluster[i]),sizeof(X7sCluster));		// tmp <-  i
				memcpy(&(ar_Cluster[i]),&(ar_Cluster[k]),sizeof(X7sCluster));	//  i  <-  k
				memcpy(&(ar_Cluster[k]),&(tmpCluster),sizeof(X7sCluster));		//  k  <- tmp
				if(k==k_matched) k_matched=i;	//Change the k_matched.
				else {
					if(i==k_matched) k_matched=k;	//Change the k_matched.
				}
			}
		}
	}
	return k_matched;
}

int ix7sClusterSorting(X7sCluster *ar_Cluster, int nof_K, int k_matched) {
	X7sCluster tmpCluster;
	int i,k;

	for(i=0;i<nof_K;i++) {
		for(k=i+1;k<nof_K;k++) {
			if(ar_Cluster[k].w > ar_Cluster[i].w) {
				memcpy(&(tmpCluster),&(ar_Cluster[i]),sizeof(X7sCluster));		// tmp <-  i
				memcpy(&(ar_Cluster[i]),&(ar_Cluster[k]),sizeof(X7sCluster));	//  i  <-  k
				memcpy(&(ar_Cluster[k]),&(tmpCluster),sizeof(X7sCluster));		//  k  <- tmp
				if(k==k_matched) k_matched=i;	//Change the k_matched.
				else {
					if(i==k_matched) k_matched=k;	//Change the k_matched.
				}
			}
		}
	}
	return k_matched;
}

void pixInsertion(FIXED11_3 *C_k, FIXED11_3 *X, uint8_t type) {
	int divide, shift, multiply;

	shift=(5-type);
	divide=(int)pow(2,shift);
	multiply=(divide-1);

	C_k->full = multiply*C_k->full;
	C_k->full = C_k->full + X->full;
	C_k->full = (C_k->full + (divide/2)) >> shift;
}

float getFloatFIXEDX_3(FIXED11_3 fi) {
	float ret=fi.part.integer;
	ret+=((float)fi.part.fraction)/8.0f;
	return ret;
}


FIXED11_3 setIntegerFIXEDX_3(uint8_t integer) {
	FIXED11_3 ret={0};
	ret.part.integer=integer;
	return ret;
}

bool ix7sIsPixelBelongCluster_F(float C_k, float X_ij, float dist) {
	return ((C_k-dist) <= X_ij && X_ij <= (C_k + dist));
}


bool ix7sIsPixelBelongCluster(FIXED11_3 C_k, FIXED11_3 X_ij, FIXED11_3 dist) {
	FIXED11_3 X_min;
	X_min.full = C_k.full > dist.full ? (C_k.full - dist.full) : 0; //!< Check if value is less than zeros.
	return ((X_min.full) <= X_ij.full && X_ij.full <= (C_k.full + dist.full));
}

