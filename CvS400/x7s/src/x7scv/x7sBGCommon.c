#include "x7sBGCommon.h"

#include "x7sCvLog.h"

/**
 * @brief Obtain the default parameters for a BGStatModel.
 * @param type the BG_MODEL given by @ref X7S_BG_MODEL_xxx
 * @return The structure which contain all type of parameters.
 */
X7sBGStatModelParams* x7sCreateBGStatModelParams(int type) {

	CV_FUNCNAME("x7sCreateBGStatModelParams()");


	//Set memory.
	X7sBGStatModelParams *gen_param =
		(X7sBGStatModelParams*)calloc(sizeof(X7sBGStatModelParams),1);

	//Ensure all others to be NULL
	gen_param->fgd=NULL;
	gen_param->gauss=NULL;
	gen_param->bcd=NULL;
	gen_param->cluster=NULL;
	gen_param->cbk=NULL;
	gen_param->cbk_inter=NULL;
	gen_param->mid=NULL;

	//Then set the memory for the one selected.
	switch(type) {
	case X7S_BG_MODEL_FGD:
	case X7S_BG_MODEL_FGD_SIMPLE:
		gen_param->fgd= (CvFGDStatModelParams*)calloc(sizeof(CvFGDStatModelParams),1);
		x7sSetFGDStatModelParams(gen_param->fgd);
		break;
	case X7S_BG_MODEL_MOG:
		gen_param->gauss = (CvGaussBGStatModelParams*)calloc(sizeof(CvGaussBGStatModelParams),1);
		x7sSetGaussBGStatBGModelParams(gen_param->gauss);
		break;
	case X7S_BG_MODEL_CBK:
		gen_param->cbk = (x7sCBookBGStatModelParams*)calloc(sizeof(x7sCBookBGStatModelParams),1);
		x7sSetCBookBGStatBGModelParams(gen_param->cbk);
		break;
	case X7S_BG_MODEL_CBK_INTER:
		gen_param->cbk_inter = (X7sCBookInterBGStatModelParams*)calloc(sizeof(X7sCBookInterBGStatModelParams),1);
		x7sSetCBookInterBGStatBGModelParams(gen_param->cbk_inter);
		break;
	case X7S_BG_MODEL_MOC:
	case X7S_BG_MODEL_MOC_HW:
		gen_param->cluster = (X7sClusterBGStatModelParams*)calloc(sizeof(X7sClusterBGStatModelParams),1);
		x7sSetClusterBGStatModelParams(gen_param->cluster);
		break;
	case X7S_BG_MODEL_BCD:
		gen_param->bcd = (X7sBCDistBGStatModelParams*)calloc(sizeof(X7sBCDistBGStatModelParams),1);
		x7sSetBCDistBGStatModelParams(gen_param->bcd);
		break;
	case X7S_BG_MODEL_MID:
		gen_param->mid = (X7sMIDiffBGStatModelParams*)calloc(sizeof(X7sMIDiffBGStatModelParams),1);
		x7sSetMIDiffBGStatModelParams(gen_param->mid);
		break;
	default:
		X7S_PRINT_ERROR(cvFuncName,"Unknown type=%d",type);
		x7sReleaseBGStatModelParams(&gen_param);
		return NULL;
	}

	gen_param->type=type;
	return gen_param;
}

/**
 * @brief release the memory created by x7sCreateBGStatModelParams().
 * @param p_gen_params The adress of the pointer on a X7sBGStatModelParams structure.
 */
void x7sReleaseBGStatModelParams(X7sBGStatModelParams** p_gen_params) {

	X7sBGStatModelParams *gen_param = *(p_gen_params);
	if(gen_param->fgd) free(gen_param->fgd);
	if(gen_param->gauss) free(gen_param->gauss);
	if(gen_param->cluster) free(gen_param->cluster);
	if(gen_param->cbk) free(gen_param->cbk);
	if(gen_param->cbk_inter) free(gen_param->cbk_inter);
	if(gen_param->bcd) free(gen_param->bcd);
	free(gen_param);

	*(p_gen_params)=NULL;
}


/**
* @brief Create a BG model (with default parameters) given the type of model we want.
* @param first_frame The first frame in a correct format in order to create the correct BG model
* @param type The type of bg model (See @ref X7S_BG_MODEL_xxx)
* @return A pointer on a generic CvBGStatModel.
* @see x7sCreateBGModel(IplImage *first_frame ,X7sBGStatModelParams *params)
*/
CvBGStatModel* x7sCreateBGModelType(IplImage *first_frame ,int type) {
	X7sBGStatModelParams gen_params = {0};	//Set the structure with all at zeros.
	gen_params.type=type;
	return x7sCreateBGModel(first_frame,&gen_params);

}


/**
 * @brief Obtain the generic CvBGStatModel using generic parameters structure.
 * @param first_frame The first frame in the format desired for background model.
 * @param params A structure containing all the different parameters for
 * @return A generic CvBGStatModel.
 * @see cvCreateFGDStatModel(), cvCreateGaussianBGModel().
 * @see x7sCreateClusterBGModel(), x7sCreateBCDistBGModel().
 */
CvBGStatModel* x7sCreateBGModel(IplImage *first_frame ,X7sBGStatModelParams *params) {

	CV_FUNCNAME("x7sCreateBGModel()");

	if(!params) {
		X7S_PRINT_ERROR(cvFuncName,"Generic Parameters is NULL");
		return NULL;
	}

	switch(params->type) {
	case X7S_BG_MODEL_FGD:
	case X7S_BG_MODEL_FGD_SIMPLE:
		return cvCreateFGDStatModel(first_frame,params->fgd);
	case X7S_BG_MODEL_MOG:
		return cvCreateGaussianBGModel(first_frame,params->gauss);
	case X7S_BG_MODEL_CBK_INTER:
		return x7sCreateCBookInterBGModel(first_frame,params->cbk_inter);
	case X7S_BG_MODEL_MOC:
	case X7S_BG_MODEL_MOC_HW:
		return x7sCreateClusterBGModel(first_frame,params->cluster);
	case X7S_BG_MODEL_BCD:
		return x7sCreateBCDistBGModel(first_frame,params->bcd);
	case X7S_BG_MODEL_MID:
		return x7sCreateMIDiffBGModel(first_frame,params->mid);
	default:
		X7S_PRINT_ERROR(cvFuncName,"Unknown type=%d",params->type);
		return NULL;
	}
}

/**
* @brief Set the default value for parameters of
* Gaussian background model.
*/
int x7sSetGaussBGStatBGModelParams(CvGaussBGStatModelParams *params) {

	//Check Memory
	if(params==NULL) { return X7S_CV_RET_ERR; }

	//Set default parameters
	params->win_size=CV_BGFG_MOG_WINDOW_SIZE;
	params->n_gauss=CV_BGFG_MOG_NGAUSSIANS;

	params->bg_threshold=CV_BGFG_MOG_BACKGROUND_THRESHOLD;
	params->std_threshold=CV_BGFG_MOG_STD_THRESHOLD;
	params->minArea=CV_BGFG_MOG_MINAREA;
	params->weight_init=CV_BGFG_MOG_WEIGHT_INIT;
	params->variance_init=CV_BGFG_MOG_SIGMA_INIT;

	return X7S_CV_RET_OK;
}


/**
* @brief Set the default value for parameters of
*  Foreground Detector background model.
*/
int x7sSetFGDStatModelParams(CvFGDStatModelParams *params) {

	//Check Memory
	if(params==NULL) { return X7S_CV_RET_ERR; }

	//Set default parameters
	params->Lc=CV_BGFG_FGD_LC;
	params->N1c=CV_BGFG_FGD_N1C;
	params->N2c=CV_BGFG_FGD_N2C;

	params->Lcc=CV_BGFG_FGD_LCC;
	params->N1cc=CV_BGFG_FGD_N1CC;
	params->N2cc=CV_BGFG_FGD_N2CC;

	params->alpha1=CV_BGFG_FGD_ALPHA_1;
	params->alpha2=CV_BGFG_FGD_ALPHA_2;
	params->alpha3=CV_BGFG_FGD_ALPHA_3;
	params->delta=CV_BGFG_FGD_DELTA;
	params->T=CV_BGFG_FGD_T;
	params->minArea=CV_BGFG_FGD_MINAREA;


	return X7S_CV_RET_OK;
}

/**
* @brief Set the default value for parameters of
*  Codebook background model.
*/
int x7sSetCBookBGStatBGModelParams(x7sCBookBGStatModelParams *params)
{
	params=0;
	return X7S_CV_RET_OK;
}

