#include "x7scv.h"
#include "x7sCvLog.h"


//------------------ Private prototypes.
void x7sAddWeightedScalar_1C3C4C(const IplImage *src1, const IplImage *src2, IplImage *dst,CvScalar value,const IplImage *mask);
void x7sAddAlphaLayer_3C4C8U(const IplImage *src1, const IplImage *src2, IplImage *dst);

//------------------ Interface function (Check arguments)

/**
* @brief Merge two arrays using a scalar to obtain the proportion of both.
*
* @code
*
* foreach(c=channels)
* 	dst(I,c) = src1(I,c)*(ratio-1) + src2(I,c)*ratio.
* end
* @endcode
*
* If src2 is a grey image (1 channel),
* the image is first transformed in BGR with the 3 channels equals.
*
*
* @param src1	The first source array (background image)
* @param src2	The second source array (transparent image)
* @param dst	The destination array.
* @param value The mix ratio for each channels between [0-1]
* (If a value is > 1, then it is considered that they are between [0-255] and therefore we
* normalize dividing by 255).
* @param mask	The mixing of value is only applied is mask[I] > 0.
*
*/
void x7sAddWeightedScalar(const CvArr *src1, const CvArr *src2, CvArr *dst,CvScalar value, const IplImage *mask)
{
	IplImage *im_src1, *im_src2, *im_dst;

	CV_FUNCNAME("x7sAddWeightedScalar()");

	im_src1=(IplImage*)src1;
	im_src2=(IplImage*)src2;
	im_dst=(IplImage*)dst;

		//Check size of the image
	if(!X7S_ARE_IMSIZEEQ(im_src1,im_src2) || !X7S_ARE_IMSIZEEQ(im_src2,im_dst)) {
		X7S_PRINT_ERROR(cvFuncName,"Images size are different.");
		return;
	}


	if( (im_src1->nChannels >= 3) && (im_dst->nChannels>=3) )
	{
		if(im_src2->nChannels==1 || im_src2->nChannels>=3)
		{
			x7sAddWeightedScalar_1C3C4C(im_src1,im_src2,dst,value,mask);
			return;
		}
	}
	X7S_PRINT_ERROR(cvFuncName,"Channels are not correct. src1=%d, src2=%d, dst=%d",
			im_src1->nChannels, im_src2->nChannels, im_dst->nChannels);

}


/**
* @brief Add src2 on src1 using src2 alpha channel (transparency).
*
* @code
* foreach(c=channels)
*      dst[I,c] = src1[I,c]*(1-(src2[I,alpha]/255)) + src2[I,c]*(src2[I,alpha]/255)
* end
* @endcode
*
* @param src1 The first source array (background image, 3 or 4 channels)
* @param src2 The second source array (transparent image, 4 channels minimum)
* @param dst The destination array (merge image, 3 or 4 channels).
* @note This function only works with IPL_DEPTH_8U image for the moment.
*/
void x7sAddAlphaLayer(const CvArr *src1, const CvArr *src2, CvArr *dst)
{
	IplImage *im_src1, *im_src2, *im_dst;

	CV_FUNCNAME("x7sAddAlphaLayer()");

	im_src1=(IplImage*)src1;
	im_src2=(IplImage*)src2;
	im_dst=(IplImage*)dst;



	//Check size of the image
	if(!X7S_ARE_IMEQ(im_src1,im_src2) || !X7S_ARE_IMEQ(im_src2,im_dst)) {
		X7S_PRINT_ERROR(cvFuncName,"Images size are different.");
		return;
	}

	//Check depth of the image
	if(!( 	(im_src1->depth==IPL_DEPTH_8U)
			&& (im_src1->depth==IPL_DEPTH_8U)
			&& (im_src1->depth==IPL_DEPTH_8U)
	)){
		X7S_PRINT_ERROR(cvFuncName,"Images depth are not IPL_DEPTH_8U.");
		return;
	}


	//Check channels
	if( (im_src1->nChannels < 3) || (im_dst->nChannels<3) || (im_src2->nChannels<4))
	{
		X7S_PRINT_ERROR(cvFuncName,"Channels are not correct. src1=%d, src2=%d, dst=%d",
				im_src1->nChannels, im_src2->nChannels, im_dst->nChannels);
		return;
	}

	x7sAddAlphaLayer_3C4C8U(im_src1,im_src2,im_dst);
}


//------------------ Private function

void x7sAddWeightedScalar_1C3C4C(const IplImage *src1, const IplImage *src2, IplImage *dst,CvScalar value, const IplImage *mask)
{

	IplImage *tmp = NULL;
	IplImage *im1_c[4] = {NULL, NULL, NULL, NULL};
	IplImage *im2_c[4] = {NULL, NULL, NULL, NULL};
	IplImage *im2_ctmp[4] = {NULL, NULL, NULL, NULL};
	double ratio;
	int i;

	//Create the temporary image
	tmp = cvCreateImage(cvGetSize(src1),IPL_DEPTH_8U,1);

	//First split the channels or source 1
	for(i=0;i<src1->nChannels;i++) 	im1_c[i] = cvCreateImage(cvGetSize(src1),IPL_DEPTH_8U,1);
	cvSplit(src1, im1_c[0],im1_c[1],im1_c[2],im1_c[3]);

	//Split the channels of source 2, and duplicate some
	for(i=0;i<src2->nChannels;i++)
	{
		im2_ctmp[i] = cvCreateImage(cvGetSize(src2),IPL_DEPTH_8U,1);
		im2_c[i]=im2_ctmp[i];
	}
	cvSplit(src2, im2_c[0],im2_c[1],im2_c[2],im2_c[3]);
	for(i=src2->nChannels;i<4;i++) im2_c[i]=im2_ctmp[0];


	for(i=0;i<src1->nChannels;i++)
	{
		//If channels exist
		if(im1_c[i] && im2_c[i])
		{
			//Obtain the ratio between [0-1]
			ratio= value.val[i]/255.0;
			if(mask)
			{
				cvAddWeighted(im1_c[i],(1.0-ratio),im2_c[i],ratio,0,tmp);
				cvCopy(tmp,im1_c[i],mask);
			}
			else
			{
				cvAddWeighted(im1_c[i],(1.0-ratio),im2_c[i],ratio,0,im1_c[i]);
			}
		}
	}


	//Then merge the channels
	cvMerge(im1_c[0],im1_c[1],im1_c[2],im1_c[3],dst);

	//Release temporary image created for having independent channel
	cvReleaseImage(&tmp);
	for(i=0;i<4;i++)
	{
		if(im1_c[i]) cvReleaseImage(&(im1_c[i]));
		if(im2_ctmp[i]) cvReleaseImage(&(im2_ctmp[i]));
	}
}


void x7sAddAlphaLayer_3C4C8U(const IplImage *src1, const IplImage *src2, IplImage *dst)
{

	uint8_t *p_im1, *p_im2,*end,*p_dst;
	int nChan_im1, nChan_dst, c;
	float tmp, ratio;


	//Set the pointers.
	p_im1 =  (uint8_t*)src1->imageData;
	p_im2 =  (uint8_t*)src2->imageData;
	p_dst = (uint8_t*)dst->imageData;
	end = p_im2 + src2->imageSize; //4 Channels (Include Alpha)

	nChan_im1 = src1->nChannels;
	nChan_dst = dst->nChannels;


	//loop by 2 pixels
	while(p_im2<end)
	{

		//Compute the ratio for this pixel.
		ratio=(float)(p_im2[3]/255.f);

		//Chan 0
		c=0;
		tmp=((float)(p_im1[c]))*(1-ratio) + ((float)(p_im2[c]))*(ratio);
		p_dst[c]=(uint8_t)(tmp+0.5f);

		//Chan 1
		c=1;
		tmp=((float)(p_im1[c]))*(1-ratio) + ((float)(p_im2[c]))*(ratio);
		p_dst[c]=(uint8_t)(tmp+0.5f);

		//Chan 2
		c=2;
		tmp=((float)(p_im1[c]))*(1-ratio) + ((float)(p_im2[c]))*(ratio);
		p_dst[c]=(uint8_t)(tmp+0.5f);

		//Chan Alpha
		if(nChan_dst==4)
		{
			if(nChan_im1!=4) p_dst[3]=255;
			else 		p_dst[3]=p_im1[3];
		}


		//Jump to next pixel
		p_im1+=nChan_im1;
		p_im2+=4;
		p_dst+=nChan_dst;
	}
}
