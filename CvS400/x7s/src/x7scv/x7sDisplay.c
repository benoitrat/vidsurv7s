#include "x7scv.h"
#include "x7sCvLog.h"
#include "highgui.h"

/**
 * @brief Show image using the colorModel fields as the code defined by CvtColor.
 */
void x7sShowImage(const char *name, const IplImage *image) {
	IplImage *tmp;
	int code;

	//Check input image
	if(image==NULL) return;

	//Obtain color code of image.
	X7S_CV_GET_CMODEL(image,code);
	if(code==0) {
		cvShowImage(name,image);
	}
	else {
		tmp = cvCreateImage(cvGetSize(image),image->depth,3);
		if(x7sCvtColor(image,tmp,code)==X7S_CV_RET_OK)
			cvShowImage(name,tmp);
		cvReleaseImage(&tmp);
	}
}

/**
 * @brief Show an image rescaling its min and max between 0 and 255.
 */
void x7sShowImageRescale(const char *name, const IplImage *image) {

	//Set the temporary value to rescale the image
	double max_val,min_val, scale;

	//Create the temporary image
	IplImage* tmp=cvCreateImage(cvGetSize(image),IPL_DEPTH_8U,image->nChannels);

	//Find min and max value
	cvMinMaxLoc(image,&min_val,&max_val,NULL,NULL,NULL);


	//Compute scaling coefficient
	scale=255.0/( max_val-min_val);


	//Rescale to fit IPL_DEPTH_8U images
	cvConvertScale(image, tmp, scale,-min_val);

	//Show Image
	cvShowImage(name,tmp);

	// release the temporary image
	cvReleaseImage(&tmp);
}


/**
 * @brief Show an image rescaling its max to 255.
 */
void x7sShowImageRescaleMax(const char *name, const IplImage *image) {

	//Set the temporary value to rescale the image
	double max_val, scale;

	//Create the temporary image
	IplImage* tmp=cvCreateImage(cvGetSize(image),IPL_DEPTH_8U,image->nChannels);

	//Find min and max value
	max_val = x7sFindMax(image);

	//Compute scaling coefficient
	if(max_val == 0.0) scale =1.0;
	else scale=255.0/max_val;

	//Rescale to fit IPL_DEPTH_8U images
	cvConvertScale(image, tmp, scale,0);

	//Show Image
	cvShowImage(name,tmp);

	// release the temporary image
	cvReleaseImage(&tmp);
}


/**
 * @brief Display the histogram of an Image.
 */
void x7sShowHistogram(const IplImage *im) {

	IplImage* imgHistogram = NULL;
	CvHistogram* hist;
	int bins, hsize[1],i,normalized;
	float max_value, min_value, value;
	float xranges[2], *ranges[1];
	IplImage * planes[1];


	//Check if the image is grey scale
	if((im->depth != IPL_DEPTH_8U) && (im->nChannels != 1)) return;

	//size of the histogram -1D histogram
	bins = 10;
	hsize[0] = bins;

	//max and min value of the histogram
	max_value = 0, min_value = 0;
	//value and normalized value


	//ranges - grayscale 0 to 256
	xranges[0] = 0; xranges[1]= 256;
	ranges[0] = xranges;

	//planes to obtain the histogram, in this case just one
	planes[0] = (IplImage*)im;


	//get the histogram and some info about it
	hist = cvCreateHist( 1, hsize, CV_HIST_ARRAY, ranges,1);
	cvCalcHist( planes, hist, 0, NULL);
	cvGetMinMaxHistValue( hist, &min_value, &max_value,NULL,NULL);
	printf("min: %f, max: %f\n", min_value, max_value);

	//create an 8 bits single channel image to hold the histogram
	//paint it white
	imgHistogram = cvCreateImage(cvSize(bins, 50),8,1);
	cvRectangle(imgHistogram, cvPoint(0,0), cvPoint(256,50), CV_RGB(255,255,255),-1,8,0);

	//draw the histogram :P
	for(i=0; i < bins; i++){
		value = cvQueryHistValue_1D( hist, i);
		normalized = cvRound(value*50.0/max_value);
		cvLine(imgHistogram,cvPoint(i,50), cvPoint(i,50-normalized), CV_RGB(0,0,0),1,8,0);
	}

	//Show inside a windows
	cvNamedWindow("histogram",1);
	cvShowImage( "histogram", imgHistogram );

	cvWaitKey(0);
	return;
}

