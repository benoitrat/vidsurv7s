/**
 *  @file
 *  @brief Contains the class x7scvdbg
 *  @date 05-may-2009
 *  @author Benoit RAT
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */


#ifndef X7SCVLOG_H_
#define X7SCVLOG_H_

#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include "x7scv.h"	//!< Use the X7S_CV_RET_xxx
#include "x7sdef.h"	//!< Use the X7S_LOGLEVEL_xxx

#ifndef X7S_CV_DBG
#define X7S_CV_DBG 1
#endif

//old function
bool X7sCvLog_IsDisplay(int level);
void X7sCvLog_PrintMsg(int level, const char* szFormat, ...);
void X7sCvLog_PrintErr(const char* szFormat, ...);

//new function
bool ix7sCvLog_IsDisplay(int level);
void ix7sCvLog_PrintLevel(const char *funcname,const char *file, int line, int level, const char* szFormat, ...);

#define   X7S_LOG(funcname,lvl,...)      if(ix7sCvLog_IsDisplay(lvl)) { ix7sCvLog_PrintLevel(funcname,__FILE__, __LINE__,lvl,##__VA_ARGS__); }

#define   X7S_PRINT_ERROR(funcname,...)     X7S_LOG(funcname,X7S_LOGLEVEL_LOWEST,##__VA_ARGS__)
#define   X7S_PRINT_WARN(funcname,...)      X7S_LOG(funcname,X7S_LOGLEVEL_LOW,##__VA_ARGS__)
#define   X7S_PRINT_INFO(funcname,...)      X7S_LOG(funcname,X7S_LOGLEVEL_MED,##__VA_ARGS__)
#ifdef DEBUG
#define   X7S_PRINT_DEBUG(funcname,...)		X7S_LOG(funcname,X7S_LOGLEVEL_HIGH,##__VA_ARGS__)
#define   X7S_PRINT_DUMP(funcname,...) 		X7S_LOG(funcname,X7S_LOGLEVEL_HIGHEST,##__VA_ARGS__)
#else
#define X7S_PRINT_DEBUG(funcname,...)
#define X7S_PRINT_DUMP(funcname,...)
#endif

#define X7S_CHECK_WARN(funcName,condition,ret,...) \
		if(!(condition)) \
		{ \
			 X7S_LOG(funcName,X7S_LOGLEVEL_LOW,##__VA_ARGS__); \
			 return ret; \
		}

#define X7S_CHECK_ERROR(funcName,condition,ret,...) \
		if(!(condition)) \
		{ \
			 X7S_LOG(funcName,X7S_LOGLEVEL_LOWEST,##__VA_ARGS__); \
			 return ret; \
		}

#endif /* X7SCVDBG_H_ */
