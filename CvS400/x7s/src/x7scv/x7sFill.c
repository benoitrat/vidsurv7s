#include "x7scv.h"
#define MASKTMPVAL 128

//------------------ Private prototypes.

int x7sFillHoles_1C8U(IplImage *mask);
int x7sFillLineHoles_1C8U(IplImage *mask);
int x7sFillEdges_1C8U(IplImage *edge,const IplImage *mask);

//------------------ Public function (Check arguments)

/**
 * @brief Fill holes in an image.
 *
 * Example of an image line convertion:
 * @code
 *
 *  //original image
 * -----------------------------
 * |......xxxxxxxxxxxxxxxxx....|
 * |......xxxxx...xxxx..xxx....|
 * |......xxxxxxxxxxxxxxxxx....|
 * -----------------------------
 *
 *  //transformed image
 * -----------------------------
 * |......xxxxxxxxxxxxxxxxx....|
 * |......xxxxxxxxxxxxxxxxx....|
 * |......xxxxxxxxxxxxxxxxx....|
 * -----------------------------
 *
 * @endcode
 * with @tt{x}=@ref X7S_CV_TRUEPIX, and @tt{.}=@ref X7S_CV_FALSEPIX
 *
 *
 * @param im An image with 1 channel and IPL_DEPTH_8U.
 */
int x7sFillHoles(IplImage *im) {

	if(im->nChannels==1 && im->depth==IPL_DEPTH_8U) {
		return x7sFillHoles_1C8U(im);
	}
	return X7S_CV_RET_ERR;
}

/**
 * @brief Fill holes on a line
 *
 * Example of an image line convertion:
 * @code
 * //orginal line
 * |......xxxxx...xxxx..xxx....|
 * //converted line
 * |......xxxxxxxxxxxxxxxxx....|
 * @endcode
 *
 * @bug This function might not work on all images.
 * @param im An image with 1 channel and IPL_DEPTH_8U.
 */
int x7sFillLineHoles(IplImage *im) {

	if(im->nChannels==1 && im->depth==IPL_DEPTH_8U) {
		return x7sFillLineHoles_1C8U(im);
	}
	return X7S_CV_RET_ERR;
}


/**
 * @brief Fill an images between two edges.
 *
 * @note This function works only if the edge image is very good.
 * @param edge The image with the edges (obtain previously with a filter like Sobel).
 * @param mask The mask where we want can search and write.
 * @return Images should be both @ref CV_8U1C or it return @ref X7S_CV_RET_ERR.
 */
int x7sFillEdges(IplImage *edge, const IplImage *mask) {

	if(mask->nChannels==1 && mask->depth==IPL_DEPTH_8U  && edge->nChannels==1 && edge->depth==IPL_DEPTH_8U) {
		return x7sFillEdges_1C8U(edge,mask);
	}
	return  X7S_CV_RET_ERR;
}


//------------------ Private function

/**
 * @brief @ref x7sFillHoles() function for @ref CV_8U1C image.
 * @see x7sFillHoles().
 */
int x7sFillHoles_1C8U(IplImage *mask) {
	int h, w;
	bool back = FALSE;
	uchar *m, *p1, *p2, *p3, *mend, *cend;

	m = (uchar*)mask->imageData;
	p1 = m; p2 = m; p3 = m;
	h = mask->height;
	w = mask->width;



	/* Convert the different border equal to zero in border equal to 2 */

	//Top Border
	m = (uchar*)mask->imageData;
	mend = m + w;
	while(m <= mend) {
		if(*m == 0) *m = MASKTMPVAL;
		m++;
	}

	//Right and Left Border
	m = (uchar*)mask->imageData;
	mend = m + w*h - w;
	while(m < mend) {
		m += w - 1; //Last position in the image
		if(*m == 0) *m = MASKTMPVAL; //Right border
		m++;	//Next pixel is in new line at the left border
		if(*m == 0) *m = MASKTMPVAL;	//Left border
	}

	//Bottom Border
	mend += w;
	while(m < mend) {
		if(*m == 0) *m = MASKTMPVAL;
		m++;
	}


	/*Scanning matrix*/
	m = (uchar*)mask->imageData;
	mend = m + w*h -w - 1;	//position p[w-1,h-1]	(last  = p[w,h])
	m = m + w + 1;			//position p[2,2]		(first = p[1,1])
	cend = m + w - 2;		//position p[w-1,2]
	p1++;					//position p[2,1]
	p2 += w;				//position p[2,2]

	while (m < mend) {

		//Forward columns scanning
		if(!back) {
			//Scanning line left-right
			while(m < cend) {
				if (*m==0 && (*p1 == MASKTMPVAL ||  *p2 == MASKTMPVAL)) {
					*m = MASKTMPVAL;
					if(!back && *p1 == 0) back = 1;
				}
				m++;
				p1++;
				p2++;
			}
			//Scanning line right-left
			m--;
			p1--;
			p2++;
			cend = m - w + 2;
			while(m > cend) {
				if (*m == 0 && *p2 == MASKTMPVAL){
					*m = MASKTMPVAL;
					if(!back && *p1 == 0) back = 1;
				}
				m--;
				p1--;
				p2--;
			}
		}
		//Backward columns scanning
		else {
			back = 0;
			//Scanning line left-right
			while(m < cend) {
				if (*m==0 && (*p1 == MASKTMPVAL ||  *p2 == MASKTMPVAL)) {
					*m = MASKTMPVAL;
					if(!back && *p3 == 0) back = 1;
				}
				m++;
				p1++;
				p2++;
				p3++;
			}
			//Scanning line right-left
			m--;
			p2++;
			p3--;
			cend = m - w + 2;
			while(m > cend) {
				if (*m == 0 && *p2 == MASKTMPVAL){
					*m = MASKTMPVAL;
					if(!back && *p3 == 0) back = 1;
				}
				m--;
				p2--;
				p3--;
			}
		}

		/*Forward-Backward column positioning*/
		p1 = m + 1;
		/*if NOT back flag go to next column whith the appropriate elements to test*/
		if(!back) m = p1 + w;
		/*if back flag go to previous column whith the appropriate elements to test*/
		else {
			m = p1 - w;
			p3 = m - w;
		}
		p2 = m - 1;
		cend = m + w - 2;

	} /* end while(m < mend) */

	/*update matrix*/
	m = (uchar*)mask->imageData;
	mend =  m + w*h;
	while (m < mend) {
		if(*m == MASKTMPVAL) *m = 0;
		else *m = X7S_CV_TRUEPIX; /*max;*/
		m++;
	}
	return X7S_CV_RET_OK;
}

/**
 * @brief @ref x7sFillLineHoles() function for @ref CV_8U1C image.
 * @see x7sFillLineHoles().
 */
int x7sFillLineHoles_1C8U(IplImage *mask) {
	uchar *line;
	int count_p=0, count_n=0;
	int x,y;

	for(y=0;y<mask->height;y++) {
		line = (uchar*)(mask->imageData+y*mask->widthStep);
		for(x=5;x<(mask->width-5);x++) {

			if(line[x]==0) {
				count_p=line[x-5]+line[x-4]+line[x-3];
				count_n=line[x+5]+line[x+4]+line[x+3];

				if(count_p==UINT8_MAX*3 && count_n==UINT8_MAX*3) {
					line[x]=UINT8_MAX;
				}
			}

		}

	}
	return X7S_CV_RET_OK;
}




/**
 * @brief @ref x7sFillEdges() function for @ref CV_8U1C image.
 * @see x7sFillEdges().
 */
int x7sFillEdges_1C8U(IplImage *edge,const IplImage *mask)
{

	int x,y,height, width, w_step;
	bool searchE, findE;
	uint8_t *m0, *e0, *lineM, *lineE;

	m0 = (uint8_t*)mask->imageData;
	e0 = (uint8_t*)edge->imageData;

	height = mask->height;
	width = mask->width;
	w_step = mask->widthStep;



	/*Scanning matrix*/
	for(y=0;y<height;y++) {
		lineM = m0+y*w_step;
		lineE = e0+y*w_step;

		searchE=TRUE;
		findE=TRUE;

		//Scan a line from left to right
		for(x=0;x<width; x++) {

			//Search where the mask is okay
			if(lineM[x] == 255) {
				searchE=TRUE;
			}
			else {
				searchE=FALSE;
				findE=FALSE;
			}
			if(searchE) {
				if(findE) {
					if(lineE[x] == 255) lineE[x]=255;
					else lineE[x]=128;
				}

				if(lineE[x] == 255) {
					findE=TRUE;
				}

			}
		} //end for x


		searchE=TRUE;
		findE=TRUE;
		//Scan a line from right to left
		for(x=width-1;x>=0; x--) {
			if(lineM[x] == 255) {
				searchE=TRUE;
			}
			else {
				searchE=FALSE;
				findE=FALSE;
			}
			if(searchE) {

				if(lineE[x] == 255) {
					findE=TRUE;
				}

				if(!findE) lineE[x]=0;
				else {
				//	if(lineE[x] == 255) lineE[x]=255;
				//	else lineE[x]=128;
				}


			}
		}

	} // end for y
	return X7S_CV_RET_OK;
}
