#include "x7scv.h"
#include "x7sCvLog.h"

//------------------ Private prototypes.

int x7sMakeBlobs_1C8U(IplImage *mask);
int x7sMakeBlobs_1C8U_RLE(IplImage *mask);


//------------------ Public function (Check arguments)

/**
 * @brief Compute the blob image
 */
int x7sMakeBlobs(IplImage *im) {

	if (im->nChannels==1 && im->depth==IPL_DEPTH_8U) {
		//return x7sMakeBlobs_1C8U(im);
		return x7sMakeBlobs_1C8U_RLE(im);
	}
	return X7S_CV_RET_ERR;
}

//------------------ Private function

/**
 * @brief Compute the blob image
 */
int x7sMakeBlobs_1C8U_RLE(IplImage *im) {

	int i = 0, j = 0, block = 0, start = 0, num_RLEs = 0, m = 0, row_next = 0, kc = 0;
	int TID, TEQ, cont, ID_max, ID_min, flag_ID, first_ID;
	uchar *line; 		//Pointer on the image
	typedef struct{
		int start, end, row, ID, EQ;
	} sRLE;

	sRLE *list_RLE;


	//Iterate over the whole image
	block = 0;
	start = 0;
	num_RLEs = 0;
	list_RLE = (sRLE *)malloc(5000*sizeof(sRLE));


	for (i=0; i<im->height; i++) {
		line=(uchar*)(im->imageData+i*im->widthStep);

		for (j=0; j<im->width; j++) {

			//If pixel is black
			if (line[j] && block == 0) {
				start = j;
				block = 1;				
			}
			if(block == 1 && (line[j] == 0 || j == im->width-1)){
				block = 0;
				list_RLE[num_RLEs].start = start;
				list_RLE[num_RLEs].end = j;
				list_RLE[num_RLEs].row = i;
				list_RLE[num_RLEs].ID = 0;
				list_RLE[num_RLEs].EQ = 0;

				num_RLEs++;
			}
		}
	}

	row_next = 0;
	m = 1;
	kc = 0;
	for(i = 0; i < num_RLEs; i++){

		if(list_RLE[i].ID  == 0){
			list_RLE[i].ID = m;   //ID <- EQ <- m
			list_RLE[i].EQ = m;
			m++;
		}

		row_next = list_RLE[i].row + 1;

		kc = i + 1;

		while(row_next >= list_RLE[kc].row && kc < num_RLEs){

			if (list_RLE[i].end >= list_RLE[kc].start && list_RLE[i].start <= list_RLE[kc].end){
				if(list_RLE[kc].ID == 0){
					//IDj <- IDi;   EQj <- EQi
					list_RLE[kc].ID = list_RLE[i].ID;
					list_RLE[kc].EQ = list_RLE[i].EQ;
				} 
				else{
					//EQi <- IDj
					list_RLE[i].ID = list_RLE[i].ID;
					list_RLE[i].EQ = list_RLE[kc].EQ;
				}
			}

			kc = kc + 1;

		}
	}

	//Resolving conflict
	for(i = 0; i < num_RLEs; i++){

		if(list_RLE[i].ID != list_RLE[i].EQ){
			TID = list_RLE[i].ID;
			TEQ = list_RLE[i].EQ;
			for(kc = 0; kc < num_RLEs; kc++){

				if (list_RLE[kc].ID == TID){
					list_RLE[kc].ID = TEQ;
				}
				if (list_RLE[kc].EQ == TID){
					list_RLE[kc].EQ = TEQ;							
				}
			}
		}
	}
	for(i = 0; i < num_RLEs; i++){
		list_RLE[i].EQ = list_RLE[i].ID;
	}

	cont = 1;
 	flag_ID = 0;
	while(flag_ID != 1){
		flag_ID = 1;
		first_ID = 0;
		for(i = 0; i < num_RLEs; i++){
			if(list_RLE[i].EQ != 0){
				if (first_ID == 0){
					ID_min = list_RLE[i].ID;
					first_ID = 1;
				}
				else{
					if (ID_min > list_RLE[i].ID){
						ID_min = list_RLE[i].ID;
					}
				}
				flag_ID = 0;
			}
		}
		if(flag_ID != 1){
			for(i = 0; i < num_RLEs; i++){
				if(list_RLE[i].ID == ID_min){
					list_RLE[i].ID = cont;
					list_RLE[i].EQ = 0;
				}		
			}
			cont++;
		}
	}

	cont = 1;
	ID_max = 0;
	for(i = 0; i < num_RLEs; i++){
		if (ID_max < list_RLE[i].ID)
			ID_max = list_RLE[i].ID;
		line = (uchar*)(im->imageData + list_RLE[i].row*im->widthStep);
		for (j = list_RLE[i].start; j < list_RLE[i].end; j++) {
			line[j] = list_RLE[i].ID;
		}

	}



	//Free the two temporary table
	free(list_RLE);


	return ID_max;
}



/**
 * @brief Compute the blob image
 */
int x7sMakeBlobs_1C8U(IplImage *im) {

	int i=0, j=0, x=0, y=0,x_new=0;
	uchar *line; 		//Pointer on the image
	uchar *line_up; 	//Pointer on the temporary line
	uchar *con; 		//Connexion table
	uchar p_prev=0; 	//Value of the previous pixel
	uchar id=0, next_id=1, max_con=255; //Value to merge component


	//Temporary line (keep the pixel value for the previous line )
	//length is (im->width+1) to avoid border condition when we look at line_up[x+1]
	line_up=(uchar*)malloc(im->width+1);
	for (i=0; i<im->width+1; i++) line_up[i] = 0; //Set the temporary line to 0

	//Obtain memory and set it to zero
	con=(uchar*)malloc(max_con+1);
	for (i=0; i<max_con+1; i++) con[i] = 0; //Set the connexion table to 0

	//Itarate over the whole image
	for (y=0; y<im->height; y++) {
		line=(uchar*)(im->imageData+y*im->widthStep);

		for (x=0; x<im->width; x++) {

			//If pixel is black
			if (!line[x]) {
				p_prev = 0; 	//set the previous value for next step
				line_up[x] = 0; //Set upper value for next line
				x_new=0;		//Set
			}

			else {
				//If pixel is white (not a blob)

				//if there is a pixel on the left, take its value
				if (p_prev) {
					id=p_prev;
				}
				//if the pixel on the line above is Setted
				else if (line_up[x]) {
					id=line_up[x];
				}
				//if the pixel+1 on the line above is setted
				else if (line_up[x+1]) {
					id=line_up[x+1];
				}
				//Otherwise it's a new white pixel
				else {
					id=next_id;
					x_new=x;
					con[id]=id;
					next_id++;
#if X7S_CV_DBG
					X7sCvLog_PrintMsg(X7S_LOGLEVEL_HIGH,"x=%3d,y=%3d ", x, y);
					X7sCvLog_PrintMsg(X7S_LOGLEVEL_HIGH,"new white: p=%d (p_up=%d) ID=%d\n", line[x],line_up[x+1], id);
#endif
				}


				//Check if it is connected with the line above somewhere
				if (line_up[x+1] &&  (con[line_up[x+1]] != con[id])) {

					//If A and B can be connected, then go back on the
					//beggining of B (x_new) and do it.
					if(x_new) {
						/* 00000AAA00
						 * 0BBBBxxxxx
						 * xxxxxxxxxx
						 */
#ifdef X7S_CV_DBG
						X7sCvLog_PrintMsg(X7S_LOGLEVEL_HIGH,"x=%3d,y=%3d ", x, y);
						X7sCvLog_PrintMsg(X7S_LOGLEVEL_HIGH,"merge: p=%d (1st_x=%d): ID=%d -> %d, next_id:%d\n", line[x],x_new,id,line_up[x+1],next_id-1);
#endif
						id=line_up[x+1];
						for(i=x_new;i<x;i++) line[i] = id;
						next_id--;
						x_new=0;

					}
					//Otherwise, Prepare merging two blocks on next step
					//with the connexion table => con[B]=A
					else {
						/* AA0000BBB0
						 * 0AA00BBBB0
						 * 0AAAAAAA00
						 */
						con[line_up[x+1]]=con[id];
#if X7S_CV_DBG
						X7sCvLog_PrintMsg(X7S_LOGLEVEL_HIGH,"x=%3d,y=%3d ", x, y);
						X7sCvLog_PrintMsg(X7S_LOGLEVEL_HIGH,"connected: p=%d (1st_x=%d): up_x=%d con[(id=%d)]=%d, next_id:%d\n", line[x],x_new,line_up[x+1], id, con[id],next_id);
#endif
					}
				}


				//Set the value for the next pixel
				line[x]=id;
				//line_up[x]=id;
				p_prev=id;
			}
		} //End of the iteration on the line

		//Copy actual line in old line
		memcpy(line_up,line,im->width);
		p_prev=0;
		x_new=0;
	}

	//Resort the connexion
	//Ex: con[3]=3 con[4]=3 con[5]=4 con[6]=5 => con[3]=3 con[4]=3 con[5]=3 con[6]=3
	for (i=1; i<next_id; i++) {
		con[i] = con[con[i]];
	}
	id = 0;
	for (i=1; i<next_id; i++) {
		if (con[i]>id) {
			id++;
			for (j=i+1; j<next_id; j++)
				if (con[j]==con[i])
					con[j]=id;
			con[i]=id;
		}
	}

#if X7S_CV_DBG
	for (i=0; i<=next_id; i++) X7sCvLog_PrintMsg(X7S_LOGLEVEL_HIGH,"con[%d]=%d ", i, con[i]);
	X7sCvLog_PrintMsg(X7S_LOGLEVEL_HIGH,"\n\n\n");
	//cvShowBlobsImage("win6", im,TRUE);
#endif


	//Make a final rescan to merge the different region.
	for (y=0; y<im->height; y++) {
		line=(uchar*)(im->imageData+y*im->widthStep);
		for (x=0; x<im->width; x++) {
			if(line[x]) {
				line[x]=con[line[x]];
			}
		}
	}

	//Free the two temporary table
	free(con);
	free(line_up);


	return id;
}

