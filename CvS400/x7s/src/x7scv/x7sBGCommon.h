/*
 * x7sBGCommon.h
 *
 *  Created on: 14-may-2009
 *      Author: Andrea
 */

#ifndef X7SBGCOMMON_H_
#define X7SBGCOMMON_H_

#include "x7scv.h"


int x7sSetClusterBGStatModelParams(X7sClusterBGStatModelParams *params);
int x7sSetCBookInterBGStatBGModelParams(X7sCBookInterBGStatModelParams *params);
int x7sSetBCDistBGStatModelParams(X7sBCDistBGStatModelParams *params);
int x7sSetGaussBGStatBGModelParams(CvGaussBGStatModelParams *params);
int x7sSetFGDStatModelParams(CvFGDStatModelParams *params);
int x7sSetCBookBGStatBGModelParams(x7sCBookBGStatModelParams *params);
int x7sSetMIDiffBGStatModelParams(X7sMIDiffBGStatModelParams *params);

#endif /* X7SBGCOMMON_H_ */
