#include "x7scv.h"
#include "x7sCvLog.h"

//------------------ Private prototypes.
double x7sFindMax_8U(const IplImage *im);
double x7sFindMax_16U(const IplImage *im);

//------------------ Public function (Check arguments)

/**
 * @brief Find maximum value of an image on all the channels.
 * @param im The input images,
 * @return the maximum value.
 */
double x7sFindMax(const IplImage *im) {
	switch(im->depth) {
			case IPL_DEPTH_8U: 	return x7sFindMax_8U(im);
			case IPL_DEPTH_16U:	return x7sFindMax_16U(im);
			default:	printf("Not implemented\n"); return 0.0;
	}
}

//------------------ Private function

/**
 * @brief @ref x7sFindMax() function for uint8 depth image.
 * @see x7sFindMax().
 */
double x7sFindMax_8U(const IplImage *im) {

	uchar *ptr,*end;
	uchar ret=0;

	//Check the image type
	if(im->depth != IPL_DEPTH_8U) return -1;

	//Set pointers on 1st pixel and the last+1 pixels
	ptr = (uchar*)im->imageDataOrigin;
	end = ptr + im->imageSize; //Size in byte

	while(ptr < end) {
		if(*ptr > ret) ret=*ptr;	//If it is bigger add change value
		ptr++;						// Go to next pixel value (dont take care about channel)
	}
	return (double)ret;
}


/**
 * @brief @ref x7sFindMax() function for a uint16 depth image.
 * @see x7sFindMax().
 */
double x7sFindMax_16U(const IplImage *im) {

	ushort *ptr,*end;
	ushort ret=0;

	//Check the image type
	if(im->depth != IPL_DEPTH_16U) return -1;

	//Set pointers on 1st pixel and the last+1 pixels
	ptr = (ushort*)im->imageDataOrigin;
	end = ptr + im->width*im->height*im->nChannels;

	while(ptr < end) {
		if(*ptr > ret) ret=*ptr;	//If it is bigger add change value
		ptr++;						// Go to next pixel value (dont take care about channel)
	}
	return (double)ret;
}


