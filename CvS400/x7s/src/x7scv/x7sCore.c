#include "x7scv.h"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Defines/Enums/Includes
//----------------------------------------------------------------------------------------
#include "x7sCvLog.h"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Private Prototypes
//----------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Functions
//----------------------------------------------------------------------------------------

/**
* @brief Create a simple array of byte.
* The size given is the maximum size reserved in the memory,
* however the size of usefull data is given by length.
* @note This function use malloc() and free() because it must be compatible
* with C code.
*/
X7sByteArray* x7sCreateByteArray(int size_max)
{
	X7sByteArray* pByteArray = (X7sByteArray*)malloc(sizeof(X7sByteArray));
	pByteArray->type=X7S_BYTEARRAY_UNKNOWN;
	pByteArray->length=0;
	pByteArray->capacity=0;
	pByteArray->data=NULL;
	if(size_max>0)
	{
		pByteArray->capacity=size_max;
		pByteArray->data = (uint8_t*)calloc(sizeof(uint8_t),pByteArray->capacity);
	}
	return pByteArray;
}


/**
* @brief Release the memory reserved for this buffer.
* @note This function use malloc() and free() because it must be compatible
* with C code.
* @warning if the X7sByteArray::capicity is <=0, then the data is not release.
* @see x7sCreateByteArray().
*/
void x7sReleaseByteArray(X7sByteArray** pByteArray)
{
	X7sByteArray* byteArray=*(pByteArray);
	if(byteArray)
	{
		if(byteArray->capacity<=0) {
			X7S_PRINT_WARN("x7sReleaseNetBuffer()",
					"pByteArray->capacity=%d ,memory is not released",byteArray->capacity);
			return;
		}
		
		free(byteArray->data);
		byteArray->capacity=0;
		
		byteArray->data=NULL;
		byteArray->length=0;
		free(byteArray);
	}
	*(pByteArray)=NULL;
}


/**
 * @brief Copy a simple array of byte.
 * @param src The source array of byte
 * @param dst The destination array (dst->capacity >= src->length)
 * @return @ref X7S_RET_OK if the copy was done, otherwise @ref X7S_RET_ERR.
 */
int x7sCopyByteArray(const X7sByteArray* src,X7sByteArray *dst)
{
	if(src && dst)
	{
		if(dst->capacity >= src->length) {
			dst->type=src->type;
			dst->length=src->length;
			memcpy(dst->data,src->data,src->length);
			return X7S_RET_OK;
		}
		else
		{
			X7S_PRINT_WARN("x7sCopyByteArray()", "src->length (%d) > dst->capacity (%d)",src->length,dst->capacity);
		}
	}
	else
	{
		X7S_PRINT_WARN("x7sCopyByteArray()", "src==0x%xNULL or dst==0x%x",src,dst);
	}
	return X7S_RET_ERR;
}

/**
* @brief Convert a pointer on a IplImage to a byteArray structure.
* @note the return X7sByteArray has the capacity value set to X7sByteArray::capacity=0, which means
* that if this structure is given to x7sReleaseByteArray(), the memory will not be released.
* @param pImg A pointer on a IplImage.
* @return the structure of a byteArray.
*/
X7sByteArray x7sByteArray(IplImage *pImg)
{
	X7sByteArray ba;

	ba.type=X7S_BYTEARRAY_IPLIMAGE;
	ba.data		= (uint8_t*)pImg->imageData;
	ba.length	= pImg->imageSize;
	ba.capacity=0;

	return ba;
}

/**
* @brief Convert a byteArray to a CvMat structure.
* @param pByteArray A pointer on a byte array.
* @return the structure of a CvMat.
*/
CvMat x7sByteArrayToCvMat(X7sByteArray* pByteArray)
{
	return cvMat(1,pByteArray->length,CV_8UC1,pByteArray->data);
}

/**
 *
 * @brief Convert a byteArray to a IplImage structure.
 *
 * @warning No memory is allocated using this function, therefore
 * the returned IplImage does not need any release. Use the
 * x7sReleaseByteArray() function instead.
 *
* @param pByteArray A pointer on a byte array.
* @return the structure of an IplImage.
 */
IplImage x7sByteArrayToIplImage(X7sByteArray* pByteArray)
{
	IplImage im;
	cvInitImageHeader(&im,cvSize(pByteArray->length,1),IPL_DEPTH_8U,0,0,4);
	im.imageData=(char*)pByteArray->data;
	im.imageDataOrigin=(char*)pByteArray->data;
	im.nChannels=0;
	return im;
}

/**
 * @brief Convert a IplImage to a ByteArray structure.
 *
 * @warning No memory is allocated using this function, therefore
 * the returned X7sByteArray does not need any release. Use the
 * cvReleaseImage() function instead.
 *
 * @param pImg A pointer on a IplImage
 * @return the structure of a x7sByteArray
 */
X7sByteArray x7sIplImageToByteArray(const IplImage* pImg)
{
	X7sByteArray ba;
	X7S_FUNCNAME("x7sIplImageToByteArray()");

	ba.capacity=0;
	ba.data=NULL;
	ba.type=X7S_BYTEARRAY_UNKNOWN;
	ba.length=0;

	X7S_CHECK_WARN(funcName,pImg,ba,"pImg is NULL");

	ba.capacity=pImg->imageSize;
	ba.length=pImg->imageSize;
	ba.type=X7S_BYTEARRAY_IPLIMAGE;
	ba.data=(uint8_t*)pImg->imageDataOrigin;

	return ba;
}


/**
* @brief Close the FILE and set the pointer to NULL.
*/
int x7sReleaseFILE(FILE **ppFile)
{
	int ret=0;
	FILE *pFile=*ppFile;
	if(pFile) {
		fflush(pFile);
		ret=fclose(pFile);
	}
	*ppFile=NULL;
	if(ret==0) return X7S_RET_OK;
	else return X7S_RET_ERR;
}


/**
* @brief This function is similar to memcpy except that it perform a memory check.
* @param destination Pointer to the destination array where the content is to be written.
* * The value of destination is automatically place to the next value at the end of the function.
* @param end_buff Pointer to the end of the destination array.
* @param source Pointer to the source of data to be copied, type-casted to a pointer of type void*.
* @param num Number of bytes to write.
* @return @true, when the copy has been done or @false if there is a problem with
* the memory such a buffer overflow.
*/
uint8_t* x7sMemWrite(uint8_t* destination,const uint8_t *end_buff,const void* source, size_t num)
{
	if(destination && source && end_buff)
	{
		if(destination+num<=end_buff)
		{
			memcpy(destination,source,num);
			destination+=num;
			return destination;
		}
	}
	return NULL;
}


/**
* @brief Read from a buffer and copy the value to a structure.
* This function is similar to memcpy except that it perform a memory check.
* @param destination Pointer to the destination array where the content is read.
* The value of destination is automatically place to the next value at the end of the function.
* @param end_buff Pointer to the end of the destination array.
* @param source Pointer to the source of data which are going to be written.
* @param num Number of bytes to read.
* @return @true, when the copy has been done or @false if there is a problem with
* the memory such a buffer overflow.
*/
uint8_t* x7sMemRead(uint8_t* destination,const uint8_t *end_buff,void* source, size_t num)
{
	if(destination && source && end_buff)
	{
		if(destination+num<=end_buff)
		{
			memcpy(source,destination,num);
			destination+=num;
			return destination;
		}
	}
	return NULL;
}

