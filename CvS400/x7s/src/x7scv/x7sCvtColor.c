/**
*  @date 10-feb-2009
*  @author Benoit RAT
*/
#include "stdio.h"
#include "x7scv.h"
#include "x7sCvLog.h"
#include "highgui.h"


enum {R=0,G,B,A};
enum {Y=0,U,V};

uint8_t jpeg_hdr_type[4] ={ 0xFF, 0xD8,0xFF, 0xE0 };


uint32_t colmap_heatDCW[256]={
		0x00000000, 0x01000000, 0x04000000, 0x07000000, 0x0A000000, 0x0D000000, 0x10000000, 0x13000000,
		0x16000000, 0x1A000000, 0x1C000000, 0x20000000, 0x22000000, 0x25000000, 0x28000000, 0x2B000000,
		0x2E000000, 0x31000000, 0x33000000, 0x37000000, 0x39000000, 0x3C000000, 0x40000000, 0x43000000,
		0x45000000, 0x49000000, 0x4C000000, 0x4F000000, 0x51000000, 0x54000000, 0x57000000, 0x5B000000,
		0x5E000000, 0x60000000, 0x63000000, 0x66000000, 0x69000000, 0x6C000000, 0x6F000000, 0x71000000,
		0x75000000, 0x78000000, 0x7A000000, 0x7E000000, 0x83000000, 0x89000000, 0x90000000, 0x98000000,
		0x9F000000, 0xA6000000, 0xAD000000, 0xB3000000, 0xBB000000, 0xC1000000, 0xC9000000, 0xD0000000,
		0xD7000000, 0xDE000000, 0xE5000000, 0xEC000000, 0xF3000000, 0xFA000000, 0xFF020000, 0xFF070000,
		0xFF0B0000, 0xFF110000, 0xFF150000, 0xFF1B0000, 0xFF1F0000, 0xFF240000, 0xFF290000, 0xFF2E0000,
		0xFF330000, 0xFF380000, 0xFF3D0000, 0xFF420000, 0xFF470000, 0xFF4B0000, 0xFF500000, 0xFF550000,
		0xFF5A0000, 0xFF5F0000, 0xFF640000, 0xFF690000, 0xFF6E0000, 0xFF720000, 0xFF770000, 0xFF7D0000,
		0xFF820000, 0xFF870000, 0xFF8B0000, 0xFF910000, 0xFF950000, 0xFF9A0000, 0xFFA00000, 0xFFA50000,
		0xFFAA0000, 0xFFAF0000, 0xFFB40000, 0xFFB90000, 0xFFBE0000, 0xFFC30000, 0xFFC80000, 0xFFCD0000,
		0xFFD20000, 0xFFD70000, 0xFFDC0000, 0xFFE10000, 0xFFE60000, 0xFFEB0000, 0xFFF00000, 0xFFF50000,
		0xFFFA0000, 0xFFFE0000, 0xF7FF0700, 0xEFFF1000, 0xE7FF1700, 0xDEFF1F00, 0xD7FF2700, 0xCFFF2F00,
		0xC6FF3700, 0xBEFF3F00, 0xB6FF4700, 0xAEFF4E00, 0xA6FF5600, 0x9EFF5E00, 0x95FF6500, 0x8EFF6D00,
		0x85FF7600, 0x7EFF7D00, 0x7BFF7F00, 0x77FF8400, 0x74FF8700, 0x6FFF8B00, 0x6BFF8F00, 0x68FF9200,
		0x64FF9600, 0x60FF9900, 0x5CFF9D00, 0x59FFA100, 0x55FFA500, 0x51FFA800, 0x4DFFAC00, 0x49FFAF00,
		0x45FFB300, 0x41FFB700, 0x3EFFBB00, 0x3AFFBE00, 0x37FFC100, 0x33FFC500, 0x2EFFC900, 0x2BFFCD00,
		0x27FFD000, 0x23FFD400, 0x1FFFD800, 0x1CFFDB00, 0x17FFDF00, 0x14FFE300, 0x10FFE600, 0x0CFFEA00,
		0x09FFEE00, 0x05FFF100, 0x01FFF500, 0x00FDF600, 0x00FAF700, 0x00F8F600, 0x00F5F600, 0x00F3F700,
		0x00F1F700, 0x00EEF700, 0x00EBF800, 0x00E9F700, 0x00E6F800, 0x00E3F800, 0x00E1F800, 0x00DFF800,
		0x00DCF900, 0x00DAF900, 0x00D8F900, 0x00D5FA00, 0x00D2FA00, 0x00CFFA00, 0x00CDFA00, 0x00CAF900,
		0x00C8FA00, 0x00C5FB00, 0x00C3FB00, 0x00C1FB00, 0x00BEFB00, 0x00BCFB00, 0x00B9FB00, 0x00B7FB00,
		0x00B4FB00, 0x00B1FC00, 0x00AFFC00, 0x00ADFC00, 0x00ABFC00, 0x00A8FD00, 0x00A6FD00, 0x00A3FD00,
		0x00A1FD00, 0x009EFE00, 0x009CFE00, 0x0099FE00, 0x0097FD00, 0x0094FE00, 0x0093FE00, 0x0090FE00,
		0x008EFF00, 0x008AFE00, 0x0088FF00, 0x0086FF00, 0x0082FE00, 0x0180FE00, 0x017CFE00, 0x0179FE00,
		0x0276FD00, 0x0273FD00, 0x0270FC00, 0x026DFC00, 0x0269FB00, 0x0266FA00, 0x0363FB00, 0x0360FA00,
		0x035DFA00, 0x045AF900, 0x0456F900, 0x0553F800, 0x0550F800, 0x044DF800, 0x064AF700, 0x0547F700,
		0x0545F600, 0x0542F600, 0x053FF600, 0x063DF600, 0x063BF500, 0x0638F500, 0x0735F400, 0x0734F400,
		0x0731F400, 0x082EF400, 0x072CF400, 0x072AF300, 0x0827F300, 0x0825F200, 0x0922F200, 0x081FF100,
		0x091DF200, 0x091BF100, 0x0A18F100, 0x0A15F000, 0x0A13F000, 0x0A10F000, 0x0A0EEF00, 0x0A0CEF00
};


uint32_t colmap_heatDW[256]={
		0x00000000, 0x00000000, 0x00000000, 0x00000300, 0x00000500, 0x00000600, 0x00000900, 0x00000C00,
		0x00000E00, 0x00001000, 0x00001200, 0x00001400, 0x00001700, 0x00001800, 0x00001B00, 0x00001D00,
		0x00002000, 0x00002100, 0x00002300, 0x00002600, 0x00002800, 0x00002B00, 0x00002C00, 0x00002F00,
		0x00003100, 0x00003300, 0x00003500, 0x00003800, 0x00003A00, 0x00003C00, 0x00003E00, 0x00004100,
		0x00004300, 0x00004500, 0x00004800, 0x00004900, 0x00004C00, 0x00004E00, 0x00005000, 0x00005300,
		0x00005500, 0x00005700, 0x00005900, 0x00005C00, 0x00005D00, 0x00006000, 0x00006100, 0x00006300,
		0x00006500, 0x00006700, 0x00006800, 0x00006A00, 0x00006C00, 0x00006E00, 0x00007000, 0x00007100,
		0x00007300, 0x00007500, 0x00007600, 0x00007800, 0x00007A00, 0x00007C00, 0x00007E00, 0x00008000,
		0x00008100, 0x00008300, 0x00008500, 0x00008800, 0x00008900, 0x00008A00, 0x00008D00, 0x00008F00,
		0x00009000, 0x00009200, 0x00009400, 0x00009600, 0x00009800, 0x00009A00, 0x00009B00, 0x00009D00,
		0x00009F00, 0x0000A000, 0x0000A200, 0x0000A400, 0x0000A600, 0x0000A800, 0x0000AA00, 0x0000AC00,
		0x0000AD00, 0x0000AE00, 0x0000B100, 0x0000B200, 0x0000B500, 0x0000B600, 0x0000B800, 0x0000BA00,
		0x0001BA00, 0x0002BB00, 0x0104BC00, 0x0106BD00, 0x0107BE00, 0x0108BE00, 0x0209BF00, 0x020BC000,
		0x020DC100, 0x020EC200, 0x0310C200, 0x0411C300, 0x0312C400, 0x0413C500, 0x0415C600, 0x0417C600,
		0x0418C700, 0x051AC800, 0x051BC900, 0x061CCA00, 0x061DCB00, 0x061FCC00, 0x0620CC00, 0x0722CC00,
		0x0723CD00, 0x0725CE00, 0x0826CF00, 0x0828D000, 0x0829D100, 0x082AD200, 0x082BD200, 0x092DD300,
		0x092ED400, 0x0930D400, 0x0A31D500, 0x0A33D600, 0x0A34D700, 0x0A36D700, 0x0B37D800, 0x0C38D900,
		0x0B3ADA00, 0x0C3CDB00, 0x0C3CDB00, 0x0C3EDC00, 0x0D3FDD00, 0x0D42DE00, 0x0D44DF00, 0x0E46E100,
		0x0E47E100, 0x0F49E200, 0x0F4AE400, 0x0F4CE500, 0x0F4EE500, 0x1050E600, 0x1052E700, 0x1054E800,
		0x1156E900, 0x1258EA00, 0x125AEB00, 0x125BEC00, 0x125DEE00, 0x125FEE00, 0x1361F000, 0x1362F100,
		0x1364F200, 0x1566F300, 0x1568F400, 0x156AF500, 0x156CF500, 0x156DF700, 0x166FF800, 0x1671F900,
		0x1773FA00, 0x1774FB00, 0x1777FB00, 0x1778FC00, 0x187AFE00, 0x187CFF00, 0x197EFF00, 0x1880FE00,
		0x1882FE00, 0x1883FE00, 0x1884FE00, 0x1887FF00, 0x1888FE00, 0x188AFE00, 0x188BFE00, 0x178EFE00,
		0x178FFE00, 0x1791FE00, 0x1893FD00, 0x1794FD00, 0x1897FD00, 0x1798FE00, 0x179AFD00, 0x179CFE00,
		0x179EFD00, 0x179FFD00, 0x17A0FD00, 0x16A3FC00, 0x17A4FD00, 0x16A6FD00, 0x16A8FC00, 0x17AAFD00,
		0x16ABFD00, 0x16ADFC00, 0x16AEFC00, 0x16B1FC00, 0x16B2FC00, 0x16B4FC00, 0x16B5FC00, 0x16B9FC00,
		0x15BBFB00, 0x16BDFB00, 0x16C0FB00, 0x16C3FA00, 0x15C5FB00, 0x15C8FA00, 0x15CBFA00, 0x14CEFA00,
		0x15D0FA00, 0x14D3F900, 0x14D5FA00, 0x14D7F900, 0x14DAFA00, 0x13DDF900, 0x14E0F900, 0x14E3F900,
		0x14E5F800, 0x14E7F900, 0x13EAF800, 0x13EDF900, 0x14EFF800, 0x1AEFF800, 0x21F0F800, 0x28F0F900,
		0x30F0F800, 0x36F0F900, 0x3EF1F900, 0x44F1F800, 0x4BF1F800, 0x52F2F800, 0x59F2F800, 0x60F2F900,
		0x66F3F900, 0x6DF3F800, 0x75F3F800, 0x7CF4F900, 0x83F3F900, 0x8AF4F900, 0x92F5F800, 0x99F5F900,
		0xA1F5F900, 0xA8F5F900, 0xAFF5F900, 0xB6F6F800, 0xBEF6F900, 0xC6F6F900, 0xCDF7F900, 0xD4F7F900
};

uint32_t colmap_heatCW[256]={
		0x01000000, 0x01000000, 0x05000400, 0x09000800, 0x0D000D00, 0x12001100, 0x16001500, 0x1A001900,
		0x1E001D00, 0x22002200, 0x26002600, 0x2B002A00, 0x2F002E00, 0x33003200, 0x37003700, 0x3B003B00,
		0x40004000, 0x44004300, 0x48004700, 0x4C004C00, 0x51005000, 0x54005400, 0x59005800, 0x5D005D00,
		0x61006100, 0x66006500, 0x6A006900, 0x6D006E00, 0x72007200, 0x76007600, 0x7A007B00, 0x7E007E00,
		0x81008100, 0x7F008400, 0x7E008500, 0x7C008600, 0x7B008700, 0x7A008900, 0x78008B00, 0x76018C00,
		0x74018E00, 0x73008F00, 0x71019100, 0x6F009200, 0x6E019300, 0x6D009500, 0x6B009700, 0x69019800,
		0x68009A00, 0x66009B00, 0x65019D00, 0x63009E00, 0x6101A000, 0x6000A100, 0x5F01A300, 0x5D01A400,
		0x5B00A600, 0x5901A700, 0x5801A900, 0x5601AA00, 0x5501AC00, 0x5301AD00, 0x5101AE00, 0x5001B000,
		0x4F01B100, 0x4D01B300, 0x4B01B400, 0x4901B600, 0x4801B800, 0x4701B800, 0x4501BB00, 0x4301BC00,
		0x4201BE00, 0x4001BE00, 0x3E01C000, 0x3D01C200, 0x3B01C300, 0x3901C500, 0x3801C600, 0x3701C700,
		0x3501C900, 0x3301CB00, 0x3201CC00, 0x3101CE00, 0x2F01CF00, 0x2D01D100, 0x2C01D200, 0x2A02D400,
		0x2801D500, 0x2702D600, 0x2501D800, 0x2302D900, 0x2202DB00, 0x2102DD00, 0x1F01DE00, 0x1D02E000,
		0x1B01E100, 0x1A01E200, 0x1801E400, 0x1701E500, 0x1502E700, 0x1401E900, 0x1201EA00, 0x1102EB00,
		0x0F02EC00, 0x0D02EF00, 0x0C02EF00, 0x0A01F100, 0x0902F300, 0x0702F500, 0x0502F600, 0x0302F700,
		0x0202F900, 0x0105F900, 0x0208F900, 0x020CFA00, 0x0210FA00, 0x0114FA00, 0x0218FA00, 0x021CFA00,
		0x0220FA00, 0x0124FA00, 0x0127FB00, 0x022BFB00, 0x022EFB00, 0x0133FB00, 0x0137FB00, 0x013AFB00,
		0x023EFC00, 0x0141FC00, 0x0145FC00, 0x0147FC00, 0x014AFC00, 0x014DFC00, 0x0150FC00, 0x0152FD00,
		0x0155FD00, 0x0058FD00, 0x005AFD00, 0x015DFD00, 0x005FFD00, 0x0062FE00, 0x0165FE00, 0x0067FE00,
		0x006AFE00, 0x006DFE00, 0x016FFE00, 0x0072FE00, 0x0075FE00, 0x0078FE00, 0x007AFF00, 0x007DFE00,
		0x0180FE00, 0x0082FF00, 0x0085FF00, 0x0087FF00, 0x008AFF00, 0x008EFF00, 0x0090FF00, 0x0093FF00,
		0x0096FF00, 0x0098FF00, 0x009BFF00, 0x009EFF00, 0x00A0FF00, 0x00A3FF00, 0x00A6FF00, 0x00A9FF00,
		0x00ACFF00, 0x00AEFF00, 0x00B2FF00, 0x00B4FF00, 0x00B6FF00, 0x00B9FF00, 0x00BDFF00, 0x00BFFF00,
		0x00C2FF00, 0x00C3FF00, 0x00C6FF00, 0x00C7FF00, 0x00CAFF00, 0x00CCFF00, 0x00CDFF00, 0x00CFFF00,
		0x00D1FF00, 0x00D3FF00, 0x00D4FF00, 0x00D7FF00, 0x00D9FF00, 0x00DAFF00, 0x00DCFF00, 0x00DEFF00,
		0x00E0FF00, 0x00E2FF00, 0x00E4FF00, 0x00E5FF00, 0x00E8FF00, 0x00E9FF00, 0x00EBFF00, 0x00EDFF00,
		0x00EFFF00, 0x00F1FF00, 0x00F3FF00, 0x00F5FF00, 0x00F7FF00, 0x00F8FF00, 0x00FBFF00, 0x00FCFF00,
		0x00FEFF00, 0x03FFFF00, 0x08FFFF00, 0x0EFFFF00, 0x13FFFF00, 0x19FFFF00, 0x1EFFFF00, 0x23FFFF00,
		0x29FFFF00, 0x2FFFFF00, 0x34FFFF00, 0x3AFFFF00, 0x3FFFFF00, 0x45FFFF00, 0x4AFFFF00, 0x50FFFF00,
		0x55FFFF00, 0x5BFFFF00, 0x60FFFF00, 0x66FFFF00, 0x6BFFFF00, 0x71FFFF00, 0x76FFFF00, 0x7BFFFF00,
		0x81FFFF00, 0x87FFFF00, 0x8CFFFF00, 0x92FFFF00, 0x97FFFF00, 0x9CFFFF00, 0xA2FFFF00, 0xA8FFFF00,
		0xADFFFF00, 0xB2FFFF00, 0xB7FFFF00, 0xBDFFFF00, 0xC2FFFF00, 0xC9FFFF00, 0xCEFFFF00, 0xD3FFFF00,
		0xD9FFFF00, 0xDEFFFF00, 0xE3FFFF00, 0xE9FFFF00, 0xEFFFFF00, 0xF4FFFF00, 0xFAFFFF00, 0xFFFFFF00
};


//------------------ Private prototypes.

uint8_t clipByte_i32(int val);

//For YUV422
void YUV2pRGB(uint8_t y, uint8_t u, uint8_t v,uint8_t* p_rgb);
void YUV2pBGR(uint8_t y, uint8_t u, uint8_t v,uint8_t* p_bgr);
void pBGR2pYUV(uint8_t* p_bgr, uint8_t* p_yuv);
void pRGB2pYUV(uint8_t* p_rgb, uint8_t* p_yuv);
int x7sCvtYUV422toRGB(const IplImage *src, IplImage *dst, bool is_rgb);
int x7sCvtRGBtoYUV422(const IplImage *src, IplImage *dst, bool is_rgb);
int x7sCvtYUV4222YUV(const IplImage *src, IplImage *dst);
int x7sCvtYUV2YUV422(const IplImage *src, IplImage *dst);



//For Hue/RGB
int x7sCvtH2BGR(const IplImage *src, IplImage *dst,bool is_nonzeros, bool is_legend);
CvScalar x7sCvtHue2BGRScalar(int i, int max, int mod);
CvScalar x7sCvtHue2RGBScalar(int i, int max, int mod);
int x7sCvtHeat2BGR(const IplImage *src, IplImage *dst,int type);



int x7sDecode2BGR(const IplImage *src, IplImage **pDst, int is_rgb, int realloc);
int x7sDecode2BGRA(const IplImage *src, IplImage **pDst, int is_rgba, int realloc);

//For JPEG.
#undef X7S_CV_JPEG
#ifdef X7S_CV_JPEG
#include <jpeglib.h>
#include <jerror.h>
int x7sLibJPEG2BGR(const IplImage *src, IplImage **pDst, int is_rgb, int realloc);
int x7sLibJPEG2RGBA(const IplImage *src, IplImage **pDst, int is_rgba, int realloc);
#define x7sCvtJPEG2BGR x7sLibJPEG2BGR
#define x7sCvtJPEG2BGRA x7sLibJPEG2RGBA
#else
#define x7sCvtJPEG2BGR x7sDecode2BGR
#define x7sCvtJPEG2BGRA x7sDecode2BGRA
#endif



int x7sCvtBGRA2BGRA255(const IplImage *src, IplImage *dst);


//For Line-RLE.
int x7sCvtLRLE2Grey(const IplImage *src, IplImage *dst);

//------------------ Public function (Check arguments)

/**
* @brief Convert an image to a specific color space.
* @note This function is the same as in openCV with the conversion from/to YUV422 added.
*
* The YUV422 color space is a special color space used essentially in hardware.
* The image contains 2 channels and are order the following way.
*
* @code
* | U Y_0 | V Y_1 | U Y_0 | V Y_1 | ...
* @endcode
*
* As two luminances (Y_0, Y_1) share the same chrominance (U, V):
* pixels are generally proceed 2-by-2.
*
*
* @see @cv{CvtColor}
* @return @ref X7S_RET_ERR if there was an error, @ref X7S_RET_OK otherwise.
*/
int x7sCvtColor(const IplImage *src, IplImage *dst, int code) {

	CV_FUNCNAME("x7sCvtColor()");

	if(0 <= code && code < CV_COLORCVT_MAX) {
		cvCvtColor(src,dst,code);
		return X7S_CV_RET_OK;
	}
	else {
		switch(code) {
		case X7S_CV_CVTCOPY:
			cvCopy(src,dst,NULL);
			return X7S_CV_RET_OK;
		case X7S_CV_YUV4222RGB:
		case X7S_CV_YUV4222BGR:
			return x7sCvtYUV422toRGB(src,dst,code==X7S_CV_YUV4222RGB);
			break;
		case X7S_CV_RGB2YUV422:
		case X7S_CV_BGR2YUV422:
			return x7sCvtRGBtoYUV422(src,dst,code==X7S_CV_RGB2YUV422);
			break;
		case X7S_CV_YUV4222YUV:
			return x7sCvtYUV4222YUV(src,dst);
			break;
		case X7S_CV_YUV2YUV422:
			return x7sCvtYUV2YUV422(src,dst);
		case X7S_CV_HEAT2BGR:
		case X7S_CV_HEATC2BGR:
		case X7S_CV_HEATW2BGR:
			return x7sCvtHeat2BGR(src,dst,code);
		case X7S_CV_H2BGR:
		case X7S_CV_H2BGRLEGEND:
			return x7sCvtH2BGR(src,dst,0,code==X7S_CV_H2BGRLEGEND);
		case X7S_CV_HNZ2BGR:
		case X7S_CV_HNZ2BGRLEGEND:
			return x7sCvtH2BGR(src,dst,1,code==X7S_CV_HNZ2BGRLEGEND);
			// Decode functions
		case X7S_CV_JPEG2BGR:
		case X7S_CV_JPEG2RGB:
			return x7sCvtJPEG2BGR(src,&dst,code==X7S_CV_JPEG2RGB,FALSE);
		case X7S_CV_JPEG2RGBA:
		case X7S_CV_JPEG2BGRA:
			return x7sCvtJPEG2BGRA(src,&dst,code==X7S_CV_JPEG2RGBA,FALSE);
		case X7S_CV_LRLE2GREY:
			return x7sCvtLRLE2Grey(src,dst);
		case X7S_CV_BGRA2BGRA255:
			return x7sCvtBGRA2BGRA255(src,dst);
		default:
			X7S_PRINT_ERROR(cvFuncName,"code %d is not implemented",code);
			return X7S_CV_RET_ERR;
			break;
		}
	}
	return X7S_CV_RET_ERR;
}

/**
* @brief Create the destination image for a color convertion.
*
* This image does not copy or convert the source, you need to use
* x7sCvtColor() to perform this operation.
*
* @param src The source image (to obtain size of destination)
* @param code The code of color convertion (to know how many channel we need)
* @return An IplImage with the good format for color convertion. If the code is
* unkown it returns @NULL.
*/
IplImage* x7sCreateCvtImage(const IplImage *src, int code) {

	CV_FUNCNAME("x7sCreateCvtImage()");

	int nChannels_dst;

	//Obtain the number of channel from source and dest
	if(code==X7S_CV_CVTCOPY) nChannels_dst=src->nChannels;
	else nChannels_dst=x7sCvtColorGetChannel(code,TRUE);

	//Create the image if channels is >=0.
	if(nChannels_dst>=0) {
		return cvCreateImage(cvGetSize(src),src->depth,nChannels_dst);
	}
	else {
		X7S_PRINT_ERROR(cvFuncName,"Could not create a destination image for code %d",code);
		return NULL;
	}
}

/**
* @brief Obtain the number of channel used for a color convertion.
* @param code The color convertion code. @see X7S_CV_xxx
* @param is_dst	if @TRUE, the function return the number of channel for the destination image,
*  otherwise it returns the number of channels for the source image.
* @return the number of channels for src or dst image.
*/
int x7sCvtColorGetChannel(int code, bool is_dst)
{
	if(is_dst)
	{
		switch(code) {
		case CV_BGR2GRAY:
		case CV_RGB2GRAY:
		case CV_BGRA2GRAY:
		case CV_RGBA2GRAY:
		case X7S_CV_LRLE2GREY:
			return 1;
		case X7S_CV_BGR2YUV422:
		case X7S_CV_RGB2YUV422:
		case X7S_CV_YUV2YUV422:
			return 2;
		case X7S_CV_YUV4222BGR:
		case X7S_CV_YUV4222RGB:
		case X7S_CV_YUV4222YUV:
		case X7S_CV_HEAT2BGR:
		case X7S_CV_HEATC2BGR:
		case X7S_CV_HEATW2BGR:
		case X7S_CV_H2BGR:
		case X7S_CV_H2BGRLEGEND:
		case X7S_CV_HNZ2BGR:
		case X7S_CV_HNZ2BGRLEGEND:
		case X7S_CV_JPEG2BGR:
		case X7S_CV_JPEG2RGB:
		case CV_GRAY2BGR:
		case CV_BGRA2BGR:
		case CV_RGBA2BGR:
		case CV_BGR2RGB:
		case CV_BGR2XYZ:
		case CV_RGB2XYZ:
		case CV_XYZ2BGR:
		case CV_XYZ2RGB:
		case CV_BGR2YCrCb:
		case CV_RGB2YCrCb:
		case CV_YCrCb2BGR:
		case CV_YCrCb2RGB:
		case CV_BGR2HSV:
		case CV_RGB2HSV:
		case CV_BGR2Lab:
		case CV_RGB2Lab:
		case CV_BayerBG2BGR:
		case CV_BayerGB2BGR:
		case CV_BayerRG2BGR:
		case CV_BayerGR2BGR:
		case CV_BGR2Luv:
		case CV_RGB2Luv:
		case CV_BGR2HLS:
		case CV_RGB2HLS:
		case CV_HSV2BGR:
		case CV_HSV2RGB:
		case CV_Lab2BGR:
		case CV_Lab2RGB:
		case CV_Luv2BGR:
		case CV_Luv2RGB:
		case CV_HLS2BGR:
		case CV_HLS2RGB:
			return 3;
		case X7S_CV_JPEG2BGRA:
		case X7S_CV_JPEG2RGBA:
		case CV_BGR2BGRA:
		case CV_BGR2RGBA:
			return 4;
		default:
			return -1;
		}
	}
	else {
		switch(code) {
		case X7S_CV_JPEG2BGR:
		case X7S_CV_JPEG2RGB:
		case X7S_CV_JPEG2BGRA:
		case X7S_CV_JPEG2RGBA:
		case X7S_CV_LRLE2GREY:
			return 0;
		case CV_GRAY2BGR:
		case X7S_CV_H2BGR:
		case X7S_CV_H2BGRLEGEND:
		case X7S_CV_HNZ2BGR:
		case X7S_CV_HNZ2BGRLEGEND:
		case X7S_CV_HEAT2BGR:
		case X7S_CV_HEATC2BGR:
		case X7S_CV_HEATW2BGR:
			return 1;
		case X7S_CV_YUV4222RGB:
		case X7S_CV_YUV4222BGR:
		case X7S_CV_YUV4222YUV:
			return 2;
		case X7S_CV_BGR2YUV422:
		case X7S_CV_RGB2YUV422:
		case X7S_CV_YUV2YUV422:
		case CV_BGR2BGRA:
		case CV_BGR2RGBA:
		case CV_BGR2GRAY:
		case CV_BGR2RGB:
		case CV_BGR2XYZ:
		case CV_RGB2XYZ:
		case CV_XYZ2BGR:
		case CV_XYZ2RGB:
		case CV_BGR2YCrCb:
		case CV_RGB2YCrCb:
		case CV_YCrCb2BGR:
		case CV_YCrCb2RGB:
		case CV_BGR2HSV:
		case CV_RGB2HSV:
		case CV_BGR2Lab:
		case CV_RGB2Lab:
		case CV_BGR2Luv:
		case CV_RGB2Luv:
		case CV_BGR2HLS:
		case CV_RGB2HLS:
		case CV_HSV2BGR:
		case CV_HSV2RGB:
		case CV_Lab2BGR:
		case CV_Lab2RGB:
		case CV_Luv2BGR:
		case CV_Luv2RGB:
		case CV_HLS2BGR:
		case CV_HLS2RGB:
			return 3;
		default:
			return -1;
		}
	}
}

int x7sReallocImage(CvSize size, int depth, int channels, IplImage **pDst)
{
	CV_FUNCNAME("x7sReallocImage()");

	IplImage *dst=pDst[0];

	//If image size, depth or nChannels is different from the one given
	if(dst && !(X7S_ARE_SIZESEQ(cvGetSize(dst),size) && dst->depth==depth && dst->nChannels == channels)) cvReleaseImage(&dst);

	//If dst was NULL or is NULL now create it.
	if(dst==NULL)  dst = cvCreateImage(size,depth,channels);

	//Set the pointer of pointer
	pDst[0]=dst;

	//Check that the dst image has been created
	X7S_CHECK_WARN(cvFuncName,dst,X7S_RET_ERR,"Destination could not be created");

	return X7S_RET_OK;
}

/**
* @brief Create or Realloc an image given a specific color convertion.
* @param src	The source image
* @param pDst 	A pointer on the destination image.
* The dst can be reallocated if it does not have the correct number of channel or size.
* It can be also allocated if the dst point to a NULL value.
* @param code The color conversion code (even if the image is not converted).
* @return @ref X7S_RET_ERR if there was an error, @ref X7S_RET_OK otherwise.
* @see x7sCreateCvtImage(), x7sCvtColorGetChannel().
*/
int x7sReallocColorImage(const IplImage *src, IplImage **pDst, int code)
{
	CV_FUNCNAME("x7sCvtColorRealloc()");

	int nChannels_src;
	int nChannels_dst;
	IplImage *dst;

	//Obtain the number of channel from source and dest
	if(code==X7S_CV_CVTCOPY)
	{
		nChannels_dst=src->nChannels;
		nChannels_src=src->nChannels;

	}
	else {
		nChannels_src=x7sCvtColorGetChannel(code,FALSE);
		nChannels_dst=x7sCvtColorGetChannel(code,TRUE);
	}

	if(nChannels_src<0 || nChannels_dst <0) {
		X7S_PRINT_ERROR(cvFuncName,"Number of channels is unknown for code %d",code);
		return X7S_CV_RET_ERR;
	}

	dst=pDst[0];

	//If the source is an encoded format
	if(nChannels_src==0 && src->height<=1)
	{
		//Like JPEG source.
		if(code==X7S_CV_JPEG2BGRA || code==X7S_CV_JPEG2RGBA )
		{
			return x7sCvtJPEG2BGRA(src,pDst,(code==X7S_CV_JPEG2RGBA),TRUE);
		}
		X7S_PRINT_WARN(cvFuncName,"Not implemented for code %d",code);
		return X7S_CV_RET_ERR;
	}
	else 			//Otherwise check the size
	{
		//When both image have fixed size (nChannel >0 )
		if(nChannels_dst>0)
		{
			//If image size is different
			if(dst && !X7S_ACHECK_IMG8U(src,dst,nChannels_src,nChannels_dst)) cvReleaseImage(&dst);

			//If dst was NULL or is NULL now create it.
			if(dst==NULL)  dst = cvCreateImage(cvGetSize(src),IPL_DEPTH_8U,nChannels_dst);

		}
		else {
			X7S_PRINT_WARN(cvFuncName,"Not implemented for nChannels_dst=0");
		}

	}

	//And finally convert source in dest
	pDst[0]=dst;
	return X7S_RET_OK;
}


/**
* @brief Create/Realloc an image given a specific color conversion and convert it to this color.
* @param src	The source image.
* @param pDst 	A pointer on the destination image.
* The dst can be reallocated if it does not have the correct number of channel or size.
* It can be also allocated if the dst point to a NULL value.
* @param code The color conversion code
* @return @ref X7S_RET_ERR if there was an error, @ref X7S_RET_OK otherwise.
* @see x7sCvtColor(), x7sColorRealloc(), x7sCreateCvtImage(), x7sCvtColorGetChannel().
*/
int x7sCvtColorRealloc(const IplImage *src, IplImage **pDst, int code)
{
	int ret=X7S_RET_ERR;

	ret= x7sReallocColorImage(src,pDst,code);
	if(ret==X7S_RET_OK) x7sCvtColor(src,pDst[0],code);

	return ret;
}




//------------------ Private function

//== YUV422 <-> BGR/YUV ===============================================================================

/**
* @brief Convert an image in YUV 4:2:2 to BGR (OpenCV).
*
* Pixel are ordered 2 by 2 as:
* @code
* | U Y_0 V Y_1 | U Y_0 V Y_1 | ...
* @endcode
* @see YCrCb2pBGR.
*/
int x7sCvtYUV422toRGB(const IplImage *src, IplImage *dst, bool is_rgb) {
	uint8_t *p_rgb, *p_uyvy,*end;

	//Check size or input image
	if(!X7S_ACHECK_IMG8U(src,dst,2,3)) {
		printf("x7sCvtYUV422toRGB: Images are not compatible\n");
		return FALSE;
	}

	//Set the pointers.
	p_rgb = (uint8_t*)dst->imageData;
	p_uyvy = (uint8_t*)src->imageData;
	end = p_rgb + dst->imageSize; //3 Channels for RGB


	if(is_rgb) {
		//loop by 2 pixels
		while(p_rgb<end) {

			YUV2pRGB(p_uyvy[1],p_uyvy[0],p_uyvy[2], p_rgb);  		//Convert the 1st pixel
			YUV2pRGB(p_uyvy[3],p_uyvy[0],p_uyvy[2], p_rgb+3);		//Convert the 2nd pixel

			//Jump to the next pair of pixel.
			p_uyvy+=4;
			p_rgb+=6;
		}
	}
	else {
		//loop by 2 pixels
		while(p_rgb<end) {

			YUV2pBGR(p_uyvy[1],p_uyvy[0],p_uyvy[2], p_rgb);  		//Convert the 1st pixel
			YUV2pBGR(p_uyvy[3],p_uyvy[0],p_uyvy[2], p_rgb+3);		//Convert the 2nd pixel

			//Jump to the next pair of pixel.
			p_uyvy+=4;
			p_rgb+=6;
		}
	}
	return TRUE;
}

/**
* @brief Convert an image in YUV 4:2:2 to BGR (OpenCV).
*
* Pixel are ordered 2 by 2 as:
* @code
* | U Y_0 V Y_1 | U Y_0 V Y_1 | ...
* @endcode
* @see YCrCb2pBGR.
*/
int x7sCvtRGBtoYUV422(const IplImage *src, IplImage *dst, bool is_rgb) {
	uint8_t *p_src, *p_uyvy,*end;
	uint8_t p_yuv[6];
	uint16_t u_tmp, v_tmp;

	CV_FUNCNAME("x7sCvtRGBtoYUV422()");

	//Check size or input image
	if(!X7S_ACHECK_IMG8U(src,dst,3,2)) {
		X7S_PRINT_ERROR(cvFuncName,"Images are not compatible");
		return FALSE;
	}

	//Set the pointers.
	p_src = (uint8_t*)src->imageData;
	p_uyvy = (uint8_t*)dst->imageData;
	end = p_src + src->imageSize; //3 Channels for RGB


	if(is_rgb) {
		//loop by 2 pixels
		while(p_src<end) {


			pRGB2pYUV(p_src,p_yuv);
			pRGB2pYUV(p_src+3,p_yuv+3);

			//Compute the mean of 2 U,V pixels.
			u_tmp=p_yuv[1]+p_yuv[4];
			v_tmp=p_yuv[2]+p_yuv[5];

			//Reoder the channels
			p_uyvy[0]=(uint8_t)(u_tmp >> 1);
			p_uyvy[1]=p_yuv[0];
			p_uyvy[2]=(uint8_t)(v_tmp >> 1);
			p_uyvy[3]=p_yuv[3];

			//Jump to the next pair of pixel.
			p_uyvy+=4;
			p_src+=6;
		}
	}
	else {
		//loop by 2 pixels
		while(p_src<end) {

			pBGR2pYUV(p_src,p_yuv);
			pBGR2pYUV(p_src+3,p_yuv+3);

			//Compute the mean of 2 U,V pixels.
			u_tmp=p_yuv[1]+p_yuv[4];
			v_tmp=p_yuv[2]+p_yuv[5];

			//Reoder the channels
			p_uyvy[0]=(uint8_t)(u_tmp >> 1);
			p_uyvy[1]=p_yuv[0];
			p_uyvy[2]=(uint8_t)(v_tmp >> 1);
			p_uyvy[3]=p_yuv[3];

			//Jump to the next pair of pixel.
			p_uyvy+=4;
			p_src+=6;
		}
	}
	return TRUE;
}

/**
* @brief Convert image in YUV422 format to the YUV format.
*/
int x7sCvtYUV4222YUV(const IplImage *src, IplImage *dst) {
	uint8_t *p_uyvy, *p_yuv,*end;

	//Check size or input image
	if(!X7S_ACHECK_IMG8U(src,dst,2,3)) {
		printf("x7sCvtYUV4222YUV: Images are not compatible\n");
		return FALSE;
	}

	//Set the pointers.
	p_uyvy = (uint8_t*)src->imageData;
	p_yuv =  (uint8_t*)dst->imageData;
	end = p_yuv + dst->imageSize; //3 Channels for YUV


	//loop by 2 pixels
	while(p_yuv<end) {

		//First pixel
		p_yuv[0]=p_uyvy[1];	//Y1
		p_yuv[1]=p_uyvy[0];	//U
		p_yuv[2]=p_uyvy[2];	//V

		//Second pixel
		p_yuv[3]=p_uyvy[3];	//Y2
		p_yuv[4]=p_uyvy[0];	//U
		p_yuv[5]=p_uyvy[2];	//V

		//Jump to the next pair of pixel.
		p_uyvy+=4;
		p_yuv+=6;

	}
	return TRUE;
}

/**
* @brief Convert image in YUV format to the YUV422 format.
*/
int x7sCvtYUV2YUV422(const IplImage *src, IplImage *dst) {
	uint8_t *p_uyvy, *p_yuv,*end;
	uint16_t u_tmp, v_tmp;

	//Check size or input image
	if(!X7S_ACHECK_IMG8U(src,dst,3,2)) {
		printf("x7sCvtYUV2YUV422: Images are not compatible\n");
		return FALSE;
	}

	//Set the pointers.
	p_yuv =  (uint8_t*)src->imageData;
	p_uyvy = (uint8_t*)dst->imageData;
	end = p_yuv + src->imageSize; //3 Channels for YUV

	//loop by 2 pixels
	while(p_yuv<end) {

		//Compute the mean of 2 U,V pixels.
		u_tmp=p_yuv[1]+p_yuv[4];
		v_tmp=p_yuv[2]+p_yuv[5];

		//Reoder the channels
		p_uyvy[0]=(uint8_t)(u_tmp >> 1);	//U
		p_uyvy[1]=p_yuv[0];	//Y1
		p_uyvy[2]=(uint8_t)(v_tmp >> 1);	//V
		p_uyvy[3]=p_yuv[3];	//Y2

		//Jump to the next pair of pixel.
		p_uyvy+=4;
		p_yuv+=6;

	}
	return TRUE;
}

//-------- pixel 2 pixels

/**
* @brief Convert values Y,U,V to an RGB pixel.
*
* @note This conversion don't use floating point arithmetic.
* @see http://en.wikipedia.org/wiki/YUV#Numerical_approximations
*
*/
void pRGB2pYUV(uint8_t *p_rgb, uint8_t *p_yuv) {
	p_yuv[Y] = clipByte_i32(((( 66 * (int)p_rgb[R]) + (129 * (int)p_rgb[G]) + ( 25 * (int)p_rgb[B]) + 128) >> 8)+16);
	p_yuv[U] = clipByte_i32((((-38 * (int)p_rgb[R]) - ( 74 * (int)p_rgb[G]) + (112 * (int)p_rgb[B]) + 128) >> 8)+128);
	p_yuv[V] = clipByte_i32((((112 * (int)p_rgb[R]) - ( 94 * (int)p_rgb[G]) - ( 18 * (int)p_rgb[B]) + 128) >> 8)+128);
}

/**
* @brief Convert values Y,U,V to an BGR pixel.
*
* @note This conversion don't use floating point arithmetic.
* @see http://en.wikipedia.org/wiki/YUV#Numerical_approximations
*
*/
void pBGR2pYUV(uint8_t *p_rgb, uint8_t *p_yuv) {
	p_yuv[Y] = clipByte_i32(((( 66 * (int)p_rgb[B]) + (129 * (int)p_rgb[G]) + ( 25 * (int)p_rgb[R]) + 128) >> 8)+16);
	p_yuv[U] = clipByte_i32((((-38 * (int)p_rgb[B]) - ( 74 * (int)p_rgb[G]) + (112 * (int)p_rgb[R]) + 128) >> 8)+128);
	p_yuv[V] = clipByte_i32((((112 * (int)p_rgb[B]) - ( 94 * (int)p_rgb[G]) - ( 18 * (int)p_rgb[R]) + 128) >> 8)+128);
}

/**
* @brief Convert values Y,U,V to an RGB pixel.
*
* This function is inspired from the Y'UV444toRGB888() that you can find on wikipedia.
*
* @note This conversion don't use floating point arithmetic.
* @see http://en.wikipedia.org/wiki/YUV#Y.27UV444
*
*/
void YUV2pRGB(uint8_t y, uint8_t u, uint8_t v,uint8_t* p_rgb) {
	int C,E,D;

	C= y -16;
	D = u -128;
	E = v -128;

	p_rgb[R] = clipByte_i32((298*C + 409*E +128) >> 8);
	p_rgb[G] = clipByte_i32((298*C - 100*D - 208*E +128) >> 8);
	p_rgb[B] = clipByte_i32((298*C + 516*D +128) >> 8);
}

/**
* @brief Convert values Y,U,V to an BGR pixel.
*
* This function is inspired from the Y'UV444toRGB888() that you can find on wikipedia.
*
* @note This conversion don't use floating point arithmetic.
* @see http://en.wikipedia.org/wiki/YUV#Y.27UV444
*
*/
void YUV2pBGR(uint8_t y, uint8_t u, uint8_t v,uint8_t* p_bgr) {
	int C,E,D;

	C= y -16;
	D = u -128;
	E = v -128;

	p_bgr[B] = clipByte_i32((298*C + 409*E +128) >> 8);
	p_bgr[G] = clipByte_i32((298*C - 100*D - 208*E +128) >> 8);
	p_bgr[R] = clipByte_i32((298*C + 516*D +128) >> 8);
}

//== HUE -> BGR ===============================================================================

#define IX7S_BUFFSIZE 10
int x7sCvtH2BGR(const IplImage *src, IplImage *dst,bool is_nonzeros, bool is_legend) {

	IplImage *im_H, *im_SV;
	double scale;
	int id,y_pos=20,max_line=10;
	int i,max_blob, nof_blob=0;
	char str[IX7S_BUFFSIZE];
	CvFont font;

	CV_FUNCNAME("x7sCvtH2BGR()");

	int shift=(int)is_nonzeros;

	if (src->nChannels!=1 || src->depth!=IPL_DEPTH_8U) {
		X7S_PRINT_ERROR(cvFuncName,"Wrong Image Type");
		return X7S_CV_RET_ERR;
	}

	im_H=cvCreateImage(cvGetSize(src),IPL_DEPTH_8U,1);
	im_SV=cvCreateImage(cvGetSize(src),IPL_DEPTH_8U,1);

	//Init Font
	cvInitFont(&font,CV_FONT_HERSHEY_PLAIN, 1,1, 0,1,8);

	//Create the Saturation and Value Channel
	if(!is_nonzeros) cvSet(im_SV,cvScalarAll(255),NULL);
	else cvThreshold(src, im_SV,0,255, CV_THRESH_BINARY);

	//Find the scaling for the Hue channel
	nof_blob = (int)x7sFindMax(src);
	scale=200.0/(double)(nof_blob+1);
	//printf("max=%f scale=%f\n",max,scale);

	//Create the Hue Channel
	cvScale(src,im_H,scale,shift);

	//Merge the channels to HSV colorspace and transform it to RGB
	cvMerge(im_H,im_SV,im_SV,NULL,dst);


	max_blob=X7S_MIN(nof_blob,40);
	if(is_nonzeros) id=1;
	else id=0;

	//Print the legend
	if(is_legend) {
		for(i=0;id<=max_blob;id++,i++) {
			snprintf(str,IX7S_BUFFSIZE,"%02d",id);
			cvPutText(dst, str, cvPoint((i%max_line)*40+10, y_pos), &font,  CV_RGB(255,255,id*scale));
			if(id%max_line==0) y_pos+=20;
		}
	}

	cvCvtColor(dst, dst,CV_HSV2BGR);

	cvReleaseImage(&im_H);
	cvReleaseImage(&im_SV);

	return X7S_CV_RET_OK;
}

/**
* @brief Convert a hue value in a CvScalar with BGR format.
* @param i	    The hue value
* @param max	Maximum value that we can have in our hue value (in other words hue=hue%max).
* @param mod	The modulo (should be a multiple of 6)
*
* As the hue is normally sampled on 360, we suggest to use a module with a multiple of 6
* such as if we use the smallest modulo '6' we have the primary color.
* (RED=0,YELLOW=1,GREEN=2,CYAN=3,BLUE=4,MAGENTA=5,RED=6,...)
*
* @return A scalar with {B,G,R} values
*/
CvScalar x7sCvtHue2BGRScalar(int i, int max, int mod) {
	CvScalar rgba = x7sCvtHue2RGBScalar(i,max,mod);
	return cvScalar(rgba.val[2],rgba.val[1],rgba.val[0],rgba.val[3]);
}

/**
* @brief Convert a hue value in a CvScalar with RGB format.
* @see x7sCvtHue2BGRScalar()
*/
CvScalar x7sCvtHue2RGBScalar(int i, int max, int mod) {

	float shift;
	float h=0.,s=1.,v=1.;
	float n,m,f,a=255;

	//First be sure that the maximum value is never reach.
	i=i%max;

	//H take an integer value between 0 and mod-1.
	h=(float)(i%mod);

	//Shift the value depending on how many full circle we have done
	shift=(float)(i/mod)/((float)max/(float)mod);
	h+=shift;
	s-=(shift/2);

	//Finally rescale the value between [0-6[
	h*=(6.f/(float)mod);

	i = (int)floor(h);
	f = h - i;
	if ( !(i&1) ) f = 1 - f; // if i is even
	m = v * (1 - s);
	n = v * (1 - s * f);

	//Rescale value between 0 and 255
	v=(float)floor(255.f*v);
	n=(float)floor(255.f*n);
	m=(float)floor(255.f*m);

	switch (i) {
	case 6:
	case 0: return cvScalar(v, n, m,a);
	case 1: return cvScalar(n, v, m,a);
	case 2: return cvScalar(m, v, n,a);
	case 3: return cvScalar(m, n, v,a);
	case 4: return cvScalar(n, m, v,a);
	case 5: return cvScalar(v, m, n,a);
	default: return cvScalar(v, n, m,a);
	}
}

/**
* @brief Convert HSV value to a RGB scalar.
* @param h Hue value between [0-6[
* @param s Saturation between [0-1[
* @param v Value between [0-1[
* @return
*/
CvScalar x7sCvtHSV2RGBScalar(float h, float s, float v)
{
	int i;
	float n,m,f,a=255;

	i = (int)floor(h);
	f = h - i;
	if ( !(i&1) ) f = 1 - f; // if i is even
	m = v * (1 - s);
	n = v * (1 - s * f);

	//Rescale value between 0 and 255
	v=(float)floor(255.f*v);
	n=(float)floor(255.f*n);
	m=(float)floor(255.f*m);

	switch (i) {
	case 6:
	case 0: return cvScalar(v, n, m,a);
	case 1: return cvScalar(n, v, m,a);
	case 2: return cvScalar(m, v, n,a);
	case 3: return cvScalar(m, n, v,a);
	case 4: return cvScalar(n, m, v,a);
	case 5: return cvScalar(v, m, n,a);
	default: return cvScalar(v, n, m,a);
	}
}

/**
* @brief Convert heat image to BSV
*/
int x7sCvtHeat2BGR(const IplImage *src, IplImage *dst,int type)
{
	CV_FUNCNAME("x7sCvtHeat2BGR()");
	uint32_t *colMap=NULL;

	switch(type)
	{
	case X7S_CV_HEATC2BGR:
		colMap=(uint32_t*)colmap_heatCW;
		break;
	case X7S_CV_HEATW2BGR:
		colMap=(uint32_t*)colmap_heatDW;
		break;
	case X7S_CV_HEAT2BGR:
	default:
		colMap=(uint32_t*)colmap_heatDCW;
		break;
	}

	return x7sCvtColorMap2BGR(src,dst,colMap);

}

/**
* @brief Convert a Grey image in BGR(A) using a colorMap.
* @param src
* @param dst
* @param colorMap Must be an array of 256 values, where each value correspond to a color (B,G,R,A) on 32-bits.
* @return
* @see x7sCvtHeat2BGR()
*/
int x7sCvtColorMap2BGR(const IplImage *src, IplImage *dst,uint32_t* colorMap)
{
	CV_FUNCNAME("x7sCvtColorMap2BGR()");
	uint8_t *pSrc, *pDst, *pEnd, val, *bgraVal,c;

	X7S_CHECK_WARN(cvFuncName,colorMap,X7S_CV_RET_ERR,"colorMap is NULL");
	X7S_CHECK_WARN(cvFuncName,X7S_IS_IMAGENC(src,1),X7S_CV_RET_ERR,"src is not correct");
	X7S_CHECK_WARN(cvFuncName,X7S_IS_IMAGENC(dst,3),X7S_CV_RET_ERR,"dst is not correct");

	pSrc=(uint8_t*)src->imageData;
	pDst=(uint8_t*)dst->imageData;
	pEnd=pSrc+src->imageSize;


	while(pSrc < pEnd)
	{
		val = X7S_INRANGE(*pSrc,0,255);
		bgraVal = (uint8_t*)(&colorMap[val]);

		for(c=0;c<dst->nChannels;c++) 	pDst[c]=bgraVal[3-c];

		pDst+=dst->nChannels;
		pSrc++;
	}



	return X7S_CV_RET_OK;
}



//== JPEG -> BGR ===============================================================================

#ifdef X7S_CV_JPEG

/*
 * Initialize source --- called by jpeg_read_header
 * before any data is actually read.
 */
METHODDEF(void)
init_source (j_decompress_ptr cinfo)
{
}

/*
 * Fill the input buffer --- called whenever buffer is emptied.
 *
 * If this procedure gets called, we have a buffer overrun condition -
 * there is not enough data in the buffer to satisfy the decoder.
 * The procedure just generates a warning and feeds the decoder a fake
 * JPEG_EOI marker.
 *
 * JMESSAGE(JWRN_JPEG_EOF, "Premature end of JPEG file")
 */
METHODDEF(boolean)
fill_input_buffer (j_decompress_ptr cinfo)
{
	struct jpeg_source_mgr * src = cinfo->src;
	static JOCTET FakeEOI[] = { 0xFF, JPEG_EOI };

	/* Generate warning */
	WARNMS(cinfo, JWRN_JPEG_EOF);

	/* Insert a fake EOI marker */
	src->next_input_byte = FakeEOI;
	src->bytes_in_buffer = 2;

	return TRUE;
}


METHODDEF(void)
skip_input_data (j_decompress_ptr cinfo, long num_bytes)
{
	struct jpeg_source_mgr * src = cinfo->src;

	if(num_bytes >= (long)src->bytes_in_buffer)
	{
		fill_input_buffer(cinfo);
		return;
	}

	src->bytes_in_buffer -= num_bytes;
	src->next_input_byte += num_bytes;
}


/*
 * Terminate source --- called by jpeg_finish_decompress
 * after all data has been read.  Often a no-op.
 *
 * NB: *not* called by jpeg_abort or jpeg_destroy; surrounding
 * application must deal with any cleanup that should happen even
 * for error exit.
 */
METHODDEF(void)
term_source (j_decompress_ptr cinfo)
{
	/* no work necessary here */
}

METHODDEF(void)
x7s_error_exit (j_common_ptr cinfo_ptr)
{
	struct jpeg_decompress_struct* cinfo = (struct jpeg_decompress_struct*)(cinfo_ptr);
	const char* msg1 = cinfo->err->jpeg_message_table[cinfo->err->msg_code - cinfo->err->first_addon_message];
	const char* msg2 = "";
	X7S_PRINT_ERROR("x7sCvtJPEG2RGBA()","x7s_error_exit %s",msg1);
	if (cinfo->err->addon_message_table != NULL)
	{
		msg2 = cinfo->err->addon_message_table[cinfo->err->msg_code - cinfo->err->first_addon_message];
		X7S_PRINT_WARN("x7sCvtJPEG2RGBA()","x7s_error_exit 2 %s",msg2);
	}
	jpeg_destroy_decompress(cinfo);
}


/**
* @brief Convert JPEG buffer in a BGR image.
* @param src A pointer on a JPEG buffer defined as an IplImage.
* This IplImage should have the nChannels equal to zeros because it can not be read by others
* OpenCV function due to the fact that its data are encoded. It must also have the correct size
* of the buffer using the field imageSize.
* @param dst Destination image that must have the correct width, length and nChannels=3.
*/
int x7sLibJPEG2BGR(const IplImage *src, IplImage **pDst, int is_rgba, int realloc) {

	//Declare variable
	int r,p;
	uint8_t* p_dst, *line;
	IplImage *dst = pDst[0];


	CV_FUNCNAME("x7sCvtJPEG2BGR()");


	//Declare JPEG structure
	struct jpeg_source_mgr jsrc;
	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;

	//Setup function pointer
	jsrc.init_source=init_source;
	jsrc.fill_input_buffer = fill_input_buffer;
	jsrc.skip_input_data = skip_input_data;
	jsrc.resync_to_restart = jpeg_resync_to_restart; /* use default method */
	jsrc.term_source = term_source;

	//Initiate JPEG structure.
	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_decompress(&cinfo);

	//Check source
	if(src==NULL) {
		X7S_PRINT_ERROR(cvFuncName,"IplImage src is NULL");
		return X7S_CV_RET_ERR;
	}

	//Check fields from source.
	if(src->nChannels>0 || src->imageSize<=0) {
		X7S_PRINT_WARN(cvFuncName,"Image source (JPEG) is not correct "
				"(nChannels=%d, size=%d)",src->nChannels,src->imageSize);
		x7sIplPrintInfos(src);
	}

	//Initiate JPEG source
	jsrc.bytes_in_buffer=(size_t)src->imageSize;
	jsrc.next_input_byte=(JOCTET *)src->imageData;
	cinfo.src=&jsrc;

	//read the source datastream header markers
	if(jpeg_read_header(&cinfo, FALSE)!=JPEG_HEADER_OK) return X7S_CV_RET_ERR;
	jpeg_start_decompress(&cinfo);

	//Check that destination image has correct size
	if(dst->width != (int)cinfo.output_width ||
			dst->height != (int)cinfo.output_height ||
			dst->nChannels != cinfo.num_components) {
		X7S_PRINT_ERROR(cvFuncName,"Size changed");
		return X7S_CV_RET_ERR;
	}


	//Setup JPEG.
	p_dst = (uint8_t*)dst->imageData;
	line = (uint8_t*)(malloc(dst->widthStep));

	//For each line read a scanline.
	for(r=0;r<dst->height;r++) {
		jpeg_read_scanlines(&cinfo,&line,1);

		//Then invert RG pixels during scan.
		for(p=0;p<dst->widthStep;p+=3) {
			p_dst[p]=line[p+2];
			p_dst[p+1]=line[p+1];
			p_dst[p+2]=line[p];
		}

		//Finaly go to the next line
		p_dst+=dst->widthStep;
	}



	/* wrap up decompression, destroy objects, free pointers and close open files */
	jpeg_finish_decompress( &cinfo );
	jpeg_destroy_decompress( &cinfo );
	free(line);

	return X7S_CV_RET_OK;
}

/**
* @brief Convert JPEG buffer in a RGBA or BGRA image (For QT, wxWidget or other library).
* @param src A pointer on a JPEG buffer defined as an IplImage.
* This IplImage should have the nChannels equal to zeros because it can not be read by others
* OpenCV function due to the fact that its data are encoded. It must also have the correct size
* of the buffer using the field imageSize.
* @param dst Destination image that must have the correct width, length and nChannels=4.
* @param is_rgba When @TRUE the image returned is RGBA, otherwise it is BGRA.
* @param realloc When @TRUE the dst image can be reallocated
* @note The alpha channel is by default set to 255.
*/
int x7sLibJPEG2RGBA(const IplImage *src, IplImage **pDst, int is_rgba, int realloc)
{

	//Declare variable
	int r,p0,p2, ret;
	uint8_t *line,*p_line, *p_end;
	uint8_t* p_dst;
	IplImage *dst=pDst[0];

	CV_FUNCNAME("x7sCvtJPEG2RGBA()");


	//Declare JPEG structure
	struct jpeg_source_mgr jsrc;
	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;

	//Setup function pointer
	jsrc.init_source=init_source;
	jsrc.fill_input_buffer = fill_input_buffer;
	jsrc.skip_input_data = skip_input_data;
	jsrc.resync_to_restart = jpeg_resync_to_restart; /* use default method */
	jsrc.term_source = term_source;

	//Initiate JPEG structure.
	cinfo.err = jpeg_std_error(&jerr);
	cinfo.err->error_exit = x7s_error_exit;
	jpeg_create_decompress(&cinfo);

	//Check source
	if(src==NULL) {
		X7S_PRINT_ERROR(cvFuncName,"IplImage src is NULL");
		return X7S_CV_RET_ERR;
	}

	//Check fields from source.
	if(src->nChannels>0 || src->imageSize<=10) {
		X7S_PRINT_WARN(cvFuncName,"Image source (JPEG) is not correct "
				"(nChannels=%d, size=%d)",src->nChannels,src->imageSize);
		x7sIplPrintInfos(src);
		jpeg_finish_decompress( &cinfo );
		jpeg_destroy_decompress( &cinfo );
		return X7S_CV_RET_ERR;
	}


	//Check data in source
	ret = memcmp(src->imageDataOrigin, jpeg_hdr_type,sizeof(jpeg_hdr_type));
	if(ret!=0)
	{
		uint8_t *tmp=(uint8_t*)src->imageDataOrigin;
		X7S_PRINT_WARN(cvFuncName,"JPEG data does not start with 0xFFD8FFD9: 0x%02x%02x%02x%02x",
				tmp[0],tmp[1],tmp[2],tmp[3]);
		jpeg_finish_decompress( &cinfo );
		jpeg_destroy_decompress( &cinfo );
		return X7S_CV_RET_ERR;

	}

	//Initiate JPEG source
	jsrc.bytes_in_buffer=(size_t)src->imageSize;
	jsrc.next_input_byte=(JOCTET *)src->imageData;
	cinfo.src=&jsrc;

	//read the source datastream header markers
	if(jpeg_read_header(&cinfo, FALSE)!=JPEG_HEADER_OK) return X7S_CV_RET_ERR;
	jpeg_start_decompress(&cinfo);

	//Check that destination image has correct size
	if(dst) {
		if(dst->width != (int)cinfo.output_width ||
				dst->height != (int)cinfo.output_height ||
				dst->nChannels != 4) {
			if(realloc) {
				//Delete the old image
				cvReleaseImage(&dst);
				pDst[0]=NULL;
			}
			else {
				X7S_PRINT_ERROR(cvFuncName,"Size changed");
				return X7S_CV_RET_ERR;
			}
		}
	}

	//If destination is NULL create it (only for realloc).
	if(dst==NULL) {
		if(realloc==FALSE) {
			X7S_PRINT_ERROR(cvFuncName,"dst==NULL and realloc=false");
			return X7S_CV_RET_ERR;
		}
		else {
			pDst[0]=cvCreateImage(cvSize((int)cinfo.output_width,(int)cinfo.output_height)
					,IPL_DEPTH_8U,4);
			dst=pDst[0];
		}
	}



	//Premature end of JPEG file
	//Corrupt JPEG data
	if(cinfo.err->msg_code < JMSG_COPYRIGHT && JWRN_ADOBE_XFORM <= cinfo.err->msg_code) {
		X7S_PRINT_ERROR(cvFuncName,"Error while reading header #%d : %s",
				cinfo.err->msg_code,cinfo.err->format_message);
		jpeg_finish_decompress( &cinfo );
		jpeg_destroy_decompress( &cinfo );
		return X7S_CV_RET_ERR;
	}

	//Setup JPEG.
	p_dst = (uint8_t*)dst->imageData;
	line = (uint8_t*)(malloc(dst->width*3));


	//Setup RGBA or BGRA
	if(is_rgba)
	{
		p0=0;
		p2=2;
	} else
	{
		p0=2;
		p2=0;
	}

	//For each line read a scanline.
	for(r=0;r<dst->height;r++) {
		jpeg_read_scanlines(&cinfo,&line,1);
		p_line=line;
		p_end=p_line+dst->width*3;

		//Then invert RG pixels during scan.
		while(p_line<p_end) {
			p_dst[R]=p_line[p0];
			p_dst[G]=p_line[G];
			p_dst[B]=p_line[p2];
			p_dst[A]=255;

			p_dst+=4;
			p_line+=3;
		}
	}



	/* wrap up decompression, destroy objects, free pointers and close open files */
	jpeg_finish_decompress( &cinfo );
	jpeg_destroy_decompress( &cinfo );
	free(line);

	return X7S_CV_RET_OK;
}

#endif


int x7sDecode2BGR(const IplImage *src, IplImage **pDst, int is_rgb, int realloc)
{
	CvMat *mxSrc=NULL;
	IplImage *dstRGB=NULL, *dst=pDst[0];
	const int nChannels=3;
	bool ret;

	X7S_FUNCNAME("x7sDecode2BGR()");


	//Check source header
	X7S_CHECK_WARN(funcName,src,X7S_CV_RET_ERR,"IplImage src is NULL");
	X7S_CHECK_WARN(funcName,src->nChannels==0,X7S_CV_RET_ERR,"src->nChannels=%d (should be zeros)",src->nChannels);
	X7S_CHECK_WARN(funcName,src->imageSize>10,X7S_CV_RET_ERR,"src->imageSize=%d (should be >10)",src->imageSize);
	X7S_CHECK_WARN(funcName,src->imageData && src->imageDataOrigin,X7S_CV_RET_ERR,"src->imageData or imageDataOrigin is NULL");

	//Check data in source
	ret = memcmp(src->imageDataOrigin, jpeg_hdr_type,sizeof(jpeg_hdr_type));
	if(ret!=0)
	{
		uint8_t *tmp=(uint8_t*)src->imageDataOrigin;
		X7S_PRINT_WARN(funcName,"JPEG data does not start with 0xFFD8FFD9: 0x%02x%02x%02x%02x",
				tmp[0],tmp[1],tmp[2],tmp[3]);
		return X7S_CV_RET_ERR;
	}

	//----------------------------------------

	//Decode the source.
	mxSrc = cvCreateMatHeader(src->width,src->height,CV_8UC1);
	mxSrc->data.ptr=src->imageDataOrigin;
	dstRGB = cvDecodeImage(mxSrc,CV_LOAD_IMAGE_COLOR);


	//----------------------------------------

	//Check that destination image has correct size
	if(dst) {
		if(dst->width != (int)dstRGB->width ||
				dst->height != (int)dstRGB->height ||
				dst->nChannels != nChannels) {
			if(realloc) {
				//Delete the old image
				cvReleaseImage(&dst);
				pDst[0]=NULL;
			}
			else {
				X7S_PRINT_ERROR(funcName,"Size changed");
				return X7S_CV_RET_ERR;
			}
		}
	}

	//If destination is NULL create it (only for realloc).
	if(dst==NULL) {
		X7S_CHECK_ERROR(funcName,realloc,X7S_CV_RET_ERR,"dst==NULL and realloc=false");
		pDst[0]=cvCreateImage(cvGetSize(dstRGB),IPL_DEPTH_8U,nChannels);
		dst=pDst[0];
	}

	//Finally convert the image to the good value
	if(is_rgb) cvConvertImage(dstRGB,dst,CV_BGR2RGB);
	else cvCopyImage(dstRGB,dst);

	//Remove the dstRGB image use temporarily
	cvReleaseImage(&dstRGB);
	cvReleaseMatHeader(&mxSrc);

	return X7S_CV_RET_OK;
}


int x7sDecode2BGRA(const IplImage *src, IplImage **pDst, int is_rgba, int realloc)
{
	CvMat *mxSrc=NULL;
	IplImage *dstRGB=NULL, *dst=pDst[0];
	const int nChannels=4;
	bool ret;

	X7S_FUNCNAME("x7sDecode2BGRA()");


	//Check source header
	X7S_CHECK_WARN(funcName,src,X7S_CV_RET_ERR,"IplImage src is NULL");
	X7S_CHECK_WARN(funcName,src->nChannels==0,X7S_CV_RET_ERR,"src->nChannels=%d (should be zeros)",src->nChannels);
	X7S_CHECK_WARN(funcName,src->imageSize>10,X7S_CV_RET_ERR,"src->imageSize=%d (should be >10)",src->imageSize);

	//Check data in source
	ret = memcmp(src->imageDataOrigin, jpeg_hdr_type,sizeof(jpeg_hdr_type));
	if(ret!=0)
	{
		uint8_t *tmp=(uint8_t*)src->imageDataOrigin;
		X7S_PRINT_WARN(funcName,"JPEG data does not start with 0xFFD8FFD9: 0x%02x%02x%02x%02x",
				tmp[0],tmp[1],tmp[2],tmp[3]);
		return X7S_CV_RET_ERR;
	}

	//----------------------------------------

	//Decode the source.
	mxSrc = cvCreateMatHeader(src->width,src->height,CV_8UC1);
	mxSrc->data.ptr=src->imageDataOrigin;
	dstRGB = cvDecodeImage(mxSrc,CV_LOAD_IMAGE_COLOR);

	X7S_CHECK_WARN(funcName,dstRGB,X7S_CV_RET_ERR,"dstRGB=NULL (cvDecodeImage failed)");

	//----------------------------------------

	//Check that destination image has correct size
	if(dst) {
		if(dst->width != (int)dstRGB->width ||
				dst->height != (int)dstRGB->height ||
				dst->nChannels != nChannels) {
			if(realloc) {
				//Delete the old image
				cvReleaseImage(&dst);
				pDst[0]=NULL;
			}
			else {
				X7S_PRINT_ERROR(funcName,"Size changed");
				return X7S_CV_RET_ERR;
			}
		}
	}

	//If destination is NULL create it (only for realloc).
	if(dst==NULL) {
		X7S_CHECK_ERROR(funcName,realloc,X7S_CV_RET_ERR,"dst==NULL and realloc=false");
		pDst[0]=cvCreateImage(cvGetSize(dstRGB),IPL_DEPTH_8U,nChannels);
		dst=pDst[0];
	}

	//Finally convert the image to the good value
	cvCvtColor(dstRGB, dst,CV_BGR2BGRA);
	if(is_rgba) cvCvtColor(dst,dst,CV_BGRA2RGBA);

	x7sCvtBGRA2BGRA255(dst,dst);


	//Remove the dstRGB image use temporarily
	cvReleaseImage(&dstRGB);
	cvReleaseMatHeader(&mxSrc);

	return X7S_CV_RET_OK;
}



int x7sCvtBGRA2BGRA255(const IplImage *src, IplImage *dst)
{
	X7S_FUNCNAME("x7sCvtBGRA2BGRA255()");

	uint8_t *p_dst,*end;


	//Check source header
	X7S_CHECK_WARN(funcName,X7S_IS_IMAGENC(src,4),X7S_CV_RET_ERR,"src is NULL or not 4 channels");
	X7S_CHECK_WARN(funcName,X7S_IS_IMAGENC(dst,4),X7S_CV_RET_ERR,"dst is NULL or not 4 channels");
	X7S_CHECK_WARN(funcName,X7S_ARE_IMSIZEEQ(src,dst),X7S_CV_RET_ERR,"src & dst have not the same size");


	//Set the pointers.
	p_dst = (uint8_t*)dst->imageData;
	end = p_dst + dst->imageSize; //4 Channels for RGBA

	if(src!=dst) 
	{
		cvCopy(src,dst,NULL);
	}

	while(p_dst<end) 
	{
		p_dst[3]=255; //Set alpha to 255
		p_dst+=4; //Go to next BGRA pixel
	}

	return X7S_CV_RET_OK;
}


//== LRLE -> Grey ===============================================================================

/**
* @brief A LRLE tupple.
** @code
* ----------------------------------
* || 31 ........16 15.......... 0 ||
* ----------------------------------
* ||   noise      |       ID      ||
* || ? | start  |   end  |  stop  ||
* ----------------------------------
* ||                              ||
* ||            ...               ||
* ----------------------------------
* @endcode
*
* Using the following bytes:
* 		- ID is on 16 bits  [15<00].
* 		- start on 9 bits   [24<18].
* 		- end on 9 bits     [17<09].
* 		- stop on 9 bits	[08<00].
*
* @note The field equivalence (eq) is not used in this decoder.
*  */
typedef struct {
	unsigned int id;
	unsigned int row:9;
	unsigned int end:9;
	unsigned int start:9;
	unsigned int zeros:5;
} lrle_tupple_t;


/**
* @brief Swap endianness of uint32_t
*/
void swapEndianU32(uint32_t *out,const uint32_t *in) {
	uint8_t *p = (uint8_t*)in;
	*out=0;
	(*out)+= (p[0] << 24);
	(*out)+= (p[1] << 16);
	(*out)+= (p[2] << 8);
	(*out)+= p[3];
}

/**
* @brief Take the value of a RLE tupple and convert it to grey value.
*/
int pLRLTupple2Grey(uint8_t id, uint16_t start, uint16_t end, uint16_t row, IplImage *out) {
	uint8_t *p_im, *p_end;
	uint8_t nC=out->nChannels;	//Number of channels.


	if(out == NULL) {
		X7S_PRINT_ERROR("x7sCvtLRLE2Grey()","Image out is NULL");
		return X7S_CV_RET_ERR;
	}

	//TODO: Shift by -3 (due to rafa filter)
	if(start>2) start-=3;
	if(end>2) end-=3;
	if(start>350 && start < 360) start=350;
	if(end>351 && start <= 360) end=351;

	//Check Range
	if(!X7S_CHECK_URANGE(start, out->width))
	{
		X7S_PRINT_WARN("x7sCvtLRLE2Grey()","ID=%d, start=%d > width=%d",id,start,out->width);
		return X7S_CV_RET_ERR;
	}

	if(!X7S_CHECK_RANGE(end,start,out->width))
	{
		X7S_PRINT_WARN("x7sCvtLRLE2Grey()","ID=%d end=%d != [start=%d,width=%d])",id,end,start,out->width);
		return X7S_CV_RET_ERR;
	}

	if(!X7S_CHECK_URANGE(row, out->height)) {
		X7S_PRINT_WARN("x7sCvtLRLE2Grey()","ID=%d, row (%d) > height (%d)",id,row,out->height);
		return X7S_CV_RET_ERR;
	}


	//Set begginning and end of the line for this tupple.
	p_im =(uint8_t*)(out->imageData + row*out->widthStep);
	p_end = (uint8_t*)(p_im + (end*nC));
	p_im+=start*nC;

	//For each pixels between start and end.
	while(p_im< p_end) {
		p_im[0]=id;	//Set Hue with the ID.
		if(nC>1) {	//!< If the output is HSV type (three channels).
			p_im[1]=UINT8_MAX;	// Set H to maximum value.
			p_im[2]=UINT8_MAX;	// Set V to maximum value.
		}
		p_im+=nC;		// Jump to the next pixel.
	}
	return X7S_CV_RET_OK;
}

/**
* @brief Convert Line-RLE buffer in a BGR image.
*
* The Line Run-Length-Encoding is given by @cite{Appiah2008}.
* A tupple of a LRLE is given by @ref lrle_tupple_t.
*
* @param src A pointer on a RLE buffer defined as an IplImage.
* This IplImage should have the nChannels equal to zeros because it can not be read by others
* OpenCV function due to the fact that its data are encoded. It must also have the correct size
* of the buffer using the field imageSize.
* @param dst Destination image that must have the correct width, length and nChannels.
*/
int x7sCvtLRLE2Grey(const IplImage *src, IplImage *dst) {

	//Declare variable
	int i;
	uint32_t max=0, *p_in, *p_tup;
	int nof_tupples=0, nZeros=0,nFirst=0;
	lrle_tupple_t lrle_tup;

	CV_FUNCNAME("x7sCvtLRLE2Grey()");

	//Check source
	if(src==NULL) {
		X7S_PRINT_ERROR(cvFuncName,"IplImage src is NULL");
		return X7S_CV_RET_ERR;
	}

	//Check fields from source.
	if(src->nChannels>0 || src->imageSize<=0) {
		X7S_PRINT_WARN(cvFuncName,"Image source (LRLE) is not correct");
	}

	//Init the variables.
	nof_tupples=src->imageSize/sizeof(lrle_tupple_t);
	p_in=(uint32_t*)src->imageData;
	p_tup=(uint32_t*)(&lrle_tup.id);


	//Reset the image to zeros.
	cvSet(dst,cvScalarAll(0),NULL);

	//Take parameters for each tupple to convert it by grey pixels.
	for(i=0;i<nof_tupples;i++) {

		//Swap buffers
		swapEndianU32(p_tup,p_in++);
		swapEndianU32(p_tup+1,p_in++);
		lrle_tup.id = lrle_tup.id & 0xFFF;	//10 bits

		//Check Maximum.
		if(lrle_tup.id>max) {
			max=lrle_tup.id;
		}

		//Don't draw when ID is zeros.
		if(lrle_tup.id == 0) {
			nZeros++;
			break;
		}
		else {
			//Draw RLE tupple
			if(nZeros==0) {
				nFirst++;
				pLRLTupple2Grey(lrle_tup.id,lrle_tup.start,lrle_tup.end,lrle_tup.row, dst);
			}
		}
	}
	X7S_PRINT_DEBUG(cvFuncName,"LRLE => nof tupples =%d, nFirst=%d,nZeros=%d, maxID=%d",
			nof_tupples, nFirst,nZeros,max);

	return X7S_CV_RET_OK;
}



//== Clipping ===============================================================================


/**
* @brief Clip a value between [0-255]and return it in uint8_t type.
* @param val The input in int (signed 32 bits).
* @return The output value on 8-bit.
*/
uint8_t clipByte_i32(int val) {
	int tmp=val;
	if(tmp< 0) tmp=(int)0;
	if(tmp > 255) tmp = (int)255;
	return (uint8_t)tmp;
}





