#include "x7scv.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Defines/Enums/Includes
//----------------------------------------------------------------------------------------
#include "x7sCvLog.h"


static unsigned char bitOneLUT256[256] =
{
#   define B2(n) n,     n+1,     n+1,     n+2
#   define B4(n) B2(n), B2(n+1), B2(n+1), B2(n+2)
#   define B6(n) B4(n), B4(n+1), B4(n+1), B4(n+2)
		B6(0), B6(1), B6(1), B6(2)
};



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Private Prototypes
//----------------------------------------------------------------------------------------



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public functions
//----------------------------------------------------------------------------------------

/**
* @brief Push back/right a bit in a 32-bit word.
*
* @param word A pointer on the 32-bit word.
* @param push_val the value to push_back (from the right), must be 1 or zeros.
* @return The pop_front value given by the most left bit in word previous this operation.
*/
uint8_t x7sBitPushBack32(uint32_t *word, uint8_t push_val)
{
	uint32_t mlb;
	mlb = (*word & X7S_BIT_MLMASK_32); //Save the Most-left-bit (or reminder)
	*word <<= 1; //Shift val of 1
	*word += (push_val & X7S_BIT_MRMASK_32); //Add the new value
	return (uint8_t)(mlb==X7S_BIT_MLMASK_32);	//Setup the next push value
}



/**
* @brief Simple method to count the 1-bits in a 32-bit word
* @return The number of bit equals to one.
*/
uint8_t x7sBitCount32(uint32_t word)
{
	uint32_t c=0;
	for (c = 0; word; word >>= 1)
	{
		c += (word & 1);
	}
	return (uint8_t)c;
}


uint8_t x7sBitCountConsec32(uint32_t word, int val)
{
	uint32_t c=0;
	uint32_t tmp=0;
	uint32_t last=0;


	if(val==0)
	{
		for (c = 0; c<32; c++, word >>= 1)
		{
			if(tmp>last) last=tmp;
			if(!(word &1)) tmp++;
			else tmp=0;
		}
		c=last;
	}
	else if(val==1)
	{
		last = 0;
		for (c = 0; word; word >>= 1)
		{
			if(tmp>last) last=tmp;
			if(word &1) tmp++;
			else tmp=0;
		}
		c=last;
	}
	else
	{
		for (c = 0; word; word >>= 1)
		{
			last=(word &1);
			if((word & 1))
			{
				if(last) c+=2;
				else c+=1;
			}
			else
			{
				if(last) c--;
			}
		}
	}
	return (uint8_t)c;
}



/**
* @brief Generate the look-up table that give the number of 1 bits in a 8-bits word.
*
* This function use a static array that, therefore there is no need to release the memory.
*
* @see x7sBitFastCount32()
*/
const uint8_t* x7sBitFastCountGenLUT256()	{
	int i=0;
	bitOneLUT256[0] = 0;

	for (i = 0; i < 256; i++)
	{
		bitOneLUT256[i] = (i & 1) + bitOneLUT256[i / 2];
	}
	return bitOneLUT256;
}

/**
* @brief Count the number of 1 bit in a fast way
* @warning To use this function you must call x7sBitGenCountLookUpTable256() first.
* @see x7sBitGenCountLookUpTable256(), @ref http://graphics.stanford.edu/~seander/bithacks.html
*/
uint8_t x7sBitFastCount32(uint32_t word)
{
	unsigned char * p = (unsigned char *) &word;
	return (uint8_t)bitOneLUT256[p[0]] + bitOneLUT256[p[1]] + bitOneLUT256[p[2]] + bitOneLUT256[p[3]];
}

/**
* @brief Convert a 8-bits value to array of integer 8 x{1,0}
*
* @param barray The size of the array must be greater or equal than 8.
*/
void x7sBitToByteArray8(uint8_t val, uint8_t *barray)
{
	int pos;
	for(pos = 7; val; val >>= 1, pos--)
	{
		barray[pos] = (val & 1);
	}
}

/**
* @brief Convert a 32-bits value to array of integer 32 x{1,0}
*
* @param barray The size of the array must be greater or equal than 32.
*/
void x7sBitToByteArray32(uint32_t val, uint8_t *barray)
{
	int pos;
	for(pos = 31; val; val >>= 1, pos--)
	{
		barray[pos] = (val & 1);
	}
}

/**
* @brief Convert a 8-bits value to C-String with {'1','0'}
*
* @param sz A C-String with at least 8+1 characters.
*/
void x7sBitToCString8(uint8_t val, char *sz)
{
	int pos;
	for(pos = 7; pos>=0; val >>= 1, pos--)
	{
		sz[pos] = '0' + (val & 1);
	}
	sz[8]=0;
}

/**
* @brief Convert a 32-bits value to C-String with {'1','0'}
*
* @param sz A C-String with at least 32+1 characters.
*/
void x7sBitToCString32(uint32_t val, char *sz)
{
	int pos;
	for(pos = 31; pos>=0; val >>= 1, pos--)
	{
		sz[pos] = '0' + (val & 1);
	}
	sz[32]=0;
}



