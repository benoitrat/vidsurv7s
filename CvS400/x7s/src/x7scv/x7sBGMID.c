#include "x7scv.h"
#include "x7sCvLog.h"


//================================================================================
//-- Defines and Enumerates
#define IX7S_PRINT_POS 12370




//================================================================================
//-- Private Prototypes

uint8_t ix7sMIDDiffBGModel_PushBack32(IplImage *layers, int pos, uint8_t push_value);
uint8_t ix7sMIDDiffBGModel_ComputeStats(IplImage *layers,  int pos, uint32_t * listP);
static void ix7sReleaseMIDiffBGModel( X7sMIDiffBGModel** bg_model );
static int  ix7sUpdateMIDiffBGModel( IplImage* curr_frame, X7sMIDiffBGModel*  bg_model );
int x7sMosaicMeanBlock(const IplImage *src, IplImage *dst);
int x7sMosaicMeanBlock_1C8U(const IplImage *src, IplImage *dst);
int x7sMosaicMeanBlock_3C8U(const IplImage *src, IplImage *dst);
int ix7sUpdateMIDiffBGModel_ColorDiff(IplImage *curr_frame, X7sMIDiffBGModel* bg_model);
int ix7sUpdateMIDiffBGModel_EdgeDiff(IplImage *curr_frame, X7sMIDiffBGModel* bg_model);


IplImage *imDiffSmall=NULL;
IplImage *imMean=NULL, *imVar=NULL, *imNNZ=NULL;
IplImage *imOC=NULL, *imVar2=NULL;
IplImage *imTmp=NULL;

IplImage *imDiff=NULL, *imBin=NULL;
IplImage *imGrey=NULL, *imEntro=NULL;
IplImage *imActEdge=NULL, *imPrevEdge=NULL;





//================================================================================
//-- Functions

int x7sMosaicMeanBlock(const IplImage *src, IplImage *dst)
{
	CV_FUNCNAME("x7sPseudoMed()");

	if(X7S_IS_IMAGENC(src,3) && X7S_IS_IMAGENC(dst,3)) {
		return x7sMosaicMeanBlock_3C8U(src,dst);
	}
	else if(X7S_IS_IMAGENC(src,1) && X7S_IS_IMAGENC(dst,1))
	{
		return x7sMosaicMeanBlock_1C8U(src,dst);
	}
	else {
		X7S_PRINT_ERROR(cvFuncName,"Images are not compatible");
		return X7S_CV_RET_ERR;
	}
}



int x7sMosaicMeanBlock_3C8U(const IplImage *src, IplImage *dst)
{


	X7S_FUNCNAME("x7sMosaicBlock()");

	int cSrc,rDst,cDst, nBlocks,i;
	float wRatio, hRatio;
	uint8_t *pSrc, *pDst;
	uint32_t *pTmp, np2Blocks;
	IplImage *tmpLine;



	wRatio = (float)src->width/(float)dst->width;
	hRatio = (float)src->height/(float)dst->height;
	nBlocks = (int)wRatio;

	X7S_CHECK_WARN(funcName,wRatio==hRatio,X7S_RET_ERR,"wRatio:%f != hRatio:%f",wRatio,hRatio);
	X7S_CHECK_WARN(funcName,(float)nBlocks==wRatio,X7S_RET_ERR,"nBlocks:%d != wRatio:%f",nBlocks,wRatio);

	//Assign pointers
	pDst = (uint8_t*)dst->imageData;
	pSrc = (uint8_t*)src->imageData;

	//Reserve memory for two lines
	np2Blocks=nBlocks*nBlocks;
	tmpLine = cvCreateImage(cvSize(dst->width,1),IPL_DEPTH_32S,dst->nChannels);
	pTmp = (uint32_t*)tmpLine->imageData;



	//Then browse on each line
	for(rDst=0;rDst<dst->height;rDst++)
	{

		//Set the tmpLine with 0
		memset(tmpLine->imageData,0,tmpLine->imageSize);

		//First accumulate on the tmpLine the nBlocks Row.
		for(i=0;i<nBlocks;i++)
		{
			//For each column in the block
			cSrc=0;
			pTmp=(uint32_t*)tmpLine->imageData;

			while(cSrc<src->width)
			{
				pTmp[0]+=(uint32_t)*(pSrc++);
				pTmp[1]+=(uint32_t)*(pSrc++);
				pTmp[2]+=(uint32_t)*(pSrc++);
				cSrc++; //Go to next column

				//If we are going to start a new block
				if(cSrc%nBlocks==0) pTmp+=dst->nChannels;
			}
		}


		//Then, compute the mean in the destination
		pTmp=(uint32_t*)tmpLine->imageData;
		for(cDst=0;cDst<dst->width;cDst++)
		{
			*(pDst++)=(uint8_t)( *(pTmp++)/np2Blocks);
			*(pDst++)=(uint8_t)( *(pTmp++)/np2Blocks);
			*(pDst++)=(uint8_t)( *(pTmp++)/np2Blocks);
		}


	}

	//Free the memory of temporary lines
	cvReleaseImage(&tmpLine);
	tmpLine=NULL;


	return X7S_RET_OK;

}



int x7sMosaicMeanBlock_1C8U(const IplImage *src, IplImage *dst)
{


	X7S_FUNCNAME("x7sMosaicBlock()");

	int cSrc,rDst,cDst, nBlocks,i;
	float wRatio, hRatio;
	uint8_t *pSrc, *pDst;
	uint32_t *pTmp, np2Blocks;
	IplImage *tmpLine;



	wRatio = (float)src->width/(float)dst->width;
	hRatio = (float)src->height/(float)dst->height;
	nBlocks = (int)wRatio;

	X7S_CHECK_WARN(funcName,wRatio==hRatio,X7S_RET_ERR,"wRatio:%f != hRatio:%f",wRatio,hRatio);
	X7S_CHECK_WARN(funcName,(float)nBlocks==wRatio,X7S_RET_ERR,"nBlocks:%d != wRatio:%f",nBlocks,wRatio);

	//Assign pointers
	pDst = (uint8_t*)dst->imageData;
	pSrc = (uint8_t*)src->imageData;

	//Reserve memory for two lines
	np2Blocks=nBlocks*nBlocks;
	tmpLine = cvCreateImage(cvSize(dst->width,1),IPL_DEPTH_32S,dst->nChannels);
	pTmp = (uint32_t*)tmpLine->imageData;



	//Then browse on each line
	for(rDst=0;rDst<dst->height;rDst++)
	{

		//Set the tmpLine with 0
		memset(tmpLine->imageData,0,tmpLine->imageSize);

		//First accumulate on the tmpLine the nBlocks Row.
		for(i=0;i<nBlocks;i++)
		{
			//For each column in the block
			cSrc=0;
			pTmp=(uint32_t*)tmpLine->imageData;

			while(cSrc<src->width)
			{
				pTmp[0]+=(uint32_t)*(pSrc++);
				cSrc++; //Go to next column

				//If we are going to start a new block
				if(cSrc%nBlocks==0) pTmp+=tmpLine->nChannels;
			}
		}


		//Then, compute the mean in the destination
		pTmp=(uint32_t*)tmpLine->imageData;
		for(cDst=0;cDst<dst->width;cDst++)
		{
			*(pDst++)=(uint8_t)( *(pTmp++)/np2Blocks);
		}


	}

	//Free the memory of temporary lines
	cvReleaseImage(&tmpLine);
	tmpLine=NULL;


	return X7S_RET_OK;

}



int x7sMosaicEntropyBlock_1C8U(const IplImage *src, IplImage *dst)
{
	X7S_FUNCNAME("x7sMosaicBlock()");

	int sBlocks;	//Size of the block
	int cSrc,rDst,cDst, i,b;
	float wRatio, hRatio;
	double entropy, val, np2Blocks;
	uint8_t *pSrc, *pDst;
	uint16_t *pTmpHist;
	IplImage *tmpLine;


	wRatio = (float)src->width/(float)dst->width;
	hRatio = (float)src->height/(float)dst->height;
	sBlocks = (int)wRatio;

	X7S_CHECK_WARN(funcName,wRatio==hRatio,X7S_RET_ERR,"wRatio:%f != hRatio:%f",wRatio,hRatio);
	X7S_CHECK_WARN(funcName,(float)sBlocks==wRatio,X7S_RET_ERR,"nBlocks:%d != wRatio:%f",sBlocks,wRatio);
	X7S_CHECK_WARN(funcName,X7S_IS_IMAGENC(dst,1),X7S_RET_ERR,"nBlocks:%d != wRatio:%f",sBlocks,wRatio);
	X7S_CHECK_WARN(funcName,sBlocks<=256,X7S_RET_ERR,"sBlocks:%d>256",sBlocks);

	//Assign pointers
	pDst = (uint8_t*)dst->imageData;
	pSrc = (uint8_t*)src->imageData;

	//Reserve memory for two lines
	np2Blocks=sBlocks*sBlocks;
	tmpLine = cvCreateImage(cvSize(dst->width,1),IPL_DEPTH_16U,16); //Create a 16 bin histograms, of 16bits
	pTmpHist = (uint16_t*)tmpLine->imageData;



	//Then browse on each line
	for(rDst=0;rDst<dst->height;rDst++)
	{

		//Set the tmpLine with 0
		memset(tmpLine->imageData,0,tmpLine->imageSize);

		//First accumulate on the tmpLine the nBlocks Row.
		for(i=0;i<sBlocks;i++)
		{
			//For each column in the block
			cSrc=0;
			pTmpHist=(uint16_t*)tmpLine->imageData;

			while(cSrc<src->width)
			{
				//Obtain on which bin it belong
				b=(int)*(pSrc++); //Value between [0-255[
				b=b>>4; //Divide by 16
				b=X7S_INRANGE(b,0,16);	//Check that we are in the correct range

				pTmpHist[b]++; //At an occurrence in the bin of the histogram
				cSrc++; //Go to next column

				//If we are going to start a new block
				if(cSrc%sBlocks==0) pTmpHist+=tmpLine->nChannels;
			}
		}


		//Then, compute the mean in the destination
		pTmpHist=(uint16_t*)tmpLine->imageData;
		for(cDst=0;cDst<dst->width;cDst++)
		{
			entropy=0;
			for(b=0;b<tmpLine->nChannels;b++)
			{
				val=(double)pTmpHist[b]/(double)np2Blocks;
				if(val>0) entropy-=(val*x7sLog2(val));	//be careful when val==0 log2(0)=>infinite.
			}
			*(pDst++)=(uint8_t)X7S_MIN(entropy*entropy*tmpLine->nChannels,255);
			pTmpHist+=tmpLine->nChannels;
		}

	}

	//Free the memory of temporary lines
	cvReleaseImage(&tmpLine);
	tmpLine=NULL;

	return X7S_RET_OK;
}


/**
* @brief Set the structure for the parameters of
*  Mosaic Image Difference background model.
*
* The parameters are:

*/
int x7sSetMIDiffBGStatModelParams(X7sMIDiffBGStatModelParams *params) {

	//Check Memory
	if(params==NULL) { return X7S_CV_RET_ERR; }

	//Set default parameters
	params->blockSize=4;
	params->subSeqLogSize=4;		//Value are keep on 32-bits.
	params->nSubSeq=4;			//We have only 10 subseq
	params->threshold=10;
	params->runModulo=2;			//Run each two frames.

	return X7S_CV_RET_OK;
}

/**
* @brief Create and initiate the Background model with the first frame and the parameters.
* @param first_frame A pointer on the first_frame. Format must be BGR
* @param parameters The parameters for the background model.
* @return A generic CvBGStatModel with its specific type.
*/
CvBGStatModel* x7sCreateMIDiffBGModel(IplImage *first_frame ,X7sMIDiffBGStatModelParams *parameters) {

	//Declare variable
	X7sMIDiffBGModel *bg_model;
	CvSize size, bsize;

	CV_FUNCNAME("x7sCreateMIDiffBGModel()");

	//Check input variable.
	if(!CV_IS_IMAGE(first_frame) && first_frame->nChannels!=3) {
		X7S_PRINT_ERROR(cvFuncName,"imI==NULL or nChannels!=3");
		return NULL;
	}

	//Reserved memory for structure.
	bg_model = (X7sMIDiffBGModel*)calloc(sizeof(*bg_model),1);
	bg_model->type=X7S_BG_MODEL_MID;
	bg_model->release=(CvReleaseBGStatModel)ix7sReleaseMIDiffBGModel;
	bg_model->update=(CvUpdateBGStatModel)ix7sUpdateMIDiffBGModel;


	//Init parameters
	if(parameters == NULL)
	{
		x7sSetMIDiffBGStatModelParams(&bg_model->params);
	}
	else
	{
		bg_model->params=*parameters;
	}

	size=cvGetSize(first_frame);
	bsize=cvSize(size.width/bg_model->params.blockSize, size.height/bg_model->params.blockSize);


	//Then reserved the memory for the generic background.
	bg_model->background = cvCreateImage(size,IPL_DEPTH_8U,first_frame->nChannels);
	bg_model->foreground = cvCreateImage(size,IPL_DEPTH_8U,1);


	//Then reserved the specific memory for X7sBGDistModel.
	bg_model->mb_act   = cvCreateImage(bsize,IPL_DEPTH_8U,first_frame->nChannels);
	bg_model->mb_prev = cvCreateImage(bsize,IPL_DEPTH_8U,first_frame->nChannels);
	bg_model->midPeriod = cvCreateImage(bsize,IPL_DEPTH_32S,bg_model->params.nSubSeq);
	cvSet(bg_model->midPeriod,cvScalarAll(0),NULL);


	//Init the first frame
	cvResize(first_frame,bg_model->mb_prev,CV_INTER_AREA);
	cvSet(bg_model->background,cvScalarAll(255),NULL);
	cvSet(bg_model->foreground,cvScalarAll(0),NULL);

	//Generate the LUT for 256 bit count
	x7sBitFastCountGenLUT256();


	return (CvBGStatModel*)bg_model;
}

/**
* @brief Release the memory of the Brightness/Contrast Distortion background model.
*/
static void ix7sReleaseMIDiffBGModel(X7sMIDiffBGModel** p_bg_model)
{
	//Declaration
	X7sMIDiffBGModel* bg_model;
	int i=0;

	//Checking the arguments.
	CV_FUNCNAME( "ix7sReleaseMIDiffBGModel()" );
	if( !p_bg_model ) X7S_PRINT_ERROR(cvFuncName,"Address of pointer is not valid",cvFuncName);
	bg_model= *p_bg_model;
	if(!bg_model) X7S_PRINT_ERROR(cvFuncName,"Pointer is NULL",cvFuncName);

	//Remove generic memory
	cvReleaseImage(&(bg_model->background));
	cvReleaseImage(&(bg_model->foreground));

	for(i=0;i<bg_model->layer_count;i++) cvReleaseImage(&(bg_model->layers[i]));
	free(bg_model->layers);

	//Remove specific memory
	cvReleaseImage(&(bg_model->mb_act));
	cvReleaseImage(&(bg_model->mb_prev));

	//Free the structure memory and set the pointed value unusable.
	free(bg_model);
	(*p_bg_model)=NULL;
}


/**
* @brief Update the background model with the current_frame.
* @param first_frame A pointer on the first_frame in YUV 4:2:2 format (2 channels).
* @param params The parameters for the background model.
* @note The computation of this function is done with floating image.
*/
static int ix7sUpdateMIDiffBGModel(IplImage *curr_frame, X7sMIDiffBGModel* bg_model)
{

	X7S_FUNCNAME("ix7sUpdateMIDiffBGModel()");
	X7S_CHECK_WARN(funcName,bg_model,X7S_RET_ERR,"bgModel is NULL");
	X7S_CHECK_WARN(funcName,X7S_IS_IMAGENC(curr_frame,3),X7S_RET_ERR,"curr_frame is not valid");



	bg_model->nFrames++;


	if(bg_model->params.runModulo>0 &&
			(bg_model->nFrames%bg_model->params.runModulo) !=0)
	{
		x7sAddWeightedScalar(curr_frame,bg_model->foreground,curr_frame,cvScalar(0,0,210,0),bg_model->foreground);
		return X7S_RET_OK;
	}


	X7S_PRINT_INFO(funcName,"frame=%d",bg_model->nFrames);


	//return ix7sUpdateMIDiffBGModel_ColorDiff(curr_frame,bg_model);
	return ix7sUpdateMIDiffBGModel_EdgeDiff(curr_frame,bg_model);



	return X7S_CV_RET_OK;
}


int ix7sUpdateMIDiffBGModel_ColorDiff(IplImage *curr_frame, X7sMIDiffBGModel* bg_model)
{
	X7sMIDiffBGStatModelParams* params;
	int pos, pos_end, p3,p4;
	uint8_t mid;
	int means[3];
	uint8_t *p_act,*p_prev;
	uint32_t *listP=NULL;
	CvSize sizeImPeriod;

	//Set the parameters value
	params=(X7sMIDiffBGStatModelParams*)&(bg_model->params);

	sizeImPeriod = cvGetSize(bg_model->midPeriod);
	x7sReallocImage(sizeImPeriod,IPL_DEPTH_32F,1,&imMean);
	x7sReallocImage(sizeImPeriod,IPL_DEPTH_32F,1,&imVar);
	x7sReallocImage(sizeImPeriod,IPL_DEPTH_32F,1,&imNNZ);
	x7sReallocImage(sizeImPeriod,IPL_DEPTH_32F,1,&imVar2);
	x7sReallocImage(sizeImPeriod,IPL_DEPTH_32F,1,&imOC);
	x7sReallocImage(sizeImPeriod,IPL_DEPTH_8U,1,&imTmp);


	x7sCvtColorRealloc(curr_frame,&imGrey,CV_BGR2GRAY);


	if(imEntro==NULL)
	{
		imEntro = cvCreateImage(
				cvSize(bg_model->mb_act->width/2,bg_model->mb_act->height/2),
				IPL_DEPTH_8U,1);
	}
	x7sMosaicEntropyBlock_1C8U(imGrey,imEntro);


	listP=(uint32_t*)calloc(sizeof(uint32_t),bg_model->midPeriod->nChannels);

	//Create the block image
	//cvResize(curr_frame,bg_model->mb_act,CV_INTER_AREA);
	x7sMosaicMeanBlock(curr_frame,bg_model->mb_act);

	//Defining shortcut variable
	p_act   = (uint8_t*)bg_model->mb_act->imageData;
	p_prev = (uint8_t*)bg_model->mb_prev->imageData;
	pos_end=bg_model->mb_prev->width*bg_model->mb_prev->height;
	pos=0;

	while(pos<pos_end)
	{
		p3=pos*3;
		p4=pos*4;

		//Maybe improve this computation
		means[0]=p_act[p3+0]-p_prev[p3+0];
		means[1]=p_act[p3+1]-p_prev[p3+1];
		means[2]=p_act[p3+2]-p_prev[p3+2];

		mid=(uint8_t)sqrt(means[0]*means[0] + means[1]*means[1] + means[2]*means[2]);


		imDiff->imageData[pos]=mid;
		mid=(mid > bg_model->params.threshold);
		imBin->imageData[pos]=mid*255;

		ix7sMIDDiffBGModel_PushBack32(bg_model->midPeriod, pos, mid);
		mid=ix7sMIDDiffBGModel_ComputeStats(bg_model->midPeriod,pos,listP);
		imTmp->imageData[pos]=mid;

		if(pos==IX7S_PRINT_POS)
		{
			p_act[p3+0]=0;
			p_act[p3+1]=0;
			p_act[p3+2]=255;
		}

		pos++;
	}



	cvCopy(bg_model->mb_act,bg_model->mb_prev,NULL);

	cvThreshold(imEntro,imEntro,150,255,CV_THRESH_TOZERO);
	x7sPseudoMed(imTmp,imTmp,4);
	x7sPseudoMed(imEntro,imEntro,5);
	cvResize(imEntro,imGrey,CV_INTER_AREA);
	cvResize(imTmp,bg_model->foreground,CV_INTER_AREA);
	cvAnd(imGrey,bg_model->foreground,bg_model->foreground,NULL);

	x7sAddWeightedScalar(curr_frame,bg_model->foreground,curr_frame,cvScalar(0,0,210,0),bg_model->foreground);



	free(listP);



	cvNamedWindow("imAct");
	cvShowImage("imAct",bg_model->mb_act);
	cvNamedWindow("Diff");
	cvShowImage("Diff",imDiff);
	cvNamedWindow("imBin");
	cvShowImage("imBin",imBin);
	cvNamedWindow("Mean");
	x7sShowImageRescale("Mean",imMean);
	cvNamedWindow("Var");
	x7sShowImageRescale("Var",imVar);
	cvNamedWindow("NNZ");
	cvShowImage("NNZ",imNNZ);
	cvNamedWindow("Var2");
	x7sShowImageRescale("Var2",imVar2);

	cvNamedWindow("Tmp");
	cvShowImage("Tmp",imTmp);

	cvNamedWindow("imEntro");
	cvShowImage("imEntro",imEntro);

	return X7S_CV_RET_OK;

}

int ix7sUpdateMIDiffBGModel_EdgeDiff(IplImage *curr_frame, X7sMIDiffBGModel* bg_model)
{
	X7sMIDiffBGStatModelParams* params;
	uint32_t *listP;
	int pos, pos_end;
	uint8_t mid;
	uint8_t *p_act;
	double min, max;
	CvSize sizeImPeriod;


	//Set the parameters value
	params=(X7sMIDiffBGStatModelParams*)&(bg_model->params);


	sizeImPeriod = cvGetSize(bg_model->midPeriod);
	x7sReallocImage(sizeImPeriod,IPL_DEPTH_32F,1,&imMean);
	x7sReallocImage(sizeImPeriod,IPL_DEPTH_32F,1,&imVar);
	x7sReallocImage(sizeImPeriod,IPL_DEPTH_32F,1,&imNNZ);
	x7sReallocImage(sizeImPeriod,IPL_DEPTH_32F,1,&imVar2);
	x7sReallocImage(sizeImPeriod,IPL_DEPTH_32F,1,&imOC);
	x7sReallocImage(sizeImPeriod,IPL_DEPTH_8U,1,&imTmp);


	listP=(uint32_t*)calloc(sizeof(uint32_t),bg_model->midPeriod->nChannels);

	x7sCvtColorRealloc(curr_frame,&imGrey,CV_BGR2GRAY);
	x7sReallocImage(cvGetSize(imGrey),IPL_DEPTH_16S,1,&imActEdge);
	x7sReallocImage(cvGetSize(imGrey),IPL_DEPTH_16S,1,&imPrevEdge);
	x7sReallocColorImage(imGrey,&imDiff,CV_BGR2GRAY);
	x7sReallocColorImage(bg_model->mb_act,&imBin,CV_BGR2GRAY);
	x7sReallocColorImage(bg_model->mb_act,&imDiffSmall,CV_BGR2GRAY);

//	cvSobel(imGrey,imActEdge,1,1,5);
//	cvAbsDiff(imPrevEdge,imActEdge,imPrevEdge);
//	cvMinMaxLoc(imPrevEdge, &min,&max,NULL,NULL,NULL);
//	//cvConvertScale(imPrevEdge,imDiff,.007,0); //Convert to .007
//	cvConvertScale(imPrevEdge,imDiff,.07,0);

	cvSmooth(imGrey,imDiff,CV_BLUR, 3, 3,0,0);
	cvCanny(imDiff,imDiff,100,150,3);
	cvMinMaxLoc(imDiff, &min,&max,NULL,NULL,NULL);

	X7S_PRINT_INFO("ix7sUpdateMIDiffBGModel_EdgeDiff()","min=%f, max=%f",min,max);

	x7sMosaicMeanBlock(imDiff,imDiffSmall);

	cvNamedWindow("imGrey");
	x7sShowImageRescale("imGrey",imGrey);
	cvNamedWindow("Edge");
	x7sShowImageRescale("Edge",imActEdge);
	cvNamedWindow("EdgeDiff");
	x7sShowImageRescale("EdgeDiff",imPrevEdge);
	cvNamedWindow("Diff");
	cvShowImage("Diff",imDiff);
	cvNamedWindow("DiffSmall");
	cvShowImage("DiffSmall",imDiffSmall);




	cvCopy(imActEdge,imPrevEdge,NULL);





	//Defining shortcut variable

	p_act   = (uint8_t*)imDiffSmall->imageData;
	pos_end=imDiffSmall->width*imDiffSmall->height;

	pos=0;
	while(pos<pos_end)
	{

		mid=(p_act[pos] > 10);
		imBin->imageData[pos]=(mid)?255:0;

		ix7sMIDDiffBGModel_PushBack32(bg_model->midPeriod, pos, mid);
		mid = ix7sMIDDiffBGModel_ComputeStats(bg_model->midPeriod,pos,listP);
		imTmp->imageData[pos]=mid;

		pos++;
	}



	x7sPseudoMed(imTmp,imTmp,4);
	cvResize(imTmp,bg_model->foreground,CV_INTER_AREA);
	x7sAddWeightedScalar(curr_frame,bg_model->foreground,curr_frame,cvScalar(0,0,210,0),bg_model->foreground);



	free(listP);



	cvNamedWindow("imAct");
	cvShowImage("imAct",bg_model->mb_act);



	cvNamedWindow("imBin");
	cvShowImage("imBin",imBin);

	cvNamedWindow("Mean");
	x7sShowImageRescale("Mean",imMean);
	cvNamedWindow("Var");
	x7sShowImageRescale("Var",imVar);
	cvNamedWindow("NNZ");
	cvShowImage("NNZ",imNNZ);
	cvNamedWindow("Var2");
	x7sShowImageRescale("Var2",imVar2);
	cvNamedWindow("OC");
	x7sShowImageRescale("OC",imOC);

	cvNamedWindow("Tmp");
	cvShowImage("Tmp",imTmp);

	return X7S_CV_RET_OK;

}








/**
*
*/
uint8_t ix7sMIDDiffBGModel_PushBack32(IplImage *layers, int pos, uint8_t push_value)
{
	int l;
	uint32_t *last;

	//		char buff[34];

	last = (uint32_t*)(layers->imageData + pos*4*layers->nChannels);

	if(pos==IX7S_PRINT_POS) printf("\n diff=%d\n",push_value);

	for(l=layers->nChannels-1;l>0;l--)
	{

		push_value = x7sBitPushBack32(last+l,push_value);
		//		x7sBitToCString32(last[l],buff);
		//		if(pos==IX7S_PRINT_POS)	printf("l=%02d: %s   next=%d\n",l,buff,push_value);
	}


	return push_value;
}



uint8_t ix7sMIDDiffBGModel_ComputeStats(IplImage *layers,  int pos, uint32_t * listP)
{
	int l;
	uint32_t sum;
	uint8_t fg_pix=0;

	float Pl;
	float mt=0;	//!< Mean time
	float var=0;	//!< Variance of mean time
	float nnz=0;	//!< Number of non-zeros
	float mt2=0;	//!< Mean time
	float var2=0;	//!< Variance of mean time
	float tmp;

	float *pMt=(float*)imMean->imageData;
	float *pVar=(float*)imVar->imageData;
	float *pNNZ=(float*)imNNZ->imageData;
	float *pVar2=(float*)imVar2->imageData;
	float *pOC=(float*)imOC->imageData;



	uint32_t *last = (uint32_t*)(layers->imageData + pos*4*layers->nChannels);
	int nSubSec=layers->nChannels;///2;

	//Compute P_{l}(m,n}
	sum=0;
	for(l=0;l<nSubSec;l++)
	{
		listP[l]=(uint32_t)x7sBitFastCount32(last[l]);
		//listP[l]+=(uint32_t)x7sBitFastCount32(last[l*2+1]);
		sum+=listP[l];

		tmp =  x7sBitCountConsec32(last[l],-1);
		mt2 += tmp;
		var2+= tmp*tmp;
	}

	mt2=mt2/nSubSec;


	pOC[pos] = x7sBitCountConsec32(last[nSubSec-1],1);


	//Then Compute
	for(l=0;l<nSubSec;l++)
	{
		Pl=(sum>0)?(float)listP[l]/(float)sum:0;
		mt+=l*Pl;
		var+=(float)pow(((l-mt)),2)*Pl;
		nnz+=(Pl>0);

		var2+=Pl*Pl;
	}

	var2=var2/nSubSec - mt2*mt2;



	if(imMean && imVar && imNNZ && imVar2)
	{
		pMt[pos]=mt;
		pVar[pos]=var;
		pNNZ[pos]=nnz/nSubSec;
		pVar2[pos]=var2;
	}

	if(IX7S_PRINT_POS==0)
	{
		X7S_PRINT_DEBUG("ix7sMIDDiffBGModel_ComputeStats()","#%d: mt=%0.3f, var=%0.3f, nnz=%0.3f ",
				pos,mt,var,nnz);
	}


	if(nnz > nSubSec*0.7) fg_pix=255;


	return fg_pix;
}




