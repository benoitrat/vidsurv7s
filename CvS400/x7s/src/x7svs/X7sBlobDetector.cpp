#include "X7sBlobDetector.hpp"
//========================================================================
//-- Internal include to fasten compilation.
#include <highgui.h>
#include <vector>
#include "X7sVSLog.hpp"

const char* ix7sBlobNames[X7S_VS_BD_END] =
{
		"BD_Simple",
		"BD_CC",
		"BD_RL",
		"BD_LRLE"
};



///////////////////////////				 X7sBlobDetector	   		 //////////////////////////


//========================================================================
//-- Constructor, Destructor and Init.


/**
* @brief Default constructor.
*
* Create a list of composed component (at most 255).
*/
X7sBlobDetector::X7sBlobDetector()
:m_pBlobLists(NULL), m_sizeBlobList(0)
{
	m_pConComList = x7sCreateConCompoList(255);

	m_isDrawDebug=FALSE;
	AddParam((x7scharA*)"isDrawDebug",&m_isDrawDebug);
	CommentParam((x7scharA*)"isDrawDebug", (x7scharB*)"Draw image to debug tracking");

	m_distFilterStatic=3;
	AddParam((x7scharA*)"distFilterStatic",&m_distFilterStatic);
	CommentParam((x7scharA*)"distFilterStatic", (x7scharB*)"Set the distance in pixel to filter static object d=[0,3]");

	m_minRatio=0.02f;
	AddParam((x7scharA*)"minAreaRatio",&m_minRatio);
	CommentParam((x7scharA*)"minAreaRatio", (x7scharB*)"Minimum area size of a blob in % of the image size");

	m_onlyFullBlob=FALSE;
	AddParam((x7scharA*)"onlyFullBlob",&m_onlyFullBlob);
	CommentParam((x7scharA*)"onlyFullBlob", (x7scharB*)"If @TRUE insert blob only when it doesn't touch the border.");

	m_mergeMaxArea=64.f;
	AddParam((x7scharA*)"mergeMaxArea",&m_mergeMaxArea);
	CommentParam((x7scharA*)"mergeMaxArea", (x7scharB*)"Minimum area size of a blob in % of the image size");

	m_mergeMaxDist=20.f;
	AddParam((x7scharA*)"mergeMaxDist",&m_mergeMaxDist);
	CommentParam((x7scharA*)"mergeMaxDist", (x7scharB*)"Minimum area size of a blob in % of the image size");

	m_mergeMaxRatioArea=0.7f;
	AddParam((x7scharA*)"mergeMaxRatioArea",&m_mergeMaxRatioArea);
	CommentParam((x7scharA*)"mergeMaxRatioArea", (x7scharB*)"Minimum area size of a blob in % of the image size");


}

/**
* @brief Default destuctor.
*
* Delete the history of blobList.
*/
X7sBlobDetector::~X7sBlobDetector()
{
	SetNickName(NULL);	// Release the nick name
	x7sReleaseConCompoList(&m_pConComList);
	if(m_pBlobLists) {
		for(int i=0;i<m_sizeBlobList;i++) 	X7S_DELETE_PTR(m_pBlobLists[i]);
		delete [] m_pBlobLists;
	}
}

//========================================================================
//-- Protected Methods.


/**
* @brief Shift the queue history blob list.
*/
void X7sBlobDetector::ShiftQueueHistoryBlobList()
{
	int     i;
	if(m_pBlobLists[0]) delete m_pBlobLists[0];
	for(i=1;i<m_sizeBlobList;++i) m_pBlobLists[i-1]=m_pBlobLists[i];
	m_pBlobLists[m_sizeBlobList-1] = new CvBlobSeq;
}

/**
* @brief Get a blob list given using RL and Connected Component.
*
* While we are processing the Connected Component we try to link different small
* connected component to other bigger. This is done calling the function:
* X7sBlobDetector::MergeSmallConCompo().
*
* @param pFGMask The foreground mask in black and white.
* @param pBlobList A pointer on an empty blob list to Fill.
* @see Cv7ConCompoList.
*/
void X7sBlobDetector::GetNewBlobList(IplImage* pFGMask,CvBlobSeq *pBlobList)
{

	//Update the list of connected component
	IplImage *pImCC = cvCreateImage(cvGetSize(pFGMask),IPL_DEPTH_8U,1);
	cvCopy(pFGMask,pImCC);
	x7sUpdateConCompoList(m_pConComList,pImCC,false);

	IplImage *pImDbg=NULL;


	if(m_Wnd) {
		pImDbg = cvCreateImage(cvGetSize(pFGMask),IPL_DEPTH_8U,1);
		cvCopy(pImCC,pImDbg);
		cvNamedWindow("CCDraw",CV_WINDOW_AUTOSIZE);
		cvNamedWindow("CCDraw2",CV_WINDOW_AUTOSIZE);
		x7sDrawConCompoList(pImCC,m_pConComList);
		x7sShowImageRescale("CCDraw",pImCC);
		cvWaitKey(2);
	}

	MergeSmallConCompo(m_pConComList);

	if(m_Wnd)
	{
		x7sDrawConCompoList(pImDbg,m_pConComList);
		x7sShowImageRescale("CCDraw2",pImDbg);
		cvWaitKey(2);
		cvReleaseImage(&pImDbg);
	}

	cvReleaseImage(&pImCC);
	pImCC=NULL;

	//For each connected component found.
	X7sConCompo *cc;
	for(int i=0;i<m_pConComList->size;i++)
	{
		cc = &(m_pConComList->vec[i]);
		if(cc==NULL || cc->area<=0) continue;

		CvBlob NewBlob = cvBlob((float)cc->x,(float)cc->y,(float)(cc->max_x-cc->min_x),(float)(cc->max_y-cc->min_y));
		NewBlob.ID=i;
		pBlobList->AddBlob(&NewBlob);
	}
}


/**
* @brief Compute the best trajectory using the OpenCV way and add it to the pNewBlobList.
*
* Analyze blob history to find best blob trajectory:
*
* 	- Check, If the queue history has at least one blob in each time layer.
* 	- Then for each tupple (t=time,i=id_blob), obtain its trajectory:
*       //TOCHECK: If it is necessary!
* 		- First, check if the more recent blob t=t_last is intersected with the old list
* 		- Then, check if distance to image boarder of the latest blob!
* 		- Finally, check uniform motion.
*
* @note The pBLIndex is like a i, j loop.
*/
void X7sBlobDetector::FindBestTraj(IplImage *pFG, CvBlobSeq* pNewBlobList, CvBlobSeq* pOldBlobList)
{

	bool result=false;
	CvSize S = cvGetSize(pFG);
	IplImage* pImDbg = NULL;

	X7S_FUNCNAME("X7sBlobDetector::FindBestTraj()");
	X7S_CHECK_WARN(funcName,pFG,,"pFG is NULL");
	X7S_CHECK_WARN(funcName,pNewBlobList,,"pNewBlobList is NULL");
	X7S_CHECK_WARN(funcName,pOldBlobList,,"pOldBlobList is NULL");

	if(m_isDrawDebug)
	{
		pImDbg = cvCreateImage(cvGetSize(pFG),IPL_DEPTH_8U,3);	//3 Channels to draw the image
		cvCvtColor(pFG,pImDbg,CV_GRAY2BGR);
		cvThreshold(pImDbg,pImDbg,0,255,CV_THRESH_BINARY);
		cvNamedWindow("Blob Best");
	}

	int     Count = 0;
	int*    pBLIndex = new int[m_sizeBlobList];
	int*   pBL_BEST = new int[m_sizeBlobList];
	int     i;
	int     finish = 0;
	double  BestError = -1;
	int     Good = 1;

	//reset queue list.
	for(i=0; i<m_sizeBlobList; ++i)
	{
		pBLIndex[i] = 0;	//Contains the history index for a specific blob (the first blob=0).
		pBL_BEST[i] = 0;
	}

	//Check if queue history (@ref m_pBlobLists) as at
	//least one blob in each frame of the history.
	for(i=0; Good && (i<m_sizeBlobList); ++i)
		if(m_pBlobLists[i] == NULL || m_pBlobLists[i]->GetBlobNum() == 0)
			Good = 0;

	//----------------------------------------

	//If queue history is complete or good (for this frame)
	if(Good) {
		do { /* For each configuration history (loop on pBLIndex[i]) */
			CvBlob** pBL  = new CvBlob*[m_sizeBlobList];	//Temporary blob list
			int     Good = 1;
			double  Error = 0;

			//Take the most recent blob list in the queue history, and find the one corresponding to our configuration using pBLIndex
			CvBlob* pBNew = m_pBlobLists[m_sizeBlobList-1]->GetBlob(pBLIndex[m_sizeBlobList-1]);

			//Copy pointer of the queue history for this blob.
			for(i=0; i<m_sizeBlobList; ++i)
				pBL[i] = m_pBlobLists[i]->GetBlob(pBLIndex[i]);


			// Check intersection between more recent and old other blobs.
			if(Good && pOldBlobList)
			{
				for(int k=pOldBlobList->GetBlobNum(); k>0; --k)
				{
					//Check intersection.
					CvBlob* pBOld = pOldBlobList->GetBlob(k-1);
					if(IX7S_BLOB_INTERSECT(pBOld,pBNew)) Good =0;

					//Draw when the intersection occurs.
					if(IX7S_BLOB_INTERSECT(pBOld,pBNew) && m_isDrawDebug)
					{
						X7S_PRINT_DUMP("X7sBlobDetector::FindBestTraj()",
								"Intersection found (Not Good) with new=%d,old=%d",pBNew->ID,pBOld->ID);
						x7sDrawCross(pImDbg,cvPoint((int)pBNew->x,(int)pBNew->y),cvScalar(255,150,50),3);
						cvShowImage("Blob Best",pImDbg);
						cvWaitKey(0);
					}

				}
			}

			//If no intersection

			// Check distance to image border:
			if(Good && m_onlyFullBlob)
			{   /* Check distance to image border: */
				CvBlob*  pB = pBNew;
				float    dx = MIN(pB->x,S.width-pB->x)/CV_BLOB_RX(pB);
				float    dy = MIN(pB->y,S.height-pB->y)/CV_BLOB_RY(pB);

				if(dx < 1.1 || dy < 1.1) {
					X7S_PRINT_DUMP("X7sBlobDetectorRL::DetectNewBlob()",
							"Blob %d Near Image Boarder (Not Good)",pB->ID);
					Good = 0;
				}
			}   /* Check distance to image border. */


			/* Check uniform motion: */
			if(Good)
			{
				int     N = m_sizeBlobList;
				float   sum[2] = {0,0};
				float   jsum[2] = {0,0};
				float   diff[2] = {0,0};
				float 	old[2]	= {0,0};
				float   a[2],b[2]; /* estimated parameters of moving x(t) = a*t+b*/

				int j;
				for(j=0; j<N; ++j)
				{
					float   x = pBL[j]->x;
					float   y = pBL[j]->y;
					sum[0] += x;
					jsum[0] += j*x;
					sum[1] += y;
					jsum[1] += j*y;

					if(j>0) {
						diff[0]=old[0]-x;
						diff[1]=old[1]-y;
					}

					old[0]=x;
					old[1]=y;
				}

				a[0] = 6*((1-N)*sum[0]+2*jsum[0])/(N*(N*N-1));
				b[0] = -2*((1-2*N)*sum[0]+3*jsum[0])/(N*(N+1));
				a[1] = 6*((1-N)*sum[1]+2*jsum[1])/(N*(N*N-1));
				b[1] = -2*((1-2*N)*sum[1]+3*jsum[1])/(N*(N+1));

				for(j=0; j<N; ++j)
				{
					Error +=
							pow(a[0]*j+b[0]-pBL[j]->x,2)+
							pow(a[1]*j+b[1]-pBL[j]->y,2);
				}

				Error = sqrt(Error/N);

				if( Error > S.width*0.01 ||
						fabs(a[0])>S.width*0.1 ||
						fabs(a[1])>S.height*0.1)
				{
					Good = 0;
				}

				//IMPROVE: How static blob are handle.
				//If the trajectory is uniform, check if it is static.
				else if(m_distFilterStatic>0)
				{
					if((fabs(diff[0]) + fabs(diff[1])) <  m_distFilterStatic)
					{
						Good=0;
						X7S_PRINT_DUMP("X7sBlobDetectorRL::DetectNewBlob()",
								"Blob %d is Static (mvt (x=%.1f+y=%.1f)<%.1f) (Not Good)",
								pBNew->ID,fabs(diff[0]),fabs(diff[1]),m_distFilterStatic);
					}
				}


			}   /* Check configuration. */



			/* New best trajectory: */
			if(Good && (BestError == -1 || BestError > Error))
			{
				for(i=0; i<m_sizeBlobList; ++i)
				{
					pBL_BEST[i] = pBLIndex[i];
				}
				BestError = Error;


				if(m_isDrawDebug) //Draw in Green the best blob just added.
				{
					CvBlobSeq blobSeq;
					for(int n=0;n<m_sizeBlobList;n++)
						blobSeq.AddBlob(m_pBlobLists[n]->GetBlob(pBL_BEST[n]));
					x7sDrawCvBlobSeq(pImDbg,&blobSeq,IX7S_GREEN);
					cvShowImage("Blob Best",pImDbg);
					//					printf("Best Motion: ");
					//					printf("count=%d >> ",Count);
					//					for(int n=0; n<m_sizeBlobList; ++n) {
					//						printf("%d ", pBLIndex[n]);
					//					}
					//					printf("\n");
					cvWaitKey(0);
				}

			}   /* New best trajectory. */



			if(m_isDrawDebug)
			{
				CvBlobSeq blobSeq;
				for(int n=0;n<m_sizeBlobList;n++)
					blobSeq.AddBlob(m_pBlobLists[n]->GetBlob(pBLIndex[n]));
				x7sDrawCvBlobSeq(pImDbg,&blobSeq,IX7S_CYAN);
				cvShowImage("Blob Best",pImDbg);
				cvCvtColor(pFG,pImDbg,CV_GRAY2BGR);
				cvThreshold(pImDbg,pImDbg,0,255,CV_THRESH_BINARY);
			}



			//Go to the next configuration:
			Count++;
			for(i=0; i<m_sizeBlobList; ++i)
			{
				pBLIndex[i]++;
				if(pBLIndex[i] != m_pBlobLists[i]->GetBlobNum()) break;
				pBLIndex[i]=0;
			}   /* Next time shift. */

			if(i==m_sizeBlobList) finish=1;


			if(pBL) delete [] pBL;
			pBL=NULL;

		} while(!finish);	/* Check next time configuration of connected components. */
	}


	//----------------------------------------

	if(m_isDrawDebug)
	{
		CvBlobSeq blobSeq;
		for(int i=0;i<m_sizeBlobList;i++)
			blobSeq.AddBlob(m_pBlobLists[m_sizeBlobList-1]->GetBlob(pBL_BEST[i]));
		x7sDrawCvBlobSeq(pImDbg,&blobSeq,IX7S_MAGENTA);
	}

	if(BestError != -1)
	{   /* Write new blob to output and delete from blob list: */
		X7S_PRINT_DEBUG("X7sBlobDetectorRL::FindBestTraj()",
				"Add only one blob=%d",pBL_BEST[m_sizeBlobList-1]);
		CvBlob* pNewBlob = m_pBlobLists[m_sizeBlobList-1]->GetBlob(pBL_BEST[m_sizeBlobList-1]);
		pNewBlobList->AddBlob(pNewBlob);

		//Delete blob trajectory from the history.
		for(i=0; i<m_sizeBlobList; ++i)
		{   /* Remove blob from each list: */
			m_pBlobLists[i]->DelBlob(pBL_BEST[i]);
		}   /* Remove blob from each list. */


		result = true;

	}   /* Write new blob to output and delete from blob list. */

	if(m_isDrawDebug && pImDbg) cvReleaseImage(&pImDbg);

	//Remove the pBL list
	if(pBLIndex) delete [] pBLIndex;
	if(pBL_BEST) delete [] pBL_BEST;
}


/**
*
* @brief Merge small small connected component to their neighbors.
*
* The parameters to merge two connected component are:
* 		- The maximum area that a blob can have before to be merged.
* 		- The distance between their border.
* 		- The ratio between their size.
*
* @param ccList Pointer on a List of Connected Components, which is going to be modify.
* @warning The small connected component merge with bigger one are not really remove from the list,
* but their area=0, and their position is negative. It is therefore easy to remove them in a second step.
*
*
* @return @true, when at least one blob merged into another.
*/
bool X7sBlobDetector::MergeSmallConCompo(X7sConCompoList *ccList)
{

	x7sSortConCompoList(ccList,false);	//Sort the connected component in descending order.

	int N=ccList->size;
	std::vector<int> mergeID(N,-1); //Set the vector of merging ID to -1
	std::vector<float> bestValue(N,m_mergeMaxDist); //Set the vector of merging ID to -1
	X7sConCompo *pCComI=NULL, *pCComJ=NULL;
	X7sConCompo *pCCList = (X7sConCompo *)ccList->vec;

	float cdist, ratio;
	float ratio_small, ratio_med, ratio_high;

	ratio_small=m_mergeMaxRatioArea/3;
	ratio_med=2*ratio_small;
	ratio_high=m_mergeMaxRatioArea;


	//Loop from the last one (smallest) to the second one (2nd biggest)
	for(int i=N-1;i>0;i--) {
		pCComI=&(pCCList[i]);

		//If the blob area is smaller than the maximum area for merging , break the loop.
		if(pCComI->area >= m_mergeMaxArea) break;

		//Loop from the bigger one to the biggest [biggest, ..., j start, i one, ..., smallest]
		for(int j=i-1;j>=0;j--) {
			pCComJ=&(pCCList[j]);

			ratio=(float)pCComI->area/(float)pCComJ->area;
			cdist=(float)IX7S_BLOB_CDISTANCE(pCComI,pCComJ);

			if(cdist < m_mergeMaxDist) {
				//Here we have a special rule depending on the ratio
				cdist*=ratio;
				if(cdist<bestValue[i])
				{
					mergeID[i]=j;
					bestValue[i]=cdist;
				}
			}
		}
	}


	X7sConCompo *pCcA, *pCcB;
	bool one_merge=false;

	//Finally merge the blobs using the mergeID vector.
	for(int i=N-1;i>0;i--)
	{
		if(mergeID[i]!=-1)
		{
			//Merge (A) into ().
			pCcA = &(ccList->vec[i]);
			pCcB = &(ccList->vec[mergeID[i]]);

			if(pCcA && pCcB) //if both are available.
			{
				//Top left corner
				pCcB->min_x=X7S_MIN(pCcB->min_x,pCcA->min_x);
				pCcB->min_y=X7S_MIN(pCcB->min_y,pCcA->min_y);

				//Bottom right corner.
				pCcB->max_x=X7S_MAX(pCcB->max_x,pCcA->max_x);
				pCcB->max_y=X7S_MAX(pCcB->max_y,pCcA->max_y);

				//Compute the center of the bounding box.
				pCcB->x=(int)(((float)(pCcB->max_x+pCcB->min_x))/2.f);
				pCcB->y=(int)(((float)(pCcB->max_y+pCcB->min_y))/2.f);


				//And sum the area of A.
				pCcB->area+=pCcA->area;

				one_merge=true;

				X7S_PRINT_DUMP("X7sBlobDetectorRL::MergeSmallConCompo()",
						"Connected Compo Merged: #%d > #%d",i, mergeID[i]);
			}

			//The disable the one merged.
			pCcA->area=0;
			pCcA->x=-1;
			pCcA->y=-1;
		}
	}

	return one_merge;
}


//========================================================================
//-- Static Methods.

/**
* @brief Return true if the blob is smaller than the given size.
*/
bool X7sBlobDetector::IsBlobTooSmall(CvBlob *pBlob,int width, int height)
{
	return (pBlob->w < width || pBlob->h < height);
}

/**
* @brief Return true if the blob is smaller than the ratio of the image.
* Typically this ratio is about 2% of the image (ratio=0.02)
*
* TOCHECK: Maybe put that it should be smaller than both.
*/
bool X7sBlobDetector::IsBlobTooSmall(CvBlob *pBlob,const CvSize &FGSize, float ratio)
{
	float w=FGSize.width*ratio;
	float h=FGSize.height*ratio;
	float area=w*h*1.5f;

	if((pBlob->h < h) || (pBlob->w < w)) {	//Check if one of the dimension is smaller.
		if(IX7S_BLOB_AREA(pBlob) < area) {	//Check if the total size is less than 1.5 time the size of both.
			X7S_PRINT_DUMP("X7sBlobDetectorRL::IsBlobTooSmall()",
					"Blob #%d is too small (area=%.0f=%.0fx%.0f < %.1f)",
					pBlob->ID,IX7S_BLOB_AREA(pBlob),pBlob->w,pBlob->h,area);
			return true;
		}
	}
	return false;
}

/**
* @brief Look if the blob is intersected with a blob in the listç
*
*/
bool X7sBlobDetector::IsBlobIntersect(CvBlob *pBlob,CvBlobSeq* pBlobList)
{
	//Delete the intersected blob.
	if(pBlobList)
	{
		for(int j=pBlobList->GetBlobNum(); j>0; j--)
		{
			CvBlob* pBOld = pBlobList->GetBlob(j-1);
			if(IX7S_BLOB_INTERSECT(pBOld,pBlob))
			{
				X7S_PRINT_DUMP("X7sBlobDetector::IsBlobIntersect()",
						"Intersection found with new=%d, old=%d",
						pBlob->ID,pBOld->ID);
				return true;
			}
		}   /* Check next old blob. */
	}   /*  if pOldBlobList */
	return false;
}

/**
* @brief Give the intersection area of the smallest blob.
*
* if the blob are inversed then the intersection area is negatively signed.
* The intersection area is given by the percentage of the small blob into the big blob.
*
*/
float X7sBlobDetector::IntersectionRatio(CvBlob *pBBig, CvBlob *pBSmall)
{
	int sign=1;
	float area=0;
	if(IX7S_BLOB_INTERSECT(pBBig,pBSmall))
	{
		//Check if pBBig is the biggest blob.
		if(!IX7S_BLOB1_BIGGER_BLOB2(pBBig,pBSmall))
		{
			CvBlob *pTmp;
			CV_SWAP(pBBig,pBSmall,pTmp);	//Swap internally, big and small
			sign=-1;						//Set Negative sign because the order is not respected.
		}

		CvPoint2D32f TL, BR;
		CvPoint2D32f BigTL = IX7S_BLOB_TLCORNER(pBBig);
		CvPoint2D32f BigBR = IX7S_BLOB_BRCORNER(pBBig);
		CvPoint2D32f SmallTL = IX7S_BLOB_TLCORNER(pBSmall);
		CvPoint2D32f SmallBR = IX7S_BLOB_BRCORNER(pBSmall);

		TL.x=X7S_MAX(BigTL.x,SmallTL.x);
		TL.y=X7S_MAX(BigTL.y,SmallTL.y);
		BR.x=X7S_MIN(BigBR.x,SmallBR.x);
		BR.y=X7S_MIN(BigBR.y,SmallBR.y);

		area=(float)((BR.x-TL.x)*(BR.y-TL.y));
		area= (float)(sign*area)/(float)IX7S_BLOB_AREA(pBSmall);

	}
	return area;
}


/**
* @brief Bubble sort the list.
*/
void X7sBlobDetector::SortBlobList(CvBlobSeq *pBlobList)
{
	int N = pBlobList->GetBlobNum();
	int i,j;
	for(i=1; i<N; ++i)
	{
		for(j=i; j>0; --j)
		{
			CvBlob  temp;
			CvBlob* pP = pBlobList->GetBlob(j-1);
			CvBlob* pN = pBlobList->GetBlob(j);
			if(IX7S_BLOB_AREA(pN) < IX7S_BLOB_AREA(pP)) break;
			CV_SWAP(pN[0],pP[0],temp);
		}
	}
}



/**
* @brief Bubble sort the given blob list by their area and copy the N-First in the
* the most recent layer of m_pBlobLists.
* @param pBlobList A pointer on a blob list.
* @param Nfirst The Number of blob to copy, if N first is negative we copy all of them.
*/
void X7sBlobDetector::SetNBestRecentBlob(CvBlobSeq * pBlobList, int Nfirst)
{

	if(pBlobList==NULL) return;

	//First order the blobs by size using bubble sort
	SortBlobList(pBlobList);


	if(Nfirst<0 || Nfirst>pBlobList->GetBlobNum()) {
		Nfirst=pBlobList->GetBlobNum();
	}

	// Copy only first the Nfirst blobs
	for(int i=0; i<Nfirst; ++i)
	{
		m_pBlobLists[m_sizeBlobList-1]->AddBlob(pBlobList->GetBlob(i));
	}

}


//========================================================================
//-- Public function and interfaces.


/**
* @brief Create a blob detector of the given type.
*
*/
CvBlobDetector* x7sCreateBlobDetector(int type)
{
	CvBlobDetector *module=NULL;
	switch(type)
	{
	case X7S_VS_BD_SIMPLE: 	module = cvCreateBlobDetectorSimple(); 	break;
	case X7S_VS_BD_CC: 		module = cvCreateBlobDetectorCC();		break;
	case X7S_VS_BD_CV:		module = x7sCreateBlobDetectorCV();		break;
	case X7S_VS_BD_CCCM:	module = x7sCreateBlobDetectorCCCM();		break;
	case X7S_VS_BD_LRLE:	module = x7sCreateBlobDetectorLRLE();	break;
	default:
		X7S_PRINT_WARN("x7sCreateBlobDetector()","No Blob detector for type %d",type);
		return NULL;
	}
	if(module) {
		char buff[20];
		snprintf(buff,20,"%d",type);
		module->SetNickName((x7scharA*)buff);
		X7S_PRINT_DEBUG("x7sCreateBlobDetector()","with name: %s",module->GetNickName());
	}
	return module;
}






void x7sDrawCvBlobList(IplImage *pImg,CvBlob* ar_blobs, int nof_blobs, int hue_color)
{
	CvBlobSeq blobSeq;
	for(int i=0;i<nof_blobs;i++) {
		blobSeq.AddBlob(&ar_blobs[i]);
	}
	x7sDrawCvBlobSeq(pImg,&blobSeq,hue_color);

}


/**
* @brief Draw a CvBlobSeq on an image.
* @param pImg A pointer on the image to draw.
* @param pBlobList A pointer on the blob list.
* @param hue_color A defined color for all blob if hue_color is between [0,255].
* Otherwise the color is assigned according to the ID of each blob.
* The primary and secondary color are given by:
* 		-  0 <=> RED
* 		-  2 <=> YELLOW
* 		-  4 <=> GREEN
* 		-  6 <=> CYAN
* 		-  8 <=> BLUE
* 		- 10 <=> MAGENTA
* @param is_legend If @true, each blob will have its ID set above the bounding box.
*/
void x7sDrawCvBlobSeq(IplImage *pImg, CvBlobSeq *pBlobList,int hue_color, bool is_legend)
{

	CvFont font;
	CvBlob *pB;
	int blob_huecolor;
	cvInitFont(&font,CV_FONT_HERSHEY_PLAIN, 1,1, 0,1,5);
	CvScalar bgr_color = X7S_BLOBCOLOR(hue_color);
	char str[50];


	for(int i=pBlobList->GetBlobNum(); i>0; i--) {
		pB=pBlobList->GetBlob(i);
		CvPoint TL = cvPoint((int)(pB->x-CV_BLOB_RX(pB)),(int)(pB->y-CV_BLOB_RY(pB)));
		CvPoint BR = cvPoint((int)(pB->x+CV_BLOB_RX(pB)),(int)(pB->y+CV_BLOB_RY(pB)));

		if(!X7S_CHECK_RANGE(hue_color,0,255)) {
			blob_huecolor=pB->ID;
			bgr_color = X7S_BLOBCOLOR(blob_huecolor);
		}

		if(is_legend) {
			snprintf(str,50,"%d",pB->ID);
			cvPutText(pImg, str, cvPoint(TL.x,TL.y-1), &font,bgr_color);
		}
		cvDrawRect(pImg,TL,BR,bgr_color);

	}
}


