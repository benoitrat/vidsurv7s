/**
 *  @file
 *  @brief Contains the class X7sVSAlrDtrGlobal.hpp
 *  @date Jul 13, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SVSALRDTRGLOBAL_HPP_
#define X7SVSALRDTRGLOBAL_HPP_

#include "ix7svidsurv.h"
#include <map>


struct IX7sBlobHistory
{
	CvPoint2D32f last_pos;
	int frm_start;		//!< ID of the first frame that have been proceed.
	int frm_hide;		//!< ID of the frame when the blob was hidden.
};


/**
 *	@brief The Global Alarm Detector for Video Surveillance module.
 *
 *	Count the number of person that enter/exit an image.
 *	It also have an internal memory to keep track of people who disapears in the image because they are not moving
 *	or because they are hidden behind something.
 *
 * Two zones exist in the global alarm detector:
 * 		- 	The External Zone (ExZ) 	: where blobs enter/exit the tracking.
 * 		-	The Internal Zone 	(InZ)		: where blobs show/hide but does not disapear from the scene.
 *
 * The size of the two zones is given by the param : @tt{in_ratio}.
 * 		-	in_ratio=0 (No internal zone)
 * 		- 	in_ratio=1 (No external zone)
 * 		- 	in_ratio=0.7 (default value)
 *
 *
 *
 *	@ingroup x7svidsurv
 */
class X7sVSAlrDtrGlobal: public X7sVSAlrDetector {
public:
	X7sVSAlrDtrGlobal(X7sVSAlrManager *parent);
	virtual ~X7sVSAlrDtrGlobal();
	virtual bool Process(int frame_id, const X7sBlobTrajectory *pTraj);
	virtual bool Process(int frame_id, const X7sVSAlarmEvt& evt);
	void Draw(IplImage *pImg,int hue=-1, int thickness=1);
	void DelTrajectory(int ID);
	void Reload();

protected:
	bool IsInside(const CvPoint2D32f& pt);
	void PrintAlrEvt();

private:
	int m_lastFrame;		//!< ID of the last frame.
	int m_countHide;		//!< Count the number of blob that are hidden

	std::map<int,IX7sBlobHistory> m_mapBlob;

	CvPoint txt_pos;
	CvScalar color, color_bg, color_fill;
	CvScalar in_rect;		//!< The coordinate of the TL and BT corner.
};

#endif /* X7SVSALRDTRGLOBAL_HPP_ */
