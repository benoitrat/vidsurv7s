#include "X7sVSAlrDtrLine.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sVSLog.hpp"
#include <sstream>

using std::stringstream;

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Defines, Enums and Prototypes.
//----------------------------------------------------------------------------------------
#define IX7S_VS_LINE_OFFSET_X 	2
#define IX7S_VS_LINE_OFFSET_Y 	2
#define IX7S_VS_LINE_SAMPLING 	5


struct ix7sVSAlrDtrLineData
{
	ix7sVSAlrDtrHdr hdr;	//!< Commun and general header.
	int32_t 	countIn;	//!< Counter inside
	int32_t 	countOut;	//!< Counter outside.
};


enum {
	IX7S_VS_LINE_HORI=0,	//Horizontal line
			IX7S_VS_LINE_VERT,	//Verical line
			IX7S_VS_LINE_OTHER,	//Other type of line.
};

void ix7sGetLineEquation(float* m, float* b, int* lineType, const CvPoint2D32f* P, const CvPoint2D32f *Q);

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

X7sVSAlrDtrLine::X7sVSAlrDtrLine(X7sVSAlrManager *parent)
:X7sVSAlrDetector(parent,X7S_VS_ALR_TYPE_LINE),
m_lineType(-1), m_slope(-1.f), m_yIntercept(-1.f),
m_P0(cvPoint2D32f(0,0)), m_P1(cvPoint2D32f(0,0)),
m_countType(-1), m_countOut(0), m_countIn(0)
{
	m_params.Insert(X7sParam("p0",X7S_XML_ANYID,"0 0",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("p1",X7S_XML_ANYID,"1 1",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("count_mod",X7S_XML_ANYID,1));
	m_params.Insert(X7sParam("count_type",X7S_XML_ANYID,X7S_VS_ALRLINE_DISP_BOTH));

	m_params["count_type"]->LinkParamValue(&m_countType);

	//Setting the default parameters for drawing
	P = cvPoint(0,0);
	Q = cvPoint(0,0);
	txt_pos = cvPoint(20,20);
	txt_color= cvScalarAll(255);
	txt_bg = cvScalarAll(0);
}

X7sVSAlrDtrLine::~X7sVSAlrDtrLine()
{

}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------


/**
*
* @brief Process a blob trajectory to know if this one produce a line alarm event.
*
* The line alarm event is given by the intersection between the blob trajectory and the line draw.
*
* We use the equation of the line, to calculate this event.
* @code
* l1: y1 = m_slope*x1 + m_yIntercept;
* l2: y2 = m2*x2 + b2;
*
* Where the lines crossing: x1 = x2 and y1 = y2 so
*
* y = m_slope*x + m_yIntercept;
* y = m2*x + b2;
*
* m_slope*x + m_yIntercept = m2*x + b2;
* x*( m_slope - m2 ) = b2 - m_yIntercept;
*
* so
*
* x = ( b2 - m_yIntercept ) / ( m_slope - m2 );
* y = m_slope * x + m_yIntercept;
* @endcode
*
* The event produced can be taken with the function GetAlrEvt().
*
*
* @param frame_id 	the ID of the frame.
* @param pTraj_cst		the trajectory of a blob
* @return @true when an event occurs, @false otherwise.
* @see X7sVSAlrDetector::GetAlrEvt()
*/
bool X7sVSAlrDtrLine::Process(int frame_id,const X7sBlobTrajectory *pTraj_cst)
{
	if(m_params["enable"]->toIntValue()==false) return false;

	//For compatibility remove keyword const
	X7sBlobTrajectory* pTraj=(X7sBlobTrajectory*)pTraj_cst;

	//First reset the alarm event.
	ResetAlrEvt(pTraj->ID);


	if(m_lineType<0)
	{
		double tmp;
		//Obtain the point from the parameters.
		m_params["p0"]->GetRealValue(0,&tmp);
		m_P0.x= (float)tmp;
		m_params["p0"]->GetRealValue(1,&tmp);
		m_P0.y= (float)tmp;
		m_params["p1"]->GetRealValue(0,&tmp);
		m_P1.x= (float)tmp;
		m_params["p1"]->GetRealValue(1,&tmp);
		m_P1.y= (float)tmp;

		ix7sGetLineEquation(&m_slope,&m_yIntercept,&m_lineType,&m_P0,&m_P1);
	}

	if(pTraj==NULL)
	{
		X7S_PRINT_WARN("X7sVSEvtCrossLine::Process()","pTraj is NULL");
		return false;
	}


	const CvBlob *last = pTraj->GetLastBlob();
	CvPoint2D32f blobPos = IX7S_BLOB_TRAJ_POINT2D32F(last);
	if((frame_id-pTraj->GetFirstFrame())%IX7S_VS_LINE_SAMPLING==0) {


		bool is_outside, was_outside, is_between;

		//Compute if the actual blob is outside or inside the correct part of the image
		if(m_lineType==IX7S_VS_LINE_HORI) 		is_outside=(m_slope*blobPos.y > m_yIntercept);
		else if(m_lineType==IX7S_VS_LINE_VERT)	is_outside=(m_slope*blobPos.x > m_yIntercept);
		else									is_outside=((blobPos.x*m_slope+m_yIntercept) > blobPos.y);


		//Check if the the previous side was computed.
		if(m_mapWasOutside.find(pTraj->ID)==m_mapWasOutside.end())
		{
			//If not insert it.
			m_mapWasOutside[pTraj->ID]=is_outside;
		}
		else
		{
			//Otherwise, obtain the previous result.
			was_outside=m_mapWasOutside[pTraj->ID];
			//And keep insert the one computed for next iteration.
			m_mapWasOutside[pTraj->ID]=is_outside;

			if(is_outside!=was_outside)
			{
				//Create the bounding box
				CvPoint2D32f TL, BR;
				TL.x=X7S_MIN(m_P0.x,m_P1.x);
				TL.y=X7S_MIN(m_P0.y,m_P1.y);
				BR.x=X7S_MAX(m_P0.x,m_P1.x);
				BR.y=X7S_MAX(m_P0.y,m_P1.y);

				//Put an offset on the bounding box to make it a little bigger.
				TL.x-= IX7S_VS_LINE_OFFSET_X;
				TL.y-= IX7S_VS_LINE_OFFSET_Y;
				BR.x+= IX7S_VS_LINE_OFFSET_X;
				BR.y+= IX7S_VS_LINE_OFFSET_Y;


				//Obtain the equation of the line trajectory (between last and last-5).
				float slope2,yIntercept2;
				int lineType2;
				const CvBlob *pBlobPrev = pTraj->GetBlob(-(1+IX7S_VS_LINE_SAMPLING));
				if(pBlobPrev==NULL) return false;
				CvPoint2D32f blobPosPrev = IX7S_BLOB_TRAJ_POINT2D32F(pBlobPrev);
				ix7sGetLineEquation(&slope2,&yIntercept2,&lineType2,&blobPos,&blobPosPrev);


				//And find the intersection between the two lines.
				CvPoint2D32f ptCross=blobPos;
				if(m_lineType==IX7S_VS_LINE_HORI) //If line draw is horizontal (y=a*1 + 0)
				{
					ptCross.y=m_P0.y;
					ptCross.x=(ptCross.y-yIntercept2)/slope2;
				}
				else
				{
					if(lineType2==IX7S_VS_LINE_HORI)
					{
						ptCross.y=blobPos.y;
						ptCross.x=(ptCross.y-m_yIntercept)/m_slope;
					}
					else
					{
						if(lineType2==IX7S_VS_LINE_VERT) ptCross.x=blobPos.x;
						else ptCross.x = ( yIntercept2 - m_yIntercept ) / ( m_slope - slope2 );
						ptCross.y = m_slope * ptCross.x + m_yIntercept;
					}
				}


				//Obtain the line equation of blob trajectory
				bool is_between_x=(TL.x <= ptCross.x && ptCross.x <= BR.x);
				bool is_between_y=(TL.y <= ptCross.y && ptCross.y <= BR.y);

				if(m_lineType==IX7S_VS_LINE_HORI) 		is_between=is_between_x;
				else if(m_lineType==IX7S_VS_LINE_VERT) 	is_between=is_between_y;
				else is_between=(is_between_x && is_between_y);

				if(is_between) {
					if(is_outside) {
						X7S_PRINT_INFO("X7sVSAlrLine::Process","Crossing line: (In->Out) for blob %d",pTraj->ID);
						alrEvt.val=X7S_VS_ALR_EVT_OUT;
						m_countOut++;
					}
					else {
						X7S_PRINT_INFO("X7sVSAlrLine::Process","Crossing line: (Out->In) for blob %d",pTraj->ID);
						alrEvt.val=X7S_VS_ALR_EVT_IN;
						m_countIn++;
					}
					int mod=m_params["count_mod"]->toIntValue();

					switch(m_countType) {
					case X7S_VS_ALRLINE_DISP_IN:
						if(m_countIn%mod==0) alrEvt.warn=true;
						break;
					case X7S_VS_ALRLINE_DISP_OUT:
						if(m_countOut%mod==0) alrEvt.warn=true;
						break;
					case X7S_VS_ALRLINE_DISP_TOTAL:
						if((m_countOut+m_countIn)%mod==0) alrEvt.warn=true;
						break;
					case X7S_VS_ALRLINE_DISP_BOTH:
						if((m_countOut%mod==0)||(m_countIn%mod==0)) alrEvt.warn=true;
						break;
					}
				}
			}
		}
	}
	return (alrEvt.val!=X7S_VS_ALR_EVT_NONE);
}


/**
* @brief Process the alarm detector using a X7sVSAlarmEvt
*
* This function correctly update the internal value. Then one can use the draw function
* to print the result of the alarm in an image, or obtain the warning text.
*
* @param frame_id unused value for this function
* @param evt a X7sVSAlarmEvt.
* @return @true if the event correspond to this alarm, and if an event occurs.
*/
bool X7sVSAlrDtrLine::Process(int /*frame_id*/, const X7sVSAlarmEvt& evt)
{
	//Check if it belong to
	if(evt.alarm_ID==this->GetID())
	{
		//Use default copy constructor
		alrEvt=evt;

		//Check if it is an event.
		switch(alrEvt.val)
		{
		case X7S_VS_ALR_EVT_IN:
			m_countIn++;
			return true;
		case X7S_VS_ALR_EVT_OUT:
			m_countOut++;
			return true;
		case X7S_VS_ALR_EVT_NONE:
			X7S_PRINT_DEBUG("X7sVSAlrDtrLine::Process()","AlrEvt.val should not be X7S_VS_ALR_EVT_NONE");
			return false;
		default:
			X7S_PRINT_WARN("X7sVSAlrDtrLine::Process()","Unkown alrEvt.val=%d",alrEvt.val);
			return false;
		}
	}
	return false;
}



/**
* @brief Draw the line and counter for the Line Alarm Detector.
* @param pImg Image on which we want to draw
* @param hue desired color (if it's negative we use the color of the detector)
* @param thickness Size of the pencil use to draw the line, points, circles...
*/
void X7sVSAlrDtrLine::Draw(IplImage *pImg,int hue, int thickness)
{
	CvFont font;
	CvScalar color = cvScalarAll(255);
	if(hue<0) color=this->txt_color;
	else 	color = x7sCvtHue2BGRScalar(hue,360,360);

	cvLine(pImg,P,Q,color,thickness);

	if(m_countType==X7S_VS_ALRLINE_DISP_NONE) return;

	int baseline;
	CvSize font_ROI;
	stringstream sstr;
	bool is_notempty=false;
	cvInitFont(&font,CV_FONT_HERSHEY_PLAIN, 0.8,0.8, 0,1,5);

	//First find the size of the square.
	if(m_countType==X7S_VS_ALRLINE_DISP_IN || m_countType>=X7S_VS_ALRLINE_DISP_BOTH)
	{
		sstr << "In: " << m_countIn;
		is_notempty=true;
	}

	if(m_countType==X7S_VS_ALRLINE_DISP_OUT || m_countType>=X7S_VS_ALRLINE_DISP_BOTH)
	{
		if(is_notempty) sstr << " / ";
		sstr << "Out: " << m_countOut;
		is_notempty=true;
	}

	if(m_countType==X7S_VS_ALRLINE_DISP_TOTAL || m_countType==X7S_VS_ALRLINE_DISP_ALL)
	{
		if(is_notempty) sstr << " / ";
		sstr << "Total: " << m_countIn+m_countOut;

	}

	cvGetTextSize(sstr.str().c_str(), &font, &font_ROI,&baseline);
	cvSetImageROI(pImg,cvRect(txt_pos.x-1,txt_pos.y-font_ROI.height-2,font_ROI.width+2,font_ROI.height+4));
	cvSet(pImg,txt_bg);
	cvResetImageROI(pImg);
	cvPutText(pImg,sstr.str().c_str(),txt_pos,&font,txt_color);
}


/**
* @brief Delete the id in the map that keep if the last position
* in the trajectory was right/left of line vector.
* @see X7sVSAlrDetector::DelTrajectory
*/
void X7sVSAlrDtrLine::DelTrajectory(int ID)
{
	map<int,bool>::iterator it=m_mapWasOutside.find(ID);
	if(it!=m_mapWasOutside.end())
	{
		m_mapWasOutside.erase(it);
		X7S_PRINT_DEBUG("X7sVSAlrDtrLine::DelTrajectory()", "alrDtrID=%d, blobID=%d",this->GetID(),ID);
	}
}

/**
* @brief Obtain the counter and the internal list of this Line detector.
* @return An array of byte with the proper memory.
*/
const X7sByteArray* X7sVSAlrDtrLine::GetData()
{

	int expected_length;			//Expected length
	uint8_t *p, *end;				//Pointer to write on m_pData memory.
	ix7sVSAlrDtrLineData lineData; //Structure for saving the data.

	//Setup the common data.
	lineData.hdr.version=IX7S_VS_ALR_DTR_VERSION;
	lineData.hdr.id=this->GetID();
	lineData.hdr.type=this->GetType();

	//Setup the specific data.
	lineData.countIn=m_countIn;
	lineData.countOut=m_countOut;

	//Compute the expected length
	expected_length=sizeof(lineData);

	//We create a buffer with the correct size
	if(m_pData && m_pData->capacity<expected_length) x7sReleaseByteArray(&m_pData);
	if(m_pData==NULL) m_pData = x7sCreateByteArray((int)(expected_length*1.5));

	//Setup the internal pointer
	p=m_pData->data;
	end=m_pData->data+m_pData->capacity;

	//Writing the number of blobs in this list,
	p=x7sMemWrite(p,end,&lineData,sizeof(lineData));

	//And check the size of the data written
	m_pData->length=(p-m_pData->data);
	if(m_pData->length != expected_length)
	{
		X7S_PRINT_WARN("X7sVSAlrDtrLine::GetData()",
				"Expected length (%d) different from written length (%d)",
				expected_length,m_pData->length);
	}
	return m_pData;
}

/**
* @brief
* @param pData
*/
bool X7sVSAlrDtrLine::SetData(const X7sByteArray *pData)
{
	uint8_t *p, *end;				//Pointer to write on m_pData memory.
	ix7sVSAlrDtrLineData lineData; //Structure for saving the data.
	X7S_FUNCNAME("x7sVSAlrDtrLine::SetData()");

	X7S_CHECK_WARN(funcName,pData,false,"pData is NULL");
	//Set the pointers
	p=pData->data;
	end=pData->data+pData->capacity;

	//Read The
	p=x7sMemRead(p,end,&lineData,sizeof(lineData));


	//Check the common data.
	int16_t ID= this->GetID();
	int16_t type= this->GetType();
	if(lineData.hdr.id== ID && lineData.hdr.version==IX7S_VS_ALR_DTR_VERSION && lineData.hdr.type==type)
	{

		//Setup the specific data.
		m_countIn	=	lineData.countIn;
		m_countOut	=	lineData.countOut;

		return true;

	}
	else X7S_PRINT_WARN(funcName,"pData has not a correct header (ID=%d, version=%d, or type=%d)",lineData.hdr.id,lineData.hdr.version,lineData.hdr.type);

	return false;

}

/**
 * @brief Reload the parameters to draw correctly the line, color
 */
void X7sVSAlrDtrLine::Reload()
{
	//Obtain the point from the parameters.
	X7sXmlParamGetVector(&P,m_params["p0"]);
	X7sXmlParamGetVector(&Q,m_params["p1"]);

	//Obtain the parameters for drawing
	X7sXmlParamGetVector(&txt_pos,m_params["txt_pos"]);
	X7sXmlParamGetVector(&txt_color,m_params["color"]);
	X7sXmlParamGetVector(&txt_bg,m_params["color_bg"]);
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

/**
* @brief Set the value for the pointer m.b.lineType given two points.
*
* @param m A pointer on the slope value
* @param b A pointer on the y-intercept
* @param lineType	A pointer on the type of line.
* @param P	The input start point.
* @param Q	The input end point
*/
void ix7sGetLineEquation(float* m, float* b, int* lineType, const CvPoint2D32f* P, const CvPoint2D32f *Q)
{
	//Check the type of line.
	if ((Q->x - P->x) == 0)	//Vertical line. (x=m_yIntercept)
	{
		*lineType=IX7S_VS_LINE_VERT;
		*m=(Q->y - P->y)/fabs((Q->y - P->y));	//Find the direction of the line.
		*b=Q->x;

	} else if((Q->y - P->y) == 0)	//Horizontal line (y=m_yIntercept)
	{
		*lineType=IX7S_VS_LINE_HORI;
		*m=(Q->x - P->x)/fabs((Q->x - P->x));	//Find the direction of the line.
		*b= Q->y;

	} else	//Normal line (compute the equation)
	{
		*lineType=IX7S_VS_LINE_OTHER;
		*m = (Q->y - P->y )/(Q->x - P->x);  //Compute slope (with the direction)
		*b = P->y - (*m) * P->x; 			//Compute y-intercept.
	}
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Interfaces
//----------------------------------------------------------------------------------------

/**
* @brief Construct an Alarm with a line.
*
* An alarm occurs when someone cross the line (the direction of crossing is given by the slope vector (P0->P1).
* @param parent The parent alarm manager.
* @param x0 X coordinate of the first point (P0)
* @param y0 Y coordinate of the first point (P0)
* @param x1 X coordinate of the second point (P1)
* @param y1 Y coordinate of the second point (P1)
* @param name The name of the Alarm.
* @return Return a generic alarm object.
*/
X7sVSAlrDetector *x7sCreateVSAlarmLine(X7sVSAlrManager *parent, int x0, int y0, int x1, int y1, const char *name)
{
	//Create the object
	X7sVSAlrDtrLine *alrLine = new X7sVSAlrDtrLine(parent);

	//Setup the principal parameters
	stringstream sstr0, sstr1;

	sstr0 << x0 << " " << y0;
	alrLine->SetParam("p0",sstr0.str().c_str());

	sstr1 << x1 << " " << y1;
	alrLine->SetParam("p1",sstr1.str().c_str());

	if(name) alrLine->SetParam("name",name);
	alrLine->Reload();

	return (X7sVSAlrDetector*)alrLine;
}

