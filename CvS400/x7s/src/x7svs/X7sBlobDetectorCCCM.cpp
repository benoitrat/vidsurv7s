#include "X7sBlobDetector.hpp"
//========================================================================
//-- Internal include to fasten compilation.
#include <highgui.h>
#include "X7sVSLog.hpp"


//========================================================================
//-- Constructor, Destructor and Init.

/**
* @brief Default constructor.
*
* Set the number of layer in the history to be IX7S_BDRL_NUM_FRAME.
*/
X7sBlobDetectorCCCM::X7sBlobDetectorCCCM() {

	m_sizeBlobList = IX7S_BDRL_NUM_FRAME;
	m_pBlobLists = new CvBlobSeq*[m_sizeBlobList];
	for(int i=0;i< m_sizeBlobList; i++) m_pBlobLists[i]=NULL;
	m_pActualBlobList = m_pBlobLists[m_sizeBlobList-1];

	m_param_roi_scale = 1.5F;
	AddParam((x7scharA*)"ROIScale",&m_param_roi_scale);
	CommentParam((x7scharA*)"ROIScale", (x7scharB*)"Determines the size of search window around a blob");

	SetModuleName((x7scharA*)"BD_CCCM");

}

/**
* @brief Empty destructor.
*/
X7sBlobDetectorCCCM::~X7sBlobDetectorCCCM() {

}


//========================================================================
//-- Public Methods.



/**
* @brief Detect the new blob
* @param pImg The unused image.
* @param pFGMask The foreground mask (Binary or with the ID in grey)
* @param pNewBlobList A pointer on the list where new blob will be add.
* @param pOldBlobList A pointer on the actual (old) blob list where the position of the blob.
* @note This blob detector keeps an history for the last IX7S_BDRL_NUM_FRAME
*
*	The Blob Detector works like cvCreateBlobDetectorCC:
*		- If full, shift the queue history of CvBlobSeq, and create a new one (like <<).
*		- Find CC and add new blobs to blob list
*			-# Fill a new CvBlobSeq with the pFGMask (Draw in RED).
*			-# Delete Small Blob.
*			-# Delete intersected blob (using old List).
*			-# Bubble Sort Blob by their Area (using width and height).
*			-# Finally, copy only the first IX7S_BDRL_NUMMAX_NEWBLOB to the queue
*			history of CvBlobSeq (using the last position in the history).
*		- Analyze blob history to find best blob trajectory:
*			-# Then Add blob by blob, end delete its trajectory from history.
*/
int X7sBlobDetectorCCCM::DetectNewBlob(IplImage* /*pImg*/, IplImage* pFGMask, CvBlobSeq* pNewBlobList, CvBlobSeq* pOldBlobList)
{
	CvSize S = cvGetSize(pFGMask);	//Size of the FG image.
	IplImage* pTmp = NULL;
	bool tmpDel=true;

	if(m_isDrawDebug)
	{
		cvNamedWindow(GetModuleName());
		pTmp = cvCreateImage(cvGetSize(pFGMask),IPL_DEPTH_8U,3);	//3 Channels to draw the image
		cvCvtColor(pFGMask,pTmp,CV_GRAY2BGR);
		cvThreshold(pTmp,pTmp,0,255,CV_THRESH_BINARY);
	}


	//Shift Blob List
	this->ShiftQueueHistoryBlobList();

	//Find the CC and add them to the temporary blob list.
	CvBlobSeq       Blobs;
	this->GetNewBlobList(pFGMask,&Blobs);

	//Blob draw in red are the blob detected directly from foreground.
	if(m_isDrawDebug) x7sDrawCvBlobSeq(pTmp,&Blobs,IX7S_RED);

	//Filter small and intersected (WARNING: As we Delete() element, the loop should start by the end)
	for(int i=Blobs.GetBlobNum()-1; i>=0; --i)
	{
		CvBlob* pB = Blobs.GetBlob(i);


		X7S_PRINT_DUMP("X7sBlobDetectorCCCM::DetectNewBlob()",
				"Filter i=%d, ID=%d",i,pB->ID);

		//Filter small one.
		if(m_minRatio > 0.f && IsBlobTooSmall(pB,S,m_minRatio))
		{
			X7S_PRINT_DUMP("X7sBlobDetectorCCCM::DetectNewBlob()",
					"Blob is too small for blob->ID=%d\n",pB->ID);
			Blobs.DelBlob(i);
			continue;	//Go to the next blob.
		}

		//Filter intersect one.
		if(IsBlobIntersect(pB,pOldBlobList)) {
			Blobs.DelBlob(i);
		}
	}

	//Filter the N first blob (by size area)
	SetNBestRecentBlob(&Blobs,IX7S_BDRL_NUMMAX_NEWBLOB);

	//Blob in magenta the blob that have been filtered.
	if(m_isDrawDebug) x7sDrawCvBlobSeq(pTmp,m_pBlobLists[m_sizeBlobList-1],IX7S_MAGENTA);


	//=================================================
	FindBestTraj(pFGMask, pNewBlobList,pOldBlobList);

	if(m_isDrawDebug)
	{
		x7sDrawCvBlobSeq(pTmp,pOldBlobList,IX7S_YELLOW,false);	//Yellow are blob tracked
		x7sDrawCvBlobSeq(pTmp,pNewBlobList,IX7S_GREEN);		//Green are blob set as new.
		cvShowImage(GetModuleName(),pTmp);
		cvWaitKey(0);
		if(tmpDel && pTmp) cvReleaseImage(&pTmp);
	}

	return (pNewBlobList->GetBlobNum()>0);
}




/**
* @brief Compute the best trajectory using the OpenCV way and add it to the pNewBlobList.
*
* Analyze blob history to find best blob trajectory:
*
* 	- Check, If the queue history has at least one blob in each time layer.
* 	- Then for each tupple (t=time,i=id_blob), obtain its trajectory:
*       //TOCHECK: If it is necessary!
* 		- First, check if the more recent blob t=t_last is intersected with the old list
* 		- Then, check if distance to image boarder of the latest blob!
* 		- Finally, check uniform motion.
*
* @note The pBLIndex is like a i, j loop.
*/
void X7sBlobDetectorCCCM::FindBestTraj(IplImage *pFG, CvBlobSeq* pNewBlobList, CvBlobSeq* pOldBlobList)
{

	bool result=false;
	CvSize S = cvGetSize(pFG);

	IplImage* pTmp2 = NULL;
	pTmp2 = cvCreateImage(cvGetSize(pFG),IPL_DEPTH_8U,3);	//3 Channels to draw the image
	bool tmp2Del=true;
	cvCvtColor(pFG,pTmp2,CV_GRAY2BGR);
	cvThreshold(pTmp2,pTmp2,0,255,CV_THRESH_BINARY);
	//cvNamedWindow("Blob Best");


	int     Count = 0;
	int*    pBLIndex = new int[m_sizeBlobList];
	int*	pBL_BEST = new int[m_sizeBlobList];
	int     i;
	int     finish = 0;
	double  BestError = -1;
	int     Good = 1;


	//Check if queue history (@ref m_pBlobLists) as at
	//least one blob in each frame of the history.
	for(i=0; Good && (i<m_sizeBlobList); ++i)
		if(m_pBlobLists[i] == NULL || m_pBlobLists[i]->GetBlobNum() == 0)
			return;



	//----------------------------------------

	//Loop over each blob in the last layer of the history
	for(i=0; i<m_pActualBlobList->GetBlobNum(); ++i)
	{
		//Set the actual blob
		CvBlob* pBActu = m_pActualBlobList->GetBlob(i);
		pBLIndex[m_sizeBlobList]=i;	//The pBLIndex is in the following order (oldest -> most recent)
		pBL_BEST[m_sizeBlobList]=i;

		//Then for each layer in the history reset the index.
		for(int t=m_sizeBlobList-2;t>=0;t--)
		{
			pBLIndex[t]=0;
			pBL_BEST[t]=0;
		}

		//To Remove
		pBActu->ID=1;


		//Go back in the history.
		for(int t=m_sizeBlobList-2;t>=0;t--)
		{




		}

	}

	//If queue history is complete or good (for this frame)
	if(Good) {
		do { /* For each configuration history (loop on pBLIndex[i]) */
			CvBlob** pBL = new CvBlob*[m_sizeBlobList];	//Temporary blob list
			int     Good = 1;
			double  Error = 0;

			//Take the most recent blob list in the queue history, and find the one corresponding to our configuration using pBLIndex
			CvBlob* pBNew = m_pBlobLists[m_sizeBlobList-1]->GetBlob(pBLIndex[m_sizeBlobList-1]);

			//Copy pointer of the queue history for this blob.
			for(i=0; i<m_sizeBlobList; ++i)
				pBL[i] = m_pBlobLists[i]->GetBlob(pBLIndex[i]);


			// Check intersection between more recent and old other blobs.
			if(Good && pOldBlobList)
			{
				for(int k=pOldBlobList->GetBlobNum(); k>0; --k)
				{
					//Check intersection.
					CvBlob* pBOld = pOldBlobList->GetBlob(k-1);
					if(IX7S_BLOB_INTERSECT(pBOld,pBNew))
					{
						Good = 0;
						X7S_PRINT_DUMP("X7sBlobDetectorCCCM::FindBestTraj()",
								"Intersection found (Not Good)");
					}
				}
			}

			//If no intersection

			// Check distance to image border:
			if(Good)
			{   /* Check distance to image border: */
				CvBlob*  pB = pBNew;
				float    dx = MIN(pB->x,S.width-pB->x)/CV_BLOB_RX(pB);
				float    dy = MIN(pB->y,S.height-pB->y)/CV_BLOB_RY(pB);

				if(dx < 1.1 || dy < 1.1) {
					//X7S_PRINT_DUMP("X7sBlobDetectorCCCM::FindBestTraj()",
					//"Near Image Boarder (Not Good)");
					Good = 0;
				}
			}   /* Check distance to image border. */


			/* Check uniform motion: */
			if(Good)
			{
				int     N = m_sizeBlobList;
				float   sum[2] = {0,0};
				float   jsum[2] = {0,0};
				float   a[2],b[2]; /* estimated parameters of moving x(t) = a*t+b*/

				int j;
				for(j=0; j<N; ++j)
				{
					float   x = pBL[j]->x;
					float   y = pBL[j]->y;
					sum[0] += x;
					jsum[0] += j*x;
					sum[1] += y;
					jsum[1] += j*y;
				}

				a[0] = 6*((1-N)*sum[0]+2*jsum[0])/(N*(N*N-1));
				b[0] = -2*((1-2*N)*sum[0]+3*jsum[0])/(N*(N+1));
				a[1] = 6*((1-N)*sum[1]+2*jsum[1])/(N*(N*N-1));
				b[1] = -2*((1-2*N)*sum[1]+3*jsum[1])/(N*(N+1));

				for(j=0; j<N; ++j)
				{
					Error +=
							pow(a[0]*j+b[0]-pBL[j]->x,2)+
							pow(a[1]*j+b[1]-pBL[j]->y,2);
				}

				Error = sqrt(Error/N);

				if( Error > S.width*0.01 ||
						fabs(a[0])>S.width*0.1 ||
						fabs(a[1])>S.height*0.1)
				{
					Good = 0;
					//X7S_PRINT_DUMP("X7sBlobDetectorCCCM::FindBestTraj()",
					//"Motion is not Uniform");
				}

			}   /* Check configuration. */


			/* New best trajectory: */
			if(Good && (BestError == -1 || BestError > Error))
			{
				for(i=0; i<m_sizeBlobList; ++i)
				{
					pBL_BEST[i] = pBLIndex[i];
				}
				BestError = Error;

				CvBlobSeq blobSeq;
				for(int n=0;n<m_sizeBlobList;n++)
					blobSeq.AddBlob(m_pBlobLists[n]->GetBlob(pBL_BEST[n]));
				x7sDrawCvBlobSeq(pTmp2,&blobSeq,IX7S_GREEN);
				cvShowImage("Blob Best",pTmp2);
				printf("Best Motion: ");
				//cvWaitKey(0);

			}   /* New best trajectory. */


			//Plot the configuration
			printf("count=%d >> ",Count);
			for(int n=0; n<m_sizeBlobList; ++n) {
				printf("%d ", pBLIndex[n]);
			}
			printf("\n");


			CvBlobSeq blobSeq;
			for(int n=0;n<m_sizeBlobList;n++)
				blobSeq.AddBlob(m_pBlobLists[n]->GetBlob(pBLIndex[n]));
			x7sDrawCvBlobSeq(pTmp2,&blobSeq,IX7S_CYAN);


			cvShowImage("Blob Best",pTmp2);
			cvWaitKey(5);
			cvCvtColor(pFG,pTmp2,CV_GRAY2BGR);
			cvThreshold(pTmp2,pTmp2,0,255,CV_THRESH_BINARY);


			//Go to the next configuration:
			Count++;
			for(i=0; i<m_sizeBlobList; ++i)
			{
				pBLIndex[i]++;
				if(pBLIndex[i] != m_pBlobLists[i]->GetBlobNum()) break;
				pBLIndex[i]=0;
			}   /* Next time shift. */

			if(i==m_sizeBlobList) finish=1;

		} while(!finish);	/* Check next time configuration of connected components. */
	}


	//----------------------------------------

	{/**/
		printf("BlobDetector configurations = %d [",Count);
		int i;
		for(i=0; i<m_sizeBlobList; ++i)
		{
			printf("%d,",m_pBlobLists[i]?m_pBlobLists[i]->GetBlobNum():0);
		}
		printf("]\n");

	}

	CvBlobSeq blobSeq;
	for(int i=0;i<m_sizeBlobList;i++)
		blobSeq.AddBlob(m_pBlobLists[m_sizeBlobList-1]->GetBlob(pBL_BEST[i]));
	x7sDrawCvBlobSeq(pTmp2,&blobSeq,IX7S_MAGENTA);

	if(BestError != -1)
	{   /* Write new blob to output and delete from blob list: */
		printf("Add only one blob=%d\n",pBL_BEST[m_sizeBlobList-1]);
		CvBlob* pNewBlob = m_pBlobLists[m_sizeBlobList-1]->GetBlob(pBL_BEST[m_sizeBlobList-1]);
		pNewBlobList->AddBlob(pNewBlob);

		printf("Delete blob from history: ");
		for(i=0; i<m_sizeBlobList; ++i)
		{   /* Remove blob from each list: */
			m_pBlobLists[i]->DelBlob(pBL_BEST[i]);

			//Delete other blob from history
			printf("%d ",pBL_BEST[i]);

		}   /* Remove blob from each list. */
		printf("\n");

		result = true;

	}   /* Write new blob to output and delete from blob list. */

	if(tmp2Del && pTmp2) cvReleaseImage(&pTmp2);


}


/**
* @brief Create a blob detector with connected component retrieved and cluster merging..
*/
CvBlobDetector* x7sCreateBlobDetectorCCCM()
{
	return new X7sBlobDetectorCCCM();
}


//==============================================================================================

///////////////////////////				X7SBlobDetectorLRLE 		///////////////////////////








/**
* @brief Obtain a new blob list using a buffer with data in Line-Run-Length
* Encoding format.
* @param pFGMask A special image buffer where we have :
* 		- IplImage::nChannels=0.
* 		- IplImage::imageSize=Size of the buffer (!=width*height*nChannels).
* 		- IplImage::width and IplImage::height size of the resulting image.
* @param pBlobList An empty blob list.
*
*/
void X7sBlobDetectorLRLE::GetNewBlobList(IplImage *pFGMask,CvBlobSeq *pBlobList)
{
	X7S_PRINT_INFO("X7sBlobDetectorCCCM::GetNewBlobList()",
			"Image nChannels is %d",pFGMask->nChannels);
}






/**
* @brief Create a blob detector using Line-Run-Length encoding
* data as input for DetectNewBlob() function.
* The IplImage given is therefore in a special format.
* @see X7sBlobDetectorLRLE::GetNewBlobList().
*/
CvBlobDetector* x7sCreateBlobDetectorLRLE()
{
	return new X7sBlobDetectorLRLE();
}

