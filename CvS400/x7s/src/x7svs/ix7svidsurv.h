/**
 *  @file
 *  @brief Internal include files for X7S VidSurv Library
 *  @date Jul 8, 2009
 *  @author neub
 */

#ifndef _IX7SVIDSURV_H_
#define _IX7SVIDSURV_H_

#include "x7svidsurv.h"

#include <map>

#define IX7S_VS_TAGNAME_FGD 	"FGDetector"
#define IX7S_VS_TAGNAME_BD 	"BlobDetector"
#define IX7S_VS_TAGNAME_BT 	"BlobTracker"
#define IX7S_VS_TAGNAME_BTPP "BlobTrackPostProc"
#define IX7S_VS_TAGNAME_BTA 	"BlobTrackAnalysis"
#define IX7S_VS_TAGNAME_BTG 	"BlobTrackGen"

#define IX7S_RED		0
#define IX7S_YELLOW		1
#define IX7S_GREEN		2
#define IX7S_CYAN		3
#define IX7S_BLUE		4
#define IX7S_MAGENTA	5
#define X7S_BLOBCOLOR(hue_ID) \
		x7sCvtHue2BGRScalar(hue_ID,12,6)


#define X7S_RELEASE_PTR(ptr) \
	if(ptr) { ptr->Release(); ptr=NULL; }


#define IX7S_VS_DATAHDR_SIZE 11


#define IX7S_CHECK_ARG_NULLPTR(funcName,ptr,ret) \
		if(ptr==NULL) \
	{ \
		 X7S_LOG(funcName,X7S_LOGLEVEL_LOW,"%s is NULL",#ptr); \
		 return ret; \
	}


//!< Return the Euclidean distance from the centers.
#define IX7S_BLOB_CDISTANCE(pBlob1,pBlob2) \
	(sqrt(pow((float)pBlob1->x-(float)pBlob2->x,2.f)+pow((float)pBlob1->y-(float)pBlob2->y,2.f)))

#define IX7S_BLOB_TLCORNER(pBlob) \
	( cvPoint2D32f((pBlob->x-CV_BLOB_RX(pBlob)),(pBlob->y-CV_BLOB_RY(pBlob))) )

#define IX7S_BLOB_BRCORNER(pBlob) \
	( cvPoint2D32f((pBlob->x+CV_BLOB_RX(pBlob)),(pBlob->y+CV_BLOB_RY(pBlob))) )



//! Return the area of a blob
#define IX7S_BLOB_AREA(pBlob) \
	(CV_BLOB_WX(pBlob)*CV_BLOB_WY(pBlob))

//!< Return true if BLOB1 is on the left Blob2
#define IX7S_BLOB1_LEFT_BLOB2(pBlob1, pBlob2) \
	((pBlob1->x+CV_BLOB_RX(pBlob1)) <= (pBlob2->x + CV_BLOB_RX(pBlob2)))

//!< Return true if BLOB1 is on the right of Blob2
#define IX7S_BLOB1_RIGHT_BLOB2(pBlob1, pBlob2) \
	((pBlob1->x-CV_BLOB_RX(pBlob1)) >= (pBlob2->x - CV_BLOB_RX(pBlob2))

//!< Return true if Blob1 is above of Blob2
#define IX7S_BLOB1_ABOVE_BLOB2(pBlob1, pBlob2) \
	((pBlob1->y+CV_BLOB_RY(pBlob1)) <= (pBlob2->x + CV_BLOB_RY(pBlob2)))

//!< Return true if Blob1 is under Blob2
#define IX7S_BLOB1_UNDER_BLOB2(pBlob1, pBlob2) \
	((pBlob1->x-CV_BLOB_RY(pBlob1)) <= (pBlob2->x - CV_BLOB_RY(pBlob2)))

//!< Return true if Blob1 is equal or bigger than Blob2
#define IX7S_BLOB1_BIGGER_BLOB2(pBlob1,pBlob2) \
	(IX7S_BLOB_AREA(pBlob1) >= IX7S_BLOB_AREA(pBlob2))

/**
 * @brief Return true if the 2 blobs intersect
 * The intersection occurs when the absolute distance
 * between the two center is less than the sum of the radius
 * of the two blobs (CV_BLOB_RX is the radius of X).
 */
#define IX7S_BLOB_INTERSECT(pBlob1,pBlob2) \
	( \
			(fabs(pBlob1->x-pBlob2->x) < (CV_BLOB_RX(pBlob1)+CV_BLOB_RX(pBlob2))) && \
			(fabs(pBlob1->y-pBlob2->y) < (CV_BLOB_RY(pBlob1)+CV_BLOB_RY(pBlob2))) \
	)


//----------- alarm detector

#define X7S_VS_TAGNAME_ALRMAN 	"alarms"		//!< Name of the XML node that correspond to the X7SVSAlrManager
#define X7S_VS_TAGNAME_ALRDTR 		"alarm"		//!< Name of the XML node that correspond to the X7SVSAlrDetector

/**
 * @brief Reset all the value of a structure to zeros.
 */
#define IX7S_VS_SETZEROS(struct0) \
	memset(&struct0,0,sizeof(struct0))

//Internal structure of alarm detector.
struct ix7sVSAlrDtrHdr
{
	uint32_t 	id:16;
	uint32_t 	version:8;
	uint32_t 	type:8;
};

#define IX7S_VS_ALR_DTR_VERSION 0x1

#define IX7S_BLOB_TRAJ_POINT(pBlob) \
	( cvPoint( (int)(pBlob->x) ,(int)(pBlob->y+pBlob->h/3.f) ) )

#define IX7S_BLOB_TRAJ_POINT2D32F(pBlob) \
	( cvPoint2D32f( (double)(pBlob->x) ,(double)(pBlob->y+pBlob->h/3.f) ) )


CvBlobTracker*  cvCreateBlobTrackerKalman();


struct X7sBlob: public CvBlob {
	float vx;
	float vy;
	float ratio;
	float averArea;
	float perim;
	int first;
	int last;
};


inline CvBlob cvBlob(int ID, int min_x, int max_x, int min_y, int max_y)
{
	float x = ((float)(min_x+max_x))/2.f;
	float y = ((float)(min_y+max_y))/2.f;
	float w = ((float)(max_x-min_x));
	float h = ((float)(max_y-min_y));

    CvBlob B = {x,y,w,h,ID};
    return B;
}


#endif /* _X7SVIDSURV_H_ */
