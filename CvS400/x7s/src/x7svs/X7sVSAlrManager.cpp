#include "ix7svidsurv.h"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sVSLog.hpp"
#include <x7snet.h>

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

/**
* @brief Create an alarm manager with an XML node.
* @param parent The Parent X7sXMLNode if we want to automatically load the alarm manager.
* @param cnode	The node with the name @ref X7S_VS_TAGNAME_ALRMAN
*/
X7sVSAlrManager::X7sVSAlrManager(X7sXmlObject *parent,const TiXmlNode *cnode)
:X7sXmlObject(parent,X7S_VS_TAGNAME_ALRMAN,true), m_pData(NULL)
{
	Init();
	if(cnode) this->ReadFromXML(cnode);
}

/**
* @brief Create an alarm managers with the definition of the alarm detector
* @param parent The Parent X7sXMLNode if we want to automatically load the alarm manager.
* @param alrDef
* @return
*/
X7sVSAlrManager::X7sVSAlrManager(X7sXmlObject *parent,X7sByteArray* alrDef)
:X7sXmlObject(parent,X7S_VS_TAGNAME_ALRMAN,true), m_pData(NULL)
{
	Init();

	/* Read the definition of alarm detector */

	if(alrDef && alrDef->length > 0)
	{
		FILE *tmp = x7sMakeTmp(); //Create a temporary file
		if(tmp==NULL) X7S_PRINT_ERROR("X7sVSAlrManager::X7sVSAlrManager()","Could not create a temporary file");

		fwrite(alrDef->data,alrDef->length,1,tmp); //and write the binary data.

		TiXmlDocument doc;
		if(doc.LoadFile(tmp))
		{
			this->ReadFromXML(doc.RootElement());
		}

		x7sCloseTmp(&tmp);
	}
	else
	{
		X7S_PRINT_WARN("X7sVSAlrManager::X7sVSAlrManager()","Bad alarm definition");
	}
}


/**
* @brief Init the alarm manager.
*/
void X7sVSAlrManager::Init()
{
	m_lastID=-1;	//Because the next one should be zeros.
	m_frameID=-1;
	m_pData = x7sCreateByteArray(X7S_VS_ALRMAN_DATA_SIZE);
	m_pDefData=NULL;
	m_draw=true;
	for(int i=0;i<X7S_VS_ALR_TYPE_END;i++)
	{
		numTypeAlarm[i]=0;
	}

	X7sParam *prm;
	prm = m_params.Insert(X7sParam("draw",X7S_XML_ANYID,m_draw,X7STYPETAG_BOOL));
	prm->setComment("Draw all the counters and the alarms");
	prm->LinkParamValue(&m_draw);

}

/**
* @brief Default destructor
*/
X7sVSAlrManager::~X7sVSAlrManager()
{
	x7sReleaseByteArray(&(m_pData));
	x7sReleaseByteArray(&(m_pDefData));
	this->DeleteAll();
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

/**
* @brief Draw all the alarm on the image
* @param pImg An Image in RGB or RGBA format and IPL_DEPTH_8U
*/
void X7sVSAlrManager::Draw(IplImage *pImg)
{
	if(pImg && m_draw)
	{
		for(size_t i=0;i<m_vecAlarm.size();i++)
		{
			if(m_vecAlarm[i])
			{
				m_vecAlarm[i]->Draw(pImg);
			}
		}
	}
}

/**
* @brief Process all the alarm detector for one trajectory.
*
* This function keep for each new frame the list of all events detected
* (for each blobs and each alarm detectors).
*
* @warning the frame ID give to this function should be one more than the one to create the trajectory,
* otherwise the Alarm Detector can not delete the trajectory from their list.
* @param frame_id	The current frame ID.
* @param pTraj	A pointer on a blob trajectory.
* @return Return @true if at least one alarm detector have produced an event for this blob.
*/
bool X7sVSAlrManager::Process(int frame_id, X7sBlobTrajectory *pTraj)
{
	//If it is a new frame, clear the previous alarm events
	if(frame_id!=m_frameID) m_vecAlrEvt.clear();
	m_frameID=frame_id;
	m_hasAlarm=false;

	bool isToDelete=pTraj->IsToDelete(frame_id);	//Look if frame is going to be deleted, so delete alarm memory
	bool ret=false;

	if(isToDelete)
	{
		ret=false;
	}

	for(size_t i=0;i<m_vecAlarm.size();i++)
	{
		X7sVSAlrDetector *alrDetector=m_vecAlarm[i];

		if(alrDetector)
		{
			//If the trajectory is going to disappear delete it from the alarmDetector memory.
			if(isToDelete) alrDetector->DelTrajectory(pTraj->ID);

			//Then check if the alarm detector catch an event.
			if(alrDetector->Process(frame_id,pTraj))
			{
				//Add an alarm event to the vector.
				X7sVSAlarmEvt alrEvt=alrDetector->GetAlrEvt();
				if(alrEvt.warn) m_hasAlarm=true;
				m_vecAlrEvt.push_back(alrEvt);
				ret=true;
			}
		}
	}
	return ret;
}


/**
* @brief Add an alarm detector to the alarm manager.
*
* The alarm is added only if its parent is this Alarm Manager, or it doesn't have any parents.
*
* The given pointer is keep internally and automatically deleted when
* the alarm manager is destructed. Therefore pAlarm should never be deleted outside.
*
* @param pAlrDtr A pointer on an alarm detector.
* @return @true if the pAlarm was correctly added, otherwise it returns @false which means that the
* pointer must be deleted from outside.
*/
bool X7sVSAlrManager::AddAlrDector(X7sVSAlrDetector *pAlrDtr)
{
	X7S_FUNCNAME("X7sVSAlrManager::AddAlrDector()");

	if(pAlrDtr)
	{
		if(pAlrDtr->GetParent()==NULL) this->AddAsChild(pAlrDtr);
		if(pAlrDtr->GetParent()==this)
		{
			int type=pAlrDtr->GetType();
			X7S_CHECK_WARN(funcName,X7S_CHECK_RANGE(type,0,X7S_VS_ALR_TYPE_END),
				false,"type %d is not in correct range", type);


			m_vecAlarm.push_back(pAlrDtr);

			if(type==X7S_VS_ALR_TYPE_LINE)
			{
				const int BUFFER_SIZE=10;
				char buf[BUFFER_SIZE];
				int n=numTypeAlarm[type];
				if(n<10)
				{
					int ret = snprintf(buf,BUFFER_SIZE,"%03d %03d",20, 20*(1 + n%10));
					X7S_CHECK_WARN(funcName,ret < (int)BUFFER_SIZE,false,"Buffer overflow (snprintf), text is corrupted");
					pAlrDtr->SetParam("txt_pos",buf);
				}
			}

			numTypeAlarm[type]++;
			m_lastID=pAlrDtr->GetID();
			return true;
		}
		else
		{
			X7S_PRINT_WARN("X7sVSAlrManager::AddAlrDector()",
					"The alrDtr (ID=%d) already belongs to another parent",
					pAlrDtr->GetID());
		}
	}
	return false;
}



/**
* @brief Get alarm detector given its ID.
*/
X7sVSAlrDetector* X7sVSAlrManager::GetByID(int ID) const
{
	X7sVSAlrDetector *pAlarm=NULL;
	for(size_t i=0;i<m_vecAlarm.size();i++)
	{
		if(m_vecAlarm[i] && (m_vecAlarm[i]->GetID()==ID))
		{
			pAlarm=m_vecAlarm[i];
		}
	}
	return pAlarm;
}

/**
* @brief Return the last alarm detector inserted in the list.
* @return It only return @NULL when no alarm detector has been added previously.
*/
X7sVSAlrDetector* X7sVSAlrManager::GetLast() const
{
	if(m_vecAlarm.size()>0) return m_vecAlarm[m_vecAlarm.size()-1];
	else return NULL;
}

/**
* @brief Delete the Alarm Detector with the corresponding ID.
* @param ID
*/
void X7sVSAlrManager::Delete(int ID)
{
	vector<X7sVSAlrDetector*>::iterator it;
	for(it=m_vecAlarm.begin() ; it < m_vecAlarm.end(); it++)
	{
		if((*it) && (*it)->GetID()==ID)
		{
			X7S_DELETE_PTR((*it));	//Delete the pointer.
			it=m_vecAlarm.erase(it);
		}
	}
}


/**
* @brief Return the last position of each blob in a array of byte.
*/
const X7sByteArray* X7sVSAlrManager::GetData(bool full_data)
{
	size_t i;
	int expected_length;
	uint8_t *p = m_pData->data;
	uint8_t *end = m_pData->data+m_pData->capacity;
	m_pData->length=0;
	int nof_alarms=0;
	const char name[6]={'X','7','S','A','L','R'};

	//First write the name
	p=x7sMemWrite(p,end,name,sizeof(name));

	//Then write if the data are full or not.
	uint8_t fulldata=full_data;
	p=x7sMemWrite(p,end,&fulldata,sizeof(fulldata));

	expected_length=IX7S_VS_DATAHDR_SIZE;

	//----------- Full data

	//Add the definition of the first
	if(full_data)
	{
		std::vector<const X7sByteArray*> vecAlrData;
		for(i=0;i<m_vecAlarm.size();i++)
		{
			const X7sByteArray* bArray = m_vecAlarm[i]->GetData();
			if(bArray)
			{
				vecAlrData.push_back(bArray);
				expected_length+=bArray->length+4;//Size to write the length
			}
		}

		//First check the size
		if(expected_length > m_pData->capacity)
		{
			X7S_PRINT_ERROR("X7sVSAlarmManager::GetData()",
					"Size of data=%d is bigger than memory buffer %d",
					expected_length,m_pData->capacity);
			return NULL;
		}

		nof_alarms=vecAlrData.size();

		//Then write the number of blob
		p=x7sMemWrite(p,end,&nof_alarms,sizeof(nof_alarms));

		for(i=0; i<vecAlrData.size(); i++)
		{
			const X7sByteArray* bArray = vecAlrData[i];
			if(bArray==NULL) continue;

			//Write the size of the data.
			int32_t tmp=bArray->length;
			p=x7sMemWrite(p,end,&tmp,sizeof(int32_t));

			//And write the data obtain previously.
			p=x7sMemWrite(p,end,bArray->data,bArray->length);
			if(p==NULL) break;
		}
	}

	//Not full data

	else
	{
		//First check how many actual element we have
		nof_alarms=m_vecAlrEvt.size();
		expected_length+=nof_alarms*sizeof(X7sVSAlarmEvt);


		//First check the size
		if(expected_length > m_pData->capacity)
		{
			X7S_PRINT_ERROR("X7sVSAlarmManager::GetData()",
					"Size of data=%d is bigger than memory buffer %d",
					expected_length,m_pData->capacity);
			return NULL;
		}

		//Then write the number of blob
		p=x7sMemWrite(p,end,&nof_alarms,sizeof(nof_alarms));
		//Then loop over each trajectory
		for(i=0; i<m_vecAlrEvt.size(); i++)
		{
			X7sVSAlarmEvt* pAlrEvt = &(m_vecAlrEvt[i]);

			//And write the lastPosition
			p=x7sMemWrite(p,end,pAlrEvt,sizeof(X7sVSAlarmEvt));
			if(p==NULL) break;
		}
	}
	if(p==NULL)
	{
		X7S_PRINT_WARN("X7sVSAlarmManager::GetData()","Failed to write blob %d",i);
		return NULL;
	}

	m_pData->length=(p-m_pData->data);
	return m_pData;
}


/**
* @brief Set the given trajectory with an Array of byte.
*/
bool X7sVSAlrManager::SetData(const X7sByteArray *pData, int frameID)
{
	int i;
	uint8_t *p = pData->data;
	uint8_t *end = pData->data+pData->length;
	uint8_t fulldata=false;
	int nof_alarms=0;
	const char name[6]={'X','7','S','A','L','R'};
	char buff[6];


	//Set the current frame
	m_frameID =frameID;
	m_vecAlrEvt.clear();

	//First look if it the correct type of data
	p=x7sMemRead(p,end,buff,sizeof(name));
	if(memcmp(name, buff,sizeof(name))!=0)
	{
		X7S_PRINT_WARN("X7sBlobTrajGenerator::SetData()",
				"The data given does not correspond to the required type");
		return false;
	}

	//Then read if we have the full data
	p=x7sMemRead(p,end,&fulldata,sizeof(fulldata));

	//Then read the number of blob
	p=x7sMemRead(p,end,&nof_alarms,sizeof(nof_alarms));

	if(fulldata)
	{
		ix7sVSAlrDtrHdr *pAlrHdr;
		for(i=nof_alarms; i>0; --i)
		{
			//Obtain the size of each sub X7sByteArray*
			int32_t tmp=0;
			p=x7sMemRead(p,end,&tmp,sizeof(tmp));

			//Assign the subarray
			X7sByteArray bArray;
			bArray.data=p;
			bArray.length=tmp;
			bArray.capacity=tmp;

			//First find the pointing zone.
			pAlrHdr=(ix7sVSAlrDtrHdr*)p;

			X7sVSAlrDetector *pAlrDtr = this->GetByID(pAlrHdr->id);
			if(pAlrDtr) pAlrDtr->SetData(&bArray);

			p+=bArray.length;	//Jump to the next position
		}
	}
	else
	{
		//And loop over each blob.
		X7sVSAlarmEvt alrEvt;
		for(i=nof_alarms; i>0; --i)
		{
			//Read the data in a blob
			p=x7sMemRead(p,end,&alrEvt,sizeof(X7sVSAlarmEvt));
			if(p==NULL) break;

			//And add the blob to the list of trajectory.
			X7sVSAlrDetector *alr = this->GetByID(alrEvt.alarm_ID);
			if(alr) alr->Process(frameID,alrEvt);
			m_vecAlrEvt.push_back(alrEvt);
		}
	}
	if(p==NULL)
	{
		X7S_PRINT_WARN("X7sVSAlrManager::SetData()","Failed to read blob %d",i);
		return false;
	}
	return true;
}


/**
* @brief Return a buffer with the definition of the alarm detectors.
*/
const X7sByteArray* X7sVSAlrManager::GetDefinition()
{
	//Declare the node
	TiXmlElement node("alarms");
	TiXmlPrinter printer;

	//Obtain the definition as a text
	this->WriteToXML(&node,true);
	printer.SetIndent("");
	node.Accept(&printer);
	const char *alrDef = printer.CStr();

	//Copy it in a ByteArray
	if(m_pDefData) x7sReleaseByteArray(&m_pDefData);
	m_pDefData = x7sCreateByteArray(strlen(alrDef));
	m_pDefData->length=strlen(alrDef);
	memcpy(m_pDefData->data,alrDef,m_pDefData->length);

	return m_pDefData;
}

/**
* @brief Return the number of alarm detectors.
* @param alarmType if it's correspond to a @ref X7S_VS_ALR_TYPE_xxx, it return the number of alarm
* detector for this type only. Otherwise, it return the total number of alarm detector.
*/
int X7sVSAlrManager::Size(int alarmType) const
{
	if(X7S_VS_ALR_TYPE_GLOB <= alarmType && alarmType < X7S_VS_ALR_TYPE_END)
	{
		return numTypeAlarm[alarmType];
	}
	else return m_vecAlarm.size();
}

/**
* @brief Return a string with the content of X7sVSAlrManager
*/
std::string X7sVSAlrManager::toString(bool recursive) const
{
	stringstream sstr;
	sstr << "X7sVSAlrManager (#" << m_vecAlarm.size() << "): ";
	for(size_t i=0;i<m_vecAlarm.size();i++)
	{
		X7sVSAlrDetector *pAlrDtr = m_vecAlarm[i];
		if(pAlrDtr==NULL) continue;
		if(recursive)  sstr << endl << pAlrDtr->toString();
		else sstr << "ID="<<pAlrDtr->GetID() <<" (type="<<X7sVSAlrDetector::GetTypeName(pAlrDtr->GetType()) <<"), ";
	}
	return sstr.str();
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

/**
* @brief Create a Child from XML node.
*
* This method is called from ReadFromXML(), when it found xml children nodes which are not <param></params>.
*
* @param child the given node.
* @return	@true if the operation was performed correctly
*/
bool X7sVSAlrManager::CreateChildFromXML(const TiXmlElement *child)
{
	if(child==NULL)
	{
		this->DeleteAll();
		return true;
	}
	else {

		//Call the static interface.
		X7sVSAlrDetector *pAlarm = X7sCreateVSAlarm(this,child);

		//If it was correct add this alarm to the list.
		if(pAlarm && pAlarm->IsValid()) return true;
		else X7S_DELETE_PTR(pAlarm);
	}
	return false;
}




/**
* @brief Delete All alarm detector.
*/
void X7sVSAlrManager::DeleteAll()
{
	for(size_t i=0;i<m_vecAlarm.size();i++)
	{
		X7S_DELETE_PTR(m_vecAlarm[i]);
	}
	m_vecAlarm.clear();
}
