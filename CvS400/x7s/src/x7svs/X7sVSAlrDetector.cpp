#include "ix7svidsurv.h"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sVSAlrDtrGlobal.hpp"
#include "X7sVSAlrDtrLine.hpp"
#include "X7sVSAlrDtrPoly.hpp"
#include "X7sVSLog.hpp"
#include <sstream>
using std::stringstream;

const char* ix7sVSAlarmNames[X7S_VS_ALR_TYPE_END] =
{
		"global",
		"line",
		"polygon",
		"wrgdir",
		"prowl",
		"and",
};

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
X7sVSAlrDetector::X7sVSAlrDetector(X7sVSAlrManager *parent, uint8_t type)
:X7sXmlObject(parent,X7S_VS_TAGNAME_ALRDTR,(int)type), alrMan(parent),
m_frame(-1), m_start(-1), m_pData(NULL)
{
	//Set all the memory to zeros.
	memset(&(alrEvt),0,sizeof(alrEvt));

	//Then reset it to have the ID=-1
	ResetAlrEvt(-1);

	stringstream sstr;
	if(type==X7S_VS_ALR_TYPE_GLOB)  sstr << GetTypeName(type);
	else sstr << GetTypeName(type) << " #"<< this->GetID();

	m_params.Insert(X7sParam("name",X7S_XML_ANYID,sstr.str().c_str()));
	m_params.Insert(X7sParam("enable",X7S_XML_ANYID,1,X7STYPETAG_INT,0,1));
	m_params.Insert(X7sParam("txt_pos",X7S_XML_ANYID,"20 20",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("color",X7S_XML_ANYID,"255 255 255 180",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("color_bg",X7S_XML_ANYID,"0 0 0 180",X7STYPETAG_STRVEC));

	X7S_PRINT_DEBUG("X7sVSAlrDetector::X7sVSAlrDetector()","setup with type %d and calling AddAlrDector",type);
	alrMan->AddAlrDector(this);
}


X7sVSAlrDetector::~X7sVSAlrDetector()
{
	x7sReleaseByteArray(&(m_pData));
}
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

int X7sVSAlrDetector::GetEndFrame()
{
	return m_start+m_params["min_duration"]->toIntValue();
}

/**
 * @brief Delete a Trajectory from a the alarm.
 * @param ID the ID of the trajectory
 */
void X7sVSAlrDetector::DelTrajectory(int ID)
{
	X7S_PRINT_DUMP("x7sVSAlrDetector::DelTrajectory()", "Does nothing need to be virtualized");
}


/**
* @brief Obtain the internal counter and list of an alarm detector.
* @return An array of byte with the proper memory.
*/
const X7sByteArray* X7sVSAlrDetector::GetData()
{
		X7S_PRINT_DEBUG("X7sVSAlrDtrLine::GetData()", "Not Implemented function");
		return NULL;
};


/**
* @brief Set the internal counters/lists of an alarm detector.
* @param pData An array of byte with the proper memory.
* @return @true if the alarm detector was correctly updated.
*/
bool X7sVSAlrDetector::SetData(const X7sByteArray *pData)
{
	X7S_PRINT_DEBUG("X7sVSAlrDtrLine::SetData()", "Not Implemented function");
	return false;
}

/**
 * @brief Re-implement ReadFromXML adding the Reload method.
 * @see X7sXmlObject::ReadFromXML();
 */
bool X7sVSAlrDetector::ReadFromXML(const TiXmlNode *cnode)
{
	bool ret = X7sXmlObject::ReadFromXML(cnode);
	this->Reload();
	return ret;
}

/**
 * @brief Reload the drawing parameters of the alarm dectector.
 *
 * This method must be call if the X7sParam color, position, etc... has been changed.
 */
void X7sVSAlrDetector::Reload()
{
	//Empty method look-at the herited one.
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

/**
 * @brief Reset an alarm event with the default value.
 *
 * This function should be used before processing any information because,
 * it will be written in alrEvt.
 *
 * @param blobID The ID used to reset the event for a specific blob.
 */
void X7sVSAlrDetector::ResetAlrEvt(int blobID)
{
	alrEvt.alarm_ID=(uint8_t)this->GetID();
	alrEvt.blob_ID=blobID;
	alrEvt.val=X7S_VS_ALR_EVT_NONE;
	alrEvt.warn=false;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Static Methods / Interface Functions
//----------------------------------------------------------------------------------------

/**
 * @brief Static function to return the type in a c-string
 * @param type The given type @ref X7S_VS_ALR_TYPE_xxx
 * @return The name which correspond to the type value.
 */
const char* X7sVSAlrDetector::GetTypeName(int type)
{
	if(0<= type && type < X7S_VS_ALR_TYPE_END)
	{
		return ix7sVSAlarmNames[type];
	}
	return "UNKNOWN";
}


/**
* Create an VideoSurveillance Alarm given an XML node.
* @param parent The parent alarm manager.
* @param cnode The XMlNode which must have the name of X7S_VS_TAGNAME_ALRDTR
* @return A polymorphic pointer on a X7sVSAlrDetector if the XML node was correctly read,
* Otherwise it return NULL.
*/
X7sVSAlrDetector* X7sCreateVSAlarm(X7sVSAlrManager *parent,const TiXmlNode *cnode)
{
	//Declaration of variable
	X7sVSAlrDetector *pAlarm=NULL;
	const TiXmlElement *xmlelem=NULL;
	const char *tagname=NULL;

	//Check input and set variable
	CV_FUNCNAME("x7sCreateVSAlarm()");
	IX7S_CHECK_ARG_NULLPTR(cvFuncName,cnode,pAlarm);
	xmlelem = cnode->ToElement();
	IX7S_CHECK_ARG_NULLPTR(cvFuncName,xmlelem,pAlarm);
	tagname = xmlelem->Value();
	IX7S_CHECK_ARG_NULLPTR(cvFuncName,tagname,pAlarm);

	//It is not a typical parameters.
	if(strcmp(tagname,X7S_VS_TAGNAME_ALRDTR)==0)
	{
		int type;
		int ID=-1;
		if(xmlelem->Attribute("type",&type))
		{

			xmlelem->Attribute("ID",&ID);
			 X7S_PRINT_INFO("X7sCreateVSAlarm()","tagname=%s, type=%s (%d), id=%d",
					 tagname,X7sVSAlrDetector::GetTypeName(type),type,ID);

			switch(type)
			{
			case X7S_VS_ALR_TYPE_GLOB:
				pAlarm=(X7sVSAlrDetector*)new X7sVSAlrDtrGlobal(parent);
				break;
			case X7S_VS_ALR_TYPE_LINE:
				pAlarm=(X7sVSAlrDetector*)new X7sVSAlrDtrLine(parent);
				break;
			case X7S_VS_ALR_TYPE_POLY:
				pAlarm=(X7sVSAlrDetector*)new X7sVSAlrDtrPoly(parent);
				break;
			case X7S_VS_ALR_TYPE_WRGDIR:
			case X7S_VS_ALR_TYPE_PROWL:
			default:
				 X7S_PRINT_WARN("X7sCreateVSAlarm()","Wrong type %d",type);
				break;
			}

			if(pAlarm) {
				pAlarm->ReadFromXML(cnode);
			}
		}

	}
	return pAlarm;
}
