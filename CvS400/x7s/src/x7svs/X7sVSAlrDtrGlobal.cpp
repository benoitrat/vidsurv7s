#include "X7sVSAlrDtrGlobal.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sVSLog.hpp"
#include <sstream>
using std::stringstream;

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Defines, Enums and Prototypes.
//----------------------------------------------------------------------------------------
#define IX7S_VS_POLY_DIST_OFFSET  2


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

X7sVSAlrDtrGlobal::X7sVSAlrDtrGlobal(X7sVSAlrManager *parent)
:X7sVSAlrDetector(parent,X7S_VS_ALR_TYPE_GLOB)
{
	m_params.Insert(X7sParam("in_rect",X7S_XML_ANYID,"0 0 0 0",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("draw_rect",X7S_XML_ANYID,"0",X7STYPETAG_BOOL));
	m_params.Insert(X7sParam("color_fill",X7S_XML_ANYID,"0 0 0 0",X7STYPETAG_STRVEC));


	m_params["name"]->SetValue("global");

	txt_pos = cvPoint(20,20);
	color=cvScalar(255,255,255,180);
	color_bg=cvScalar(0,0,0,180);
	color_fill=cvScalarAll(0);

	m_mapBlob.clear();
	m_countHide=0;
}


X7sVSAlrDtrGlobal::~X7sVSAlrDtrGlobal()
{
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------


/**
* @brief Calculate if a point is inside a polygon or not.
* @param frame_id
* @param pTraj_cst
* @return
*/
bool X7sVSAlrDtrGlobal::Process(int frame_id,const X7sBlobTrajectory *pTraj_cst)
{
	bool ret=false;
	if(m_params["enable"]->toIntValue()==false) return ret;

	//Set in which frame we are.
	m_lastFrame=frame_id;

	//For compatibility remove keyword const
	X7sBlobTrajectory* pTraj=(X7sBlobTrajectory*)pTraj_cst;

	//First reset the alarm event.
	ResetAlrEvt(pTraj->ID);

	if(pTraj && pTraj->GetLastBlob())
	{

		//If this blob doesn't exist create it.
		if(m_mapBlob.find(pTraj->ID)==m_mapBlob.end())
		{
			m_mapBlob[pTraj->ID].frm_start=frame_id;
			m_mapBlob[pTraj->ID].frm_hide=-1;
		}

		//Obtain the polygon values.
		CvPoint2D32f pt=IX7S_BLOB_TRAJ_POINT2D32F(pTraj->GetLastBlob());

		//Internal the zone.
		if(IsInside(pt))
		{
			if(pTraj->IsToDelete(frame_id)) 	{
				alrEvt.val=X7S_VS_ALR_EVT_HIDE;
				m_countHide++;
				DelTrajectory(pTraj->ID);
			}
			else {
				alrEvt.val=X7S_VS_ALR_EVT_SHOW;
				m_countHide--;
			}
		}
		//External zone
		else
		{
			if(m_mapBlob[pTraj->ID].frm_start==frame_id)
			{
				alrEvt.val=X7S_VS_ALR_EVT_IN;
			}
			else {
				if(pTraj->IsToDelete(frame_id)) 	{
					alrEvt.val=X7S_VS_ALR_EVT_OUT;
					DelTrajectory(pTraj->ID);
				}
			}
		}

		ret=(alrEvt.val!=X7S_VS_ALR_EVT_NONE);
		if(ret) PrintAlrEvt();
	}
	return ret;
}


/**
* @brief Process the alarm detector giving it the alarm event.
* @param frame_id The ID of the frame corresponding to this alarm.
* @param evt An alarm event generated previously by the same detector.
* @return @true if the X7sVSAlarmEvt correspond to this alarm detector, @false otherwise.
*/
bool X7sVSAlrDtrGlobal::Process(int frame_id,const X7sVSAlarmEvt& evt)
{
	//Check if it belong to
	if(evt.alarm_ID==this->GetID())
	{
		//Use default copy constructor
		alrEvt=evt;

		PrintAlrEvt();

		//Set in which frame we are.
		m_lastFrame=frame_id;

		//Check if it is an event.
		switch(alrEvt.val)
		{
		case X7S_VS_ALR_EVT_IN:
			m_mapBlob[alrEvt.blob_ID].frm_start=frame_id;
			return true;
		case X7S_VS_ALR_EVT_OUT:
			DelTrajectory(alrEvt.blob_ID);
			return true;
		case X7S_VS_ALR_EVT_SHOW:
			m_countHide--;
			return true;
		case X7S_VS_ALR_EVT_HIDE:
			m_mapBlob[alrEvt.blob_ID].frm_hide=frame_id;
			DelTrajectory(alrEvt.blob_ID);
			m_countHide++;
			return true;
		case X7S_VS_ALR_EVT_NONE:
			X7S_PRINT_DEBUG("X7sVSAlrDtrLine::Process()","AlrEvt.val should not be X7S_VS_ALR_EVT_NONE");
			return false;
		default:
			X7S_PRINT_WARN("X7sVSAlrDtrLine::Process()","Unkown alrEvt.val=%d",alrEvt.val);
			return false;
		}
	}
	return false;
}


/**
* @brief Draw the the global limit
* @param pImg Image on which we want to draw
* @param hue desired color (if it's negative we use the color of the detector)
* @param thickness Size of the pencil use to draw the line, points, circles...
*/
void X7sVSAlrDtrGlobal::Draw(IplImage *pImg,int hue, int thickness)
{
	if(pImg)
	{
		CvFont font;
		CvScalar color = cvScalarAll(255);
		if(hue<0) color=this->color;
		else 	color = x7sCvtHue2BGRScalar(hue,360,360);

		if(m_params["draw_rect"]->toIntValue())
		{
			cvDrawRect(pImg,cvPoint((int)in_rect.val[0],(int)in_rect.val[1]),cvPoint((int)in_rect.val[2],(int)in_rect.val[3]),color,thickness);
		}

		int baseline;
		CvSize font_ROI;
		stringstream sstr;
		bool is_notempty=false;
		cvInitFont(&font,CV_FONT_HERSHEY_PLAIN, 0.8,0.8, 0,1,5);

		sstr << "Actual: " << m_mapBlob.size()-m_countHide;
		is_notempty=true;
		if(is_notempty) sstr << " / ";
		sstr << "Hidden: " << m_countHide;

		cvGetTextSize(sstr.str().c_str(), &font, &font_ROI,&baseline);
		cvSetImageROI(pImg,cvRect(txt_pos.x-1,txt_pos.y-font_ROI.height-2,font_ROI.width+2,font_ROI.height+4));
		cvSet(pImg,color_bg);
		cvResetImageROI(pImg);
		cvPutText(pImg,sstr.str().c_str(),txt_pos,&font,color);



	}
}


/**
* @brief Delete the id in the map that keep if the last position
* in the trajectory was right/left of line vector.
* @see X7sVSAlrDetector::DelTrajectory
*/
void X7sVSAlrDtrGlobal::DelTrajectory(int ID)
{
	std::map<int,IX7sBlobHistory>::iterator it=m_mapBlob.find(ID);
	if(it!=m_mapBlob.end())
	{
		if(alrEvt.val==X7S_VS_ALR_EVT_HIDE)
		{
			m_mapBlob[ID].frm_hide=m_lastFrame;
			X7S_PRINT_DEBUG("X7sVSAlrDtrGlobal::DelTrajectory()", "Hide blobID=%d",ID);
		}
		else
		{
			m_mapBlob.erase(it);
			X7S_PRINT_DEBUG("X7sVSAlrDtrGlobal::DelTrajectory()", "Erase blobID=%d",ID);
		}
	}
}

/**
* @brief Return if the point is inside or outside the internal zone.
* @param pt
* @return
*/
bool X7sVSAlrDtrGlobal::IsInside(const CvPoint2D32f& pt)
{
	return (
			(in_rect.val[0] <= pt.x && pt.x <= in_rect.val[2]) &&
			(in_rect.val[1] <= pt.y && pt.y <= in_rect.val[3])
	);
}


/**
* @brief Reload position, color and min_duration of the alarm.
*/
void X7sVSAlrDtrGlobal::Reload()
{
	//Obtain the parameters for drawing
	X7sXmlParamGetVector(&txt_pos,m_params["txt_pos"]);
	X7sXmlParamGetVector(&color,m_params["color"]);
	X7sXmlParamGetVector(&color_bg,m_params["color_bg"]);
	X7sXmlParamGetVector(&color_fill,m_params["color_fill"]);
	X7sXmlParamGetVector(&in_rect,m_params["in_rect"]);
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------


/**
* @brief Print the internal alarm event if an event occurs.
*/
void X7sVSAlrDtrGlobal::PrintAlrEvt()
{
	const char *funcName = "X7sVSAlrDtrGlobal::Process()";

	switch(alrEvt.val)
	{

	case X7S_VS_ALR_EVT_IN:
		X7S_PRINT_INFO(funcName,"Alert: Blob %d enter inside the video",
				alrEvt.blob_ID);
		break;
	case X7S_VS_ALR_EVT_OUT:
		X7S_PRINT_INFO(funcName,"Alert: Blob %d go outside video",
				alrEvt.blob_ID);
		break;
	case X7S_VS_ALR_EVT_SHOW:
		X7S_PRINT_INFO(funcName,"Alert: Blob %d appears inside the Internal Zone",
				alrEvt.blob_ID);
		break;
	case X7S_VS_ALR_EVT_HIDE:
		X7S_PRINT_INFO(funcName,"Alert: Blob %d disappears inside the Internal Zone",
				alrEvt.blob_ID);
		break;
	}
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Interfaces
//----------------------------------------------------------------------------------------

/**
* @brief Construct an Alarm with a polygon.
* An alarm occurs when someone is inside the zone defined by the polygon.
* @param parent The parent alarm manager.
* @param width Width of the whole image.
* @param height Heigh of the whole image.
* @param ratio Percentage of the internal box compare to the whole image.
* If the ratio is small (<0.3), then object tends to never appear/disappear
* from the scene. If the ratio is big (>0.8), it is possible that some objects
* are considered hidden when they are in reality exiting the scene.
* @param name The name of the Alarm.
* @return Return a generic alarm object.
*/
X7sVSAlrDetector *x7sCreateVSAlarmGlobal(X7sVSAlrManager *parent,int width, int height,float ratio, const char *name)
{
	//Create the object
	X7sVSAlrDtrGlobal *alrGlob = new X7sVSAlrDtrGlobal(parent);

	CvSize imSize=cvSize(width,height);

	int dx=cvFloor(imSize.width*(1-ratio));
	int dy=cvFloor(imSize.height*(1-ratio));

	alrGlob->SetParam("in_rect","%d %d %d %d",dx,dy,imSize.width-dx,imSize.height-dy);
	if(name) alrGlob->SetParam("name",name);
	alrGlob->Reload();

	return (X7sVSAlrDetector*)alrGlob;
}

