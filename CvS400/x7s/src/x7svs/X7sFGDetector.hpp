/**
 *  @file
 *  @brief Contains the class X7sFGDetector.hpp
 *  @date 19-may-2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SFGDETECTOR_HPP_
#define X7SFGDETECTOR_HPP_

#include "x7svidsurv.h"

using std::map;

//========================================================================
//-- Defines, Enumerates and Typedefs.
typedef std::map<int, const char*> AssoArray;
#define X7S_BG_MODEL_DEFAULT X7S_BG_MODEL_BCD


//========================================================================
//-- Declaration of implementaded class

/**
 * @brief Implementation of the CvFGDector abstract class.
 *
 * This class is configure through an XML node:
 */
class X7sFGDetectorImpl: public X7sFGDetector
{
public:
	X7sFGDetectorImpl(X7sXmlObject *parent, int type);
	~X7sFGDetectorImpl();
	void Release(){delete this;};

	virtual IplImage* GetMask();
	virtual IplImage* GetBackGround();
	virtual void Process(IplImage *pImg);
	virtual void SetNoInterest(IplImage *pImg);
	virtual bool Init(int type);


	static AssoArray GetTypeNameMap();

protected:

	void PostFilter(const IplImage *src, IplImage *dst);
	void PostFilterCBK(const IplImage *src, IplImage *dst);
	void PostFilterBCD(const IplImage *src, IplImage *dst);

	void AddParam(const char* name, float* pAddr);
	void AddParam(const char* name, double* pAddr);
	void AddParam(const char* name, int* pAddr);

	CvBGStatModel * m_bgModel;
	X7sBGStatModelParams *m_bgParams;
	int cvtcode_frombgr;
	int cvtcode_tobgr;
	bool m_postFilterFG;		//!< Filter the FG image outside BG-update loop.
	std::map<int,const char*> m_typeNameMap;
	int m_Wnd;

private:
	IplImage* GetForeGround();
	IplImage* m_filterFG;
	IplImage* m_imBG;
	IplImage* m_imInput;


};


#endif /* X7SFGDETECTOR_HPP_ */
