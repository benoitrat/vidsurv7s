#include "X7sVSAlrDtrPoly.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sVSLog.hpp"
#include <sstream>
using std::stringstream;

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Defines, Enums and Prototypes.
//----------------------------------------------------------------------------------------
#define IX7S_VS_POLY_DIST_OFFSET  2


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

X7sVSAlrDtrPoly::X7sVSAlrDtrPoly(X7sVSAlrManager *parent)
:X7sVSAlrDetector(parent,X7S_VS_ALR_TYPE_POLY),
m_contour(NULL)
{
	m_params.Insert(X7sParam("ptsX",X7S_XML_ANYID,"0 1 1 0",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("ptsY",X7S_XML_ANYID,"0 0 1 1",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("color_fill",X7S_XML_ANYID,"0 0 0 0",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("min_duration",X7S_XML_ANYID,60,X7STYPETAG_INT,1)); //Minimum duration of the event in number of frame.

	txt_pos = cvPoint(20,20);
	color=cvScalar(255,255,255,180);
	color_bg=cvScalar(0,0,0,180);
	color_fill=cvScalarAll(0);
}


X7sVSAlrDtrPoly::~X7sVSAlrDtrPoly()
{
	cvReleaseMat(&m_contour);
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------


/**
* @brief Calculate if a point is inside a polygon or not.
* @param frame_id
* @param pTraj_cst
* @return
*/
bool X7sVSAlrDtrPoly::Process(int frame_id,const X7sBlobTrajectory *pTraj_cst)
{
	if(m_params["enable"]->toIntValue()==false) return false;

	//For compatibility remove keyword const
	X7sBlobTrajectory* pTraj=(X7sBlobTrajectory*)pTraj_cst;

	int minDuration = m_params["min_duration"]->toIntValue();

	//First reset the alarm event.
	ResetAlrEvt(pTraj->ID);

	if(pTraj && pTraj->GetLastBlob())
	{
		//Obtain the polygon values.
		if(m_contour || ResetPolygon())
		{

			//Obtain the point position of the blob (considering the feet).
			CvPoint2D32f pt=IX7S_BLOB_TRAJ_POINT2D32F(pTraj->GetLastBlob());

			//Check if the point is inside the polygon.
			if(cvPointPolygonTest(m_contour,pt,IX7S_VS_POLY_DIST_OFFSET)>0)
			{

				//Then check if the point was not already inside.
				if(m_mapInside.find(pTraj->ID)==m_mapInside.end())
				{
					m_mapInside[pTraj->ID]=frame_id; //If not insert it.
					alrEvt.val=X7S_VS_ALR_EVT_IN;
				}

				//Check if the time spent is bigger.
				if((frame_id-m_mapInside[pTraj->ID])==minDuration && alrEvt.val==X7S_VS_ALR_EVT_NONE)
				{
					alrEvt.warn=true;
					alrEvt.val=X7S_VS_ALR_EVT_TOUT;
				}
			}
			else //If outside polygon
			{
				//If point was inside.
				if(m_mapInside.find(pTraj->ID)!=m_mapInside.end())
				{
					//Set an event that it goes out.
					alrEvt.val=X7S_VS_ALR_EVT_OUT;
					//Delete the trajectory because it goes outside.
					DelTrajectory(pTraj->ID);
				}
			}
			PrintAlrEvt();
		}
	}
	return (alrEvt.val!=X7S_VS_ALR_EVT_NONE);
}


/**
* @brief Process the alarm detector giving it the alarm event.
* @param frame_id The ID of the frame corresponding to this alarm.
* @param evt An alarm event generated previously by the same detector.
* @return @true if the X7sVSAlarmEvt correspond to this alarm detector, @false otherwise.
*/
bool X7sVSAlrDtrPoly::Process(int frame_id,const X7sVSAlarmEvt& evt)
{
	//Check if it belong to
	if(evt.alarm_ID==this->GetID())
	{
		//Use default copy constructor
		alrEvt=evt;

		PrintAlrEvt();

		//Check if it is an event.
		if(alrEvt.warn==false) {
			switch(alrEvt.val)
			{
			case X7S_VS_ALR_EVT_IN:
				m_mapInside[alrEvt.blob_ID]=frame_id;
				return true;
			case X7S_VS_ALR_EVT_OUT:
				DelTrajectory(alrEvt.blob_ID);
				return true;
			case X7S_VS_ALR_EVT_NONE:
				X7S_PRINT_DEBUG("X7sVSAlrDtrLine::Process()","AlrEvt.val should not be X7S_VS_ALR_EVT_NONE");
				return false;
			default:
				X7S_PRINT_WARN("X7sVSAlrDtrLine::Process()","Unkown alrEvt.val=%d",alrEvt.val);
				return false;
			}
		}
	}
	return false;
}


/**
* @brief Draw the polygon for the Polygon Alarm detector.
* @param pImg Image on which we want to draw
* @param hue desired color (if it's negative we use the color of the detector)
* @param thickness Size of the pencil use to draw the line, points, circles...
*/
void X7sVSAlrDtrPoly::Draw(IplImage *pImg,int hue, int thickness)
{
	if(pImg)
	{
		if(m_contour || ResetPolygon())
		{
			CvScalar color = cvScalarAll(255);
			if(hue<0) color=this->color;
			else 	color = x7sCvtHue2BGRScalar(hue,360,360);

			CvPoint *pts = (CvPoint*)m_contour->data.i;
			int npts=m_contour->rows;


			cvPolyLine(pImg, &pts, &npts,1,true,color,thickness);
			if(color_fill.val[3]>0)
			{
				cvFillConvexPoly(pImg, pts, npts,color_fill);
			}
		}
	}
}


/**
* @brief Delete the id in the map that keep if the last position
* in the trajectory was right/left of line vector.
* @see X7sVSAlrDetector::DelTrajectory
*/
void X7sVSAlrDtrPoly::DelTrajectory(int ID)
{
	std::map<int,int>::iterator it=m_mapInside.find(ID);
	if(it!=m_mapInside.end())
	{
		m_mapInside.erase(it);
		X7S_PRINT_DEBUG("X7sVSAlrDtrPoly::DelTrajectory()", "blobID=%d",ID);
	}
}

/**
* @brief Reload position, color and min_duration of the alarm.
*/
void X7sVSAlrDtrPoly::Reload()
{
	//Obtain the parameters for drawing
	X7sXmlParamGetVector(&txt_pos,m_params["txt_pos"]);
	X7sXmlParamGetVector(&color,m_params["color"]);
	X7sXmlParamGetVector(&color_bg,m_params["color_bg"]);
	X7sXmlParamGetVector(&color_fill,m_params["color_fill"]);

	this->ResetPolygon();
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

/**
* @brief Reset the internal polygon given by the params ptsX, and ptsY.
*/
bool X7sVSAlrDtrPoly::ResetPolygon()
{

	if(m_contour) cvReleaseMat(&m_contour);

	int sizeX = m_params["ptsX"]->GetLength();
	int sizeY = m_params["ptsY"]->GetLength();
	int size = X7S_MIN(sizeX,sizeY);

	if(size<=2) return false;
	m_contour = cvCreateMat(size,1,CV_32SC2);

	double tmp;
	int *ptr;
	for(int i=0;i<size;i++)
	{
		ptr= (int*)CV_MAT_ELEM_PTR(*(m_contour),i,0);
		if(m_params["ptsX"]->GetRealValue(i,&tmp)) 	ptr[0]=(int)tmp;
		if(m_params["ptsY"]->GetRealValue(i,&tmp)) 	ptr[1]=(int)tmp;
	}

	return true;
}

/**
* @brief Print the internal alarm event if an event occurs.
*/
void X7sVSAlrDtrPoly::PrintAlrEvt()
{
	const char *funcName = "X7sVSAlrPoly::Process()";


	switch(alrEvt.val)
	{

	case X7S_VS_ALR_EVT_IN:
		X7S_PRINT_INFO(funcName,"Alert: Blob %d enter inside polygon %s",
				alrEvt.blob_ID,m_params["name"]->toString().c_str());
		break;
	case X7S_VS_ALR_EVT_OUT:
		X7S_PRINT_INFO(funcName,"Alert: Blob %d go outside polygon %s",
				alrEvt.blob_ID,m_params["name"]->toString().c_str());
		break;
	case X7S_VS_ALR_EVT_TOUT:
		X7S_PRINT_INFO(funcName,"Alert: Blob %d stay in polygon %s for %d frames",
				alrEvt.blob_ID,m_params["name"]->toString().c_str(),
				m_params["min_duration"]->toIntValue());
		break;
	}
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Interfaces
//----------------------------------------------------------------------------------------

/**
* @brief Construct an Alarm with a polygon.
* An alarm occurs when someone is inside the zone defined by the polygon.
* @param parent The parent alarm manager.
* @param n_pts Number of points to define the polygon.
* @param x_pts A pointer on a array which contains the x coordinate of each point.
* @param y_pts A pointer on a array which contains the x coordinate of each point.
* @param name The name of the Alarm.
* @return Return a generic alarm object.
*/
X7sVSAlrDetector *x7sCreateVSAlarmPoly(X7sVSAlrManager *parent,int n_pts, int *x_pts, int *y_pts, const char *name)
{
	//Create the object
	X7sVSAlrDtrPoly *alrPoly = new X7sVSAlrDtrPoly(parent);

	//Setup the principal parameters
	stringstream sstr_x, sstr_y;

	if(x_pts!=NULL && y_pts!=NULL)
	{
		for(int i=0;i<n_pts;i++)
		{
			sstr_x << x_pts[i] << " ";
			sstr_y << y_pts[i] << " ";
		}

		alrPoly->SetParam("ptsX",sstr_x.str().c_str());
		alrPoly->SetParam("ptsY",sstr_y.str().c_str());
	}

	if(name) alrPoly->SetParam("name",name);
	alrPoly->Reload();

	return (X7sVSAlrDetector*)alrPoly;
}

