#include "ix7svidsurv.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sVSLog.hpp"
#include <cstring>
#include <iostream>
#include "x7snet.h"

using namespace std;



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

X7sBlobTrajectory::X7sBlobTrajectory(const CvBlob& blob)
:CvBlob(blob), m_blobSeq((sizeof(CvBlob))), m_frameBegin(-1), m_frameLast(-1), m_frameHidden(-1),
 m_minDist(3), m_smoothSize(5),  m_isExternalHidden(false), m_pData(NULL)

{

}

X7sBlobTrajectory::X7sBlobTrajectory(const X7sByteArray* pData)
:CvBlob(), m_blobSeq((sizeof(CvBlob))), m_frameBegin(-1), m_frameLast(-1),  m_frameHidden(-1),
 m_minDist(3), m_smoothSize(5), m_isExternalHidden(false), m_pData(NULL)
{
	int32_t tmp;
	if(pData==NULL)
	{
		X7S_PRINT_WARN("X7sBlobTrajectory::X7sBlobTrajectory()","pData is NULL");
	}

	//Clear old blob sequence
	m_blobSeq.Clear();

	//Set the pointers
	uint8_t *p=pData->data;
	uint8_t *end=pData->data+pData->capacity;

	CvBlob blob;
	int32_t nof_frames;
	p=x7sMemRead(p,end,&nof_frames,sizeof(nof_frames));

	p=x7sMemRead(p,end,&tmp,sizeof(tmp));
	if(p) m_frameBegin=tmp;
	p=x7sMemRead(p,end,&tmp,sizeof(tmp));
	if(p) m_frameLast=tmp;

	for(int j=0;j<nof_frames;j++)
	{
		//Read the data in a blob
		p=x7sMemRead(p,end,&blob,sizeof(CvBlob));
		if(p==NULL) break;
		m_blobSeq.AddBlob(&blob);
	}

	CvBlob *pBlob = m_blobSeq.GetBlob(0);
	this->ID = pBlob->ID;
	this->x = pBlob->x;
	this->y = pBlob->y;
	this->w = pBlob->w;
	this->h = pBlob->h;

	//X7S_PRINT_INFO("X7sBlobTrajectory::X7sBlobTrajectory()","%s",toString().c_str());
}

/**
* @brief Copy constructor to ensure that pData is correctly copied.
* @param copy The Blob trajectory we want to copy.
*/
X7sBlobTrajectory::X7sBlobTrajectory(const X7sBlobTrajectory& copy)
{
	X7S_PRINT_DEBUG("X7sBlobTrajectory::X7sBlobTrajectory(const X7sBlobTrajectory& copy)");
	*this = copy;
}

X7sBlobTrajectory& X7sBlobTrajectory::operator=(const X7sBlobTrajectory& copy)
{
	if(copy.m_pData!=NULL)
	{
		this->m_pData=x7sCreateByteArray(copy.m_pData->capacity);
		this->m_pData->length=copy.m_pData->length;	//Copy the length.
		memcpy(this->m_pData->data,copy.m_pData->data,copy.m_pData->length);
	}
	return *this;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------


bool X7sBlobTrajectory::AddBlob(CvBlob* pBlob, int frameID)
{

	m_frameLast=frameID;
	if(pBlob->ID != ID) return false;

	if(m_blobSeq.GetBlobNum()==0) {
		m_frameBegin=frameID;
	}
	else {
		const CvBlob *last = GetLastBlob();
		float dist = IX7S_BLOB_CDISTANCE(last,pBlob);
		if(dist < m_minDist) return false;
	}
	m_blobSeq.AddBlob(pBlob); 	//Create a copy of the blob in the memory.
	Smooth(m_smoothSize);
	if(m_isExternalHidden==false) m_frameHidden=-1; //Reset hidden field
	return true;
}

/**
* @brief Return the blob given its index.
*
* The blob are added to the trajectory using FILO stack. To retrieve them one
* can play with the index:
* 		-	The first blob added to the trajectory correspond to index=0
* 		-	The second first blob added to the trajectory correspond to index=1
* 		-	The last blob added to the trajectory correspond to index=-1
* 		- 	The penultimate added, correspond to index=-2
* 		- 	If the index is not in the correct range, the value returned is @NULL.
*
* @param index of the blob (negative or positive value)
* @return A pointer on a blob if a correct index has been given.
*/
const CvBlob *X7sBlobTrajectory::GetBlob(int index)
{
	CvBlob *pBlob=NULL;
	if(index >= 0) 	pBlob=m_blobSeq.GetBlob(index);
	else 	{
		int length = m_blobSeq.GetBlobNum();
		pBlob=m_blobSeq.GetBlob(length+index);
	}
	return (const CvBlob*)pBlob;
}

/**
* @brief Retrieve the last blob added to the list
* @see X7sBlobTrajectory::GetBlob().
*/
const CvBlob* X7sBlobTrajectory::GetLastBlob()
{
	return this->GetBlob(-1);
}

/**
* @brief Smooth the trajectory
* @param filterSize The size of the trajectory to be smooth.
*/
void X7sBlobTrajectory::Smooth(int filterSize)
{
	int N=m_blobSeq.GetBlobNum();
	int i;

	if(N>filterSize)
	{
		CvBlob meanBlob = cvBlob(0,0,0,0);
		CvBlob *pBlob;
		for(i=N-1;i>=N-filterSize;i--)
		{
			pBlob = m_blobSeq.GetBlob(i);
			meanBlob.x+=pBlob->x;
			meanBlob.y+=pBlob->y;
			meanBlob.w+=pBlob->w;
			meanBlob.h+=pBlob->h;
		}
		meanBlob.x/=filterSize;
		meanBlob.y/=filterSize;
		meanBlob.w/=filterSize;
		meanBlob.h/=filterSize;
		pBlob = m_blobSeq.GetBlob(i+1);
		pBlob->x=meanBlob.x;
		pBlob->y=meanBlob.y;
		pBlob->w=meanBlob.w;
		pBlob->h=meanBlob.h;
	}
}


void X7sBlobTrajectory::Hide(bool enable)
{
	if(enable) {
		m_frameHidden=m_frameLast;
	}
	else {
		m_frameHidden=-1;
	}
	m_isExternalHidden=enable;

}

/**
* @brief Release the memory of CvBlobSeq which correspond to the trajectory
*/
void X7sBlobTrajectory::Release()
{
	x7sReleaseByteArray(&(m_pData));
	for(int i=m_blobSeq.GetBlobNum()-1;i>=0;--i)
	{
		m_blobSeq.DelBlob(i);
	}

	this->m_frameBegin=-1;
	this->m_frameLast=-1;

	X7S_PRINT_DEBUG("X7sBlobTrajectory::Release()","ID=%d",this->ID);
}

void X7sBlobTrajectory::Draw(IplImage *pImg,int capacity, int hue_color,int is_legend, int thickness)
{
	X7S_FUNCNAME("X7sBlobTrajectory::Draw()");

	if(this->isHidden()) return;

	char str[50];
	CvFont font;
	cvInitFont(&font,CV_FONT_HERSHEY_PLAIN, 1,1, 0,1,5);

	X7S_CHECK_ERROR(funcName,pImg,,"pImg==NULL");

	int length=X7S_MIN(m_blobSeq.GetBlobNum(),capacity);	//Get the true length.
	if(length<0) length = X7S_VS_BLOBTRAJ_DRAW_LENGTH;

	if(hue_color<0) hue_color=this->ID;
	CvScalar color=cvScalarAll(255);

	if(pImg->nChannels>=3) 	color=X7S_BLOBCOLOR(hue_color);
	else if(pImg->nChannels==1) color=cvScalarAll(hue_color);
	color.val[3]=255;

	int start=m_blobSeq.GetBlobNum()-1;
	CvBlob *pB_1 = m_blobSeq.GetBlob(start);
	CvBlob *pB_2;

	//Draw from the middle.
	//cvLine(pImg,IX7S_BLOB_TRAJ_POINT(pB_1),cvPointFrom32f(CV_BLOB_CENTER(pB_1)),color);
	X7S_CHECK_WARN(funcName,pB_1,,"pB_1 is null");

	for(int i=1;i<length;i++)
	{
		pB_2= m_blobSeq.GetBlob(start-i);
		X7S_CHECK_WARN(funcName,pB_2,,"pB_2 is null (i=%d)",i);	
		cvLine(pImg,IX7S_BLOB_TRAJ_POINT(pB_1),IX7S_BLOB_TRAJ_POINT(pB_2),color);
		pB_1=pB_2;
	}

	CvBlob *pB=m_blobSeq.GetBlob(start);
	CvPoint TL = cvPoint((int)(pB->x-CV_BLOB_RX(pB)),(int)(pB->y-CV_BLOB_RY(pB)));
	CvPoint BR = cvPoint((int)(pB->x+CV_BLOB_RX(pB)),(int)(pB->y+CV_BLOB_RY(pB)));

	if(is_legend) {
		snprintf(str,sizeof(str),"%d",pB->ID);
		cvPutText(pImg, str, cvPoint(TL.x,TL.y-1), &font,color);
	}
	cvDrawRect(pImg,TL,BR,color,thickness);
}

/**
* @brief Setup if the frame need to be deleted or not.
*
* A Trajectory which is not update during at least @ref X7S_VS_BLOBTRAJ_MEMORY frames
* should be deleted.
*
* @param frameID The actual frame ID.
* @see X7sBlobTrajGenerator::Process()
*
*/
bool X7sBlobTrajectory::IsToDelete(int frameID)
{
	bool ret=false;
	if(m_frameLast < frameID)
	{
		if(!this->isHidden() && ((m_frameLast+X7S_VS_BLOBTRAJ_DRAW_STAY) < frameID))
			m_frameHidden=frameID;

		ret=((m_frameLast+X7S_VS_BLOBTRAJ_MEMORY) < frameID);
		if(ret) {
			X7S_PRINT_DEBUG("X7sBlobTrajectory::IsToDelete()","true ID=%d frame=%d",this->ID,frameID);
		}
	}
	return ret;

}

/**
* @brief Obtain the data for a full trajectory.
*
*  The X7sByteArray is generated internally (at each step), and must not be delete from outside.
* @return
*/
const X7sByteArray* X7sBlobTrajectory::GetData(int maxNbPoints, int maxLength)
{
	X7S_FUNCNAME("X7sBlobTrajectory::GetData()");


	//Declaration
	int expected_length=0;
	float step=1;
	int32_t n=0, last_blob, tmp, offset;

	//Check maxNbPoints and maxLength
	if(maxNbPoints>maxLength)
	{
		X7S_PRINT_WARN(funcName,"maxNbPoints %d > maxLength %d (Now they are equal)");
		maxNbPoints=maxLength;
	}

	//First we compute the number of frame for this trajectory.
	last_blob=this->GetBlobNum()-1;
	n=X7S_MIN(this->GetBlobNum(),maxLength);
	offset=X7S_MAX(0,(this->GetBlobNum()-maxLength));

	if(n>maxNbPoints)
	{
		//Find the step
		step=(float)n/(float)(maxNbPoints-1);
		n=maxNbPoints;
	}
	expected_length=n*sizeof(CvBlob)+3*sizeof(int32_t);

	//We create a buffer with the correct size
	if(m_pData && m_pData->capacity<expected_length) x7sReleaseByteArray(&m_pData);
	if(m_pData==NULL) m_pData = x7sCreateByteArray((int)(expected_length*1.5));

	uint8_t *p=m_pData->data;
	uint8_t *end=m_pData->data+m_pData->capacity;


	//Writing the number of blobs in this list,
	p=x7sMemWrite(p,end,&n,sizeof(n));

	//Writing start frame and end frame.
	tmp = m_frameBegin;
	p=x7sMemWrite(p,end,&tmp,sizeof(tmp));
	tmp = m_frameLast;
	p=x7sMemWrite(p,end,&tmp,sizeof(tmp));

	//Looping over blob to write.
	int i;
	for(int j=0;j<n;j++)
	{
		i=cvRound(step*(double)j)+offset;
		if(i>last_blob) { i=last_blob; j=n+1; }
		p=x7sMemWrite(p,end,this->GetBlob(i),sizeof(CvBlob));
		if(p==NULL) break;
	}

	//And check the size of the data written
	m_pData->length=(p-m_pData->data);
	if(m_pData->length != expected_length)
	{
		X7S_PRINT_WARN("X7sBlobTrajGenerator::GetData()",
				"Expected length (%d) different from written length (%d)",
				expected_length,m_pData->length);
	}
	return m_pData;
}

/**
* @brief Tell if this trajectory is similar to the other trajectory.
*
* @param other The other X7sBlobTrajectory.
* @param history check how many times we need to go back in history to check similarities.
* @param olderOnly if @true, this trajectory must be older than pBlobOther trajectory
* @return
*/
bool X7sBlobTrajectory::isSimilar(const X7sBlobTrajectory* other, int history, bool olderOnly) const	{

	X7sBlobTrajectory* pTrajThis = (X7sBlobTrajectory*)this;
	X7sBlobTrajectory* pTrajOther = (X7sBlobTrajectory*)other;

	X7S_FUNCNAME("X7sBlobTrajectory::isSimilar()");
	X7S_CHECK_WARN(funcName,pTrajOther,false,"pBlobOther is NULL");
	X7S_CHECK_WARN(funcName,history>0,false,"history must be at least 1");




	//Discard when the first frame of blob others is not the oldest one.
	if(olderOnly && (pTrajOther->GetFirstFrame() < pTrajThis->GetFirstFrame())) return false;
	if(pTrajThis->GetBlobNum() < history || pTrajOther->GetBlobNum()<history) return false;

	const CvBlob *pBlobA, *pBlobB;

	int thrs=2;
	int sum=0;
	int sum_thrs=0;
	for(int i=1;i<=history;i++)
	{
		pBlobA = pTrajThis->GetBlob(-i);
		pBlobB = pTrajOther->GetBlob(-i);
		sum+=cvRound(IX7S_BLOB_CDISTANCE(pBlobA,pBlobB));
		sum_thrs+=thrs;
		if(sum>sum_thrs) return false;
	}

	return true;
}


/**
* @brief Print the trajectory in a string.
*/
std::string X7sBlobTrajectory::toString()
{
	std::stringstream sstr;
	sstr << "ID="<< this->ID << ", length="<< m_blobSeq.GetBlobNum() << " ";
	sstr << "start=" << m_frameBegin << ",end=" << m_frameLast << " : ";
	sstr.precision(1);
	for(int i=0;i<m_blobSeq.GetBlobNum();i++)
	{
		const CvBlob *pBlob=this->GetBlob(i);
		sstr << fixed << pBlob->x << "," << pBlob->y << " ; ";
	}
	return sstr.str();
}


//============================================================================================//


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

/**
*
* @brief Constructor of the X7sBlobTrajectory List.
*/
X7sBlobTrajGenerator::X7sBlobTrajGenerator(X7sXmlObject *parent)
:X7sXmlObject(parent,X7S_VS_TAGNAME_TRAJGEN)
{
	m_SetByData=false;
	m_Frame = 0;
	m_draw=true;
	m_pData = x7sCreateByteArray(X7S_VS_BLOBTRAJ_DATA_SIZE);

	int drawLength=X7S_VS_BLOBTRAJ_DRAW_LENGTH;

	X7sParam *prm;
	prm = m_params.Insert(X7sParam("draw",X7S_XML_ANYID,m_draw,X7STYPETAG_BOOL));
	prm->setComment("Draw the blob and its trajectory");
	prm->LinkParamValue(&m_draw);

	prm = m_params.Insert(X7sParam("data_nPoints",X7S_XML_ANYID,(int)X7S_VS_BLOBTRAJ_DATA_NPTS,X7STYPETAG_INT,10,100));
	prm->setComment("Number of points that define a trajectory sended");

	prm = m_params.Insert(X7sParam("data_lastPoint",X7S_XML_ANYID,(int)X7S_VS_BLOBTRAJ_DRAW_LENGTH,X7STYPETAG_INT,20,500));
	prm->setComment("Number of points that define a trajectory");

	m_params.Insert(X7sParam("draw_stay",X7S_XML_ANYID,(int)X7S_VS_BLOBTRAJ_DRAW_STAY,X7STYPETAG_INT,0,500));
	prm->setComment("Number of frame that a trajectory stay before being deleted");

	m_params.Insert(X7sParam("draw_length",X7S_XML_ANYID,drawLength,X7STYPETAG_INT,0,500));
	prm->setComment("Number of point to draw of a trajectory");

	m_params.Insert(X7sParam("draw_legend",X7S_XML_ANYID,true,X7STYPETAG_BOOL));
	prm->setComment("If we draw the legend (Number and bounding box of a blob)");
};

/**
*	@brief Default destructor.
*
*	Remove all the trajectory
*	@see DeleteAll()
*/
X7sBlobTrajGenerator::~X7sBlobTrajGenerator()
{
	X7S_PRINT_DEBUG("X7sBlobTrajGenerator::~X7sBlobTrajGenerator()");

	x7sReleaseByteArray(&(m_pData));	//Because it may be opened
	this->DeleteAll();
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------


/**
* @brief Add a Blob to the list of trajectory.
*
* If the blob is new, then a X7sBlobTrajectory is created,
* otherwise this function find the corresponding trajectory,
* and add the last position to the blob.
*
* @note This function copy internally the blob therefore,
* the blob given can be destroy after executing this function.
*
* @param pBlob A pointer on a blob (can be temporary).
*/
void	X7sBlobTrajGenerator::AddBlob(CvBlob* pBlob)
{

	//Look if the blob we want to add already exist in the trajectoryList
	X7sBlobTrajectory* pTraj = this->GetBlobTrajByID(CV_BLOB_ID(pBlob));

	if(pTraj==NULL) // Add new blob trajectory
	{
		m_pTrajVec.push_back(new X7sBlobTrajectory(pBlob[0]));
		pTraj = this->GetBlobTrajByID(CV_BLOB_ID(pBlob)); //Obtain the trajectory with the ID.
	}

	assert(pTraj);
	pTraj->AddBlob(pBlob,m_Frame);
}

/**
* @brief At each frame check if a blob disappear and therefore a trajectory need
* to be saved and/or deleted.
*/
void X7sBlobTrajGenerator::Process(IplImage* /*pImg*/, IplImage* /*pFG*/)
{
	//Loop over each track.
	for(int i=0; i<this->GetBlobTrajNum(); ++i)
	{
		X7sBlobTrajectory* pTraj = this->GetBlobTraj(i);

		//Check if the trajectory need to be deleted.
		if(pTraj->IsToDelete(m_Frame))
		{
			this->DelBlobTraj(i); //Release the Blob trajectory
		}
	}

	this->HideSimilarTraj();

	m_Frame++;
}

/**
* @brief Draw the list of blob.
* @param pImg
* @param length Length of the blob trajectory (If negative, we use the parameter @tt{draw_length})
* @param hue_color The color of the blob (If hue_color<0, then we use the ID of the blob to determine the hue color)
* @param is_legend If we draw the ID corresponding to the blob (If negative, we use the parameter @tt{draw_legend})
* @param thickness Size of the trajectory
*/
void X7sBlobTrajGenerator::Draw(IplImage *pImg,int length, int hue_color,int is_legend, int thickness)
{
	if(pImg && m_draw)
	{
		if(length<0) length=m_params["draw_length"]->toIntValue();
		if(is_legend<0) is_legend= m_params["draw_legend"]->toIntValue();
		for(int i=0; i<this->GetBlobTrajNum(); ++i)
		{
			X7sBlobTrajectory* pTraj = this->GetBlobTraj(i);
			pTraj->Draw(pImg,length,hue_color,is_legend,thickness);
		}
	}
}

/**
* @brief Return the trajectory of a blob given its ID.
* @param BlobID The Index of the blob.
* @return If the blob doesn't exist (anymore), it return NULL.
*/
X7sBlobTrajectory* X7sBlobTrajGenerator::GetBlobTrajByID(int BlobID)
{
	X7sBlobTrajectory *pTraj=NULL;
	for(int i=0; i<this->GetBlobTrajNum(); ++i)
	{
		pTraj=this->GetBlobTraj(i);
		if(BlobID == CV_BLOB_ID(pTraj)) return pTraj;
	}
	return NULL;
}

/**
* @brief Return the trajectory of blob.
* @param index is the position in the list (@ref GetBlobTrajNum())
* @return If the blob doesn't exist, it return NULL.
*/
X7sBlobTrajectory* X7sBlobTrajGenerator::GetBlobTraj(int index)
{
	return m_pTrajVec[index];
}

/**
* @brief Delete a Blob Trajectory given its index in the list.
* @param index position of the blob trajectory in the list.
* @return @true if the blob has beeen deleetd, @false otherwise.
*/
bool X7sBlobTrajGenerator::DelBlobTraj(int index)
{
	if(0 <= index && index < (int)m_pTrajVec.size() )
	{
		//Delete the pointer
		X7sBlobTrajectory *pTraj=GetBlobTraj(index);
		X7S_DELETE_PTR(pTraj);

		//Remove it from the vector
		m_pTrajVec.erase(m_pTrajVec.begin()+index);
		return true;
	}

	X7S_PRINT_WARN("X7sBlobTrajGenerator::DelBlob()","index %d !=[0,%d]",index,m_pTrajVec.size());
	return false;
}

/**
* @brief Delete the trajectory of blob knowing its ID.
* @param BlobID the ID of the blob trajectory.
* @see X7sBlobTrajGenerator::DelBlobTraj().
*/
bool X7sBlobTrajGenerator::DelBlobTrajByID(int BlobID)
{
	for(int i=0; i<this->GetBlobTrajNum(); ++i)
	{
		if(BlobID == CV_BLOB_ID(GetBlobTraj(i))) return DelBlobTraj(i);
	}
	return false;
}

/**
* @brief Return the last position of each blob in a array of byte.
* @param full_data When @true the data correspond to full trajectory, otherwise it's only incremental.
* @return
*/
const X7sByteArray* X7sBlobTrajGenerator::GetData(bool full_data)
{
	X7S_FUNCNAME("X7sBlobTrajGenerator::GetData()");


	int i;
	uint8_t *p = m_pData->data;
	uint8_t *end = m_pData->data+m_pData->capacity;
	m_pData->length=0;
	int nof_blobs=0;
	size_t expected_length=0;
	const char name[6]={'X','7','S','B','T','G'};
	int data_lastPoint=m_params["data_lastPoint"]->toIntValue();
	int data_nPoints=m_params["data_nPoints"]->toIntValue();

	std::vector<const X7sByteArray*> vecTrajData;

	//First write the name
	p=x7sMemWrite(p,end,name,sizeof(name));

	//Write if the data are full or not
	uint8_t fulldata=full_data;
	p=x7sMemWrite(p,end,&fulldata,sizeof(fulldata));

	//Compute the size of the data to write.
	if(full_data)
	{
		expected_length=IX7S_VS_DATAHDR_SIZE;
		for(i=0;i<this->GetBlobTrajNum();++i)
		{
			X7sBlobTrajectory* pTraj = this->GetBlobTraj(i);
			if(pTraj==NULL || pTraj->isHidden()) continue;
			const X7sByteArray *trajData = pTraj->GetData(data_nPoints,data_lastPoint);
			size_t next_length = expected_length+trajData->length+4;//Size to write the length;

			if(next_length<=(size_t)m_pData->capacity)
			{
				vecTrajData.push_back(trajData);
				expected_length=next_length;
			}
			else
			{
				X7S_PRINT_ERROR(funcName,"Reach maximum capacity %d (<%d)",expected_length,m_pData->capacity);
				break;
			}
		}
		nof_blobs=vecTrajData.size();
	}
	else
	{
		expected_length=IX7S_VS_DATAHDR_SIZE;
		for(i=0;i<this->GetBlobTrajNum();++i)
		{
			if(this->IsActual(this->GetBlobTraj(i)->ID))
			{
				size_t next_length = expected_length+sizeof(CvBlob);
				if(next_length<=(size_t)m_pData->capacity)
				{
					nof_blobs++;
					expected_length=next_length;
				}
				else
				{
					X7S_PRINT_ERROR(funcName,"Reach maximum capacity %d (<%d)",expected_length,m_pData->capacity);
					break;
				}
			}
		}
	}

	//Make the last warning check
	if(expected_length > (size_t)m_pData->capacity)
	{
		X7S_PRINT_ERROR(funcName,
				"Size of data=%d is bigger than memory buffer %d return NULL",
				expected_length,m_pData->capacity);
		return NULL;
	}


	//Then write the number of blob
	p=x7sMemWrite(p,end,&nof_blobs,sizeof(nof_blobs));

	if(fulldata)
	{
		for(i=0;i<nof_blobs;i++)
		{
			const X7sByteArray* bArray = vecTrajData[i];
			if(bArray==NULL) continue;

			//Write the size of the data.
			int32_t tmp=bArray->length;
			p=x7sMemWrite(p,end,&tmp,sizeof(int32_t));

			//And write the data obtain previously
			p=x7sMemWrite(p,end,bArray->data,bArray->length);
			if(p==NULL) break;
		}
	}
	else
	{
		//Then loop over each trajectory
		int blob_written=0;
		for(i=this->GetBlobTrajNum(); i>0; --i)
		{
			//Obtain the trajectory
			X7sBlobTrajectory* pTraj = this->GetBlobTraj(i-1);
			if(pTraj && (IsActual(pTraj->ID)) && (blob_written<nof_blobs)) //Otherwise check if it's an actual blob.
			{
				//And write the lastPosition
				p=x7sMemWrite(p,end,pTraj->GetLastBlob(),sizeof(CvBlob));
				if(p==NULL) break;
				blob_written++;
			}
		}
	}

	if(p==NULL)
	{
		X7S_PRINT_ERROR("X7sBlobTrajGenerator::GetData()","failed to copy for blob %d",i);
		return NULL;
	}

	m_pData->length=(p-m_pData->data);

	if((size_t)m_pData->length != expected_length)
	{
		X7S_PRINT_WARN("X7sBlobTrajGenerator::GetData()",
				"Expected length (%d) different from written length (%d)",
				expected_length,m_pData->length);
	}

	return m_pData;
}


/**
* @brief Set the given trajectory with an Array of byte.
*/
bool X7sBlobTrajGenerator::SetData(const X7sByteArray *pData, int frameID)
{
	int i;
	uint8_t fulldata=false;
	uint8_t *p = pData->data;
	uint8_t *end = pData->data+pData->length;
	int nof_blobs=0;
	const char name[6]={'X','7','S','B','T','G'};
	char buff[6];

	//Set the current frame
	m_Frame =frameID;

	//First look if it the correct type of data
	p=x7sMemRead(p,end,buff,sizeof(name));
	if(memcmp(name, buff,sizeof(name))!=0)
	{
		X7S_PRINT_WARN("X7sBlobTrajGenerator::SetData()",
				"The data given does not correspond to the required type");
		return false;
	}

	//Then read if we have the full data
	p=x7sMemRead(p,end,&fulldata,sizeof(fulldata));

	//Then read the number of blob
	p=x7sMemRead(p,end,&nof_blobs,sizeof(nof_blobs));

	if(fulldata)	//If we have a complete description of the data
	{
		X7S_PRINT_DEBUG("X7sBlobTrajectory::SetData()","Reset %d blobs trajectory",nof_blobs);

		this->DeleteAll();	//Remove all the trajectory

		//And loop over each trajectory
		for(i=nof_blobs; i>0; --i)
		{

			//Obtain the size of each sub X7sByteArray*
			int32_t tmp=0;
			p=x7sMemRead(p,end,&tmp,sizeof(tmp));

			//Assign the subarray
			X7sByteArray bArray;
			bArray.data=p;
			bArray.length=tmp;
			bArray.capacity=tmp;

			m_pTrajVec.push_back(new X7sBlobTrajectory(&bArray));

			p+=bArray.length;
		}
	}
	else	//Otherwise obtain the trajectory by adding blob position each frame.
	{


		//And loop over each blob.
		CvBlob blob;
		for(i=nof_blobs; i>0; --i)
		{
			//Read the data in a blob
			p=x7sMemRead(p,end,&blob,sizeof(CvBlob));
			if(p==NULL) break;

			//And add the blob to the list of trajectory.
			this->AddBlob(&blob);
		}
		if(p==NULL)
		{
			X7S_PRINT_WARN("X7sBlobTrajGenerator::SetData()","failed to copy to read blob %d",i);
			return false;
		}
	}

	//Once all blob has been added process the trajectory generator.
	this->Process();

	X7S_PRINT_DUMP("X7sBlobTrajectory::SetData()","%s",this->toString(true).c_str());

	return true;
}

std::string X7sBlobTrajGenerator::toString(bool full)
{
	stringstream sstr;
	sstr << "n_trajectory=" << this->GetBlobTrajNum() <<": ";
	for(int i=0;i<this->GetBlobTrajNum();++i)
	{
		X7sBlobTrajectory* pTraj = this->GetBlobTraj(i);
		if(pTraj==NULL) continue;
		if(full) sstr << endl << pTraj->toString();
		else sstr <<  pTraj->ID << " (" << pTraj->GetBlobNum() << "); ";
	}
	return sstr.str();
}



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------


bool X7sBlobTrajGenerator::IsActual(int BlobID)
{
	int frameID=0;
	X7sBlobTrajectory* pTraj = this->GetBlobTrajByID(BlobID);
	if(pTraj && !pTraj->isHidden()) frameID = pTraj->GetLastFrame()+1;
	return (frameID==m_Frame);
}

/**
* @brief Remove all the existent trajectory.
*/
void X7sBlobTrajGenerator::DeleteAll()
{
	for(int i=0;i<this->GetBlobTrajNum();++i)
	{
		X7sBlobTrajectory* pTraj = this->GetBlobTraj(i);
		X7S_DELETE_PTR(pTraj);	//Delete all the pointers
	}
	m_pTrajVec.clear();	//Because there are object in the BlobTrajectoryGenerator.
}

/**
* @brief Search similar Blob and hide them.
* @return
*/
bool X7sBlobTrajGenerator::HideSimilarTraj()
{
	X7sBlobTrajectory *pTrajA, *pTrajB;
	bool ret=false;

	for(int i=0;i<this->GetBlobTrajNum();i++)
	{
		pTrajA= this->GetBlobTraj(i);
		if(pTrajA==NULL) continue;
		if(pTrajA->isHidden()) continue;

		for(int j=i+1;j<this->GetBlobTrajNum();j++)
		{
			pTrajB= this->GetBlobTraj(j);
			if(pTrajB==NULL || pTrajA==pTrajB) continue;
			if(pTrajB->isHidden()) continue;

			if(pTrajA->isSimilar(pTrajB,5,false)) {

				if(pTrajB->GetFirstFrame()<pTrajA->GetFirstFrame()) pTrajA->Hide();
				else 	pTrajB->Hide();
				ret=true;
			}
		}

	}
	return ret;
}





//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Internal Functions
//----------------------------------------------------------------------------------------
