#include "ix7svidsurv.h"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sVSLog.hpp"
#include <cstdlib>  // per IEEE Std 1003.1, 2004
#include <cstdio>
#include <map>
#ifndef WINDOWS
#include <unistd.h> // for "legacy" systems
#endif


static std::map<FILE*, std::string> tmpFile;

void X7sXmlParamGetVector(CvPoint *val, const X7sParam* pParam)
{
	CvPoint2D64f tmp;
	if(val && pParam)
	{
		//Obtain the point from the parameters.
		if(pParam->GetRealValue(0,&tmp.x)) val->x=(int)tmp.x;
		if(pParam->GetRealValue(1,&tmp.y)) val->y=(int)tmp.y;
	}
}

void X7sXmlParamGetVector(CvPoint2D32f *val, const X7sParam* pParam)
{
	CvPoint2D64f tmp;
	if(val && pParam)
	{
		//Obtain the point from the parameters.
		if(pParam->GetRealValue(0,&tmp.x)) val->x=(float)tmp.x;
		if(pParam->GetRealValue(1,&tmp.y)) val->y=(float)tmp.y;
	}
}


void X7sXmlParamGetVector(CvScalar *val,const X7sParam* pParam)
{
	CvScalar tmp;
	if(val && pParam)
	{
		//Obtain the point from the parameters.
		if(pParam->GetRealValue(0,&tmp.val[0])) val->val[0]=(float)tmp.val[0];
		if(pParam->GetRealValue(1,&tmp.val[1])) val->val[1]=(float)tmp.val[1];
		if(pParam->GetRealValue(2,&tmp.val[2])) val->val[2]=(float)tmp.val[2];
		if(pParam->GetRealValue(3,&tmp.val[3])) val->val[3]=(float)tmp.val[3];
	}
}


FILE* x7sMakeTmp()
										{
	FILE *file=NULL;
	X7S_FUNCNAME("x7sMakeTmp()");


#ifdef WINDOWS
	char sfn[255] = "";
	bool toremove=false;

	//First try the simple method
	file=tmpfile();

	//Otherwise, try tmp directory
	if(file==NULL)
	{
		snprintf(sfn,sizeof(sfn),"%s%s.x7s.tmp",getenv("TMP"),tmpnam(NULL));
		file = fopen(sfn,"wb+");
		toremove=true;
	}

	//Then try the application directory
	if(file==NULL)
	{
		snprintf(sfn,sizeof(sfn),"%s%s.x7s.tmp",getenv("APPDIR"),tmpnam(NULL));
		file = fopen(sfn,"wb+");
		toremove=true;
	}

	//If this still not work send an error.
	if(file==NULL)
	{
		X7S_PRINT_ERROR(funcName,"%s: %s\n",sfn, strerror(errno));
		perror( "Could not open new temporary file\n" );
	}
	else
	{
		if(toremove) {
			tmpFile[file] = std::string(sfn);
			X7S_PRINT_DEBUG(funcName,"%s must be deleted using x7sCloseTmp()",sfn);
		}
	}

#else
	char sfn[21] = "";
	int fd = -1;
	strncpy(sfn, "/tmp/x7stmp.XXXXXX", sizeof(sfn));
	sfn[20]='\0';

	if ((fd = mkstemp(sfn)) == -1 ||
			(file = fdopen(fd, "w+")) == NULL) {
		if (fd != -1) {
			unlink(sfn);
			close(fd);
		}
		X7S_PRINT_ERROR("x7sMakeTmp()","%s: %s\n", sfn, strerror(errno));
		return (NULL);
	}
	if(file==NULL)
	{
		X7S_PRINT_WARN("x7sMakeTmp()","file is NULL mkstemp()");
	}
#endif

	return file;
}


void x7sCloseTmp(FILE **pFile)
{
	FILE *file;
	if(pFile) file=*pFile;
	std::string filePath;



#ifdef WINDOWS

	//Delete the file if this one has been created
	if(tmpFile.find(file)!= tmpFile.end())
	{
		X7S_PRINT_DEBUG("x7sCloseTmp()","%s is deleted",tmpFile[file].c_str());
		filePath = tmpFile[file].c_str();
	}
#endif

	fclose(file);
	if(!filePath.empty()) remove(filePath.c_str());
	*pFile=NULL;

}



