#include "X7sVSPipelineFeatures.hpp"

#ifdef _VLD
#include <vld.h>
#endif

//========================================================================
//-- Includes done in cpp file to accelerate compilation.
#include "X7sVSLog.hpp"
#include <vector>
#include <highgui.h>
#include <math.h>



/* Special extended blob structure for auto blob tracking: */
typedef struct CvBlobTrackAuto
{
	CvBlob  blob;
	int     BadFrames;
} CvBlobTrackAuto;



//========================================================================
//-- Constructor and destructor.

/**
* @brief Our Pipeline with a mix between OpenCV module and our module to
* perform video surveillance and trajectory analysis.
* @param parent The parent X7sXmlObject.
*/
X7sVidSurvPipelineFeatures::X7sVidSurvPipelineFeatures(X7sXmlObject *parent)
:X7sVidSurvPipeline(parent,X7S_VS_PIPELINE_FEATURES,X7S_XML_ANYID),
 m_BlobList(sizeof(CvBlobTrackerAuto))
 {
	X7S_PRINT_DEBUG("X7sVidSurvPipelineFeatures::X7sVidSurvPipelineFeatures()");

	m_FrameCount=0;
	m_pImPrev=NULL;
	m_pFGMask=NULL;
	m_pConComList = x7sCreateConCompoList(255);
	m_pFeaturesList = x7sCreateFeaturesList();
	m_FGTrainFrames=X7S_VS_NUMFRAMES_TRAIN;

	m_drawBlob=true;
	m_drawAlarm=true;

	X7sParam *prm;

	//===========================
	// Setup the parameters

	prm = m_params.Insert(X7sParam("nFGTrainFrames",X7S_XML_ANYID,m_FGTrainFrames));
	prm->setComment("Number of frames to trains before obtaining a good model");
	prm->LinkParamValue(&m_FGTrainFrames);

	prm = m_params.Insert(X7sParam("segmentFGMask",X7S_XML_ANYID,true,X7STYPETAG_BOOL));
	prm->LinkParamValue(&m_segmentFGMask);
	prm->setComment("If we can segment the FG mask or not");

	prm = m_params.Insert(X7sParam("minArea",X7S_XML_ANYID,m_pFeaturesList->min_area,X7STYPETAG_INT));
	prm->LinkParamValue(&(m_pFeaturesList->min_area));
	prm->setComment("Min area (number of white pixels) that a blob can have");

	prm = m_params.Insert(X7sParam("maxArea",X7S_XML_ANYID,m_pFeaturesList->max_area_person,X7STYPETAG_INT,0));
	prm->LinkParamValue(&(m_pFeaturesList->max_area_person));
	prm->setComment("Maximum area (number of white pixels) that a blob can have");

	prm = m_params.Insert(X7sParam("minDimXY",X7S_XML_ANYID,m_pFeaturesList->min_dim_xy,X7STYPETAG_INT,0));
	prm->LinkParamValue(&(m_pFeaturesList->min_dim_xy));
	prm->setComment("Minimum dimension of a blob (people) in x or y");

	prm = m_params.Insert(X7sParam("maxDimXY",X7S_XML_ANYID,m_pFeaturesList->max_dim_xy,X7STYPETAG_INT,0));
	prm->LinkParamValue(&(m_pFeaturesList->max_dim_xy));
	prm->setComment("Maximum dimension of a blob (people) in x or y");



	m_pFGD = x7sCreateFGDetector(this);	//!Create the default FG detector.

	//kernelDIFF
	kernelMor5x5 = cvCreateStructuringElementEx(5, 5, 0, 0, CV_SHAPE_ELLIPSE, NULL);
	kernelMor3x3 = cvCreateStructuringElementEx(3, 3, 0, 0, CV_SHAPE_ELLIPSE, NULL);

#ifdef _VLD
	X7S_PRINT_DEBUG("","VLD");
#endif


 }

/**
* @brief Delete all the thing we have created in the destructor.
*/
X7sVidSurvPipelineFeatures::~X7sVidSurvPipelineFeatures()
{
	X7S_PRINT_DEBUG("X7sVidSurvPipelineFeatures::~X7sVidSurvPipelineFeatures()");

	// Delete submodules (X7S).
	X7S_RELEASE_PTR(m_pFGD);
	X7S_DELETE_PTR(m_pAlarmMan);

	//Delete images
	if(m_pImPrev) cvReleaseImage(&m_pImPrev);

	if(m_pFGMask) cvReleaseImage(&m_pFGMask);

	if(m_pFeaturesList) x7sReleaseFeaturesList(&m_pFeaturesList);
}

//========================================================================
//-- Public Methods.

void X7sVidSurvPipelineFeatures::Process(IplImage* pImg, IplImage* pFG)
{
	X7S_FUNCNAME("X7sVidSurvPipelineFeatures::Process()");


	if(m_params["enable"]->toIntValue())
	{

		/* Bump frame counter: */
		m_FrameCount++;
		if(m_FrameCount%10==0)
			X7S_PRINT_DUMP("X7sVidSurvPipelineFeatures::Process()","#%04d nBlobs=%d",	m_FrameCount,m_BlobList.GetBlobNum());

		//-------------------------------------------------------------------------------------
		//Obtain the foreground

		X7S_CHECK_ERROR(funcName,pImg,,"pImg is NULL");

		//Compute the difference image
		if(m_pFGD)
		{

			//DIFF
			cvCvtColor(pImg, m_pFeaturesList->grey, CV_BGR2GRAY);

			cvAbsDiff(m_pFeaturesList->grey, m_pFeaturesList->prev_grey, m_pFeaturesList->pImg_diff);
			cvThreshold(m_pFeaturesList->pImg_diff, m_pFeaturesList->pImg_diff2, 30, 255, CV_THRESH_BINARY);
			cvDilate(m_pFeaturesList->pImg_diff2, m_pFeaturesList->pImg_diff2, kernelMor5x5, 1);
			cvDilate(m_pFeaturesList->pImg_diff2, m_pFeaturesList->pImg_diff2, kernelMor5x5, 1);
			cvErode(m_pFeaturesList->pImg_diff2, m_pFeaturesList->pImg_diff2, kernelMor3x3, 1);
			cvSegmentFGMask(m_pFeaturesList->pImg_diff2, 1, 10);
			if(m_pFGMask) cvReleaseImage(&m_pFGMask);
			m_pFGMask = cvCloneImage(m_pFeaturesList->pImg_diff2);


		}   /* If FG detector is needed. */
		else{
			m_pFGMask = NULL; 
		}



		//-------------------------------------------------------------------------------------
		//Obtain the connected component
		X7S_CHECK_ERROR(funcName,m_pFGMask,,"m_pFGMask is NULL or could not be proceed");
		//Update the list of connected component
		IplImage *pImCC = cvCloneImage(m_pFGMask);
		x7sUpdateConCompoList(m_pConComList,pImCC,false);







		//-------------------------------------------------------------------------------------

		if(m_FrameCount>m_FGTrainFrames)
		{
			IplImage *pImgClone = cvCloneImage(pImg);
			ProcessFeatures(pImgClone, pImCC);

			if(m_dbgWnd)
			{
				cvNamedWindow("Features");
				x7sShowImage("Features", pImgClone);
			}
			cvReleaseImage(&pImgClone);

		}


		if(m_dbgWnd)
		{
			x7sDrawConCompoList(pImCC,m_pConComList);
			X7S_CV_SET_CMODEL(pImCC,X7S_CV_HNZ2BGRLEGEND);
			cvNamedWindow("CC");
			x7sShowImage("CC", pImCC);
		}

		cvReleaseImage(&pImCC);
		pImCC=NULL;


		//-------------------------------------------------------------------------------------
		//Generate a trajectory from the blobList history.
		this->ProcessBTGen(pImg,m_pFGMask);

		//And finally process the events
		this->ProcessAlarm();

		if(m_pImPrev) cvReleaseImage(&m_pImPrev);
		m_pImPrev=cvCloneImage(pImg);

	}
}


bool  X7sVidSurvPipelineFeatures::CreateChildFromXML(const TiXmlElement *child)
{

	if(child && X7S_XML_ISEQNODENAME(child,m_pFGD->GetTagName()))
	{
		if(m_pFGD) m_pFGD->Release();
		m_pFGD = x7sCreateFGDetector(this,child->ToElement());
		return  true;
	}
	return false;
}



//========================================================================
//-- Private and Protected Methods.


void X7sVidSurvPipelineFeatures::ProcessFeatures(IplImage* pImg, IplImage* pFG)
{
	X7sFeaturesHeaderBox *f, *f1, *f2;
	X7sFeaturesBox *b;
	int k;
	int index, indexf1, indexf2;
	CvPoint min, max;

	int sub_frame_min_x = 2;
	int sub_frame_max_x = pImg->width - 2;
	float center_x = pImg->width/2;

	int sub_frame_min_y = 2;
	int sub_frame_max_y = pImg->height - 2; 
	float center_y = pImg->height/2; 

	char cindex[30];
	CvPoint ps, pr;
	CvScalar s, r;
	CvFont myFont;//Declare font
	//Initialise font
	cvInitFont(&myFont, CV_FONT_HERSHEY_COMPLEX ,0.5f,0.5f,0,1,8); 

	X7S_FUNCNAME("X7sVidSurvPipelineFeatures::ProcessFeatures()");

	//IplConvKernel *kernelMor7x7, *kernelMor3x3;

	//cvCvtColor(pImg, m_pFeaturesList->grey, CV_BGR2GRAY);
	if(pImg->nChannels==4)  	cvCvtColor(pImg, m_pFeaturesList->pImg_clone, CV_BGRA2BGR );
	else cvCopy(pImg, m_pFeaturesList->pImg_clone, NULL);

	cvCvtColor( m_pFeaturesList->pImg_clone, m_pFeaturesList->yuv, CV_BGR2YCrCb );


	//DIFF IMAGES
	/*kernelMor7x7 = cvCreateStructuringElementEx(7, 7, 0, 0, CV_SHAPE_ELLIPSE, NULL);
	kernelMor3x3 = cvCreateStructuringElementEx(3, 3, 0, 0, CV_SHAPE_ELLIPSE, NULL);

	cvAbsDiff(m_pFeaturesList->grey, m_pFeaturesList->prev_grey, m_pFeaturesList->pImg_diff);
	cvThreshold(m_pFeaturesList->pImg_diff, m_pFeaturesList->pImg_diff2, 10, 255, CV_THRESH_BINARY);
	//cvErode(m_pFeaturesList->pImg_diff2, m_pFeaturesList->pImg_diff2, kernelMor3x3, 1);
	cvDilate(m_pFeaturesList->pImg_diff2, m_pFeaturesList->pImg_diff2, kernelMor7x7, 1);
	cvDilate(m_pFeaturesList->pImg_diff2, m_pFeaturesList->pImg_diff2, kernelMor7x7, 1);
	cvSegmentFGMask(m_pFeaturesList->pImg_diff2, 1, 10);
	cvNamedWindow("DIFF");
	cvShowImage("DIFF", m_pFeaturesList->pImg_diff2);*/


	/*	if(pFG){
		X7S_CV_SET_CMODEL(pFG, X7S_CV_HNZ2BGRLEGEND);
		cvNamedWindow("FG_NUM");
		x7sShowImage("FG_NUM", pFG);
	}
	 */

	X7S_PRINT_DUMP(funcName,"\n\n-----------------------------------------------\n");
	if (m_pFeaturesList->size > 0){

		cvCalcOpticalFlowPyrLK(m_pFeaturesList->prev_grey, m_pFeaturesList->grey, m_pFeaturesList->prev_pyramid, 
				m_pFeaturesList->pyramid, m_pFeaturesList->points[0], m_pFeaturesList->points[1], m_pFeaturesList->count_features,
				cvSize(m_pFeaturesList->win_size, m_pFeaturesList->win_size), 3, m_pFeaturesList->status, 0,
				cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03), m_pFeaturesList->flags_pyramid );


		m_pFeaturesList->flags_pyramid |= CV_LKFLOW_PYR_A_READY;

		// draw features
		for(int i = 0; i < m_pFeaturesList->count_features; i++){
			ps = cvPointFrom32f(m_pFeaturesList->points[0][i]);
			pr = cvPointFrom32f(m_pFeaturesList->points[1][i]);   
			m_pFeaturesList->lost_points[i] = 0; 
			if(ps.y>=0 && ps.x>=0 && ps.y<pImg->height && ps.x<pImg->width && pr.y>=0 && pr.x>=0 && pr.y<pImg->height && pr.x<pImg->width){
				s = cvGet2D(m_pFeaturesList->eig, ps.y, ps.x);
				r = cvGet2D(m_pFeaturesList->eig, pr.y, pr.x);
				cvCircle( pImg, cvPointFrom32f(m_pFeaturesList->points[0][i]), 2, CV_RGB(0,255,0), -1, 8,0);
				if(m_pFeaturesList->status[i] == 1){
					cvCircle( pImg, cvPointFrom32f(m_pFeaturesList->points[1][i]), 2, CV_RGB(255,0,0), -1, 8,0);
					m_pFeaturesList->points_difx[i] = pr.x - ps.x;
					m_pFeaturesList->points_dify[i] = pr.y - ps.y; 
					m_pFeaturesList->lost_points[i] = 1; 
				}
				else{
					m_pFeaturesList->points_difx[i] = pr.x - ps.x;
					m_pFeaturesList->points_dify[i] = pr.y - ps.y; 
					X7S_PRINT_DUMP("","UNA PERDIDA");
					cvCircle( pImg, cvPointFrom32f(m_pFeaturesList->points[1][i]), 2, CV_RGB(50,0,0), -1, 8,0);				
				}
			}
		}


		for(int j = 0; j < m_pFeaturesList->count_features; j++){
			m_pFeaturesList->valid_ID[j] = -1;
		}

		//move and draw box
		for(f = m_pFeaturesList->vec; f != 0; f = f->next_HeaderBox){
			float median_difx = 0;
			float median_dify = 0;
			float desv_difx = 0;
			float desv_dify = 0;
			float interval_desv1x = 0;
			float interval_desv2x = 0;
			float interval_desv1y = 0;
			float interval_desv2y = 0;
			int cont_box = 0;

			index = f->last_index % m_pFeaturesList->num_nbox;
			k = f->box_list[index].id;
			for(int j = 0; j < m_pFeaturesList->count_features; j++){
				if(m_pFeaturesList->lost_points[j]){
					pr = cvPointFrom32f(m_pFeaturesList->points[0][j]);
					CvScalar s; 
					int id_cc = 0;
					if(pr.y>=0 && pr.x>=0 && pr.y<pImg->height && pr.x<pImg->width){
						s = cvGet2D(pFG, (int)pr.y, (int)pr.x); 
						id_cc = (int)s.val[0];
					}
					if (id_cc >= 1 && m_pFeaturesList->points_IDbox[j] == k){
						median_difx = median_difx + m_pFeaturesList->points_difx[j];
						median_dify = median_dify + m_pFeaturesList->points_dify[j];
						cont_box++;
					}
				}
			}
			median_difx = median_difx/cont_box;
			median_dify = median_dify/cont_box;

			for(int j = 0; j < m_pFeaturesList->count_features; j++){
				if(m_pFeaturesList->lost_points[j]){
					pr = cvPointFrom32f(m_pFeaturesList->points[0][j]);
					CvScalar s; 
					int id_cc = 0;
					if(pr.y>=0 && pr.x>=0 && pr.y<pImg->height && pr.x<pImg->width){
						s = cvGet2D(pFG, (int)pr.y, (int)pr.x); 
						id_cc = (int)s.val[0];
					}
					if (id_cc >= 1 && m_pFeaturesList->points_IDbox[j] == k){
						desv_difx = desv_difx + pow((m_pFeaturesList->points_difx[j] - median_difx), 2);
						desv_dify = desv_dify + pow((m_pFeaturesList->points_dify[j] - median_dify), 2);
					}
				}
			}
			desv_difx = sqrt(desv_difx/cont_box);
			desv_dify = sqrt(desv_dify/cont_box);

			X7S_PRINT_DUMP("","NUMERO DE CARACTERISTICAS------------------> %d  index %d", f->box_list[index].num_features_box, index);

			if(f->box_list[index].num_features_box < 25){
				interval_desv1x = median_difx - 3.0*desv_difx;
				interval_desv2x = median_difx + 3.0*desv_difx;
				interval_desv1y = median_dify - 3.0*desv_dify;
				interval_desv2y = median_dify + 3.0*desv_dify;
			}
			else{
				interval_desv1x = median_difx - 1.0*desv_difx;
				interval_desv2x = median_difx + 1.0*desv_difx;
				interval_desv1y = median_dify - 1.0*desv_dify;
				interval_desv2y = median_dify + 1.0*desv_dify;			
			}


			f->box_list[index].features_mean_diffx = median_difx; 
			f->box_list[index].features_mean_diffy = median_dify; 
			f->box_list[index].features_desv_diffx = desv_difx; 
			f->box_list[index].features_desv_diffy = desv_dify; 

			X7S_PRINT_DUMP("","ID = %d,  desv_difx  %f, desv_dify  %f", k, desv_difx, desv_dify);

			f->last_index++;
			index = f->last_index % m_pFeaturesList->num_nbox;
			
			if(f->last_index >= m_pFeaturesList->num_nbox && f->box_list[index].hist_ini)
				cvReleaseHist(&f->box_list[index].hist_ini);

			f->box_list[index].hist_ini = cvCreateHist(3, m_pFeaturesList->hist_size, CV_HIST_ARRAY, m_pFeaturesList->ranges, 1);

			int index_prev = (f->last_index-1) % m_pFeaturesList->num_nbox;
			f->box_list[index].flag_hist = f->box_list[index_prev].flag_hist;
			f->box_list[index].id = f->box_list[index_prev].id; 

			if(f->box_list[index].flag_hist == 1){
				cvCopyHist(f->box_list[index_prev].hist_ini, &f->box_list[index].hist_ini);			
			}


			f->box_list[index].num_features_box = 0;

			int fea_per_box = 0;
			for(int j = 0; j < m_pFeaturesList->count_features; j++){
				if(m_pFeaturesList->lost_points[j]){
					pr = cvPointFrom32f(m_pFeaturesList->points[1][j]);
					CvScalar s; 
					int id_cc = 0;
					if(pr.y>=0 && pr.x>=0 && pr.y<pImg->height && pr.x<pImg->width){
						s = cvGet2D(pFG, (int)pr.y, (int)pr.x); 
						id_cc = (int)s.val[0];
					}
					if (id_cc >= 1 && m_pFeaturesList->points_IDbox[j] == k && 
						m_pFeaturesList->points_difx[j] >= interval_desv1x && m_pFeaturesList->points_difx[j] <= interval_desv2x &&
						m_pFeaturesList->points_dify[j] >= interval_desv1y && m_pFeaturesList->points_dify[j] <= interval_desv2y
					){
						if(k == 2){
							cvCircle( pImg, cvPointFrom32f(m_pFeaturesList->points[1][j]), 5, CV_RGB(0,0,0), -1, 8,0);
						}

						m_pFeaturesList->valid_ID[j] = k;
						pr = cvPointFrom32f(m_pFeaturesList->points[1][j]);

						if(f->box_list[index].num_features_box == 0){
							f->box_list[index].min_x = pr.x;
							f->box_list[index].min_y = pr.y;
							f->box_list[index].max_x = pr.x;
							f->box_list[index].max_y = pr.y;
						}
						else{
							f->box_list[index].max_x = MAX(f->box_list[index].max_x, pr.x);	
							f->box_list[index].min_x = MIN(f->box_list[index].min_x, pr.x);
							f->box_list[index].max_y = MAX(f->box_list[index].max_y, pr.y);	
							f->box_list[index].min_y = MIN(f->box_list[index].min_y, pr.y);
						}
						f->box_list[index].num_features_box++;
					}
					if(id_cc >= 1 && m_pFeaturesList->points_IDbox[j] == k){
						fea_per_box++;	
					}
				}
			}

			if (!f->ok && f->last_index >= m_pFeaturesList->max_count_history){
				int index_mov;
				int flag_mov = 1;

				for(int index_history = 0; index_history < m_pFeaturesList->max_count_history; index_history++){
					index_mov = (f->last_index - index_history)  % m_pFeaturesList->num_nbox;

					if(abs(f->box_list[index_mov].features_mean_diffx) < m_pFeaturesList->min_movement && abs(f->box_list[index_mov].features_mean_diffy) < m_pFeaturesList->min_movement){
						flag_mov = 0;					
					}
				}

				if (flag_mov)
					f->ok = 1;
			}

			X7S_PRINT_DUMP("","En el ID %d tenemos fea_per_box %d   VALIDOS = %d", k, fea_per_box, f->box_list[index].num_features_box);
			min.x = f->box_list[index].min_x;
			min.y = f->box_list[index].min_y;
			max.x = f->box_list[index].max_x;
			max.y = f->box_list[index].max_y;

			//TODO:Remove Green rectangle
			if(m_dbgWnd)
			{
				cvRectangle(pImg, min, max, cvScalar(0,255,0,0), 2, 8, 0);
				snprintf( cindex,sizeof(cindex),"ID %d", f->box_list[index].id );
				cvPutText( pImg, cindex, cvPoint(f->box_list[index].min_x, f->box_list[index].min_y-3), &myFont , cvScalar(0,255,0,0) );
			}


		}
	} //END if features list sieze >0





    //Delete box with min_area_delete < min and num_features_box = 0
	f = m_pFeaturesList->vec;
	while(f != NULL){
		index = f->last_index % m_pFeaturesList->num_nbox;
		if(f->box_list[index].num_features_box < 2 && f->last_index > 2){
			X7S_PRINT_DEBUG(funcName,"ME CARGO id = %d PORQUE NO TIENE SUFICIENTES CARACTERISTICAS \n", index);
			x7sDelHeaderBox(&m_pFeaturesList->vec, &f, m_pFeaturesList->num_nbox);
			m_pFeaturesList->size--;
		}
		else{
			f = f->next_HeaderBox;
		}
	}



	//Re-size Box features
	for(f = m_pFeaturesList->vec; f != 0; f = f->next_HeaderBox){
		int area_real = 0;
		//		int ma_x, ma_y, mi_x, mi_y, cont = 0;
		//		bool flag_overlapping = FALSE, flag_one_overlapping = FALSE;
		float dimen_x_feat;
		float dimen_y_feat;
		float area_features;

		index = f->last_index % m_pFeaturesList->num_nbox;
		dimen_x_feat = f->box_list[index].max_x - f->box_list[index].min_x;
		dimen_y_feat = f->box_list[index].max_y - f->box_list[index].min_y;
		area_features = dimen_x_feat * dimen_y_feat;
		k = f->box_list[index].id;

		X7S_PRINT_DUMP(funcName,"ID: %d   Area features = %f    dimen_x_feat = %f   dimen_y_feat = %f  \n", k, area_features, dimen_x_feat, dimen_y_feat);

		if(area_features < m_pFeaturesList->max_area_person){
			int *list_nblobs;
			list_nblobs = (int *)malloc((m_pConComList->size + 1) * sizeof(int));
			for(int i = 0; i < m_pConComList->size + 1; i++)
				list_nblobs [i] = -1;

			f->box_list[index].area = 0;


			for(int i = 0; i < m_pFeaturesList->count_features; i++){
				if(m_pFeaturesList->valid_ID[i] == k && m_pFeaturesList->lost_points[i]){
					//cvCircle( pImg, pr, 3, CV_RGB(255,255,0), 1, 8, 1);
					pr = cvPointFrom32f(m_pFeaturesList->points[1][i]);
					CvScalar s; 
					if(pr.y>=0 && pr.x>=0 && pr.y<pImg->height && pr.x<pImg->width){
						s = cvGet2D(pFG, (int)pr.y, (int)pr.x); 
						int id_cc = (int)s.val[0];

						if(id_cc >= 1){
							if(list_nblobs [id_cc] == -1){
								int max_p_x = MAX(m_pConComList->vec[id_cc-1].max_x, f->box_list[index].max_x);
								int min_p_x = MIN(m_pConComList->vec[id_cc-1].min_x, f->box_list[index].min_x);
								int max_p_y = MAX(m_pConComList->vec[id_cc-1].max_y, f->box_list[index].max_y);	
								int min_p_y = MIN(m_pConComList->vec[id_cc-1].min_y, f->box_list[index].min_y);
								int dimens_x = max_p_x - min_p_x;
								int dimens_y = max_p_y - min_p_y;
								if((f->box_list[index].area + m_pConComList->vec[id_cc-1].area) < m_pFeaturesList->max_area_person && dimens_x < m_pFeaturesList->max_dim_xy && dimens_y < m_pFeaturesList->max_dim_xy){
									f->box_list[index].max_x = max_p_x;
									f->box_list[index].min_x = min_p_x;
									f->box_list[index].max_y = max_p_y;
									f->box_list[index].min_y = min_p_y;
									f->box_list[index].area = f->box_list[index].area + m_pConComList->vec[id_cc-1].area;
									X7S_PRINT_DUMP(funcName,"CAMBIO");
								}
								area_real = area_real + m_pConComList->vec[id_cc-1].area;
								list_nblobs[id_cc] = 1;
								X7S_PRINT_DUMP(funcName,"\nArea sumatoria %d     ID=%d", f->box_list[index].area, f->box_list[index].id);
							}
							area_real = area_real + m_pConComList->vec[id_cc-1].area;
							list_nblobs[id_cc] = 1;
							X7S_PRINT_DUMP(funcName,"\nArea sumatoria %d     ID=%d", f->box_list[index].area, f->box_list[index].id);
						}
					}
				}
			}
			
			if (f->box_list[index].area < area_features){
				pr.x = f->box_list[index].max_x;
				pr.y = f->box_list[index].max_y;
				cvCircle(pImg, pr, 7, CV_RGB(255,0,0), CV_FILLED, CV_AA, 0);

				pr.x = f->box_list[index].min_x;
				pr.y = f->box_list[index].min_y;
				cvCircle(pImg, pr, 7, CV_RGB(255,0,0), CV_FILLED, CV_AA, 0);

				f->box_list[index].area = area_features;
			}

			X7S_PRINT_DUMP(funcName,"Area real %d  area = %d\n", area_real, f->box_list[index].area);
			if(m_pFeaturesList->min_area > f->box_list[index].area && area_real > f->box_list[index].area){
				float dimen_x = dimen_x_feat/3;
				float dimen_y = dimen_y_feat/3;

				float increm_max_x = f->box_list[index].max_x + dimen_x;
				float decrem_min_x = f->box_list[index].min_x - dimen_x;
				float increm_max_y = f->box_list[index].max_y + dimen_y;
				float decrem_min_y = f->box_list[index].min_y - dimen_y;

				if(increm_max_x <= pImg->width)
					f->box_list[index].max_x = increm_max_x;
				else
					f->box_list[index].max_x = pImg->width;

				if(increm_max_y <= pImg->height)
					f->box_list[index].max_y = increm_max_y;
				else
					f->box_list[index].max_y = pImg->height;

				if(decrem_min_x >= 0)
					f->box_list[index].min_x = decrem_min_x;
				else
					f->box_list[index].min_x = 0;

				if(decrem_min_y >= 0)
					f->box_list[index].min_y = decrem_min_y;
				else
					f->box_list[index].min_y = 0;

			}

			free(list_nblobs);
		}
	}

	/*for(f = m_pFeaturesList->vec; f != 0; f = f->next_HeaderBox){
		if (f->ok){
			index = f->last_index % m_pFeaturesList->num_nbox;
			min.x = f->box_list[index].min_x;
			min.y = f->box_list[index].min_y;
			max.x = f->box_list[index].max_x;
			max.y = f->box_list[index].max_y;
			cvRectangle(pImg, min, max, cvScalar(255, 0, 255, 0), 4, 8, 0);
		}
	}*/


	//DEL OVERLAPPING
	for(f1 = m_pFeaturesList->vec; f1 != 0; f1 = f1->next_HeaderBox){
		indexf1 = f1->last_index % m_pFeaturesList->num_nbox;
		for(f2 = m_pFeaturesList->vec; f2 != 0; f2 = f2->next_HeaderBox){
			indexf2 = f2->last_index % m_pFeaturesList->num_nbox;
			if(f1 != f2)
				x7sDelBoxTrackOverlapping(&f1->box_list[indexf1], &f2->box_list[indexf2]);
		}
	}



	//Draw history
	/*int g=0;
	for(f = m_pFeaturesList->vec; f != 0; f = f->next_HeaderBox){
		if (m_pFeaturesList->size > 0){
			int color = 0;
			for(int i=0; i < f->last_index && i < m_pFeaturesList->num_nbox; i++){
				min.x = f->box_list[i].min_x;
				min.y = f->box_list[i].min_y;
				max.x = f->box_list[i].max_x;
				max.y = f->box_list[i].max_y;
				color = color+20;
				cvRectangle(pImg, min, max, cvScalar(color,color,0,0), 2, 8, 0);
				sprintf( cindex,sizeof(cindex), "ID %d", f->box_list[i].id );
				cvPutText( pImg, cindex, cvPoint(min.x, min.y-3), &myFont , cvScalar(255,0,0,0) );
				X7S_PRINT_DUMP("","ID %d     area %d", f->box_list[i].id, f->box_list[i].area);
				g++;
			}
		}
	}*/


	// Search overlapping
	for(int i = 0; i < m_pConComList->size; i++){
		min.x = m_pConComList->vec[i].min_x;
		min.y = m_pConComList->vec[i].min_y;
		max.x = m_pConComList->vec[i].max_x;
		max.y = m_pConComList->vec[i].max_y;

		float dim_x, dim_y, div;
		dim_x = abs(m_pConComList->vec[i].max_x - m_pConComList->vec[i].min_x);
		dim_y = abs(m_pConComList->vec[i].max_y - m_pConComList->vec[i].min_y);
		div = dim_x/dim_y;
		X7S_PRINT_DUMP("","dim_x = %f   dim_y = %f    area = %d    div = %f ", dim_x, dim_y, m_pConComList->vec[i].area, div);

		if(m_pConComList->vec[i].area < m_pFeaturesList->max_area_person && m_pConComList->vec[i].area > m_pFeaturesList->min_area && div < m_pFeaturesList->interval[1] && div > m_pFeaturesList->interval[0]){
			bool flag_overlapping = FALSE;
			for(f = m_pFeaturesList->vec; f != 0; f = f->next_HeaderBox){
				index = f->last_index % m_pFeaturesList->num_nbox;
				flag_overlapping = x7sBoxOverlapping(&m_pConComList->vec[i], &f->box_list[index]);
				if (flag_overlapping == TRUE){
					break;
				}
			}
			int li = 0;
			if (flag_overlapping == FALSE){
				if (m_pFeaturesList->vec == NULL)
				{
					m_pFeaturesList->vec = x7sCreateHeaderBox(m_pFeaturesList,NULL);
					b = m_pFeaturesList->vec->box_list;
				}
				else{
					X7sFeaturesHeaderBox *f_last=NULL;
					for(f = m_pFeaturesList->vec; f != 0; f = f->next_HeaderBox)
						f_last = f;

					f = x7sCreateHeaderBox(m_pFeaturesList,f_last);
					b = f->box_list;
				}

				b[li].min_x = m_pConComList->vec[i].min_x;
				b[li].min_y = m_pConComList->vec[i].min_y;
				b[li].max_x = m_pConComList->vec[i].max_x;
				b[li].max_y = m_pConComList->vec[i].max_y;
				b[li].x = m_pConComList->vec[i].x;
				b[li].y = m_pConComList->vec[i].y;
				b[li].area = m_pConComList->vec[i].area;
				b[li].dim_x = b->max_x - b->min_x;
				b[li].dim_y = b->max_y - b->min_y;

				m_pFeaturesList->size++;
				b[li].id = m_pFeaturesList->last_IDbox;
				m_pFeaturesList->last_IDbox++;

				/*
				min.x = b[li].min_x;
				min.y = b[li].min_y;
				max.x = b[li].max_x;
				max.y = b[li].max_y;
				cvRectangle(pImg, min, max, cvScalar(50,0,50,0), 2, 8, 0);*/

			}
		}
	}

	for(f = m_pFeaturesList->vec; f != 0; f = f->next_HeaderBox){
		if (f->ok){
			index = f->last_index % m_pFeaturesList->num_nbox;
			min.x = f->box_list[index].min_x;
			min.y = f->box_list[index].min_y;
			max.x = f->box_list[index].max_x;
			max.y = f->box_list[index].max_y;
			cvRectangle(pImg, min, max, cvScalar(255, 0, 0, 0), 2, 8, 0);
		}
	}

	//Delete box with min_area_delete < min and num_features_box = 0
	f = m_pFeaturesList->vec;
	while(f != NULL){
		index = f->last_index % m_pFeaturesList->num_nbox;
		X7S_PRINT_DUMP("","AREA DEL CACHARRO %d   NUM CARACTERISTICAS %d index %d", f->box_list[index].area, f->box_list[index].num_features_box, index);
		if(f->box_list[index].area < m_pFeaturesList->min_area_delete && (f->box_list[index].num_features_box <= 3 && f->last_index > 2)){
			X7S_PRINT_DEBUG(funcName,"ME CARGO %d",index);
			x7sDelHeaderBox(&m_pFeaturesList->vec, &f, m_pFeaturesList->num_nbox);
			m_pFeaturesList->size--;
		}
		else{
			f = f->next_HeaderBox;
		}
	}


	//Delete box with wrong dimension
	f = m_pFeaturesList->vec;
	while(f != NULL){
		index = f->last_index % m_pFeaturesList->num_nbox;
		if(m_pFeaturesList->min_dim_xy > (f->box_list[index].max_x - f->box_list[index].min_x) || m_pFeaturesList->min_dim_xy > (f->box_list[index].max_y - f->box_list[index].min_y)){
			X7S_PRINT_DEBUG(funcName,"ME CARGO %d PORQUE TIENE UNA DIMENSION MENOR DE LO NORMAL",index);
			x7sDelHeaderBox(&m_pFeaturesList->vec, &f, m_pFeaturesList->num_nbox);
			m_pFeaturesList->size--;
		}
		else{
			f = f->next_HeaderBox;
		}
	}


    //Delete box with no movement
	f = m_pFeaturesList->vec;
	while(f != NULL){
		if(f->ok){
			int index_mov;
			int flag_mov = 0;

			for(int index_history = 0; index_history < m_pFeaturesList->max_count_history; index_history++){
				index_mov = (f->last_index - index_history) % m_pFeaturesList->num_nbox;

				if(abs(f->box_list[index_mov].features_mean_diffx) > m_pFeaturesList->min_movement || abs(f->box_list[index_mov].features_mean_diffy) > m_pFeaturesList->min_movement){
					flag_mov = 1;					
				}
			}

			if (!flag_mov){
				X7S_PRINT_DEBUG(funcName,"ME CARGO %d",index);
				x7sDelHeaderBox(&m_pFeaturesList->vec, &f, m_pFeaturesList->num_nbox);
				m_pFeaturesList->size--;
			}
			else{
				f = f->next_HeaderBox;				
			}
		}
		else{
			f = f->next_HeaderBox;		
		}
	}


	//Draw box & Histograms
	cvSet(m_pFeaturesList->mask_tracker, cvScalar(0), 0);

	f = m_pFeaturesList->vec;
	while(f != 0){
		int index_prev;
		float coordx_index, coordy_index;
		float coordx_index_prev, coordy_index_prev;
		float dist_center, dist_center_prev;
		int flag_center;


		index = f->last_index % m_pFeaturesList->num_nbox;

		//X7S_PRINT_DUMP("","index  %d", index);

		min.x = f->box_list[index].min_x;
		min.y = f->box_list[index].min_y;
		max.x = f->box_list[index].max_x;
		max.y = f->box_list[index].max_y;
		
		cvRectangle(m_pFeaturesList->mask_tracker, min, max, cvScalar(255,0), -1, 8, 0);
		cvAnd( m_pFeaturesList->mask_tracker, this->m_pFGMask, m_pFeaturesList->mask_hist, 0 );

		if(f->last_index > 2){
			index_prev = (f->last_index-2) % m_pFeaturesList->num_nbox;
			coordx_index = (f->box_list[index].max_x - f->box_list[index].min_x)/2;
			coordy_index = (f->box_list[index].max_y - f->box_list[index].min_y)/2;
			coordx_index_prev = (f->box_list[index_prev].max_x - f->box_list[index_prev].min_x)/2;
			coordy_index_prev = (f->box_list[index_prev].max_y - f->box_list[index_prev].min_y)/2;

			dist_center = sqrt(pow((coordx_index - center_x),2) + pow((coordy_index - center_y),2));
			dist_center_prev = sqrt(pow((coordx_index_prev - center_x),2) + pow((coordy_index_prev - center_y),2));

			if(dist_center < dist_center_prev)
				flag_center = 1;
			else
				flag_center = 0;
		}



		if(f->box_list[index].flag_hist==0){
			if(flag_center && f->last_index > m_pFeaturesList->max_count_history){
				CvRect areai = cvRect(f->box_list[index].min_x, f->box_list[index].min_y, (f->box_list[index].max_x - f->box_list[index].min_x), (f->box_list[index].max_y - f->box_list[index].min_y));
				cvSplit(m_pFeaturesList->yuv, m_pFeaturesList->y_plane_i, m_pFeaturesList->u_plane_i, m_pFeaturesList->v_plane_i, 0 );
				cvSetImageROI(m_pFeaturesList->y_plane_i, areai);
				cvSetImageROI(m_pFeaturesList->u_plane_i, areai);
				cvSetImageROI(m_pFeaturesList->v_plane_i, areai);
				cvSetImageROI(m_pFeaturesList->mask_hist, areai);
				cvCalcHist(m_pFeaturesList->planes_i, f->box_list[index].hist_ini, 0, m_pFeaturesList->mask_hist);
				cvResetImageROI(m_pFeaturesList->y_plane_i);
				cvResetImageROI(m_pFeaturesList->u_plane_i);
				cvResetImageROI(m_pFeaturesList->v_plane_i);
				cvResetImageROI(m_pFeaturesList->mask_hist);
				f->box_list[index].flag_hist = 1;
				f = f->next_HeaderBox;
			}else{
				f = f->next_HeaderBox;			
			}
		}
		else{
			CvRect areaj = cvRect(f->box_list[index].min_x, f->box_list[index].min_y, (f->box_list[index].max_x - f->box_list[index].min_x), (f->box_list[index].max_y - f->box_list[index].min_y));


			cvSplit(m_pFeaturesList->yuv, m_pFeaturesList->y_plane_f, m_pFeaturesList->u_plane_f, m_pFeaturesList->v_plane_f, 0);

			cvSetImageROI(m_pFeaturesList->y_plane_f, areaj);
			cvSetImageROI(m_pFeaturesList->u_plane_f, areaj);
			cvSetImageROI(m_pFeaturesList->v_plane_f, areaj);
			cvSetImageROI(m_pFeaturesList->mask_hist, areaj);

			cvCalcHist(m_pFeaturesList->planes_f, m_pFeaturesList->hist_fin, 0, m_pFeaturesList->mask_hist);

			if(m_dbgWnd)
			{
				cvNamedWindow("INI");
				cvShowImage("INI", m_pFeaturesList->y_plane_i);
				cvNamedWindow("FIN");
				cvShowImage("FIN", m_pFeaturesList->y_plane_f);
			}

			cvResetImageROI( m_pFeaturesList->y_plane_f );
			cvResetImageROI( m_pFeaturesList->u_plane_f );
			cvResetImageROI( m_pFeaturesList->v_plane_f );
			cvResetImageROI(m_pFeaturesList->mask_hist);

			double result = cvCompareHist(f->box_list[index].hist_ini, m_pFeaturesList->hist_fin, CV_COMP_CORREL);
			X7S_PRINT_DUMP("","RESULTADO DE LA COMPARACION DE HISTOGRAMAS %lf   f_id = %d", result, f->box_list[index].id);

			if(result > 0.7){
				if (min.x > sub_frame_min_x && min.y > sub_frame_min_y && max.x < sub_frame_max_x && max.y < sub_frame_max_y){
					//TODO: Check if hist_ini is allocated ???
					cvReleaseHist(&f->box_list[index].hist_ini);
					cvCopyHist(m_pFeaturesList->hist_fin, &f->box_list[index].hist_ini);
					X7S_PRINT_DUMP("","No toca el borde, histogramas casi identicos, intercambio por el anterior");
				}
				else{
					if(flag_center){
						//TODO: Check if hist_ini is allocated ???
						cvReleaseHist(&f->box_list[index].hist_ini);
						cvCopyHist(m_pFeaturesList->hist_fin, &f->box_list[index].hist_ini);
						X7S_PRINT_DUMP("","Estamos tocando un borde (oclusion), histogramas casi identicos, actualizo porque se mueve al centro");
					}
					else
						X7S_PRINT_DUMP("","Estamos tocando un borde (oclusion), se mueve hacia fuera, histogramas casi identicos, no actualizo....?");
				}
				f = f->next_HeaderBox;
			}
			else{
				if (min.x > sub_frame_min_x && min.y > sub_frame_min_y && max.x < sub_frame_max_x && max.y < sub_frame_max_y){
					X7S_PRINT_DUMP("","Histograma muy diferente sin tocar el borde (oclusion) ...? ");
					f = f->next_HeaderBox;
				}
				else{
					if(result > 0.6 || f->box_list[index].area > m_pFeaturesList->min_area_delete){
						X7S_PRINT_DUMP("","Borde. El Histograma es parecido o el area del blob es grande. Area: %d ... lo mantengo todavia", f->box_list[index].area);
						f = f->next_HeaderBox;
					}
					else{
						X7S_PRINT_DUMP("","Borde. ME CARGO LA CAJA, ES MUY DISTINTA AL OBJETO INICIAL");
						x7sDelHeaderBox(&m_pFeaturesList->vec, &f, m_pFeaturesList->num_nbox);
						m_pFeaturesList->size--;
					}
				}
			}

		}


	}



	cvAnd(m_pFeaturesList->mask_tracker, this->m_pFGMask, m_pFeaturesList->mask_tracker, 0);	


	if(m_dbgWnd)
	{
		cvNamedWindow("FG");
		cvShowImage("FG", this->m_pFGMask);
		cvNamedWindow("MASK TRACKER");
		cvShowImage("MASK TRACKER", m_pFeaturesList->mask_tracker);
	}


	////GOOD FEATURES ************************************************************
	if (m_pFeaturesList->size > 0){
		m_pFeaturesList->count_features = m_pFeaturesList->max_num_boxfeatures;

		cvGoodFeaturesToTrack( m_pFeaturesList->grey, m_pFeaturesList->eig, m_pFeaturesList->temp, 
				m_pFeaturesList->points[0], &m_pFeaturesList->count_features,
				m_pFeaturesList->quality, m_pFeaturesList->min_distance, m_pFeaturesList->mask_tracker, 3, 1, 0.04 );
		cvFindCornerSubPix( m_pFeaturesList->grey, m_pFeaturesList->points[0], m_pFeaturesList->count_features,
				cvSize(m_pFeaturesList->win_size, m_pFeaturesList->win_size), cvSize(-1,-1),
				cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03));

		//Draw features
		//for(int i = 0; i < m_pFeaturesList->count_features; i++){
		//	ps = cvPointFrom32f(m_pFeaturesList->points[0][i]);
		//	if(ps.y>=0 && ps.x>=0 && ps.y<288 && ps.x<360){
		//		s = cvGet2D(m_pFeaturesList->eig, ps.y, ps.x);
		//		cvCircle( pImg, cvPointFrom32f(m_pFeaturesList->points[0][i]), 2, CV_RGB(255,0,0), -1, 8,0);
		//	}
		//}

		for(int j = 0; j < m_pFeaturesList->count_features; j++){
			m_pFeaturesList->points_IDbox[j] = -1;
		}

		//To put features inside the box
		for(f = m_pFeaturesList->vec; f != 0; f = f->next_HeaderBox){
			index = f->last_index % m_pFeaturesList->num_nbox;
			min.x = f->box_list[index].min_x;
			min.y = f->box_list[index].min_y;
			max.x = f->box_list[index].max_x;
			max.y = f->box_list[index].max_y;
			k = f->box_list[index].id;
			for(int j = 0; j < m_pFeaturesList->count_features; j++){
				ps = cvPointFrom32f(m_pFeaturesList->points[0][j]);
				if (min.x <= ps.x && max.x >= ps.x && min.y <= ps.y && max.y >= ps.y){
					m_pFeaturesList->points_IDbox[j] = k;
				}
			}
		}

		//print id box features.
		//for(int j = 0; j < m_pFeaturesList->count_features; j++){
		//	ps = cvPointFrom32f(m_pFeaturesList->points[0][j]);
		//	sprintf(cindex ,sizeof(cindex), "%d", m_pFeaturesList->points_IDbox[j] );
		//	cvPutText( pImg, cindex, cvPoint(ps.x, ps.y-3), &myFont , cvScalar(255,255,0,0) );
		//}
	}
	else{
		m_pFeaturesList->flags_pyramid = 0;	
	}
	//****************************************************************************

	CV_SWAP( m_pFeaturesList->prev_grey, m_pFeaturesList->grey, m_pFeaturesList->swap_temp );
	if (m_pFeaturesList->flags_pyramid != 0)	
		CV_SWAP( m_pFeaturesList->prev_pyramid, m_pFeaturesList->pyramid, m_pFeaturesList->swap_temp );



}



void X7sVidSurvPipelineFeatures::ProcessBTGen(IplImage* pImg, IplImage* pFG)
{
	if(m_pBTGen)
	{   /* Run track generator: */
		int index;
		X7sFeaturesHeaderBox *f;

		for(f = m_pFeaturesList->vec; f != 0; f = f->next_HeaderBox){
			if (f->ok){
				index = f->last_index % m_pFeaturesList->num_nbox;
				CvBlob blob = cvBlob(f->box_list[index].id,
						f->box_list[index].min_x,f->box_list[index].max_x,
						f->box_list[index].min_y,f->box_list[index].max_y);
				m_pBTGen->AddBlob(&blob);
			}
		}
		m_pBTGen->Process(pImg, pFG);
	}   /* Run track generator: */
}


void X7sVidSurvPipelineFeatures::ProcessAlarm()
{
	bool ret;
	X7sBlobTrajectory *pTraj;
	if(m_pAlarmMan && m_pBTGen) {

		for(int i=0;i<m_pBTGen->GetBlobTrajNum();i++)
		{
			pTraj = m_pBTGen->GetBlobTraj(i);
			ret=m_pAlarmMan->Process(m_FrameCount,pTraj);
		}
	}
}



//========================================================================
//-- Public Interfaces.

/**
* @brief Public interface to create a VidSurvPipeline.
* @param parent The parent X7sXmlObject.
* @return A pointer on the generic VidSurvPipeline class.
*/
X7sVidSurvPipeline* x7sCreateVidSurvPipelineFeatures(X7sXmlObject *parent)
																		{
	return (X7sVidSurvPipeline*)new X7sVidSurvPipelineFeatures(parent);
																		}


