/**
 *  @file
 *  @brief Contains the class BlobDetector.h
 *  @date 02-abr-2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef BLOBDETECTOR_H_
#define BLOBDETECTOR_H_


#include "ix7svidsurv.h"


#define IX7S_BDRL_NUM_FRAME 5	//!< Number of consecutive frame used for the BlobDetector module with Run-Length implementation.
#define IX7S_BDRL_NUMMAX_NEWBLOB 10	//!< Maximum number of new blob to copy to the list



/**
 * @brief Abstract class the give the main tools for detecting new blobs.
 */
class X7sBlobDetector: public CvBlobDetector
{
public:
	X7sBlobDetector();
	virtual ~X7sBlobDetector();
	virtual int DetectNewBlob(IplImage* pImg, IplImage* pFGMask, CvBlobSeq* pNewBlobList, CvBlobSeq* pOldBlobList)=0;
	virtual void Release(){delete this;};

protected:
	void ShiftQueueHistoryBlobList();
	virtual void GetNewBlobList(IplImage* pFGMask,CvBlobSeq *pBlobList);
	virtual void FindBestTraj(IplImage *pFG, CvBlobSeq* pNewBlobList, CvBlobSeq* pOldBlobList);
	virtual void SetNBestRecentBlob(CvBlobSeq * pBlobPList, int Nfirst=-1);
	virtual bool MergeSmallConCompo(X7sConCompoList *ccList);

	static void SortBlobList(CvBlobSeq *blobList);
	static bool IsBlobTooSmall(CvBlob *pBlob,int width, int height);
	static bool IsBlobTooSmall(CvBlob *pBlob,const CvSize &FGSize, float ratio);
	static bool IsBlobIntersect(CvBlob *pBlob,CvBlobSeq* pBlobList);
	static float IntersectionRatio(CvBlob *pBBig, CvBlob *pBSmall);


	X7sConCompoList *m_pConComList;		//!< Actual list of the connected component (It may be not used).
	CvBlobSeq **m_pBlobLists; 			//!< History of the lists of Blob detected on previous frames
	CvBlobSeq *m_pActualBlobList;		//!< This blob list point on the most recent of history of blob lists.
	int m_sizeBlobList;					//!< The number of history layer to keep blobs.
	float m_minRatio;					//!< If it is <=0, no filtering on the size is applied.
	int m_isDrawDebug;					//!< Set to true if you want to draw the debugging function
	int m_onlyFullBlob;					//!< If @TRUE insert blob only when it doesn't touch the border.
	float m_distFilterStatic;			//!< Distance at which we filter the static object.
	float m_mergeMaxArea;
	float m_mergeMaxDist;
	float m_mergeMaxRatioArea;

};


/**
 * @brief Blob are detected from Connected Component obtained using a Run-Length
 * based algorithm.
 * @ingroup x7svidsurv
 */
class X7sBlobDetectorCV: public X7sBlobDetector
{
public:
	X7sBlobDetectorCV();
	~X7sBlobDetectorCV();
	int DetectNewBlob(IplImage* pImg, IplImage* pFGMask, CvBlobSeq* pNewBlobList, CvBlobSeq* pOldBlobList);

protected:

	/* Parameters */
	float m_param_roi_scale;
};


/**
 * @brief Blob are detected from Connected Component obtained using a Run-Length
 * based algorithm.
 * @ingroup x7svidsurv
 */
class X7sBlobDetectorCCCM: public X7sBlobDetector
{
public:
	X7sBlobDetectorCCCM();
	~X7sBlobDetectorCCCM();
	int DetectNewBlob(IplImage* pImg, IplImage* pFGMask, CvBlobSeq* pNewBlobList, CvBlobSeq* pOldBlobList);
	void FindBestTraj(IplImage *pFG, CvBlobSeq* pNewBlobList, CvBlobSeq* pOldBlobList);

protected:

	/* Parameters */
	float m_param_roi_scale;
};



/**
 * @brief Blob detector which use directly a buffer with data in Line-Run-Length
 * Encoding format. (Convertion can be done using @ref X7S_CV_LRLE2GREY)
 * This class just redefine the GetNewBlobList list method of X7sBlobDetectorCV.
 * @ingroup x7svidsurv
 */
class X7sBlobDetectorLRLE: public X7sBlobDetectorCV
{
protected:
	void GetNewBlobList(IplImage* pFGMask,CvBlobSeq *pBlobList);
};



void x7sDrawCvBlobList(IplImage *pImg,CvBlob* pBlobs, int nof_blobs, int hue_color=-1);


#endif /* BLOBDETECTOR_H_ */
