/**
* @internal
* @file
* @brief Contains various function to handle openCV VideoSurveillance Module.
*/



#include "ix7svidsurv.h"
#include "X7sVSLog.hpp"
#include <vector>

const char* ix7sBlobTrackerNames[X7S_VS_BT_END] =
{
		"BT_CC",
		"BT_CC2",
		"BT_CCMSPF",
		"BT_MS",
		"BT_MSPF",
		"BT_MSFG",
		"BT_MSFGS",
		"BT_KM",
		"BT_LHR",
		"BT_LHRS",
};

const char* ix7sBlobTrackerPostProcNames[X7S_VS_BTPP_END] =
{
		"BTPP_KF",
		"BTPP_TAE",
		"BTPP_TAR",
};

const char* ix7sBlobTrackerGenNames[X7S_VS_BTGEN_END] =
{
		"BTGEN_1",
		"BTGEN_YML",
};



CvBlobTracker* x7sCreateBlobTracker(int type, void *param)
{
	CvBlobTracker *module=NULL;
	switch(type)
	{
	case X7S_VS_BT_CC: 		module = x7sCreateBlobTrackerCC(); 			break;
	case X7S_VS_BT_CC2: 	module = x7sCreateBlobTrackerCC2(); 		break;
	case X7S_VS_BT_CCMSPF: 	module = cvCreateBlobTrackerCCMSPF();		break;
	case X7S_VS_BT_MS:		module = cvCreateBlobTrackerMS();			break;
	case X7S_VS_BT_MSPF:	module = cvCreateBlobTrackerMSPF();			break;
	case X7S_VS_BT_MSFG:	module = cvCreateBlobTrackerMSFG();			break;
	case X7S_VS_BT_MSFGS:	module = cvCreateBlobTrackerMSFGS();		break;
	case X7S_VS_BT_KM:		module = cvCreateBlobTrackerKalman(); 		break;
	//case X7S_VS_BT_LHR:	module = cvCreateBlobTrackerLHR(param);		break;
	//case X7S_VS_BT_LHRS:	module = cvCreateBlobTrackerLHRS(param);	break;
	default:
		X7S_PRINT_WARN("x7sCreateBlobTracker()","No Blob Tracker for type %d",type);
		return NULL;
	}
	if(module) {
		char buff[20];
		snprintf(buff,20,"%d",type);
		module->SetNickName((x7scharA*)buff);
		X7S_PRINT_DEBUG("x7sCreateBlobTracker()","Blob Tracker created: %s",module->GetNickName());
	}
	return module;
}


CvBlobTrackPostProc* x7sCreateVSModBTPostProc(int type)
{
	CvBlobTrackPostProc *module=NULL;
	switch(type)
	{
	case X7S_VS_BTPP_KF: 	module = cvCreateModuleBlobTrackPostProcKalman(); 			break;
	case X7S_VS_BTPP_TAE: 	module = cvCreateModuleBlobTrackPostProcTimeAverExp();		break;
	case X7S_VS_BTPP_TAR:	module = cvCreateModuleBlobTrackPostProcTimeAverRect();		break;
	default:
		X7S_PRINT_WARN("x7sCreateVSModBTPostProc()","No Blob Tracker PostProc for type %d",type);
		return NULL;
	}
	if(module) {
		char buff[20];
		snprintf(buff,20,"%d",type);
		module->SetNickName((x7scharA*)buff);
		X7S_PRINT_DEBUG("x7sCreateVSModBTPostProc()","Blob Tracker Post Processing created: %s type=",module->GetTypeName(),module->GetNickName());
	}
	return module;
}

/**
* Generate a file to save the trajectory of each blob are generated.
*
* At each frame: the blob is added to a blob trajectory list.
* If this list doesn't exist it is created, and we keep the frame in
* which this list is created.
* Then, when a blob disappear or when the CvBlobTrackGen is deleted
* the blob is saved to the corresponding file.
*
* @param type The Type of format for saving the trajectory (@ref X7S_VS_BTGEN_xxx)
* @return A Generic CvBlobTrackGen module.
*/
CvBlobTrackGen *x7sCreateVSModBTGen(int type)
{
	CvBlobTrackGen *module=NULL;
	switch(type)
	{
	case X7S_VS_BTGEN_TXT: 	module = cvCreateModuleBlobTrackGen1(); 			break;
	case X7S_VS_BTGEN_YML: 	module = cvCreateModuleBlobTrackGenYML();		break;
	default:
		X7S_PRINT_WARN("x7sCreateVSModBTGen()","No Blob Tracker Generator for type %d",type);
		return NULL;
	}
	if(module) {
		char buff[20];
		snprintf(buff,20,"%d",type);
		module->SetNickName((x7scharA*)buff);
		X7S_PRINT_INFO("x7sCreateVSModBTGen()","Blob Tracker Post Processing created: %s",module->GetNickName());
	}
	return module;
}


//========================================================================
//-- X7SVS Public Interfaces.

int x7sVSModulesWriteAllToXML(TiXmlElement *parent)
{
	std::vector<CvVSModule*> vecModule;
	//Blob Detector Module.
	vecModule.push_back(x7sCreateBlobDetector(X7S_VS_BD_SIMPLE));
	vecModule.push_back(x7sCreateBlobDetector(X7S_VS_BD_CC));
	vecModule.push_back(x7sCreateBlobDetector(X7S_VS_BD_CCCM));
	vecModule.push_back(x7sCreateBlobDetector(X7S_VS_BD_LRLE));

	//Blob Tracker Module.
	vecModule.push_back(x7sCreateBlobTracker(X7S_VS_BT_CC));
	vecModule.push_back(x7sCreateBlobTracker(X7S_VS_BT_CCMSPF));
	vecModule.push_back(x7sCreateBlobTracker(X7S_VS_BT_MS));
	vecModule.push_back(x7sCreateBlobTracker(X7S_VS_BT_MSPF));
	vecModule.push_back(x7sCreateBlobTracker(X7S_VS_BT_MSFG));
	vecModule.push_back(x7sCreateBlobTracker(X7S_VS_BT_MSFGS));
	vecModule.push_back(x7sCreateBlobTracker(X7S_VS_BT_LHR));
	vecModule.push_back(x7sCreateBlobTracker(X7S_VS_BT_LHRS));

	//Trajectory post-processing module
	vecModule.push_back(x7sCreateVSModBTPostProc(X7S_VS_BTPP_KF));
	vecModule.push_back(x7sCreateVSModBTPostProc(X7S_VS_BTPP_TAE));
	vecModule.push_back(x7sCreateVSModBTPostProc(X7S_VS_BTPP_TAR));

	//Trajectory generation modules
	vecModule.push_back(x7sCreateVSModBTGen(X7S_VS_BTGEN_TXT));
	vecModule.push_back(x7sCreateVSModBTGen(X7S_VS_BTGEN_YML));

	//Trajectory Analyzer modules.
	vecModule.push_back(cvCreateModuleBlobTrackAnalysisHistP());
	vecModule.push_back(cvCreateModuleBlobTrackAnalysisHistPV());
	vecModule.push_back(cvCreateModuleBlobTrackAnalysisHistPVS());
	vecModule.push_back(cvCreateModuleBlobTrackAnalysisHistSS());
	vecModule.push_back(cvCreateModuleBlobTrackAnalysisTrackDist());
	vecModule.push_back(cvCreateModuleBlobTrackAnalysisIOR());

	for(uint32_t i=0;i<vecModule.size();i++) {
		x7sVSModuleWriteToXML(vecModule[i],parent);
	}

	return X7S_VS_RET_OK;
}

/**
* @brief Read all the parameters from a XML node to a CvVSModule (using X7sXMlParamList)
* @param module A pointer to the openCV module.
* @param node	 A xml node that can be the parent node or the child node itself.
*/
int x7sVSModuleReadFromXML(CvVSModule *module, const TiXmlElement *node)
{

	//Declaration of variable
	int index=0;
	const char *name;
	int type;
	x7scharA *val_str;
	X7sParam *param=NULL;
	const TiXmlElement *ele;

	//Checking if the argument are not NULL
	if(!module || !node) {
		X7S_PRINT_ERROR("x7sVSModuleReadFromXML()","module=0x%x or node=0x%x",module,node);
		return X7S_VS_RET_ERR;
	}

	//Use the module name as tagname for the xml node.
	name=module->GetTypeName();
	type=atoi(module->GetNickName());

	if(!name || strcmp(name,"unknown")==0 || strcmp(name,"")==0) {
		X7S_PRINT_ERROR("x7sVSModuleReadFromXML()","name \"%s\" is not valid",name);
		return X7S_VS_RET_ERR;
	}

	const TiXmlElement *subnode;
	//Search on this node
	if(X7S_XML_ISEQNODENAME(node,name)==false)
	{
		//Search on the subnode
		subnode=node->FirstChildElement(name);
		if(subnode==NULL) {
			X7S_PRINT_WARN("x7sVSModuleReadFromXML()","xml node <%s> does not correspond or contains the module name \"%s\"",node->Value(),name);
			return X7S_VS_RET_ERR;
		}
		else node=subnode;
	}


	X7sXmlParamList xmlParams(name,X7S_XML_ANYID,type);

	X7S_PRINT_INFO("x7sVSModuleReadFromXML()","name=%s, nick=%s, type=%s",module->GetModuleName(),module->GetNickName(),module->GetTypeName());


	//While we obtain a parameters name
	while((name = module->GetParamName(index)) != NULL) {
		//Obtain the value given the name of the parameters
		val_str = module->GetParamStr((x7scharA*)name);

		if(val_str) param = new X7sParam(name,X7S_XML_ANYID,val_str); 			//Add string value
		else param = new X7sParam(name,X7S_XML_ANYID,module->GetParam((x7scharA*)name));	//Add double value

		//Add the name of the parameters to the XML list.
		xmlParams.Insert(param);
		X7S_PRINT_DEBUG("x7sVSModuleReadFromXML()","name=%s <=> val=%s",name,param->toString().c_str());

		//Then delete the parameters
		X7S_DELETE_PTR(param);

		//Jump on the next param index.
		index++;
	}

	//Once the XML parameters list has been filled read the XML node.
	ele=node->ToElement();

	//Read the parameters from this node.
	if(ele==NULL || xmlParams.ReadFromXML(ele)==false) return X7S_RET_ERR;


	//Then set the CvVSModule using the X7sXmlParamList.
	param=NULL;
	while((param=xmlParams.GetNextParam(param))!=NULL) {
		module->SetParamStr((x7scharA*)param->GetName().c_str(),(x7scharA*)param->toString().c_str());
	}
	return X7S_VS_RET_OK;
}

/**
* @brief Write all the parameters of a module to XML node (using X7sXMlParamList)
* @param _module A pointer to the openCV module.
* @param cnode	A pointer to the node with the same tag name as the module type.
*/
int x7sVSModuleWriteToXML(const CvVSModule *_module,TiXmlElement *cnode)
{
	//Declaration of variable
	int index=0;
	const char *name;
	int type;
	x7scharA* val_str;
	X7sParam *param=NULL;
	TiXmlElement *ele;

	//Removing constant function
	CvVSModule *module = (CvVSModule *)_module;

	//Checking if the argument are not NULL
	if(!module || !cnode) {
		X7S_PRINT_ERROR("x7sVSModuleWriteToXML()","module=0x%x or node=0x%x",module,cnode);
		return X7S_VS_RET_ERR;
	}

	X7S_PRINT_INFO("x7sVSModuleWriteToXML()","Writing name=%s : <%s ID=\"-1\" type=\"%s\"></%s>",
			module->GetModuleName(),module->GetTypeName(),module->GetNickName(),module->GetTypeName());

	//Use the module name as tagname for the xml node.
	name=module->GetTypeName();
	type=atoi(module->GetNickName());


	if(!name || strcmp(name,"unknown")==0) {
		X7S_PRINT_ERROR("x7sVSModuleWriteToXML()","name \"%s\" is not valid",name);
		return X7S_VS_RET_ERR;
	}
	X7sXmlParamList xmlParams(name,X7S_XML_ANYID,type);


	//While we obtain a parameters name
	while((name = module->GetParamName(index)) != NULL) {
		//Obtain the value given the name of the parameters
		val_str = module->GetParamStr((x7scharA*)name);

		if(val_str) param = new X7sParam(name,X7S_XML_ANYID,val_str); 			//Add string value
		else param = new X7sParam(name,X7S_XML_ANYID,module->GetParam((x7scharA*)name));	//Add double value

		//Add the name of the parameters to the XML list.
		xmlParams.Insert(param);
		X7S_PRINT_DEBUG("x7sVSModuleWriteToXML()","name=%s <=> val=%s",name,param->toString().c_str());

		X7S_DELETE_PTR(param);

		//Jump on the next param index.
		index++;
	}

	//Then find the correct node
	ele = cnode->FirstChildElement(xmlParams.GetName());
	if(ele) cnode->RemoveChild(ele);	//And delete it exists.
	ele = new TiXmlElement(xmlParams.GetName());	//And then create it (empty).

	//Once the XML parameters list has been filled read the XML node.
	if(ele == NULL || xmlParams.WriteToXML(ele)==false) 	return X7S_VS_RET_ERR;

	//Finally, Add the node to the parent and return OK.
	cnode->LinkEndChild(ele);
	return X7S_VS_RET_OK;
}


int x7sVSModulePrint(const CvVSModule *_module)
{
	//Declaration of variable
	int index=0;
	const char *name;
	const char *val_str;
	X7sParam *param=NULL;

	//Removing constant function
	CvVSModule *module = (CvVSModule *)_module;

	//Checking if the argument are not NULL
	if(!module) {
		X7S_PRINT_ERROR("x7sVSModulePrint()","module is NULL");
		return X7S_VS_RET_ERR;
	}

	X7S_PRINT_INFO("x7sVSModulePrint()","name=%s, nick=%s, type=%s",module->GetModuleName(),module->GetNickName(),module->GetTypeName());

	//While we obtain a parameters name
	while((name = module->GetParamName(index)) != NULL) {
		//Obtain the value given the name of the parameters
		val_str = module->GetParamStr((x7scharA*)name);

		if(val_str) param = new X7sParam(name,X7S_XML_ANYID,val_str); 			//Add string value
		else param = new X7sParam(name,X7S_XML_ANYID,module->GetParam((x7scharA*)name));	//Add double value

		//Add the name of the parameters to the XML list.
		X7S_PRINT_INFO("x7sVSModulePrint()","\tname=%s <=> val=%s",name,param->toString().c_str());

		//Jump on the next param index.
		index++;
		delete param;
		param=NULL;
	}

	return X7S_VS_RET_OK;
}
