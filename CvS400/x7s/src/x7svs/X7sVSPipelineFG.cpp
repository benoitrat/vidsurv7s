#include "X7sVSPipelineFG.hpp"
//========================================================================
//-- Includes done in cpp file to accelerate compilation.
#include "X7sVSLog.hpp"
#include <vector>
#include <highgui.h>

/* Get frequency for each module time working estimation: */
static double FREQ = 1000*cvGetTickFrequency();

#if 1
#define COUNTNUM 100
#define TIME_BEGIN() \
		{\
	static double   _TimeSum = 0;\
	static int      _Count = 0;\
	static int      _CountBlob = 0;\
	int64           _TickCount = cvGetTickCount();\

#define TIME_END(_name_,_BlobNum_)    \
		_Count++;\
		_CountBlob+=_BlobNum_;\
		_TimeSum += (cvGetTickCount()-_TickCount)/FREQ;\
		if(m_TimesFile)if(_Count%COUNTNUM==0)\
		{ \
			FILE* out = fopen(m_TimesFile,"at");\
			if(out)\
			{\
				fprintf(out,"ForFrame Frame: %d %s %f on %f blobs\n",_Count,_name_, _TimeSum/COUNTNUM,((float)_CountBlob)/COUNTNUM);\
				if(_CountBlob>0)fprintf(out,"ForBlob  Frame: %d %s - %f\n",_Count,_name_, _TimeSum/_CountBlob);\
				fclose(out);\
			}\
			_TimeSum = 0;\
			_CountBlob = 0;\
		}\
		}
#else
#define TIME_BEGIN()
#define TIME_END(_name_)
#endif

/* Special extended blob structure for auto blob tracking: */
typedef struct CvBlobTrackAuto
{
	CvBlob  blob;
	int     BadFrames;
} CvBlobTrackAuto;



//========================================================================
//-- Constructor and destructor.

/**
* @brief Our Pipeline with a mix between OpenCV module and our module to
* perform video surveillance and trajectory analysis.
* @param parent The parent X7sXmlObject.
*/
X7sVidSurvPipelineFG::X7sVidSurvPipelineFG(X7sXmlObject *parent)
:X7sVidSurvPipeline(parent,X7S_VS_PIPELINE_FG,X7S_XML_ANYID),
 m_pFGD(NULL),m_pBD(NULL), m_pBT(NULL), m_pBTPP(NULL), m_pBTA(NULL),
 m_BlobList(sizeof(CvBlobTrackerAuto))
{
	X7S_PRINT_DEBUG("X7sVidSurvPipelineFG::X7sVidSurvPipelineFG()");


	m_NextBlobID =0;
	m_FrameCount=0;
	m_PrevBlobNum=0;
	m_pFGMask=NULL;
	m_FGMaskDel=false;
	m_FGTrainFrames=X7S_VS_NUMFRAMES_TRAIN;
	m_BTReal=false;
	m_UsePPData=false;
	m_subModCreated=false;

	m_BlobList.AddFormat((char*)"i");
	m_pBlobList=&m_BlobList;

	m_TimesFile=NULL;
}

/**
* @brief Delete all the thing we have created in the destructor.
*/
X7sVidSurvPipelineFG::~X7sVidSurvPipelineFG()
{
	X7S_PRINT_DEBUG("X7sVidSurvPipelineFG::~X7sVidSurvPipelineFG()");

	// Delete submodules (X7S).
	X7S_RELEASE_PTR(m_pFGD);
	X7S_RELEASE_PTR(m_pBD);

	//Delete OpenCV submodules
	if(m_pBT) 	{ 	m_pBT->SetNickName(NULL); 	m_pBT->Release();  m_pBT=NULL; }
	if(m_pBTPP) {	m_pBTPP->SetNickName(NULL); m_pBTPP->Release(); m_pBTPP=NULL; }
	if(m_pBTA) 	{	m_pBTA->SetNickName(NULL); 	m_pBTA->Release(); m_pBTA=NULL;}
}

//========================================================================
//-- Public Methods.

void X7sVidSurvPipelineFG::Process(IplImage* pImg, IplImage* pFG)
{
	int         CurBlobNum = 0;
	if(m_subModCreated==false)
		m_subModCreated=this->CreateDefaultSubModule();


	if(m_params["enable"]->toIntValue())
	{

		/* Bump frame counter: */
		m_FrameCount++;

		//-------------------------------------------------------------------------------------

		if(m_FrameCount%10==0)
			X7S_PRINT_DUMP("X7sVidSurvPipelineFG::Process()","#%04d nBlobs=%d",
					m_FrameCount,m_BlobList.GetBlobNum());




		if(m_TimesFile)
		{
			static int64  TickCount = cvGetTickCount();
			static double TimeSum = 0;
			static int Count = 0;
			Count++;

			if(Count%100==0)
			{
				time_t ltime;
				time( &ltime );
				FILE* out = fopen(m_TimesFile,"at");
				double Time;
				TickCount = cvGetTickCount()-TickCount;
				Time = TickCount/FREQ;
				if(out){fprintf(out,"- %sFrame: %d ALL_TIME - %f\n",ctime( &ltime ),Count,Time/1000);fclose(out);}

				TimeSum = 0;
				TickCount = cvGetTickCount();
			}
		}

		//-------------------------------------------------------------------------------------


		//Update BG model
		pFG = this->ProcessFGD(pImg,pFG);

		if(pFG==NULL) {
			X7S_PRINT_WARN("X7sVidSurvPipelineFG::Process()","pFG is NULL");
			return;
		}

		int code;
		X7S_CV_GET_CMODEL(pFG,code);
		if(X7S_CV_H2BGR<= code && code<=X7S_CV_HNZ2BGRLEGEND)
		{
			if(m_pFGMask==NULL)
			{
				m_pFGMask=x7sCreateCvtImage(pFG,X7S_CV_CVTCOPY);	//Create a copy of this frame
				m_FGMaskDel=true;
			}
			cvThreshold(pFG,m_pFGMask,0,255,CV_THRESH_BINARY);
		}
		else
		{
			if(m_pFGMask && m_FGMaskDel) cvReleaseImage(&m_pFGMask);
			m_FGMaskDel=false;
			m_pFGMask=pFG;
		}

		//-------------------------------------------------------------------------------------

		//Track the Blob
		CurBlobNum = this->ProcessBT(pImg,m_pFGMask);

		//Than PostProcess the trajectory.
		this->PostProcess(pImg,m_pFGMask,CurBlobNum);

		//-------------------------------------------------------------------------------------

		//Delete blob that are outside the windows.
		this->DeleteBlob(pImg,m_pFGMask);

		//Update blobs position with the tracker.
		TIME_BEGIN()
		if(m_pBT)  m_pBT->Update(pImg, m_pFGMask);
		TIME_END("BlobTrackerUpdate",CurBlobNum)

		//Detect the new blob to add to the tracker list.
		this->ProcessBD(pImg,pFG);

		//-------------------------------------------------------------------------------------
		//Generate a trajectory from the blobList history.
		this->ProcessBTGen(pImg,m_pFGMask);

		//Analyze this trajectory.
		this->ProcessBTAna(pImg,m_pFGMask);

		//And finally process the events
		this->ProcessAlarm();
	}
}


void X7sVidSurvPipelineFG::Draw(IplImage *pImg)
{
	if(pImg && m_pBTGen==NULL)
	{
		x7sDrawCvBlobSeq(pImg,&m_BlobList);
	}
	else
	{
		X7sVidSurvPipeline::Draw(pImg);
	}
}



bool  X7sVidSurvPipelineFG::ReadFromXML(const TiXmlNode *cnode)
{
	X7S_PRINT_INFO("X7sVidSurvPipelineFG::ReadFromXML()");

	//Call parent function.
	X7sXmlObject::ReadFromXML(cnode);

	//Then make the specific call for the children.
	if(X7S_XML_ISEQNODENAME(cnode,X7S_VS_TAGNAME_PIPELINE))
	{
		const TiXmlElement *subnode=NULL;

		subnode=NULL;
		if(m_pBD) subnode = cnode->FirstChildElement(m_pBD->GetTypeName());
		if(subnode)   x7sVSModuleReadFromXML(m_pBD,subnode);

		subnode=NULL;
		if(m_pBT) subnode = cnode->FirstChildElement(m_pBT->GetTypeName());
		if(subnode) x7sVSModuleReadFromXML(m_pBT,subnode);

		subnode=NULL;
		if(m_pBTPP) subnode = cnode->FirstChildElement(m_pBTPP->GetTypeName());
		if(subnode) x7sVSModuleReadFromXML(m_pBTPP,subnode);

		subnode=NULL;
		if(m_pBTA) subnode = cnode->FirstChildElement(m_pBTA->GetTypeName());
		if(subnode) x7sVSModuleReadFromXML(m_pBTA,subnode);
	}

	return true;
}


bool  X7sVidSurvPipelineFG::WriteToXML(TiXmlNode *cnode,bool clear) const
{
	X7S_PRINT_DUMP("X7sVidSurvPipelineFG::WriteToXML()");

	//Call parent function.
	X7sXmlObject::WriteToXML(cnode,clear);

	//Then make the specific call for the children.
	if(X7S_XML_ISEQNODENAME(cnode,"vidsurv_pipeline"))
	{
		TiXmlElement *xmlelem=cnode->ToElement();

		//if(m_pFGD) 	x7sVSModuleWriteToXML(m_pFGD,xmlelem);
		if(m_pBD) 	x7sVSModuleWriteToXML(m_pBD,xmlelem);
		if(m_pBT) 	x7sVSModuleWriteToXML(m_pBT,xmlelem);
		if(m_pBTPP) x7sVSModuleWriteToXML(m_pBTPP,xmlelem);
		if(m_pBTA)	x7sVSModuleWriteToXML(m_pBTA,xmlelem);
	}

	return true;
}




//========================================================================
//-- Protected Methods.

IplImage* X7sVidSurvPipelineFG::ProcessFGD(IplImage* pImg, IplImage* pFG)
{
	/* Update BG model: */
	TIME_BEGIN()
					if(m_pFGD && pFG==NULL)
					{   /* No FG image is given therefore FG detector is needed: */
						m_pFGD->Process(pImg);	//Process image to update background model.
						pFG=m_pFGD->GetMask();	//Obtain the mask from the background
					}   /* If FG detector is needed. */
	TIME_END("FGDetector",-1)
	return pFG;
}



int X7sVidSurvPipelineFG::ProcessBT(IplImage* pImg, IplImage* pFG)
{
	/* Track blobs: */
	if(m_pBT)
	{
		TIME_BEGIN()
						int i;
		m_pBT->Process(pImg, pFG);

		for(i=m_BlobList.GetBlobNum(); i>0; --i)
		{   /* Update data of tracked blob list: */
			CvBlob* pB = m_BlobList.GetBlob(i-1);
			int     BlobID = CV_BLOB_ID(pB);
			int     i = m_pBT->GetBlobIndexByID(BlobID);
			m_pBT->ProcessBlob(i, pB, pImg, pFG);
			pB->ID = BlobID;
		}
		TIME_END("BlobTracker",m_pBT->GetBlobNum())

		if(m_PrevBlobNum != m_pBT->GetBlobNum())
		{
			X7S_PRINT_DEBUG("X7sVidSurvPipelineFG::ProcessBT()","CurBlobNumber %d",m_pBT->GetBlobNum());
		}
		m_PrevBlobNum=m_pBT->GetBlobNum();
		return m_PrevBlobNum;
	}
	return 0;
}

void X7sVidSurvPipelineFG::PostProcess(IplImage* pImg, IplImage* pFG,int CurBlobNum)
{
	TIME_BEGIN()
					if(m_pBTPP)
					{   /* Post-processing module: */
						int i;
						for(i=m_BlobList.GetBlobNum(); i>0; --i)
						{   /* Update tracked-blob list: */
							CvBlob* pB = m_BlobList.GetBlob(i-1);
							m_pBTPP->AddBlob(pB);
						}
						m_pBTPP->Process();

						for(i=m_BlobList.GetBlobNum(); i>0; --i)
						{   /* Update tracked-blob list: */
							CvBlob* pB = m_BlobList.GetBlob(i-1);
							int     BlobID = CV_BLOB_ID(pB);
							CvBlob* pBN = m_pBTPP->GetBlobByID(BlobID);

							if(pBN && m_UsePPData && pBN->w >= CV_BLOB_MINW && pBN->h >= CV_BLOB_MINH)
							{   /* Set new data for tracker: */
								m_pBT->SetBlobByID(BlobID, pBN );
							}

							if(pBN)
							{   /* Update blob list with results from postprocessing: */
								pB[0] = pBN[0];
							}
						}
					}   /* Post-processing module. */
	TIME_END("PostProcessing",CurBlobNum)
}

void X7sVidSurvPipelineFG::DeleteBlob(IplImage* pImg, IplImage* pFG)
{
	/* Blob deleter (experimental and simple): */
	TIME_BEGIN()
					if(pFG)
					{   /* Blob deleter: */
						int i;
						if(!m_BTReal)for(i=m_BlobList.GetBlobNum();i>0;--i)
						{   /* Check all blobs on list: */
							CvBlobTrackAuto* pB = (CvBlobTrackAuto*)(m_BlobList.GetBlob(i-1));
							int     Good = 0;
							int     w=pFG->width;
							int     h=pFG->height;
							CvRect  r = CV_BLOB_RECT(pB);
							CvMat   mat;
							double  aver = 0;
							double  area = CV_BLOB_WX(pB)*CV_BLOB_WY(pB);
							if(r.x < 0){r.width += r.x;r.x = 0;}
							if(r.y < 0){r.height += r.y;r.y = 0;}
							if(r.x+r.width>=w){r.width = w-r.x-1;}
							if(r.y+r.height>=h){r.height = h-r.y-1;}

							if(r.width > 4 && r.height > 4 && r.x < w && r.y < h &&
									r.x >=0 && r.y >=0 &&
									r.x+r.width < w && r.y+r.height < h && area > 0)
							{
								aver = cvSum(cvGetSubRect(pFG,&mat,r)).val[0] / area;
								/* if mask in blob area exists then its blob OK*/
								if(aver > 0.1*255)Good = 1;
							}
							else
							{
								pB->BadFrames+=2;
							}

							if(Good)
							{
								pB->BadFrames = 0;
							}
							else
							{
								pB->BadFrames++;
							}
						}   /* Next blob: */

						/* Check error count: */
						for(i=0; i<m_BlobList.GetBlobNum(); ++i)
						{
							CvBlobTrackAuto* pB = (CvBlobTrackAuto*)m_BlobList.GetBlob(i);

							if(pB->BadFrames>3)
							{   /* Delete such objects */
								/* from tracker...     */
								m_pBT->DelBlobByID(CV_BLOB_ID(pB));

								/* ... and from local list: */
								m_BlobList.DelBlob(i);
								i--;
							}
						}   /* Check error count for next blob. */
					}   /*  Blob deleter. */
	TIME_END("BlobDeleter",m_BlobList.GetBlobNum())
}

void X7sVidSurvPipelineFG::ProcessBD(IplImage* pImg, IplImage* pFG)
{
	/* Detect new blob: */
	TIME_BEGIN()
					if(!m_BTReal && m_pBD && pFG && (m_FrameCount > m_FGTrainFrames) )
					{   /* Detect new blob: */
						CvBlobSeq    NewBlobList;
						CvBlobTrackAuto     NewB;

						NewBlobList.Clear();

						if(m_pBD->DetectNewBlob(pImg, pFG, &NewBlobList, &m_BlobList))
						{   /* Add new blob to tracker and blob list: */
							int i;

							for(i=0; i<NewBlobList.GetBlobNum(); ++i)
							{
								CvBlob* pBN = NewBlobList.GetBlob(i);

								if(pBN && pBN->w >= CV_BLOB_MINW && pBN->h >= CV_BLOB_MINH)
								{
									pBN->ID = m_NextBlobID;

									CvBlob* pB = NULL;
									if(m_pBT) pB = m_pBT->AddBlob(pBN, pImg, m_pFGMask); //TOCHECK: Be carefull with this part.
									if(pB)
									{
										NewB.blob = pB[0];
										NewB.BadFrames = 0;
										m_BlobList.AddBlob((CvBlob*)&NewB);
										m_NextBlobID++;
									}
								}
							}   /* Add next blob from list of detected blob. */
						}   /* Create and add new blobs and trackers. */


						//		IplImage *pImg2 = cvCloneImage(pImg);
						//		BlobDetector::DrawCvBlobSeq(pImg2,&m_BlobList);
						//		BlobDetector::DrawCvBlobSeq(pImg2,&NewBlobList);
						//		cvShowImage("Blob New",pImg2);


					}   /*  Detect new blob. */
	TIME_END("BlobDetector",-1)
}


void X7sVidSurvPipelineFG::ProcessBTGen(IplImage* pImg, IplImage* pFG)
{
	TIME_BEGIN()
					if(m_pBTGen)
					{   /* Run track generator: */
						for(int i=m_BlobList.GetBlobNum(); i>0; --i)
						{   /* Update data of tracked blob list: */
							CvBlob* pB = m_BlobList.GetBlob(i-1);
							m_pBTGen->AddBlob(pB);
						}
						m_pBTGen->Process(pImg, pFG);
					}   /* Run track generator: */
	TIME_END("TrajectoryGenerator",-1)
}

void X7sVidSurvPipelineFG::ProcessBTAna(IplImage* pImg, IplImage* pFG)
{
	TIME_BEGIN()
					if(m_pBTA)
					{   /* Trajectory analysis module: */
						for(int i=m_BlobList.GetBlobNum(); i>0; i--)
							m_pBTA->AddBlob(m_BlobList.GetBlob(i-1));

						m_pBTA->Process(pImg, pFG);

					}   /* Trajectory analysis module. */
	TIME_END("TrackAnalysis",m_BlobList.GetBlobNum())
}


void X7sVidSurvPipelineFG::ProcessAlarm()
{
	bool ret;
	X7sBlobTrajectory *pTraj;
	if(m_pAlarmMan && m_pBTGen) {

		for(int i=0;i<m_pBTGen->GetBlobTrajNum();i++)
		{
			pTraj = m_pBTGen->GetBlobTraj(i);
			if(pTraj==NULL || pTraj->isHidden()) continue;
			ret=m_pAlarmMan->Process(m_FrameCount,pTraj);
		}
	}
	else {
		X7S_PRINT_ERROR("X7sVidSurvPipelineFG::ProcessAlarm()","m_pAlarmMan or m_pBTGen is NULL");
	}
}

//========================================================================
//-- Private Methods

/**
* @brief Create a children if we know which type of children is created.
* @param child The XML node that correspond to a child
* @return @true if one sub-module has been created, @false when the xml child does not correspond to a known sub-module.
*/
bool X7sVidSurvPipelineFG::CreateChildFromXML(const TiXmlElement *child)
{
	bool ret=true;
	bool ok;
	if(child==NULL)
	{
		X7S_PRINT_WARN("X7sVidSurvPipelineFG::CreateChildFromXML","child is NULL");
		ret=false;
	}
	else if(!m_pFGD && X7S_XML_ISEQNODENAME(child,IX7S_VS_TAGNAME_FGD))
	{
		m_pFGD = x7sCreateFGDetector(this,child);
	}
	else if(!m_pBD && X7S_XML_ISEQNODENAME(child,IX7S_VS_TAGNAME_BD))
	{
		int type=X7sXmlObject::ReadTypeFromXML(child,&ok);
		m_pBD = x7sCreateBlobDetector(type);
		x7sVSModuleReadFromXML(m_pBD,child);
	}
	else if(!m_pBT && X7S_XML_ISEQNODENAME(child,IX7S_VS_TAGNAME_BT))
	{
		int type=X7sXmlObject::ReadTypeFromXML(child,&ok);
		m_pBT= x7sCreateBlobTracker(type);
		x7sVSModuleReadFromXML(m_pBT,child);
	}
	else if(!m_pBTPP && X7S_XML_ISEQNODENAME(child,IX7S_VS_TAGNAME_BTPP))
	{
		int type=X7sXmlObject::ReadTypeFromXML(child,&ok);
		m_pBTPP= x7sCreateVSModBTPostProc(type);
		x7sVSModuleReadFromXML(m_pBTPP,child);
	}
	else
	{
		X7S_PRINT_DEBUG("X7sVidSurvPipelineFG::CreateChildFromXML","Unkown child %s",child->Value());
		ret=false;
	}
	return ret;
}

/**
* @brief  Create the default sub-module.
* @return always @true.
*/
bool X7sVidSurvPipelineFG::CreateDefaultSubModule()
{
	//Create the default submodules if there were not created
	if(m_pFGD==NULL)
	{
		m_pFGD = x7sCreateFGDetector(this);
	}
	if(m_pBD==NULL)
	{
		m_pBD = x7sCreateBlobDetector(X7S_VS_BD_CV);
		m_pBD->SetParam((x7scharA*)"minAreaRatio",0.012);
		m_pBD->SetParam((x7scharA*)"isDrawDebug",0);
		m_pBD->SetParam((x7scharA*)"distFilterStatic",3);

	}
	if(m_pBT==NULL)
	{
		m_pBT = x7sCreateBlobTracker(X7S_VS_BT_CC2);
		m_pBT->SetParam((x7scharA*)"AlphaSize",0.1);
		m_pBT->SetParam((x7scharA*)"Collision",0);
		m_pBT->SetParam((x7scharA*)"DebugWnd",0);
	}
	if(m_pBTPP==NULL)
	{
		m_pBTPP=x7sCreateVSModBTPostProc(X7S_VS_BTPP_KF);
	}

	return true;
}



//========================================================================
//-- Public Interfaces.

/**
* @brief Public interface to create a VidSurvPipeline.
* @param parent The parent X7sXmlObject.
* @return A pointer on the generic VidSurvPipeline class.
*/
X7sVidSurvPipeline* x7sCreateVidSurvPipelineFG(X7sXmlObject *parent)
				{
	return (X7sVidSurvPipeline*)new X7sVidSurvPipelineFG(parent);
				}


