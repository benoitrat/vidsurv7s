/**
 *  @file
 *  @brief Contains the class X7sVSPipeline.hpp
 *  @date 20-may-2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SVSPIPELINE_HPP_
#define X7SVSPIPELINE_HPP_

#include "ix7svidsurv.h"

/**
* @brief Our Pipeline with a mix between OpenCV module and our module to
* perform video surveillance and trajectory analysis.
* @ingroup x7svidsurv
*/
class X7sVidSurvPipelineFeatures : public X7sVidSurvPipeline {
public:
	X7sVidSurvPipelineFeatures(X7sXmlObject *parent);
	virtual ~X7sVidSurvPipelineFeatures();
	virtual void  Process(IplImage* pImg, IplImage* pMask = NULL);

protected:
	void ProcessFeatures(IplImage* pImg, IplImage* pFG);
	void ProcessBTGen(IplImage* pImg, IplImage* pFG);
	void ProcessAlarm();

private:
	virtual bool CreateChildFromXML(const TiXmlElement *child);

protected:
    /* The variable */
	  X7sFGDetector*          m_pFGD; 		//!< ForeGround Detector module.

	 CvBlobSeq			m_BlobList;			//!< The list of the blob tracked.
	 X7sConCompoList	*m_pConComList;		//!< Actual list of the connected component (It may be not used).
	 X7sFeaturesList	*m_pFeaturesList;	//!< Actual list of the connected component (It may be not used).
	 IplImage*			m_pImPrev;			//!< The previous image
	 int				m_FrameCount;		//!< Count the number of frame for each process.

    bool		m_segmentFGMask;		//!< If the function cvSegmentFGMask must be used
    int 		m_FGTrainFrames;		//!< Number of training frame to obtain a correct BG model.
    float		m_eigValThrs;			//!< Thresholds for the eigein value.
    int 		m_distMin;				//!< Maximum distance between two corner point.
    int 		m_maxCornerCount;		//!< Maximum number of corner point


	IplConvKernel *kernelMor5x5, *kernelMor3x3;

	bool m_drawBlob;
	bool m_drawAlarm;


};




#endif /* X7SVSPIPELINE_HPP_ */
