#include "ix7svidsurv.h"
#include "X7sVSLog.hpp"

/**
* @brief Constructor of the generic VidSurvPipeline.
*
* This constructor initialize the blob trajectory generator module and the alarm manager.
*
* @param parent
* @param type the type of vidsurv pipeline.
* @param ID The ID of the vidsurv pipeline
*/
X7sVidSurvPipeline::X7sVidSurvPipeline(X7sXmlObject *parent,int type,int ID)
: X7sXmlObject(parent,X7S_VS_TAGNAME_PIPELINE,true,type,ID)
{
	m_pBGImg=NULL;
	m_pFGMask=NULL;
	m_FGMaskDel=false;
	m_pNIMask=NULL;
	m_NIMaskDel=false;
	m_pBlobList=NULL;
	m_FrameCount=-1;

	m_pBTGen=new X7sBlobTrajGenerator(this);
	m_pAlarmMan=new X7sVSAlrManager(this);

	m_enable=true;
	m_dbgWnd=false;
	m_draw=true;

	X7sParam *prm;

	prm = m_params.Insert(X7sParam("enable",X7S_XML_ANYID,m_enable,X7STYPETAG_BOOL));
	prm->setComment("Enable the video surveillance module");
	prm->LinkParamValue(&m_enable);

	prm = m_params.Insert(X7sParam("dbgWnd",X7S_XML_ANYID,m_dbgWnd,X7STYPETAG_BOOL));
	prm->setComment("Display the debug windows to understand better the algorithm");
	prm->LinkParamValue(&m_dbgWnd);

	prm = m_params.Insert(X7sParam("draw",X7S_XML_ANYID,m_draw,X7STYPETAG_BOOL));
	prm->setComment("Draw the meta-data on the video frame");
	prm->LinkParamValue(&m_draw);
}

X7sVidSurvPipeline::~X7sVidSurvPipeline()
{
	//Delete images
	if(m_pFGMask && m_FGMaskDel) cvReleaseImage(&m_pFGMask);
	if(m_pNIMask && m_NIMaskDel) cvReleaseImage(&m_pNIMask);

	//Delete trajectory manager and alarm manager
	X7S_RELEASE_PTR(m_pBTGen);
	X7S_DELETE_PTR(m_pAlarmMan);
}

//! Return a pointer on a CvBlob using the index in the BlobList.
CvBlob* X7sVidSurvPipeline::GetBlob(int index)
{
	return m_pBlobList?m_pBlobList->GetBlob(index):NULL;
}

//! Return a pointer on a CvBLob which have a specific ID (Doesn't change over time)
CvBlob* X7sVidSurvPipeline::GetBlobByID(int ID)
{
	return m_pBlobList?m_pBlobList->GetBlobByID(ID):NULL;
}

//! Get the actual number of blob.
int X7sVidSurvPipeline::GetBlobNum()
{
	return m_pBlobList?m_pBlobList->GetBlobNum():0;
}

//! Obtain the approximation of the background (in BGR format)
IplImage* X7sVidSurvPipeline::GetBGImg()
{
	return m_pBGImg;
}

//! Return the foreground image in HUE format (use x7sCvtColor() to convert it with code X7S_CV_HNZ2BGR)
IplImage* X7sVidSurvPipeline::GetFGMask()
{
	return m_pFGMask;
}

//! Return the state of the trajectory analysis for the corresponding blob.
float X7sVidSurvPipeline::GetState(int BlobID)
{
	return 0;
};

//! Return a description of the state. @see GetState().
x7scharB* X7sVidSurvPipeline::GetStateDesc(int BlobID)
{
	return NULL;
}

//! Return a pointer on the alarm manager.
X7sVSAlrManager* X7sVidSurvPipeline::GetAlarmManager()
{
	return m_pAlarmMan;
};

//! Return a pointer on the alarm manager.
X7sBlobTrajGenerator* X7sVidSurvPipeline::GetTrajGenerator()
{
	return m_pBTGen;
};


void X7sVidSurvPipeline::SetNoInterest(IplImage *pImg)
{
	m_pNIMask=pImg;
}

//! Release the VidSurvPipeline (Call the destructor)
void X7sVidSurvPipeline::Release()
{
	delete this;
}

//! Draw the cvBlobSeq or the trajectory.
void X7sVidSurvPipeline::Draw(IplImage *pImg)
{

	if(pImg==NULL) return;
	if(m_pBTGen) m_pBTGen->Draw(pImg);
	if(m_pAlarmMan)	m_pAlarmMan->Draw(pImg);

}

//!< Process the alarm knowing the Blob Trajectory Generator
void X7sVidSurvPipeline::ProcessAlarm()
{
	bool ret;
	X7sBlobTrajectory *pTraj;
	if(m_pAlarmMan && m_pBTGen) {

		for(int i=0;i<m_pBTGen->GetBlobTrajNum();i++)
		{
			pTraj = m_pBTGen->GetBlobTraj(i);
			ret=m_pAlarmMan->Process(m_FrameCount,pTraj);
		}
	}
}


X7sVidSurvPipeline* x7sCreateVidSurvPipeline(X7sXmlObject* parent, int type) {

	//Set default value.
	if(X7S_CHECK_RANGE(type,0,X7S_VS_END_PIPELINE)==false) type=X7S_VS_PIPELINE_FG;

	switch(type)
	{
	case X7S_VS_PIPELINE_FG: return x7sCreateVidSurvPipelineFG(parent);
	case X7S_VS_PIPELINE_FEATURES: return x7sCreateVidSurvPipelineFeatures(parent);
	case X7S_VS_PIPELINE_FALLING: return x7sCreateVidSurvPipelineFalling(parent);
	case X7S_VS_PIPELINE_QUEUE: return x7sCreateVidSurvPipelineQueue(parent);
	case X7S_VS_PIPELINE_DESCRIPTOR:
	default:
		X7S_PRINT_WARN("x7sCreateVidSurvPipeline()","Type %d is not defined (loading FG)",type);
		return x7sCreateVidSurvPipelineFG(parent);
	}
}


X7sVidSurvPipeline* x7sCreateVidSurvPipeline(X7sXmlObject* parent,const TiXmlElement *cnode) {
	bool ok;
	int type=-1;
	X7sVidSurvPipeline* vsPipeline=NULL;


	//Read parameters from XML
	if(cnode) type=X7sXmlObject::ReadTypeFromXML(cnode,&ok);

	//Create the object
	if(ok)
	{
		vsPipeline=x7sCreateVidSurvPipeline(parent,type);
		vsPipeline->ReadFromXML(cnode);
	}
	else {
		X7S_PRINT_ERROR("x7sCreateX7sVSPipeline()", "Impossible to read type=%d",type);
	}
	return vsPipeline;
}
