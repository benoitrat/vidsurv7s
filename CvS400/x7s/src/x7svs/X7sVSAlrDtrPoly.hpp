/**
 *  @file
 *  @brief Contains the class X7sVSAlrDtrPoly.hpp
 *  @date Jul 13, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SVSALRPOLY_HPP_
#define X7SVSALRPOLY_HPP_

#include "ix7svidsurv.h"
#include <map>

/**
 *	@brief The X7sVSAlrDtrPoly.
 *	@ingroup x7svidsurv
 */
class X7sVSAlrDtrPoly: public X7sVSAlrDetector {
public:
	X7sVSAlrDtrPoly(X7sVSAlrManager *parent);
	virtual ~X7sVSAlrDtrPoly();
	virtual bool Process(int frame_id, const X7sBlobTrajectory *pTraj);
	virtual bool Process(int frame_id, const X7sVSAlarmEvt& evt);
	void Draw(IplImage *pImg,int hue=-1, int thickness=1);
	void DelTrajectory(int ID);
	void Reload();

protected:
	bool ResetPolygon();
	void PrintAlrEvt();

private:
	std::map<int,int> m_mapInside;	//!< Map<ID_blob,start_frame>
	CvMat *m_contour;
	//CvSeq *m_countour;

	CvPoint txt_pos;
	CvScalar color, color_bg, color_fill;
};

#endif /* X7SVSALRPOLY_HPP_ */
