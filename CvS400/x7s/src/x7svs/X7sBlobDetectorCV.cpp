#include "X7sBlobDetector.hpp"
//========================================================================
//-- Internal include to fasten compilation.
#include <highgui.h>
#include "X7sVSLog.hpp"

//========================================================================
//-- Constructor, Destructor and Init.

/**
* @brief Default constructor.
*
* Set the number of layer in the history to be IX7S_BDRL_NUM_FRAME.
*/
X7sBlobDetectorCV::X7sBlobDetectorCV()
{

	m_sizeBlobList = IX7S_BDRL_NUM_FRAME;
	m_pBlobLists = new CvBlobSeq*[m_sizeBlobList];
	for(int i=0;i< m_sizeBlobList; i++) m_pBlobLists[i]=NULL;
	m_pActualBlobList = m_pBlobLists[m_sizeBlobList-1];

	m_param_roi_scale = 1.5F;
	AddParam((x7scharA*)"ROIScale",&m_param_roi_scale);
	CommentParam((x7scharA*)"ROIScale", (x7scharB*)"Determines the size of search window around a blob");

	SetModuleName((x7scharA*)"BD_CV");

}

/**
* @brief Empty destructor.
*/
X7sBlobDetectorCV::~X7sBlobDetectorCV() {

}


//========================================================================
//-- Public Methods.



/**
* @brief Detect the new blob
* @param pImg The unused image.
* @param pFGMask The foreground mask (Binary or with the ID in grey)
* @param pNewBlobList A pointer on the list where new blob will be add.
* @param pOldBlobList A pointer on the actual (old) blob list where the position of the blob.
* @note This blob detector keeps an history for the last IX7S_BDRL_NUM_FRAME
*
*	The Blob Detector works like cvCreateBlobDetectorCC:
*		- If full, shift the queue history of CvBlobSeq, and create a new one (like <<).
*		- Find CC and add new blobs to blob list
*			-# Fill a new CvBlobSeq with the pFGMask (Draw in RED).
*			-# Delete Small Blob.
*			-# Delete intersected blob (using old List).
*			-# Bubble Sort Blob by their Area (using width and height).
*			-# Finally, copy only the first IX7S_BDRL_NUMMAX_NEWBLOB to the queue
*			history of CvBlobSeq (using the last position in the history).
*		- Analyze blob history to find best blob trajectory:
*			-# Then Add blob by blob, end delete its trajectory from history.
*/
int X7sBlobDetectorCV::DetectNewBlob(IplImage* /*pImg*/, IplImage* pFGMask, CvBlobSeq* pNewBlobList, CvBlobSeq* pOldBlobList)
{
	CvSize S = cvGetSize(pFGMask);	//Size of the FG image.
	IplImage* pTmp = NULL;
	bool tmpDel=true;

	if(m_isDrawDebug)
	{
		cvNamedWindow(GetModuleName());
		pTmp = cvCreateImage(cvGetSize(pFGMask),IPL_DEPTH_8U,3);	//3 Channels to draw the image
		cvCvtColor(pFGMask,pTmp,CV_GRAY2BGR);
		cvThreshold(pTmp,pTmp,0,255,CV_THRESH_BINARY);
	}


	//Shift Blob List
	this->ShiftQueueHistoryBlobList();

	//Find the CC and add them to the temporary blob list.
	CvBlobSeq       Blobs;
	this->GetNewBlobList(pFGMask,&Blobs);

	//Blob draw in red are the blob detected directly from foreground.
	if(m_isDrawDebug) {
		x7sDrawCvBlobSeq(pTmp,&Blobs,IX7S_RED);
		cvShowImage(GetModuleName(),pTmp);
		cvWaitKey(2);
	}

	//Filter small and intersected (WARNING: As we Delete() element, the loop should start by the end)
	for(int i=Blobs.GetBlobNum()-1; i>=0; --i)
	{
		CvBlob* pB = Blobs.GetBlob(i);

		//Filter small one.
		if(m_minRatio > 0.f && IsBlobTooSmall(pB,S,m_minRatio))
		{
			Blobs.DelBlob(i);
			continue;	//Go to the next blob.
		}

		//Filter intersect one.
		if(IsBlobIntersect(pB,pOldBlobList)) {
			Blobs.DelBlob(i);
		}
	}

	//Filter the N first blob (by size area)
	SetNBestRecentBlob(&Blobs,IX7S_BDRL_NUMMAX_NEWBLOB);

	//Blob in magenta the blob that have been filtered.
	if(m_isDrawDebug) x7sDrawCvBlobSeq(pTmp,m_pBlobLists[m_sizeBlobList-1],IX7S_MAGENTA);


	//=================================================
	FindBestTraj(pFGMask, pNewBlobList,pOldBlobList);

	if(m_isDrawDebug)
	{
		x7sDrawCvBlobSeq(pTmp,pOldBlobList,IX7S_YELLOW,false);	//Yellow are blob tracked
		x7sDrawCvBlobSeq(pTmp,pNewBlobList,IX7S_GREEN);		//Green are blob set as new.
		cvShowImage(GetModuleName(),pTmp);
		cvWaitKey(0);
		if(tmpDel && pTmp) cvReleaseImage(&pTmp);
	}

	return (pNewBlobList->GetBlobNum()>0);
}



/**
* @brief Create a blob detector with connected component retrieved and cluster merging..
*/
CvBlobDetector* x7sCreateBlobDetectorCV()
{
	return new X7sBlobDetectorCV();
}
