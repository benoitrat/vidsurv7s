/**
 *  @file
 *  @brief Contains the class X7sVSAlrDtrLine.h
 *  @date Jul 6, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SVSEVTCROSSLINE_H_
#define X7SVSEVTCROSSLINE_H_

#include <map>
#include "ix7svidsurv.h"

using std::map;

enum {
	X7S_VS_ALRLINE_DISP_NONE=0,
	X7S_VS_ALRLINE_DISP_IN,
	X7S_VS_ALRLINE_DISP_OUT,
	X7S_VS_ALRLINE_DISP_TOTAL,
	X7S_VS_ALRLINE_DISP_BOTH,
	X7S_VS_ALRLINE_DISP_ALL,
};

/**
 *	@brief The X7sVSAlrDtrLine.
 *	@ingroup x7svidsurv
 */
class X7sVSAlrDtrLine: public X7sVSAlrDetector {
public:
	X7sVSAlrDtrLine(X7sVSAlrManager *parent);
	virtual ~X7sVSAlrDtrLine();
	virtual bool Process(int frame_id, const X7sBlobTrajectory *pTraj);
	virtual bool Process(int frame_id, const X7sVSAlarmEvt& evt);
	void Draw(IplImage *pImg,int hue=-1, int thickness=1);
	void DelTrajectory(int ID);
	void Reload();
	virtual const X7sByteArray* GetData();
	virtual bool SetData(const X7sByteArray *pData);


protected:
	void ComputeLineEquation();

private:
	int m_lineType;		//!< Type of the line
	float m_slope;		//!< Slope of the line (y=m_slope*x+m_yIntercept)
	float m_yIntercept;	//!< Y-intercept of the line (y=m_slope*x+m_yIntercept)
	CvPoint2D32f m_P0,m_P1;

	int m_countType;	//!< Type of counter
	int m_countOut;		//!< Count persons who go from inside to outside.
	int m_countIn;		//!< Count persons who go from outside to inside

	//Drawing parameters
	CvPoint P,Q;
	CvScalar txt_color;
	CvScalar txt_bg;
	CvPoint txt_pos;


	map<int,bool> m_mapWasOutside;
};

#endif /* X7SVSEVTCROSSLINE_H_ */
