#include "X7sVSPipelineQueue.hpp"
//========================================================================
//-- Includes done in cpp file to accelerate compilation.
#include "X7sVSLog.hpp"
#include <vector>
#include <highgui.h>
#include <math.h>

#include "cv/Def.h"
#include "cv/Polygon.h"




//========================================================================
//-- Constructor and destructor.

/**
* @brief Our Pipeline with a mix between OpenCV module and our module to
* perform video surveillance and trajectory analysis.
* @param parent The parent X7sXmlObject.
*/
X7sVidSurvPipelineQueue::X7sVidSurvPipelineQueue(X7sXmlObject *parent)
:X7sVidSurvPipeline(parent,X7S_VS_PIPELINE_QUEUE,X7S_XML_ANYID),
 m_BlobList(sizeof(CvBlobTrackerAuto))
{
	X7S_PRINT_DEBUG("X7sVidSurvPipelineQueue::X7sVidSurvPipelineQueue()");

	m_FrameCount=0;
	m_pConComList = x7sCreateConCompoList(255);

	m_trainFrames=X7S_VS_NUMFRAMES_TRAIN;
	m_thrFG=200;
	m_drawPolys=1;
	m_cvtThermal=1;


	IX7S_VS_SETZEROS(m_iFG);



	//===========================
	// Setup the parameters
	X7sParam *prm;

	prm = m_params.Insert(X7sParam("nTrainFrames",X7S_XML_ANYID,m_trainFrames));
	prm->LinkParamValue(&m_trainFrames);
	prm->setComment("Number of frames to trains before obtaining a good model");

	prm = m_params.Insert(X7sParam("thresholdFG",X7S_XML_ANYID,m_thrFG,X7STYPETAG_U8));
	prm->LinkParamValue(&m_thrFG);
	prm->SetStep(10);
	prm->setComment("Image threshold for FG/BG discrimination");

	prm = m_params.Insert(X7sParam("drawPolys",X7S_XML_ANYID,m_drawPolys,X7STYPETAG_INT,0,3));
	prm->LinkParamValue(&m_drawPolys);
	prm->setComment("Draw polygons and subpolygons on the images");

	prm = m_params.Insert(X7sParam("cvtThermal",X7S_XML_ANYID,m_cvtThermal,X7STYPETAG_INT,0,3));
	prm->LinkParamValue(&m_cvtThermal);
	prm->setComment("Convert grey image to thermal color");

	prm = m_params.Insert(X7sParam("ptsPoly",X7S_XML_ANYID,"0 1 1 0",X7STYPETAG_STRVEC));
	prm->setComment("List of points for the polygons: 'x1 y1 x2 y2 x3 y3 ...'");

	prm = m_params.Insert(X7sParam("ptsQVec",X7S_XML_ANYID,"0 1 1 0",X7STYPETAG_STRVEC));
	prm->setComment("The point to define the queue direction vector: 'x1 y1 x2 y2'");


	m_pFGD = x7sCreateFGDetector(this);	//!Create the default FG detector.
}

/**
* @brief Delete all the thing we have created in the destructor.
*/
X7sVidSurvPipelineQueue::~X7sVidSurvPipelineQueue()
{
	X7S_PRINT_DEBUG("X7sVidSurvPipelineQueue::~X7sVidSurvPipelineQueue()");

	// Delete submodules (X7S).
	if(m_pFGD) m_pFGD->Release();
}

//========================================================================
//-- Public Methods.

void X7sVidSurvPipelineQueue::Process(IplImage* pImg, IplImage* pFG)
{
	cv::Mat src;
	cv::Mat fg;

	if(pImg) src= cv::Mat(pImg);
	if(pFG) fg=cv::Mat(pFG);

	this->process(src,fg);
}

void X7sVidSurvPipelineQueue::process(const cv::Mat& src, const cv::Mat& fg)
{
	X7S_FUNCNAME("X7sVidSurvPipelineQueue::Process()");


	if(m_enable)
	{

		m_FrameCount++;
		if(m_FrameCount%10==0) X7S_PRINT_DUMP(funcName,"#%04d nBlobs=%d",m_FrameCount,m_BlobList.GetBlobNum());
		if(m_FrameCount<m_trainFrames)
		{
			for(size_t i=0;i<m_imSubMask.size();i++) m_areasSmoothed[i]=0;
		}
		else if(m_FrameCount==m_trainFrames)
		{
			for(size_t i=0;i<m_imSubMask.size();i++) m_areasSmoothed[i]=m_areasActual[i];
			X7S_PRINT_DEBUG(funcName,"Training is finished");
		}



		//Check argument
		X7S_CHECK_ERROR(funcName,!src.empty(),,"src image is empty");

		cv::Mat imGrey, imDiff, imTmp;


		//Start processing
		cv::cvtColor(src,imGrey,CV_BGR2GRAY);
		cv::threshold(imGrey,m_imThr,m_thrFG,255,cv::THRESH_BINARY);
		//cv::adaptiveThreshold(m_imThr,m_imThr,255,cv::ADAPTIVE_THRESH_MEAN_C,cv::THRESH_BINARY,7,10);

		if(m_imMask.empty()) fillMask();
		else {
			m_imThr &= m_imMask;
			cv::Mat sEle = cv::getStructuringElement(cv::MORPH_RECT,cv::Size(5,5));
			cv::dilate(m_imThr,m_imThr,sEle,cv::Point(-1,-1),3);

			for(size_t i=0;i<m_imSubMask.size();i++)
			{
				cv::bitwise_and(m_imSubMask[i],m_imThr,imTmp);
				m_areasActual[i] = 0.9f*m_areasActual[i] + 0.1f*(float)cv::countNonZero(imTmp)/m_areasTotal[i];
				m_areasSmoothed[i]=0.995f*m_areasSmoothed[i]+0.005f*m_areasActual[i];
			}
		}


		///==========================================
#ifdef DEBUG
		if(X7sVSLog::IsDisplay(X7S_LOGLEVEL_HIGHEST))
		{
			for(size_t i=0;i<m_imSubMask.size();i++)
			{
				printf("A[%02d]=%05.2f  ",i,m_areasActual[i]*100);
			}
			printf("\n");
		}
#endif
		///===========================================



		//Assignation at the end of the processing
		m_iFG=m_imThr;
		m_imLast=imGrey;



		//cv::imshow("heat2",heatImage);














	}
}


void X7sVidSurvPipelineQueue::Draw(IplImage *pImg)
{
	if(pImg)
	{
		cv::Mat im = cv::Mat(pImg,false);
		cv::vector<cv::Mat> planes;
		cv::split(im,planes);
		planes[0].convertTo(planes[0],-1,256./(256.-32.),-32);
		IplImage iplSrc = planes[0];
		cv::Scalar col=cv::Scalar::all(255);

		if(m_cvtThermal)
		{
			x7sCvtColor(&iplSrc,pImg,X7S_CV_HEAT2BGR+(m_cvtThermal-1));
		}

		if(m_drawPolys)
		{
			m_poly.draw(im,col,2);

			if(m_drawPolys==2) m_qVec.draw(im,cv::Scalar::all(255));
			if(m_drawPolys >2)
			{
				for(size_t i=0;i<m_subPolys.size();i++)
				{
					m_subPolys[i].draw(im,col,1);
				}
			}
		}

		if(m_dbgWnd)
		{
			cv::Mat imCpy;
			im.copyTo(imCpy);
			float ocAct =estimateOccupency(m_areasActual);
			x7s::drawJauge(imCpy,ocAct,x7s::LEFT);
			cv::imshow("actual",imCpy);
		}

		float ocAvg =estimateOccupency(m_areasSmoothed);
		cv::Rect jRect = x7s::drawJauge(im,ocAvg,x7s::RIGHT);

		cv::Point pt=jRect.tl();
		cv::Point shiftText(-33,7);
		cv::Point shiftTickA(-5,3);
		cv::Point shiftTickB(0,3);

		cv::line(im,pt+shiftTickA,pt+shiftTickB,col,2);
		cv::putText(im,"5m",pt+shiftText,cv::FONT_HERSHEY_PLAIN,1,col);

		//Jump to next position
		pt.y+=cvRound(jRect.height*((5-3)/5.));
		cv::line(im,pt+shiftTickA,pt+shiftTickB,col,2);
		cv::putText(im,"3m",pt+shiftText,cv::FONT_HERSHEY_PLAIN,1,col);

	}
}


bool  X7sVidSurvPipelineQueue::CreateChildFromXML(const TiXmlElement *child)
{

	if(child && X7S_XML_ISEQNODENAME(child,m_pFGD->GetTagName()))
	{
		if(m_pFGD) m_pFGD->Release();
		m_pFGD = x7sCreateFGDetector(this,child->ToElement());
		return  true;
	}
	return false;
}

bool X7sVidSurvPipelineQueue::Apply(int depth)
{
	//Call the parent class
	bool ret=X7sXmlObject::Apply(depth);

	//Then apply the contour
	m_poly = x7s::Polygon(m_params["ptsPoly"]);
	m_qVec = x7s::Polygon(m_params["ptsQVec"]);

	if(m_poly.isValid() && m_qVec.isValid())
	{
		m_poly.split(m_qVec.getPoint(1),m_qVec.getPoint(0),m_subPolys);

		m_areasTotal = std::vector<float>(m_subPolys.size(),0);
		m_areasActual = std::vector<float>(m_subPolys.size(),0.0);
		m_areasSmoothed = std::vector<float>(m_subPolys.size(),-1);


	}
	else return false;

	return ret;
}



//========================================================================
//-- Private and Protected Methods.


float X7sVidSurvPipelineQueue::estimateOccupency(const std::vector<float>& areasPct)
{
	float valMin=0.3f;
	float sum=0;
	int nBads=0;
	for(size_t i=0;i<areasPct.size() && nBads < 2;i++)
	{
		if(areasPct[i]>valMin)
		{
			valMin=0.7f*areasPct[i]; //Next value should be removed;
		}
		else
		{
			nBads++;
		}
		sum+=	areasPct[i];

	}
	sum/=areasPct.size();
	sum*=1.3f;	//Boosting a little bit.
	return X7S_INRANGE(sum,0,1);


}


void X7sVidSurvPipelineQueue::fillMask()
{
	if(!m_poly.empty() && !m_imThr.empty())
	{

		//Reset the matrix to zeros
		m_imMask = cv::Mat::zeros(m_imThr.size(),m_imThr.type());
		m_poly.draw(m_imMask,cv::Scalar::all(255),CV_FILLED);

	}

	m_imSubMask = cv::vector<cv::Mat>(10,cv::Mat());

	for(size_t i=0;i<m_subPolys.size();i++)
	{
		m_imSubMask[i]=cv::Mat::zeros(m_imThr.size(),m_imThr.type());
		m_subPolys[i].draw(m_imSubMask[i],cv::Scalar::all(255),CV_FILLED);
		m_areasTotal[i] = (float)cv::countNonZero(m_imSubMask[i]);
	}
}



void X7sVidSurvPipelineQueue::ProcessBTGen(IplImage* pImg, IplImage* pFG)
{

}






//========================================================================
//-- Public Interfaces.

/**
* @brief Public interface to create a X7sVidSurvPipelineQueue object.
* @param parent The parent X7sXmlObject.
* @return A pointer on the generic VidSurvPipeline class.
*/
X7sVidSurvPipeline* x7sCreateVidSurvPipelineQueue(X7sXmlObject *parent)
																																														{
	return (X7sVidSurvPipeline*)new X7sVidSurvPipelineQueue(parent);
																																														}


