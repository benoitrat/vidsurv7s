#include "X7sVSPipelineFalling.hpp"
//========================================================================
//-- Includes done in cpp file to accelerate compilation.
#include "X7sVSLog.hpp"
#include <vector>
#include <highgui.h>
#include <math.h>


/* Special extended blob structure for auto blob tracking: */
typedef struct CvBlobTrackAuto
{
	CvBlob  blob;
	int     BadFrames;
} CvBlobTrackAuto;



//========================================================================
//-- Constructor and destructor.

/**
* @brief Our Pipeline with a mix between OpenCV module and our module to
* perform video surveillance and trajectory analysis.
* @param parent The parent X7sXmlObject.
*/
X7sVidSurvPipelineFalling::X7sVidSurvPipelineFalling(X7sXmlObject *parent)
:X7sVidSurvPipeline(parent,X7S_VS_PIPELINE_FEATURES,X7S_XML_ANYID),
 m_BlobList(sizeof(CvBlobTrackerAuto))
 {
	X7S_PRINT_DEBUG("X7sVidSurvPipelineFalling::X7sVidSurvPipelineFalling()");

	m_FrameCount=0;
	m_pImPrev=NULL;
	m_pFGMask=NULL;
	m_pConComList = x7sCreateConCompoList(255);
	m_pFeaturesList = x7sCreateFeaturesList();
	m_FGTrainFrames=X7S_VS_NUMFRAMES_TRAIN;


	m_params.Insert(X7sParam("segmentFGMask",X7S_XML_ANYID,true,X7STYPETAG_BOOL));
	m_params.Insert(X7sParam("nFGTrainFrames",X7S_XML_ANYID,m_FGTrainFrames));
	m_params.Insert(X7sParam("eigValThrs",X7S_XML_ANYID,100,X7STYPETAG_FLOAT,0,1));
	m_params.Insert(X7sParam("distMin",X7S_XML_ANYID,20,X7STYPETAG_INT,5,50));
	m_params.Insert(X7sParam("maxCornerCount",X7S_XML_ANYID,50,X7STYPETAG_INT,0,200));

	m_params["segmentFGMask"]->LinkParamValue(&m_segmentFGMask);
	m_params["nFGTrainFrames"]->LinkParamValue(&m_FGTrainFrames);
	m_params["eigValThrs"]->LinkParamValue(&m_eigValThrs);
	m_params["distMin"]->LinkParamValue(&m_distMin);
	m_params["maxCornerCount"]->LinkParamValue(&m_maxCornerCount);

	m_pBTGen->SetParam("draw_stay", "%d", 2); 



	m_pFGD = x7sCreateFGDetector(this);	//!Create the default FG detector.

	//kernelDIFF
	kernelMor5x5 = cvCreateStructuringElementEx(5, 5, 0, 0, CV_SHAPE_ELLIPSE, NULL);
	kernelMor3x3 = cvCreateStructuringElementEx(3, 3, 0, 0, CV_SHAPE_ELLIPSE, NULL);

	max_points = 30;
	ID = 0;
	cont_missing = 0;
	coordTracker = (coordenadas_xy *)calloc(max_points, sizeof(coordenadas_xy));
	num_valid = 0;
 }

/**
* @brief Delete all the thing we have created in the destructor.
*/
X7sVidSurvPipelineFalling::~X7sVidSurvPipelineFalling()
{
	X7S_PRINT_DEBUG("X7sVidSurvPipelineFalling::~X7sVidSurvPipelineFalling()");

	// Delete submodules (X7S).
	if(m_pFGD) m_pFGD->Release();
	if(m_pBTGen) m_pBTGen->Release();
	if(m_pAlarmMan) delete m_pAlarmMan;

	//Delete images
	if(m_pImPrev) cvReleaseImage(&m_pImPrev);


	if(m_pFGMask) cvReleaseImage(&m_pFGMask);

	if(m_pFeaturesList) free(m_pFeaturesList);
}

//========================================================================
//-- Public Methods.

void X7sVidSurvPipelineFalling::Process(IplImage* pImg, IplImage* pFG)
{
	X7S_FUNCNAME("X7sVidSurvPipelineFalling::Process()");


	if(m_params["enable"]->toIntValue())
	{

		/* Bump frame counter: */
		m_FrameCount++;
		if(m_FrameCount%10==0)
			X7S_PRINT_DUMP("X7sVidSurvPipelineFalling::Process()","#%04d nBlobs=%d",	m_FrameCount,m_BlobList.GetBlobNum());

		//-------------------------------------------------------------------------------------
		//Obtain the foreground

		X7S_CHECK_ERROR(funcName,pImg,,"pImg is NULL");

		if(m_FrameCount==1){
			m_grey = cvCreateImage(cvGetSize(pImg),8,1);
			m_prev_grey = cvCreateImage(cvGetSize(pImg),8,1);
			m_diff = cvCreateImage(cvGetSize(pImg),8,1);
			m_diff2 = cvCreateImage(cvGetSize(pImg),8,1);
			m_diff3 = cvCreateImage(cvGetSize(pImg),8,1);
		}


		//Compute the difference image
		if(m_pFGD)
		{

			/*x7sCvtColorRealloc(pImg,&m_grey,CV_BGR2GRAY);
			x7sCvtColorRealloc(pImg,&m_prev_grey,CV_BGR2GRAY);
			x7sCvtColorRealloc(pImg,&m_diff,CV_BGR2GRAY);
			x7sCvtColorRealloc(pImg,&m_diff2,CV_BGR2GRAY);*/


			//DIFF
			cvCvtColor(pImg, m_grey, CV_BGR2GRAY);
			cvAbsDiff(m_grey, m_prev_grey, m_diff);
			cvThreshold(m_diff, m_diff2, 5, 255, CV_THRESH_BINARY);
			cvErode(m_diff2, m_diff2, kernelMor3x3, 1);
			cvDilate(m_diff2, m_diff2, kernelMor5x5, 1);
			cvDilate(m_diff2, m_diff2, kernelMor5x5, 1);
			cvAnd(m_pNIMask, m_diff2, m_diff3, 0 );

			if(m_pFGMask) cvReleaseImage(&m_pFGMask);
			m_pFGMask = cvCloneImage(m_diff3);

			if(m_prev_grey)
				cvReleaseImage(&m_prev_grey);
			m_prev_grey = cvCloneImage(m_grey);


		}   /* If FG detector is needed. */
		else{
			m_pFGMask = NULL; 
		}



		//-------------------------------------------------------------------------------------
		//Obtain the connected component
		X7S_CHECK_ERROR(funcName,m_pFGMask,,"m_pFGMask is NULL or could not be proceed");
		//Update the list of connected component
		IplImage *pImCC = cvCloneImage(m_pFGMask);
		x7sUpdateConCompoList(m_pConComList,pImCC,false);

		if(m_pConComList->size > 0){
			if(num_valid == 0){
				ID++;			
			}
			int index = num_valid % max_points; 
			coordTracker[index].pointxy.x = (float)(m_pConComList->vec[0].x);
			coordTracker[index].pointxy.y = (float)(m_pConComList->vec[0].y);
			cont_missing = 0;
			num_valid++;
		}
		else{
			if(cont_missing > 4){
				num_valid = 0;
			}
			else{
				cont_missing++;
			}
		}

		for(int j = 0; j < m_pConComList->size; j++){
			min.x = m_pConComList->vec[j].min_x;
			min.y = m_pConComList->vec[j].min_y;
			max.x = m_pConComList->vec[j].max_x+15;
			max.y = m_pConComList->vec[j].max_y+15;
			cvRectangle(pImg, min, max, cvScalar(0,0,255), 2, 8, 0);		
		}

		if(num_valid > max_points){
			for(int k = 0; k < max_points; k++){
				int index = num_valid%max_points;
				if(k < max_points - 1 && index-1 != k)
					cvLine(pImg, cvPointFrom32f(coordTracker[k].pointxy), cvPointFrom32f(coordTracker[k+1].pointxy), CV_RGB(0,255,0), 2, 8, 0);
			}
		}
		else{
			for(int k = 0; k < num_valid; k++){
				if(k < num_valid - 1)
					cvLine(pImg, cvPointFrom32f(coordTracker[k].pointxy), cvPointFrom32f(coordTracker[k+1].pointxy), CV_RGB(0,255,0), 2, 8, 0);
			}
		}



		//-------------------------------------------------------------------------------------

		if(m_FrameCount>m_FGTrainFrames)
		{
			IplImage *pImgClone = cvCloneImage(pImg);

			if(m_dbgWnd)
			{
				cvNamedWindow("Features");
				x7sShowImage("Features", pImgClone);
			}
			cvReleaseImage(&pImgClone);

		}


		if(m_dbgWnd)
		{
			x7sDrawConCompoList(pImCC,m_pConComList);
			X7S_CV_SET_CMODEL(pImCC,X7S_CV_HNZ2BGRLEGEND);
			cvNamedWindow("CC");
			x7sShowImage("CC", pImCC);
		}

		cvReleaseImage(&pImCC);
		pImCC=NULL;


		//-------------------------------------------------------------------------------------
		//Generate a trajectory from the blobList history.
		//this->ProcessBTGen(pImg,m_pFGMask);

		//And finally process the events
		//this->ProcessAlarm();

		if(m_pImPrev) cvReleaseImage(&m_pImPrev);
		m_pImPrev=cvCloneImage(pImg);

	}
}


bool  X7sVidSurvPipelineFalling::CreateChildFromXML(const TiXmlElement *child)
{

	if(child && X7S_XML_ISEQNODENAME(child,m_pFGD->GetTagName()))
	{
		if(m_pFGD) m_pFGD->Release();
		m_pFGD = x7sCreateFGDetector(this,child->ToElement());
		return  true;
	}
	return false;
}



//========================================================================
//-- Private and Protected Methods.

void X7sVidSurvPipelineFalling::ProcessBTGen(IplImage* pImg, IplImage* pFG)
{
	if(m_pBTGen)
	{ 

		if(coordTracker && num_valid > 0)
		{
			CvPoint2D32f pos = coordTracker[num_valid%max_points].pointxy;

			CvBlob blob = cvBlob(pos.x,pos.y,20,20);
			blob.ID=ID;
			m_pBTGen->AddBlob(&blob);
		}
	}
}






//========================================================================
//-- Public Interfaces.

/**
* @brief Public interface to create a VidSurvPipeline.
* @param parent The parent X7sXmlObject.
* @return A pointer on the generic VidSurvPipeline class.
*/
X7sVidSurvPipeline* x7sCreateVidSurvPipelineFalling(X7sXmlObject *parent)
																								{
	return (X7sVidSurvPipeline*)new X7sVidSurvPipelineFalling(parent);
																								}


