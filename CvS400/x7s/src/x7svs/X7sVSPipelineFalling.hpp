/**
 *  @file
 *  @brief Contains the class X7sVSPipeline.hpp
 *  @date 20-may-2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SVSPIPELINE_HPP_
#define X7SVSPIPELINE_HPP_

#include "ix7svidsurv.h"

typedef struct coordenadas_xy{
	CvPoint2D32f pointxy;
} coordenadas_xy;

/**
* @brief Our Pipeline with a mix between OpenCV module and our module to
* perform video surveillance and trajectory analysis.
* @ingroup x7svidsurv
*/
class X7sVidSurvPipelineFalling : public X7sVidSurvPipeline {
public:
	X7sVidSurvPipelineFalling(X7sXmlObject *parent);
	virtual ~X7sVidSurvPipelineFalling();
	virtual void  Process(IplImage* pImg, IplImage* pMask = NULL);

protected:
	void ProcessBTGen(IplImage* pImg, IplImage* pFG);

private:
	virtual bool CreateChildFromXML(const TiXmlElement *child);

protected:
    /* The variable */
	  X7sFGDetector*          m_pFGD; 		//!< ForeGround Detector module.

	 CvBlobSeq			m_BlobList;			//!< The list of the blob tracked.
	 X7sConCompoList	*m_pConComList;		//!< Actual list of the connected component (It may be not used).
	 X7sFeaturesList	*m_pFeaturesList;	//!< Actual list of the connected component (It may be not used).
	 IplImage*			m_pImPrev;			//!< The previous image

	IplImage *m_grey, *m_prev_grey, *m_diff, *m_diff2, *m_diff3;
	CvPoint min, max;

	coordenadas_xy *coordTracker;
	int num_valid;
	int max_points;
	int ID;
	int cont_missing;

    bool		m_segmentFGMask;		//!< If the function cvSegmentFGMask must be used
    int 		m_FGTrainFrames;		//!< Number of training frame to obtain a correct BG model.
    float		m_eigValThrs;			//!< Thresholds for the eigein value.
    int 		m_distMin;				//!< Maximum distance between two corner point.
    int 		m_maxCornerCount;		//!< Maximum number of corner point


	IplConvKernel *kernelMor5x5, *kernelMor3x3;

};




#endif /* X7SVSPIPELINE_HPP_ */
