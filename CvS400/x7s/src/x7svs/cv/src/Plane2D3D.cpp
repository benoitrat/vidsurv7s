#include "Plane2D3D.h"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include <cstdlib>
#include "math.h"
#include <time.h>
//#include <omp.h>
#define PI 3.14159265358979323846

#include "ix7svscv.h"



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
namespace x7s
{

//-----------------------------------------------------------
//Constructor por defecto
Plane2D3D::Plane2D3D(std::string Name){

    PlaneName=Name;

    CameraHeight=0.0;
    CameraAngle=0.0;
    FocalDistance=0.0;
    HorizonLine=0.0;
    Population=200;
    //------------------------
    // Defaults DE Factors
    //------------------------
    NumParameter=4;
    MaxIterations=800;
    MinError=500;
    MutationFactor=0.6;
    CrossFactor=0.4;
}
//-----------------------------------------------------------
//Destructor por defecto
Plane2D3D::~Plane2D3D()
{

}
//-----------------------------------------------------------
// Measurement Functions
//-----------------------------------------------------------
float Plane2D3D::GetDistance(int x,int y){

    float TanTheta=tan(CameraAngle*PI/180.0);
    float SinTheta=sin(CameraAngle*PI/180.0);
    float CosTheta=cos(CameraAngle*PI/180.0);

    float PosY=(float)ImageHeight-y-(float)OpticalCenterY;
    float PosX=(float)x-(float)OpticalCenterX;

    float DistanceX=(CameraHeight*PosX)/(FocalDistance*SinTheta-PosY*CosTheta);
    float DistanceY=CameraHeight*((FocalDistance+PosY*TanTheta)/(FocalDistance*TanTheta-PosY));

    return sqrt(pow(DistanceX,2)+pow(DistanceY,2));

}
//-----------------------------------------------------------
float Plane2D3D::GetDistanceX(int x,int y){

    float SinTheta=sin(CameraAngle*PI/180.0);
    float CosTheta=cos(CameraAngle*PI/180.0);
    float PosY=(float)ImageHeight-y-(float)OpticalCenterY;
    float PosX=(float)x-(float)OpticalCenterX;

    return (CameraHeight*PosX)/(FocalDistance*SinTheta-PosY*CosTheta);

}
//-----------------------------------------------------------
float Plane2D3D::GetDistanceY(int y){

    float TanTheta=tan(CameraAngle*PI/180.0);
    float PosY=(float)ImageHeight-y-(float)OpticalCenterY;

    return CameraHeight*((FocalDistance+PosY*TanTheta)/(FocalDistance*TanTheta-PosY));

}
//-----------------------------------------------------------
float Plane2D3D::GetSegmentLength(int x1,int y1,int x2,int y2){

    float TanTheta=tan(CameraAngle*PI/180.0);
    float SinTheta=sin(CameraAngle*PI/180.0);
    float CosTheta=cos(CameraAngle*PI/180.0);

    float PosX1=(float)x1-(float)OpticalCenterX;
    float PosX2=(float)x2-(float)OpticalCenterX;
    float PosY1=(float)ImageHeight-(float)y1-(float)OpticalCenterY;
    float PosY2=(float)ImageHeight-(float)y2-(float)OpticalCenterY;
    float DistanceX1=(CameraHeight*PosX1)/(FocalDistance*SinTheta-PosY1*CosTheta);
    float DistanceX2=(CameraHeight*PosX2)/(FocalDistance*SinTheta-PosY2*CosTheta);
    float DistanceY1=CameraHeight*((FocalDistance+PosY1*TanTheta)/(FocalDistance*TanTheta-PosY1));
    float DistanceY2=CameraHeight*((FocalDistance+PosY2*TanTheta)/(FocalDistance*TanTheta-PosY2));

    return sqrt(pow(DistanceX1-DistanceX2,2)+pow(DistanceY1-DistanceY2,2));

}
//-----------------------------------------------------------
float Plane2D3D::GetDimension(int y,int length){

      float TanTheta=tan(CameraAngle*PI/180.0);
      float PosY=(float)ImageHeight-y-(float)OpticalCenterY;
      return CameraHeight*length*(FocalDistance+PosY*TanTheta)/(FocalDistance*(FocalDistance*TanTheta-PosY));
}
//-----------------------------------------------------------
// Calibration Functions
//-----------------------------------------------------------
void Plane2D3D::CalibrateSystem(float &MSE,float &ME,float CameraHeightAdded){

    float *BestIndividualAux=new float[NumParameter+1];
    float *BestIndividual=new float[NumParameter+1];
    float BestFitness;
    int NumIter=0;
    int NumIterMax=8;
    //-----------------------------------------------------------
    //Vector creation
    //-----------------------------------------------------------
    PopulationVector= new float*[Population];
    MutationVector = new float*[Population];
    CrossVector= new float*[Population];
    FitnessPopulationVector=new float[Population];
    FitnessCrossVector=new float[Population];

    for (int i=0;i<Population;i++){
         PopulationVector[i]= new float[NumParameter];
         MutationVector[i]= new float[NumParameter];
         CrossVector[i]= new float[NumParameter];
     }
    BU=new float[NumParameter];
    BL=new float[NumParameter];
    //--------------------------
    //Init BU & BL
    //--------------------------
    BU[0]=40000; //Camera Height
    BU[1]=40; //Camera Angle
    BU[2]=500;// Focal Distance
    BU[3]=ImageHeight;
    //--------------------------
    BL[0]=1000; //Camera Height
    BL[1]=10; //Camera Angle
    BL[2]=3000;// Focal Distance
    BL[3]=-2000.0;
    //-----------------------------------------------------------
    MaxIterations=4000;
    DE(3,CameraHeightAdded,BestIndividual);
    MaxIterations=800;

    while (BestIndividual[NumParameter]>MinError && NumIter<NumIterMax){
           DE(3,CameraHeightAdded,BestIndividualAux);
           if (BestIndividual[NumParameter]>BestIndividualAux[NumParameter]){
               for(int param=0;param<NumParameter+1;param++){
                   BestIndividual[param]=BestIndividualAux[param];
               }
           }
           NumIter++;
    }

    if (CameraHeightAdded==-1.0)
        CameraHeight=BestIndividual[0];
    else
        CameraHeight=CameraHeightAdded;

    CameraAngle=BestIndividual[1];
    FocalDistance=BestIndividual[2];
    MSE=0;
    ME=0;

    //---------------------------------------------------------------------------------------------
    //Mean Error Estimation
    //---------------------------------------------------------------------------------------------
    float PosX1,PosX2,PosY1,PosY2,Error;
    float PosY,DistanceX1,DistanceX2,DistanceY1,DistanceY2,EstimatedDistance,EstimatedRealHeight;
    float TanTheta=tan(CameraAngle*PI/180.0);
    float SinTheta=sin(CameraAngle*PI/180.0);
    float CosTheta=cos(CameraAngle*PI/180.0);
    for (int ii=0;ii<ObjectRealHeightVector.size();ii++){
         PosY=(float)ImageHeight-ObjectPositionVector.at(ii)-(float)OpticalCenterY;
          EstimatedRealHeight=CameraHeight*ObjectPixelHeightVector.at(ii)*(FocalDistance+PosY*TanTheta)/(FocalDistance*(FocalDistance*TanTheta-PosY));
         //float rr=ObjectRealHeightVector.at(ii);

         ME=ME+abs(EstimatedRealHeight-ObjectRealHeightVector.at(ii));
         MSE=MSE+pow(EstimatedRealHeight-ObjectRealHeightVector.at(ii),2);
    }
    for (int ii=0;ii<DistancesVector.size();ii++){

         PosX1=(float)DistancePositionsVector.at(ii)[0]-(float)OpticalCenterX;
         PosX2=(float)DistancePositionsVector.at(ii)[1]-(float)OpticalCenterX;
         PosY1=(float)ImageHeight-(float)DistancePositionsVector.at(ii)[2]-(float)OpticalCenterY;
         PosY2=(float)ImageHeight-(float)DistancePositionsVector.at(ii)[3]-(float)OpticalCenterY;
         DistanceX1=(CameraHeight*PosX1)/(FocalDistance*SinTheta-PosY1*CosTheta);
         DistanceX2=(CameraHeight*PosX2)/(FocalDistance*SinTheta-PosY2*CosTheta);
         DistanceY1=CameraHeight*((FocalDistance+PosY1*TanTheta)/(FocalDistance*TanTheta-PosY1));
         DistanceY2=CameraHeight*((FocalDistance+PosY2*TanTheta)/(FocalDistance*TanTheta-PosY2));
         EstimatedDistance=sqrt(pow(DistanceX1-DistanceX2,2)+pow(DistanceY1-DistanceY2,2));
         ME=ME+abs((EstimatedDistance)-DistancesVector.at(ii));
         MSE=MSE+pow((EstimatedDistance)-DistancesVector.at(ii),2);
    }

  ME=ME/(DistancesVector.size()+ObjectRealHeightVector.size());
  MSE=MSE/(DistancesVector.size()+ObjectRealHeightVector.size());
  //---------------------------------------------------------------------------------------------
  //Remove the allocated memory
  //---------------------------------------------------------------------------------------------
  delete BestIndividualAux;
  delete BestIndividual;

  for (int i=0;i<Population;i++){
      delete [] PopulationVector[i];
      delete [] MutationVector[i];
      delete [] CrossVector[i];
  }
  delete [] PopulationVector;
  delete [] MutationVector;
  delete [] CrossVector;

  delete FitnessPopulationVector;
  delete FitnessCrossVector;
  delete BU;
  delete BL;
  //---------------------------------------------------------------------------------------------
}
//-----------------------------------------------------------
void Plane2D3D::DE(int Type,float CameraHeightAdded,float *&BestIndividual){

    float Aux,MinValue,BestFitness;
    int Position,NumIter;
    double t = 0.0;
    float * BestIndividualAux=new float[NumParameter+1];
    NumIter=0;
//    clock_t start, stop;
//    start = clock();

    InitPopulation();
    CalculateFitness(FitnessPopulationVector,PopulationVector,CameraHeightAdded);
    FindBestFitness(FitnessPopulationVector,BestFitness,Position);

    for (int i=0;i<NumParameter;i++){
         BestIndividualAux[i]=PopulationVector[Position][i];
         BestIndividual[i]=PopulationVector[Position][i];
    }
    BestIndividualAux[NumParameter]=BestFitness;
    BestIndividual[NumParameter]=BestFitness;

    while (MaxIterations>NumIter && MinError<BestIndividualAux[NumParameter]){

           Mutation(Type,BestIndividualAux);
           Cross();

           CalculateFitness(FitnessPopulationVector,PopulationVector,CameraHeightAdded);
           CalculateFitness(FitnessCrossVector,CrossVector,CameraHeightAdded);

           Replace(BestIndividualAux);

           if (BestIndividualAux[NumParameter]<BestIndividual[NumParameter]){
               for (int i=0;i<NumParameter+1;i++){
                    BestIndividual[i]=BestIndividualAux[i];
               }

           }
           NumIter++;
    }

//    stop = clock();
//    t = (double) (stop-start)/CLOCKS_PER_SEC;
    BestIndividual[NumParameter]=BestIndividual[NumParameter]/(DistancesVector.size()+ObjectRealHeightVector.size());
    delete BestIndividualAux;
}
//-----------------------------------------------------------
void Plane2D3D::InitPopulation(){
    //#pragma omp for schedule(dynamic)
    for (int i=0;i<Population;i++){
        for(int j=0;j<NumParameter;j++){
            //BL(param)+ (BU(param)-BL(param)).*rand(1,1);
            PopulationVector[i][j]= BL[j]+(BU[j]-BL[j])*((float)rand()/(float)RAND_MAX);
        }
    }
}
//-----------------------------------------------------------
void Plane2D3D::CalculateFitness(float *&FitnessVector,float **DataVector,float CameraHeightAdded){

    float ACameraHeight,ACameraAngle,AFocalDistance,TanTheta,Error,PixelHeight,SinTheta,CosTheta;
    float PosX1,PosX2,PosY1,PosY2;
    float PosY,DistanceX1,DistanceX2,DistanceY1,DistanceY2,EstimatedDistance,EstimatedRealHeight;
    int i,ii;
    int maxVector;
    //Get the maxvector to create a balanced measurement of Fitness
    if(ObjectRealHeightVector.size()>DistancesVector.size()){
       maxVector=ObjectRealHeightVector.size();
    }
    else{
       maxVector=DistancesVector.size();
    }
    for(i=0;i<Population;i++){

        if (CameraHeightAdded==-1.0)
           ACameraHeight=DataVector[i][0];
        else
           ACameraHeight=CameraHeightAdded;

        ACameraAngle=DataVector[i][1];
        AFocalDistance=DataVector[i][2];
        TanTheta=tan(ACameraAngle*PI/180.0);
        SinTheta=sin(ACameraAngle*PI/180.0);
        CosTheta=cos(ACameraAngle*PI/180.0);
        Error=0;
        //int tam=ObjectRealHeightVector.size();
        // Height Error Estimation
        for (ii=0;ii<ObjectRealHeightVector.size();ii++){
             PosY=(float)ImageHeight-ObjectPositionVector.at(ii)-(float)OpticalCenterY;
             EstimatedRealHeight=(ACameraHeight*ObjectPixelHeightVector.at(ii)*(AFocalDistance+PosY*TanTheta))/(AFocalDistance*(AFocalDistance*TanTheta-PosY));
             Error=Error+pow(EstimatedRealHeight-ObjectRealHeightVector.at(ii),2)*(maxVector/ObjectRealHeightVector.size());
        }
        // Distance Error Estimation
        for (ii=0;ii<DistancesVector.size();ii++){

             PosX1=(float)DistancePositionsVector.at(ii)[0]-(float)OpticalCenterX;
             PosX2=(float)DistancePositionsVector.at(ii)[1]-(float)OpticalCenterX;
             PosY1=(float)ImageHeight-(float)DistancePositionsVector.at(ii)[2]-(float)OpticalCenterY;
             PosY2=(float)ImageHeight-(float)DistancePositionsVector.at(ii)[3]-(float)OpticalCenterY;
             DistanceX1=(ACameraHeight*PosX1)/(AFocalDistance*SinTheta-PosY1*CosTheta);
             DistanceX2=(ACameraHeight*PosX2)/(AFocalDistance*SinTheta-PosY2*CosTheta);
             DistanceY1=ACameraHeight*((AFocalDistance+PosY1*TanTheta)/(AFocalDistance*TanTheta-PosY1));
             DistanceY2=ACameraHeight*((AFocalDistance+PosY2*TanTheta)/(AFocalDistance*TanTheta-PosY2));
             EstimatedDistance=sqrt(pow(DistanceX1-DistanceX2,2)+pow(DistanceY1-DistanceY2,2));
             Error=Error+pow((EstimatedDistance)-DistancesVector.at(ii),2)*(maxVector/DistancesVector.size());

        }
        // Horizon Line Error Estimation
        Error=Error+pow((ImageHeight-(AFocalDistance*TanTheta+OpticalCenterY)-DataVector[i][3]),2)*maxVector;
        FitnessVector[i]=Error;

//        for ii=1:size(Distancias,2)
//             Y=AlturaCamara*(DistanciaFocal+((y(ii))*TanTheta))/(DistanciaFocal*TanTheta-(y(ii)));
//             Error=Error+(Y-Distancias(ii))^2;
//        end
//


    }
}
//-----------------------------------------------------------
void Plane2D3D::FindBestFitness(float *FitnessVector,float &MinValue,int &Position){

	MinValue=FLT_MAX;
    Position=-1;

    for(int i=0;i<Population;i++){

        if(FitnessVector[i]<MinValue){
           MinValue=FitnessVector[i];
           Position=i;
        }
    }
}
//-----------------------------------------------------------
void Plane2D3D::Mutation(int Type,float *BestIndividual){

     int a=0;
     int b=Population-1;
     int Parent2,Parent3,Parent4,Parent5;
     int i,param;
    // #pragma omp parallel for private(i,param)
     for (i=0;i<Population;i++){

         // Random parent selection
         Parent2=floor(a + (b-a)*((float)rand()/(float)RAND_MAX));
         while (Parent2==i){
                Parent2=floor(a + (b-a)*((float)rand()/(float)RAND_MAX));
         }
         Parent3=floor(a + (b-a)*((float)rand()/(float)RAND_MAX));
         while (Parent3==i || Parent3==Parent2){
                Parent3=floor(a + (b-a)*((float)rand()/(float)RAND_MAX));
         }
         if (Type==1){//DE/RAND/1
             for(param=0;param<NumParameter;param++){
                 MutationVector[i][param]=PopulationVector[i][param]+MutationFactor*(PopulationVector[Parent2][param]-PopulationVector[Parent3][param]);
             }
         }
         else if (Type==2){//DE/BEST/1
             for(param=0;param<NumParameter;param++){
                 MutationVector[i][param]=BestIndividual[param]+MutationFactor*(PopulationVector[i][param]-PopulationVector[Parent2][param]);
             }
         }
         else if (Type==3){ //DE/Current-To-Best/1

             for(param=0;param<NumParameter;param++){
                 MutationVector[i][param]=PopulationVector[i][param]+MutationFactor*(BestIndividual[param]-PopulationVector[i][param])+MutationFactor*(PopulationVector[Parent2][param]-PopulationVector[Parent3][param]);

             }
         }
         else if (Type==4){ //DE/rand/2
             Parent4=floor(a + (b-a)*((float)rand()/(float)RAND_MAX));
             Parent5=floor(a + (b-a)*((float)rand()/(float)RAND_MAX));
             for(param=0;param<NumParameter;param++){
                 MutationVector[i][param]=PopulationVector[i][param]+MutationFactor*(PopulationVector[Parent2][param]-PopulationVector[Parent3][param])+MutationFactor*(PopulationVector[Parent4][param]-PopulationVector[Parent5][param]);

             }
         }
     }
}
//-----------------------------------------------------------
void Plane2D3D::Cross(){

    float RandomValue;
    int i,param;
    //#pragma omp parallel for private(i,param)
    for (i=0;i<Population;i++){
         RandomValue=((float)rand()/(float)RAND_MAX);
         if (RandomValue<=CrossFactor){
             for(param=0;param<NumParameter;param++){
                 CrossVector[i][param]= MutationVector[i][param];
             }
         }
         else{
             for(param=0;param<NumParameter;param++){
                 CrossVector[i][param]= PopulationVector[i][param];
             }
         }

    }
}
//-----------------------------------------------------------
void Plane2D3D::Replace(float *&BestIndividualAux){
     int i,param;
     BestIndividualAux[NumParameter]=FitnessPopulationVector[0];
     
     for (i=0;i<Population;i++){
         if (FitnessPopulationVector[i]>FitnessCrossVector[i]){
             for(param=0;param<NumParameter;param++){
                  PopulationVector[i][param]=CrossVector[i][param];
             }
         }

         if (BestIndividualAux[NumParameter]>FitnessPopulationVector[i]){
             for(param=0;param<NumParameter;param++){
                  BestIndividualAux[param]=PopulationVector[i][param];
             }
             BestIndividualAux[NumParameter]=FitnessPopulationVector[i];
         }
         if (BestIndividualAux[NumParameter]>FitnessCrossVector[i]){
             for(param=0;param<NumParameter;param++){
                  BestIndividualAux[param]=CrossVector[i][param];
             }
             BestIndividualAux[NumParameter]=FitnessCrossVector[i];
         }
     }

}
//-----------------------------------------------------------
//Handle Camera Parameters Functions
//-----------------------------------------------------------
void Plane2D3D::SetCameraHeight(float Data){
     CameraHeight=Data;
}
//-----------------------------------------------------------
float Plane2D3D::GetCameraHeight(){
      return CameraHeight;
}
//-----------------------------------------------------------
void  Plane2D3D::SetCameraAngle(float Data){
      CameraAngle=Data;
}
//-----------------------------------------------------------
float Plane2D3D::GetCameraAngle(){

      return CameraAngle;
}
//-----------------------------------------------------------
void  Plane2D3D::SetFocalDistance(float Data){
      FocalDistance=Data;
}
//-----------------------------------------------------------
float Plane2D3D::GetFocalDistance(){
    return FocalDistance;
}
//-----------------------------------------------------------
void  Plane2D3D::SetHorizonLine(float Data){
      HorizonLine=Data;
}
//-----------------------------------------------------------
float Plane2D3D::GetHorizonLine(){
      return HorizonLine;
}
//-----------------------------------------------------------
float Plane2D3D::CalculateHorizonLine(){

      return  ImageHeight-(FocalDistance*tan(CameraAngle*PI/180.0)+OpticalCenterY);
}
//-----------------------------------------------------------
//Plane Propierties Functions
//-----------------------------------------------------------
void Plane2D3D::SetPlaneName(std::string Name){
     PlaneName=Name;
}
//-----------------------------------------------------------
std::string Plane2D3D::GetPlaneName(){
     return PlaneName;
}
//-----------------------------------------------------------
void Plane2D3D::SetImageProperties(int AImageHeight,int AImageWidth,int AOpticalCenterX,int AOpticalCenterY){

   ImageHeight=AImageHeight;
   ImageWidth=AImageWidth;
   OpticalCenterX=AOpticalCenterX;
   OpticalCenterY=AOpticalCenterY;
}
//-----------------------------------------------------------
void Plane2D3D::AddDistanceVector(int x1,int y1,int x2,int y2){

   DistancePositionsVector.push_back(cv::Vec4f(x1,x2,y1,y2));
}
//-----------------------------------------------------------


} //end namespace x7s
