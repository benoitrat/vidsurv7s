#include "cv/Scene2D3D.h"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Plane2D3D.h"



namespace x7s
{
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------


//-----------------------------------------------------------
//Constructor por defecto
//-----------------------------------------------------------
Scene2D3D::Scene2D3D(){

	this->clearAll();
}
//-----------------------------------------------------------
Scene2D3D::Scene2D3D(int IHeight,int IWidth){

	ImageHeight=IHeight;
	OpticalCenterY=IHeight/2;

	ImageWidth=IWidth;
	OpticalCenterX=IWidth/2;
	PlaneList.clear();
}

//-----------------------------------------------------------
//Destructor por defecto
//-----------------------------------------------------------
Scene2D3D::~Scene2D3D()
{
	this->clearAll();
}

void Scene2D3D::clearAll()
{
	//Delete all the children
	for (size_t i=0;i<PlaneList.size();i++)
	{
		X7S_DELETE_PTR(PlaneList[i]);
	}

	//Clear the pointers
	PlaneList.clear();
}



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

//-----------------------------------------------------------
int Scene2D3D::CreatePlane(std::string Name){

	if (FindPlane(Name)==-1){
		Plane2D3D * Plane=new Plane2D3D(Name);
		Plane->SetImageProperties(ImageHeight,ImageWidth,OpticalCenterX,OpticalCenterY);
		PlaneList.push_back(Plane);
		return FindPlane(Name);
	}
	else return -1;
}
//-----------------------------------------------------------
int Scene2D3D::FindPlane(std::string Name){

	for (size_t i=0;i<PlaneList.size();i++){

		if(Name.compare(PlaneList[i]->GetPlaneName())==0) return i;
	}
	return -1;
}
//-----------------------------------------------------------
void Scene2D3D::SetImageHeight(int IHeight){

	ImageHeight=IHeight;
	OpticalCenterY=IHeight/2;
}
//-----------------------------------------------------------
void Scene2D3D::SetImageWidth(int IWidth){

	ImageWidth=IWidth;
	OpticalCenterX=IWidth/2;
}
//-----------------------------------------------------------

} //end namespace x7s
