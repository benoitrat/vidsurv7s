
#include "Def.h"
#include "ix7svscv.h"

namespace x7s
{

#define IX7S_JAUGE_NSTEPS 30

cv::Rect drawJauge(cv::Mat& img, float value, x7s::Pos pos, float ratio, int thickness, int margin)
{

	X7S_FUNCNAME("drawJauge()");
	X7S_CHECK_WARN(funcName,!img.empty(),cv::Rect(),"Image is empty");
	X7S_CHECK_WARN(funcName,X7S_CHECK_RANGE(ratio,0,1.001),cv::Rect(),"Ratio is not between 0 and 1");


	cv::Point start;
	cv::Point end;
	cv::Point p1;
	cv::Point p2;
	int nSteps;
	cv::Point step;


	if(X7S_CV_TAG_IS(pos,x7s::HORI))
	{
		int x_offset = (int)(img.cols*(1-ratio))/2;
		int y_pos=0;
		start.x=x_offset;
		if(pos==TOP) y_pos= margin;
		else y_pos=img.rows-(margin+thickness);
		start.y=y_pos;
		end.y=y_pos+thickness;

		nSteps=cvFloor(IX7S_JAUGE_NSTEPS*value);
		step.x= cvRound(img.cols*ratio/(IX7S_JAUGE_NSTEPS-1));
		step.y=0;
		end.x=start.x+step.x*IX7S_JAUGE_NSTEPS;

		p1=start;
		p2=start+step;
		p2.y=end.y;

	}
	else
	{
		int y_offset = cvFloor((img.rows*(1-ratio))/2);
		int x_pos=0;
		start.y=img.rows-y_offset;
		if(pos==LEFT) x_pos= margin;
		else x_pos=img.cols-(margin+thickness);
		start.x=x_pos;
		end.x=x_pos+thickness;

		nSteps=cvFloor(IX7S_JAUGE_NSTEPS*value);
		step.x=0;
		step.y= -cvRound(img.rows*ratio/(IX7S_JAUGE_NSTEPS-1));
		end.y=start.y+step.y*IX7S_JAUGE_NSTEPS;

		p1=start;
		p2=start+step;
		p2.x=end.x;
	}

	cv::Scalar col;

	for(int i=0;i<nSteps;i++)
	{
		col = x7sCvtHue2BGRScalar(IX7S_JAUGE_NSTEPS-1-i,IX7S_JAUGE_NSTEPS,100);
		cv::rectangle(img,p1,p2,col,CV_FILLED);

		p1+=step;
		p2+=step;
	}




	cv::rectangle(img,start,end,cv::Scalar::all(255),2);
	return cv::Rect(start,end);



}


}
