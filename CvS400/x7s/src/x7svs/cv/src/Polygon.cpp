#include "cv/Polygon.h"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "ix7svscv.h"
#include "X7sVSLog.hpp"


namespace x7s
{
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
Polygon::Polygon(const cv::Mat& mx)
{
	X7S_FUNCNAME("Polygon::Polygon()");
	X7S_PRINT_DEBUG(funcName,"Copied");

	mx.copyTo(m_contour);
}


Polygon::Polygon(int nPts,cv::Point ptDef)
{
	X7S_FUNCNAME("Polygon::Polygon()");
	X7S_PRINT_DEBUG(funcName);

	m_contour=cv::Mat(nPts,1,CV_32SC2);
	m_contour.setTo(cv::Scalar(ptDef.x,ptDef.y,0,0));
}

Polygon::Polygon(const X7sParam* polyParam)
{
	X7S_FUNCNAME("Polygon::Polygon()");

	X7S_CHECK_WARN(funcName,polyParam,,"X7sParam=NULL");
	X7S_CHECK_WARN(funcName,!polyParam->IsNull(),,"X7sParam is NULL");


	int nVals = polyParam->GetLength();
	int nPts = cvFloor(nVals/2.0);

	if(nPts>=2) //At least it must be 2 points (4 values)
	{
		cv::Point2d tmp;

		//Create the main contour given the list of polygons
		m_contour=cv::Mat(nPts,1,CV_32SC2);
		for(int i=0;i<nPts;i++)
		{

			if(polyParam->GetRealValue(i*2,&tmp.x) &&
					polyParam->GetRealValue(i*2+1,&tmp.y))
			{
				m_contour.at< cv::Point >(i,0)= tmp;
			}
		}
	}
}


Polygon::Polygon(const std::vector< cv::Point >& vecPts)
{
	X7S_FUNCNAME("Polygon::Polygon()");

	X7S_CHECK_WARN(funcName,!vecPts.empty(),,"vecPts is empty");

	m_contour=cv::Mat(vecPts.size(),1,CV_32SC2);
	for(size_t i=0;i<vecPts.size();i++)
	{
		m_contour.at< cv::Point >(i,0)= vecPts[i];
	}
}


Polygon::~Polygon()
{
	X7S_FUNCNAME("Polygon::~Polygon()");
	X7S_PRINT_DEBUG(funcName);
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------
void Polygon::appendPoint(const cv::Point& pt)
{
	X7S_FUNCNAME("Polygon::addPoint()");
	X7S_PRINT_INFO(funcName,"not implemented!");
}


cv::Point Polygon::removePoint(int pos)
{
	X7S_FUNCNAME("Polygon::removePoint()");
	X7S_PRINT_INFO(funcName,"not implemented!");
	return cv::Point();
}


cv::Point Polygon::getPoint(int pos)
{
	X7S_FUNCNAME("Polygon::getPoint()");

	if(0<= pos && pos < length())
	{
		return m_contour.at< cv::Point >(pos,0);
	}
	else
	{
		X7S_PRINT_WARN(funcName,"pos (%d) is bigger than length (%d)",pos,length());
		return cv::Point();
	}
}



void Polygon::setPoint(int pos, const cv::Point& pt)
{
	X7S_FUNCNAME("Polygon::setPoint()");

	if(0<= pos && pos < length())
	{
		m_contour.at< cv::Point >(pos,0)=pt;
	}
	else
	{
		X7S_PRINT_WARN(funcName,"pos (%d) is bigger than length (%d)",pos,length());
	}
}


/**
 * @brief Draw the polygon on the image
 * @param img
 * @param color
 * @param thickness if CV_FILLED (fill the poly), otherwise draw a line of the given thickness
 */
void Polygon::draw(cv::Mat &img, cv::Scalar color, int thickness) const
{

	X7S_FUNCNAME("Polygon::draw()");
	X7S_CHECK_WARN(funcName,!(img.empty()),,"image is empty");

	int npts = 0;
	const cv::Point *pts;

	if(isValid())
	{
		npts=m_contour.rows;
		pts=(const cv::Point*)m_contour.data;

		if(thickness<0) 	cv::fillPoly(img,&pts,&npts,1,color);
		else 		cv::polylines(img,&pts,&npts,1,true,color,thickness);
	}

}




/**
*
* The pt1 is also the center of the rotation.
*
*
* @param poly
* @param pt1 Origin of the vector
* @param pt2
* @param polyRes
*/
void Polygon::split(const cv::Point& pt1, const cv::Point& pt2, std::vector<Polygon>& subPolys,int nSubPolys)
{

	X7S_FUNCNAME("Polygon::draw()");
	X7S_CHECK_WARN(funcName,isValid(),,"Polygon is not valid");
	X7S_CHECK_WARN(funcName,1 < nSubPolys && nSubPolys <100,,"nSubPolys is not in [2,100[");
	X7S_CHECK_WARN(funcName,pt1 != pt2,,"pt1 and pt2 are equals");



	Polygon polyR(this->mat());
	cv::Vec2f vecO((float)pt2.x-pt1.x,(float)pt2.y-pt1.y);
	cv::Vec2f vecR(0,1);

	double angle = vecO.ddot(vecR);


	cv::Mat rotMx =  getRotationMatrix2D(pt1, angle, 1);
	cv::transform(this->mat(),polyR.mat(),rotMx);


	cv::Rect bbox = polyR.getBoundingBox();

	cv::Mat raster(cv::Size(bbox.br()),CV_8UC1,cv::Scalar::all(0));
	cv::Mat mask(cv::Size(bbox.br()),CV_8UC1,cv::Scalar::all(0));
	polyR.draw(mask,cv::Scalar::all(255),CV_FILLED);


	cv::Point p1(bbox.tl().x,bbox.tl().y);
	cv::Point p2(bbox.br().x,bbox.tl().y);


	int areaT = cv::countNonZero(mask);
	int areaSub = areaT/nSubPolys;
	int sub=1;
	int areaA=0;

	//Find when the area complete a part of polygon
	for( int y = bbox.tl().y; y < bbox.br().y; y++ )
	{
		uchar* pRast = raster.ptr<uchar>(y);
		uchar* pMask = mask.ptr<uchar>(y);

		for( int x = bbox.tl().x; x < bbox.br().x; x++ )
		{
			if(pMask[x])
			{
				pRast[x]=255;
				areaA++;
			}
		}

		if(areaA > areaSub || y+1 >= bbox.br().y)
		{
			cv::vector<cv::vector< cv::Point > > contour;
			cv::findContours(raster,contour,CV_RETR_LIST,CV_CHAIN_APPROX_NONE);
			if(!contour.empty())
			{
				Polygon poly(contour.back());
				subPolys.push_back(poly);
			}
			else
			{
				X7S_PRINT_INFO(funcName,"No contour founded");
			}

			sub++;
			raster.setTo(cv::Scalar::all(0)); //Reset the images.
			areaA=0;	//Reset the area

		}
	}

	//Now we need to remove the TR and BL points, two compute the second BB

	//	cv::Scalar col;
	//	cv::Mat tmp(cv::Size(bbox.br()),CV_8UC3,cv::Scalar::all(0));
	//	int maxVal=polyRes.size()*3;
	//	int offsetVal=maxVal-1;
	//	int kVal=-3;
	//	for(size_t i=0;i<polyRes.size();i++)
	//	{
	//		col = x7sCvtHue2BGRScalar(offsetVal+kVal*i,maxVal,100);
	//		col[3]=0;
	//		polyRes[i].draw(tmp,col,CV_FILLED);
	//	}
	//	polyR.draw(tmp,cv::Scalar::all(255));
	//	cv::imshow("tmp",tmp);



	//Apply the inverse rotation to go back to original coordinate
	cv::Mat rotInvMx =  getRotationMatrix2D(pt1, -angle, 1);
	for(size_t i=0;i<subPolys.size();i++)
	{
		cv::transform(subPolys[i].mat(),subPolys[i].mat(),rotInvMx);
	}
}





//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

cv::Rect Polygon::getBoundingBox() const
{
	X7S_FUNCNAME("Polygon::getBoundingBox()");
	X7S_CHECK_WARN(funcName,isValid(),cv::Rect(),"not a valid polygon");

	cv::Point tl(INT_MAX,INT_MAX);
	cv::Point br(INT_MIN,INT_MIN);

	for(int i=0; i < this->length(); i++)
	{
		cv::Point pt = m_contour.at<cv::Point>(i,0);
		if(pt.x>br.x) br.x=pt.x;
		if(pt.y>br.y) br.y=pt.y;
		if(pt.x<tl.x) tl.x=pt.x;
		if(pt.y<tl.y) tl.y=pt.y;

		printf("%d,%d ; ", pt.x,pt.y);
	}
	printf("bb=%d,%d - %d,%d\n",tl.x,tl.y,br.x,br.y);


	return cv::Rect(br,tl);
}


/**
* @brief Get The Extreme point of the contour.
*
* You should look at @see findContours().
*
* @param type
* @return
*/
cv::Point Polygon::getExtremum(ExtremeType type) const
{
	cv::Point first,snd;
	X7S_FUNCNAME("Polygon::getExtremum()");


	if(type==TOP_LEFT || type == TOP_RIGHT)
	{
		cv::Point pt_1st=cv::Point(INT_MAX,INT_MAX);
		cv::Point pt_2nd=cv::Point(INT_MAX,INT_MAX);

		for(int i=0; i < this->length(); i++)
		{
			cv::Point pt = m_contour.at<cv::Point>(i,0);
			if(pt.y < pt_2nd.y)
			{
				if(pt.y < pt_1st.y)
				{
					pt_2nd=pt_1st; //Set the first to be the second.
					pt_1st=pt; //Set the actual as the first
				}
				else pt_2nd=pt;
			}
		}
		if(TOP_LEFT)
		{
			if(pt_1st.x < pt_2nd.x) return pt_1st;
			else return pt_2nd;
		}
		else
		{
			if(pt_1st.x > pt_2nd.x) return pt_1st;
			else return pt_2nd;
		}

	}
	else if(type==BOTTOM_LEFT || type == BOTTOM_RIGHT)
	{
		cv::Point pt_1st=cv::Point(INT_MIN,INT_MIN);
		cv::Point pt_2nd=cv::Point(INT_MIN,INT_MIN);

		for(int i=0; i < this->length(); i++)
		{
			cv::Point pt = m_contour.at<cv::Point>(i,0);
			if(pt.y > pt_2nd.y)
			{
				if(pt.y > pt_1st.y)
				{
					pt_2nd=pt_1st; //Set the first to be the second.
					pt_1st=pt; //Set the actual as the first
				}
				else pt_2nd=pt;
			}


		}
		if(BOTTOM_LEFT)
		{
			if(pt_1st.x < pt_2nd.x) return pt_1st;
			else return pt_2nd;
		}
		else
		{
			if(pt_1st.x > pt_2nd.x) return pt_1st;
			else return pt_2nd;
		}
	}
	else
	{
		X7S_PRINT_INFO(funcName,"not implemented for type=%d",type);
	}

	return cv::Point();
}





} //end namespace x7s
