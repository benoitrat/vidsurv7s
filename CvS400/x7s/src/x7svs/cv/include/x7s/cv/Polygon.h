/**
 *  @file
 *  @brief Contains the class Polygon.h
 *  @date Apr 26, 2010
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef POLYGON_H_
#define POLYGON_H_

#include "Def.h"

namespace x7s {



/**
 *	@brief The Polygon.
 *	@ingroup 
 */
class X7S_EXPORT Polygon {

public:
	enum ExtremeType {
		TOP_LEFT,
		TOP_RIGHT,
		BOTTOM_LEFT,
		BOTTOM_RIGHT,
		LEFT_TOP,
		LEFT_BOTTOM,
		RIGHT_TOP,
		RIGHT_BOTTOM,
	};


public:
	Polygon(const cv::Mat& mx);
	Polygon(int nPts=0,cv::Point ptDef=cv::Point(0,0));
	Polygon(const X7sParam* polyParam);
	Polygon(const std::vector< cv::Point >& vecPts);
	~Polygon();

	void appendPoint(const cv::Point& pt);
	cv::Point removePoint(int pos);
	cv::Point getPoint(int pos);
	void setPoint(int pos, const cv::Point& pt);
	void draw(cv::Mat &img, cv::Scalar color, int thickness=1) const;
	int length() const { return m_contour.rows; };
	bool empty() const { return m_contour.empty(); };
	bool isValid() const { return !(m_contour.empty()); };
	cv::Rect getBoundingBox() const;
	cv::Point getExtremum(ExtremeType type) const;

	//! Split the poly in n sub polygons of equal size
	void split(const cv::Point& pt1, const cv::Point& pt2, std::vector<Polygon>& subPolys, int nSubPolys=10);


	const cv::Mat& mat() const {return m_contour; }
	cv::Mat& mat()  {return m_contour; }


private:
	 cv::Mat			m_contour;		//!< Contour of the queue.
};


} //end namespace x7s


#endif /* POLYGON_H_ */
