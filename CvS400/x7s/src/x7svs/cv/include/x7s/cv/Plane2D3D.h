#ifndef TPLANE_H
#define TPLANE_H

#include "Def.h"
#include <vector>
#include <string>


namespace x7s {


class X7S_EXPORT Plane2D3D
{

private:
   //--------------------------------------------------
   //Image Properties
   //--------------------------------------------------
   int ImageHeight;
   int ImageWidth;
   int OpticalCenterY;
   int OpticalCenterX;
   //--------------------------------------------------
   //Plane Propierties
   //--------------------------------------------------
   std::string PlaneName;
   //--------------------------------------------------
   //Camera Parameters
   //--------------------------------------------------
   float CameraHeight;
   float CameraAngle;
   float FocalDistance;
   float HorizonLine;
   //--------------------------------------------------
   //Differential Evolution Parameters
   //--------------------------------------------------
   int Population;
   int MaxIterations;
   int MinError;
   int NumParameter;
   float ** PopulationVector;
   float ** MutationVector;
   float ** CrossVector;
   float *BU;
   float *BL;
   float *FitnessPopulationVector;
   float *FitnessCrossVector;
   float MutationFactor;
   float CrossFactor;
   //float *BestIndividual;

public:
   Plane2D3D(std::string Name=std::string());//Constructor

   ~Plane2D3D();//Destructor



   std::vector<float> ObjectRealHeightVector;
   std::vector<float> ObjectPixelHeightVector;
   std::vector<float> ObjectPositionVector;
   std::vector<cv::Vec4f> DistancePositionsVector;//Float[4] (X1,X2,Y1,Y2)
   std::vector<float> DistancesVector;
   //--------------------------------------------------
   //Camera Parameters Functions
   //--------------------------------------------------
   void  SetCameraHeight(float Data);
   float GetCameraHeight();
   void  SetCameraAngle(float Data);
   float GetCameraAngle();
   void  SetFocalDistance(float Data);
   float GetFocalDistance();
   //Esto es provisional.....
   void  SetHorizonLine(float Data);
   float GetHorizonLine();
   float CalculateHorizonLine();
   void AddDistanceVector(int x1,int y1,int x2,int y2);
   //--------------------------------------------------
   //Plane Propierties Functions
   //--------------------------------------------------
   void SetPlaneName(std::string Name);
   std::string GetPlaneName();
   //--------------------------------------------------
   //Measurement Functions
   //--------------------------------------------------
   float GetDistance(int x,int y);
   float GetDistanceX(int x,int y);
   float GetDistanceY(int y);
   float GetDimension(int y,int length);
   float GetSegmentLength(int x1,int y1,int x2,int y2);
   //--------------------------------------------------
   //Calibration Functions
   //--------------------------------------------------
   void CalibrateSystem(float &MSE,float &ME,float CameraHeightAdded);
   //--------------------------------------------------
public:

   //--------------------------------------------------
   //Image Properties Functions
   //--------------------------------------------------
   void SetImageProperties(int AImageHeight,int AImageWidth,int AOpticalCenterX,int AOpticalCenterY);

private:
    //--------------------------------------------------
    //Differential Evolution Functions
    //--------------------------------------------------
    void DE(int Type,float CameraHeightAdded,float *&BestIndividual);
    void InitPopulation();
    void CalculateFitness(float *&FitnessVector,float **DataVector,float CameraHeightAdded);
    void FindBestFitness(float *FitnessVector,float &MinValue,int &Position);
    void Mutation(int Type,float *BestIndividual);
    void Cross();
    void Replace(float *&BestIndividualAux);

};


}
#endif // TPLANE_H
