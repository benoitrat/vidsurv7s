/*
 * Scene2D3D.h
 *
 *  Created on: 05/05/2010
 *      Author: Ben
 */

#ifndef SCENE2D3D_H_
#define SCENE2D3D_H_

#include "Def.h"
#include <vector>
#include <string>

namespace x7s {


class Plane2D3D;	//Forward declaration

class X7S_EXPORT Scene2D3D
{
private:
     int ImageHeight;
     int ImageWidth;
     int OpticalCenterY;
     int OpticalCenterX;


protected:
     void clearAll();

public:
    std::vector< Plane2D3D* > PlaneList;
    Scene2D3D();//Constructor
    Scene2D3D(int IHeight,int IWidth);//Constructor
    ~Scene2D3D();//Destructor
    //---------------------------------
    // Plane Functions
    //---------------------------------
    int CreatePlane(std::string Name);
    int FindPlane(std::string Name);
    //---------------------------------
    // Variable functions
    //---------------------------------
    void SetImageHeight(int IHeight);
    void SetImageWidth(int IWidth);
    //---------------------------------
};

}

#endif /* SCENE2D3D_H_ */
