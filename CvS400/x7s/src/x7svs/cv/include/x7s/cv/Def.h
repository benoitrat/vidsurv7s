/**
 *  @file
 *  @brief Contains the class Def.h
 *  @date Apr 28, 2010
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef DEF_H_
#define DEF_H_

#include <cv.h>
#include <x7scv.h>
#include <x7svidsurv.h>


namespace x7s
{

typedef cv::Vec< cv::Point , 2 > Line;


enum PosFlags
{
	FST		=  1,	// 0001
	SND		=  2,	// 0010
	HORI	=  4,	// 0100
	VERT	=  8,	// 1000
};


enum Pos {
	TOP			= (HORI | FST),
	BOTTOM	= (HORI | SND),
	LEFT			= (VERT | FST),
	RIGHT		= (VERT | SND),
};

X7S_EXPORT cv::Rect drawJauge(cv::Mat& img, float value, x7s::Pos pos=TOP, float ratio=0.7, int thickness=12, int margin=15);


}



#endif /* DEF_H_ */
