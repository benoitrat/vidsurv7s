/**
 *  @file
 *  @brief Contains the class X7sVSPipeline.hpp
 *  @date 20-may-2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SVIDSURVPIPELINEFALLING_HPP_
#define X7SVIDSURVPIPELINEFALLING_HPP_

#include "ix7svidsurv.h"
#include "cv/Polygon.h"

/**
* @brief Our Pipeline with a mix between OpenCV module and our module to
* perform video surveillance and trajectory analysis.
* @ingroup x7svidsurv
*/
class X7sVidSurvPipelineQueue : public X7sVidSurvPipeline {
public:
	X7sVidSurvPipelineQueue(X7sXmlObject *parent);
	virtual ~X7sVidSurvPipelineQueue();
	virtual void  Process(IplImage* pImg, IplImage* pMask = NULL);
	virtual void process(const cv::Mat& src, const cv::Mat& fg=cv::Mat());
	virtual IplImage*  GetFGMask() { return (m_iFG.imageSize>0)?&m_iFG:NULL; }
	virtual bool Apply(int depth);
	virtual void Draw(IplImage *pImg);

protected:
	void ProcessBTGen(IplImage* pImg, IplImage* pFG);
	float estimateOccupency(const std::vector<float>& areasPct);
	void fillMask();

private:
	virtual bool CreateChildFromXML(const TiXmlElement *child);

protected:
    /* The variable */
	  X7sFGDetector*     m_pFGD; 		//!< ForeGround Detector module.

	 CvBlobSeq			m_BlobList;		//!< The list of the blob tracked.
	 X7sConCompoList	*m_pConComList;	//!< Actual list of the connected component (It may be not used).

	 cv::Mat 			m_imThr;		//!< Image thresholded of the input.
	 cv::Mat			m_imLast;		//!< Last frame (difference of frame)
	 IplImage			m_iFG;			//!< A structure that contains the FG  images.
	 x7s::Polygon		m_poly;		//!< Contour of the queue.
	 x7s::Polygon 		m_qVec;	//!< The queue direction vector
	 cv::Mat			m_imMask;		//!< Mask related to the contour polygons,
	 cv::vector<cv::Mat>			m_imSubMask;		//!< Mask related to the contour polygons,
	 cv::vector<x7s::Polygon>		m_subPolys;		//!< List of sub polys that correspond to level of the queue.
	 cv::vector<float> m_areasTotal;
	 cv::vector<float> m_areasActual;
	 cv::vector<float> m_areasSmoothed;



    int 		m_trainFrames;		//!< Number of training frame to obtain an estimation.
    int 		m_drawPolys;		//!< Type of drawing polygon/queue vector/sub polygon.
    int 		m_cvtThermal;		//!< Type of thermal color conversion (or color map)
    uint8_t 	m_thrFG;				//!< Threshold of the FG.

};




#endif /* X7SVIDSURVPIPELINEFALLING_HPP_ */
