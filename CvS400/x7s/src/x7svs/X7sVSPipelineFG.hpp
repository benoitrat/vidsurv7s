/**
 *  @file
 *  @brief Contains the class X7sVSPipeline.hpp
 *  @date 20-may-2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef X7SVSPIPELINE_HPP_
#define X7SVSPIPELINE_HPP_

#include "ix7svidsurv.h"

/**
* @brief Our Pipeline with a mix between OpenCV module and our module to
* perform video surveillance and trajectory analysis.
* @ingroup x7svidsurv
*/
class X7sVidSurvPipelineFG : public X7sVidSurvPipeline {
public:
	X7sVidSurvPipelineFG(X7sXmlObject *parent);
	virtual ~X7sVidSurvPipelineFG();
	virtual void        Process(IplImage* pImg, IplImage* pMask = NULL);
	virtual void 		Draw(IplImage *pImg);
	virtual bool ReadFromXML(const TiXmlNode *cnode);
	virtual bool WriteToXML(TiXmlNode *cnode,bool clear=true) const;

protected:
	void LoadModulesFromXml(const TiXmlElement *cnode);
	IplImage* ProcessFGD(IplImage* pImg, IplImage* pFG);
	void ProcessBD(IplImage* pImg, IplImage* pFG);
	int ProcessBT(IplImage* pImg, IplImage* pFG);
	void PostProcess(IplImage* pImg, IplImage* pFG, int CurBlobNumber);
	void DeleteBlob(IplImage* pImg, IplImage* pFG);
	void ProcessBTGen(IplImage* pImg, IplImage* pFG);
	void ProcessBTAna(IplImage* pImg, IplImage* pFG);
	void ProcessAlarm();

private:
	bool CreateDefaultSubModule();
	bool CreateChildFromXML(const TiXmlElement *child);

protected:
	/* The pipeline of modules */
    X7sFGDetector*          m_pFGD; 		//!< ForeGround Detector module.
    CvBlobDetector*         m_pBD; 			//!< Blob Detector module.
    CvBlobTracker*          m_pBT; 			//!< Blob Tracker module.
    CvBlobTrackPostProc*    m_pBTPP;	//!< Blob Trajectory Post Processing
    CvBlobTrackAnalysis*    m_pBTA; 		//!< Blob Trajectory Analysis

    /* The variable */
    CvBlobSeq               m_BlobList;		//!< The list of the blob tracked.
    int                     m_FGTrainFrames;//!< Number of training frame to obtain a correct BG model.
    int                     m_FrameCount;	//!< Count the number of frame for each process.
    int                     m_NextBlobID;	//!< The ID of the next blob
    int 					m_PrevBlobNum;	//!< Previous number of blob.

    int                     	m_BTReal;		//!< Blob Tracker Real value from a specific set.
    int                     	m_UsePPData;	//!< If we use PP Data.
    bool 					m_subModCreated;	//!< Return true when the submode where created.


    //TOCHECK: How and if we can remove this parameters.
    const char*				m_TimesFile;

private:

};




#endif /* X7SVSPIPELINE_HPP_ */
