#include "X7sFGDetector.hpp"

//========================================================================
//-- Includes done in cpp file to accelerate compilation.
#include "X7sVSLog.hpp"
#include "ix7svidsurv.h"


//========================================================================
//-- Constructor, Destructor and Init.


/*
 * @brief Constructor for setting all pointer to 0x0000.
 */
X7sFGDetectorImpl::X7sFGDetectorImpl(X7sXmlObject *parent,int type)
:X7sFGDetector(parent,IX7S_VS_TAGNAME_FGD,type,X7S_XML_ANYID),
 m_bgModel(NULL),m_bgParams(NULL),
 cvtcode_frombgr(X7S_CV_CVTCOPY), cvtcode_tobgr(X7S_CV_CVTCOPY),
 m_filterFG(NULL), m_imBG(NULL), m_imInput(NULL)
 {
	m_postFilterFG=true;
 }

/*
 * @brief Destructor for CvBGStatModel and x7sBGStatModelParams.
 * These two structure are created from X77CV library and do not have destructor,
 * therefore we call the release function.
 */
X7sFGDetectorImpl::~X7sFGDetectorImpl()
{
	X7S_PRINT_DEBUG("X7sFGDetectorImpl::~X7sFGDetectorImpl()");

	if(m_bgModel) cvReleaseBGStatModel(&m_bgModel);
	if(m_bgParams) x7sReleaseBGStatModelParams(&m_bgParams);
	if(m_filterFG) cvReleaseImage(&m_filterFG);

}

/**
* @brief Initialize the field of X7sFGDetectorImpl
* according to the type of background we can have.
* @note If this function has been loaded correctly,
* it can not be used again.
* @param type The type of background model: See @ref X7S_BG_MODEL_xxx.
* @return @true if this was loaded correctly, @false otherwise.
*/
bool X7sFGDetectorImpl::Init(int type)
{

	//First create the parameters according to the type.
	if(m_bgParams) x7sReleaseBGStatModelParams(&m_bgParams);
	m_bgParams = x7sCreateBGStatModelParams(type);
	if(!m_bgParams) return false;

	AssoArray typeNameMap = X7sFGDetectorImpl::GetTypeNameMap();
	const char * moduleName=typeNameMap[type];
	X7S_PRINT_DEBUG("X7sFGDetectorImpl::Init()",
			"with type=%s (%d)",moduleName,type);

	m_params.Insert(X7sParam("DebugWnd",X7S_XML_ANYID,true,X7STYPETAG_INT,0,1));
	m_params["DebugWnd"]->LinkParamValue(&(m_Wnd));

	m_params.Insert(X7sParam("postFilterFG",X7S_XML_ANYID,m_postFilterFG,X7STYPETAG_BOOL));
	m_params["postFilterFG"]->LinkParamValue(&(m_postFilterFG));


	if(type==X7S_BG_MODEL_FGD_SIMPLE ||
			type==X7S_BG_MODEL_FGD)
	{
		if(m_bgParams->fgd==NULL) {
			X7S_PRINT_ERROR("X7sFGDetectorImpl::Init()","m_bgParams->fgd==NULL");
			return false;
		}


		//Create an XML list with the name of the FG
		//m_params = new X7sXmlParamList(moduleName);

		//Add the parameters
		AddParam("alpha1",&(m_bgParams->fgd->alpha1));
		AddParam("alpha2",&(m_bgParams->fgd->alpha2));
		AddParam("alpha3",&(m_bgParams->fgd->alpha3));
		AddParam("N1c",&(m_bgParams->fgd->N1c));
		AddParam("N2c",&(m_bgParams->fgd->N2c));
		AddParam("N1cc",&(m_bgParams->fgd->N1cc));
		AddParam("N2cc",&(m_bgParams->fgd->N2cc));
		AddParam("ObjWithoutHoles",&(m_bgParams->fgd->is_obj_without_holes));
		AddParam("Morphology",&(m_bgParams->fgd->perform_morphing));

		cvtcode_frombgr=X7S_CV_CVTCOPY;
		cvtcode_tobgr=X7S_CV_CVTCOPY;

	} else if(type==X7S_BG_MODEL_MOG)
	{

		if(m_bgParams->gauss==NULL) {
			X7S_PRINT_ERROR("X7sFGDetectorImpl::Init()","m_bgParams->gauss==NULL");
			return false;
		}

		//Add the parameters
		AddParam("win_size",&(m_bgParams->gauss->win_size));
		AddParam("n_gauss",&(m_bgParams->gauss->n_gauss));

		AddParam("bg_threshold",&(m_bgParams->gauss->bg_threshold));
		AddParam("std_threshold",&(m_bgParams->gauss->std_threshold));
		AddParam("minArea",&(m_bgParams->gauss->minArea));
		AddParam("weight_init",&(m_bgParams->gauss->weight_init));
		AddParam("variance_init",&(m_bgParams->gauss->variance_init));

		cvtcode_frombgr=CV_BGR2YCrCb;
		cvtcode_tobgr=CV_YCrCb2BGR;

	} else if(type==X7S_BG_MODEL_CBK_INTER)
	{

		if(m_bgParams->cbk_inter==NULL) {
			X7S_PRINT_ERROR("X7sFGDetectorImpl::Init()","m_bgParams->cbk_inter==NULL");
			return false;
		}

		//Add the parameters
		AddParam("cbBounds0",&(m_bgParams->cbk_inter->cbBounds[0]));
		AddParam("cbBounds1",&(m_bgParams->cbk_inter->cbBounds[1]));
		AddParam("cbBounds2",&(m_bgParams->cbk_inter->cbBounds[2]));

		AddParam("modMin0",&(m_bgParams->cbk_inter->modMin[0]));
		AddParam("modMin1",&(m_bgParams->cbk_inter->modMin[1]));
		AddParam("modMin2",&(m_bgParams->cbk_inter->modMin[2]));

		AddParam("modMax0",&(m_bgParams->cbk_inter->modMax[0]));
		AddParam("modMax1",&(m_bgParams->cbk_inter->modMax[1]));
		AddParam("modMax2",&(m_bgParams->cbk_inter->modMax[2]));

		AddParam("window",&(m_bgParams->cbk_inter->window));
		AddParam("num_codewords_elements",&(m_bgParams->cbk_inter->num_codewords_elements));
		AddParam("nframesToLearnBG",&(m_bgParams->cbk_inter->nframesToLearnBG));

		cvtcode_frombgr=CV_BGR2YCrCb;
		cvtcode_tobgr=CV_YCrCb2BGR;

	}else if(type==X7S_BG_MODEL_MOC)
	{

		if(m_bgParams->cluster==NULL) {
			X7S_PRINT_ERROR("X7sFGDetectorImpl::Init()","m_bgParams->cluster==NULL");
			return false;
		}

		//Add the parameters
		AddParam("K",&(m_bgParams->cluster->K));
		AddParam("dim_thrs",&(m_bgParams->cluster->T));
		AddParam("distc_big",&(m_bgParams->cluster->dB));
		AddParam("distc_small",&(m_bgParams->cluster->dS));
		AddParam("insDelay_log",&(m_bgParams->cluster->insDelayLog));
		AddParam("is_dispBG",&(m_bgParams->cluster->is_dispBG));

		cvtcode_frombgr=CV_BGR2YCrCb;
		cvtcode_tobgr=CV_YCrCb2BGR;

	} else if(type==X7S_BG_MODEL_BCD)
	{

		if(m_bgParams->bcd==NULL) {
			X7S_PRINT_ERROR("X7sFGDetectorImpl::Init()","m_bgParams->bcd==NULL");
			return false;
		}

		//Add the parameters
		AddParam("threshold",&(m_bgParams->bcd->threshold));
		AddParam("insDelay_log",&(m_bgParams->bcd->insDelayLog));
		AddParam("refreshDelay",&(m_bgParams->bcd->refreshDelay));
		AddParam("pseudomed_1",&(m_bgParams->bcd->pseudomed_1));
		AddParam("pseudomed_2",&(m_bgParams->bcd->pseudomed_2));
		AddParam("dilate_k",&(m_bgParams->bcd->dilate_k));
		AddParam("is_dispBG",&(m_bgParams->bcd->is_dispBG));

		cvtcode_frombgr=X7S_CV_BGR2YUV422;
		cvtcode_tobgr=X7S_CV_YUV4222BGR;


	} else
	{
		X7S_PRINT_ERROR("X7sFGDetectorImpl::Init()","Unknown BG Model type=%d",type);
		return false;
	}

	return true;
}



//========================================================================
//-- Other Public Methods


/**
* @brief Update the background model and process the new foreground image.
*/
void X7sFGDetectorImpl::Process(IplImage *pImg)
{
	//Create the BG model with the image first frame.
	if(m_bgModel==NULL)
	{
		if(!CV_IS_IMAGE(pImg)) return;

		//Create the temporary image to make conversion
		m_imInput = x7sCreateCvtImage(pImg,cvtcode_frombgr);
		m_imBG = x7sCreateCvtImage(pImg,cvtcode_tobgr);

		//Check if the parameters are initialized.
		if(m_bgParams==NULL) this->Init(X7S_BG_MODEL_DEFAULT);
		m_bgModel = x7sCreateBGModel(pImg,m_bgParams);

	}
	//Otherwise update the model with the current frame.
	else
	{
		x7sCvtColor(pImg,m_imInput,cvtcode_frombgr);

		cvUpdateBGStatModel(m_imInput, m_bgModel);
	}
}

/**
* @brief Obtain a representation of the background.
* @note If we use the Mixture of Gaussian, This image is not the BG Model
*/
IplImage* X7sFGDetectorImpl::GetBackGround()
{
	x7sCvtColor(m_bgModel->background,m_imBG,cvtcode_tobgr);
	return m_bgModel->background;
}

/**
* @brief Obtain the new foreground image.
*/
IplImage* X7sFGDetectorImpl::GetMask()
{
	return this->GetForeGround();
}

/**
* @brief Set the pixel we don't want to update by using a mask.
* @todo Fill this function.
*/
void X7sFGDetectorImpl::SetNoInterest(IplImage *pImg)
{

}


/**
* @brief Create a map between the type of model of background and the XML node name.
*
*/
AssoArray X7sFGDetectorImpl::GetTypeNameMap()
{
	AssoArray typeNameMap;

	//Be careful of the order because we will look in the XML in the same order
	//if we have different BG model.
	typeNameMap.insert(std::pair<int,const char*>(X7S_BG_MODEL_BCD,"FG_BCD"));
	typeNameMap.insert(std::pair<int,const char*>(X7S_BG_MODEL_CBK,"FG_CBK"));
	typeNameMap.insert(std::pair<int,const char*>(X7S_BG_MODEL_CBK_INTER,"FG_CBK_INTER"));
	typeNameMap.insert(std::pair<int,const char*>(X7S_BG_MODEL_MOG,"FG_MOG"));
	typeNameMap.insert(std::pair<int,const char*>(X7S_BG_MODEL_MOC,"FG_MOC"));
	typeNameMap.insert(std::pair<int,const char*>(X7S_BG_MODEL_FGD,"FG_FGD"));
	typeNameMap.insert(std::pair<int,const char*>(X7S_BG_MODEL_FGD_SIMPLE,"FG_FGD_SIMPLE"));
	typeNameMap.insert(std::pair<int,const char*>(X7S_BG_MODEL_MOC_HW,"FG_MOC_HW"));

	return typeNameMap;
}





//========================================================================
//-- Protected and Static Method.


/**
* @brief Add parameters to the m_params and to the CvVsModule.
* This class overload the CvVsModule::AddParam().
*/
void X7sFGDetectorImpl::AddParam(const char* name, double *pAddr)
{
	if(!m_params.Exist(name))
	{
		X7sParam param(name,X7S_XML_ANYID,*pAddr,X7STYPETAG_DOUBLE);
		m_params.Insert(param);
	}
	m_params[name]->LinkParamValue(pAddr);
}


/**
* @brief Add parameters to the m_params and to the CvVsModule.
* This class overload the CvVsModule::AddParam().
*/
void X7sFGDetectorImpl::AddParam(const char* name, float *pAddr)
{
	if(!m_params.Exist(name))
	{
		X7sParam param(name,X7S_XML_ANYID,*pAddr,X7STYPETAG_FLOAT);
		m_params.Insert(param);
	}
	m_params[name]->LinkParamValue(pAddr);
}


/**
* @brief Add parameters to the m_params and to the CvVsModule.
* This class overload the CvVsModule::AddParam().
*/
void X7sFGDetectorImpl::AddParam(const char* name, int *pAddr)
{
	if(!m_params.Exist(name))
	{
		X7sParam param(name,X7S_XML_ANYID,*pAddr,X7STYPETAG_INT);
		m_params.Insert(param);
	}
	m_params[name]->LinkParamValue(pAddr);
}


/**
* @brief Obtain the foreground image of the BG model.
*
* This function also apply some filters to the FG to be
* easier to process afterwards.
*/
IplImage* X7sFGDetectorImpl::GetForeGround()
{
	if(m_filterFG==NULL) m_filterFG = cvCreateImage(cvGetSize(m_bgModel->foreground),IPL_DEPTH_8U,1);

	if(m_postFilterFG) {

		if(m_bgParams->type==X7S_BG_MODEL_BCD && m_bgParams->bcd)
		{
			this->PostFilterBCD(m_bgModel->foreground,m_filterFG);
		}
		else if(m_bgParams->type==X7S_BG_MODEL_CBK_INTER && m_bgParams->cbk_inter)
		{
			this->PostFilterCBK(m_bgModel->foreground,m_filterFG);
		}
		else
		{
			this->PostFilter(m_bgModel->foreground,m_filterFG);
		}
	}
	//Otherwise just copy the image
	else
	{
		cvCopy(m_bgModel->foreground,m_filterFG,NULL);
	}
	return m_filterFG;
}


void X7sFGDetectorImpl::PostFilter(const IplImage *src, IplImage *dst)
{
	IplImage *tmp=NULL;
	if(tmp==NULL) tmp = cvCreateImage(cvGetSize(src),IPL_DEPTH_8U,1);

	x7sPseudoMed(src,tmp,5);
	//cv7PseudoMed(imO_grey,imT_grey,7);
	//x7sFillLineHoles(tmp);
	cvDilate(tmp,dst,NULL,1);        //Dilate by 3x3 matrix

	if(tmp) cvReleaseImage(&tmp);
}

void X7sFGDetectorImpl::PostFilterCBK(const IplImage *src, IplImage *dst)
{
	//IplConvKernel *kernelMor5x5 =  cvCreateStructuringElementEx(5, 5, 0, 0, CV_SHAPE_ELLIPSE, NULL);
	IplConvKernel * kernelMor3x3 = cvCreateStructuringElementEx(3, 3, 0, 0, CV_SHAPE_ELLIPSE, NULL);

	cvErode(src, dst, kernelMor3x3, 1);
	//cvDilate(m_filterFG, m_filterFG, kernelMor5x5, 1);
	//cvDilate(m_filterFG, m_filterFG, kernelMor5x5, 1);
	cvSegmentFGMask(dst);

	//cvReleaseStructuringElement(&kernelMor5x5);
	cvReleaseStructuringElement(&kernelMor3x3);

}

void X7sFGDetectorImpl::PostFilterBCD(const IplImage *src, IplImage *dst)
{
	IplImage *tmp=NULL;
	if(tmp==NULL) tmp = cvCreateImage(cvGetSize(src),IPL_DEPTH_8U,1);

	cvSet(dst,cvScalarAll(0),NULL);
	cvSet(tmp,cvScalarAll(0),NULL);

	x7sPseudoMed(src,tmp,m_bgParams->bcd->pseudomed_1);
	x7sPseudoMed(tmp,dst,m_bgParams->bcd->pseudomed_2);
	x7sDilateFast(dst,m_bgParams->bcd->dilate_k);

	if(tmp) cvReleaseImage(&tmp);
}



//========================================================================
//-- X7SVS Public Interfaces.

/**
* @brief Create the cvFGDetector from the @ref X7S_BG_MODEL_xxx
* @param parent The parent XML object
* @param type The type of background model.
* @return A pointer on a CvFGDetector*
*/
X7sFGDetector* x7sCreateFGDetector(X7sXmlObject *parent, int type)
				{
	if(type<0) type=X7S_BG_MODEL_DEFAULT;

	X7sFGDetectorImpl *fgdetect = new X7sFGDetectorImpl(parent,type);
	if(!fgdetect->Init(type)) {
		//If the initialization failed
		delete fgdetect;
		fgdetect=NULL;
	}
	return (X7sFGDetector*)fgdetect;
				}

/**
* @brief Create the FGDetector Base from an Xml file.
* @code
* <FG ID="-1" type="">
*       <param name="..."></param>
*       ...
* 		<param name="toFilter">1</param>
* 		<param name="isShow">0</param>
* </FG>
*
* @endcode
*/
X7sFGDetector* x7sCreateFGDetector(X7sXmlObject *parent, const TiXmlElement * cnode)
{
	bool ok;
	int type=X7S_BG_MODEL_DEFAULT;

	//Read parameters from XML
	if(cnode) type=X7sXmlObject::ReadTypeFromXML(cnode,&ok);

	//Create the object
	X7sFGDetectorImpl *fgdetect = new X7sFGDetectorImpl(parent,type);

	//And initate them
	if(ok && fgdetect->Init(type)) {
		if(cnode) fgdetect->ReadFromXML(cnode);
		return (X7sFGDetector*)fgdetect;
	}

	//If the initialization failed
	X7S_DELETE_PTR(fgdetect);
	return NULL;
}


