#include "X7sXmlLog.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "x7sxml.h"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Initialisation of static (global) variable.
//---------------------------------------------------------------------------------------


const char* X7sXmlLog::names[X7S_LOGLEVEL_END] = X7S_LOGNAMES;
int X7sXmlLog::level = X7S_LOGLEVEL_MED;
int X7sXmlLog::noError=X7S_LOG_NOERROR;
FILE* X7sXmlLog::ferr=stderr;
FILE* X7sXmlLog::fout=stdout;


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public interfaces
//----------------------------------------------------------------------------------------



/**
* @brief Public interface to set the logLevel of the X7SCV library.
* @return @ref X7S_RET_OK if the level has been applied, otherwise @ref X7S_RET_ERR.
* @see X7sXmlLog::SetLevel().
*/
int x7sXmlLog_SetLevel(int log_level) {
	if(X7S_LOGLEVEL_LOWEST <= log_level && log_level < X7S_LOGLEVEL_END) {
		X7sXmlLog::SetLevel(log_level);
		return X7S_RET_OK;
	}
	return X7S_RET_ERR;
}

/**
 * @brief Configure the LOG object
 *
 * This is the public interface to configure the LOG object:
 *	its level, the log files, the threshold between error and normal.
 *
 * @param log_level Set the level for the current library.
 * @param f_out Set the output log file (by default it's stdout)
 * @param f_err Set the error log file (by default it's stderr)
 * @param no_error Set the level to switch between error and output log message.
 *
 * @return @ref X7S_RET_OK if the level has been applied, otherwise @ref X7S_RET_ERR.
 */
int x7sXmlLog_Config(int log_level, FILE *f_out, FILE *f_err, int no_error)
{
	int ret=x7sXmlLog_SetLevel(log_level);
	X7sXmlLog::noError=no_error;

	//Check if the file is valid.
	if(f_out==NULL) f_out=stdout;
	if(f_err==NULL) f_err=stderr;

	//Set the new one
	X7sXmlLog::fout=f_out;
	X7sXmlLog::ferr=f_err;

	return ret;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Static public functions
//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Static public functions
//----------------------------------------------------------------------------------------

/**
* @brief Set the level of print for this static class.
* @param log_level The level given by @ref X7S_LOGLEVEL_xxx
* @return @true if the operation performs correctly.
*/
bool X7sXmlLog::SetLevel(int log_level)
{
	if(X7S_LOGLEVEL_LOWEST <= log_level && log_level < X7S_LOGLEVEL_END) {
		X7sXmlLog::level=log_level;
		return true;
	}
	return false;
}

/**
* @brief If the given level is going to be display.
*
* The given level is compared with the level set with SetLevel()
* @note SetLevel(), x7sXmlLog_SetLevel()
*/
bool X7sXmlLog::IsDisplay(int level)
{
	return (0<= level && level <= X7sXmlLog::level);
}

/**
* @brief Print Log message with a specific level
*
* @param funcname The name of the function that we want to be displayed
* @param file	The name of the file that call this function.
* @param line	The line of the file where the call of this function was produced.
* @param level The log level (e.i.@ref X7S_LOGLEVEL_xxx)
* @param szFormat The format of the output.
* @param ... The variable argument list (va_list).
*
*/
void X7sXmlLog::PrintLevel(const char *funcname,const char *file, int line, int level, const char* szFormat, ...) {

	if(X7S_LOGLEVEL_LOWEST <= level && level < X7S_LOGLEVEL_END)
	{
		va_list args;
		va_start(args,szFormat);
		FILE *output;
		if(level < noError) output=ferr;
		else output=fout;
		Print(true,names[level],funcname,file,line,output,szFormat,args);

#ifdef DEBUG
		if(level < noError)
		{
			if(output!=stderr) Print(true,names[level],funcname,file,line,(FILE*)stderr,szFormat,args);
		}
		else
		{
			if(output!=stdout) Print(true,names[level],funcname,file,line,(FILE*)stdout,szFormat,args);
		}
#endif
	}
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Static private functions
//----------------------------------------------------------------------------------------

/**
* @brief Print a message with the following options:
*
* @param with_time	Add the time to the print.
* @param prefix	Add a prefix to the print (Debug, Warn, ...)
* @param funcname	Add the funcname to the print (if @NULL nothing is added)
* @param filepath 	Add the filepath at the end of the print (if @NULL nothing is added)
* @param line		Add the line number at the end of the print (if @NULL nothing is added).
* @param stream	In which stream to print the message (stdout, stderr, ...).
* @param szFormat	The format of the message
* @param args		The variable argument list.
*/
void X7sXmlLog::Print(bool with_time, const char *prefix, const char *funcname,const char *filepath, int line, FILE *stream, const char *szFormat, va_list args)
{
	char buffer [80];
	const char *filename;
	if(with_time)
	{
		time_t rawtime;
		struct tm * timeinfo;
		time ( &rawtime );
		timeinfo = localtime ( &rawtime );
		strftime(buffer,80,"[%H:%M:%S]",timeinfo);
	}
	if(funcname==NULL) fprintf(stream,"%s %s: ",buffer,prefix);
	else fprintf(stream,"%s %s > %s: ",buffer,prefix,funcname);
	if(szFormat) vfprintf(stream,szFormat,args);
	if(filepath)
	{
		filename = strrchr(filepath,X7S_PATH_SEP);
		if(filename) filename++;
		else filename=filepath;
		fprintf(stream," (%s:%d)",filename,line);
	}
	fprintf(stream,"\n");
	fflush(stream);
	va_end(args);
}
