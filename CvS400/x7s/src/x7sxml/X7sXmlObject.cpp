#include "x7sxml.h"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sXmlLog.hpp"
#include <cstdarg> 	//Use va_list(),...

/** @class X7sXmlObject
*
* We define 3 classes that inherits X7sXmlObject called MyParent, MyChild, MyGrandChild.
*
*

@center{@bold{Constructor of the parent object}}
@dot
digraph const1
{
node [shape=record, fontname=Helvetica, fontsize=8];
rankdir=LR;

c_new [ label="pMyParent =\n new MyParent(NULL)" fillcolor="grey" style="filled"];
p_con [ label="X7sXmlObject | X7sXmlObject(NULL,\"myclass\")" URL="\ref X7sXmlObject::X7sXmlObject()"];
c_con [ label="MyParent | MyParent()"]

c_new -> p_con -> c_con ->p_add;
}
@enddot



@center{@bold{Constructor of the child object}}
@dot
digraph const2
{
node [shape=record, fontname=Helvetica, fontsize=8];

c_new [ label="pMyChild =\n new MyChild(pMyParent)" fillcolor="grey" style="filled"];
p_con [ label="X7sXmlObject (pMyChild) |  X7sXmlObject(pMyParent,\"myclasschild\",true)" URL="\ref X7sXmlObject::X7sXmlObject()"];
p_add [ label="X7sXmlObject (pMyParent) | AddAsChild()" URL="\ref X7sXmlObject::AddAsChild()"]
p_check [ label="X7sXmlObject (pMyParent) | IsExistChild()" URL="\ref X7sXmlObject::IsExistChild()"]
c_con [ label="MyChild (pMyChild)| MyChild()"]

rankdir=LR;
c_new -> p_con -> c_con ->p_add -> p_check;
}
@enddot

@center{@bold{Configure and Create with XML file}}
@dot
digraph read
{
node [shape=rectangle, fontname=Helvetica, fontsize=8];

p_call  [ label="pMyParent->ReadFromXML(parent_node)" fillcolor="grey" style="filled"];
p_read  [ label="X7sXmlObject (pMyParent) :: ReadFromXML(parent_node)" URL="\ref X7sXmlObject::ReadFromXML()"];
c_read  [ label="X7sXmlObject (pMyChild) :: ReadFromXML(child_node)" URL="\ref X7sXmlObject::ReadFromXML()"];
c_crea  [ label="MyChild (pMyChild) :: CreateChildFromXML(gchild_node)"];
g_new   [ label="pMyGrandChild = new MyGrandChild(pMyChild)" fillcolor="grey" style="filled"]
g_con   [ label="X7sXmlObject (pMyGrandChild) ::  X7sXmlObject(pMyChild,\"myclassgrandchild\",true)" URL="\ref X7sXmlObject::X7sXmlObject()"];
c_add   [ label="X7sXmlObject (pMyChild) :: AddAsChild()" URL="\ref X7sXmlObject::AddAsChild()"]
c_check [ label="X7sXmlObject (pMyChild) :: IsExistChild()" URL="\ref X7sXmlObject::IsExistChild()"]
g_con2  [ label="MyGrandChild :: MyGrandChild(pMyChild)"]
g_call  [ label="pMyGrandChild->ReadFromXML(gchild_node)" fillcolor="grey" style="filled"]
g_read  [ label="X7sXmlObject (pMyGranChild) :: ReadFromXML(gchild_node)" URL="\ref X7sXmlObject::ReadFromXML()"];
g_apply [ label="X7sXmlObject (pMyGranChild) :: Apply(0)" URL="\ref X7sXmlObject::Apply()"];
c_apply [ label="X7sXmlObject (pMyChild) :: Apply(0)" URL="\ref X7sXmlObject::Apply()"];
p_apply [ label="X7sXmlObject (pMyParent) :: Apply(0)" URL="\ref X7sXmlObject::Apply()"];



p_call -> p_read -> c_read -> c_crea ->
g_new -> g_con ->  c_add -> c_check -> g_con2 ->
g_call -> g_read -> g_apply -> c_apply -> p_apply;

p_read ->p_apply [color="grey"];
g_read ->g_apply [color="grey"];
c_read ->c_apply [color="grey"];
c_crea -> g_new [color="grey" style="dashed"];
c_crea -> g_call [color="grey" style="dashed"];

}
@enddot


*
*
*
*/



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

/**
* @brief Construct an X7sXmlObject.
* @param parent The X7sXmlObject parent of this object.
* @param name	The name of the object (used as tagname).
* @param createChildren	If the method CreateChildFromXML() is going to be called.
* @param type	Type of the node.
* @param id	ID of the node (must be unique for a parent).
*/
X7sXmlObject::X7sXmlObject(X7sXmlObject *parent,const std::string& name, bool createChildren, int type, int id)
:m_params(name,id,type),m_parent(parent), m_createChildren(createChildren)
 {
	X7S_PRINT_DEBUG("X7sXmlObject::X7sXmlObject()","name=%s, id=%d, type=%d, createChild=%d",name.c_str(),id,type,createChildren);
	Init();
 }

/**
* @brief Construct an X7sXmlObject.
* @param parent The X7sXmlObject parent of this object.
* @param name	The name of the object (used as tagname).
* @param type	Type of the node.
* @param id	ID of the node (must be unique for a parent).
*/
X7sXmlObject::X7sXmlObject(X7sXmlObject *parent,const std::string& name,int type,int id)
:m_params(name,id,type), m_parent(parent), m_createChildren(false)
 {
	X7S_PRINT_DEBUG("X7sXmlObject::X7sXmlObject()","name=%s, id=%d, type=%d",name.c_str(),id,type);
	Init();
 }

/**
* @brief Init the default value (only simple type).
*/
void X7sXmlObject::Init()
{
	m_isFirstChild=false;
	m_isLastChild=false;
	if(m_parent) m_isValid=m_parent->AddAsChild(this);
}



/**
* @brief Delete the XMl object and all its children.
*
* It also remove itself from the list of children of its parent.
*
*/
X7sXmlObject::~X7sXmlObject()
{
	X7S_PRINT_DEBUG("X7sXmlObject::~X7sXmlObject()","<%s id=%d type=%d></%s> (parent=%s; nChildren=%d)",
			m_params.GetName(),m_params.GetID(),m_params.GetType(),m_params.GetName(),
			m_parent?m_parent->GetTagName():"None",m_children.size());

	//First remove this object from the parent list.
	if(m_parent) m_parent->RemoveChild(this);

	//Then delete all the children
	std::vector<X7sXmlObject*> children = m_children;//Create a copy (because they autoremove from the m_children vector)..
	for(size_t i=0; i< children.size(); i++) {
		if(children[i])
		{
			X7S_PRINT_DEBUG("X7sXmlObject::~X7sXmlObject()","Delete Child named %s",
					children[i]->GetTagName());
			X7S_DELETE_PTR(children[i]);
		}
	}

	//Finally delete
	m_children.clear();
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public method
//----------------------------------------------------------------------------------------


/**
* @brief Set the parameters of X7sXMlObject using a node obtained from an XML file.
*
* The node will also try to fill its children.
* @note Once the parameters are read this function called Apply().
*
*
* @param cnode is the current node named with the name given by X7sXmlObject()
*/
bool X7sXmlObject::ReadFromXML(const TiXmlNode *cnode)
{
	bool ret, childRead;
	ret=m_params.ReadFromXML(cnode);
	//std::cout << m_params.toString() << std::endl << std::endl;
	if(ret)
	{
		const TiXmlElement *xmlelem = cnode->FirstChildElement();
		childRead=false;
		m_isFirstChild=true;
		m_isLastChild=false;
		while(xmlelem) {

			//Find the tagname
			const char *tagname = xmlelem->Value();

			//It is not a typical parameters.
			if(strcmp(tagname,"param")!=0) {
				//Obtain the ID of the children
				int tag_id;
				if(xmlelem->Attribute("ID",&tag_id)==NULL) tag_id=X7S_XML_ANYID;



				//Then browse over the children that must be created
				for(int i=0;i<(int)m_children.size();i++)
				{
					const char *name = m_children[i]->m_params.GetName();

					if(strcmp(tagname,name)==0)
					{
						if(tag_id==m_children[i]->GetID())
						{
							X7sXmlObject *newChild = ChildTypeReadFromXML(xmlelem,m_children[i]);
							if(newChild)
							{
								X7S_PRINT_INFO("X7sXmlObject::ReadFromXML()","Reload type child <%s ID=\"%d\"><%s>",tagname,tag_id,tagname);
								m_children[i]=newChild;
							} 	else
							{
								X7S_PRINT_DEBUG("X7sXmlObject::ReadFromXML()","child <%s ID=\"%d\"><%s>",tagname,tag_id,tagname);
							}
							//Finally read the children (new or old one).
							ret = ret & m_children[i]->ReadFromXML(xmlelem);
							childRead=ret; //If this child has been read, it was already created.
						}
					}
				}

				//If no children are set by default, then look if we may create some given the type of node.
				if(m_createChildren && !childRead) {
					X7S_PRINT_DEBUG("X7sXmlObject::CreateChildFromXML()","<%s ID=\"%d\"><%s>",tagname,tag_id,tagname);
					this->CreateChildFromXML(xmlelem);
				}
			}

			//Go to the next xml node.
			xmlelem = xmlelem->NextSiblingElement();

			//Setup the parameters for the next value.
			if(xmlelem==cnode->LastChild()->ToElement()) m_isLastChild=true;
			m_isFirstChild=false;

		}
		this->Apply(0);
	}
	return ret;
}

/**
* @brief Recreate a child and/or read it from XML.
*
*
* If the child is not the good type we delete the actual one and create a new one of the good type.
* Otherwise we just read it.
*
* An exemple of how using the function is the following:
*
* This function must be overloaded, otherwise it just do nothing.
*
* @param child
* @return NULL if we don't want to modify the child before reading it.
*
*
*/
X7sXmlObject* X7sXmlObject::ChildTypeReadFromXML(const TiXmlElement* childNode, X7sXmlObject *childObj)
{
	return NULL;
}


bool X7sXmlObject::ReadFromXMLObject(const X7sXmlObject *obj)
{
	X7S_FUNCNAME("X7sXmlObject::ReadFromXMLObject()");
	X7S_CHECK_WARN(funcName,obj,false,"The given X7sXmlObject is NULL");

	if(X7S_IS_STREQ(this->GetTagName(),obj->GetTagName()))
	{
		TiXmlElement node(obj->GetTagName());
		if(obj->WriteToXML(&node,true))
		{
			return this->ReadFromXML(&node);
		}
		else X7S_PRINT_WARN(funcName,"Couldn't write in obj %s",obj->GetTagName());
	}
	else 	X7S_PRINT_WARN(funcName,"Object doesn't have the same tag name %s, %s",this->GetTagName(),obj->GetTagName());

	return false;
}


/**
* @brief Write into the node all the parameters and call WriteToXML() method of all the children.
* @param cnode The node with the same value as the name of the XMLObject
* @param clear Clear all the children in order to avoid duplicate of the code.
* @return @true if the operation was succesfull for this node and all its children.
*/
bool X7sXmlObject::WriteToXML(TiXmlNode *cnode,bool clear) const
		{
	//Remove all the children
	if(!X7S_XML_ISEQNODENAME(cnode,GetTagName())) {

		return false;
	}

	if(clear) cnode->Clear();

	bool ret;
	ret= m_params.WriteToXML(cnode);

	X7S_PRINT_INFO("X7sXmlObject::WriteToXML()",
			"Writing <%s type=\"%d\" ID=\"%d\"></%s>",
			GetTagName(),GetID(),GetType(),GetTagName());

	if(ret)
	{

		for(int i=0;i<(int)m_children.size();i++)
		{
			const char *name = m_children[i]->m_params.GetName();
			TiXmlNode *subcnode = new TiXmlElement(name);
			if(m_children[i]->WriteToXML(subcnode))
			{
				cnode->LinkEndChild(subcnode);
				ret=ret & true;
			}
		}
	}
	return ret;
		}

/**
* @brief Add another X7sXmlObject as child of this one.
*
* Children can have the same name (xml tag), but must have a unique ID.
*
* @param child A pointer on another X7sXmlObject.
* @return @true if the X7sXmlObject as be integrated in the children list.
*/
bool X7sXmlObject::AddAsChild(X7sXmlObject *child)
{
	if(child)
	{
		if(child->GetID()==X7S_XML_ANYID) //Search a free ID.
		{
			int ID=0;
			while(IsExistChild(child->GetTagName(),ID)) ID++;	//Check every ID to find one free.
			child->SetID(ID);	//Set the first free ID.
		}
		else { //Otherwise search for duplicate
			if(IsExistChild(child->GetTagName(),child->GetID()))
			{
				//Duplicate found
				X7S_PRINT_WARN("X7sXmlObject::AddAsChild()","<%s id=\"%d\"> is already in children list of <%s />",
						child->GetTagName(),child->GetID(), this->GetTagName());
				return false;
			}
		}

		//No duplicate, add the child to the list.
		m_children.push_back(child);

		//If the child have a different parent
		if(child->m_parent != this) {
			if(child->m_parent) child->m_parent->RemoveChild(child);	//Remove the child from the old parent.
			child->m_parent=this;					//And add "this" object as the actual parent.
		}


		X7S_PRINT_DEBUG("X7sXmlObject::AddAsChild()","<%s ...><%s id=\"%d\">...<%s> OK",
				this->GetTagName(),child->GetTagName(),child->GetID(),this->GetTagName());
		return true;
	}
	return false;
}

/**
* @brief Apply the parameters to the current objects and its children
*
* After we have change some parameters with X7sXmlObject::SetParam(),
* we may want to apply them on a specific object and its sub-children.
*
* This method only calls the same method for each of its children and terminate by calling
* X7sParamList::ResetIsChanged(), so that for each X7sParam X7sParam::IsChanged() return false.
*
* Its advise to re-implement this function in the inherited class when we desire to apply the parameters
* into the object. Be careful,
* When this function is re-implemented don't forget to call this one to keep the recursivity
* working. Here follow an example
@code
class MyObj : public X7sXmlObject
{
public:
MyObj(X7sXmlObject *parent, int type) : X7sXmlObject(parent,"myobj",type)
{
m_params.Insert(X7sParam("param1",X7S_XML_ANYID,0);
m_params.Insert(X7sParam("param2",X7S_XML_ANYID,0);
};
~MyObj();

bool Apply(int depth=-1)
{
//First apply to the child.
X7sXmlObject::Apply(depth);

//Applying the parameters
if(m_params["param1")->IsChanged())
printf("Value is now %s,m_params["param1"]->toString().c_str());
else
printf("Value is still %s,m_params["param1"]->toString().c_str());
};
}
@endcode
*
*
*
* @note This method is called after X7sXmlObject::ReadFromXML()

* @param depth the number of sublevel we browse for applying the parameters. When:
* 	- depth=0, we apply just this method.
* 	- depth>0, call the Apply() method of the children with (depth-1).
* 	- depth<0, call the Apply() method recursively to all the children.
*
* @return @true if the parameters where correctly applied.
*/
bool X7sXmlObject::Apply(int depth)
{
	bool ret=true;
	if(depth!=0) {
		if(depth>0) depth--;
		for(size_t i=0;i<m_children.size();i++)
		{
			if(m_children[i])
				ret = ret & m_children[i]->Apply(depth);
		}
	}
	return ret;
}

/**
* @brief Remove a children from the list of children keep
*
* This function doesn't delete the pointer on the children.
*
* @param child The children we want to remove.
* @return @true if the pointer corresponds to a child in the parent's list.
*/
bool X7sXmlObject::RemoveChild(X7sXmlObject *child)
{
	std::vector<X7sXmlObject*>::iterator it;
	for(it=m_children.begin() ; it < m_children.end(); it++)
	{
		if((*it)==child)
		{
			it=m_children.erase(it);
			X7S_PRINT_DEBUG("X7sXmlObject::RemoveChild()","<%s ...><%s id=\"%d\">...<%s> OK",
					this->GetTagName(),child->GetTagName(),child->GetID(),this->GetTagName());
			return true;
		}
	}
	return false;
}

/**
* @brief Test if a child with the same name and ID already exist in the child list.
* @param name	the name of the child (X7sXmlObject::GetTagName())
* @param ID	the ID of the child (X7sXmlObject::GetID())
* @return @true if a child with the same name and ID is found.
*/
bool X7sXmlObject::IsExistChild(const char *name, int ID) const
		{
	//Search for duplicate
	for(int i=0;i<(int)m_children.size();i++)
	{
		//If children already have the same name
		if(strcmp(m_children[i]->GetTagName(),name)==0)
		{
			if(m_children[i]->GetID()==ID)
			{
				return true;
			}
		}
	}
	return false;
		}

/**
* @brief This method does nothing it should be called overload by to construct child as we want.
* @param child A XmlElement child of the given X7sXmlObject.
* @return Always return false if this function is not overloaded.
*/
bool X7sXmlObject::CreateChildFromXML(const TiXmlElement *child)
{
	if(child)
	{
		X7S_PRINT_WARN("X7sXmlObject::CreateChildFromXML()",
				"need to be overload and therefore can't create the child <%s />",
				child->Value());
	}
	return false;
}


bool X7sXmlObject::SetParam(const char *name, const char *value, ...)
{
	bool ret=false;
	if(name && value)
	{
		va_list args;
		va_start(args,value);
		ret=m_params[name]->SetValues(value,args);
		va_end(args);
	}
	return ret;
}

bool X7sXmlObject::SetParam(const char *name, const std::string& value)
{
	bool ret=false;
	if(name)
	{
		ret=m_params[name]->SetValue(value.c_str());
	}
	return ret;
}

/**
* @brief Return a parameter using its name.
*
*/
const X7sParam* X7sXmlObject::GetParam(const char* name)
{
	return (const X7sParam*)m_params[name];
}

/**
* @brief Return a parameters given the previous one.
* @param pParam The previous parameters (if it is NULL we will get the first parameter of the list)
* @return A pointer on a X7sParam or NULL if the list has ended.
* @see X7sParamList::GetNextParam().
*/
const X7sParam* X7sXmlObject::GetNextParam(const X7sParam* pParam)
{
	return (const X7sParam*)m_params.GetNextParam((X7sParam*)pParam);
}

/**
* @brief Return a child given the previous one.
* @param child The previous child (if it is NULL we will get the first child of the list)
* @return  A pointer on a X7sXmlObject or NULL if the list has ended or was empty (No more children).
*/
X7sXmlObject* X7sXmlObject::GetNextChild(X7sXmlObject *child)  	{

	//If child is NULL
	if(child==NULL) {
		m_itChild=m_children.begin();	//Go to the beginning of the list
	}
	else
	{
		//Otherwise set the iterator on the next value after child.
		if(child==(*m_itChild)) m_itChild++;	//The child has the same value than the iterator, therefore go on next one.
		else {	//Otherwise loop on the vector until finding the correct child.
			for ( m_itChild=m_children.begin() ; m_itChild != m_children.end(); m_itChild++ ) {
				if(child==(*m_itChild)) break;
			}
		}
	}

	if(m_itChild == m_children.end()) return NULL;	//Check if it the end of the list.
	else return (*m_itChild);	//Otherwise return return the child
}


/**
* @brief Return @true if the X7sXmlObject::GetTagName() is the same as name.
*/
bool X7sXmlObject::IsTagName(const char *name) const
		{
	return (strcmp(GetTagName(),name)==0);
		}


/**
* Return a print message that show how to configure
*/
std::string X7sXmlObject::toString()
{
	std::string str(m_params.toString());
	for(int i=0;i<(int)m_children.size();i++)
	{
		if(m_children[i]) str+=m_children[i]->toString();
	}
	return str;

}

/**
* @brief Print the XmlObject and its children.
* @param cfile	The File to print (if @NULL print in @tt{stdout})
* @param depth	The depth to print.
*/
void X7sXmlObject::Print(FILE *cfile,int depth)
{
	TiXmlElement ele(this->GetTagName());
	this->WriteToXML(&ele,true);
	ele.Print(cfile,depth);
	fflush(cfile);
}



int X7sXmlObject::ReadTypeFromXML(const TiXmlNode *cnode, bool *ok)
{
	int type=X7S_XML_ANYTYPE;
	*ok=false;
	if(cnode)
	{
		const TiXmlElement * ele = cnode->ToElement();
		if(ele)
		{
			const char *valStr = ele->Attribute("type",&type);
			if(valStr) 	*ok=true;
		}
	}
	return type;
}

int X7sXmlObject::ReadIdFromXML(const TiXmlNode *cnode, bool *ok)
{
	int id=X7S_XML_ANYID;
	*ok=false;
	if(cnode)
	{
		const TiXmlElement * ele = cnode->ToElement();
		if(ele)
		{
			const char *valStr = ele->Attribute("ID",&id);
			if(valStr) 	*ok=true;
		}
	}
	return id;
}

