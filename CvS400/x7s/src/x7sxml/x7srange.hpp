#ifndef _X7SRANGE_H_
#define _X7SRANGE_H_

#include "x7sdef.h"
#include "X7sXmlLog.hpp"
#include <string>
#include <sstream>
#include <iostream>
#include <climits>
#include <cfloat>




class X7sRangeGeneric {
public:
	enum DataType {DATA_VALUE, DATA_MIN, DATA_MAX, DATA_STEP};

public:
	X7sRangeGeneric(int type);
	virtual ~X7sRangeGeneric();
	virtual std::string toString(DataType=DATA_VALUE)=0;
	virtual bool fromString(const std::string& str)=0;
	virtual bool SetStep(double step_dl)=0;
	virtual bool Increment(bool positive=true)=0;
	virtual int GetLength()=0;
	X7sRangeGeneric* GetCopy() const;
	int GetType() const;

private:
	int type;
public:
	bool m_changed; //!< Value set if the parameter has changed.
};

inline X7sRangeGeneric::X7sRangeGeneric(int type): type(type),m_changed(false) {}
inline X7sRangeGeneric::~X7sRangeGeneric() {}
inline int X7sRangeGeneric::GetType() const { return type; }

/*
 * @brief Generic structure for a range.
 */
template<typename TYPE>
class X7sRange: public X7sRangeGeneric {
public:
	X7sRange();
	X7sRange(TYPE Val);
	X7sRange(const X7sRange<TYPE>* pCopy);
	X7sRange(int type, TYPE val, TYPE min, TYPE max);
	X7sRange(int type, TYPE val, TYPE min, TYPE max, TYPE step);
	virtual ~X7sRange();

	bool Link(TYPE *pVal);
	bool SetValue(TYPE Val);
	bool SetRange(TYPE min,TYPE max);
	bool SetStep(double step_dl);
	bool Increment(bool positive=true);
	const TYPE& GetValue();
	const TYPE& GetMin() {return min; }
	const TYPE& GetMax() {return max; }
	const TYPE& GetStep() { return step; }
	int GetLength();
	std::string toString(DataType=DATA_VALUE);
	bool fromString(const std::string& str);

private:
	TYPE val;			//!< Value
	TYPE min;			//!< Minimum boundary.
	TYPE max;			//!< Maximum boundary.
	TYPE step;			//!< Value of the step when we increment the value.
	TYPE* link_val;		//!< Pointer on a linked value.
};



typedef X7sRange<uint8_t> X7sRangeU8;
typedef X7sRange<uint32_t> X7sRangeU32;
typedef X7sRange<int> X7sRangeI;
typedef X7sRange<float> X7sRangeF;
typedef X7sRange<double> X7sRangeD;
typedef X7sRange<std::string> X7sRangeStr;

/*
 * Various declaration of the constructor.
 */


template<typename TYPE> inline X7sRange<TYPE>::X7sRange() :
	X7sRangeGeneric(X7STYPETAG_UNKOWN), val(0), min(0), max(0), step(1), link_val(0) {}

template<typename TYPE> inline X7sRange<TYPE>::X7sRange(TYPE value) :
	X7sRangeGeneric(X7STYPETAG_UNKOWN), val(value), min(0), max(0), step(1), link_val(0) {}

template<typename TYPE> inline X7sRange<TYPE>::X7sRange(int type, TYPE val, TYPE min, TYPE max) :
	X7sRangeGeneric(type), val(val), min(min), max(max), step(1), link_val(0) {}

template<typename TYPE> inline X7sRange<TYPE>::X7sRange(int type, TYPE val, TYPE min, TYPE max, TYPE step) :
	X7sRangeGeneric(type), val(val), min(min), max(max), step(step), link_val(0) {}

template<>
inline X7sRangeU8::X7sRange(uint8_t value) : X7sRangeGeneric(X7STYPETAG_U8), val(value), min(0), max(UCHAR_MAX), step(1), link_val(0) {}

template<>
inline X7sRangeU32::X7sRange(uint32_t value) : X7sRangeGeneric(X7STYPETAG_U32), val(value), min(0), max(UINT_MAX) , step(1),  link_val(0) {}

template<>
inline X7sRangeI::X7sRange(int value) : X7sRangeGeneric(X7STYPETAG_INT), val(value), min(INT_MIN), max(INT_MAX) , step(1),  link_val(0) {}

template<>
inline X7sRangeF::X7sRange(float value) :  X7sRangeGeneric(X7STYPETAG_FLOAT), val(value), min(FLT_MIN), max(FLT_MAX), step(1.0f), link_val(0) {}

template<>
inline X7sRangeD::X7sRange(double value) : X7sRangeGeneric(X7STYPETAG_DOUBLE), val(value), min(DBL_MIN), max(DBL_MAX) , step(1.0),  link_val(0) {}

template<>
inline X7sRangeStr::X7sRange(std::string value) : X7sRangeGeneric(X7STYPETAG_STRING), val(value), min(""), max("") , step(""),  link_val(0) {}


template<typename TYPE> inline X7sRange<TYPE>::X7sRange(const X7sRange<TYPE>* pCopy)
:X7sRangeGeneric(pCopy->GetType()),
val(pCopy->val), min(pCopy->min), max(pCopy->max),
step(pCopy->step), link_val(pCopy->link_val)
{

}


template<typename TYPE> inline X7sRange<TYPE>::~X7sRange() {}


template<typename TYPE> std::string X7sRange<TYPE>::toString(DataType dtype) {
	std::stringstream sstr;
	switch(dtype)
	{
	case DATA_VALUE:
		sstr << this->GetValue();
		break;
	case DATA_MIN:
		sstr << min;
		break;
	case DATA_MAX:
		sstr << max;
		break;
	case DATA_STEP:
		sstr << step;
		break;
	default:
		sstr << this->GetValue();
		break;
	}
	return sstr.str();
}

template<>
std::string X7sRange<uint8_t>::toString(DataType dtype) {
	std::stringstream sstr;
	switch(dtype)
	{
	case DATA_VALUE:
		sstr << (int)this->GetValue();
		break;
	case DATA_MIN:
		sstr << (int)min;
		break;
	case DATA_MAX:
		sstr << (int)max;
		break;
	case DATA_STEP:
		sstr << (int)step;
		break;
	default:
		sstr << (int)this->GetValue();
		break;
	}
	return sstr.str();
}




template<typename TYPE> bool X7sRange<TYPE>::fromString(const std::string& str) {
	TYPE tmp;
	std::stringstream sstr;
	sstr << str;
	sstr >> tmp;
	return this->SetValue(tmp);
}

template<>
bool X7sRangeU8::fromString(const std::string& str) {
	int tmp;
	std::stringstream sstr;
	sstr << str;
	sstr >> tmp;
	return this->SetValue((uint8_t)tmp);
}

template<>
bool X7sRangeStr::fromString(const std::string& str) {
	if(val!=str)
	{
		m_changed=true;
		val=str;
		if(link_val) *link_val=val;
	}
	return true;
}


template <typename TYPE>
const TYPE& X7sRange<TYPE>::GetValue()
{
	if(link_val) {
		if(*link_val!=val) val=*link_val;
	}
	return val;
}

template <typename TYPE>
int X7sRange<TYPE>::GetLength()
{
	return 1;
}

template<>
int X7sRangeStr::GetLength()
{
	if(val.empty()) return 0;

	if(GetType()==X7STYPETAG_STRING)
	{
		return val.length();
	}
	else {
		int count=1;
			for(size_t i=1;i<val.length()-1;i++)
		{
			if(val.at(i)==' ' && val.at(i+1)!=' ')
			{
				count++;
			}
		}
		return count;
	}
}

template <typename TYPE>
bool X7sRange<TYPE>::SetValue(TYPE value)
{
	bool inrange=true;
	if(value!=val)
	{
		m_changed=true;
		inrange=(min <= value && value <= max);	//Check the range.
		if(inrange) {	//If we are inside the range.
			val=value;
			if(link_val) *link_val=val;	//An update the linked value
		}
	}
	return inrange;
}

template<>
bool X7sRangeStr::SetValue(std::string value)
{
	if(val!=value)
	{
		m_changed=true;
	val=value;
	if(link_val) *link_val=val;
	}
	return true;
}


template <typename TYPE>
bool X7sRange<TYPE>::Link(TYPE* pValue)
{
	if(pValue)  {
		link_val=pValue;
		*pValue=val;
		return true;
	}
	return false;
}

template <typename TYPE>
bool X7sRange<TYPE>::SetRange(TYPE min,TYPE max)
{
	if(min< max) {
		m_changed=true;
		this->min=min;
		this->max=max;
		return true;
	}
	return false;
}

template <typename TYPE>
bool X7sRange<TYPE>::SetStep(double step_dl)
{
	m_changed=true;
	step=(TYPE)step_dl;
	return true;
}

template <>
bool X7sRangeStr::SetStep(double step_dl)
{
	return false;
}


template <typename TYPE>
bool X7sRange<TYPE>::Increment(bool positive)
{
	TYPE tmp=val;
	if(positive) tmp+=step;
	else tmp-=step;
	return SetValue(tmp);
}

template <>
bool X7sRangeStr::Increment(bool positive)
{
	return false;
}


inline X7sRangeGeneric* X7sRangeGeneric::GetCopy() const
{
	switch(type)
	{
	case X7STYPETAG_U8:		return new X7sRangeU8((const X7sRangeU8*)this);
	case X7STYPETAG_BOOL:	return new X7sRangeU8((const X7sRangeU8*)this);
	case X7STYPETAG_U16:	return new X7sRangeU32((const X7sRangeU32*)this);
	case X7STYPETAG_U32:	return new X7sRangeU32((const X7sRangeU32*)this);
	case X7STYPETAG_INT:	return new X7sRangeI((const X7sRangeI*)this);
	case X7STYPETAG_FLOAT:	return new X7sRangeF((const X7sRangeF*)this);
	case X7STYPETAG_DOUBLE:	return new X7sRangeD((const X7sRangeD*)this);
	case X7STYPETAG_STRING: return new X7sRangeStr((const X7sRangeStr*)this);
	case X7STYPETAG_STRVEC: return new X7sRangeStr((const X7sRangeStr*)this);
	default:
		X7S_PRINT_ERROR("X7sRangeGeneric::GetCopy()","Unknow type %d, returning NULL",type);
		return NULL;
	}
}


#endif /* _X7sRange_H_ */

