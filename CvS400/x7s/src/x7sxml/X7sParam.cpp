#include "x7srange.hpp"
#include "x7sxml.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sXmlLog.hpp"
#include <cstring>
#include <cstdarg> 	//!< Use va_list(),...
#include <cstdio> 	//!< Use vprintf(),...


#define IX7SPARAM_GETDATA(pvar,dtype,sstr,ret) \
	if(pvar) { \
		sstr.clear(); \
		sstr << m_range->toString(dtype); \
		sstr >> (*pvar); \
		ret = ret & !sstr.fail(); \
	}



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Function to transform type in string.
//----------------------------------------------------------------------------------------
const char* GetTypeDesc(int type) {
	switch(type) {
	case X7STYPETAG_U8: 	return "U8";
	case X7STYPETAG_U16: 	return "U16";
	case X7STYPETAG_U32: 	return "U32";
	case X7STYPETAG_CHAR: 	return "CHAR";
	case X7STYPETAG_SHORT: 	return "SHORT";
	case X7STYPETAG_INT: 	return "INT";
	case X7STYPETAG_LONG: 	return "LONG";
	case X7STYPETAG_BOOL: 	return "BOOL";
	case X7STYPETAG_FLOAT: 	return "FLOAT";
	case X7STYPETAG_DOUBLE: return "DOUBLE";
	case X7STYPETAG_STRING: return "STRING";
	case X7STYPETAG_STRVEC: return "STRVEC";
	case X7STYPETAG_UNKOWN:
	default: return "UNKOWN";
	}
}




//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

/**
* @brief Empty constructor which return the @ref X7sParamNULL.
* @anchor X7sParamNULL
*
* The @b "X7sParamNULL", is a parameters which has the following field:
* 		- @tt{name}	 = "X7sParamNULL".
* 		- @tt{ID}    = @ref X7S_XML_NULLID.
* 		- @tt{value} = "NULL".
*
* It is used by the X7sParamList to avoid returning a NULL pointer which can cause segmentation fault.
*
*/
X7sParam::X7sParam()
:name("X7sParamNULL"),ID(X7S_XML_NULLID)
{
	m_range = new X7sRangeStr("NULL");
}

/**
* @brief Constructor for Integer parameters.
*
* 	-	If type is @ref X7STYPETAG_U8, @ref X7STYPETAG_U32, @ref X7STYPETAG_FLOAT, @ref X7STYPETAG_DOUBLE :
* val, min, max fields are cast to their corresponding type.
*
* 	-	If type as another value: val, min, max fields are cast to @b int, and the type is replaced by @ref X7STYPETAG_INT.
*
* @param name 	Name of the parameter
* @param ID	ID of the parameter (can be @ref X7S_XML_ANYID)
* @param val	Value of the parameter.
* @param type	Type of the value (@ref X7STYPETAG_xxx)
* @param min	Minimum value that the parameter can have.
* @param max 	Maximum value that the parameter can have.
*/
X7sParam::X7sParam(const char* name,int ID, int val, int type, int min,int max)
:name(name),ID(ID),m_range(NULL) {
	switch(type) {
	case X7STYPETAG_U8:
		min = X7S_MAX(min,0);
		max = X7S_MIN(max,UINT8_MAX);
		val = X7S_INRANGE(val,min,max);
		m_range = new X7sRangeU8(type,(uint8_t)val,(uint8_t)min,(uint8_t)max);
		break;
	case X7STYPETAG_BOOL:
		min = 0;
		max = 1;
		val = X7S_INRANGE(val,min,max);
		m_range = new X7sRangeU8(type,(uint8_t)val,(uint8_t)min,(uint8_t)max);
		break;
	case X7STYPETAG_U16:
		max = X7S_MIN(max,UINT16_MAX);
		val = X7S_INRANGE(val,min,max);
	case X7STYPETAG_U32:
		min = X7S_MAX(min,0);
		val = X7S_INRANGE(val,min,max);
		m_range	= new X7sRangeU32(type,(uint32_t)val,(uint32_t)min,(uint32_t)max);
		break;
	case X7STYPETAG_INT:
		min = X7S_MAX(min,INT32_MIN);
		max = X7S_MIN(max,INT32_MAX);
		val = X7S_INRANGE(val,min,max);
		m_range = new X7sRangeI(type,(int)val,(int)min,(int)max);
		break;
	case X7STYPETAG_FLOAT:
		val = X7S_INRANGE(val,min,max);
		m_range	= new X7sRangeF(type,(float)val,(float)min,(float)max); break;
		break;
	case X7STYPETAG_DOUBLE:
		val = X7S_INRANGE(val,min,max);
		m_range = new X7sRangeD(type,(double)val,(double)min,(double)max); break;
	default:
		min = X7S_MAX(min,INT32_MIN);
		max = X7S_MIN(max,INT32_MAX);
		val = X7S_INRANGE(val,min,max);
		m_range = new X7sRangeI(X7STYPETAG_INT,(int)val,(int)min,(int)max);
		break;
	}
}

/**
* @brief Constructor for Real parameters.
*
* 	-	If type is @ref X7STYPETAG_FLOAT, @ref X7STYPETAG_U8, @ref X7STYPETAG_U32, @ref X7STYPETAG_INT
*  the val, min, max fields are cast to their corresponding type.
*
* 	-	If type as another value: val, min, max fields are cast to @b double, and the type is replaced by @ref X7STYPETAG_DOUBLE.
*
* @param name 	Name of the parameter
* @param ID	ID of the parameter (can be @ref X7S_XML_ANYID)
* @param val	Value of the parameter.
* @param type	Type of the value (@ref X7STYPETAG_xxx)
* @param min	Minimum value that the parameter can have.
* @param max 	Maximum value that the parameter can have.
*/
X7sParam::X7sParam(const char* name,int ID,double val, int type, double min,double max)
:name(name),ID(ID), m_range(NULL) {
	switch(type) {
	case X7STYPETAG_U8:
		min = X7S_MAX(min,0);
		max = X7S_MIN(max,UINT8_MAX);
		val = X7S_INRANGE(val,min,max);
		m_range = new X7sRangeU8(type,(uint8_t)val,(uint8_t)min,(uint8_t)max);
		break;
	case X7STYPETAG_BOOL:
		min = 0;
		max = 1;
		val = X7S_INRANGE(val,min,max);
		m_range = new X7sRangeU8(type,(uint8_t)val,(uint8_t)min,(uint8_t)max);
		break;
	case X7STYPETAG_U16:
		min = X7S_MAX(min,0);
		max = X7S_MIN(max,UINT16_MAX);
		val = X7S_INRANGE(val,min,max);
		m_range	= new X7sRangeU32(type,(uint32_t)val,(uint32_t)min,(uint32_t)max);
		break;
	case X7STYPETAG_U32:
		min = X7S_MAX(min,0);
		max = X7S_MIN(max,UINT32_MAX);
		val = X7S_INRANGE(val,min,max);
		m_range	= new X7sRangeU32(type,(uint32_t)val,(uint32_t)min,(uint32_t)max);
		break;
	case X7STYPETAG_INT:
		min = X7S_MAX(min,INT32_MIN);
		max = X7S_MIN(max,INT32_MAX);
		val = X7S_INRANGE(val,min,max);
		m_range = new X7sRangeI(type,(int)val,(int)min,(int)max);
		break;
	case X7STYPETAG_FLOAT:
		val = X7S_INRANGE(val,min,max);
		m_range	= new X7sRangeF(type,(float)val,(float)min,(float)max);
		break;
	case X7STYPETAG_DOUBLE:
	default:
		val = X7S_INRANGE(val,min,max);
		m_range = new X7sRangeD(X7STYPETAG_DOUBLE,(double)val,(double)min,(double)max);
		break;
	}
}

/**
* @brief Constructor for Integer parameters.
*
* For the string type (@ref X7STYPETAG_STRING), no min and max values are necessary.
*
* @param name 	Name of the parameter
* @param ID	ID of the parameter (can be @ref X7S_XML_ANYID)
* @param str	A Pointer to zero-terminated string.
* @param type 	If the string is for setting a vector of double value (@ref X7STYPETAG_STRVEC)
* or a normal text
*/
X7sParam::X7sParam(const char* name,int ID,const char* str,int type)
:name(name),ID(ID), m_range(NULL) {
	if(type==X7STYPETAG_STRVEC) {
		m_range = new X7sRangeStr(type,str,"","","");
	}
	else {
		m_range = new X7sRangeStr(str);
	}
}


/**
* @brief Default destructor.
*/
X7sParam::~X7sParam() {
	delete(m_range);
}


/**
* @brief Explicit constructor by copy.
* @param copy A reference on another X7sParam.
*/
X7sParam::X7sParam(const X7sParam& copy)
:name(copy.name), ID(copy.ID)
{
	m_range = copy.m_range->GetCopy();
}

/**
* @brief Overload assignments operator @b (=).
*
* The value inside the old X7sParam are deleted and replaced by the new one.
* @param copy A reference on another X7sParam.
*/
X7sParam& X7sParam::operator=(const X7sParam& copy)
{
	name=copy.name;
	ID=copy.ID;
	if(m_range) delete m_range;	//Delete the old m_range.
	m_range = copy.m_range->GetCopy();
	return *this;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Setters.
//----------------------------------------------------------------------------------------

bool X7sParam::LinkParamValue(uint8_t *pVal) {
	bool ret=(m_range->GetType()==X7STYPETAG_U8);
	if(ret) {
		X7sRangeU8* range = (X7sRangeU8*)m_range;
		ret=range->Link(pVal);
	}
	else {
		X7S_PRINT_WARN("X7sParam::LinkParamValue()",
				"can't link uint8_t* to param \"%s\" with type=%s",
				name.c_str(),GetTypeDesc(m_range->GetType()));
	}
	return ret;
}

bool X7sParam::LinkParamValue(uint32_t *pVal) {
	bool ret=(m_range->GetType()==X7STYPETAG_U32);
	if(ret) {
		X7sRangeU32* range = (X7sRangeU32*)m_range;
		ret=range->Link(pVal);
	}
	else {
		X7S_PRINT_WARN("X7sParam::LinkParamValue()",
				"can't link uint32_t* to param \"%s\" with type=%s",
				name.c_str(),GetTypeDesc(m_range->GetType()));
	}
	return ret;
}

bool X7sParam::LinkParamValue(int *pVal) {
	bool ret=(m_range->GetType()==X7STYPETAG_INT);
	if(ret) {
		X7sRangeI* range = (X7sRangeI*)m_range;
		ret=range->Link(pVal);
	}
	else {
		X7S_PRINT_WARN("X7sParam::LinkParamValue()",
				"can't link int* to param \"%s\" with type=%s",
				name.c_str(),GetTypeDesc(m_range->GetType()));
	}
	return ret;
}

bool X7sParam::LinkParamValue(float *pVal) {
	bool ret=(m_range->GetType()==X7STYPETAG_FLOAT);
	if(ret) {
		X7sRangeF* range = (X7sRangeF*)m_range;
		ret=range->Link(pVal);
	}
	else {
		X7S_PRINT_WARN("X7sParam::LinkParamValue()",
				"can't link float* to param \"%s\" with type=%s",
				name.c_str(),GetTypeDesc(m_range->GetType()));
	}
	return ret;
}

bool X7sParam::LinkParamValue(double *pVal) {
	bool ret=(m_range->GetType()==X7STYPETAG_DOUBLE);
	if(ret) {
		X7sRangeD* range = (X7sRangeD*)m_range;
		ret=range->Link(pVal);
	}
	else {
		X7S_PRINT_WARN("X7sParam::LinkParamValue()",
				"can't link double* to param \"%s\" with type=%s",
				name.c_str(),GetTypeDesc(m_range->GetType()));
	}
	return ret;
}

bool X7sParam::LinkParamValue(std::string *pVal) {
	bool ret=(m_range->GetType()==X7STYPETAG_STRING);
	if(ret) {
		X7sRangeStr* range = (X7sRangeStr*)m_range;
		ret=range->Link(pVal);
	}
	else {
		X7S_PRINT_WARN("X7sParam::LinkParamValue()",
				"can't link string* to param \"%s\" with type=%s",
				name.c_str(),GetTypeDesc(m_range->GetType()));
	}
	return ret;
}

bool X7sParam::LinkParamValue(bool *pVal)
{
	bool ret=(m_range->GetType()==X7STYPETAG_BOOL);
	if(ret) {
		X7sRangeU8* range = (X7sRangeU8*)m_range;
		ret=range->Link((uint8_t*)pVal);
	}
	else {
		X7S_PRINT_WARN("X7sParam::LinkParamValue()",
				"can't link bool* to param \"%s\" with type=%s",
				name.c_str(),GetTypeDesc(m_range->GetType()));
	}
	return ret;
}

bool X7sParam::SetValue(const char *szStr) {
	return m_range->fromString(std::string(szStr));
}

bool X7sParam::SetValue(const std::string& str) {
	return m_range->fromString(str);
}

bool X7sParam::SetValues(const char *szFormat,...)
{
	bool ret;
	va_list args;
	va_start(args,szFormat);
	ret = this->SetValues(szFormat,args);
	va_end(args);

	return ret;
}


bool X7sParam::SetValues(const char *szFormat,va_list args)
{
	int ret;
	ret = vsnprintf(m_buff,256,szFormat,args);
	return (this->SetValue(m_buff) && ret>=256);
}


bool X7sParam::SetStep(double step)
{
	return m_range->SetStep(step);
}

bool X7sParam::setComment(const std::string& str)
{
	comment=str;
	if(m_range) {
		m_range->m_changed=true;
		return m_range->m_changed;
	}
	else return false;
}

void X7sParam::ResetIsChanged()
{
	if(m_range) m_range->m_changed=false;
}


std::istream& operator >>(std::istream& stream, X7sParam const*pParam) {
	std::string str;
	if(pParam && pParam->m_range) {
		stream >> str;
		pParam->m_range->fromString(str);

	}
	return stream;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>> Getters.
//----------------------------------------------------------------------------------------

int X7sParam::GetID() const {
	return ID;
}

const std::string& X7sParam::GetName() const {
	return name;
}

const std::string& X7sParam::getComment() const {
	return comment;
}



int X7sParam::GetType() const {
	return m_range->GetType();
}

int X7sParam::GetLength() const {
	return m_range->GetLength();
}


bool X7sParam::GetValue(double *val, double *min, double *max, double *step) const
{
	bool ret= (val!=NULL);
	std::stringstream sstr;
	IX7SPARAM_GETDATA(val,X7sRangeGeneric::DATA_VALUE,sstr,ret);
	IX7SPARAM_GETDATA(min,X7sRangeGeneric::DATA_MIN,sstr,ret);
	IX7SPARAM_GETDATA(max,X7sRangeGeneric::DATA_MAX,sstr,ret);
	IX7SPARAM_GETDATA(step,X7sRangeGeneric::DATA_STEP,sstr,ret);
	return ret;
}

bool X7sParam::GetValue(int *val, int *min, int *max, int *step) const
{
	bool ret= (val!=NULL);
	std::stringstream sstr;
	IX7SPARAM_GETDATA(val,X7sRangeGeneric::DATA_VALUE,sstr,ret);
	IX7SPARAM_GETDATA(min,X7sRangeGeneric::DATA_MIN,sstr,ret);
	IX7SPARAM_GETDATA(max,X7sRangeGeneric::DATA_MAX,sstr,ret);
	IX7SPARAM_GETDATA(step,X7sRangeGeneric::DATA_STEP,sstr,ret);
	return ret;
}

bool X7sParam::GetValue(std::string *val) const
{
	if(val)
	{
		*val = toString();
	}
	return true;
}


bool X7sParam::GetRealValue(int pos, double *ptr) const
{
	if(pos>=m_range->GetLength())
	{
		return false;
	}
	else
	{
		//Check if it a string representation return the character
		if(m_range->GetType()==X7STYPETAG_STRING)
		{
			std::string str = m_range->toString();
			char c = str.at(pos);
			*ptr=(double)((int)c);
		}
		//If its a string vector return the number.
		else if(m_range->GetType()==X7STYPETAG_STRVEC)
		{
			std::string str = m_range->toString();
			size_t count=0, i=1,iprev=0;
			for(i=1;i<str.length()-1;i++)
			{
				if(str.at(i)==' ' && str.at(i+1)!=' ')
				{
					if(count==(size_t)pos) break;
					count++;
					iprev=i;
				}
			}
			double tmp;
			size_t n_end=str.find_first_of(' ',iprev+1);
			if(n_end==0) n_end=str.length();

			std::stringstream sstr;
			sstr << str.substr(iprev,n_end);
			sstr >> tmp;
			if(sstr.fail()) return false;
			else {
				*ptr=tmp;
				return true;
			}
		}
		//Otherwise return the value.
		else {
			*ptr=this->toRealValue();
		}
		return true;
	}
}

std::string X7sParam::toString() const {
	return m_range->toString();
}

int X7sParam::toIntValue(bool *ok) const {
	int tmp=0;
	bool ret = GetValue(&tmp);
	if(ok) *ok=ret;
	return tmp;
}

double X7sParam::toRealValue(bool *ok) const {
	double tmp=0;
	bool ret = GetValue(&tmp);
	if(ok) *ok=ret;
	return tmp;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>> Is"ers".
//----------------------------------------------------------------------------------------

/**
* @brief Return if the parameters is considered as OK.
*
* The notion of "Ok" is different according to the parameter family.
* 		- X7STYPEFAMILY_INTEGER : @true <=> val > 0
* 		- X7STYPEFAMILY_REAL 	: @true <=> val > 0
* 		- X7STYPEFAMILY_STRING 	: @true <=> val != ""
*/
bool X7sParam::IsOk() const
{
	if(m_range->GetType()==X7STYPETAG_BOOL) {
		return toIntValue();
	} else if(IsInteger() || IsReal()) {
		return toRealValue()>=0.0;
	} else if(IsString()) {
		return toString().empty();
	}
	return false;
}

bool X7sParam::IsInteger() const
{
	return ((m_range->GetType()/10)==X7STYPEFAMILY_INTEGER);
}

bool X7sParam::IsReal() const
{
	return ((m_range->GetType()/10)==X7STYPEFAMILY_REAL);
}

bool X7sParam::IsString() const
{
	return ((m_range->GetType()/10)==X7STYPEFAMILY_STRING);
}

bool X7sParam::IsChanged() const
{
	if(m_range) return m_range->m_changed;
	return false;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>> Others.
//----------------------------------------------------------------------------------------

std::ostream& operator <<(std::ostream& stream, const X7sParam* pParam) {
	if(pParam && pParam->m_range) {
		stream << pParam->m_range->toString();
	}
	return stream;
}



std::string X7sParam::Print() const {
	std::stringstream sstr;
	sstr << name << " ("<<ID<<") " << "is of type "<< GetTypeDesc(m_range->GetType());
	sstr << ", val=" << this << "(changed="<< m_range->m_changed << ")";
	return sstr.str();
}



bool X7sParam::Increment(bool positive)
{
	return m_range->Increment(positive);
}
