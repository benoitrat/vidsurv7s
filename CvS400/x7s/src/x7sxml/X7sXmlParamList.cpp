#include "x7sxml.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sXmlLog.hpp"
#include <cstring>

#define IX7S_XML_PARAM_TAGNAME "param"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
X7sXmlParamList::X7sXmlParamList(const std::string& name, int id, int type)
:m_name(name), m_id(id), m_type(type)
{}


X7sXmlParamList::~X7sXmlParamList()
{
	X7S_PRINT_DEBUG("X7sXmlParamList::~X7sXmlParamList()","name=%s, id=%d, type=%d",
			m_name.c_str(),m_id,m_type);
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public method
//----------------------------------------------------------------------------------------

/**
* @brief Read a XML Node and fill the value in the list.
* @param parent The parent node of the one we want to read (name of the X7sXmlParamList).
* @return true if the correct node was found, @false otherwise.
*/
bool X7sXmlParamList::ReadFromXML(const TiXmlNode *parent)
{
	if(parent==NULL) {
		X7S_PRINT_INFO("X7sXmlParamList::ReadFromXML()","The node for list %s is NULL",GetName());
		return false;
	}

	if(strcmp(parent->Value(),GetName())!=0) {
		X7S_PRINT_INFO("X7sXmlParamList::ReadFromXML()","The given node (<%s>) is not the one expected: (<%s>)",
				parent->Value(),GetName());
		return false;
	}

	//Read the ID.
	int tmp_id=m_id;
	if(parent->ToElement()->Attribute("ID",&tmp_id)) {
		m_id=tmp_id;
	}

	int tmp_type=m_type;
	if(parent->ToElement()->Attribute("type",&tmp_type)) {
		m_type=tmp_type;
	}

	X7S_PRINT_DEBUG("","Reading X7sXmlParam <%s ID=\"%d\" type=\"%d\"><%s>",
			GetName(),m_id,m_type,GetName());



	const TiXmlElement *param_node = parent->FirstChildElement(IX7S_XML_PARAM_TAGNAME);
	while(param_node != 0) {
		this->ReadParamFromXML(param_node);
		param_node = param_node->NextSiblingElement(IX7S_XML_PARAM_TAGNAME);	//Loop over the <param></param> nodes.
	}
	return true;
}

/**
* @brief Write to a XML Node the value in the list.
* @param parent The parent node of the one we want to write (name of the X7sXmlParamList).
* @note If the node already exist, this method overwrite all the parameters in
* the list with their new value.
* @return true if the correct node was found, @false otherwise.
*/
bool X7sXmlParamList::WriteToXML(TiXmlNode *parent) const
{
	if(parent==NULL) {
		X7S_PRINT_INFO("X7sXmlParamList::WriteToXML()","The node for list %s is NULL",GetName());
		return false;
	}

	if(strcmp(parent->Value(),GetName())!=0) {
		X7S_PRINT_INFO("X7sXmlParamList::WriteToXML()","The given node (<%s>) is not the one expected: (<%s>)",
				parent->Value(),GetName());
		return false;
	}

	X7S_PRINT_DEBUG("X7sXmlParamList::WriteToXML()","Writing X7sXmlParam <%s ID=\"%d\" type=\"%d\"><%s>",
						GetName(),m_id,m_type,GetName());

	//Overwriting the ID
	parent->ToElement()->SetAttribute("ID",m_id);
	parent->ToElement()->SetAttribute("type",m_type);


	//Then, we remove all the old <param></param> node
	TiXmlElement *param_node = parent->FirstChildElement(IX7S_XML_PARAM_TAGNAME);
	while(param_node != 0) {
		parent->RemoveChild(param_node);
		param_node = param_node->NextSiblingElement(IX7S_XML_PARAM_TAGNAME);	//Loop over the <param></param> nodes.
	}


	//To insert the new one (easier way then modifying the files)
	std::map<std::string,X7sParam*>::const_iterator it;
	for ( it=m_mapName.begin() ; it != m_mapName.end(); it++ ) {
		TiXmlNode *child=CreateParamXmlNode((*it).second);
		if(child) {
			parent->LinkEndChild(child);
			X7S_PRINT_DUMP("X7sXmlParamList::WriteToXML()","writing node <%s>",((*it).first).c_str());
		}
	}
	return true;
}

/**
 * @brief Create and Get the XML node we want, given the list of parameters.
 */
TiXmlNode* X7sXmlParamList::GetXmlNode()
{
	return CreateXmlNode();
}

const char *X7sXmlParamList::GetName() const {
	return m_name.c_str();
}

/**
 * @brief Setup the ID of the Xml Parameters List.
 *
 * If the actual ID of the list is equal to @ref X7S_XML_ANYID,
 * we can change it and set it up to another value.
 *
 * @param ID The new ID that we want to change.
 * @return @true if the new value could be setup, @false it an ID already existed.
 */
bool X7sXmlParamList::SetID(int ID)
{
	bool ret;
	ret = (m_id==X7S_XML_ANYID);
	if(ret) m_id=ID;
	else X7S_PRINT_WARN("X7sXmlParamList::SetID()", "ID is already set to %d",m_id);
	return ret;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Method to manipulate the list of parameters
//----------------------------------------------------------------------------------------

TiXmlNode* X7sXmlParamList::CreateXmlNode()
{
	//Create the XML element
	TiXmlElement *el = new TiXmlElement(m_name);
	el->SetAttribute("ID",m_id);

	std::map<std::string,X7sParam*>::iterator it;
	for ( it=m_mapName.begin() ; it != m_mapName.end(); it++ ) {
		X7S_PRINT_DEBUG("","Creating node <%s>",((*it).first).c_str());
		el->LinkEndChild(CreateParamXmlNode((*it).second));
	}
	return el;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Methods to manipulate each parameters
//----------------------------------------------------------------------------------------

/**
* @brief Read the value inside an xml param.
*/
bool X7sXmlParamList::ReadParamFromXML(const TiXmlElement *el_param)
{

	X7S_FUNCNAME("X7sXmlParamList::ReadParamFromXML()");

	//Check if the pointer is not NULL.
	if(el_param==NULL) {
		return false;
	}

	//Find the value in the list
	X7sParam *pParam=NULL;
	pParam = this->m_mapName[el_param->Attribute("name")];

	if(pParam==NULL) {
		X7S_PRINT_INFO(funcName,"Could not find node (<%s>) inside <%s></%s>",
				el_param->Attribute("name"),GetName(),GetName());
		return false;
	}


	int ID;
	if(el_param->Attribute("ID",&ID)) {
		//Check if the attribute ID is correct.
		if(ID != pParam->GetID()) {
			X7S_PRINT_INFO("X7sXmlParamList::ReadParamFromXML()","ID value has changed for <%s></%s>: %d != %d (expected)",
					el_param->Attribute("name"),el_param->Attribute("name"),pParam->GetID(),ID);
			return false;
		}
	}
	else ID=X7S_XML_ANYID;	//If no ID found the ID is forced as ANYID value.

	//Get the value from the text and set it.
	const char* txt=el_param->GetText();
	if(!txt) {
		X7S_PRINT_INFO(funcName,"\t<param name=\"%s\" id=\"%d\">%s</param> was not read (empty field)",
						pParam->GetName().c_str(),ID,pParam->toString().c_str());
		return false;
	}

	//Set the value from the text
	bool isSet=pParam->SetValue(txt);
	if(isSet)
	{
		X7S_PRINT_DEBUG("","\t<param name=\"%s\" id=\"%d\">%s</param> has been read (0x%0x)",
				pParam->GetName().c_str(),ID,pParam->toString().c_str(),pParam);
	}
	else {
		X7S_PRINT_WARN("","\t<param name=\"%s\" id=\"%d\">%s</param> was not read (value is not in the range)",
				pParam->GetName().c_str(),ID,pParam->toString().c_str());
	}
	return isSet;

};

/**
* @brief Write the value of parameters in an XMLNode
*
* An X7sXml is an XML element (no child can be append).
* the format is the following:
* @code
* <param name="myname" id="myid" type="mytype">myvalue</param>
* @endcode
*/
bool X7sXmlParamList::WriteParamToXML(TiXmlElement *el_param)
{
	//Check if the pointer is not NULL.
	if(el_param==NULL) {
		return false;
	}

	//Find the value in the list
	X7sParam *pParam=NULL;
	pParam = this->m_mapName[el_param->Attribute("name")];

	if(pParam==NULL) {
		X7S_PRINT_DEBUG("","Could not find node (<%s>) inside <%s></%s>",
				el_param->Attribute("name"),GetName(),GetName());
		return false;
	}

	TiXmlText *text = el_param->FirstChild()->ToText();
	if(!text) {
		return false;
	}

	text->SetValue(pParam->toString());
	el_param->SetAttribute("ID",pParam->GetID());
	X7S_PRINT_DEBUG("","\t<param name=\"%s\" id=\"%d\">%s</param> has been written",
			pParam->GetName().c_str(),pParam->GetID(),pParam->toString().c_str());


	return true;
}


TiXmlNode* X7sXmlParamList::CreateParamXmlNode(const X7sParam *pParam_) const
{
	X7sParam *pParam = (X7sParam*)pParam_;
	if(pParam==NULL) return NULL;

	//Create the XML element
	TiXmlElement *el = new TiXmlElement(IX7S_XML_PARAM_TAGNAME);

	//Create the text value and set it
	el->LinkEndChild(new TiXmlText(pParam->toString()));

	//Create the attributes.
	el->SetAttribute("name",pParam->GetName());
	el->SetAttribute("ID",pParam->GetID());
	return el;
}


/**
* @brief Put the value a text value into a node text.
*/
bool X7sXmlParamList::SetNodeText(TiXmlElement *node,const std::string& str)
{
	//Get The text child
	TiXmlText *txt=node->FirstChild()->ToText();
	if(!txt) {
		return false;
	}
	if(txt==node->LastChild()->ToText()) txt->SetValue(str);
	return true;
}



std::string X7sXmlParamList::toString() {
	std::stringstream sstr;
	sstr << std::endl << "+-- Name=" << m_name << ", ID=" << m_id << std::endl;
	sstr << X7sParamList::toString();
	return sstr.str();
}
