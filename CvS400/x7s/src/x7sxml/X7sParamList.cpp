#include "x7sxml.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "X7sXmlLog.hpp"
#include <cstring>

using std::map;
using std::pair;
using std::string;


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

/**
* @brief Create an empty list of X7sParam
*
* This constructor also create the @ref X7sParamNULL in case a search of parameters failed.
*
*/
X7sParamList::X7sParamList()
:paramNULL()
{
}

/**
* @brief empty the list and clear the internal memory used for each pointers.
*/
X7sParamList::~X7sParamList()
{
	X7S_PRINT_DEBUG("","~X7sParamList()");
	m_mapID.clear();

	//Delete all pointer inside the list
	m_it=m_mapName.begin();
	while(m_it != m_mapName.end()) {
		if((*m_it).second) delete (*m_it).second;
		(*m_it).second = NULL;
		m_it++;
	}
	//Finally clear the list with empty pointers.
	m_mapName.clear();

	X7S_PRINT_DEBUG("","~X7sParamList() end");
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Function Manipulations.
//----------------------------------------------------------------------------------------

/**
* @brief Insert a X7sParam into the list
*
* The X7sParamList creates internal copy of the X7sParam. The memory
* reserved during the creation of the X7sParam should be therefore deleted.
* @code
* X7sParamList params;
* X7sParam *pParam = new X7sParam("test_name",TEST_ID,"string_value");
* params.Insert(pParam);
* delete pParam;
* pParam=NULL;
* cout << "Value of test_name = << params["test_name"] << endl;	//Still exist in memory because it is a copy
* @endcode
* If you don't want to worry about the memory leak,
* prefers to use @ref X7sParamList::Insert(const X7sParam& param).
*
* @param pParam A pointer on a X7sParam previously defined.
* @return @true if the X7sParam as been correctly added to the list.
* @warning Do not call this function in this way because it will be impossible to
* desallocate the memory used to create the @b new pointer.
* @code params.Insert(new X7sParam("test_name",TEST_ID,"string_value")); @endcode
* @see X7sParamList::Insert(const X7sParam& param).
*
*/
X7sParam* X7sParamList::Insert(const X7sParam* pParam) {
	if(pParam) {
		X7sParam *pParamCopy = new X7sParam(*pParam);
		if(pParamCopy) {
			pair<map<string,X7sParam*>::iterator,bool> retName;
			retName = this->m_mapName.insert(pair<string,X7sParam*>(pParam->GetName(),pParamCopy));
			if(retName.second==false)
			{
				X7S_PRINT_WARN("X7sParamList::Insert()","Can not insert %s in std::map",pParam->GetName().c_str());
			}
			else
			{
				this->m_mapID.insert(pair<int,X7sParam*>(pParam->GetID(),pParamCopy));
				return  (*(retName.first)).second;
			}
		}
		X7S_PRINT_WARN("X7sParamList::Insert()","Could not copy pParam=%s",pParam->GetName().c_str());
	}
	paramNULL = X7sParam();
	return &paramNULL;
}

/**
* @brief Insert a X7sParam into the list.
*
* To easily insert a parameters to the list you should call the function like this:
*
* @code
* X7sParamList params;
* params.Insert(X7sParam("test_name",TEST_ID,"string_value")); //The best way to fill the X7sParamList.
* cout << "Value of test_name = << params["test_name"] << endl;
* @endcode
*
* @param param A reference on a X7sParam previously created
* @return @true if the X7sParam as been correctly added to the list.
* @see X7sParamList::Insert(const X7sParam* pParam)
*/
X7sParam* X7sParamList::Insert(const X7sParam& param)
{
	return Insert(&param);
}


/**
* @brief Return the number of parameters insert inside the X7sParamList.
*/
int X7sParamList::Size() const {
	return m_mapID.size();
}

/**
* @brief Return @true if the parameter is find in the list.
* @param name The name of the parameter we want to find in the list.
*/
bool X7sParamList::Exist(const std::string& name)
{
	return (m_mapName.find(name)!=m_mapName.end());
}



/**
* @brief  Obtain a X7sParam pointer in the list using its @b ID.
* @param ID The ID of the X7sParam defined during its creation.
* @return A pointer on the corresponding X7sParam or a pointer on a X7sParam with ID=X7S_XML_NULLID (@ref X7sParamNULL)
*/
X7sParam* X7sParamList::operator[] (const int& ID) {
	X7sParam* ret=m_mapID[ID];
	if(ret) return ret;
	else {
		paramNULL = X7sParam();
		X7S_PRINT_ERROR("X7sParamList::operator[]", "Can't find X7sParam with ID=%d.\n"
				"Returning X7sParam with ID=X7S_XML_NULLID (%X) and val=%s",
				ID,paramNULL.GetID(), paramNULL.toString().c_str());
		return &paramNULL;
	}
}

/**
* @brief Obtain a X7sParam pointer in the list using its @b name.
* @see X7sParamList::operator[](const std::string &name).
*
*/
X7sParam* X7sParamList::operator[] (const char *name) {
	string tmp(name);
	if(name==NULL) tmp = string("NULL");
	return (*this)[tmp];
}

/**
* @brief Obtain a X7sParam pointer in the list using its @b name.
* @param name The name of the X7sParam defined during its creation.
* @return A pointer on the corresponding X7sParam or a pointer on a X7sParam with name="null" (@ref X7sParamNULL)
*/
X7sParam* X7sParamList::operator[] (const std::string& name) {

	string name2=name;
	if(this->Exist(name)) {
		X7sParam* ret=m_mapName[name2];
		if(ret) return ret;
	}

	//If not returned return the NULL parameters.
	paramNULL = X7sParam();
	X7S_PRINT_ERROR("X7sParamList::operator[]","Can't find X7sParam with name=%s.\n"
			"Returning X7sParam with name=%s and val=%s",
			name.c_str(),paramNULL.GetName().c_str(), paramNULL.toString().c_str());
	return &paramNULL;

}


/**
* @brief Obtain the X7sParam using the previous X7sParam obtained.
*
* For example you can use it like this:
*
* @code
* 	X7sParam *pParam=NULL;
*	while((pParam=m_params.GetNextParam(pParam))!=NULL) {
*		cout << pParam->GetName() << endl;
*	}
* @endcode
*
* @param param The previous parameters (if it is NULL we will get the first parameter of the list)
* @return A pointer on a X7sParam or NULL if the list has ended.
*/
X7sParam* X7sParamList::GetNextParam(X7sParam * param) {

	//If param is NULL
	if(param==NULL)
	{
		m_it=m_mapName.begin();	//Go to the beginning of the list
	}
	else
	{
		//Otherwise set the iterator on the next value after param.
		if(param==(*m_it).second) m_it++;	//The param has the same value than the iterator, therefore go on next one.
		else {	//Otherwise loop on the list until finding the correct parameters.
			for ( m_it=m_mapName.begin() ; m_it != m_mapName.end(); m_it++ ) {
				if(param==(*m_it).second) break;
			}
		}
	}

	//Return the value define by the iterator
	if(m_it == m_mapName.end()) return NULL;	//Check if it the end of the list.
	else return (*m_it).second;	//And finally return the parameter.
}


X7sParam* X7sParamList::last()
{
	if(m_mapName.empty()) return &paramNULL;
	else {
		std::map<std::string,X7sParam*>::reverse_iterator it;
		it=m_mapName.rbegin();
		return (*it).second;
	}
}


const X7sParam* X7sParamList::last() const
{
	//	if(m_mapName.empty()) return &paramNULL;
	//	else {
	//		std::map<std::string,const X7sParam*>::const_reverse_iterator it;
	//		it=m_mapName.rbegin();
	//		return (*it).second;
	//	}
	return &paramNULL;
}


void X7sParamList::ResetIsChanged()
{
	map<string,X7sParam*>::iterator it;
	for ( it=m_mapName.begin() ; it != m_mapName.end(); it++ ) {
		X7sParam *pParam= (*it).second;
		if(pParam) pParam->ResetIsChanged();
	}
}


/**
* @brief Return a string with the value of each parameter in the X7sParamList.
*
* The parameters are printed starting from the last one added to the first one added.
*/
std::string X7sParamList::toString() {
	std::stringstream sstr;
	map<string,X7sParam*>::iterator it;
	for ( it=m_mapName.begin() ; it != m_mapName.end(); it++ ) {
		sstr << (*it).first << " => ";
		if(((*it).second)==NULL) sstr << "NULL" ;
		else 	sstr << ((*it).second)->Print();
		sstr << std::endl;
	}
	return sstr.str();
}
