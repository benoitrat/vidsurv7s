#include "ViewerWindow.h"
#include "Log.hpp"

#include <QtGui>
#include <QApplication>



int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	Log::SetLevel(X7S_LOGLEVEL_HIGHEST);


	 QLocale::setDefault(QLocale("es_ES"));
	 QString qmDir=a.applicationDirPath()+"/locale";

	 QTranslator qtTranslator;
	 qtTranslator.load("qt_"+QLocale().name(),qmDir);
	 a.installTranslator(&qtTranslator);

	 QTranslator wgtTranslator;
	 wgtTranslator.load("widgets_"+QLocale().name(),qmDir);
	 a.installTranslator(&wgtTranslator);

	 QTranslator translator;
	 translator.load("viewer_"+QLocale().name(),qmDir);
	 a.installTranslator(&translator);

	ViewerWindow win;
	win.show();
	win.Open(QString(argv[1]));
	return a.exec();
}
