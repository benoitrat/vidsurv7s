#include "ViewerWindow.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Log.hpp"
#include "ViewerWidget.h"

#include <QFileDialog>
#include <QStatusBar>
#include <QPainter>
#include <QTest>

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
ViewerWindow::ViewerWindow(QWidget *parent)
:QMainWindow(parent)
 {
	ui.setupUi(this);

	viewerWidget = new ViewerWidget(this);

	ui.mainLayout->addWidget(viewerWidget);
	ui.actionShow_Metadata->setProperty("checked",viewerWidget->isMetaDataShown());

		//	ui.frame->setStyleSheet("background-color: yellow;");
	//	ui.centralwidget->setStyleSheet("background-color:green;");
	//	 ui.wgtButton->setStyleSheet("background-color:blue;");

	connect(ui.actionOpen,SIGNAL(triggered()),this, SLOT(OpenDialog()));
	connect(ui.actionShow_Metadata,SIGNAL(toggled(bool)),viewerWidget, SLOT(showMetaData(bool)));
	connect(viewerWidget,SIGNAL(StatusMessage(const QString&)) ,ui.statusBar,SLOT(showMessage(const QString&)));

	this->setStatusBar(new QStatusBar(this));
	this->statusBar()->showMessage("Open a new file");

	Log::SetLevel(X7S_LOGLEVEL_HIGH);
 }

ViewerWindow::~ViewerWindow()
{
	delete viewerWidget;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

/**
* @brief Open the MJPEG file and return the string
* @return
*/
bool ViewerWindow::Open(const QString& filename)
{
	avoDebug("ViewerWindow::Open()") << filename;
	return viewerWidget->Open(filename);
}

