/**
*  @file
*  @brief Contains the class ViewerWindow.h
*  @date Aug 28, 2009
*  @author neub
*	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
*/

#ifndef MEDIAPLAYER_H_
#define MEDIAPLAYER_H_

class ViewerWidget;


#include "ui_ViewerWindow.h"
#include <QtGui/QMainWindow>


/**
*	@brief The ViewerWindow.
*	@ingroup avo_viewer
*/
class ViewerWindow : public QMainWindow
{
	Q_OBJECT

public:
	ViewerWindow(QWidget *parent = 0);
	virtual ~ViewerWindow();
	bool Open(const QString& filename);

private:
	Ui::ViewerWindow ui;
	ViewerWidget *viewerWidget;

public slots:
	bool OpenDialog() { return Open(QString()); };
};

#endif /* MEDIAPLAYER_H_ */
