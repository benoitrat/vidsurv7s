#!/bin/sh

lang="es"

lupdate -target-language ${lang} ../core/src/ ../core/include ../widgets/src -ts ../widgets/widgets_${lang}.ts
lupdate -target-language ${lang} src -ts viewer_${lang}.ts
linguist ../widgets/widgets_${lang}.ts & linguist viewer_${lang}.ts


