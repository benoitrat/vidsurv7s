<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>ViewerWindow</name>
    <message>
        <location filename="src/ViewerWindow.ui" line="14"/>
        <source>Advisor MJPEG Viewer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/ViewerWindow.ui" line="43"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="src/ViewerWindow.ui" line="51"/>
        <source>View</source>
        <translation>Ver</translation>
    </message>
    <message>
        <location filename="src/ViewerWindow.ui" line="60"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="src/ViewerWindow.ui" line="71"/>
        <source>Quit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="src/ViewerWindow.ui" line="79"/>
        <source>Show Metadata</source>
        <translation>Ver Metadatos</translation>
    </message>
</context>
</TS>
