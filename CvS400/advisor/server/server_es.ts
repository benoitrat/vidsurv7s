<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>EditQLabel</name>
    <message>
        <location filename="src/EditQLabel.cpp" line="80"/>
        <source>&lt;--Out</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/EditQLabel.cpp" line="82"/>
        <source>In--&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/EditQLabel.cpp" line="85"/>
        <source>&lt;--In</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/EditQLabel.cpp" line="87"/>
        <source>Out--&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QCameraServer</name>
    <message>
        <location filename="src/QCameraServer.cpp" line="120"/>
        <source>Add polygon alarms</source>
        <translation>Añadir alarmas polígono</translation>
    </message>
    <message>
        <location filename="src/QCameraServer.cpp" line="123"/>
        <source>Add line alarms</source>
        <translation>Añadir alarmas línea</translation>
    </message>
    <message>
        <location filename="src/QCameraServer.cpp" line="126"/>
        <source>Apply alarms</source>
        <translation>Aplicar alarmas</translation>
    </message>
    <message>
        <location filename="src/QCameraServer.cpp" line="130"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="src/QCameraServer.cpp" line="134"/>
        <source>Del last polygon alarm</source>
        <translation>Borrar última alarma polígono</translation>
    </message>
    <message>
        <location filename="src/QCameraServer.cpp" line="138"/>
        <source>Del all polygon alarms</source>
        <translation>Borrar todas las alarmas polígono</translation>
    </message>
    <message>
        <location filename="src/QCameraServer.cpp" line="142"/>
        <source>Del last line alarm</source>
        <translation>Borrar última alarma línea</translation>
    </message>
    <message>
        <location filename="src/QCameraServer.cpp" line="146"/>
        <source>Del all line alarms</source>
        <translation>Borrar todas las alarmas línea</translation>
    </message>
    <message>
        <source>Enable camera</source>
        <translation type="obsolete">Habilitar cámara</translation>
    </message>
    <message>
        <source>Set Visible</source>
        <translation type="obsolete">Mostrar imagenes</translation>
    </message>
    <message>
        <location filename="src/QCameraServer.cpp" line="160"/>
        <source>Enable surveillance on camera</source>
        <translation>Habilitar videovigilancia</translation>
    </message>
    <message>
        <location filename="src/QCameraServer.cpp" line="165"/>
        <source>Set camera parameters...</source>
        <translation>Configurar parámetros de la cámara...</translation>
    </message>
    <message>
        <location filename="src/QCameraServer.cpp" line="196"/>
        <source>Add Alarms...</source>
        <translation>Añadir Alarmas...</translation>
    </message>
    <message>
        <location filename="src/QCameraServer.cpp" line="523"/>
        <location filename="src/QCameraServer.cpp" line="551"/>
        <source>&lt;--Out</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/QCameraServer.cpp" line="526"/>
        <location filename="src/QCameraServer.cpp" line="554"/>
        <source>In--&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/QCameraServer.cpp" line="529"/>
        <location filename="src/QCameraServer.cpp" line="557"/>
        <source>&lt;--In</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/QCameraServer.cpp" line="532"/>
        <location filename="src/QCameraServer.cpp" line="560"/>
        <source>Out--&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Visor</name>
    <message>
        <source>Cameras</source>
        <translation type="obsolete">Cámaras</translation>
    </message>
    <message>
        <source>Alarms and Events</source>
        <translation type="obsolete">Alarmas y Eventos</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="165"/>
        <location filename="src/visor.cpp" line="618"/>
        <source>Configuration Error</source>
        <translation>Error de Configuración</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="166"/>
        <source>The configuration file is empty or incorrect</source>
        <translation>El fichero de configuración está vació o es incorrecto</translation>
    </message>
    <message>
        <source>  &amp;Load other configuration file...  </source>
        <translation type="obsolete">Cargar un fichero de configuración diferente...</translation>
    </message>
    <message>
        <source>  &amp;Create new board...  </source>
        <translation type="obsolete">Crear una &amp;Nueva Tarjeta...</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="620"/>
        <source>  &amp;Close application  </source>
        <translation>&amp;Cerrar la aplicación</translation>
    </message>
    <message>
        <source>Configuration (*.xml)</source>
        <translation type="obsolete">Configuración (*.xml)</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="167"/>
        <source>&amp;Import a configuration file</source>
        <translation>&amp;Importar Configuración</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="167"/>
        <source>&amp;Close application!</source>
        <translation>&amp;Cerrar aplicación!</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="167"/>
        <source>Create &amp;New Board</source>
        <translation>Crear &amp;Nueva Placa</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="211"/>
        <source>Database Error</source>
        <translation>Error de la Bases de Datos</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="212"/>
        <source>%1 Application will not save videos and events.</source>
        <translation>%1 La aplicación no almacenará videos ni eventos.</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="533"/>
        <source>Import XML File</source>
        <translation>Importar Fichero XML</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="533"/>
        <location filename="src/visor.cpp" line="578"/>
        <source>XML (*.xml)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="547"/>
        <source>Overwrite Configuration and Restart Application</source>
        <translation>Sobreescribir la Configuración y Reiniciar la Aplicación</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="547"/>
        <source>In order to correctly import the new configuration, the actual configuration files is going to be overwritten and the application is going to restart.
 Do you want to continue?</source>
        <translation>Para completar correctamente la importación de la nueva configuración, el fichero actual va a ser borrado y la applicación va reiniciar.
Estas seguro de continuar?</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="578"/>
        <source>Export File</source>
        <translation>Exportar Fichero</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="587"/>
        <source>File already exists</source>
        <translation>El Fichero Existe</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="587"/>
        <source>Do you want to replace the existing file: %1</source>
        <translation>Estas seguro de reemplazar el fichero actual: %1</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="619"/>
        <source>There is no board in your system. The application is going to close.</source>
        <oldsource>There is no board in your system. The Server application must be closed</oldsource>
        <translation>No existen tarjetas en el sistema. La aplicación se cerrará.</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="1009"/>
        <source>User Error</source>
        <translation>Error de Usuario</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="1010"/>
        <source>User or Password not correct</source>
        <translation>Nombre de usuario o contraseña incorrectos</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="1011"/>
        <source>  &amp;OK  </source>
        <translation></translation>
    </message>
    <message>
        <source>  &amp;Cancel  </source>
        <translation type="obsolete">&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="1018"/>
        <source>User logged</source>
        <translation>Usuario registrado</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="1018"/>
        <source>You don&apos;t have admin privileges. Contact to your security manager.</source>
        <translation>Usted no tiene derechos de administrador. Contacte con su Administrador de Seguridad.</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="1093"/>
        <source>Purge Database</source>
        <translation>Limpiar Base de Datos</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="1094"/>
        <source>Do you want to purge the database?
(Remove data older than 1 month)</source>
        <translation>¿Quiere borrar los contenidos antiguos de la base de datos?
(se eliminarán los datos con antigüedad superior a 1 mes)</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="1103"/>
        <source>Delete physical files</source>
        <translation>Borrar los ficheros</translation>
    </message>
    <message>
        <location filename="src/visor.cpp" line="1104"/>
        <source>Also delete the corresponding video the hard drive?</source>
        <translation>¿Desea borrar también los videos correspondientes del disco duro?</translation>
    </message>
</context>
<context>
    <name>VisorClass</name>
    <message>
        <location filename="src/visor.ui" line="26"/>
        <source>Visor Server by Seven Solutions</source>
        <oldsource>Visor by Seven Solutions</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="53"/>
        <source>Cameras</source>
        <translation>Cámaras</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="58"/>
        <source>Alarms and Events</source>
        <translation>Alarmas y Eventos</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="80"/>
        <source>&amp;File</source>
        <oldsource>File</oldsource>
        <translation>&amp;Fichero</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="94"/>
        <source>&amp;Users</source>
        <oldsource>Users</oldsource>
        <translation>&amp;Usuarios</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="125"/>
        <source>&amp;View</source>
        <oldsource>View</oldsource>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="141"/>
        <source>&amp;Save settings</source>
        <oldsource>Save settings</oldsource>
        <translation>&amp;Guardar configuración</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="146"/>
        <source>&amp;Configuration ...</source>
        <oldsource>Configuration ...</oldsource>
        <translation>&amp;Configuración...</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="163"/>
        <source>&amp;Hide Graphical Interface</source>
        <oldsource>Hide Graphical Interface</oldsource>
        <translation>&amp;Ocultar Interfaz Gráfica</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="171"/>
        <source>Show</source>
        <translation>Mostrar</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="182"/>
        <source>Vertical</source>
        <translation>Vertical</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="187"/>
        <source>Horizontal</source>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="192"/>
        <source>&amp;Add New Board...</source>
        <oldsource>Add New Board...</oldsource>
        <translation>&amp;Añadir nueva tarjeta...</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="197"/>
        <source>Add new server user...</source>
        <translation>Añadir nuevo usuario del Servidor...</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="202"/>
        <source>Add new client user...</source>
        <translation>Añadir nuevo usuario del Cliente...</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="207"/>
        <source>Config server users...</source>
        <translation>Configurar usuarios del Servidor...</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="215"/>
        <source>Enter Admin mode</source>
        <oldsource>Enter &amp;Admin mode</oldsource>
        <translation>Habilitar modo Administrador</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="226"/>
        <source>Exit Admin mode</source>
        <translation>Salir del modo Administrador</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="231"/>
        <source>Config client users...</source>
        <translation>Configurar usuarios del Cliente...</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="236"/>
        <source>&amp;Reconnect to all boards</source>
        <oldsource>Reconnect to all boards</oldsource>
        <translation>&amp;Reconectar con todas las tarjetas</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="241"/>
        <source>Advanced Options</source>
        <translation>Opciones Avanzadas</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="249"/>
        <source>&amp;Full Screen</source>
        <oldsource>Full Screen</oldsource>
        <translation>Pantalla &amp;Completa</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="260"/>
        <source>Test</source>
        <translation>Test</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="265"/>
        <source>&amp;Import from .xml</source>
        <oldsource>Import from .xml</oldsource>
        <translation>&amp;Importar desde .xml</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="270"/>
        <source>&amp;Export to .xml</source>
        <oldsource>Export to .xml</oldsource>
        <translation>&amp;Exportar a .xml</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="278"/>
        <source>&amp;Remove a Board</source>
        <oldsource>Remove a Board</oldsource>
        <translation>&amp;Eliminar una Tarjeta</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="283"/>
        <source>Purge Database</source>
        <translation>Limpiar Base de Datos</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="288"/>
        <source>About Us</source>
        <translation>Sobre nosotros</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="296"/>
        <source>Show Events List</source>
        <translation>Mostrar Lista de Eventos</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="301"/>
        <source>&amp;Default View</source>
        <oldsource>Default View</oldsource>
        <translation>Vista por &amp;Defecto</translation>
    </message>
    <message>
        <source>Network</source>
        <translation type="obsolete">Red</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="107"/>
        <source>&amp;Preference</source>
        <oldsource>Preference</oldsource>
        <translation>&amp;Preferencias</translation>
    </message>
    <message>
        <location filename="src/visor.ui" line="119"/>
        <source>&amp;Help</source>
        <oldsource>Help</oldsource>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <source>Save .xml file</source>
        <translation type="obsolete">Guardar fichero .xml</translation>
    </message>
    <message>
        <source>Config parameters</source>
        <translation type="obsolete">Configurar parámetros</translation>
    </message>
</context>
<context>
    <name>setBoardParams</name>
    <message>
        <location filename="src/setBoardParams.cpp" line="112"/>
        <source>Delete Board Warning</source>
        <translation>Aviso de borrado de Tarjeta</translation>
    </message>
    <message>
        <location filename="src/setBoardParams.cpp" line="112"/>
        <source>You are going to delete this board, are you sure?</source>
        <translation>¿Desea borrar esta Tarjeta?</translation>
    </message>
</context>
<context>
    <name>setBoardParamsClass</name>
    <message>
        <location filename="src/setBoardParams.ui" line="14"/>
        <source>Board Configuration</source>
        <translation>Configuración de Tarjeta</translation>
    </message>
    <message>
        <location filename="src/setBoardParams.ui" line="39"/>
        <source>Select camera for enabling</source>
        <translation>Habilitación de cámaras</translation>
    </message>
    <message>
        <location filename="src/setBoardParams.ui" line="54"/>
        <location filename="src/setBoardParams.ui" line="295"/>
        <source>Camera 1</source>
        <translation>Cámara 1</translation>
    </message>
    <message>
        <location filename="src/setBoardParams.ui" line="77"/>
        <location filename="src/setBoardParams.ui" line="269"/>
        <source>Camera 2</source>
        <translation>Camara 2</translation>
    </message>
    <message>
        <location filename="src/setBoardParams.ui" line="87"/>
        <location filename="src/setBoardParams.ui" line="243"/>
        <source>Camera 3</source>
        <translation>Cámara 3</translation>
    </message>
    <message>
        <location filename="src/setBoardParams.ui" line="110"/>
        <location filename="src/setBoardParams.ui" line="282"/>
        <source>Camera 4</source>
        <translation>Cámara 4</translation>
    </message>
    <message>
        <location filename="src/setBoardParams.ui" line="133"/>
        <source>IP Board Address:</source>
        <translation>Dirección IP de la tarjeta:</translation>
    </message>
    <message>
        <location filename="src/setBoardParams.ui" line="143"/>
        <source>UDP Board Port:</source>
        <translation>Puerto UDP de la tarjeta:</translation>
    </message>
    <message>
        <location filename="src/setBoardParams.ui" line="150"/>
        <source>RTP Board Port:</source>
        <translation>Puerto RTP de la tarjeta:</translation>
    </message>
    <message>
        <location filename="src/setBoardParams.ui" line="193"/>
        <source>Enable:</source>
        <translation>Habilitar:</translation>
    </message>
    <message>
        <location filename="src/setBoardParams.ui" line="212"/>
        <source>Select camera for showing</source>
        <translation>Selección de cámaras mostradas</translation>
    </message>
    <message>
        <location filename="src/setBoardParams.ui" line="328"/>
        <source>HW: 0.0 -  SW: 0.0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setBoardParams.ui" line="347"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setBoardParams.ui" line="367"/>
        <source>Delete Board</source>
        <translation>Borrar Tarjeta</translation>
    </message>
    <message>
        <location filename="src/setBoardParams.ui" line="387"/>
        <source>Cancel</source>
        <translation>CancelarCancelar</translation>
    </message>
</context>
<context>
    <name>setCamerasParams</name>
    <message>
        <location filename="src/setCamerasParams.cpp" line="420"/>
        <source>Standard FG/BG (Fast)</source>
        <translation>Estándar FG/BG (Rapido)</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.cpp" line="421"/>
        <source>Fast but doesn&apos;t work properly when camera is less than 5m</source>
        <translation>Algoritmo rapido que funciona mejor en exterior o cuando la camera esta a mas de 5m de los objetos a seguir</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.cpp" line="424"/>
        <source>Features + KLT (Slow)</source>
        <translation>Caracteristicas + KLT (Lento)</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.cpp" line="425"/>
        <source>Heavy computation but works well between 1-5m</source>
        <oldsource>Heavy computation but work well between 1m &amp; 5m</oldsource>
        <translation>Algoritmo pesado que funciona correctamente a distancias cortas (1-5 metros). Utilizar en algunas cameras solamente</translation>
    </message>
</context>
<context>
    <name>setCamerasParamsClass</name>
    <message>
        <location filename="src/setCamerasParams.ui" line="72"/>
        <source>Camera</source>
        <translation>Cámara</translation>
    </message>
    <message>
        <source>Enable Surveillance</source>
        <translation type="obsolete">Videovigilancia</translation>
    </message>
    <message>
        <source>Enable MJPEG Write</source>
        <translation type="obsolete">Escritura MJPEG</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="174"/>
        <source>MJPEG Restart</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="197"/>
        <source>Camera Name:</source>
        <translation>Nombre cámara:</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="219"/>
        <source>Apply Name</source>
        <translation>Aplicar Nombre</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="97"/>
        <location filename="src/setCamerasParams.ui" line="299"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="271"/>
        <source>Alarms</source>
        <translation>Alarmas</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="479"/>
        <source>Select Alarm</source>
        <translation>Seleccionar Alarma</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="446"/>
        <source>Alarm:</source>
        <translation>Alarma:</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="382"/>
        <source>Delete Alarm</source>
        <translation>Eliminar Alarma</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="353"/>
        <source>Change Color</source>
        <translation>Cambiar Color</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="41"/>
        <source>Camera Configuration</source>
        <translation>Configuración de Cámara</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="113"/>
        <source>Surveillance</source>
        <translation>Videovigilancia</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="136"/>
        <source>MJPEG Write</source>
        <translation>Almacenar MJPEG</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="240"/>
        <source>Video Folder:</source>
        <translation>Directorio de Video:</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="262"/>
        <source>Browse...</source>
        <translation>Buscar...</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="324"/>
        <source>Edit Alarm</source>
        <translation>Editar Alarma</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="404"/>
        <location filename="src/setCamerasParams.ui" line="728"/>
        <location filename="src/setCamerasParams.ui" line="907"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="427"/>
        <location filename="src/setCamerasParams.ui" line="751"/>
        <location filename="src/setCamerasParams.ui" line="927"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="489"/>
        <source>Background</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="501"/>
        <source>BackGround Params</source>
        <translation>Parámetros de Background</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="518"/>
        <source>bg_ccMinArea</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="554"/>
        <source>bg_insDelayLog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="590"/>
        <source>bg_refBGDelay</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="626"/>
        <source>bg_refFGDelay</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="662"/>
        <source>bg_threshold</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="779"/>
        <source>Tracking</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="813"/>
        <source>Tracking Algorithm</source>
        <translation>Algoritmo de Tracking</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="829"/>
        <source>Default</source>
        <translation>Defecto</translation>
    </message>
    <message>
        <source>Standard FG/BG (Fast)</source>
        <translation type="obsolete">Estándar FG/BG (Rapido)</translation>
    </message>
    <message>
        <source>Features + KLT (Slow)</source>
        <translation type="obsolete">Caracteristicas + KLT (Lento)</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="837"/>
        <source>Type:</source>
        <translation>Tipo:</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="847"/>
        <source>description</source>
        <translation>descripción</translation>
    </message>
    <message>
        <location filename="src/setCamerasParams.ui" line="887"/>
        <source>OK</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>setConfigParams</name>
    <message>
        <location filename="src/setConfigParams.ui" line="26"/>
        <source>Path Parameters</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setConfigParams.ui" line="53"/>
        <source>Video Parameters</source>
        <translation>Parámetros de Vídeo</translation>
    </message>
    <message>
        <location filename="src/setConfigParams.ui" line="70"/>
        <source>Video Path:</source>
        <translation>Directorio de Vídeo:</translation>
    </message>
    <message>
        <location filename="src/setConfigParams.ui" line="80"/>
        <location filename="src/setConfigParams.ui" line="218"/>
        <source>Browse...</source>
        <translation>Buscar...</translation>
    </message>
    <message>
        <location filename="src/setConfigParams.ui" line="117"/>
        <source>Video path is general</source>
        <translation>El directorio de Vídeo es general</translation>
    </message>
    <message>
        <location filename="src/setConfigParams.ui" line="137"/>
        <source>Video is to write</source>
        <translation>Alamacenar Vídeo</translation>
    </message>
    <message>
        <location filename="src/setConfigParams.ui" line="191"/>
        <source>Log Parameters</source>
        <translation>Parámetros de Log</translation>
    </message>
    <message>
        <location filename="src/setConfigParams.ui" line="208"/>
        <source>Log Path:</source>
        <translation>Directorio de Log:</translation>
    </message>
    <message>
        <location filename="src/setConfigParams.ui" line="242"/>
        <source>Log:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setConfigParams.ui" line="269"/>
        <source>Log cv:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setConfigParams.ui" line="292"/>
        <source>Log xml:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setConfigParams.ui" line="315"/>
        <source>Log vs:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setConfigParams.ui" line="338"/>
        <source>Lognet:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>setVisorParamsClass</name>
    <message>
        <location filename="src/setVisorParams.ui" line="35"/>
        <source>Set Visor Parameters</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setVisorParams.ui" line="70"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="src/setVisorParams.ui" line="116"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
    <message>
        <location filename="src/setVisorParams.ui" line="96"/>
        <source>Save XML file</source>
        <translation>Guardar fichero .xml</translation>
    </message>
</context>
</TS>
