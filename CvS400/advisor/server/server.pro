TEMPLATE = app
TARGET = Visor
QT += core \
    gui
SOURCES += ./src/main.cpp \
    ./src/BoardThread.cpp \
    ./src/ClientSocket.cpp \
    ./src/QBoard.cpp \
    ./src/QCamera.cpp \
    ./src/QServer.cpp \
    ./src/Thread_Play.cpp \
    ./src/visor.cpp \
    ./src/setCamerasParams.cpp \
    ./src/setBoardParams.cpp \
    ./src/setVisorParams.cpp \
    ./src/EditQLabel.cpp \
    ./src/setConfigParams.cpp \
    D:/Proyectos_QT/AdVisor/CvS400/advisor/widgets/src/usersConfigParams.cpp \
    D:/Proyectos_QT/AdVisor/CvS400/advisor/widgets/src/addNewUser.cpp
FORMS += ./src/visor.ui \
    ./src/setCamerasParams.ui \
    ./src/setBoardParams.ui \
    ./src/setVisorParams.ui \
    ./src/setConfigParams.ui \
    ./src/ViewerWidget.ui \
    D:/Proyectos_QT/AdVisor/CvS400/advisor/widgets/src/AboutUsWidget.ui \
    D:/Proyectos_QT/AdVisor/CvS400/advisor/widgets/src/usersConfigParams.ui \
    D:/Proyectos_QT/AdVisor/CvS400/advisor/widgets/src/usersConfigParams.ui \
    D:/Proyectos_QT/AdVisor/CvS400/advisor/widgets/src/addNewUser.ui
HEADERS += ./src/setConfigParams.h \
    ./src/addNewUser.h \
    ./src/usersConfigParams.h \
	./src/BoardThread.h \
	./src/ClientSocket.h \
	./src/QBoard.h \
	./src/QCamera.h \
	./src/QServer.h \
	./src/Thread_Play.h \
	./src/visor.h \
	./src/EditQLabel.h \
	./src/setCamerasParams.h \
	./src/setBoardParams.h \
	./src/setVisorParams.h
