#include "QBoard.h"
#include "vv_defines.h"
#include "Log.hpp"
#include "BoardThread.h"
#include "Thread_Play.h"
#include "Board.hpp"
#include "QCameraServer.h"
#include "Camera.hpp"

#include <QtDebug>

QBoard::QBoard(QObject *parent, Board *board_ptr, quint8 index)
    : QObject(parent)
{
	X7S_FUNCNAME("QBoard::QBoard()");

	AVO_PRINT_DEBUG(funcName,"board_ID=%d in creation...",board_ptr->GetID());
	board = board_ptr;
	board_index = index;
	play_thread = new Thread_Play(this, board);
	board_thread = new BoardThread(this, board, play_thread);
	for(int i = 0; i < 4; i++){
		camera[i] = new QCameraServer(parent, board->GetCamera(i), /*4*board_index + */quint8(i));
		//camera[i]->setGeometry(QRect(0, 0, PIXELS_PER_LINE, LINES_PER_FRAME));
		connect(camera[i], SIGNAL(cameraEnabled(bool, int)),
									this, SLOT(enableCamera(bool, int)));
		connect(camera[i], SIGNAL(cameraVisible(bool, int)),
											this, SLOT(visibleCamera(bool, int)));
		connect(camera[i], SIGNAL(warningOnCamera(const QString&)),
							this, SLOT(warningOnBoard(const QString&)));

		connect(play_thread,SIGNAL(newFrameReceived(Camera*)),camera[i],SLOT(transmitNewFrameReceived(Camera*)));
		/*if (board->getCamera(i)->GetVidSurvPipeline()->GetParam("enable")->toIntValue() == 1)
			enableCameraSurveillance(true, quint8(i));*/
	}
	connect(play_thread, SIGNAL(CameraImageReceived(const QImage &, const QImage &, int)),
			this, SLOT(CameraImageReceived(const QImage &, const QImage &, int)));

	connect(play_thread, SIGNAL(CameraMJPEGReceived(const QByteArray &, int)),
			this, SLOT(CameraMJPEGReceived(const QByteArray &, int)));

	connect(play_thread, SIGNAL(TimeoutErrorSignal(const QString &)),
				this, SLOT(warningOnBoard(const QString &)));
	connect(board_thread, SIGNAL(WarningOnBoardThreadSignal(const QString &)),
				this, SLOT(warningOnBoard(const QString &)));

	AVO_PRINT_DEBUG(funcName,"board_ID=%d is created!",board->GetID());
}


QBoard::~QBoard()
{
//	AVO_PRINT_DEBUG("QBoard::~QBoard","board_ID= %d",board->GetID());


//	board_thread->ExecuteCommand(AVO_BCOMMAND_CLOSE);
	delete play_thread;
	delete board_thread;

//	delete qserver;
	//delete play_thread;

	for(int i = 0; i < AVO_BOARD_NUMCAM; i++){
		X7S_DELETE_PTR(camera[i]);
	}

	//delete board;

//	AVO_PRINT_DEBUG("QBoard::~QBoard","board_ID= %d END",board->GetID());
}

void QBoard::Board_Stop()
{
	//play_thread->StopPlay();
	board_thread->ExecuteCommand(AVO_BCOMMAND_STOP);
}

void QBoard::Board_Play()
{
	board_thread->ExecuteCommand(AVO_BCOMMAND_PLAY);

}
void QBoard::Board_Close()
{
	board_thread->ExecuteCommand(AVO_BCOMMAND_CLOSE);

}
void QBoard::enableCamera(bool enable, int camera_index)
{
	if (camera_index > 3) 
		return;
//	camera[camera_index]->enableQCamera(enable);
	emit cameraEnabledSignal();
	if (enable){
	    board->GetCamera(camera_index)->SetParam("enable", "1");
	    board_thread->ExecuteCommand(AVO_BCOMMAND_UPDATE);
	}
	else{
		board->GetCamera(camera_index)->SetParam("enable", "0");
		board_thread->ExecuteCommand(AVO_BCOMMAND_UPDATE);
	}
}

void QBoard::visibleCamera(bool enable, int camera_index)
{
	if (camera_index > 3)
		return;
	emit cameraEnabledSignal();
}

void QBoard::enableCameraSurveillance(bool enable, quint8 camera_index)
{
	if (camera_index > 3) 
		return;
	camera[camera_index]->enableSurveillance(enable);
}

void QBoard::addNewAlarm(QByteArray &alarm_array, quint8 camera_index)
{
	if (camera_index > 3)
		return;
	QByteArray array = QByteArray(alarm_array);
    camera[camera_index]->addNewAlarmFromClient(array);
}


void QBoard::enableGraphicalMode(bool enable)
{
	play_thread->EnableGraphicalMode(enable);
	for(unsigned int i = 0; i < 4; i++){
		board->GetCamera(i)->DrawFrame(enable);
		camera[i]->setDrawable(enable);
	}
}

void QBoard::SetAdminMode(bool enable)
{
	for(unsigned int i = 0; i < 4; i++){
		camera[i]->SetAdminMode(enable);
	}
}

void QBoard::CameraImageReceived(const QImage &image, const QImage &image_m, int camera_numb)
{
	if (camera_numb > 3)
		return;
	camera[camera_numb]->updateImage(image, image_m);
}

void QBoard::CameraMJPEGReceived(const QByteArray &metaJPEG, int camera_numb)
{
	if (camera_numb > 3)
		return;
	camera[camera_numb]->updateMJPEG(metaJPEG);
}

void QBoard::warningOnBoard(const QString &warning)
{
	emit warningOnBoardSignal(warning);
/*
	if (board_thread->GetBoardConnected()){
		if(board->IsAlive(-1)){
			emit warningOnCameraSignal("Board is alive");
			board_thread->ExecuteCommand(AVO_BCOMMAND_CONNECT);
			board_thread->ExecuteCommand(AVO_BCOMMAND_PLAY);
			return;
		}
	}
	emit warningOnCameraSignal("Board isn't alive");
	board_thread->ExecuteCommand(AVO_BCOMMAND_STOP);
	board_thread->ExecuteCommand(AVO_BCOMMAND_CONNECT);*/
}

