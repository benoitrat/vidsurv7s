/*
 * EditQLabel.cpp
 *
 *  Created on: 14/09/2009
 *      Author: Gabri
 */

#include <QPainter>
#include <QColor>

#include "EditQLabel.h"
#include "QtBind.hpp"
#include "vv_defines.h"

EditQLabel::EditQLabel(QWidget *parent, const QImage &image_b, const QVariant &alarm, const QVariant &color)
    : QLabel(parent)
{

    addpolygon = false;
    addline	= false;
    drawpolygon = false;
    polygon_index = 0;
    line_index = 0;
    polygon_size = 0;
    point_edit_index = 0;
    point_selected = false;

    image_v = QImage(image_b);//.copy(0,0,PIXELS_PER_LINE, LINES_PER_FRAME);

    alarm_color = color.value<QColor>();

    if (alarm.type() == QVariant::Line){
    	line = alarm.toLine();
    	last_line_point[0] = line.p1();
    	last_line_point[1] = line.p2();
    	addline = true;
    }
    if (alarm.type() == QVariant::Polygon){
    	polygon = alarm.value<QPolygon>();
    	polygon_size = polygon.size();
    	addpolygon = true;
    }
    addingAlarm = true;

    this->setGeometry(QRect(40, 160, 361, 281));

    //setCameraParam = new setCameraParams(0, this);

//	createActions();
//	createMenus();

}

EditQLabel::~EditQLabel()
{
//	delete setCameraParam;
//	if (last_image)
	//	delete last_image;
}

void EditQLabel::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setCompositionMode(QPainter::CompositionMode_SourceOver);

	painter.drawImage(0,0, image_v);

	if(addpolygon)
    	if(!polygon.isEmpty()){
    		painter.setPen(QPen(alarm_color, 3, Qt::DashDotLine, Qt::RoundCap));
    		painter.drawPolygon(polygon);
    	}

    if(addline){
		painter.setPen(QPen(alarm_color, 3, Qt::DashDotLine, Qt::RoundCap));
    	painter.drawPolyline (last_line_point, 2);
    	QPoint point = (last_line_point[1] + last_line_point[0])/2;
    	point.setX(point.x() - 50);
    	if (last_line_point[1].y() >= last_line_point[0].y()){
    		painter.drawText(point, tr("<--Out"));
    		point.setX(point.x() + 70);
    		painter.drawText(point, tr("In-->"));
    	}
    	else{
    		painter.drawText(point, tr("<--In"));
    		point.setX(point.x() + 70);
    		painter.drawText(point, tr("Out-->"));
    	}
    }

}

void EditQLabel::mouseDoubleClickEvent ( QMouseEvent * event )
{
	if (event->button() == Qt::LeftButton){

		if (addpolygon){
			if (point_selected == false){
				point_selected = true;
				setMouseTracking(true);
				QPoint point = event->pos();
				//Calculamos las distancias a todos los puntos del poligono
				quint32 dp_min = 0xFFFFFFFF;
				for(quint16 i = 0; i < polygon_size; i++){
					QPoint point_p = polygon.point(i);
					qint32 dp = abs(point.x() - point_p.x()) + abs(point.y() - point_p.y());
					if (dp_min > dp){//Estamos editando el punto 2
						point_edit_index = i;
						dp_min = dp;
					}
				}
			} else{
				point_selected = false;
				setMouseTracking(false);
				polygon.setPoint(point_edit_index, event->pos());
			}
			setCursor(Qt::ArrowCursor);
			update();
			return;
		}

		if (addline){
			if (point_selected == false){
				point_selected = true;
				setMouseTracking(true);
				QPoint point = event->pos();
				qint32 dp1 = abs(point.x() - line.p1().x()) + abs(point.y() - line.p1().y());
				qint32 dp2 = abs(point.x() - line.p2().x()) + abs(point.y() - line.p2().y());
				if (dp1 > dp2){//Estamos editando el punto 2
					point_edit_index = 1;
				}
				else{
					point_edit_index = 0;
				}
			} else{
				point_selected = false;
				setMouseTracking(false);
				last_line_point[point_edit_index] = event->pos();
				line.setP1(last_line_point[0]);
				line.setP2(last_line_point[1]);
			}
			setCursor(Qt::ArrowCursor);
			update();
			return;
		}
	}
}
void EditQLabel::mouseMoveEvent ( QMouseEvent * event )
{
	if (point_selected){
		if (addline)
			last_line_point[point_edit_index] = event->pos();
		if (addpolygon)
			polygon.setPoint(point_edit_index, event->pos());
		update();
	}
}
