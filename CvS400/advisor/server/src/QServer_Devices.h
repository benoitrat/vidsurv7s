#ifndef QSERVER_DEVICES_H_
#define QSERVER_DEVICES_H_

#include <QTcpServer>
#include <QtNetwork>
#include <QList>
#include <QString>

class Devices;

class QServer_Devices : public QTcpServer
{
    Q_OBJECT

public:
    QServer_Devices(QObject *parent = 0, Devices *devices_ptr = 0, QList<QString> *userList_pt = 0);
    ~QServer_Devices();
signals:
    void newUserLoggedSignal(const QString & name);
    void SaveXMLFileSignal();
    void SaveXMLTmpFileSignal();
protected slots:
    void incomingConnection(int socketId);
    void newUserLogged(const QString & name);
    void SaveXMLFile();
    void SaveXMLTmpFile();

protected:
    Devices *devices;
    QList<QString> *userList;
 
};

#endif /*QSERVER_DEVICES_H_*/
