#ifndef QBOARD_H_
#define QBOARD_H_


#include <QTcpServer>
//#include <QtGui/QWidget>
#include <QByteArray>
#include <QImage>
#include <QMutex>
#include "Core.hpp"
#include "Board.hpp"

class BoardThread;
class Thread_Play;
class QCameraServer;
class Camera;
//class QServer;


class QBoard : public QObject
{
    Q_OBJECT

public:
	QBoard(QObject *parent= 0, Board *board_ptr = 0, quint8 index = 0);
    ~QBoard();
    
//    Thread_Play *play_thread;
//    QCameraServer	*camera[4];
signals:
    void cameraEnabledSignal();
    void warningOnBoardSignal(const QString &warning);

public slots:
	void Board_Stop();
	void Board_Play();
	void Board_Close();
	void enableCamera(bool enable, int camera_index);
	void enableCameraSurveillance(bool enable, quint8 camera_index);
	void addNewAlarm(QByteArray &alarm_array, quint8 camera_index);
	Board* getBoard(){return board;};
	QCameraServer * getQCamera(unsigned int index){return camera[index&(AVO_BOARD_NUMCAM-1)];};
	void enableGraphicalMode(bool enable);
	quint8 getBoardIndex(){return board_index;};
	void CameraImageReceived(const QImage &image, const QImage &image_m, int camera_numb);
	void CameraMJPEGReceived(const QByteArray &metaJPEG, int camera_numb);
	void SetAdminMode(bool enable);

private slots:
	void warningOnBoard(const QString &warning);
	void visibleCamera(bool enable, int camera_index);


private:
    BoardThread *board_thread;
    Thread_Play *play_thread;
    Board	*board;
    quint8 board_index;
//    QServer *qserver;
    QCameraServer	*camera[AVO_BOARD_NUMCAM];
    QMutex mutex;
};

#endif /*QBOARD_H_*/
