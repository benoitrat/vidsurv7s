#ifndef QCAMERATIMEOUTTHREAD_H_
#define QCAMERATIMEOUTTHREAD_H_


#include <QThread>

class QCameraTimeoutThread: public QThread {

	Q_OBJECT

public:
	QCameraTimeoutThread(QObject *parent=0, quint8 time_out = 10);
	~QCameraTimeoutThread();
	void SetTimeout(quint8 time_out){ timeout = time_out;};
	quint8 GetTimeout(){return timeout;};
	void NewImage(){newImage = true;};
	//bool IsTimeoutEnable(){return in_progress;};
	void StartTimeout();
	void StopTimeout();

signals:
	void TimeoutError();

	
protected:
    void run();

private:
    bool in_progress;
    bool stop;
    bool newImage;
    quint8 timeout;
};


#endif /*QCAMERATIMEOUTTHREAD_H_*/
