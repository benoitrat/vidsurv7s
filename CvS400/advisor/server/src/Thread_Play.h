#ifndef THREAD_PLAY_H_
#define THREAD_PLAY_H_

class Camera;
class QCameraTimeoutThread;

#include <QThread>
#include <QImage>
#include "Board.hpp"

class Thread_Play: public QThread {

	Q_OBJECT

public:
	Thread_Play(QObject *parent=0, Board *board_ptr = 0);
	~Thread_Play();
	void SetBoard(Board *brd);
	bool ExecutePlay();
	void StopPlay();
	void EnableGraphicalMode(bool enable){graphical_mode = enable;}


signals:
	void CameraImageReceived(const QImage &image, const QImage &image_m, int camera_index);
//	void Cam2_ImageReceived(const QImage &image, const QImage &image_m);
//	void Cam3_ImageReceived(const QImage &image, const QImage &image_m);
//	void Cam4_ImageReceived(const QImage &image, const QImage &image_m);
	void CameraMJPEGReceived(const QByteArray &metaJPEG, int camera_index);
	void recordingLEDToggled();
	void newFrameReceived(Camera *cam);
	void TimeoutErrorSignal(const QString &error);
	
protected:
    void run();

protected slots:
	void TimeoutError();

private:
    bool in_progress;
    bool stop;
    bool graphical_mode;
    Board *board;
    QCameraTimeoutThread *timeout;
   //quint8 board_index;
    //Devices *devices;
};


#endif /*THREAD_PLAY_H_*/
