/*
 * setBoardParams.cpp
 *
 *  Created on: 08/09/2009
 *      Author: Gabri
 */
#include <QDebug>
#include "setVisorParams.h"
#include "setBoardParams.h"
#include "setCamerasParams.h"
#include "setConfigParams.h"
#include "Devices.hpp"
#include "QBoard.h"
#include "Board.hpp"
#include "QCameraServer.h"
#include "Camera.hpp"
#include "QtBind.hpp"
#include "Root.hpp"
#include "LogConfig.hpp"
#include "VideoHandlerScheduleWidget.h"

#include "Log.hpp"

setVisorParams::setVisorParams(QDialog *parent, Root *root_p, QVector<QBoard *> *qboards_pt)
    : QDialog(parent)
{
	ui.setupUi(this);

	root = root_p;
	qboards = qboards_pt;

	configParams = new setConfigParams(this, root);

	video_schdler = new VideoHandlerScheduleWidget(this, root->videoHandler());
	//Attach the database for all the camera.
	int index_stack = 0;

	for(int i=0;i<qboards->size();i++)
	{
		QBoard *qboard = qboards->at(i);
		Q_ASSERT(qboard);
		Board *board = qboard->getBoard();
		Q_ASSERT(board);

		ui.listWidget->addItem("Board " + QString::number(board->GetSpecialID()));
		//Board *board = devices->GetBoard(i);
		boardParams.append(new setBoardParams(0, qboards->at(i)));
		connect(boardParams.last(), SIGNAL(DeleteBoardSignal(int)), this, SLOT(DeleteBoard(int)));
		avoDebug("setVisorParams::setVisorParams()") << "Stack Board Index:"<< ui.stackedWidget->insertWidget(index_stack++, boardParams.at(i));
		for(int j=0;j<4;j++)
		{
			QCameraServer *qcamera = qboards->at(i)->getQCamera(j);
			cameraParams.append(new setCamerasParams(0, qcamera));
			ui.listWidget->addItem("  -" + qcamera->getCamera()->GetName());
			cameraParams.at(4*i +j)->VideoPathIsGeneral(root->devices()->GetParam("video_path_isgeneral")->toIntValue() == 1);
			cameraParams.at(4*i +j)->WriteJPEGIsGeneral(root->devices()->GetParam("video_istowrite")->toIntValue() == 1);
			connect(configParams, SIGNAL(VideoPathIsGeneralSignal(bool)), cameraParams.at(4*i +j), SLOT(VideoPathIsGeneral(bool)));
			connect(configParams, SIGNAL(WriteJPEGIsGeneralSignal(bool)), cameraParams.at(4*i +j), SLOT(WriteJPEGIsGeneral(bool)));

			avoDebug("setVisorParams::setVisorParams()") <<"Stack Camera Index:"<< ui.stackedWidget->insertWidget(index_stack++, cameraParams.at(4*i +j));
		}
	}

	ui.listWidget->addItem("Config");
	ui.stackedWidget->insertWidget(index_stack++, configParams);


	ui.listWidget->addItem("Video Scheduler");
	ui.stackedWidget->insertWidget(index_stack++, video_schdler);

	connect(ui.listWidget, SIGNAL(currentRowChanged(int)),ui.stackedWidget, SLOT(setCurrentIndex(int)));
	ui.listWidget->setCurrentRow(0);

	connect(ui.apply_pushButton, SIGNAL(clicked()), this, SLOT(ApplyNewValues()));
	connect(ui.saveXML_pushButton, SIGNAL(clicked()), this, SLOT(SaveXML_File()));
	ui.saveXML_pushButton->updateGeometry();
}

setVisorParams::~setVisorParams()
{
	for(int i=0;i<boardParams.size();i++)
	{
		if(boardParams[i]) delete boardParams[i];
/*		for(int j=0;j<cameraParams.size();j++){
			QCameraServer *qcamera = qboards->at(i)->getQCamera(j);
			qcamera->enableQCamera(qboards->at(i)->getBoard()->getCamera(j)->GetParam("enable")->toIntValue() == 1);
		}*/
	}
	for(int i=0;i<cameraParams.size();i++)
	{
		if(cameraParams[i]) delete cameraParams[i];
	}
	delete configParams;
	delete video_schdler;
}

void setVisorParams::ApplyNewValues()
{
	for(int i=0;i<boardParams.size();i++)
	{
		if(boardParams[i])
			boardParams.at(i)->ApplyNewValues();
	}
	for(int i=0;i<cameraParams.size();i++)
	{
		if(cameraParams[i]){

			cameraParams.at(i)->ApplyNewValues();
		}
	}
	configParams->ApplyNewValues();
	video_schdler->applyParam();
}

void setVisorParams::SaveXML_File()
{
	ApplyNewValues();
	emit SaveXML_FileSignal();
	done(0);
}
void setVisorParams::DeleteBoard(int board_index)
{
	emit DeleteBoardSignal(board_index);
	done(0);
}
