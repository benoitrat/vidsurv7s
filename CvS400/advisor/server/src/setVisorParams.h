/*
 * setBoardParams.h
 *
 *  Created on: 08/09/2009
 *      Author: Gabri
 */

#ifndef SETVISORPARAMS_H_
#define SETVISORPARAMS_H_

#include <QDialog>
#include "ui_setVisorParams.h"

class setBoardParams;
class setCamerasParams;
class setConfigParams;
class VideoHandlerScheduleWidget;
class Devices;
class QBoard;
class Root;

class setVisorParams : public QDialog
{
    Q_OBJECT

public:
    setVisorParams(QDialog *parent = 0, Root *root_p = 0, QVector<QBoard *> *qboards_pt = 0);
    ~setVisorParams();


signals:
    void SaveXML_FileSignal();
    void DeleteBoardSignal(int board_index);

private:
    Ui::setVisorParamsClass ui;

    //Devices *devices;

    QVector<QBoard *> *qboards;

    QVector<setBoardParams*>	boardParams;
    QVector<setCamerasParams*>	cameraParams;
    setConfigParams *configParams;
    Root	*root;
    VideoHandlerScheduleWidget *video_schdler;

private slots:
	void ApplyNewValues();
	void SaveXML_File();
	void DeleteBoard(int board_index);
};

#endif /* SETVISORPARAMS_H_ */
