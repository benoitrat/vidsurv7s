#ifndef _QCAMERASERVER_H
#define _QCAMERASERVER_H

//#include <QtGui/QWidget>

#include <QPixmap>
#include <QMenu>
#include <QAction>
#include <QMouseEvent>
#include <QByteArray>
class QCameraTimeoutThread;

#include <x7svidsurv.h>
#include "Core.hpp"

#include "QCamera.h"


class setCamerasParams;
class QServer;
class Camera;

#define MAX_POLYGONS 10
#define MAX_LINES	 10

class QCameraServer : public QCamera
{
    Q_OBJECT

public:
        QCameraServer(QObject *parent, Camera *cam, int index);
    ~QCameraServer();

    quint8 getCameraIndex(){return camera_index;};
    QPixmap*  getPixmap(){return &pixmap;}
    void SetAdminMode(bool enable);
    quint8 GetWatchDogTime();

signals:
    void cameraEnabledSurveillance(bool enable, int index);
    void backGroundParamsChanged();
    void imageToClient(const QByteArray &metaJPEG);
    void newAlarmToClient(const QByteArray &alarm_array);
    void sendDeleteAlarmToClient(int ID);

public slots:
    void setName(const QString &name_pt) { name = name_pt; };
    void enableQCamera(bool enable);//{enabled = enable; update();};
    void enableSurveillance(bool enable);
 //   void setName(QString &name_pt){name = name_pt;};
    void addNewAlarmFromClient(QByteArray &alarm_array);
    void updateImage(const QImage &image, const QImage &image_m);
    void updateMJPEG(const QByteArray &metaJPEG);
 //   void SetWatchDogTime(quint8 time);
    void setDrawable(bool enable);
    void StartServerSocket(QList<QString> *userList_pt);
	void transmitNewFrameReceived(Camera *cam);
	void DeleteAlarmFromClient(quint8 ID){pAlarms->Delete(ID);};
	void DeleteAlarm(quint8 ID){pAlarms->Delete(ID); sendDeleteAlarmToClient(ID);};
	void updateStatistic();
	void SetStatisticTime(quint32 time){timeout_ms = time;};
	QSize GetQCameraSize(){return qcamera_size;};



protected:
    virtual void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseDoubleClickEvent (QMouseEvent * event );
    void mouseMoveEvent (QMouseEvent * event );

private slots:
        void addPolygon();
        void addLine();
        void apply();
        void cancel();
        void delLastPolygon();
        void delAllPolygons();
        void delLastLine();
        void delAllLines();
        void createActions();
        void createMenus();
        void addAlarm();
        void exitAddAlarm();
        void setCameraParam();
//      void emitBackGroundParamsChanged();
//        void TimeoutError();


public:

private:
    QPixmap pixmap;
    QPoint last_line_point[2];
    QPolygon polygon[MAX_POLYGONS];
    QLine line[MAX_LINES];
    unsigned int polygon_index;
    unsigned int last_polygon_index;
    unsigned int polygon_size;
    unsigned int line_index;
    unsigned int last_line_index;

    QMenu       *menu_enable;
//    QAction *enableCameraAction;
//    QAction *setVisibleAction;
  //  QAction *disableCameraAction;
    QAction *enableSurveillanceAction;
 //   QAction *disableSurveillanceAction;
    QAction *addAlarmAction;
    QAction *setCameraParamsAction;


    bool        first_image_received;
    bool        admin_mode;

    X7sVSAlrManager *pAlarms;

    quint8 board_index;
    QServer *qserver;

    quint32	frame_add;
    quint32 timeout_ms;
    QString frame_rate;
    QSize 	qcamera_size;
//    QCameraTimeoutThread *timeout;

};

#endif // QCamera_H
