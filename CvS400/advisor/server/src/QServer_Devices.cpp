#include "QServer_Devices.h"
#include "ClientSocket_Devices.h"
#include "Devices.hpp"
#include <QtNetwork>

QServer_Devices::QServer_Devices(QObject *parent, Devices *devices_ptr, QList<QString> *userList_pt)
    :QTcpServer(parent)
{	   
    	devices = devices_ptr;
    	userList = userList_pt;
}

QServer_Devices::~QServer_Devices()
{

}

void QServer_Devices::incomingConnection(int socketId)
{
    ClientSocket_Devices *socket = new ClientSocket_Devices(this, devices, userList);
    socket->setSocketDescriptor(socketId);
    connect(socket, SIGNAL(newUserLoggedSignal(const QString&)), this, SLOT(newUserLogged(const QString &)));
    connect(socket, SIGNAL(SaveXMLFileSignal()), this, SLOT(SaveXMLFile()));
    connect(socket, SIGNAL(SaveXMLTmpFileSignal()), this, SLOT(SaveXMLTmpFile()));
 //   emit newConnection ();  
    
}
void QServer_Devices::SaveXMLFile()
{
	emit SaveXMLFileSignal();
}

void QServer_Devices::SaveXMLTmpFile()
{
	emit SaveXMLTmpFileSignal();
}

void QServer_Devices::newUserLogged(const QString &name)
{
	emit newUserLoggedSignal("User name: " + name + " logged.");
}
