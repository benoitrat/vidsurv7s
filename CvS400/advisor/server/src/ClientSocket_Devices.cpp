#include "ClientSocket_Devices.h"
#include <QtNetwork>
//#include <QSettings>

#include "Devices.hpp"
#include "Log.hpp"
#include <QMessageBox>

#include "vv_defines.h"
   
ClientSocket_Devices::ClientSocket_Devices(QObject *parent, Devices *devices_ptr, QList<QString> *userList_pt)
    : QTcpSocket(parent)
{
	QObject::connect(this, SIGNAL(readyRead()), this, SLOT(readClient()));
	QObject::connect(this, SIGNAL(disconnected()), this, SLOT(deleteLater()));
	QObject::connect(this, SIGNAL(disconnected()), this, SLOT(error()));
	devices = devices_ptr;
	nextBlockSize = 0;
	user_logged = false;
	clientUsers = userList_pt;

	avoDebug("ClientSocket_Devices::ClientSocket_Devices()") << this->peerAddress().toString();

}

ClientSocket_Devices::~ClientSocket_Devices()
{
	avoDebug("ClientSocket_Devices::~ClientSocket_Devices()") <<this->peerName() << this->peerAddress().toString()
			<< this->state() << this->errorString() << QAbstractSocket::error();
	this->close();
}

void ClientSocket_Devices::sendXMLFileToClient()
{

	emit SaveXMLTmpFileSignal();//Pedimos grabar el fichero XML antes de enviarlo.

	QByteArray block;
    quint32 size;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('F') << quint8(AVO_SCOMMAND_SEND_XML_FILE);

/*
    QSettings *settings = new QSettings(AVO_SERV_CONFIG_INI, QSettings::IniFormat);
    QString xmlFile = settings->value("application/xml_config_file", AVO_SERV_CONFIG_XML).toString();
    delete settings;
*/
    QFile file("advisor_tmp.xml"/*xmlFile*/);
    bool fileOk=file.open(QIODevice::ReadOnly | QIODevice::Text);
    AVO_CHECK_WARN("ClientSocket_Devices::sendXMLFileToClient()",fileOk,,"File could not open");

    //Leemos el n�mero de l�neas
    quint16 num_lines = 0;
    while (!file.atEnd()) {
        QByteArray line = file.readLine();
        num_lines++;
    }
    out << num_lines;//Escribimos n�mero de l�neas que componen el fichero
    
//    file.QIODevice::seek(0);
    file.close();
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;
//    QTextStream in(&file);
    while (!file.atEnd()) {
    	QString line = file.readLine();
    	out << line;
    }
    
    out.device()->seek(0);
    size = quint32(block.size() - sizeof(quint32));
    out << size;
    write(block);	
}

void ClientSocket_Devices::sendUserLoggedToClient(bool logged)
{
	X7S_FUNCNAME("ClientSocket_Devices::sendUserLoggedToClient()");


	avoDebug(funcName) << "begin";
	QByteArray block;
    quint32 size;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('U') << quint8(AVO_SCOMMAND_USER_LOGGED) << logged << user_level;

    out.device()->seek(0);
    size = quint32(block.size() - sizeof(quint32));
    out << size;
    write(block);

    avoDebug(funcName) << "end: " << logged;
}

void ClientSocket_Devices::readClient()
{
	X7S_FUNCNAME("ClientSocket_Devices::readClient()");


    QDataStream in(this);
    in.setVersion(QDataStream::Qt_4_3);
    quint32 bytesAvailables;
	while(true){
	   if (nextBlockSize == 0) {
	        if (this->bytesAvailable() < sizeof(quint32))
	            break;
	        in >> nextBlockSize;
	   }   
	    //in >> nextBlockSize;
	    if (this->bytesAvailable() < nextBlockSize)
	        break;

	    bytesAvailables = this->bytesAvailable();
	    
	    quint8 command_type;
	    quint8 command;
	    

	    in >> command_type >> command;
	    
	    if (command == AVO_SCOMMAND_USER_LOG){
	    	user_logged = false;
	    	user_level = -1;
	    	in >> user_name >> user_password;// >> user_level;
	    	avoDebug(funcName) << "User: " << user_name << " Password: " << user_password;
	    	if (clientUsers->size() > 2)
				for(int i = 0; i < clientUsers->size()- 2; i++)
				{
					if (user_name == clientUsers->at(i)){// && (user_password == clientUsers->at(i+1))){
						QCryptographicHash *hash = new QCryptographicHash(QCryptographicHash::Sha1);
						QByteArray data = QByteArray();
						data.append(user_name);
						data.append(user_password);
						data.append(clientUsers->at(i+2));
						hash->addData(data);
						QString hash_result = QString(hash->result().toHex());
						avoDebug(funcName) << "Hash-1 result: " << hash_result;

						if (hash_result == clientUsers->at(i+1)){
							user_logged = true;
							user_level = clientUsers->at(i+2).toInt();
							emit newUserLoggedSignal(user_name);
							break;
						}
					}
				}
	    	avoDebug(funcName) << "Logged: " << user_logged;
	    	if(user_logged == false)
	    		user_level = -1;
	    	sendUserLoggedToClient(user_logged);
	    }

	    if (user_logged == true)
			switch (command_type){
				case('U'):
					if(command ==AVO_SCOMMAND_USER_NO_ADMIN)
						user_level = 2;//Visor only
						break;
				case('F'):
					switch(command){
						case(AVO_SCOMMAND_SEND_XML_FILE):
							sendXMLFileToClient();
						break;
						case(AVO_SCOMMAND_SAVE_XML_FILE):
							emit SaveXMLFileSignal();
						break;
						default:
							break;
					}
				break;
				case('B'):
					switch(command){
						case(AVO_SCOMMAND_PLAY):
							devices->Start();
						break;
						case(AVO_SCOMMAND_STOP):
							devices->Stop();
						break;
						default:
							break;
					}
				break;
				default:
					break;
			}
	    nextBlockSize = 0;
	}
//    close();
}

void ClientSocket_Devices::error()
{
//    if (server_connected){
    avoError("ClientSocket_Devices::error()") <<
        		 this->peerAddress().toString() << this->state() <<
        		 this->errorString() << QAbstractSocket::error();
/*	QMessageBox::warning(0,tr("Error. Connecting to client."),
		this->errorString(),
		QMessageBox::Yes|QMessageBox::Default,
		QMessageBox::NoButton, QMessageBox::NoButton);
    	//this->close();
//    }*/
}


