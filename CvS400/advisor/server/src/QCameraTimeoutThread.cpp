/*
 * QCameraTimeoutThread.cpp
 *
 *  Created on: 16/10/2009
 *      Author: Gabri
 */

#include "QCameraTimeoutThread.h"

#include "Log.hpp"

QCameraTimeoutThread::QCameraTimeoutThread(QObject *parent, quint8 time_out)
	: QThread(parent)
{
	in_progress = false;
	stop = false;
	timeout = time_out;
}

QCameraTimeoutThread::~QCameraTimeoutThread()
{
	stop = true;
	if (in_progress)
		wait();
}

void QCameraTimeoutThread::StartTimeout()
{
	newImage = true;
	if (in_progress)
		//wait();
		return;
	stop = false;
	//newImage = true;
	start();
}

void QCameraTimeoutThread::StopTimeout()
{
	stop = true;
}

void QCameraTimeoutThread::run()
{
	in_progress = true;

	while(stop==false)
	{
		for(quint8 i = 0; i < timeout; i++){
			sleep(1);
			if(stop){//Comprobamos el estado de stop cada segundo para no retrasar en
				in_progress = false;//timeout segundos el cierre de la aplicacion
				return;
			}

		}
		if (newImage == false){
			AVO_PRINT_DEBUG("QCameraTimeoutThread::run()","emit TimeoutError() (timeout=%d)",timeout);
			emit TimeoutError();
			in_progress = false;
			return;
		}
		newImage = false;
	}	//End of life

	in_progress = false;
	return;
}
