#ifndef CLIENTSOCKET_H_
#define CLIENTSOCKET_H_

#include <QTcpSocket>
#include <QByteArray>
#include <QList>
#include <QString>

#include "vv_defines.h"

class QCameraServer;



class ClientSocket : public QTcpSocket
{
    Q_OBJECT

public:
    ClientSocket(QObject *parent = 0, QCameraServer *camera_ptr = 0, QList<QString> *userList_pt = 0);
    ~ClientSocket();

signals:
	void enableCamera(bool enable);
	void newUserLoggedSignal(const QString & name);
//	void updateParams();

	
	
private slots:
    void readClient();
    void imageToClient(const QByteArray &qmeta_jpeg);
	void sendImageToClient();
	void sendUserLoggedToClient(bool logged);
	void sendEnableCameraToClient(bool enabled);
	void sendEnableSurveillanceToClient(bool enabled);
	void sendNewAlarmsToClient(const QByteArray &alarm_array);
	void SendPermissionDenied();
	void sendDeleteAlarmToClient(int ID);
	void error();//QAbstractSocket::SocketError error);

private:
    quint32 nextBlockSize;
    QCameraServer *qcamera;
    QByteArray qmetaJPEG;
    bool 	imageCameraAsked;
    bool	user_logged;
    QList<QString> *userList;
    QString user_name;
    QString user_password;
    quint8 user_level;

};


#endif /*CLIENTSOCKET_H_*/
