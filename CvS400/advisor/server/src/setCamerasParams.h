/*
 * setCameraParams.h
 *
 *  Created on: 08/09/2009
 *      Author: Gabri
 */

#ifndef SETCAMERASPARAMS_H_
#define SETCAMERASPARAMS_H_

#include <QDialog>
#include "ui_setCamerasParams.h"
#include <x7svidsurv.h>
#include <QPainter>
#include <QImage>
#include <QPixmap>
#include <QString>
#include <QMap>

class EditQLabel;
class QCameraServer;
class Camera;


class setCamerasParams : public QDialog
{
    Q_OBJECT

public:
    setCamerasParams(QDialog *parent = 0, QCameraServer *qcamera_pt = 0);
    ~setCamerasParams();

public slots:
    void setImageToLabel(const QImage& image);
    void ApplyNewValues();
    void ApplyNewValuesAndClose();
    void ShowPushButtons();
    void VideoPathIsGeneral(bool general);
    void WriteJPEGIsGeneral(bool general);
    void setTrackingDesc(int index);

signals:
    void ValuesUpdated();


private:
    Ui::setCamerasParamsClass ui;

	int bg_ccMinArea;
	int bg_insDelayLog;
	int bg_refBGDelay;
	int bg_refFGDelay;
	int bg_threshold;


	QMap<int, QPair<QString,QString> > trackAlgos;
	QCameraServer 		*qcamera;
	Camera 			*camera;
	X7sVSAlrManager *alrMgr;
	QPainter 		*painter;
	QImage 			image;
	QPixmap 		pixmap;
	EditQLabel 		*edit_label;
	QString 		videoPath;

private:
	void AddTrackingType();

private slots:
	void ApplyNewBG_Values();
	void EnableNamePushButton();
	void ApplyCameraName();
	void ApplyTracking();
	void DeleteAlarm();
	void ChangeAlarmColor();
	void ChangeImageLabel();
	void EnableAlarmPushButtons();
	void EditAlarm();
	void ApplyEditAlarm();
	void BrowseVideoPath();

};

#endif /* SETCAMERASPARAMS_H_ */
