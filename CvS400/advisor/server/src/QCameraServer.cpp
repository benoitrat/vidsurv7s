#include "QCameraServer.h"
#include "QCameraTimeoutThread.h"
#include <QPainter>
#include <QColor>
#include <QTextStream>
#include <x7svidsurv.h>
#include "QtBind.hpp"

#include "Camera.hpp"
#include "Board.hpp"
#include "setCamerasParams.h"
#include "QServer.h"
#include "Log.hpp"
#include <QMessageBox>

#define     MIN_LINES_PER_FRAME 287
#define     MIN_PIXELS_PER_LINE 360

/*
QColor line_color_pen[] = { Qt::red, Qt::green, Qt::blue, Qt::cyan, Qt::magenta, Qt::yellow,
                Qt::gray, Qt::lightGray, Qt::white, Qt::black };
QColor polygon_color_pen[] = { Qt::darkYellow, Qt::darkMagenta,
                Qt::darkCyan, Qt::darkBlue, Qt::darkGreen, Qt::darkRed, Qt::darkGray, Qt::white, Qt::black};*/

QCameraServer::QCameraServer(QObject *parent, Camera *cam, int index)
    : QCamera((QWidget*)parent,cam,index)
{


    resize(MIN_PIXELS_PER_LINE, MIN_LINES_PER_FRAME);
    qcamera_size = QSize(MIN_PIXELS_PER_LINE, MIN_LINES_PER_FRAME);
    setMinimumSize(qcamera_size);

    board_index = camera->GetBoard()->GetID();
    avoDebug("QCameraServer::QCamera()") << "Cam_" << camera->GetSpecialID()<<"is in creation...";
    addpolygon = false;
    addline     = false;
    drawpolygon = false;
    init_line = false;
    polygon_index = 0;
    line_index = 0;
    polygon_size = 0;
	frame_add = 0;
	frame_rate = "";
    for(unsigned int i = 0; i < MAX_POLYGONS; i++){
        polygon[i] = QPolygon(0);
    }
    for(unsigned int i = 0; i < MAX_LINES; i++){
        line[i] = QLine();
    }
    noAvailableImage = new QImage(":/images/no_camera.png");

    addingAlarm = false;

    admin_mode = false;

//    timeout = new QCameraTimeoutThread(this, 10);
//    connect (timeout, SIGNAL(TimeoutError()), this, SLOT(TimeoutError()));

        createActions();
        createMenus();

        name = camera->GetName();
        enabled = camera->IsEnable();
        drawable = enabled;
        first_image_received = false;
        surveillanceEnabled = camera->GetVidSurvPipeline()->GetParam("enable")->toIntValue() == 1;
        if (surveillanceEnabled){
    		pAlarms = camera->GetVidSurvPipeline()->GetAlarmManager();
        	last_line_index = pAlarms->Size(X7S_VS_ALR_TYPE_LINE);
        	last_polygon_index = pAlarms->Size(X7S_VS_ALR_TYPE_POLY);
        }
        else{
    		pAlarms = 0;
        	last_line_index = 0;
        	last_polygon_index = 0;
        }

 //       enableCameraAction->setChecked(enabled);
        enableSurveillanceAction->setChecked(surveillanceEnabled);
 //       setVisibleAction->setChecked(drawable);


        enableQCamera(enabled);
        enableSurveillance(surveillanceEnabled);

         avoDebug("QCameraServer::QCamera()") <<  "Cam_" << camera->GetSpecialID()<<"is created!";

         qserver = (QServer*)0;

}

QCameraServer::~QCameraServer()
{
//        avoDebug("QCameraServer::~QCamera()") <<  "Cam_" << camera->GetSpecialID()<<"is in destruction...";
        delete noAvailableImage;
        X7S_DELETE_PTR(qserver);
//        avoDebug("QCameraServer::~QCamera()") <<  "Cam_" << camera->GetSpecialID()<<"is destructed!";

}

void QCameraServer::StartServerSocket(QList<QString> *userList_pt)
{
    qserver = new QServer(this, this, userList_pt);
        if (!qserver->listen(QHostAddress::Any, 6180 + 4*board_index + camera_index)) {
                std::cerr << "Failed to bind to port" << std::endl;
                return;
        }
}

void QCameraServer::SetAdminMode(bool enable)
{
        if (surveillanceEnabled)
        	addAlarmAction->setEnabled(enable);
        enableSurveillanceAction->setEnabled(enable);
 //       setVisibleAction->setEnabled(enable);
 //       enableCameraAction->setEnabled(enable);
        setCameraParamsAction->setEnabled(enable);
        admin_mode = enable;
}

void QCameraServer::createActions()
{
    addPolygonAction = new QAction(tr("Add polygon alarms"), this);
    connect(addPolygonAction, SIGNAL(triggered()), this, SLOT(addPolygon()));

    addLineAction = new QAction(tr("Add line alarms"), this);
    connect(addLineAction, SIGNAL(triggered()), this, SLOT(addLine()));

    applyAction = new QAction(tr("Apply alarms"), this);
    applyAction->setEnabled(false);
    connect(applyAction, SIGNAL(triggered()), this, SLOT(apply()));

    cancelAction = new QAction(tr("Cancel"), this);
    connect(cancelAction, SIGNAL(triggered()), this, SLOT(cancel()));
    cancelAction->setEnabled(false);

    delLastPolygonAction = new QAction(tr("Del last polygon alarm"), this);
    delLastPolygonAction->setEnabled(false);
    connect(delLastPolygonAction, SIGNAL(triggered()), this, SLOT(delLastPolygon()));

    delPolygonsAction = new QAction(tr("Del all polygon alarms"), this);
    delPolygonsAction->setEnabled(false);
    connect(delPolygonsAction, SIGNAL(triggered()), this, SLOT(delAllPolygons()));

    delLastLineAction = new QAction(tr("Del last line alarm"), this);
    delLastLineAction->setEnabled(false);
    connect(delLastLineAction, SIGNAL(triggered()), this, SLOT(delLastLine()));

    delLinesAction = new QAction(tr("Del all line alarms"), this);
    delLinesAction->setEnabled(false);
    connect(delLinesAction, SIGNAL(triggered()), this, SLOT(delAllLines()));

   /* enableCameraAction = new QAction(tr("Enable camera"), this);
    enableCameraAction->setEnabled(true);
    enableCameraAction->setCheckable(true);
    connect(enableCameraAction, SIGNAL(triggered(bool)), this, SLOT(enableQCamera(bool)));

    setVisibleAction = new QAction(tr("Set Visible"), this);
    setVisibleAction->setEnabled(true);
    setVisibleAction->setCheckable(true);
    connect(setVisibleAction, SIGNAL(triggered(bool)), this, SLOT(setDrawable(bool)));*/

    enableSurveillanceAction = new QAction(tr("Enable surveillance on camera"), this);
    enableSurveillanceAction->setEnabled(false);
    enableSurveillanceAction->setCheckable(true);
    connect(enableSurveillanceAction, SIGNAL(triggered(bool)), this, SLOT(enableSurveillance(bool)));

    setCameraParamsAction = new QAction(tr("Set camera parameters..."), this);
    setCameraParamsAction->setEnabled(true);
    setCameraParamsAction->setCheckable(false);
    connect(setCameraParamsAction, SIGNAL(triggered()), this, SLOT(setCameraParam()));
//    connect(setCameraParam, SIGNAL(ValuesUpdated()), this, SLOT(emitBackGroundParamsChanged()));
}

void QCameraServer::createMenus()
{
        menu = new QMenu(this);
        menu->addAction(addPolygonAction);
        menu->addAction(addLineAction);
        menu->addAction(applyAction);
        menu->addAction(cancelAction);
        menu->addAction(delLastPolygonAction);
        menu->addAction(delPolygonsAction);
        menu->addAction(delLastLineAction);
        menu->addAction(delLinesAction);
//      menu->addAction(exitAction);

        menu_enable = new QMenu(this);
//      menu_enable->addAction(enableCameraAction);
//      menu_enable->addAction(setVisibleAction);
//      menu_enable->addAction(disableCameraAction);
        menu_enable->addAction(enableSurveillanceAction);
        menu_enable->addAction(setCameraParamsAction);
        //menu_enable->addAction(addAlarmAction);

        addAlarmAction = menu_enable->addMenu(menu);
        menu_enable->addAction(addAlarmAction);
        addAlarmAction->setEnabled(false);
        addAlarmAction->setText(tr("Add Alarms..."));
}


void QCameraServer::addPolygon()
{
        addpolygon = true;
        addline = false;
        setMouseTracking(true);
        addPolygonAction->setEnabled(false);
        addLineAction->setEnabled(false);
        cancelAction->setEnabled(true);
        polygon_size = 0;
        delPolygonsAction->setEnabled(false);
        delLastPolygonAction->setEnabled(false);
        setCursor(Qt::CrossCursor);
        update();
}



void QCameraServer::addLine()
{
        addline = true;
        addpolygon = false;
        setMouseTracking(true);
        addLineAction->setEnabled(false);
        addPolygonAction->setEnabled(false);
        cancelAction->setEnabled(true);
        polygon_size = 0;
        delLinesAction->setEnabled(false);
        delLastLineAction->setEnabled(false);
        setCursor(Qt::CrossCursor);
        update();
}

void QCameraServer::addNewAlarmFromClient(QByteArray &alarm_array)
{
    quint32 alarm_index ;
    quint32 alarm_type;
    QByteArray array = QByteArray(alarm_array);
    QDataStream alarm_stream(&array, QIODevice::ReadOnly);
    alarm_stream.setVersion(QDataStream::Qt_4_3);
    alarm_stream >> alarm_type;
    if (alarm_type == X7S_VS_ALR_TYPE_LINE){
        alarm_stream >> alarm_index ;
        if(alarm_index > MAX_LINES)
        	return;
		for(quint8 i = 0; i < alarm_index; i++){
			alarm_stream >> line[i];
			QColor color = QColor();
			alarm_stream >> color;
			QtBind::toX7sVSAlrDetector(pAlarms,line[i],color);
		}
    }
    if (alarm_type == X7S_VS_ALR_TYPE_POLY){
        alarm_stream >> alarm_index ;
        if(alarm_index > MAX_POLYGONS)
        	return;
		for(quint8 j = 0; j < alarm_index; j++){
			QPolygon poligono;
			QColor color = QColor();
			alarm_stream >> poligono;
			alarm_stream >> color;
			QtBind::toX7sVSAlrDetector(pAlarms,poligono,color);
		}
    }
}

void QCameraServer::apply()
{
    	//Adding polygon alarms
    	QByteArray alarmp_array;
        QDataStream alarmp_stream(&alarmp_array, QIODevice::WriteOnly);
        alarmp_stream.setVersion(QDataStream::Qt_4_3);

        if((polygon_index)&&(polygon_index<=MAX_POLYGONS)){
    		alarmp_stream << (quint32)X7S_VS_ALR_TYPE_POLY << (quint32)polygon_index ;
    		for(int j = 0; j < (int)polygon_index; j++){
    			alarmp_stream << polygon[j];
    			QColor color = getAlarmPolyPenQColor(j + last_polygon_index);
    			alarmp_stream << color;
    			QtBind::toX7sVSAlrDetector(pAlarms,polygon[j],color);
    			polygon[j] = QPolygon(0);
    		}
    		emit newAlarmToClient(alarmp_array);
        }
    	//Adding line alarms
    	QByteArray alarml_array;
        QDataStream alarml_stream(&alarml_array, QIODevice::WriteOnly);
        alarml_stream.setVersion(QDataStream::Qt_4_3);
        if ((line_index)&&(line_index<=MAX_LINES)){
    		alarml_stream << (quint32)X7S_VS_ALR_TYPE_LINE << (quint32)line_index ;
    		for(unsigned int i = 0; i < line_index; i++){

    			QColor color = getAlarmLinePenQColor(i + last_line_index);
    			QtBind::toX7sVSAlrDetector(pAlarms,line[i],color);

    			alarml_stream << line[i];
    			alarml_stream << color;
    			line[i] = QLine();

    		}
    		emit newAlarmToClient(alarml_array);
        }


        applyAction->setEnabled(false);
        delPolygonsAction->setEnabled(false);
        delLinesAction->setEnabled(false);
        delLastPolygonAction->setEnabled(false);
        delLastLineAction->setEnabled(false);
        last_line_index = ++line_index;
        last_polygon_index = ++polygon_index;
        line_index = 0;
        polygon_index = 0;
        addline = false;
        addpolygon = false;
        addPolygonAction->setEnabled(true);
        addLineAction->setEnabled(true);
        setCursor(Qt::ArrowCursor);
        update();

}

void QCameraServer::cancel()
{
        if (addpolygon){
                addpolygon = false;
                delPolygonsAction->setEnabled(true);
                addPolygonAction->setEnabled(true);
                if(polygon_index < MAX_POLYGONS)
                	polygon[polygon_index]= QPolygon(0);
        }
        if (addline){
                addline = false;
                init_line = false;
                delLinesAction->setEnabled(true);
                addLineAction->setEnabled(true);
                if(line_index < MAX_LINES)
                	line[line_index]= QLine();
        }
        addPolygonAction->setEnabled(true);
        addLineAction->setEnabled(true);
        applyAction->setEnabled(false);
        cancelAction->setEnabled(false);
        setCursor(Qt::ArrowCursor);
        update();
}


void QCameraServer::delLastPolygon()
{
        //addpolygon = false;
        if(polygon_index == 0)
        	return;
		polygon_index--;
		if(polygon_index >= MAX_POLYGONS)
		    return;
        polygon[polygon_index] = QPolygon(0);
        if (polygon_index){
                delLastPolygonAction->setEnabled(true);
                delPolygonsAction->setEnabled(true);
        }
        else{
                delLastPolygonAction->setEnabled(false);
                delPolygonsAction->setEnabled(false);
        }
        addPolygonAction->setEnabled(true);
        update();
}

void QCameraServer::delLastLine()
{
        //addline = false;
        if(line_index == 0)
			return;
		line_index--;
		if(line_index >= MAX_LINES)
			return;
        line[line_index] = QLine();
        if (line_index){
                delLastLineAction->setEnabled(true);
                delLinesAction->setEnabled(true);
        }
        else{
                delLastLineAction->setEnabled(false);
                delLinesAction->setEnabled(false);
        }
        addLineAction->setEnabled(true);
        update();
}

void QCameraServer::delAllPolygons()
{
        //addpolygon = false;
        polygon_index = 0;
        for(unsigned int i = 0; i < MAX_POLYGONS; i++)
                polygon[i] = QPolygon(0);
        addPolygonAction->setEnabled(true);
        delPolygonsAction->setEnabled(false);
        delLastPolygonAction->setEnabled(false);
        update();
}

void QCameraServer::delAllLines()
{
        //addline = false;
        line_index = 0;
        for(unsigned int i = 0; i < MAX_LINES; i++)
                line[i] = QLine();
        addLineAction->setEnabled(true);
        delLinesAction->setEnabled(false);
        delLastLineAction->setEnabled(false);
        update();
}

void QCameraServer::enableQCamera(bool enable)
{
        if(enabled != enable)
        {
                //enableCameraAction->setChecked(enable);
                enableSurveillanceAction->setEnabled(enable);
                enabled = enable;
                setDrawable(enable);
                emit cameraEnabled(enable, camera_index);
        }
        update();
}

void QCameraServer::setDrawable(bool enable)
{
        if(enabled == false){
        	drawable = false;
        	return;
        }
		if(drawable != enable)
        {
                QWidget::setVisible(enable);
				//setVisibleAction->setChecked(enable);
                drawable = enable;
                emit cameraVisible(enable, camera_index);
//                if(drawable == false)  timeout->StopTimeout();
//                else   timeout->StartTimeout();
        }
        camera->DrawFrame(enable);
        update();
}



void QCameraServer::enableSurveillance(bool enable)
{
        if ((enabled == false) || (surveillanceEnabled == enable))
                return;
        surveillanceEnabled = enable;
        camera->GetVidSurvPipeline()->X7sXmlObject::SetParam("enable", "%d",enable);
        if (enable)
                pAlarms = camera->GetVidSurvPipeline()->GetAlarmManager();
        last_line_index = pAlarms->Size(X7S_VS_ALR_TYPE_LINE);
        last_polygon_index = pAlarms->Size(X7S_VS_ALR_TYPE_POLY);
        addAlarmAction->setEnabled(enable);
        enableSurveillanceAction->setChecked(enable);
        emit cameraEnabledSurveillance(enable, camera_index);
}

//Used to forward the event from Thread_Play to QCamera.
void QCameraServer::transmitNewFrameReceived(Camera* cam)
{
	if(cam==camera) emit newFrameReceived(cam);
}

void QCameraServer::setCameraParam()
{
        setCamerasParams *setCameraParam = new setCamerasParams(0, this);
        setCameraParam->ShowPushButtons();
        setCameraParam->show();
        setCameraParam->exec();
        delete setCameraParam;
}

void QCameraServer::addAlarm()
{
        addingAlarm = true;
}

void QCameraServer::exitAddAlarm()
{
        addingAlarm = false;
}

void QCameraServer::paintEvent(QPaintEvent *event)
{
        QPainter painter(this);
        painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
        QPainterPath path;
        //painter.fillRect(rect(), QBrush(Qt::black));

    if ((first_image_received == false) || (enabled == false) || (drawable == false)) {
        painter.drawImage(0,0, *noAvailableImage);
        painter.setPen(QPen(getAlarmLinePenQColor(camera_index), 3));
        painter.drawText(10, qcamera_size.height()-10, name);
        return;
    }
        //painter.drawPixmap(0,0, pixmap);
    painter.drawImage(0,0, image_v);
	painter.drawImage(0,0, image_d);

	if((line_index > MAX_LINES)||(polygon_index > MAX_POLYGONS))
	    return;
    for(unsigned int i = 0; i < polygon_index; i++){
        painter.setPen(QPen(getAlarmPolyPenQColor(i), 3));
        painter.drawPolygon(polygon[i]);
                //painter.setClipRegion(polygon[i], Qt::IntersectClip);
    }

    for(unsigned int i = 0; i < line_index; i++){
        painter.setPen(QPen(getAlarmLinePenQColor(i), 3));
        QPoint point[2];
        point[0] = line[i].p1();
        point[1] = line[i].p2();
                painter.drawPolyline(point, 2);
                //Dibujamos los textos asociados
        QPoint point_t = (point[1] + point[0])/2;
        point_t.setX(point_t.x() - 50);
        //point_t.setY(point_t.y() - 20);
        if (point[1].y() >= point[0].y()){
                painter.drawText(point_t, tr("<--Out"));
                point_t.setX(point_t.x() + 70);
                //point_t.setY(point_t.y() + 40);
                painter.drawText(point_t, tr("In-->"));
        }
        else{
                painter.drawText(point_t, tr("<--In"));
                point_t.setX(point_t.x() + 70);
                //point_t.setY(point_t.y() + 40);
                painter.drawText(point_t, tr("Out-->"));
        }
    }
    if(addpolygon){
        if(!polygon[polygon_index].isEmpty()){
                painter.setPen(QPen(getAlarmPolyPenQColor(polygon_index), 3, Qt::DashDotLine, Qt::RoundCap));
                painter.drawPolyline(polygon[polygon_index]);
                painter.drawPolyline(last_line_point, 2);
        }
    }

    if(addline && init_line){
                painter.setPen(QPen(getAlarmLinePenQColor(line_index), 3, Qt::DashDotLine, Qt::RoundCap));
        painter.drawPolyline (last_line_point, 2);
        //drawText ( const QPoint & position, const QString & text )
        QPoint point = (last_line_point[1] + last_line_point[0])/2;
        point.setX(point.x() - 50);
        //point.setY(point.y() - 20);
        if (last_line_point[1].y() >= last_line_point[0].y()){
                painter.drawText(point, tr("<--Out"));
                point.setX(point.x() + 70);
                //point.setY(point.y() + 40);
                painter.drawText(point, tr("In-->"));
        }
        else{
                painter.drawText(point, tr("<--In"));
                point.setX(point.x() + 70);
                //point.setY(point.y() + 40);
                painter.drawText(point, tr("Out-->"));
        }
    }
    painter.setPen(QPen(Qt::white, 3));
    painter.setBackgroundMode(Qt::OpaqueMode);
    QColor colortext = camera->GetColor();
    colortext.setAlpha(100);
    painter.setBackground(QBrush(colortext, Qt::SolidPattern));
#ifdef DEBUG
	painter.drawText(10, qcamera_size.height()-10, " "+ name + frame_rate+" ");
#else
	painter.drawText(10, qcamera_size.height()-10, " "+name+" ");
#endif
}

void QCameraServer::updateImage(const QImage &image, const QImage &image_m)
{
        if(drawable == false)
                return;
       // timeout->NewImage();
        frame_add++;
        if ((qcamera_size != image.size())&&(image.isNull()==false)) {
        	qcamera_size.setHeight(image.size().height());
        	qcamera_size.setWidth(image.size().width());
        	setMinimumSize(qcamera_size);
        	resize(qcamera_size);
        }
        image_v = /*QImage(image);//*/image.copy(0,0,qcamera_size.width(), qcamera_size.height());
        image_d = /*QImage(image_m);//*/image_m.copy(0,0,qcamera_size.width(), qcamera_size.height());

//      if (first_image_received == false)
//              timeout->StartTimeout();
        first_image_received = true;
        update();
}

void QCameraServer::updateStatistic()
{
	frame_rate = QString(" @ " + QString::number(float(1000.0*(float)frame_add/(float)timeout_ms),'f',2) + " fps");
	frame_add = 0;
}

void QCameraServer::updateMJPEG(const QByteArray &metaJPEG)
{
        emit imageToClient(metaJPEG);
}

void QCameraServer::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton){
    	if((line_index > MAX_LINES-1)||(polygon_index > MAX_POLYGONS-1))
    	    return;
    	if((event->pos().x()>qcamera_size.width()) || (event->pos().y()>qcamera_size.height()))
    	    		return;
        if (addpolygon){
                polygon[polygon_index] << event->pos();
                polygon_size++;
                if(polygon_size > 2)//At least 3 point for polygon
                        applyAction->setEnabled(true);
                last_line_point[0] = event->pos();
                update();
        }
        if (addline){
                if (init_line == false){
                line[line_index].setP1(event->pos());
                last_line_point[0] = event->pos();
                }
                init_line = true;
                applyAction->setEnabled(true);
                update();
        }
    }
    if (event->button() == Qt::RightButton){
    	if((event->pos().x()>qcamera_size.width()) || (event->pos().y()>qcamera_size.height()))
    		return;
        menu_enable->exec(event->globalPos());
    }
}

void QCameraServer::mouseDoubleClickEvent ( QMouseEvent * event )
{
        if (event->button() == Qt::LeftButton){
        	if((event->pos().x()>qcamera_size.width()) || (event->pos().y()>qcamera_size.height()))
        	    return;
			if (addpolygon &&(polygon_size > 1)){
					addPolygonAction->setEnabled(false);
					addLineAction->setEnabled(true);
					applyAction->setEnabled(true);
					delPolygonsAction->setEnabled(true);
					delLastPolygonAction->setEnabled(true);
					cancelAction->setEnabled(false);
					if (polygon_index >= MAX_POLYGONS-1){
							addPolygonAction->setEnabled(false);
							addpolygon = false;
							setCursor(Qt::ArrowCursor);
					}
					polygon_index++;
					update();
					return;
			}
			if (addline){
					line[line_index].setP2(event->pos());
					init_line = false;
					addLineAction->setEnabled(false);
					addPolygonAction->setEnabled(true);
					applyAction->setEnabled(true);
					delLinesAction->setEnabled(true);
					delLastLineAction->setEnabled(true);
					cancelAction->setEnabled(false);
					if (line_index >= MAX_LINES-1){
							addLineAction->setEnabled(false);
							addline = false;
							setCursor(Qt::ArrowCursor);
					}
					line_index++;
					update();
					return;
			}
               /* if (admin_mode)
                        enableQCamera(enabled == false);*/
        }
}
void QCameraServer::mouseMoveEvent ( QMouseEvent * event )
{
	if((event->pos().x()>qcamera_size.width()) || (event->pos().y()>qcamera_size.height()))
		return;
	if (addpolygon|| addline){
			last_line_point[1] = event->pos();
			update();
	}
}
/*
void QCameraServer::TimeoutError()
{
    avoDebug("QCameraServer::TimeoutError()") << timeout->GetTimeout();
    emit warningOnCamera(tr("TimeoutError. Camera '") + name +tr("' did not send any images during ")
                        + QString::number(timeout->GetTimeout()) + tr(" s"));
        first_image_received = false;
        update();
}



void QCameraServer::SetWatchDogTime(quint8 time){
        if (time)
             timeout->SetTimeout(time);
}

quint8 QCameraServer::GetWatchDogTime(){
        return timeout->GetTimeout();
}
*/
