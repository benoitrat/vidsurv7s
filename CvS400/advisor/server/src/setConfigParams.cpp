#include "setConfigParams.h"
#include "ui_setConfigParams.h"
#include "LogConfig.hpp"
#include "QtBind.hpp"
#include "Root.hpp"
#include "Devices.hpp"

#include <QDir>
#include <QFileDialog>

setConfigParams::setConfigParams(QWidget *parent, Root *root_p) :
    QWidget(parent),
    m_ui(new Ui::setConfigParams)
{
    m_ui->setupUi(this);

    connect(m_ui->videoPathIsGeneral_checkBox, SIGNAL(toggled(bool)), this, SLOT(VideoPathIsGeneral(bool)));
    connect(m_ui->videoIsToWrite_checkBox, SIGNAL(toggled(bool)), this, SLOT(WriteJPEGIsGeneral(bool)));

    root = root_p;
    config = root->logConfig();
    devices = root->devices();
    videoPath = QtBind::toQString(devices->GetParam("video_path"));
    m_ui->videoPath_lineEdit->setText(videoPath);
    logPath = QtBind::toQString(config->GetParam("log_path"));
    m_ui->logPath_lineEdit->setText(logPath);
    m_ui->videoPathIsGeneral_checkBox->setChecked(devices->GetParam("video_path_isgeneral")->toIntValue() == 1);
    m_ui->videoIsToWrite_checkBox->setChecked(devices->GetParam("video_istowrite")->toIntValue() == 1);
    m_ui->logLevel_spinBox->setValue(config->GetParam("log")->toIntValue());
    m_ui->log_cvLevel_spinBox->setValue(config->GetParam("log_cv")->toIntValue());
    m_ui->log_xmlLevel_spinBox->setValue(config->GetParam("log_xml")->toIntValue());
    m_ui->log_vsLevel_spinBox->setValue(config->GetParam("log_vs")->toIntValue());
    m_ui->log_netLevel_spinBox->setValue(config->GetParam("log_net")->toIntValue());

    //Delimitamos los valores m�ximos y m�nimos de los SpinBox
    int val, max, min;
    if(config->GetParam("log_net")->GetValue(&val,&min,&max))
    	m_ui->log_netLevel_spinBox->setRange(min, max);
    else
    	m_ui->log_netLevel_spinBox->setRange(0, 5);
    if(config->GetParam("log_vs")->GetValue(&val,&min,&max))
        m_ui->log_vsLevel_spinBox->setRange(min, max);
    else
    	m_ui->log_vsLevel_spinBox->setRange(0, 5);
    if(config->GetParam("log_xml")->GetValue(&val,&min,&max))
        m_ui->log_xmlLevel_spinBox->setRange(min, max);
    else
    	m_ui->log_xmlLevel_spinBox->setRange(0, 5);
    if(config->GetParam("log_cv")->GetValue(&val,&min,&max))
        m_ui->log_cvLevel_spinBox->setRange(min, max);
    else
    	m_ui->log_cvLevel_spinBox->setRange(0, 5);
    if(config->GetParam("log")->GetValue(&val,&min,&max))
        m_ui->logLevel_spinBox->setRange(min, max);
    else
    	m_ui->logLevel_spinBox->setRange(0, 5);

    connect(m_ui->videoPathBrowse_pushButton, SIGNAL(clicked()), this, SLOT(BrowseVideoPath()));
    connect(m_ui->logPathBrowse_pushButton, SIGNAL(clicked()), this, SLOT(BrowseLogPath()));

}

setConfigParams::~setConfigParams()
{
    delete m_ui;
}

void setConfigParams::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void setConfigParams::ApplyNewValues()
{
	devices->SetParam("video_path", "%s", videoPath.toStdString().c_str());
	config->SetParam("log_path", "%s", logPath.toStdString().c_str());

	devices->SetParam("video_path_isgeneral", "%d", m_ui->videoPathIsGeneral_checkBox->isChecked());
	devices->SetParam("video_istowrite", "%d", m_ui->videoIsToWrite_checkBox->isChecked());

	config->SetParam("log","%d", m_ui->logLevel_spinBox->value());
	config->SetParam("log_cv","%d", m_ui->log_cvLevel_spinBox->value());
	config->SetParam("log_xml","%d", m_ui->log_xmlLevel_spinBox->value());
	config->SetParam("log_vs","%d", m_ui->log_vsLevel_spinBox->value());
	config->SetParam("log_net","%d", m_ui->log_netLevel_spinBox->value());
	config->Apply();
}

void setConfigParams::BrowseVideoPath()
{
	QFileDialog dialog(this);
	dialog.setFileMode(QFileDialog::Directory);
	dialog.setOptions(QFileDialog::ShowDirsOnly);
	QStringList fileNames;
	if (dialog.exec())
		fileNames = dialog.selectedFiles();
	else
		return;
	videoPath = fileNames.at(0);
	m_ui->videoPath_lineEdit->setText(videoPath);
}

void setConfigParams::BrowseLogPath()
{
	QFileDialog dialog(this);
	dialog.setFileMode(QFileDialog::Directory);
	dialog.setOptions(QFileDialog::ShowDirsOnly);
	QStringList fileNames;
	if (dialog.exec())
		fileNames = dialog.selectedFiles();
	else
		return;
	logPath = fileNames.at(0);
	m_ui->logPath_lineEdit->setText(logPath);
}
void setConfigParams::VideoPathIsGeneral(bool general)
{
	emit VideoPathIsGeneralSignal(general);
}
void setConfigParams::WriteJPEGIsGeneral(bool general)
{
	emit WriteJPEGIsGeneralSignal(general);
}
