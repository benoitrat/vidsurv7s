#include "visor.h"

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <QList>
#include <QFileDialog>
#include <QMessageBox>
#include <QCryptographicHash>
#include <QtDebug>
#include <QVBoxLayout>

#include "vv_defines.h"
#include "Camera.hpp"
#include "Board.hpp"
#include "Database.hpp"
#include "VideoHandler.hpp"
#include "Root.hpp"
#include "Log.hpp"
#include "LogConfig.hpp"
#include "setVisorParams.h"
#include "setBoardParams.h"
#include "QBoard.h"
#include "QCameraServer.h"
#include "Devices.hpp"
#include "QServer_Devices.h"
#include "EventListWidget.h"
#include "ViewerWidget.h"
#include "VideoInFile.hpp"
#include "AlrTimeWidget.h"
#include "addNewUser.h"
#include "AuthenticationDialog.h"
#include "WaitingDialog.h"
#include "WarningWidget.h"
#include "usersConfigParams.h"
#include "CamEvtHandler.h"
#include "CamEvt.hpp"
#include "XmlObjTreeConfWidget.h"
#include "TimeTicker.hpp"
#include "AboutUsWidget.h"

#define AVO_SERVER_MAINWIN_STATE_VER 1

using namespace std;

QColor text_color_pen[] = { Qt::red, Qt::green, Qt::blue, Qt::cyan, Qt::magenta, Qt::yellow,
		Qt::gray, Qt::lightGray, Qt::white, Qt::black, };


class WaitDBDialog : public WaitingDialog
{
public:
	WaitDBDialog(QWidget *parent, Database *db): WaitingDialog(parent,"Connecting to Database"), m_db(db) {};
protected:
	bool trying() { return (m_db)?m_db->Connect():false; };
private:
	Database *m_db;
};





Visor::Visor(QWidget *parent)
: QMainWindow(parent),
  root(NULL), camEvtHandler(NULL), eventList(NULL), videoViewer(NULL), warningList(NULL)
  {
	X7S_FUNCNAME("Visor::Visor()");

	Log::SetLevel(X7S_LOGLEVEL_HIGH);
	AVO_PRINT_DEBUG(funcName,NULL);
	ui.setupUi(this);


	//========== Set not defined variable to NULL
	locationLabel=NULL;
//	event_Widget_Tab=NULL;
//	cameras_tab=NULL;
//	events_tab=NULL;
	splitter=NULL;
	splitter_event=NULL;
	qcameras_gridLayout=NULL;
	events_gridLayout=NULL;
	serverUsers=NULL;
	clientUsers=NULL;
	new_user=NULL;
	config_user=NULL;
	visorParams=NULL;
	settings=NULL;
	qserverd=NULL;




	//========== Connect the MENU

	ui.actionSave_xml_file->setShortcut(Qt::CTRL + Qt::Key_S);
	ui.actionReconnect_to_all_boards->setShortcut(Qt::CTRL+ Qt::Key_R);
	ui.actionHide_Graphical_Interface->setShortcut(Qt::CTRL + Qt::Key_H);
	ui.actionFull_Screen->setShortcut(Qt::Key_F5);


	connect(ui.actionSave_xml_file, SIGNAL(triggered()), this, SLOT(saveXMLFile()));
	connect(ui.actionAdd_New_Board, SIGNAL(triggered()), this, SLOT(AddNewBoard()));
	connect(ui.actionConfig_parameters, SIGNAL(triggered()), this, SLOT(configureParams()));
//	connect(ui.actionShow_Alarm_Events, SIGNAL(toggled(bool)), this, SLOT(ShowAlarmsWindows(bool)));
	connect(ui.actionHide_Graphical_Interface, SIGNAL(toggled(bool)), this, SLOT(HideGraphicalMode(bool)));
	connect(ui.actionAdd_new_server_user, SIGNAL(triggered()), this, SLOT(AddNewServerUser()));
	connect(ui.actionAdd_new_client_user, SIGNAL(triggered()), this, SLOT(AddNewClientUser()));
	connect(ui.actionConfigServer_users, SIGNAL(triggered()), this, SLOT(ConfigServerUsers()));
	connect(ui.actionConfigClient_users, SIGNAL(triggered()), this, SLOT(ConfigClientUsers()));
	connect(ui.actionReconnect_to_all_boards, SIGNAL(triggered()), this, SLOT(ReconnectAllBoards()));
	connect(ui.actionAdvance_Config, SIGNAL(triggered()), this, SLOT(AdvanceConfig()));
	connect(ui.actionFull_Screen,SIGNAL(toggled(bool)), this, SLOT(fullScreen(bool)));
	connect(ui.actionImport,SIGNAL(triggered()),this,SLOT(importXml()));
	connect(ui.actionExport,SIGNAL(triggered()),this,SLOT(exportXml()));
	connect(ui.actionEnter_Admin_mode, SIGNAL(toggled(bool)), this, SLOT(EnableAdminMode(bool)));
	connect(ui.actionDefault_view, SIGNAL(triggered()), this, SLOT(SetDefaultView()));

	images_timeout = new QTimer(this);

#ifdef DEBUG
	ui.menuPreference->addSeparator();
	connect(ui.menuPreference->addAction("Purge Database"),SIGNAL(triggered()),this,SLOT(PurgeDatabase()));
	connect(ui.menuPreference->addAction("Update LogFile"),SIGNAL(triggered()),this,SLOT(updateLogFile()));
	connect(ui.menuPreference->addAction("Update Stats"),SIGNAL(triggered()),this,SLOT(updateStats()));
#endif


	AboutUsWidget *aboutUs = new AboutUsWidget();
	aboutUs->hide();
	connect(ui.action_AboutUs,SIGNAL(triggered()),aboutUs,SLOT(show()));


	locationLabel = new QLabel(" Visor Server by Seven Solutions S.L. ");
	statusBar()->addWidget(locationLabel);

	this->init();

  }


void Visor::init()
{
	X7S_FUNCNAME("Visor::init()");


	settings = new QSettings(AVO_SERV_CONFIG_INI, QSettings::IniFormat);
	//numberOfColumms = settings->value("qcameras/numberOfColumms", 2).toInt();
	xmlFile = AVO_SERV_CONFIG_XML;


	//========== Load the ROOT XML


	root = new Root(xmlFile);
	root->LoadFile();
	Devices *devices = root->devices();
	numberOfBoards = root->devices()->GetSize();

	while (numberOfBoards == 0){
		AVO_PRINT_DEBUG(funcName,"Error. No board detected");
		int result = QMessageBox::warning(this,tr("Configuration Error"),
				tr("The configuration file is empty or incorrect"),
				tr("&Import a configuration file"), tr("Create &New Board"), tr("&Close application!"),0,1);
		AVO_PRINT_DEBUG(funcName,"result %d",result);
		if (result == 0)
		{
			//Elegir otro fichero XML
			QTimer::singleShot(10, this, SLOT(importXml())); //Call import XML function
			return;	//Go out this function
		}
		if (result == 1){//Creamos una board nueva
			devices = root->devices();
			Board *board_ptr = new Board(devices);
			board_ptr->SetParam("ip", "192.168.7.254");
			board_ptr->SetParam("port", "%d", 18000);
			board_ptr->SetParam("rtp", "%d", 50000);
			QBoard *qboard = new QBoard(0, board_ptr, 0);
			setBoardParams *board_params = new setBoardParams(0, qboard);
			board_params->show();
			board_params->ShowPushButtons(true);
			numberOfBoards = board_params->exec();
			if (numberOfBoards)
				saveXMLFile();
			//delete qboard;
			AVO_PRINT_DEBUG(funcName,"XML file saved");
			//delete board_ptr;
			//AVO_PRINT_DEBUG(funcName,"Board_ptr deleted");
			delete board_params;
			delete qboard;
			AVO_PRINT_DEBUG(funcName,"Board param deleted");
			delete root;
			root = new Root(xmlFile);
			root->LoadFile();
			numberOfBoards = root->devices()->GetSize();
			devices = root->devices();
		}
		if (result == 2)//Salimos
			exit(0);
	}

	if(root->database()==NULL) qFatal("root->database()==NULL");

	WaitDBDialog *waitDial = new WaitDBDialog(this,root->database());
	if(waitDial->exec()== QDialog::Rejected)
	{
		AVO_PRINT_DEBUG(funcName,"Error. Data Base Error");
		int result = QMessageBox::critical(this,tr("Database Error"),
				tr("%1 Application will not save videos and events.").arg(root->database()->lastErrorStr()),
				QMessageBox::Ignore | QMessageBox::Abort);
		if (result == QMessageBox::Abort){
			AVO_PRINT_DEBUG(funcName,"Data Base Error. Application closed by user");
			this->hide();
			exit(0);
		}
	}



	//========== client and servers users.



	serverUsers = new QList<QString>();
	clientUsers = new QList<QString>();

	numberOfServerUsers = settings->value("serverUsers/numberOfServerUsers", 0).toInt();
	numberOfClientUsers = settings->value("clientUsers/numberOfClientUsers", 0).toInt();
	int j = 0;
	for(int i = 0; i < numberOfServerUsers; i= i+3)
	{
		serverUsers->append(settings->value("serverUsers/name_" +  QString::number(j), "").toString());
		serverUsers->append(settings->value("serverUsers/password_" +  QString::number(j), "").toString());
		serverUsers->append(settings->value("serverUsers/level_" +  QString::number(j), "").toString());
		j++;
	}
	j = 0;
	for(int i = 0; i < numberOfClientUsers; i= i+3)
	{
		clientUsers->append(settings->value("clientUsers/name_" +  QString::number(j), "").toString());
		clientUsers->append(settings->value("clientUsers/password_" +  QString::number(j), "").toString());
		clientUsers->append(settings->value("clientUsers/level_" +  QString::number(j), "").toString());
		j++;
	}

	if (serverUsers->size() == 0)
		adminMode = true;
	else
		adminMode = false;

	j = EnableAdminMode(adminMode);
	if (j == -1)
		exit(0);
#ifdef DEBUG
	timeout_ms = settings->value(QString("QCameras/WatchDogTime"), 10000).toInt();
	images_timeout->start(timeout_ms);
#endif
	/*
	if(j == 0){//Warning si entra como adm�n.
		int result = QMessageBox::question( this, tr("Access Warning"),tr("You have admin privileges, do you want to activate Admin Mode?"),
				QMessageBox::Yes,
				QMessageBox:: No|QMessageBox::Default, QMessageBox::NoButton);
		if (result == QMessageBox::No){//Habilitamos men�s de admin
			EnableAdminMode(false);
			adminMode = false;
		}
	}
	 */


	splitter = new QSplitter(Qt::Horizontal, this);
	setCentralWidget(splitter);

/*	event_Widget_Tab = new QTabWidget(splitter);
	event_Widget_Tab->setObjectName(QString::fromUtf8("event_Widget_Tab"));
	event_Widget_Tab->setGeometry(QRect(0, 0, 1500, 1200));
	QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Maximum);
	sizePolicy1.setHorizontalStretch(1);
	sizePolicy1.setVerticalStretch(1);
	sizePolicy1.setHeightForWidth(event_Widget_Tab->sizePolicy().hasHeightForWidth());
	event_Widget_Tab->setSizePolicy(sizePolicy1);
	cameras_tab = new QWidget(event_Widget_Tab);
	cameras_tab->setObjectName(QString::fromUtf8("cameras_tab"));
	event_Widget_Tab->addTab(cameras_tab, QString());
	events_tab = new QWidget(event_Widget_Tab);
	events_tab->setObjectName(QString::fromUtf8("events_tab"));
	event_Widget_Tab->addTab(events_tab, QString());

	event_Widget_Tab->setTabText(event_Widget_Tab->indexOf(cameras_tab), tr("Cameras"));
	event_Widget_Tab->setTabText(event_Widget_Tab->indexOf(events_tab), tr("Alarms and Events"));*/



	qcameras_gridLayout = new QGridLayout(ui.cameras);
	qcameras_gridLayout->setObjectName(QString::fromUtf8("qcameras_gridLayout"));

	events_gridLayout = new QGridLayout(ui.events);
	events_gridLayout->setObjectName(QString::fromUtf8("events_gridLayout"));


	splitter_event = new QSplitter(Qt::Vertical, this);


	//Event LIST
	eventList = new EventListWidget(this->parentWidget(),500);
	splitter_event = new QSplitter(Qt::Vertical, this);

	splitter->addWidget(ui.event_Widget);
	splitter->addWidget(splitter_event);

	splitter_event->addWidget(eventList);
	splitter->show();
	connect(splitter, SIGNAL(splitterMoved(int, int)), this, SLOT(ResizeCameras(int, int)));

	videoViewer = new ViewerWidget(this);
	connect(videoViewer, SIGNAL(StatusMessage(const QString&)), locationLabel, SLOT(setText(const QString&)));
	splitter_event->addWidget(videoViewer);

	qserverd = new QServer_Devices(this, devices, clientUsers);
	if (!qserverd->listen(QHostAddress::Any, 6178)) {
		QMessageBox msgBox;
		msgBox.setText("Failed to bind on port! This application may be already running, close it first!");
		msgBox.exec();
		QApplication::exit(0); // Close the application
	}
	connect(qserverd, SIGNAL(newUserLoggedSignal(const QString&)), locationLabel, SLOT(setText(const QString&)));
	connect(qserverd, SIGNAL(SaveXMLFileSignal()), this, SLOT(saveXMLFile()));
	connect(qserverd, SIGNAL(SaveXMLTmpFileSignal()), this, SLOT(saveXMLFile_tmp()));
	warningList = new WarningWidget(0);

	root->videoHandler()->AttachDatabase(root->database());

	//--------------------- Create the CamEvtHandler
	camEvtHandler = new CamEvtHandler(root->database(),root->videoHandler());


	//Connect the various elements.
	connect(eventList, SIGNAL(EventSelectedSignal(const CamEvtCarrier&, bool)), this, SLOT(EventSelected(const CamEvtCarrier&, bool)));
	connect(camEvtHandler, SIGNAL(eventVSAlarmReceived(const CamEvtCarrier&)), eventList, SLOT(AddEvent(const CamEvtCarrier&)));

	//Add the TimeTicker to update the LogConfig File.
	TimeTicker *tticker = new TimeTicker(this,true,true,true);
	connect(tticker,SIGNAL(completedDay(const QDateTime&)),this,SLOT(updatePurgeVideo(const QDateTime&)));
	connect(tticker,SIGNAL(completedHour(const QDateTime&)),this,SLOT(updateLogFile(const QDateTime&)));
	connect(tticker,SIGNAL(completedQuater(const QDateTime&)),this,SLOT(updateStats(const QDateTime&)));

	ConnectAllBoards();
	readSettings();
	RedrawCameras();
}







Visor::~Visor()
{
	AVO_PRINT_DEBUG("Visor::~Visor()");
	delete locationLabel;
	delete images_timeout;
	this->release();
	AVO_PRINT_DEBUG("Visor::~Visor() END");
}



void Visor::release()
{
	//writeSettings();
	if(warningList) warningList->close();


	DisconnectAllBoards();

	X7S_DELETE_PTR(videoViewer);
	X7S_DELETE_PTR(warningList);
	X7S_DELETE_PTR(eventList);


	X7S_DELETE_PTR(qserverd);
	X7S_DELETE_PTR(settings);

	//delete eventTextEdit;
	X7S_DELETE_PTR(splitter_event);
	X7S_DELETE_PTR(splitter);



	X7S_DELETE_PTR(root);
	X7S_DELETE_PTR(camEvtHandler);
}



void Visor::fullScreen(bool full)
{
	if(full)
	{
		//Call the full screen function for QMainWindow
		this->showFullScreen();
		ui.statusbar->hide();
	}
	else
	{
		this->showNormal();
		ui.statusbar->show();
	}
}


void Visor::ConnectAllBoards()
{
	avoDebug("Visor::Visor()") << "Connecting to the board" << "...";
	Devices *devices = root->devices();
	numberOfBoards = root->devices()->GetSize();
	/**********************************************/
	Camera *cam=NULL;
	while((cam=devices->GetNextCamera(cam))!=NULL)
	{
		//Create alrTime Widget
		alrTime.push_back(new AlrTimeWidget(ui.events, root->database(), cam));
		connect(alrTime.last(), SIGNAL(EventSelectedSignal(const CamEvtCarrier&, bool)), this, SLOT(EventSelected(const CamEvtCarrier&, bool)));
		connect(this,SIGNAL(playingEvt(const CamEvtCarrier&,bool)),alrTime.last(),SLOT(eventClicked(const CamEvtCarrier&,bool)));
	}



	/**********************************************/
	for (unsigned int i = 0; i < numberOfBoards; i++){


		Board *board_ptr = devices->GetBoard(i);
		if(board_ptr)
		{
			qboards.push_back(new QBoard(ui.cameras, board_ptr, quint8(i)));
			qboards.value(i)->SetAdminMode(adminMode);
			connect(qboards.value(i), SIGNAL(warningOnBoardSignal(const QString &)), warningList, SLOT(AddWarning(const QString&)));
			//			Camera *camera;
			for(int j = 0; j < AVO_BOARD_NUMCAM; j++){
				qboards.value(i)->getQCamera(j)->StartServerSocket(clientUsers);
				//camera = board_ptr->GetCamera(j);
			}
		}
		else
			continue;
		//qboards.value(i)->hide();


		connect(qboards.value(i), SIGNAL(cameraEnabledSignal()), this, SLOT(RedrawCameras()));
		qboards.value(i)->Board_Play();
	}

	//---------------------- Connect the camEvtHandler with QCamera
	for(int i=0;i<qboards.size();i++)
	{
		QBoard *qboard = qboards[i];
		QCameraServer *qcam;

		for(int j=0;j<AVO_BOARD_NUMCAM; j++)
		{
			qcam=qboard->getQCamera(j);
			if(qcam){
				connect(qcam,SIGNAL(newFrameReceived(Camera *)),camEvtHandler,SLOT(processNewFrame(Camera *)));
				qcam->SetStatisticTime(quint32(timeout_ms));
				connect(images_timeout, SIGNAL(timeout()),qcam, SLOT(updateStatistic()));
			}
		}
	}
}

void Visor::DisconnectAllBoards()
{
	X7S_DELETE_PTR(qcameras_gridLayout);
	X7S_DELETE_PTR(events_gridLayout);
	while(qboards.size()){
		//qboards.last()->Board_Close();
		delete qboards.last();
		qboards.pop_back();
	}
	while(alrTime.size()){
		delete alrTime.last();
		alrTime.pop_back();
	}
}

void Visor::ReconnectAllBoards()
{
	DisconnectAllBoards();
	ConnectAllBoards();
	//	readSettings();
	RedrawCameras();
}


void Visor::saveXMLFile()
{
	X7S_FUNCNAME("Visor::saveXMLFile()");
	AVO_PRINT_DEBUG(funcName);

	//Create a file
	if(root->SaveFile()) root->Print();
	if (qboards.size()){
		avoDebug(funcName)<< "ResizeCameras()";
		//		ResizeCameras(0, 1);//
		avoDebug(funcName)<< "RedrawCameras()";
		//		RedrawCameras();
	}
	avoDebug(funcName)<< "End.";
}

void Visor::saveXMLFile_tmp()//grabaci�n del fichero de configuraci�n XML actual
{
	AVO_PRINT_DEBUG("Visor::saveXMLFile_tmp()");

	//Create a file
	if(root->SaveFile("advisor_tmp.xml")) root->Print();
	qDebug()<< "saveXMLFile(): " << "End.";
}

void Visor::configureParams()
{
	AVO_PRINT_DEBUG("Visor::configureParams()");

	setVisorParams *visorParams = new setVisorParams(0, root, &qboards);
	connect(visorParams, SIGNAL(SaveXML_FileSignal()), this, SLOT(saveXMLFile()));
	connect(visorParams, SIGNAL(DeleteBoardSignal(int)), this, SLOT(DeleteBoard(int)));
	visorParams->show();
	visorParams->exec();

	delete visorParams;
}

void Visor::importXml()
{
	X7S_FUNCNAME("Visor::importXml()");

	QString fileName = QFileDialog::getOpenFileName(this, tr("Import XML File"),	"",tr("XML (*.xml)"));
	avoDebug(funcName) << fileName;

	//Return if we didn't want to open a file
	if(fileName.isEmpty()) return;

	Root *tmpRoot=  new Root();
	if(tmpRoot->LoadFile(fileName))
	{

		//Aks if we are going to overwrite current configuration.
		if(QFile::exists(AVO_SERV_CONFIG_XML))
		{
			QMessageBox::StandardButton result = QMessageBox::warning(this,
					tr("Overwrite Configuration and Restart Application"),tr("In order to correctly import the new configuration, "
							"the actual configuration files is going to be overwritten and the application is going to restart.\n "
							"Do you want to continue?"),
							QMessageBox::Ok | QMessageBox::Abort);

			if(result== QMessageBox::Abort)
			{
				avoWarning(funcName) << "Import failed" << fileName;
				delete tmpRoot;
				return;
			}
		}

		tmpRoot->SaveFile(AVO_SERV_CONFIG_XML);
		delete tmpRoot;


		this->release();
		this->init();

	}
	else
	{
		avoWarning(funcName) << "Bad configuration file" << fileName;
	}
}

void Visor::exportXml()
{
	X7S_FUNCNAME("Visor::exportXML()");

	QString fileName = QFileDialog::getSaveFileName(this, tr("Export File"),"",tr("XML (*.xml)"));

	//Return if we didn't want to open a file
	if(fileName.isEmpty()) return;

	//Check if file exist and ask to replace it.
	if(QFile::exists(fileName))
	{
		QMessageBox::StandardButton result = QMessageBox::question(this,
				tr("File already exists"),tr("Do you want to replace the existing file: %1").arg(fileName),
				QMessageBox::Ok | QMessageBox::Cancel);
		if(result== QMessageBox::Cancel)
		{
			avoWarning(funcName) << "Could not replace file" << fileName;
			return;
		}
	}

	//Try to save the file.
	if(root->SaveFile(fileName))
	{
		avoInfo(funcName) << "Correctly exported to " << fileName;
	}
	else
	{
		avoWarning(funcName) << "Could not save to" << fileName;
	}
}


void Visor::DeleteBoard(int board_index)
{
	if(root->devices()->GetSize() == 0){
		return;
	}
	DisconnectAllBoards();
	root->devices()->DelBoardByID(board_index);
	saveXMLFile();
	if(root->devices()->GetSize() == 0){
		AVO_PRINT_DEBUG("DeleteBoard","Error. No board detected");
		QMessageBox::warning(this,tr("Configuration Error"),
				tr("There is no board in your system. The application is going to close."),
				tr("  &Close application  "), QString(), QString(), 0,1);
		QApplication::exit(0);
	}
	else
		ReconnectAllBoards();
}
/*
void Visor::ShowAlarmsWindows(bool enable){

	if (enable){
		eventList->show();
		//ui.actionVertical->setEnabled(true);
		if (numberOfColumms < 4){
			//			splitter->resize(numberOfColumms*(PIXELS_PER_LINE + 10)+ 500, numberOfArrows * (LINES_PER_FRAME + 7));
			QList<int> sizes;
			sizes << numberOfColumms*(PIXELS_PER_LINE + 10) << 500;
			splitter->setSizes(sizes);
			//			this->resize(numberOfColumms*(PIXELS_PER_LINE + 15)+550, numberOfArrows * (LINES_PER_FRAME + 10));
		}
		else{
			//			splitter->resize(numberOfColumms*(PIXELS_PER_LINE + 10), numberOfArrows * (LINES_PER_FRAME + 7) + 500);
			QList<int> sizes;
			sizes << numberOfArrows * (LINES_PER_FRAME + 7) << 500;
			splitter->setSizes(sizes);
			//		    this->resize(numberOfColumms*(PIXELS_PER_LINE + 15), numberOfArrows * (LINES_PER_FRAME + 7) + 500);
		}
	}
	else{
		//		splitter->resize(numberOfColumms*(PIXELS_PER_LINE + 10), numberOfArrows * (LINES_PER_FRAME + 7));
		//		this->resize(numberOfColumms*(PIXELS_PER_LINE + 15), numberOfArrows * (LINES_PER_FRAME + 10));
		eventList->hide();
		//ui.actionVertical->setEnabled(false);
	}
}
*/
void Visor::SetDefaultView()
{
	QList<int> sizes;
	sizes << 3*(MIN_PIXELS_PER_LINE + 10) << videoViewer->minimumWidth();
	splitter->setSizes(sizes);
	RedrawCameras();
	sizes.clear();
	sizes << 1600 << videoViewer->minimumWidth();
	splitter_event->setSizes(sizes);
}

void Visor::HideGraphicalMode(bool enable)
{
	for (int i = 0; i < qboards.size(); i++)
		qboards[i]->enableGraphicalMode(enable==false);
	if (enable==false){
		restoreState(settings->value("mainwindow/state").toByteArray(),AVO_SERVER_MAINWIN_STATE_VER);
		restoreGeometry(settings->value("mainwindow/geometry").toByteArray());
		splitter->show();
		RedrawCameras();
	}
	else{
		settings->setValue("mainwindow/state",saveState(AVO_SERVER_MAINWIN_STATE_VER));
		settings->setValue("mainwindow/geometry",saveGeometry());
		this->resize(30, 30);
		splitter->hide();
	}
	ui.actionDefault_view->setEnabled(enable == false);
}

void Visor::closeEvent(QCloseEvent *event)
{
	warningList->close();
	writeSettings();
	event->accept();
}


void Visor::writeSettings()
{
	settings->setValue("application/xml_config_file", xmlFile);

	settings->setValue("mainwindow/state",saveState(AVO_SERVER_MAINWIN_STATE_VER));
	settings->setValue("mainwindow/geometry",saveGeometry());

	settings->setValue("graphical_mode/visible", ui.actionHide_Graphical_Interface->isChecked());
	settings->setValue("alarms_window/visible", ui.actionShow->isChecked());
	settings->setValue("alarms_window/vertical", ui.actionVertical->isChecked());

	settings->setValue("splitter/config", splitter->saveState());
//	settings->setValue("qcameras/numberOfColumms", numberOfColumms);

	settings->setValue("serverUsers/numberOfServerUsers", serverUsers->size());
	int j = 0;
	for(int i = 0; i < serverUsers->size(); i= i+3)
	{
		settings->setValue("serverUsers/name_" +  QString::number(j), serverUsers->at(i));
		settings->setValue("serverUsers/password_" +  QString::number(j), serverUsers->at(i+1));
		settings->setValue("serverUsers/level_" +  QString::number(j), serverUsers->at(i+2));
		j++;
	}
	settings->setValue("clientUsers/numberOfClientUsers", clientUsers->size());
	j = 0;
	for(int i = 0; i < clientUsers->size(); i= i+3)
	{
		settings->setValue("clientUsers/name_" + QString::number(j), clientUsers->at(i));
		settings->setValue("clientUsers/password_" + QString::number(j), clientUsers->at(i+1));
		settings->setValue("clientUsers/level_" + QString::number(j), clientUsers->at(i+2));
		j++;
	}
#ifdef DEBUG
	settings->setValue(QString("QCameras/WatchDogTime"),timeout_ms);
#endif
}
void Visor::readSettings()
{

	restoreState(settings->value("mainwindow/state").toByteArray(),AVO_SERVER_MAINWIN_STATE_VER);
	restoreGeometry(settings->value("mainwindow/geometry").toByteArray());

	ui.actionShow->setChecked(settings->value("alarms_window/visible", true).toBool());
	ui.actionVertical->setChecked(settings->value("alarms_window/vertical", true).toBool());
	ui.actionHide_Graphical_Interface->setChecked(settings->value("graphical_mode/visible", true).toBool());
	splitter->restoreState(settings->value("splitter/config").toByteArray());

//	numberOfColumms = settings->value("qcameras/numberOfColumms", 2).toInt();
}


void Visor::ResizeCameras(int pos, int index)
{
	X7S_FUNCNAME("Visor::ResizeCameras");

	if(index != 1)
		return;
	RedrawCameras();
}

void Visor::ChangeAlarmsWindowOrientation(bool enable)
{
	if (enable){
		splitter->setOrientation(Qt::Horizontal);
	}
	else{
		splitter->setOrientation(Qt::Vertical);
	}
	RedrawCameras();
}

void Visor::RedrawCameras()
{
	if (qcameras_gridLayout)
		delete qcameras_gridLayout;
	qcameras_gridLayout = new QGridLayout(ui.cameras);
	qcameras_gridLayout->setObjectName(QString::fromUtf8("qcameras_gridLayout"));

	if (events_gridLayout)
		delete events_gridLayout;
	events_gridLayout = new QGridLayout(ui.events);
	events_gridLayout->setObjectName(QString::fromUtf8("events_gridLayout"));

	quint32 x = 0, y = 0;
	QList<int> size = splitter->sizes();
	quint32 size0 = size.at(0)- 50;
	quint32 size_c = 0;

	for (int i = 0; i < qboards.size(); i++){
		for(int j = 0; j < AVO_BOARD_NUMCAM; j++){
			if(qboards[i]->getQCamera(j)->IsDrawable())
			{
				size_c += qboards[i]->getQCamera(j)->GetQCameraSize().width();
				if (size_c < size0){
					qcameras_gridLayout->addWidget(qboards[i]->getQCamera(j), y, x);
					events_gridLayout->addWidget(alrTime.value(AVO_BOARD_NUMCAM*i + j), y, x++);
				}
				else
				{
					size_c = qboards[i]->getQCamera(j)->GetQCameraSize().width();
					x = 0;
					qcameras_gridLayout->addWidget(qboards[i]->getQCamera(j), ++y, x);
					events_gridLayout->addWidget(alrTime.value(AVO_BOARD_NUMCAM*i + j), y, x++);
				}

				qboards[i]->getQCamera(j)->show();
				alrTime.value(AVO_BOARD_NUMCAM*i + j)->show();

			}
			else{
				qboards[i]->getQCamera(j)->hide();
				alrTime.value(AVO_BOARD_NUMCAM*i + j)->hide();
			}
		}
	}
}
void Visor::AddNewBoard()
{
	Board *board_ptr = new Board(root->devices());
	QString string = "192.168.7." + QString::number(254-numberOfBoards);
	board_ptr->SetParam("ip", "%s", string.toStdString().c_str());
	board_ptr->SetParam("port", "%d", 18000);
	board_ptr->SetParam("rtp", "%d", 50000 + 2*numberOfBoards);

	qboards.push_back(new QBoard(ui.cameras, board_ptr, numberOfBoards));
	setBoardParams *board_params = new setBoardParams(0, qboards.last());
	board_params->show();
	board_params->ShowPushButtons(true);
	int added = board_params->exec();

	delete board_params;

	if(added == 0)
	{
		delete qboards.last();
		qboards.pop_back();
		delete board_ptr;
		return;
	}
	//	qboards.push_back(new QBoard(cameras_tab, board_ptr, numberOfBoards));
	ReconnectAllBoards();
}

void Visor::CursorChanged(){
	//	QRect rect = eventTextEdit->cursorRect();
	//avoDebug("Visor::CursorChanged()") << rect;

}

/**
* @brief Process an event from the EventList or AlrTimeWidget and open its corresponding video.
*
* @note If we use a double-click event this function is called twice (one with play=false, and a second one with play=true).
* Therefore we should check if the event is already running.
*
* @param alrEvt The Camera event that we need to proceed.
* @deprecated
*/
bool Visor::EventSelected(const AVOAlrEvt& alrEvt, bool play)
{
	bool ret;
	X7S_FUNCNAME("Visor::EventSelected()");
	AVO_CHECK_WARN(funcName,videoViewer,false,"VideoViewer is NULL");
	Database *db = root->database();

	QDateTime time= QDateTime::fromTime_t(alrEvt.t);
	avoDebug(funcName)<< "Start"  << time << alrEvt.cam << alrEvt.blob_ID << play;

	//First check if we have clicked on the same alarm
	if(AVO_STRUCT_EQUAL(alrEvt,videoViewer->getLastAlrEvt()))
	{
		//Do nothing
		avoDebug(funcName) << "Click on same event... do nothing";
	}
	else
	{
		//Check if the alrEvt correspond to video already opened
		const VideoInFile* pVid = videoViewer->getPVideo();
		if(pVid && pVid->isValid() && pVid->hasAlrEvent(alrEvt))
		{
			if(!pVid->isOpened())
			{
				ret = videoViewer->Open(videoViewer->getVideo());
				if(ret==false) return false;
			}
		}

		//
		//		const VideoInFile& vid = videoViewer->getVideo();
		//		if(vid.isValid() && vid.hasAlrEvent(alrEvt))
		//		{
		//			if(!vid.isOpened()) {
		//				ret = videoViewer->Open(videoViewer->getVideo());
		//				if(ret==false) return false;
		//			}
		//		}
		//Otherwise search a video in the database.
		else
		{
			VideoInFile videofile = db->GetVideo(alrEvt.cam,time);
			ret = videoViewer->Open(videofile);
			if(ret==false) return false;
		}

		//Then go to the correct frame.
		videoViewer->JumpToAlarm(alrEvt);
		videoViewer->displayFrame();
	}
	if(play) videoViewer->PlayPause(true);
	avoDebug(funcName) << "End" << time << alrEvt.cam << play;
	return true;
}


bool Visor::EventSelected(const CamEvtCarrier& camEvt, bool play)
{
	X7S_FUNCNAME("Visor::EventSelec(camEvt)");
	avoDebug(funcName);
	bool ret=false;

	//Check if the CamEvtCarrier is from the good type.
	if(camEvt.isType(CamEvt::VSAlarm))
	{
		CamEvtVSAlarm *camEvtAlr=(CamEvtVSAlarm*)camEvt.getCamEvt();

		//Copy the new camAlrEvt to the old struct
		AVOAlrEvt avoAlrEvt;
		avoAlrEvt.t=camEvtAlr->getTime_t();
		avoAlrEvt.cam=camEvtAlr->getCamera();
		AVO_STRUCT_COPY(avoAlrEvt, camEvtAlr->vsAlrEvt);

		ret=this->EventSelected(avoAlrEvt,play);
	}
	else
	{
		avoWarning(funcName) << "Event is not of the correct type";
	}

	//Emit that the event open a file to be played.
	emit playingEvt(camEvt,ret);

	return ret;
}

int Visor::CheckServerUser(const QString &name, const QString &password)
{
	if (serverUsers->size() <= 2)
		return 0;
	for(int i = 0; i < serverUsers->size()- 2; i++)
	{
		if (name == serverUsers->at(i)){
			QCryptographicHash *hash_p = new QCryptographicHash(QCryptographicHash::Sha1);
			QByteArray data_p = QByteArray();
			data_p.append(password);
			hash_p->addData(data_p);
			QByteArray hash_result_p = hash_p->result().toHex();

			QCryptographicHash *hash = new QCryptographicHash(QCryptographicHash::Sha1);
			QByteArray data = QByteArray();
			data.append(name);
			data.append(hash_result_p);
			data.append(serverUsers->at(i+2));
			hash->addData(data);
			QByteArray hash_result = hash->result().toHex();

			avoDebug("Visor::CheckServerUser()") << "Hash-1 result: " << hash_result;

			if (hash_result == serverUsers->at(i+1))
				return serverUsers->at(i+2).toInt();
		}
	}
	return -1;
}

int Visor::EnableAdminMode(bool enable)
{
	if (enable && serverUsers->size())
	{
		QString name = "";
		QString password = "";
		int result = 1;
		while(result){
			AuthenticationDialog accessDialog(0, &name, &password);
			accessDialog.show();
			result = accessDialog.exec();
			avoDebug("Visor::EnableAdminMode()")<<quint32(result) << name << password;
			if (result == 0){
				ui.actionEnter_Admin_mode->setChecked(false);
				return -1;
			}
			result = CheckServerUser(name, password);
			if(result == -1)//No user
			{
				QMessageBox::warning(this,tr("User Error"),
						tr("User or Password not correct"),
						tr("  &OK  "), "",0,1);

				ui.actionEnter_Admin_mode->setChecked(false);
				return result;
			}
			if(result != 0)//No admin
			{
				QMessageBox::information( this, tr("User logged"),tr("You don't have admin privileges. Contact to your security manager."),
						QMessageBox::Ok|QMessageBox::Default,
						QMessageBox::NoButton, QMessageBox::NoButton);

				ui.actionEnter_Admin_mode->setChecked(false);
				return result;
			}
		}
	} //End enable.

	for(int i = 0; i < qboards.size(); i++)
		qboards[i]->SetAdminMode(enable);

	ui.menuFile->setEnabled(enable);
	ui.menuUsers->setEnabled(enable);

	QList<QAction*> prefList = ui.menuPreference->actions();
	for(int i=0;i<prefList.size();i++) 	prefList[i]->setEnabled(enable);
	ui.menuPreference->setEnabled(true);
	ui.actionEnter_Admin_mode->setEnabled(true);
	ui.actionEnter_Admin_mode->setChecked(enable);

	if(enable)
		return 0;//Return Admin
	return 1;
}

void Visor::AddNewServerUser()
{
	new_user = new addNewUser(0, serverUsers);
	new_user->show();
	connect(new_user, SIGNAL(newUserAdded()), this, SLOT(writeSettings()));
}

void Visor::AddNewClientUser()
{
	new_user = new addNewUser(0, clientUsers);
	new_user->show();
	connect(new_user, SIGNAL(newUserAdded()), this, SLOT(writeSettings()));
}

void Visor::ConfigServerUsers()
{
	config_user = new usersConfigParams(0, serverUsers);
	config_user->show();
	//config_user->exec();
	connect(config_user, SIGNAL(userModifiedSignal()), this, SLOT(writeSettings()));
}

void Visor::ConfigClientUsers()
{
	config_user = new usersConfigParams(0, clientUsers);
	config_user->show();
	//config_user->exec();
	connect(config_user, SIGNAL(userModifiedSignal()), this, SLOT(writeSettings()));
}

void Visor::AdvanceConfig()

{
	QDialog dialog(0);
	QVBoxLayout *layout = new QVBoxLayout;
	dialog.setLayout(layout);
	XmlObjTreeConfWidget *advanceConfig = new XmlObjTreeConfWidget(root, this);
	connect(advanceConfig, SIGNAL(closedWidget()), &dialog, SLOT(accept()));
	layout->addWidget(advanceConfig);
	dialog.setModal(true);
	dialog.show();
	dialog.exec();
	delete advanceConfig;
	delete layout;
}

void Visor::PurgeDatabase()
{
	int ret = QMessageBox::warning(this, tr("Purge Database"),
			tr("Do you want to purge the database?\n"
					"(Remove data older than 1 month)"),
					QMessageBox::Ok  | QMessageBox::Cancel
	);

	VideoHandler *vidHandler = root->videoHandler();

	if(ret==QMessageBox::Ok && vidHandler)
	{
		int ret2 = QMessageBox::warning(this, tr("Delete physical files"),
				tr("Also delete the corresponding video the hard drive?"),
				QMessageBox::YesToAll  | QMessageBox::No
		);


		QDateTime lastMonth = QDateTime::currentDateTime().addMonths(-1);
		vidHandler->DeleteOldVideo(lastMonth,ret2==QMessageBox::YesToAll);
	}
}


void Visor::receivedTick(const QDateTime& actual)
{
	avoInfo("Visor::receivedTick()") << actual;
}

void Visor::updateLogFile(const QDateTime& actual)
{
	avoInfo("Visor::updateLogFile()") << actual;
	LogConfig* logConf=NULL;
	if(root) logConf = root->logConfig();
	if(logConf) logConf->UpdateLogFiles();
}

void Visor::updatePurgeVideo(const QDateTime& actual)
{
	avoInfo("Visor::updatePurgeVideo()") << actual;
	VideoHandler* vidHandler=NULL;
	if(root) vidHandler = root->videoHandler();
	if(vidHandler) vidHandler->DeleteOldVideo(actual.addMonths(-1),true);
}


void Visor::updateStats(const QDateTime& actual)
{
	Devices *dev=NULL;
	Board* board=NULL;
	if(root) dev= root->devices();
	if(dev)
	{
		for(int i=0;i<dev->GetSize();++i)
		{
			board = dev->GetBoard(i);
			if(board) avoInfo("Visor::updateStats()") << board->GetStats();
		}
	}
}

