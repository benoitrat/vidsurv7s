/*
 * setBoardParams.cpp
 *
 *  Created on: 08/09/2009
 *      Author: Gabri
 */
#include <QDebug>
#include <QMessageBox>
#include "setBoardParams.h"
#include "QBoard.h"
#include "QCameraServer.h"
#include "Board.hpp"
#include "QtBind.hpp"
#include "Camera.hpp"


setBoardParams::setBoardParams(QDialog *parent, QBoard *qboard_p)
    : QDialog(parent)
{
	ui.setupUi(this);

	qboard = qboard_p;
	board = qboard->getBoard();

	connect(ui.OK_pushButton, SIGNAL(clicked()), this, SLOT(ApplyNewValues()));
	connect(ui.cancel_pushButton, SIGNAL(clicked()), this, SLOT(Cancel()));
	connect(ui.DeleteBoard_pushButton, SIGNAL(clicked()), this, SLOT(DeleteBoard()));

	ui.ipAddress_lineEdit->setText(QtBind::toQString(board->GetParam("ip")));
	ui.udpPort_spinBox->setValue(board->GetParam("port")->toIntValue());
	ui.rtpPort_spinBox->setValue(board->GetParam("rtp")->toIntValue());
	ui.cbox_enable->setChecked(board->GetParam("enable")->IsOk());
	ui.version_Label->setText(board->GetFWVersion());

	Camera *camera = board->GetCamera(0);
	ui.camera1_checkBox->setChecked(camera->IsEnable());
	ui.camera1_checkBox->setText(camera->GetName());
	camera = board->GetCamera(1);
	ui.camera2_checkBox->setChecked(camera->IsEnable());
	ui.camera2_checkBox->setText(camera->GetName());
	camera = board->GetCamera(2);
	ui.camera3_checkBox->setChecked(camera->IsEnable());
	ui.camera3_checkBox->setText(camera->GetName());
	camera = board->GetCamera(3);
	ui.camera4_checkBox->setChecked(camera->IsEnable());
	ui.camera4_checkBox->setText(camera->GetName());

	ShowPushButtons(false);

}

setBoardParams::~setBoardParams()
{

}

void setBoardParams::ApplyNewValues()
{
	//Actualizamos los valores de conexi�n de la board
	board->SetParam("ip", "%s", ui.ipAddress_lineEdit->text().toStdString().c_str());
	board->SetParam("port", "%d", ui.udpPort_spinBox->value());
	board->SetParam("rtp", "%d", ui.rtpPort_spinBox->value());
	board->SetParam("enable","%d",ui.cbox_enable->isChecked());

	//Habilitamos y deshabilitamos las c�maras
	QCameraServer *camera = qboard->getQCamera(0);
	camera->enableQCamera(ui.camera1_checkBox->isChecked());
	//camera->SetParam("enable", "%d",ui.camera1_checkBox->isChecked());
	ui.camera1_checkBox->setText(camera->getName());//Actualizamos el nombre de las c�maras en los checkBox

	//camera = board->GetCamera(1);
	//camera->SetParam("enable", "%d",ui.camera2_checkBox->isChecked());
	camera = qboard->getQCamera(1);
	camera->enableQCamera(ui.camera2_checkBox->isChecked());
	ui.camera2_checkBox->setText(camera->getName());//Actualizamos el nombre de las c�maras en los checkBox

	//camera = board->GetCamera(2);
	//camera->SetParam("enable", "%d",ui.camera3_checkBox->isChecked());
	camera = qboard->getQCamera(2);
	camera->enableQCamera(ui.camera3_checkBox->isChecked());
	ui.camera3_checkBox->setText(camera->getName());//Actualizamos el nombre de las c�maras en los checkBox

	//camera = board->GetCamera(3);
	//camera->SetParam("enable", "%d",ui.camera4_checkBox->isChecked());
	camera = qboard->getQCamera(3);
	camera->enableQCamera(ui.camera4_checkBox->isChecked());
	ui.camera4_checkBox->setText(camera->getName());//Actualizamos el nombre de las c�maras en los checkBox

	if (ui.OK_pushButton->isVisible())
		done(1);
}
void setBoardParams::Cancel()
{
	done(0);
}
void setBoardParams::ShowPushButtons(bool enable)
{
	if (enable){
		ui.OK_pushButton->show();
		ui.cancel_pushButton->show();
		ui.DeleteBoard_pushButton->hide();
	}
	else{
		ui.OK_pushButton->hide();
		ui.cancel_pushButton->hide();
		ui.DeleteBoard_pushButton->show();
	}

}
void setBoardParams::DeleteBoard()
{
	int result = QMessageBox::question( this, tr("Delete Board Warning"),tr("You are going to delete this board, are you sure?"),
					QMessageBox::Yes,
					QMessageBox:: No|QMessageBox::Default, QMessageBox::NoButton);
	if (result == QMessageBox::No){//Habilitamos men�s de admin
		return;
	}
	emit DeleteBoardSignal(qboard->getBoard()->GetID());
	done(0);
}

