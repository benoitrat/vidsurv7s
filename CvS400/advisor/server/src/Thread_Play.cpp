#include "Thread_Play.h"
#include "x7snet.h"
#include "Board.hpp"
#include <cv.h>
#include <highgui.h> 
#include "Camera.hpp"
#include "vv_defines.h"
#include "Log.hpp"
#include "QCameraTimeoutThread.h"


#define IAVO_DISPLAY_CHRONO(funcName,chrono) \
	if(chrono.elapsed()>1000) { AVO_PRINT_INFO(funcName,"board %d, elapsed=%.3f s",board->GetSpecialID(),(double)chrono.restart()/1000.0); }


Thread_Play::Thread_Play(QObject *parent, Board *board_ptr)
: QThread(parent)
  {
	in_progress = false;
	stop = false;
	board = board_ptr;
	graphical_mode = true;
    timeout = new QCameraTimeoutThread(this, IMAGES_TIMEOUT);
    connect (timeout, SIGNAL(TimeoutError()), this, SLOT(TimeoutError()));
	//board_index = index;
  }

Thread_Play::~Thread_Play()
{
	AVO_PRINT_DEBUG("Thread_Play::~Thread_Play()", "0");
	if (in_progress){
		stop = true;
		wait();
	}
	delete timeout;
	//	board->Close();
	AVO_PRINT_DEBUG("Thread_Play::~Thread_Play()", "1");
}

void Thread_Play::SetBoard(Board *brd)
{
	board = brd;
}

bool Thread_Play::ExecutePlay()
{
	X7S_FUNCNAME("Thread_Play::ExecutePlay()");

	AVO_PRINT_INFO("Thread_Play::ExecutePlay()", "before start()");

	if (in_progress){
		stop = true;
		wait();
	}
	if(board->Start()){
		stop = false;
		start();
		AVO_PRINT_INFO("Thread_Play::ExecutePlay()", "after start()");
		return true;
	}
	AVO_PRINT_WARN(funcName,"Error while starting!");
	return false;
}

void Thread_Play::StopPlay()
{
   AVO_PRINT_INFO("Thread_Play::StopPlay()");
	timeout->StopTimeout();//Paramos el timeout timer
	stop = true;
}

void Thread_Play::TimeoutError()
{
	AVO_PRINT_INFO("Thread_Play::TimeoutError()","%d",board->GetSpecialID());
	stop = true;//Paramos la hebra de recibir imagenes.
	//timeout->StopTimeout();//Paramos el timeout timer
	emit TimeoutErrorSignal("Board " + QString::number(board->GetSpecialID())+ ": images not received from S400");
}

void Thread_Play::run()
{
	X7S_FUNCNAME("Thread_Play::run()");
	in_progress = true;
	int camera_index;
	//QImage image(PIXELS_PER_LINE, LINES_PER_FRAME, QImage::Format_ARGB32);
	QImage image;
	QImage image_b;
	//QImage image_b(PIXELS_PER_LINE, LINES_PER_FRAME, QImage::Format_ARGB32);
	Camera* camera;
	const X7sByteArray *metaJPEG;
	QByteArray qmetaJPEG;

	bool isRecording;
	bool recordingChanged;

	AVO_PRINT_DEBUG(funcName, "Starting thread for board %d...", board->GetSpecialID());

	AVO_PRINT_DEBUG(funcName, "Start OK");
	timeout->StartTimeout();

	QTime chrono;	//Start chrono
	while(stop==false)
	{

		chrono.start();

		camera_index = board->ProcessFrame();
		IAVO_DISPLAY_CHRONO(funcName,chrono);


		//Check if the state of recording has changed
		isRecording=board->IsRecording(&recordingChanged);
		if((recordingChanged)&&(stop==false))
			emit recordingLEDToggled();
		IAVO_DISPLAY_CHRONO(funcName,chrono);

		if ((camera_index >= 0)&&(camera_index < AVO_BOARD_NUMCAM)&&(stop==false))
		{
			camera = board->GetCamera(camera_index);
			if (camera == NULL) {
				AVO_PRINT_WARN(funcName,"camera==NULL (%d)",board->GetSpecialID(camera_index));
				continue;
			}
			timeout->NewImage();
			emit newFrameReceived(camera);

			metaJPEG = camera->GetMetaJPEGFrame();
			qmetaJPEG = QByteArray((const char*)metaJPEG->data, (int)metaJPEG->length);
			emit CameraMJPEGReceived(qmetaJPEG, camera_index);
			IAVO_DISPLAY_CHRONO(funcName,chrono);

			if(graphical_mode){
				image = QImage(camera->imageFrame());
				//image_b = QImage(camera->imageMData());
				//image = camera->imageFrame().copy(0,0,camera->imageFrame().width(), camera->imageFrame().height());
				image_b = camera->imageMData().copy(0,0,camera->imageFrame().width(), camera->imageFrame().height());
				emit CameraImageReceived(image, image_b, camera_index);
			}
		}
		IAVO_DISPLAY_CHRONO(funcName,chrono);

	}	//End of life
	AVO_PRINT_DEBUG(funcName,"stopping timeout ");
	timeout->StopTimeout();
	AVO_PRINT_WARN(funcName,"timeout stopped...stop thread for board %d",board->GetSpecialID());
	in_progress = false;
	return;
}
