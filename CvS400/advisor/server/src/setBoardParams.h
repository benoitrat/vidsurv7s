/*
 * setBoardParams.h
 *
 *  Created on: 08/09/2009
 *      Author: Gabri
 */

#ifndef SETBOARDPARAMS_H_
#define SETBOARDPARAMS_H_

#include <QDialog>
#include "ui_setBoardParams.h"


class QBoard;
class Board;

class setBoardParams : public QDialog
{
    Q_OBJECT

public:
    setBoardParams(QDialog *parent = 0, QBoard *qboard_p = 0);
    ~setBoardParams();

public slots:
    void ApplyNewValues();
    void ShowPushButtons(bool enable);
    void DeleteBoard();


signals:
	void DeleteBoardSignal(int board_index);

private:
    Ui::setBoardParamsClass ui;

    Board *board;
    QBoard *qboard;

private slots:
	void Cancel();

};

#endif /* SETBOARDPARAMS_H_ */
