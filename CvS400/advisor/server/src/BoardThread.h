/**
 *  @file
 *  @brief Contains the class BoardThread.hpp
 *  @date Jun 25, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef BOARDTHREAD_HPP_
#define BOARDTHREAD_HPP_

/**
 *	@brief The BoardThread.
 *	@ingroup avo_server
 */
#include "Board.hpp"
#include "Thread_Play.h"

#include <QThread>
#include <QMutex>
#include <QWaitCondition>


enum BoardCommand {
		AVO_BCOMMAND_IDDLE,
		AVO_BCOMMAND_CONNECT,
		AVO_BCOMMAND_RECONNECT,
		AVO_BCOMMAND_PING,
		AVO_BCOMMAND_SETUP,
		AVO_BCOMMAND_UPDATE,
		AVO_BCOMMAND_PLAY,
		AVO_BCOMMAND_POOL,
		AVO_BCOMMAND_STOP, 
		AVO_BCOMMAND_TOGGLELED,
		AVO_BCOMMAND_CLOSE		
};


class BoardThread: public QThread {

	Q_OBJECT

public:
	BoardThread(QObject *parent=0, Board *board_ptr = 0, Thread_Play *play_thread_ptr = 0);
	~BoardThread();

//	void SetState(BoardState state);
	void SetBoard(Board *brd);
	bool ExecuteCommand(BoardCommand commnd);
	bool GetBoardConnected(){return boardConnected;};
	void SetBoardConnected(bool conn){boardConnected = conn;};
	static QString BoardCmdDesc(BoardCommand cmd);

	signals:
	void WarningOnBoardThreadSignal(const QString &warning);


protected:
    void run();

protected slots:
	void PlayTimeoutError();
	void ToggleRecordingLED();

protected:
	void snap(int seconds);

private:
	Board *board;
	QVector<BoardCommand> v_command;
	QWaitCondition commandIsArrived;
	QWaitCondition commandIsPerformed;
	QMutex mutex;
	bool stop; 
	bool boardConnected;
	Thread_Play *play_thread;
	bool thread_run;

};

#endif /* BOARDTHREAD_HPP_ */
