#ifndef CLIENTSOCKET_DEVICES_H_
#define CLIENTSOCKET_DEVICES_H_

#include <QTcpSocket>
#include <QByteArray>
#include <QList>
#include <QString>

#include "vv_defines.h"

class Devices;



class ClientSocket_Devices : public QTcpSocket
{
    Q_OBJECT

public:
    ClientSocket_Devices(QObject *parent = 0, Devices *devices_ptr = 0, QList<QString> *userList_pt = 0);
    ~ClientSocket_Devices();

signals:
	void Board_Stop();
	void Board_Play();
	void newUserLoggedSignal(const QString & name);
	void SaveXMLFileSignal();
	void SaveXMLTmpFileSignal();
//	void enableCamera(bool enable, quint8 camera);
//	void updateParams();

	
	
private slots:
    void readClient();
//    void imageToClient(const QByteArray &qmeta_jpeg, int camera_index);
//	void sendImageToClient(int camera);
	void sendXMLFileToClient();
	void sendUserLoggedToClient(bool logged);
	void error();

private:
	Devices *devices;
    quint32 nextBlockSize;
    bool user_logged;
	QString user_name;
	QString user_password;
	qint8	user_level;
	QList<QString> *clientUsers;
/*    QBoard *board;
    QByteArray qmetaJPEG[4];
    bool 	imageCameraAsked[4];
*/
};


#endif /*CLIENTSOCKET_DEVICES_H_*/
