#include "visor.h"
#include "QServer.h"
#include <QtGui>
#include <QApplication>
#include <QTranslator>
#include "Log.hpp"

int main(int argc, char *argv[])
{

	 qInstallMsgHandler(AVOMsgOutput);
   QApplication a(argc, argv);

	//Set the logging level
    Log::SetLevel(5);
//	x7sNetLog_SetLevel(X7S_LOGLEVEL_MED);
//	x7sXmlLog_SetLevel(X7S_LOGLEVEL_HIGHEST);
//	x7sVSLog_SetLevel(X7S_LOGLEVEL_MED);

    QLocale::setDefault(QLocale("es_ES"));

    QString qmDir=a.applicationDirPath()+"/locale";

    QTranslator qtTranslator;
	 qtTranslator.load("qt_"+QLocale().name(),qmDir);
	 a.installTranslator(&qtTranslator);

	 QTranslator wgtTranslator;
	 wgtTranslator.load("widgets_"+QLocale().name(),qmDir);
	 a.installTranslator(&wgtTranslator);

   QTranslator translator;
   translator.load("server_"+QLocale().name(),qmDir);
   a.installTranslator(&translator);


    Visor w;
    w.show();
    return a.exec();
}
