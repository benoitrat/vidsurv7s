/*
 * setCameraParams.cpp
 *
 *  Created on: 08/09/2009
 *      Author: Gabri
 */
#include <QColorDialog>
#include <QTextStream>
#include <QDir>
#include <QFileDialog>

#include "Log.hpp"
#include "setCamerasParams.h"
#include "Camera.hpp"
#include "QCameraServer.h"
#include "QtBind.hpp"
#include "EditQLabel.h"
#include "vv_defines.h"




setCamerasParams::setCamerasParams(QDialog *parent, QCameraServer *qcamera_pt)
: QDialog(parent)
  {
	AVO_PRINT_DEBUG("setCamerasParams::setCamerasParams()");

	ui.setupUi(this);

	qcamera = qcamera_pt;
	camera = qcamera->getCamera();

	ui.cameraName_lineEdit->setText(camera->GetName());

	ui.MJPEGWrite_checkBox->setChecked(camera->GetParam("mjpeg_write")->toIntValue() == 1);
	ui.enable_Surveillance_checkBox->setChecked(camera->GetVidSurvPipeline()->GetParam("enable")->toIntValue() == 1);
	ui.MJPEGRestart_spinBox->setValue(camera->GetParam("mjpeg_restart")->toIntValue());


	bg_ccMinArea = camera->GetParam("bg_ccMinArea")->toIntValue();
	ui.bg_ccMinArea_SpinBox->setValue(bg_ccMinArea);

	bg_insDelayLog = camera->GetParam("bg_insDelayLog")->toIntValue();
	ui.bg_insDelayLog_SpinBox->setValue(bg_insDelayLog);

	bg_refBGDelay = camera->GetParam("bg_refBGDelay")->toIntValue();
	ui.bg_refBGDelay_SpinBox->setValue(bg_refBGDelay);

	bg_refFGDelay = camera->GetParam("bg_refFGDelay")->toIntValue();
	ui.bg_refFGDelay_SpinBox->setValue(bg_refFGDelay);

	bg_threshold = camera->GetParam("bg_threshold")->toIntValue();
	ui.bg_threshold_SpinBox->setValue(bg_threshold);

	connect(ui.applyButton, SIGNAL(clicked()), this, SLOT(ApplyNewBG_Values()));
	connect(ui.cameraName_lineEdit, SIGNAL(textChanged ( const QString &)), this, SLOT(EnableNamePushButton()));
	connect(ui.applyName_pushButton, SIGNAL(clicked ()), this, SLOT(ApplyCameraName()));
	connect(ui.delAlarm_pushButton, SIGNAL(clicked()), this, SLOT(DeleteAlarm()));
	connect(ui.changeColor_pushButton, SIGNAL(clicked()), this, SLOT(ChangeAlarmColor()));
	connect(ui.alarms_comboBox, SIGNAL(currentIndexChanged ( int )), this, SLOT(EnableAlarmPushButtons()));
	connect(ui.editAlarm_pushButton, SIGNAL(clicked()), this, SLOT(EditAlarm()));
	connect(ui.applyEditAlarm_pushButton, SIGNAL(clicked()), this, SLOT(ApplyEditAlarm()));
	connect(ui.cancelEditAlarm_pushButton, SIGNAL(clicked()), this, SLOT(ChangeImageLabel()));
	connect(ui.apply_pushButton, SIGNAL(clicked()), this, SLOT(ApplyNewValues()));
	connect(ui.OK_pushButton, SIGNAL(clicked()), this, SLOT(ApplyNewValuesAndClose()));
	alrMgr = camera->GetVidSurvPipeline()->GetAlarmManager();
	for(int i=0;i<alrMgr->Size();i++){
		X7sVSAlrDetector *alrDtr = alrMgr->Get(i);

		CvScalar rgba=cvScalar(255,255,255,255);
		X7sXmlParamGetVector(&rgba,alrDtr->GetParam("color"));
		QColor color(rgba.val[2],rgba.val[1],rgba.val[0],rgba.val[3]);


		ui.alarms_comboBox->addItem(QtBind::toQString(alrDtr->GetParam("name")), QVariant(alrDtr->GetID()));
		ui.alarms_comboBox->setItemData(i+1, color, Qt::DecorationRole/*BackgroundRole, TextColorRole*/);
	}

	//noAvailableImage = QImage("no_camera.png");
	image = QImage(qcamera->image_v);//.copy(0,0,PIXELS_PER_LINE, LINES_PER_FRAME);
	ChangeImageLabel();

	ui.apply_pushButton->hide();
	ui.cancel_pushButton->hide();
	ui.OK_pushButton->hide();

	ui.tabWidget->setCurrentIndex(0);
	videoPath = QtBind::toQString(camera->GetParam("mjpeg_rootdir"));
	ui.videoPath_lineEdit->setText(videoPath);
	connect(ui.videoPathBrowse_pushButton, SIGNAL(clicked()), this, SLOT(BrowseVideoPath()));

	AddTrackingType();

  }

setCamerasParams::~setCamerasParams()
{
	//delete painter;
	//delete noAvailableImage;
	//delete pixmap;
}

void setCamerasParams::ApplyNewBG_Values()
{
	if(bg_ccMinArea != ui.bg_ccMinArea_SpinBox->value())
		camera->SetParam("bg_ccMinArea", "%d", ui.bg_ccMinArea_SpinBox->value());
	if(bg_insDelayLog != ui.bg_insDelayLog_SpinBox->value())
		camera->SetParam("bg_insDelayLog", "%d", ui.bg_insDelayLog_SpinBox->value());
	if(bg_refBGDelay != ui.bg_refBGDelay_SpinBox->value())
		camera->SetParam("bg_refBGDelay", "%d", ui.bg_refBGDelay_SpinBox->value());
	if(bg_refFGDelay != ui.bg_refFGDelay_SpinBox->value())
		camera->SetParam("bg_refFGDelay", "%d", ui.bg_refFGDelay_SpinBox->value());
	if(bg_threshold != ui.bg_threshold_SpinBox->value())
		camera->SetParam("bg_threshold", "%d", ui.bg_threshold_SpinBox->value());

	//emit ValuesUpdated();

	//close();
}
void setCamerasParams::EnableNamePushButton()
{
	ui.applyName_pushButton->setEnabled(true);
}

void setCamerasParams::EnableAlarmPushButtons()
{
	int index = ui.alarms_comboBox->currentIndex();
	if (index == 0){
		ui.delAlarm_pushButton->setEnabled(false);
		ui.changeColor_pushButton->setEnabled(false);
		ui.editAlarm_pushButton->setEnabled(false);
		ui.applyEditAlarm_pushButton->setEnabled(false);
		ui.cancelEditAlarm_pushButton->setEnabled(false);
		ui.alarms_comboBox->setEnabled(true);
		ChangeImageLabel();
		return;
	}


	ui.delAlarm_pushButton->setEnabled(true);
	ui.changeColor_pushButton->setEnabled(true);
	ui.editAlarm_pushButton->setEnabled(true);
	//Dibujamos la alarma seleccionada

	pixmap = QPixmap::fromImage(image);
	painter = new QPainter(&pixmap);

	ui.Image_label->hide();
	bool ok=false;
	int ID = ui.alarms_comboBox->itemData(index).toInt(&ok);
	if(ok==false) return;

	if (alrMgr->GetByID(ID)->GetType() == X7S_VS_ALR_TYPE_LINE){
		QColor color = QtBind::toQColor(alrMgr->GetByID(ID)->GetParam("color"));
		QLine line = QtBind::toQLine(alrMgr->GetByID(ID));
		painter->setPen(QPen(color, 3));
		if(!line.isNull()) painter->drawLine(line);
	}

	if (alrMgr->GetByID(ID)->GetType() == X7S_VS_ALR_TYPE_POLY){
		QColor color = QtBind::toQColor(alrMgr->GetByID(ID)->GetParam("color"));
		painter->setPen(QPen(color, 3));
		QPolygon poly = QtBind::toQPolygon(alrMgr->GetByID(ID));
		if(!poly.isEmpty()) painter->drawPolygon(poly);
	}

	ui.Image_label->setBackgroundRole(QPalette::Dark);
	ui.Image_label->setAutoFillBackground(true);
	ui.Image_label->setAlignment(Qt::AlignLeft | Qt::AlignTop);
	ui.Image_label->setPixmap(pixmap);
	ui.Image_label->show();

	delete painter;
}

void setCamerasParams::ApplyCameraName()
{
	qcamera->setName((QString)ui.cameraName_lineEdit->text());
	camera->SetParam("name","%s",ui.cameraName_lineEdit->text().toStdString().c_str());
}

void setCamerasParams::DeleteAlarm()
{
	int index = ui.alarms_comboBox->currentIndex();
	if (index == 0)
		return;
	ui.Image_label->hide();
	bool ok=false;
	int ID = ui.alarms_comboBox->itemData(index).toInt(&ok);
	if(ok==false) return;
	//alrMgr->Delete(ID);
	qcamera->DeleteAlarm(ID);
	ui.alarms_comboBox->removeItem ( index );
	ChangeImageLabel();
}

void setCamerasParams::setImageToLabel(const QImage& image)
{
	pixmap = QPixmap::fromImage(image);
	painter = new QPainter(&pixmap);

	for(int i=0;i<alrMgr->Size();i++){
		if (alrMgr->Get(i)->GetType() == X7S_VS_ALR_TYPE_LINE){
			QColor color = QtBind::toQColor(alrMgr->Get(i)->GetParam("color"));
			QLine line = QtBind::toQLine(alrMgr->Get(i));
			painter->setPen(QPen(color, 3));
			if(!line.isNull()) painter->drawLine(line);
		}
		if (alrMgr->Get(i)->GetType() == X7S_VS_ALR_TYPE_POLY){
			QColor color = QtBind::toQColor(alrMgr->Get(i)->GetParam("color"));
			QPolygon poly = QtBind::toQPolygon(alrMgr->Get(i));
			painter->setPen(QPen(color, 3));
			if(!poly.isEmpty()) painter->drawPolygon(poly);
		}
	}

	ui.Image_label->setBackgroundRole(QPalette::Dark);
	ui.Image_label->setAutoFillBackground(true);
	ui.Image_label->setAlignment(Qt::AlignLeft | Qt::AlignTop);
	ui.Image_label->setPixmap(pixmap);
	ui.Image_label->show();
	delete painter;
}

void setCamerasParams::ChangeAlarmColor()
{
	//bool ok;
	int index = ui.alarms_comboBox->currentIndex();
	if (index == 0)
		return;
	bool ok=false;
	int ID = ui.alarms_comboBox->itemData(index).toInt(&ok);
	if(ok==false) return;
	QColor color_i = QtBind::toQColor(alrMgr->GetByID(ID)->GetParam("color"));
	QColor color = QColorDialog::getColor(color_i, this);
	if(color.isValid())
	{
		ui.alarms_comboBox->setItemData(index, color, Qt::DecorationRole/*BackgroundRole, TextColorRole*/);
		alrMgr->GetByID(ID)->SetParam("color","%d %d %d %d",color.blue(), color.green(), color.red(), color.alpha());
		alrMgr->GetByID(ID)->Reload();
	}

	ChangeImageLabel();

}

void setCamerasParams::ChangeImageLabel()
{
	pixmap = QPixmap::fromImage(image);
	painter = new QPainter(&pixmap);
	if (ui.cancelEditAlarm_pushButton->isEnabled())
		edit_label->hide();

	for(int i=0;i<alrMgr->Size();i++){
		if (alrMgr->Get(i)->GetType() == X7S_VS_ALR_TYPE_LINE){
			QColor color = QtBind::toQColor(alrMgr->Get(i)->GetParam("color"));
			QLine line = QtBind::toQLine(alrMgr->Get(i));
			painter->setPen(QPen(color, 3));
			if(!line.isNull()) painter->drawLine(line);
		}
		if (alrMgr->Get(i)->GetType() == X7S_VS_ALR_TYPE_POLY){
			QColor color = QtBind::toQColor(alrMgr->Get(i)->GetParam("color"));
			QPolygon poly = QtBind::toQPolygon(alrMgr->Get(i));
			painter->setPen(QPen(color, 3));
			if(!poly.isEmpty()) painter->drawPolygon(poly);
		}
	}

	ui.Image_label->setBackgroundRole(QPalette::Dark);
	ui.Image_label->setAutoFillBackground(true);
	ui.Image_label->setAlignment(Qt::AlignLeft | Qt::AlignTop);
	ui.Image_label->setPixmap(pixmap);
	ui.Image_label->show();

	ui.CameraImage_label->setBackgroundRole(QPalette::Dark);
	ui.CameraImage_label->setAutoFillBackground(true);
	ui.CameraImage_label->setAlignment(Qt::AlignLeft | Qt::AlignTop);
	ui.CameraImage_label->setPixmap(pixmap);
	ui.CameraImage_label->show();

	delete painter;

	ui.alarms_comboBox->setEnabled(true);
	if (ui.alarms_comboBox->currentIndex()){
		ui.alarms_comboBox->setCurrentIndex(0);
		EnableAlarmPushButtons();
	}
	//	ui.delAlarm_pushButton->setEnabled(false);
	//	ui.changeColor_pushButton->setEnabled(false);
	//	ui.editAlarm_pushButton->setEnabled(false);
}
void setCamerasParams::EditAlarm()
{
	int index = ui.alarms_comboBox->currentIndex();
	if (index == 0)
		return;
	ui.alarms_comboBox->setEnabled(false);
	bool ok=false;
	int ID = ui.alarms_comboBox->itemData(index).toInt(&ok);
	if(ok==false) return;
	ui.Image_label->hide();
	ui.delAlarm_pushButton->setEnabled(false);
	ui.changeColor_pushButton->setEnabled(false);
	ui.editAlarm_pushButton->setEnabled(false);
	if (alrMgr->GetByID(ID)->GetType() == X7S_VS_ALR_TYPE_LINE){
		QColor color = QtBind::toQColor(alrMgr->GetByID(ID)->GetParam("color"));
		QLine line = QtBind::toQLine(alrMgr->GetByID(ID));
		if(line.isNull()) return;
		QVariant alarm = QVariant(line);
		edit_label = new EditQLabel(ui.alarm_tab, image, alarm, color);
		edit_label->show();
	}
	if (alrMgr->GetByID(ID)->GetType() == X7S_VS_ALR_TYPE_POLY){
		QColor color = QtBind::toQColor(alrMgr->GetByID(ID)->GetParam("color"));
		QPolygon poly = QtBind::toQPolygon(alrMgr->GetByID(ID));
		if(poly.isEmpty()) return;
		QVariant alarm = QVariant(poly);
		//alarm.= QVariant(line);
		edit_label = new EditQLabel(ui.alarm_tab, image, alarm, color);
		edit_label->show();
	}
	ui.applyEditAlarm_pushButton->setEnabled(true);
	ui.cancelEditAlarm_pushButton->setEnabled(true);
}

void setCamerasParams::ApplyEditAlarm()
{
	int index = ui.alarms_comboBox->currentIndex();
	ui.applyEditAlarm_pushButton->setEnabled(false);
	ui.cancelEditAlarm_pushButton->setEnabled(false);
	if (index == 0)
		return;
	bool ok=false;
	ui.alarms_comboBox->setEnabled(true);
	int ID = ui.alarms_comboBox->itemData(index).toInt(&ok);
	if(ok==false) return;
	if (alrMgr->GetByID(ID)->GetType() == X7S_VS_ALR_TYPE_LINE){
		QLine line = QLine(edit_label->GetLineAlarm());
		alrMgr->GetByID(ID)->SetParam("p0","%d %d",line.p1().x(),line.p1().y());
		alrMgr->GetByID(ID)->SetParam("p1","%d %d",line.p2().x(),line.p2().y());
		edit_label->hide();
		ChangeImageLabel();
		alrMgr->GetByID(ID)->Reload();
	}
	if (alrMgr->GetByID(ID)->GetType() == X7S_VS_ALR_TYPE_POLY){
		QPolygon poly = QPolygon(edit_label->GetPolygonAlarm());
		X7sParam *pParamX=(X7sParam *)alrMgr->GetByID(ID)->GetParam("ptsX");
		X7sParam *pParamY=(X7sParam *)alrMgr->GetByID(ID)->GetParam("ptsY");
		QString string_x = QString();
		QString string_y = QString();
		QTextStream stream_x(&string_x, QIODevice::ReadOnly);
		QTextStream stream_y(&string_y ,QIODevice::ReadOnly);
		for(int i=0;i<poly.size();i++)
		{
			stream_x << poly.point(i).rx() << " ";
			stream_y << poly.point(i).ry() << " ";
		}
		avoDebug("setCamerasParams::ApplyEditAlarm()") << "Cadena X:" << string_x << "Cadena Y:" << string_y;
		pParamX->SetValue(string_x.toStdString());
		pParamY->SetValue(string_y.toStdString());
		edit_label->hide();
		ChangeImageLabel();
		alrMgr->GetByID(ID)->Reload();
	}
	edit_label->close();
	delete edit_label;
	//ui.delAlarm_pushButton->setEnabled(true);
	//ui.changeColor_pushButton->setEnabled(true);
	//ui.editAlarm_pushButton->setEnabled(true);
}

void setCamerasParams::ApplyNewValues()
{
	ApplyNewBG_Values();
	ApplyCameraName();
	camera->SetParam("mjpeg_write", "%d",ui.MJPEGWrite_checkBox->isChecked());
	camera->SetParam("mjpeg_restart", "%d",ui.MJPEGRestart_spinBox->value());
	camera->GetVidSurvPipeline()->SetParam("enable", "%d",ui.enable_Surveillance_checkBox->isChecked());
	camera->SetParam("mjpeg_rootdir", "%s", videoPath.toStdString().c_str());
	ApplyTracking();
}

void setCamerasParams::ApplyNewValuesAndClose()
{
	this->ApplyNewValues();
	done(0);
}

void setCamerasParams::ShowPushButtons()
{
	ui.apply_pushButton->show();
	ui.cancel_pushButton->show();
	ui.OK_pushButton->show();
}
void setCamerasParams::BrowseVideoPath()
{
	QFileDialog dialog(this);
	dialog.setFileMode(QFileDialog::Directory);
	dialog.setOptions(QFileDialog::ShowDirsOnly);
	QStringList fileNames;
	if (dialog.exec())
		fileNames = dialog.selectedFiles();
	else
		return;
	videoPath = fileNames.at(0);
	ui.videoPath_lineEdit->setText(videoPath);
}


void setCamerasParams::AddTrackingType()
{
	X7S_FUNCNAME("setCamerasParams::AddTrackingType()");

	QComboBox* cb = ui.trackTypeComboBox;
	AVO_CHECK_WARN(funcName,cb,,"cb is NULL");

	QString desc;
	cb->clear();
	connect(cb,SIGNAL(currentIndexChanged(int)),this,SLOT(setTrackingDesc(int)));
	cb->addItem(tr("Standard FG/BG (Fast)"),QVariant((int)X7S_VS_PIPELINE_FG));
	desc = tr("Fast but doesn't work properly when camera is less than 5m");
	cb->setItemData(cb->count()-1,QVariant(desc),Qt::WhatsThisRole);
	cb->setItemData(cb->count()-1,QVariant(desc),Qt::ToolTipRole);
	cb->addItem(tr("Features + KLT (Slow)"),QVariant((int)X7S_VS_PIPELINE_FEATURES));
	desc = tr("Heavy computation but works well between 1-5m");
	cb->setItemData(cb->count()-1,QVariant(desc),Qt::WhatsThisRole);
	cb->setItemData(cb->count()-1,QVariant(desc),Qt::ToolTipRole);

	if(camera && camera->GetVidSurvPipeline())
	{
		int type = camera->GetVidSurvPipeline()->GetType();
		int index=cb->findData(QVariant(type));
		if(index>=0) cb->setCurrentIndex(index);
		setTrackingDesc(index);
		avoDebug(funcName) << "index" << index << "type" << type;
	}

}


void setCamerasParams::setTrackingDesc(int index)
{
	QVariant desc = ui.trackTypeComboBox->itemData(index,Qt::WhatsThisRole);
	if(desc.isValid())
	{
		ui.trackTypeDesc->setText(desc.toString());
		ui.trackTypeDesc->setVisible(true);
	}
	else ui.trackTypeDesc->setVisible(false);

}

void setCamerasParams::ApplyTracking()
{
	QComboBox* cb = ui.trackTypeComboBox;
	if(cb && camera)
	{
		bool ok=false;
		int index=cb->currentIndex();
		QString name = cb->itemText(index);
		QVariant data = cb->itemData(index,Qt::UserRole);
		QVariant whatthis = cb->itemData(index,Qt::WhatsThisRole);
		int type= data.toInt(&ok);

		avoDebug("setCamerasParams::ApplyTracking()") << index << name << type << ok << whatthis;
		if(ok) camera->ChangeVidSurvType(type);
	}
}


void setCamerasParams::VideoPathIsGeneral(bool general)
{
	ui.videoPathBrowse_pushButton->setEnabled(general==false);
	ui.videoPath_lineEdit->setEnabled(general==false);
}

void setCamerasParams::WriteJPEGIsGeneral(bool general)
{
	ui.MJPEGWrite_checkBox->setEnabled(general==false);
}
