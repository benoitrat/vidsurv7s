#include "BoardThread.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include <highgui.h>
#include "Camera.hpp"
#include "Log.hpp"




//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructorç
//----------------------------------------------------------------------------------------
BoardThread::BoardThread(QObject *parent, Board *board_ptr, Thread_Play *play_thread_ptr)
:QThread(parent)
 {
	//v_command = QVector();
	stop = false;
	boardConnected = false;
	thread_run = false;
	board = board_ptr;
	play_thread = play_thread_ptr;

	connect(play_thread, SIGNAL(TimeoutErrorSignal(const QString &)), this, SLOT(PlayTimeoutError()));
	connect(play_thread, SIGNAL(recordingLEDToggled()), this, SLOT(ToggleRecordingLED()));

	start();
	//sleep(2);
	//    ExecuteCommand(AVO_BCOMMAND_CONNECT);
 }


BoardThread::~BoardThread()
{
	if(thread_run){
		//Cannot loop inside the thread anymore
		stop = true;
		//Clear the previous command.
		v_command.clear();

		AVO_PRINT_DEBUG("BoardThread::~BoardThread()","Starting... ID=%d",board->GetSpecialID());
		ExecuteCommand(AVO_BCOMMAND_CLOSE);
		AVO_PRINT_DEBUG("BoardThread::~BoardThread()","...waiting... ID=%d",board->GetSpecialID());
		wait();
	}
	AVO_PRINT_DEBUG("BoardThread::~BoardThread()","Deleted! ID=%d",board->GetSpecialID());
}
void BoardThread::PlayTimeoutError()
{
	AVO_PRINT_DEBUG("BoardThread::PlayTimeoutError()");
	if(ExecuteCommand(AVO_BCOMMAND_RECONNECT)==false)
		 avoDebug("BoardThread::PlayTimeoutError()") << "Executing reconnect not append";
}
void BoardThread::SetBoard(Board *brd)
{
	board = brd;
	ExecuteCommand(AVO_BCOMMAND_CONNECT);
}

void BoardThread::ToggleRecordingLED()
{
	ExecuteCommand(AVO_BCOMMAND_TOGGLELED);
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

bool BoardThread::ExecuteCommand(BoardCommand commnd)
{
	X7S_FUNCNAME("BoardThread::ExecuteCommand()");

	avoDebug(funcName) << BoardCmdDesc(commnd) << "(" << commnd << ")";
	int timeout = 50;
	while (timeout){
		if (mutex.tryLock()){
			if (thread_run == false){
				mutex.unlock();
				return false;
			}
			//			AVO_PRINT_DUMP(funcName,"mutex.trylock OK");
			v_command.append(commnd);
			commandIsArrived.wakeAll();
			mutex.unlock();
			//		    AVO_PRINT_DUMP(funcName,"mutex.unlock");
			return true;
		}
		AVO_PRINT_DEBUG(funcName,"ID=%d, mutex.tryLock fail", board->GetSpecialID());
		timeout--;
		msleep(100);
	}
	avoDebug(funcName) << board->GetSpecialID() << "Unlock failed: Could not execute command" <<
			BoardCmdDesc(commnd) << "(" << commnd << ")";

	return false;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------
/**
* @brief
*/
void BoardThread::run()
{
	X7S_FUNCNAME("BoardThread::run()");

	int ID=board->GetSpecialID();

	BoardCommand command;
	thread_run = true;
	int timeout = 10;
	bool ret;
	while(stop == false){
		mutex.lock();
		if(v_command.size() == 0){
			AVO_PRINT_DEBUG(funcName,"mutex.lock");
			while(commandIsArrived.wait(&mutex, 10000) == false){
				if(boardConnected){
					//AVO_PRINT_DEBUG(funcName,"board->IsAlive(-1)");
					mutex.unlock();
					if(board->IsAlive(-3)==false) {
						emit WarningOnBoardThreadSignal("Board " + QString::number(ID) + " is not alive");
					}
					mutex.lock();
					if(v_command.size())
						break;
				}
			}
		}
		command = v_command.first();
		v_command.pop_front();
		mutex.unlock();

		//		AVO_PRINT_INFO(funcName,"ID=%d, commandArrived: %s (#%d)",ID, BoardCommandName[(int)command],(int)command);
		if (board->IsEnable()==false || command == AVO_BCOMMAND_IDDLE) {
			snap(100);
			continue;
		}
		if ((boardConnected == false && command != AVO_BCOMMAND_CLOSE)||(command == AVO_BCOMMAND_CONNECT)){
			while(stop == false){
				timeout = 3;
				if(board->Connect() && board->Setup()){
					AVO_PRINT_DEBUG(funcName,"ID=%d, Board Setup OK",ID);
					AVO_PRINT_DEBUG(funcName,"ID=%d, PLAY",ID);
					boardConnected = true;
					break;
				}else
					AVO_PRINT_DEBUG(funcName,"ID=%d, Board Connect fail",ID);
				if(timeout == 0){
					emit WarningOnBoardThreadSignal("Board " + QString::number(ID) + " connecting error");
					timeout = 3;
				}
				snap(1);
				timeout--;
			}
			if(stop)
				break;
		}
		switch(command){
		case AVO_BCOMMAND_PING:
			board->IsAlive(5);
			break;
		case AVO_BCOMMAND_SETUP:
			board->Setup();
			break;
		case AVO_BCOMMAND_UPDATE:
			board->Update();
			break;
		case AVO_BCOMMAND_PLAY:
			if (board->Setup()&&play_thread->ExecutePlay()){
				AVO_PRINT_DEBUG(funcName,"ID=%d, Board Setup OK",ID);
				AVO_PRINT_DEBUG(funcName,"ID=%d, PLAY",ID);
				break;
			}else
				AVO_PRINT_DEBUG(funcName,"ID=%d, Board Setup or Start fail",ID);
			//break; Eliminamos el break para que se ejecute el Reconnect
		case AVO_BCOMMAND_RECONNECT:
			timeout = 0;
			while(stop == false){
				if(board->IsAlive(-1) && board->Connect() && board->Setup()){
					AVO_PRINT_DEBUG(funcName,"ID=%d, Board Setup OK",ID);
					if(play_thread->ExecutePlay()== false){
						AVO_PRINT_DEBUG(funcName,"ID=%d, Board Start fail",ID);
						continue;
					}
					AVO_PRINT_DEBUG(funcName,"ID=%d, PLAY",ID);
					boardConnected = true;
					emit WarningOnBoardThreadSignal("Board " + QString::number(ID) + " is connected again");
					break;
				}else{
					AVO_PRINT_DEBUG(funcName,"ID=%d, Board Setup fail",ID);
				}
				if(timeout == 0){
					emit WarningOnBoardThreadSignal("Board " + QString::number(ID) + " connecting error. Trying again...");
					timeout = 50;
				}
				snap(3);
				timeout--;
			}
			break;
		case AVO_BCOMMAND_STOP:
			play_thread->StopPlay();
			board->Stop();
			AVO_PRINT_DEBUG(funcName,"ID=%d, STOP",ID);
			break;
		case AVO_BCOMMAND_TOGGLELED:
			ret=board->ToggleLED();
			AVO_PRINT_DEBUG(funcName,"ID=%d, TOGGLE LED (OK=%d)",ID,ret);
			break;
		case AVO_BCOMMAND_CLOSE:
			//	        		AVO_PRINT_DEBUG(funcName,"ID=%d, CLOSING...",ID);

			if(boardConnected){
				//play_thread->StopPlay();
				//msleep(10);
				board->Close();
			}
			//mutex.unlock();
			boardConnected = false;
			//	        		AVO_PRINT_DEBUG(funcName,"ID=%d, CLOSED",ID);
			//	        		thread_run = false;
			//	        		return;
			break;
		default:
			qDebug() << "Executing unknown command";
			break;
		}
		//mutex.unlock();
		// AVO_PRINT_DEBUG(funcName,"mutex.unlock");
	}
	thread_run = false;
}


void BoardThread::snap(int seconds)
{
	double elapsed=0;
	double snapTime=seconds;
	while(stop == false)
	{
		msleep(10);
		elapsed+=0.010;
		if(elapsed>=snapTime) return;
	}
}


QString BoardThread::BoardCmdDesc(BoardCommand cmd)
{
	QString ret;
	switch(cmd)
	{
	case AVO_BCOMMAND_IDDLE: ret="IDDLE"; break;
	case AVO_BCOMMAND_CONNECT: ret="CONNECT"; break;
	case AVO_BCOMMAND_RECONNECT: ret="RECONNECT"; break;
	case AVO_BCOMMAND_PING: ret="PING"; break;
	case AVO_BCOMMAND_SETUP: ret="SETUP"; break;
	case AVO_BCOMMAND_UPDATE: ret="UPDATE"; break;
	case AVO_BCOMMAND_PLAY: ret="PLAY"; break;
	case AVO_BCOMMAND_POOL: ret="POOL"; break;
	case AVO_BCOMMAND_STOP: ret="STOP"; break;
	case AVO_BCOMMAND_TOGGLELED: ret="TOGGLELED"; break;
	case AVO_BCOMMAND_CLOSE: ret="CLOSE"; break;
	default: ret="UNKOWN";
	}
	return ret;
}
