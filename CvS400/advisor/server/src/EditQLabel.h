/*
 * EditQLabel.h
 *
 *  Created on: 14/09/2009
 *      Author: Gabri
 */

#ifndef EDITQLABEL_H_
#define EDITQLABEL_H_



#include <QLabel>
#include <QVariant>
#include <QPixmap>
#include <QMenu>
#include <QImage>
#include <QAction>
#include <QMouseEvent>
#include <QByteArray>

class EditQLabel : public QLabel
{
    Q_OBJECT

public:
    EditQLabel(QWidget *parent = 0, const QImage &image_b = QImage(), const QVariant &alarm = QVariant(),
				const QVariant &color = QColor(Qt::red));
    ~EditQLabel();

    QLine GetLineAlarm(){return line;};
    QPolygon GetPolygonAlarm(){return polygon;};

    QImage 	image_v;


protected:
    virtual void paintEvent(QPaintEvent *event);
//    void mousePressEvent(QMouseEvent *event);
    void mouseDoubleClickEvent (QMouseEvent * event );
    void mouseMoveEvent (QMouseEvent * event );
/*
private slots:
	void addPolygon();
	void addLine();
	void apply();
	void cancel();
//	void delLastPolygon();
//	void delAllPolygons();
//	void delLastLine();
//	void delAllLines();
//	void createActions();
//	void createMenus();
//	void addAlarm();
//	void exitAddAlarm();
*/
private:

    QPoint last_line_point[2];
    QPolygon polygon;
    QLine line;
    QColor alarm_color;
    unsigned int polygon_index;
    unsigned int last_polygon_index;
    unsigned int polygon_size;
    unsigned int line_index;
    unsigned int last_line_index;
    unsigned int point_edit_index;
/*
    QMenu *menu;
    QAction *addLineAction;
    QAction *delLinesAction;
    QAction *delLastLineAction;
    QAction *addPolygonAction;
    QAction *delPolygonsAction;
    QAction *delLastPolygonAction;
    QAction *applyAction;
    QAction *cancelAction;
    QAction *exitAction;

    QMenu	*menu_enable;
    QAction *enableCameraAction;
    QAction *enableSurveillanceAction;
    QAction *addAlarmAction;
    QAction *setBackGroundAction;
*/
    bool	addpolygon;
    bool 	addline;
    bool 	drawpolygon;
    bool 	addingAlarm;
    bool 	point_selected;

};

#endif /* EDITQLABEL_H_ */
