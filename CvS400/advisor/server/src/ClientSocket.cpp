#include "ClientSocket.h"
#include <QtNetwork>

#include "QCameraServer.h"
#include "Camera.hpp"
#include "Log.hpp"

#include "vv_defines.h"
   
ClientSocket::ClientSocket(QObject *parent, QCameraServer *camera_ptr, QList<QString> *userList_pt)
    : QTcpSocket(parent)
{
	QObject::connect(this, SIGNAL(readyRead()), this, SLOT(readClient()));
	QObject::connect(this, SIGNAL(disconnected()), this, SLOT(deleteLater()));
	//QObject::connect(this, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error(QAbstractSocket::SocketError)));
	QObject::connect(this, SIGNAL(disconnected()), this, SLOT(error()));
	nextBlockSize = 0;
    
    qcamera = camera_ptr;
    
    imageCameraAsked = false;
    user_logged = false;
       
    userList = userList_pt;

    connect(qcamera, SIGNAL(imageToClient(const QByteArray &)), this, SLOT(imageToClient(const QByteArray &)));
    connect(qcamera, SIGNAL(newAlarmToClient(const QByteArray &)), this, SLOT(sendNewAlarmsToClient(const QByteArray &)));
    connect(qcamera, SIGNAL(cameraEnabledSurveillance(bool , int )), this, SLOT(sendEnableSurveillanceToClient(bool)));
    connect(qcamera, SIGNAL(cameraEnabled(bool , int )), this, SLOT(sendEnableCameraToClient(bool)));
    connect(qcamera, SIGNAL(sendDeleteAlarmToClient(int )), this, SLOT(sendDeleteAlarmToClient(int )));
}

ClientSocket::~ClientSocket()
{
	this->close();
	avoDebug("ClientSocket::~ClientSocket()") << qcamera->getCamera()->GetSpecialID();
}


void ClientSocket::imageToClient(const QByteArray &qmeta_jpeg)
{
	qmetaJPEG = QByteArray(qmeta_jpeg);
	if (imageCameraAsked){
		sendImageToClient();
		imageCameraAsked = false;
	}
}

void ClientSocket::sendImageToClient()
{
    QByteArray block;
    quint32 size;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('I') << quint8(AVO_SCOMMAND_SEND_IMAGE) << qmetaJPEG;

        
    out.device()->seek(0);
    size = quint32(block.size() - sizeof(quint32));
    out << size;
    write(block);
    //qDebug() << "Server: AVO_SCOMMAND_SEND_IMAGE Command sent";
}

void ClientSocket::sendEnableCameraToClient(bool enabled)
{
    QByteArray block;
    quint32 size;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('C') << quint8(AVO_SCOMMAND_ENABLE_CAM) << enabled;
    out.device()->seek(0);
    size = quint32(block.size() - sizeof(quint32));
    out << size;
    write(block);
    //qDebug() << "Server: AVO_SCOMMAND_SEND_IMAGE Command sent";
}

void ClientSocket::sendEnableSurveillanceToClient(bool enabled)
{
    QByteArray block;
    quint32 size;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('C') << quint8(AVO_SCOMMAND_ENABLE_SURVEILLANCE) << enabled;
    out.device()->seek(0);
    size = quint32(block.size() - sizeof(quint32));
    out << size;
    write(block);
    //qDebug() << "Server: AVO_SCOMMAND_SEND_IMAGE Command sent";
}

void ClientSocket::sendUserLoggedToClient(bool logged)
{
	avoDebug("ClientSocket::sendUserLoggedToClient()") <<  "begin";
	QByteArray block;
    quint32 size;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('U') << quint8(AVO_SCOMMAND_USER_LOGGED) << logged;

    out.device()->seek(0);
    size = quint32(block.size() - sizeof(quint32));
    out << size;
    write(block);

    avoDebug("ClientSocket::sendUserLoggedToClient()") <<  "end: " << logged;
}

void ClientSocket::SendPermissionDenied()
{
	QByteArray block;
    quint32 size;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('U') << quint8(	AVO_SCOMMAND_PERMISSION_DENIED);

    out.device()->seek(0);
    size = quint32(block.size() - sizeof(quint32));
    out << size;
    write(block);
}
void ClientSocket::sendNewAlarmsToClient(const QByteArray &alarm_array)
{
	QByteArray block;
	QByteArray array = QByteArray(alarm_array);
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);

    out << quint32(0) << quint8('C') << quint8(AVO_SCOMMAND_SEND_NEW_ALARMS) << array;

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);
}
void ClientSocket::sendDeleteAlarmToClient(int ID)
{
	QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);

    out << quint32(0) << quint8('C') << quint8(AVO_SCOMMAND_SEND_DELETE_ALARM) << quint8(ID);

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);

}
void ClientSocket::readClient()
{
    QDataStream in(this);
    in.setVersion(QDataStream::Qt_4_3);
    quint32 bytesAvailables;
    QByteArray alarm_array;
    QByteArray string_array;
	QString param;
	QString value;
	forever{
		//qDebug() << "Camera " << qcamera->getCameraIndex()<< " server, readClient()";
	   if (nextBlockSize == 0) {
	        if (this->bytesAvailable() < sizeof(quint32))
	            break;
	        in >> nextBlockSize;
	   }   
	    //in >> nextBlockSize;
	    if (this->bytesAvailable() < nextBlockSize)
	        break;

	    bytesAvailables = this->bytesAvailable();
	    
	    quint8 command_type;
	    quint8 command;
	    
	    nextBlockSize = 0;
	    in >> command_type >> command;
	    
	    if (command == AVO_SCOMMAND_USER_LOG){
	    	//user_logged = false;
	    	in >> user_name >> user_password;// >> user_level;
	    	avoInfo("ClientSocket::readClient()") << "Camera: " << qcamera->getCamera()->GetSpecialID()<< "Client User: " << user_name << " Password: " << user_password;
	    	if (userList->size() > 2)
				for(int i = 0; i < userList->size()- 2; i++)
				{
					if (user_name == userList->at(i)){
						QCryptographicHash *hash = new QCryptographicHash(QCryptographicHash::Sha1);
						QByteArray data = QByteArray();
						data.append(user_name);
						data.append(user_password);
						data.append(userList->at(i+2));
						hash->addData(data);
						QString hash_result = QString(hash->result().toHex());
						avoInfo("ClientSocket::readClient()") << "Hash-1 result: " << hash_result;

						if (hash_result == userList->at(i+1)){
							user_logged = true;
							user_level = userList->at(i+2).toInt();
							emit newUserLoggedSignal(user_name);
							break;
						}
					}
				}
	    	avoInfo("ClientSocket::readClient()") << "Logged: " << user_logged;
	    	sendUserLoggedToClient(user_logged);
	    	break;
	    }

	    if (user_logged == true)
			switch (command_type){
				case('U'):
					if(command ==AVO_SCOMMAND_USER_NO_ADMIN)
						user_level = 2;//Visor only
					break;
				case('C'):
					if(command ==AVO_SCOMMAND_SEND_IMAGE){
						imageCameraAsked = true;
						//qDebug() << "Server: AVO_SCOMMAND_SEND_IMAGE Command received, camera: " << qcamera->getCameraIndex();
						break;
					}
					if (user_level == 0)//commands for admin users
						switch(command){
							case(AVO_SCOMMAND_ENABLE_CAM):
								qcamera->enableQCamera(true);
								break;
							case(AVO_SCOMMAND_DISABLE_CAM):
								qcamera->enableQCamera(false);
								break;
							case(AVO_SCOMMAND_ENABLE_SURVEILLANCE):
								qcamera->enableSurveillance(true);
								break;
							case(AVO_SCOMMAND_DISABLE_SURVEILLANCE):
								qcamera->enableSurveillance(false);
								break;
							case(AVO_SCOMMAND_SEND_NEW_ALARMS):
								//QByteArray alarm_array;
								bytesAvailables = this->bytesAvailable();
								quint32 size;// = alarm_array.size();
								in >> alarm_array;
								size = alarm_array.size();
								bytesAvailables = this->bytesAvailable();
								qcamera->addNewAlarmFromClient(alarm_array);
								break;
							case(AVO_SCOMMAND_SET_PARAM):
								//QString param;
								//QString value;
								in >> param >> value;
								string_array = QByteArray();
								string_array.append(param);
								qcamera->getCamera()->SetParam(string_array.data(), value.toStdString());
								string_array.clear();
								avoInfo("ClientSocket::readClient()") << "SetParam():" << param << " Value: " << value;
								break;
							case(AVO_SCOMMAND_SEND_DELETE_ALARM):
								quint8 ID;
								in >> ID;
								qcamera->DeleteAlarmFromClient(ID);
								break;
							default:
								avoWarning("ClientSocket::readClient()") << "Undefined command received from server";
								break;
						}
					else
						SendPermissionDenied();
					break;
				default:
					avoWarning("ClientSocket::readClient()") << "Undefined command received from server";
					break;
			}
	    else
	    	avoWarning("ClientSocket::readClient()") << "User not logged";
	}
}


void ClientSocket::error()//QAbstractSocket::SocketError error)
{
    avoError("ClientSocket::error()")<< qcamera->getCamera()->GetSpecialID()  <<
    		 this->peerAddress().toString() << this->state() <<
    		 this->errorString() << QAbstractSocket::error();
}


