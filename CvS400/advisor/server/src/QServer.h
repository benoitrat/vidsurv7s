#ifndef QSERVER_H_
#define QSERVER_H_

#include <QTcpServer>
#include <QtNetwork>
#include <QList>
#include <QString>

class QCameraServer;

class QServer : public QTcpServer
{
    Q_OBJECT

public:
    QServer(QObject *parent = 0, QCameraServer *camera_ptr = 0, QList<QString> *userList_pt = 0);
    ~QServer();
signals:
	void newUserLoggedSignal(const QString &name);
   
protected slots:
    void incomingConnection(int socketId);
    void newUserLogged(const QString &name);

private:
    QCameraServer *qcamera;
    QList<QString> *userList;
 
};

#endif /*QSERVER_H_*/
