#include "QServer.h"
#include "ClientSocket.h"
#include "QCameraServer.h"
#include <QtNetwork>
#include "Log.hpp"
#include "Camera.hpp"

QServer::QServer(QObject *parent, QCameraServer *camera_ptr, QList<QString> *userList_pt)
    :QTcpServer(parent)
{	   
    	qcamera = camera_ptr;
    	userList = userList_pt;
}

QServer::~QServer()
{

}

void QServer::incomingConnection(int socketId)
{
    ClientSocket *socket = new ClientSocket(this, qcamera, userList);
    avoDebug("QServer::incomingConnection()") << "Cam " << qcamera->getCamera()->GetSpecialID();
    socket->setSocketDescriptor(socketId);
    connect(socket, SIGNAL(newUserLoggedSignal(const QString&)), this, SLOT(newUserLogged(const QString &)));
 //   emit newConnection ();
}

void QServer::newUserLogged(const QString &name)
{
	emit newUserLoggedSignal("User name: " + name + " logged.");
}
