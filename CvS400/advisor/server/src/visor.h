#ifndef VISOR_H
#define VISOR_H


#include <QtGui/QMainWindow>
#include <QLabel>
#include <QTextEdit>
#include <QSplitter>
#include <QGroupBox>
#include <QGridLayout>
#include <QSettings>
#include <QCloseEvent>
#include <QTabWidget>

#include "ui_visor.h"
#include "Core.hpp"
#include "CamEvtCarrier.hpp"

class QBoard;
class Root;
class setVisorParams;
class QServer_Devices;
class EventListWidget;
class ViewerWidget;
class AlrTimeWidget;
class Camera;
class addNewUser;
class WarningWidget;
class usersConfigParams;
class CamEvtHandler;

class Visor : public QMainWindow
{
	Q_OBJECT

public:
	Visor(QWidget *parent = 0);
	virtual ~Visor();

	signals:
	void playingEvt(const CamEvtCarrier& camEvt, bool play);

protected:
	void closeEvent(QCloseEvent *event);
	void init();
	void release();

private:
	bool EventSelected(const AVOAlrEvt& alrEvt, bool play);

	// SLOTS

public slots:
	void saveXMLFile();
	void fullScreen(bool full=true);

private slots:
	//void saveXMLFile();
	void configureParams();
	//void ShowAlarmsWindows(bool enable);
	void HideGraphicalMode(bool enable);
	void writeSettings();
	void readSettings();
	void ResizeCameras(int pos, int index);
	void RedrawCameras();
	void ChangeAlarmsWindowOrientation(bool enable);
	void AddNewBoard();
	void DeleteBoard(int board_index);
	void CursorChanged();
	int EnableAdminMode(bool enable);
	void AddNewServerUser();
	void AddNewClientUser();
	int CheckServerUser(const QString &name, const QString &password);
	void ConfigServerUsers();
	void ConfigClientUsers();
	void saveXMLFile_tmp();
	void ReconnectAllBoards();
	void ConnectAllBoards();
	void DisconnectAllBoards();
	void AdvanceConfig();
	void PurgeDatabase();
	void importXml();
	void exportXml();
	void SetDefaultView();

	bool EventSelected(const CamEvtCarrier& camEvt, bool play);
	void receivedTick(const QDateTime& actual=QDateTime());
	void updateLogFile(const QDateTime& actual=QDateTime());
	void updatePurgeVideo(const QDateTime& actual=QDateTime());
	void updateStats(const QDateTime& actual=QDateTime());

	//----------- VARIABLE

public:
	QString xmlFile;

private:
	Ui::VisorClass ui;

	QVector<QBoard *>	qboards;
	QLabel *locationLabel;
	//QTabWidget *event_Widget_Tab;
	//QWidget *cameras_tab;
	//QWidget *events_tab;
	QSplitter *splitter;
	QSplitter *splitter_event;
	//QGroupBox *qcameras_groupBox;
	QGridLayout *qcameras_gridLayout;
	QGridLayout *events_gridLayout;

	QVector<AlrTimeWidget*> alrTime;
	//quint8 numberOfColumms;
	//quint8 numberOfArrows;
	bool adminMode;
	int numberOfServerUsers;
	int numberOfClientUsers;
	QList<QString> *serverUsers;
	QList<QString> *clientUsers;

	addNewUser *new_user;
	usersConfigParams *config_user;

	Root *root;			//!< Containing the Root of all X7sXmlObject (db,dev,...)
	CamEvtHandler *camEvtHandler;
	EventListWidget *eventList;
	ViewerWidget *videoViewer;
	WarningWidget *warningList;

	setVisorParams *visorParams;
	QServer_Devices *qserverd;
	QSettings *settings;

	QTimer *images_timeout;
	int timeout_ms;

	quint8 numberOfBoards;

};

#endif // VISOR_H
