#ifndef SETCONFIGPARAMS_H
#define SETCONFIGPARAMS_H

#include <QtGui/QWidget>
#include <QString>

class LogConfig;
class Root;
class Devices;

namespace Ui {
    class setConfigParams;
}

class setConfigParams : public QWidget {
    Q_OBJECT
    Q_DISABLE_COPY(setConfigParams)
public:
    explicit setConfigParams(QWidget *parent = 0, Root *root_p = 0);
    virtual ~setConfigParams();

signals:
	void VideoPathIsGeneralSignal(bool general);
	void WriteJPEGIsGeneralSignal(bool general);

public slots:
	void ApplyNewValues();

protected:
    virtual void changeEvent(QEvent *e);
private slots:
	void BrowseVideoPath();
    void BrowseLogPath();
    void VideoPathIsGeneral(bool general);
    void WriteJPEGIsGeneral(bool general);

private:
    Ui::setConfigParams *m_ui;
    LogConfig *config;
    Root *root;
    Devices *devices;
    QString videoPath;
    QString logPath;
};

#endif // SETCONFIGPARAMS_H
