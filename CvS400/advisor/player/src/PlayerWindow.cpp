#include "PlayerWindow.h"
#include "ui_PlayerWindow.h"
#include "AlrTimeWidget.h"
#include "ViewerWidget.h"
#include "Database.hpp"
#include "Camera.hpp"
#include "Devices.hpp"
#include "Root.hpp"
#include "Log.hpp"
#include <x7sxml.h>


PlayerWindow::PlayerWindow(QWidget *parent)
: QMainWindow(parent), ui(new Ui::PlayerWindow), root(NULL)
  {
	ui->setupUi(this);


	x7sVSLog_SetLevel(X7S_LOGLEVEL_LOW);

	root = new Root("advisord.xml");
	if(root->LoadFile()==false)
	{
		qCritical() << "Error: Bad XML format: Can not load configuration";

	}
	Database *db=root->database();
	Devices *dev=root->devices();

	if(dev->GetSize()==0)
	{
		qCritical() << "No board found in devices (loading from XML file)";
	}

	db->Connect();


	bool ok;
	QStringList items;
	Camera *cam=NULL;
	while((cam=dev->GetNextCamera(cam))!=NULL)
	{
		items << QString("%1").arg(cam->GetSpecialID());
	}
	QString item = QInputDialog::getItem(this, tr("Select the camera"),
			tr("Camera ID:"), items, 0, false, &ok);

	cam=dev->GetCameraByID(item.toInt());
	if(cam==NULL) cam=dev->GetNextCamera(NULL);

	alrsWidget = new AlrTimeWidget(this,db,cam);
	this->setMinimumSize(300,300);
	this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);


	videoViewer = new ViewerWidget(this);


	QDockWidget* dock;

	dock = new QDockWidget("History",this);
	dock->setWidget(alrsWidget);
	setCentralWidget(dock);

	dock = new QDockWidget("Viewer",this);
	dock->setWidget(videoViewer);
	addDockWidget(Qt::BottomDockWidgetArea, dock);


	connect(alrsWidget,SIGNAL(EventSelectedSignal(const AVOAlrEvt&, bool)),this,SLOT(videoEvent(const AVOAlrEvt&, bool)));
  }


PlayerWindow::~PlayerWindow()
{
	delete ui;
	delete root;
}


bool PlayerWindow::videoEvent(const AVOAlrEvt& alrEvt, bool play)
{
	Database *client_database = root->database();

	bool ret;
	X7S_FUNCNAME("PlayerWindow::videoEvent()");
	AVO_CHECK_WARN(funcName,videoViewer,false,"VideoViewer is NULL");

	QDateTime time= QDateTime::fromTime_t(alrEvt.t);
	avoDebug(funcName)<< "Start"  << time << alrEvt.cam << alrEvt.blob_ID << play;

	//First check if we have clicked on the same alarm
	if(AVO_STRUCT_EQUAL(alrEvt,videoViewer->getLastAlrEvt()))
	{
		//Do nothing
		avoDebug(funcName) << "Click on same event... do nothing";
	}
	else
	{
		//Check if the alrEvt correspond to video already opened
		if(videoViewer->getVideo().hasAlrEvent(alrEvt))
		{
			if(!videoViewer->getVideo().isOpened()) {
				ret = videoViewer->Open(videoViewer->getVideo());
				if(ret==false) return ret;
			}
		}
		//Otherwise search a video in the database.
		else
		{
			VideoInFile videofile = client_database->GetVideo(alrEvt.cam,time);
			ret = videoViewer->Open(videofile);
			if(ret==false) return ret;
		}

		//Then go to the correct frame.
		videoViewer->JumpToAlarm(alrEvt);
		videoViewer->displayFrame();
	}
	if(play) videoViewer->PlayPause(true);
	avoDebug(funcName) << "End" << time << alrEvt.cam << play;
	return true;

}
