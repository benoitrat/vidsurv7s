#ifndef PLAYERWINDOW_H
#define PLAYERWINDOW_H

#include <QtGui/QMainWindow>
#include "Core.hpp"

class AlrTimeWidget;
class ViewerWidget;
class Root;

namespace Ui
{
    class PlayerWindow;
}

/**
 * @brief Player Windows
 * @ingroup avo_player
 */
class PlayerWindow : public QMainWindow
{
    Q_OBJECT

public:
    PlayerWindow(QWidget *parent = 0);
    ~PlayerWindow();

private:
    Ui::PlayerWindow *ui;
    AlrTimeWidget *alrsWidget;
    ViewerWidget *videoViewer;
    Root *root;

private slots:
	bool videoEvent(const AVOAlrEvt& alrEvt, bool play);
};

#endif // PLAYERWINDOW_H
