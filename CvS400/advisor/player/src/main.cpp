#include <QtGui/QApplication>
#include "PlayerWindow.h"
#include "Log.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PlayerWindow w;

    Log::SetLevel(X7S_LOGLEVEL_HIGHEST);

    w.show();
    return a.exec();
}
