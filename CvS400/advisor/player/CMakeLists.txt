project("AVOPlayer")
SET(EXECNAME "AVOPlayer")


MESSAGE(STATUS  "===============================================================================")
MESSAGE(STATUS  "Configure ${EXECNAME}:")
MESSAGE(STATUS  "-------------------------------------------------------------------------------")

## Find QT4 librarie and set important variables.
FIND_PACKAGE( Qt4 REQUIRED )

## Add the hdr directory to thej project
SET(HDR_DIRECTORY ${AVO_WIDGETS_INCLUDE_DIR} ${AVO_CORE_INCLUDE_DIR} ${CMAKE_CURRENT_SOURCE_DIR} )  

## Add the list of all .cpp files to the project
SET(CPP_SRCS  
	./src/main.cpp
	./src/PlayerWindow.cpp
)

## Add the list of all .ui files
SET(UI_FILES
	./src/PlayerWindow.ui
)

## Add the list of header files that should be treated with moc
## (all headers that have Q_OBJECT directive should be treated as MOC_HDRS)
SET(MOC_HDRS
	./src/PlayerWindow.h
)

## And finally Add the list of  resource file
SET(RC_FILES
#   ./src/rc/sample.qrc
)

## Add some definition to the program
#ADD_DEFINITIONS( -Wall )


## Enable the QT module (Don't forget to include ${QT_USE_FILE})
SET( QT_USE_QTCORE TRUE )
SET( QT_USE_QTGUI TRUE )
SET( QT_USE_QTSQL TRUE )
INCLUDE( ${QT_USE_FILE} )

# Add the PSQL library to the compilation
FIND_LIBRARY(QT_PSQL_LIBRARY_RELEASE NAMES qsqlpsql4 libqsqlpsql4 PATHS ${QT_PLUGINS_DIR}/sqldrivers)
FIND_LIBRARY(QT_PSQL_LIBRARY_DEBUG NAMES qsqlpsqld4 libqsqlpsqld4 PATHS ${QT_PLUGINS_DIR}/sqldrivers)
SET(QT_PSQL_LIBRARY optimized;${QT_PSQL_LIBRARY_RELEASE};debug;${QT_PSQL_LIBRARY_RELEASE})
IF(QT_PSQL_LIBRARY) 
SET(QT_LIBRARIES ${QT_LIBRARIES} ${QT_PSQL_LIBRARY}) #Add PSQL to QT libraries
ENDIF()

# Add the MySQL library to the compilation
FIND_LIBRARY(QT_MYSQL_LIBRARY_RELEASE NAMES qsqlmysql4 libqsqlmysql4 PATHS ${QT_PLUGINS_DIR}/sqldrivers)
FIND_LIBRARY(QT_MYSQL_LIBRARY_DEBUG NAMES qsqlmysqld4 libqsqlmysqld4 PATHS ${QT_PLUGINS_DIR}/sqldrivers)
SET(QT_PSQL_LIBRARY optimized;${QT_MYSQL_LIBRARY_RELEASE};debug;${QT_MYSQL_LIBRARY_RELEASE})
IF(QT_PSQL_LIBRARY) 
SET(QT_LIBRARIES ${QT_LIBRARIES} ${QT_PSQL_LIBRARY}) #Add PSQL to QT libraries
ENDIF()


## Generate resource
QT4_ADD_RESOURCES(RC_SRCS ${RC_FILES} )

## Generate moc source:
QT4_WRAP_CPP(MOC_SRCS ${MOC_HDRS} )


## Run uic on .ui files and create ui header in binary directory:
QT4_WRAP_UI(UI_HDRS ${UI_FILES} )
INCLUDE_DIRECTORIES( ${CMAKE_CURRENT_BINARY_DIR} )


add_executable(${EXECNAME} ${CPP_SRCS} ${MOC_SRCS} ${RC_SRCS} ${UI_HDRS} ${AVO_WIDGETS_RESOURCES})
include_directories(${HDR_DIRECTORY} ${X7S_INCLUDES} ${3RDPARTY_INCLUDES})
set_target_properties(${EXECNAME} PROPERTIES PROJECT_LABEL "${EXECNAME}") #For MSVC
add_dependencies(${EXECNAME} ${AVO_WIDGETS_LIBRARY} ${AVO_CORE_LIBRARY} ${X7S_LIBRARIES})
target_link_libraries(${EXECNAME} ${AVO_WIDGETS_LIBRARY} ${AVO_CORE_LIBRARY} ${X7S_LIBRARIES} ${OpenCV_LIBS} ${QT_LIBRARIES})


MESSAGE(STATUS  "SRC_FILES=${CPP_SRCS} | ${MOC_SRCS} | ${RC_SRCS} | ${UI_HDRS}")
MESSAGE(STATUS  "HDR_DIREC=${HDR_DIRECTORY} ${X7S_INCLUDES} ${OpenCV_INCLUDE_DIRS} ${3RDPARTY_INCLUDES}")
MESSAGE(STATUS  "LIB_FILES=${X7S_LIBRARIES} ${OpenCV_LIBRARIES}")

MESSAGE(STATUS  "-------------------------------------------------------------------------------")
MESSAGE(STATUS  "")