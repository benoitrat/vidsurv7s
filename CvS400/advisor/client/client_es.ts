<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>AvoSysTrayIcon</name>
    <message>
        <location filename="src/AvoSysTrayIcon.cpp" line="67"/>
        <source>Mi&amp;nimize</source>
        <translation>Mi&amp;nimizar</translation>
    </message>
    <message>
        <location filename="src/AvoSysTrayIcon.cpp" line="70"/>
        <source>Ma&amp;ximize</source>
        <translation>Ma&amp;ximizar</translation>
    </message>
    <message>
        <location filename="src/AvoSysTrayIcon.cpp" line="73"/>
        <source>&amp;Restore</source>
        <translation>&amp;Restablecer</translation>
    </message>
    <message>
        <location filename="src/AvoSysTrayIcon.cpp" line="76"/>
        <source>&amp;Quit</source>
        <translation>&amp;Salir</translation>
    </message>
</context>
<context>
    <name>CameraClient_Socket</name>
    <message>
        <location filename="src/CameraClient_Socket.cpp" line="285"/>
        <source>Command Error</source>
        <translation>Error de comando</translation>
    </message>
    <message>
        <location filename="src/CameraClient_Socket.cpp" line="285"/>
        <source>You don&apos;t have admin privileges. Contact to your security manager.</source>
        <translation>Usted no tiene derechos de administrador. Contacte con su Administrador de Seguridad.</translation>
    </message>
    <message>
        <location filename="src/CameraClient_Socket.cpp" line="363"/>
        <location filename="src/CameraClient_Socket.cpp" line="380"/>
        <source>Camera </source>
        <translation>Cámara</translation>
    </message>
    <message>
        <location filename="src/CameraClient_Socket.cpp" line="363"/>
        <source> error. Connecting to server.</source>
        <translation>error. Conectando con el Servidor.</translation>
    </message>
    <message>
        <location filename="src/CameraClient_Socket.cpp" line="380"/>
        <source> Connected again to server.</source>
        <translation>Conectado otra vez con el Servidor.</translation>
    </message>
</context>
<context>
    <name>ClientVisorClass</name>
    <message>
        <location filename="src/client_visor.ui" line="26"/>
        <source>Visor Client by Seven Solutions</source>
        <translation>Cliente Visor de Seven Solutions</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="53"/>
        <source>&amp;Cameras</source>
        <oldsource>Cameras</oldsource>
        <translation>&amp;Camaras</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="58"/>
        <source>&amp;Alarms and Events</source>
        <oldsource>Alarms and Events</oldsource>
        <translation>&amp;Alarmas y eventos</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="74"/>
        <source>&amp;File</source>
        <oldsource>File</oldsource>
        <translation>&amp;Fichero</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="81"/>
        <source>User &amp;Modes</source>
        <oldsource>User Modes</oldsource>
        <translation>&amp;Modos de operación</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="89"/>
        <source>&amp;Preference</source>
        <oldsource>Preference</oldsource>
        <translation>Confi&amp;guración</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="98"/>
        <source>&amp;Help</source>
        <oldsource>Help</oldsource>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="104"/>
        <source>&amp;View</source>
        <oldsource>View</oldsource>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <source>Alarms Windows</source>
        <translation type="obsolete">Ventana de Alarmas</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="121"/>
        <source>Enter &amp;Admin Mode</source>
        <oldsource>Enter Admin Mode</oldsource>
        <translation>Entrar modo &amp;Administrador</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="132"/>
        <location filename="src/client_visor.ui" line="203"/>
        <source>Show</source>
        <translation>Mostrar</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="143"/>
        <location filename="src/client_visor.ui" line="211"/>
        <source>Vertical</source>
        <translation>Vertical</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="148"/>
        <source>Config Client Parameters</source>
        <translation>Configurar Parámetros Cliente</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="156"/>
        <source>Config Server Parameters</source>
        <translation>Configurar Parámetros Servidor</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="167"/>
        <source>Enable Privacity Mode</source>
        <translation>Habilitar Modo Privacidad</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="172"/>
        <source>Server &amp;Save XML file</source>
        <oldsource>Server Save XML file</oldsource>
        <translation>&amp;Guardar Fichero XML Servidor</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="177"/>
        <source>Select Privacity Mode</source>
        <translation>Seleccionar Modo Privacidad</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="182"/>
        <source>Advance Config</source>
        <translation>Configuración Avanzada</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="187"/>
        <source>Reconnect to all boards</source>
        <translation>Reconectar todas las tarjetas</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="195"/>
        <source>&amp;Full Screen</source>
        <oldsource>Full Screen</oldsource>
        <translation>Pantalla &amp;Completa</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="198"/>
        <source>Full Screen</source>
        <comment>Qt::Key_F5</comment>
        <extracomment>F5</extracomment>
        <translation>Pantalla Completa</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="216"/>
        <source>About us...</source>
        <translation>Sobre nosotros...</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="221"/>
        <source>&amp;Default view</source>
        <oldsource>Default view</oldsource>
        <translation>Vista por &amp;Defecto</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="229"/>
        <source>Enter &amp;Privacity Mode</source>
        <oldsource>Enter Privacity Mode</oldsource>
        <translation>Entrar modo &amp;Privacidad</translation>
    </message>
    <message>
        <location filename="src/client_visor.ui" line="237"/>
        <source>Enter &amp;Visor Mode</source>
        <oldsource>Enter Visor Mode</oldsource>
        <translation>Entrar modo &amp;Visor</translation>
    </message>
</context>
<context>
    <name>Client_Socket</name>
    <message>
        <location filename="src/Client_Socket.cpp" line="291"/>
        <location filename="src/Client_Socket.cpp" line="292"/>
        <source>Error. Can&apos;t write XML file.</source>
        <translation>Error. No se puede guardar el fichero XML.</translation>
    </message>
    <message>
        <location filename="src/Client_Socket.cpp" line="366"/>
        <source>Error. Connecting to server.</source>
        <translation>Error Conexión con el Servidor.</translation>
    </message>
    <message>
        <location filename="src/Client_Socket.cpp" line="379"/>
        <source>Connected again to server.</source>
        <translation>Conectado otra vez al Servidor.</translation>
    </message>
</context>
<context>
    <name>Client_Visor</name>
    <message>
        <location filename="src/client_visor.cpp" line="84"/>
        <location filename="src/client_visor.cpp" line="127"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="85"/>
        <source>Error. Connecting to server timeout.</source>
        <translation>Error. Timeout al conectar con el Server.</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="86"/>
        <location filename="src/client_visor.cpp" line="129"/>
        <source>&amp;Try again</source>
        <translation>&amp;Reintentar</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="86"/>
        <source>&amp;Config parameters...</source>
        <translation>&amp;Configurar parámetros...</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="86"/>
        <location filename="src/client_visor.cpp" line="129"/>
        <source>&amp;Close application</source>
        <translation>&amp;Cerrar Aplicación</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="128"/>
        <source>Error. Logging not authorized by the server.</source>
        <translation>Error. Logging no autorizado por el Servidor.</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="161"/>
        <source>Access Question</source>
        <translation>Acceso de Administración</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="145"/>
        <source>You have admin privileges, do you want to activate Admin Mode?</source>
        <translation>Usted tiene privilegios de administrador ¿desea activar el Modo Administrador?</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="145"/>
        <source>Admin Access</source>
        <translation>Acceso de Administrador</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="161"/>
        <source>You have privacity privileges, do you want to activate Privacity Mode?</source>
        <translation>Usted tiene privilegios de privacidad ¿desea activar el Modo Privacidad?</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="220"/>
        <source>Data Base Error</source>
        <translation>Error Base de Datos</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="221"/>
        <source> Application will not save video. </source>
        <translation>La aplicación no almacenará videos.</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="222"/>
        <source>  &amp;Ignore error and continue  </source>
        <translation>&amp;Ignorar error y continuar</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="222"/>
        <source>  &amp;Close application  </source>
        <translation>&amp;Cerrar aplicación</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="231"/>
        <source>Select a video by clicking on the &quot;Online Events Panel&quot; or by looking offline with the &quot;Alarms and Events&quot; panel</source>
        <translation>Seleccione un vídeo pulsando sobre el &quot;Panel de Eventos Online&quot; o, de forma offline, con el panel &quot;Alarmas y Eventos&quot;</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="335"/>
        <source>Confirm Exit</source>
        <translation>Confirmar salida</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="336"/>
        <source>Do you really want to close application ?</source>
        <translation>Estas seguro de cerrar el programa ?</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="799"/>
        <source>User Error</source>
        <translation>Error de usuario</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="800"/>
        <source>User or Password not correct</source>
        <translation>Nombre de usuario o Contraseña incorrecto</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="807"/>
        <source>Command Error</source>
        <translation>Error de comando</translation>
    </message>
    <message>
        <location filename="src/client_visor.cpp" line="807"/>
        <source>You don&apos;t have correct privileges. Contact to your security manager.</source>
        <translation>Usted no tiene los derechos necesarios. Contacte con su Administrador de Seguridad.</translation>
    </message>
    <message>
        <source>You don&apos;t have admin privileges. Contact to your security manager.</source>
        <translation type="obsolete">Usted no tiene derechos de administrador. Contacte con su Administrador de Seguridad.</translation>
    </message>
    <message>
        <source>You don&apos;t have privacity privileges. Contact to your security manager.</source>
        <translation type="obsolete">Usted no tiene derechos sobre privacidad. Contacte con su Administrador de Seguridad.</translation>
    </message>
</context>
<context>
    <name>Connect_Dialog_Class</name>
    <message>
        <location filename="src/connect_dialog.ui" line="20"/>
        <source>Network Configuration</source>
        <translation>Configuración de Red</translation>
    </message>
    <message>
        <location filename="src/connect_dialog.ui" line="37"/>
        <source>Server IP Address:</source>
        <translation>Dirección IP del Servidor:</translation>
    </message>
    <message>
        <location filename="src/connect_dialog.ui" line="71"/>
        <source>Connect</source>
        <translation>Conectar</translation>
    </message>
    <message>
        <location filename="src/connect_dialog.ui" line="91"/>
        <source>Exit</source>
        <translation>Salir</translation>
    </message>
</context>
<context>
    <name>Connecting_Dialog_Class</name>
    <message>
        <location filename="src/connecting_dialog.ui" line="20"/>
        <source>Connecting</source>
        <translation>Conectando</translation>
    </message>
    <message>
        <location filename="src/connecting_dialog.ui" line="38"/>
        <source>Connecting to server. Wait a moment, please...</source>
        <translation>Conectando con el Servidor. Por favor, espere...</translation>
    </message>
    <message>
        <location filename="src/connecting_dialog.ui" line="61"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>QCameraClient</name>
    <message>
        <location filename="src/QCameraClient.cpp" line="136"/>
        <source>Add polygon alarm</source>
        <translation>Añadir alarmas polígono</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="139"/>
        <source>Add line alarm</source>
        <translation>Añadir alarmas línea</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="142"/>
        <source>Apply alarms</source>
        <translation>Aplicar alarmas</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="146"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="150"/>
        <source>Del last polygon alarm</source>
        <translation>Borrar última alarma polígono</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="154"/>
        <source>Del all polygon alarms</source>
        <translation>Borrar todas las alarmas polígono</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="158"/>
        <source>Del last line alarm</source>
        <translation>Borrar última alarma línea</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="162"/>
        <source>Del all line alarms</source>
        <translation>Borrar todas las alarmas línea</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="166"/>
        <source>Config camera parameters...</source>
        <translation>Configurar parametros de la cámara...</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="171"/>
        <source>Add privacity area</source>
        <translation>Añadir área de privacidad</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="174"/>
        <source>Del last privacity area</source>
        <translation>Borrar última área de privacidad</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="178"/>
        <source>Del all privacities areas</source>
        <translation>Borrar todas las áreas de privacidad</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="202"/>
        <source>Add Alarms...</source>
        <translation>Añadir Alarmas...</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="210"/>
        <source>Add privacity areas...</source>
        <translation>Añadir áreas de privacidad...</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="661"/>
        <location filename="src/QCameraClient.cpp" line="688"/>
        <source>&lt;--Out</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="664"/>
        <location filename="src/QCameraClient.cpp" line="691"/>
        <source>In--&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="667"/>
        <location filename="src/QCameraClient.cpp" line="694"/>
        <source>&lt;--In</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="670"/>
        <location filename="src/QCameraClient.cpp" line="697"/>
        <source>Out--&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="874"/>
        <source>TimeoutError: The camera &apos;%1&apos; did not received images from server during %2 minutes.</source>
        <oldsource>TimeoutError: The camera &apos;%1&apos; did not received images from server during %2 seconds.</oldsource>
        <translation>Error: La cámara &apos;%1&apos; no ha recibido imagenes del servidor durante %2 minutos.</translation>
    </message>
    <message>
        <location filename="src/QCameraClient.cpp" line="878"/>
        <source>The camera &apos;%1&apos; has a lousy connection.</source>
        <oldsource>Camera &apos;%1&apos; has a lousy connection</oldsource>
        <translation>La cámara &apos;%1&apos; tiene problema de conexión.</translation>
    </message>
    <message>
        <source>TimeoutError: Camera &quot;</source>
        <translation type="obsolete">Error: La camara &quot;</translation>
    </message>
    <message>
        <source>Camera &apos;</source>
        <translation type="obsolete">Camara &apos;</translation>
    </message>
    <message>
        <source>TimeoutError. New image on camera </source>
        <translation type="obsolete">TimeoutError. No se ha recibido una nueva imágen de la cámara</translation>
    </message>
    <message>
        <source>  no received from server in </source>
        <translation type="obsolete">durante más de </translation>
    </message>
    <message>
        <source> seconds</source>
        <translation type="obsolete">segundos</translation>
    </message>
</context>
<context>
    <name>setClientCamerasParamsClass</name>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="41"/>
        <source>Cameras parameters</source>
        <translation>Parámetros Cámaras</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="60"/>
        <source>Camera</source>
        <translation>Cámara</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="72"/>
        <location filename="src/setClientCamerasParams.ui" line="274"/>
        <location filename="src/setClientCamerasParams.ui" line="535"/>
        <source>TextLabel</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="103"/>
        <source>Video Folder:</source>
        <translation>Directorio de Video:</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="125"/>
        <source>Browse...</source>
        <translation>Buscar...</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="150"/>
        <source>Is enabled</source>
        <translation>Habilitada</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="189"/>
        <source>Is Visible</source>
        <translation>Visible</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="205"/>
        <source>MJPEG Write</source>
        <translation>Almacenar MJPEG</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="221"/>
        <source>Enable videosurveillance</source>
        <translation>Habilitar video vigilancia</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="243"/>
        <source>Camera Name:</source>
        <translation>Nombre Cámara:</translation>
    </message>
    <message>
        <source>Apply Name</source>
        <translation type="obsolete">Aplicar Nombre</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="544"/>
        <source>Background</source>
        <translation>Background</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="556"/>
        <source>BackGround Params</source>
        <translation>Parámetros Background</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="573"/>
        <source>bg_ccMinArea</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="609"/>
        <source>bg_insDelayLog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="645"/>
        <source>bg_refBGDelay</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="681"/>
        <source>bg_refFGDelay</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="717"/>
        <source>bg_threshold</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="262"/>
        <source>Privacity</source>
        <translation>Privacidad</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="303"/>
        <source>Select privacity mode:</source>
        <translation>Seleccionar modo privacidad:</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="317"/>
        <source>Privacity disabled</source>
        <translation>Deshabilitar Privacidad</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="322"/>
        <source>View blobs only</source>
        <translation>Ver sólo blobs </translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="327"/>
        <source>View blobs and fixed image</source>
        <translation>Ver blobs e imagen fija</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="332"/>
        <source>View colored image</source>
        <translation>Ver imagen coloreada</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="173"/>
        <source>View surveillance data</source>
        <translation>Ver datos de vigilancia</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="352"/>
        <source>Show image pixelized</source>
        <translation>Mostrar imagen pixelada</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="359"/>
        <source>Show privacity areas</source>
        <translation>Mostrar áreas de privacidad</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="368"/>
        <source>Alarms</source>
        <translation>Alarmas</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="392"/>
        <source>Edit Alarm</source>
        <translation>Editar Alarma</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="421"/>
        <source>Change Color</source>
        <translation>Cambiar Color</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="450"/>
        <source>Delete Alarm</source>
        <translation>Borrar Alarma</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="469"/>
        <source>Alarm:</source>
        <translation>Alarma:</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="502"/>
        <source>Select Alarm</source>
        <translation>Seleccionar Alarma</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="770"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="790"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="src/setClientCamerasParams.ui" line="810"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>setClientConfigParams</name>
    <message>
        <location filename="src/setClientConfigParams.ui" line="26"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="46"/>
        <source>Video Parameters</source>
        <translation>Parámetros de Vídeo</translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="56"/>
        <source>Video Path:</source>
        <translation>Directorio Vídeo:</translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="66"/>
        <location filename="src/setClientConfigParams.ui" line="214"/>
        <source>Browse...</source>
        <translation>Buscar...</translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="106"/>
        <source>Video path is general</source>
        <translation>Directorio de Vídeo General</translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="126"/>
        <source>Video is to write</source>
        <translation>Almacenar Vídeo</translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="194"/>
        <source>Log Parameters</source>
        <translation>Parámetros de Log</translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="204"/>
        <source>Log Path:</source>
        <translation>Directorio Log:</translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="241"/>
        <source>Log:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="268"/>
        <source>Log cv:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="291"/>
        <source>Log xml:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="314"/>
        <source>Log vs:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="337"/>
        <source>Lognet:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="383"/>
        <source>Others</source>
        <translation>Otros</translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="395"/>
        <source>Language:</source>
        <translation>Idiomas:</translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="408"/>
        <source>Watch-Dog Time:</source>
        <translation>Tiempo de Notificación:</translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="418"/>
        <source> seconds</source>
        <translation>segundos</translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="437"/>
        <source>Enable Watch-Dog:</source>
        <translation>Activar Notificación:</translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.ui" line="447"/>
        <source> (on image reception)</source>
        <translation>(durante la recepción de images)</translation>
    </message>
    <message>
        <source>Enable Watch-Dog on image reception</source>
        <translation type="obsolete">Habilitar Watch-Dog en la recepción de imagen</translation>
    </message>
    <message>
        <source>Watch-Dog Time in seconds:</source>
        <translation type="obsolete">Tiempo Watch-Dog en segundos:</translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.cpp" line="144"/>
        <source>Change Language</source>
        <translation>Cambio de Idioma</translation>
    </message>
    <message>
        <location filename="src/setClientConfigParams.cpp" line="145"/>
        <source>The language has been changed, the new language value will be taken in account on the next reboot.
Do you still want to apply the new language?</source>
        <translation>El nuevo idioma sera aplicado a la proxima apertura del programa
Seguir con los cambios?</translation>
    </message>
</context>
<context>
    <name>setClientVisorParams</name>
    <message>
        <source>-Camera: </source>
        <translation type="obsolete">-Cámara:</translation>
    </message>
    <message>
        <location filename="src/setClientVisorParams.cpp" line="49"/>
        <source>Config</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <location filename="src/setClientVisorParams.cpp" line="53"/>
        <source>Video Scheduler</source>
        <translation>Programador Grabación </translation>
    </message>
</context>
<context>
    <name>setClientVisorParamsClass</name>
    <message>
        <location filename="src/setClientVisorParams.ui" line="35"/>
        <source>Configuration</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <location filename="src/setClientVisorParams.ui" line="70"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="src/setClientVisorParams.ui" line="90"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
</context>
</TS>
