TEMPLATE = app
TARGET = Client_Visor
QT += core \
    gui
	
SOURCES += 	./src/Connect_Dialog.cpp \
	./src/Client_Socket.cpp \
	./src/QBoard.cpp \
	./src/QCamera.cpp \
	./src/setClientVisorParams.cpp  \
	./src/setClientCamerasParams.cpp  \
	./src/setClientConfigParams.cpp  \	
	./src/client_visor.cpp \
	./src/Thread_Socket.cpp \
	./src/client_visor.cpp 

FORMS += ./src/connecting_dialog.ui  \
	./src/connect_dialog.ui  \
	./src/setClientVisorParams.ui  \
	./src/setClientCamerasParams.ui  \
	./src/setClientConfigParams.ui  \
	./src/client_visor.ui 

HEADERS += ./src/Connect_Dialog.h \
	./src/Client_Socket.h \
	./src/QBoard.h \
	./src/QCamera.h \
	./src/setClientVisorParams.h  \
	./src/setClientCamerasParams.h  \
	./src/setClientConfigParams.h  \
	./src/client_visor.h \
	./src/Thread_Socket.h \
	./src/client_visor.h 
