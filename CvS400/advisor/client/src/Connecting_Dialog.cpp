/*
 * Connecting_Dialog.cpp
 *
 *  Created on: 01/09/2009
 *      Author: Gabri
 */

#include "Connecting_Dialog.h"

#include <QHostAddress>
#include <QDebug>

#include "vv_defines.h"

Connecting_Dialog::Connecting_Dialog(QTcpSocket *tcpSocket, QHostAddress *serv_add)
{
	ui.setupUi(this);
	tcp_Socket = tcpSocket;
	connect(tcp_Socket, SIGNAL(connected()), this, SLOT(Connected()));
	connect(ui.cancelButton, SIGNAL(clicked()), this, SLOT(Abort()));
	server_add = serv_add;
	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(Connect()));
	timer->start(3000);
	tcp_Socket->connectToHost(*server_add, 6178);
}
Connecting_Dialog::~Connecting_Dialog()
{
	delete timer;
}
void Connecting_Dialog::Connecting()
{
	while(true);
}
void Connecting_Dialog::Connect()
{
	tcp_Socket->abort();
	tcp_Socket->connectToHost(*server_add, 6178);
}

void Connecting_Dialog::Connected()
{
	timer->stop();
	done(1);
}

void Connecting_Dialog::Abort()
{
	tcp_Socket->abort();
	timer->stop();
	done(0);
}


