#include "QCameraClient.h"
#include "vv_defines.h"
//#include "QCameraTimeoutThread.h"
#include <QPainter>
#include <QColor>
#include <QTextStream>

#include "Camera.hpp"
#include "CameraClient_Socket.h"
#include "setClientCamerasParams.h"
#include "QCameraClient.h"
#include "QtBind.hpp"
#include "Log.hpp"

#include <x7svidsurv.h>
#include <QDataStream>
#include <QMessageBox>

#define     MIN_LINES_PER_FRAME 287
#define     MIN_PIXELS_PER_LINE 360

QCameraClient::QCameraClient(QObject *parent, Camera *camera_ptr, int index)
: QCamera((QWidget*)parent,camera_ptr,index)
  {

	images_received = false;

    qcamera_size = QSize(MIN_PIXELS_PER_LINE, MIN_LINES_PER_FRAME);
    setMinimumSize(qcamera_size);
    resize(qcamera_size);

	addpolygon = false;
	addline	= false;
	drawpolygon = false;
	init_line = false;
	polygon_index = 0;
	line_index = 0;
	polygon_size = 0;
	addprivacity = false;
	privacity_index = 0;
	draw_privacity = false;
	privacity_size = 0;

	frame_add = 0;
	frame_rate = "";

	timeout_ms = 10000;//timeout 10 seconds
	image_timer = new QTimer(this);
	connect(image_timer, SIGNAL(timeout()), this, SLOT(askImageByTimer()));

	m_nTimeOut=0;

	privacity = new QVector<QPolygon>();

	for(unsigned int i = 0; i < MAX_POLYGONS; i++){
		polygon[i] = QPolygon(0);
	}
	for(unsigned int i = 0; i < MAX_LINES; i++){
		line[i] = QLine();
	}
	noAvailableImage = new QImage(":/images/no_camera.png");

	addingAlarm = false;

	createActions();
	createMenus();

	enabled = camera->GetParam("enable")->toIntValue() == 1;
	camera->DrawFrame(enabled);
	name = camera->GetName();

	surveillanceEnabled = camera->GetVidSurvPipeline()->GetParam("enable")->toIntValue() == 1;

	if (surveillanceEnabled){
		pAlarms = camera->GetVidSurvPipeline()->GetAlarmManager();
		last_line_index = pAlarms->Size(X7S_VS_ALR_TYPE_LINE);
		last_polygon_index = pAlarms->Size(X7S_VS_ALR_TYPE_POLY);
	}
	else{
		pAlarms = 0;
		last_line_index = 0;
		last_polygon_index = 0;
	}

	drawable = enabled;

	addAlarmAction->setEnabled(surveillanceEnabled);

	admin_mode = false;
	user_name = "";
	user_password = "";
	SetAdminMode(admin_mode);

	EnableImagePixelized(false);
	viewPrivacityAreas = true;
	privacity_mode = PM_PRIVACITY_DISABLED;

	viewSurveillanceData = true;

	images_received_timeout = true;

	//setUpdatesEnabled(true);

  }

QCameraClient::~QCameraClient()
{
	delete noAvailableImage;
	//	delete timeout;
	delete camera_socket;
	delete privacity;
	delete image_timer;
}

void QCameraClient::EnableImagePixelized(bool enable)
{
	if(enable)
		camera->SetParam("pixelate", "%d", 5);
	else
		camera->SetParam("pixelate", "%d", 0);
	imagePixeled = enable;
}

void QCameraClient::ClientSocketLog(const QString &name, const QString &password, quint32 server_address)
{
	camera_socket = new CameraClient_Socket(this, quint32(server_address));
	connect(camera_socket, SIGNAL(CameraImageReceived(const QByteArray &)), this, SLOT(updateImage(const QByteArray &)));
	connect(camera_socket, SIGNAL(warningOnSocket(const QString &)), this, SLOT(emitWarningOnCamera(const QString &)));
	camera_socket->Connect();
	camera_socket->askServerToLog(name, password);
	user_name = name;
	user_password = password;
	if(enabled && camera_socket->getServerConnected())
	{
		camera_socket->askImageToServer();
		image_timer->start(1000);
	}
/*	if (enabled)
		camera_socket->askImageToServer();//ask for other image*/
}

void QCameraClient::ChangeUser(const QString &name, const QString &password)
{
	camera_socket->askServerToLog(name, password);
	user_name = name;
	user_password = password;
//	if (drawable)
		camera_socket->askImageToServer();//ask for other image};
}

void QCameraClient::SetParamCamera(const QString &param, const QString &value)
{
	camera_socket->SetParamCamera(param, value);
}

void QCameraClient::createActions()
{
	addPolygonAction = new QAction(tr("Add polygon alarm"), this);
	connect(addPolygonAction, SIGNAL(triggered()), this, SLOT(addPolygon()));

	addLineAction = new QAction(tr("Add line alarm"), this);
	connect(addLineAction, SIGNAL(triggered()), this, SLOT(addLine()));

	applyAction = new QAction(tr("Apply alarms"), this);
	applyAction->setEnabled(false);
	connect(applyAction, SIGNAL(triggered()), this, SLOT(apply()));

	cancelAction = new QAction(tr("Cancel"), this);
	connect(cancelAction, SIGNAL(triggered()), this, SLOT(cancel()));
	cancelAction->setEnabled(false);

	delLastPolygonAction = new QAction(tr("Del last polygon alarm"), this);
	delLastPolygonAction->setEnabled(false);
	connect(delLastPolygonAction, SIGNAL(triggered()), this, SLOT(delLastPolygon()));

	delPolygonsAction = new QAction(tr("Del all polygon alarms"), this);
	delPolygonsAction->setEnabled(false);
	connect(delPolygonsAction, SIGNAL(triggered()), this, SLOT(delAllPolygons()));

	delLastLineAction = new QAction(tr("Del last line alarm"), this);
	delLastLineAction->setEnabled(false);
	connect(delLastLineAction, SIGNAL(triggered()), this, SLOT(delLastLine()));

	delLinesAction = new QAction(tr("Del all line alarms"), this);
	delLinesAction->setEnabled(false);
	connect(delLinesAction, SIGNAL(triggered()), this, SLOT(delAllLines()));

	setCameraParamsAction = new QAction(tr("Config camera parameters..."), this);
	setCameraParamsAction->setEnabled(true);
	setCameraParamsAction->setCheckable(false);
	connect(setCameraParamsAction, SIGNAL(triggered()), this, SLOT(setCameraParams()));

	addPrivacityAction = new QAction(tr("Add privacity area"), this);
	connect(addPrivacityAction, SIGNAL(triggered()), this, SLOT(addPrivacity()));

	delLastPrivacityAction = new QAction(tr("Del last privacity area"), this);
	delLastPrivacityAction->setEnabled(false);
	connect(delLastPrivacityAction, SIGNAL(triggered()), this, SLOT(delLastPrivacity()));

	delPrivacityAction = new QAction(tr("Del all privacities areas"), this);
	delPrivacityAction->setEnabled(false);
	connect(delPrivacityAction, SIGNAL(triggered()), this, SLOT(delAllPrivacities()));
}

void QCameraClient::createMenus()
{
	menu = new QMenu();
	menu->addAction(addPolygonAction);
	menu->addAction(addLineAction);
	menu->addAction(applyAction);
	menu->addAction(cancelAction);
	menu->addAction(delLastPolygonAction);
	menu->addAction(delPolygonsAction);
	menu->addAction(delLastLineAction);
	menu->addAction(delLinesAction);

	menu_enable = new QMenu();
	menu_enable->addAction(setCameraParamsAction);


	addAlarmAction = menu_enable->addMenu(menu);
	menu_enable->addAction(addAlarmAction);
	addAlarmAction->setEnabled(false);
	addAlarmAction->setText(tr("Add Alarms..."));

	privacity_menu  = new QMenu();
	privacity_menu->addAction(addPrivacityAction);
	privacity_menu->addAction(delPrivacityAction);
	privacity_menu->addAction(delLastPrivacityAction);
	addPrivacityAction = menu_enable->addMenu(privacity_menu);
	addPrivacityAction->setEnabled(true);
	addPrivacityAction->setText(tr("Add privacity areas..."));
}


void QCameraClient::SetAdminMode(bool enable)
{
	if(surveillanceEnabled)
		addAlarmAction->setEnabled(enable);
	addPrivacityAction->setEnabled(enable);
	admin_mode = enable;
	setCameraParamsAction->setEnabled(enable);
//	if ((enable == false)&&(user_name != ""))
	//	camera_socket->exitAdminMode();
}
/*
void QCameraClient::EnablePrivacityMode(bool enable)
{
	setCameraParamsAction->setEnabled(enable);
	addPrivacityAction->setEnabled(enable);
}
*/
void QCameraClient::addPolygon()
{
	addpolygon = true;
	addline = false;
	setMouseTracking(true);
	addLineAction->setEnabled(false);
	addPolygonAction->setEnabled(false);
	addPrivacityAction->setEnabled(false);
	cancelAction->setEnabled(true);
	polygon_size = 0;
	delPolygonsAction->setEnabled(false);
	delLastPolygonAction->setEnabled(false);
	setCursor(Qt::CrossCursor);
	update();
}

void QCameraClient::addPrivacity()
{
	addprivacity = true;
	addpolygon = false;
	addline = false;
	setMouseTracking(true);
	addPrivacityAction->setEnabled(false);
	addPolygonAction->setEnabled(false);
	addLineAction->setEnabled(false);
	cancelAction->setEnabled(true);
	privacity_size = 0;
	delPrivacityAction->setEnabled(false);
	delLastPrivacityAction->setEnabled(false);
	setCursor(Qt::CrossCursor);
	polygon_tmp = QPolygon(0);
	//privacity->append(QPolygon(0));
	avoDebug("QCameraClient::addPrivacity()") << "Add privacity: " << privacity->size();
	//	update();
}

void QCameraClient::addLine()
{
	addline = true;
	addpolygon = false;
	setMouseTracking(true);
	addLineAction->setEnabled(false);
	addPolygonAction->setEnabled(false);
	addPrivacityAction->setEnabled(false);
	cancelAction->setEnabled(true);
	polygon_size = 0;
	delLinesAction->setEnabled(false);
	delLastLineAction->setEnabled(false);
	setCursor(Qt::CrossCursor);
	update();
}

void QCameraClient::addNewAlarmFromServer(QByteArray &alarm_array)
{
	quint32 alarm_index ;
	quint32 alarm_type;
	QByteArray array = QByteArray(alarm_array);
	QDataStream alarm_stream(&array, QIODevice::ReadOnly);
	alarm_stream.setVersion(QDataStream::Qt_4_3);
	alarm_stream >> alarm_type;

	avoDebug("QCameraClient::addNewAlarmFromServer()") << " new Alarm received from Server";
	if (alarm_type == X7S_VS_ALR_TYPE_LINE){
		alarm_stream >> alarm_index ;
		if(alarm_index > MAX_LINES)
			return;
		for(quint8 i = 0; i < alarm_index; i++){
			alarm_stream >> line[i];
			QColor color = QColor();
			alarm_stream >> color;
			QtBind::toX7sVSAlrDetector(pAlarms,line[i],color);
		}
	}
	if (alarm_type == X7S_VS_ALR_TYPE_POLY){
		alarm_stream >> alarm_index ;
		if(alarm_index > MAX_POLYGONS)
			return;
		for(quint8 j = 0; j < alarm_index; j++){
			QPolygon poligono;
			QColor color = QColor();
			alarm_stream >> poligono;
			alarm_stream >> color;
			QtBind::toX7sVSAlrDetector(pAlarms,poligono,color);
		}
	}
	camera->resetVideo();
	update();
}


void QCameraClient::apply()
{
	//Adding polygon alarms
	QByteArray alarmp_array;
	QDataStream alarmp_stream(&alarmp_array, QIODevice::WriteOnly);
	alarmp_stream.setVersion(QDataStream::Qt_4_3);

	if((polygon_index)&&(polygon_index <= MAX_POLYGONS)){
		alarmp_stream << (quint32)X7S_VS_ALR_TYPE_POLY << (quint32)polygon_index ;
		for(int j = 0; j < (int)polygon_index; j++){
			alarmp_stream << polygon[j];

			QColor color = getAlarmPolyPenQColor(j + last_polygon_index);

			alarmp_stream << color;

			//QColor color = polygon_color_pen[j + last_polygon_index];
			QtBind::toX7sVSAlrDetector(pAlarms,polygon[j],color);
			polygon[j] = QPolygon(0);

		}
		camera_socket->sendNewAlarmsToServer(alarmp_array);
		emit newAlarmsAdded(alarmp_array, quint8(camera_index));
	}
	//Adding line alarms
	QByteArray alarml_array;
	QDataStream alarml_stream(&alarml_array, QIODevice::WriteOnly);
	alarml_stream.setVersion(QDataStream::Qt_4_3);
	if ((line_index)&&(line_index <= MAX_LINES)){
		alarml_stream << (quint32)X7S_VS_ALR_TYPE_LINE << (quint32)line_index ;
		for(unsigned int i = 0; i < line_index; i++){

			QColor color = getAlarmLinePenQColor(i + last_line_index);
			QtBind::toX7sVSAlrDetector(pAlarms,line[i],color);

			alarml_stream << line[i];
			alarml_stream << color;
			line[i] = QLine();

		}
		emit newAlarmsAdded(alarml_array, quint8(camera_index));
		camera_socket->sendNewAlarmsToServer(alarml_array);
	}
	update();
	applyAction->setEnabled(false);
	delPolygonsAction->setEnabled(false);
	delLinesAction->setEnabled(false);
	delLastPolygonAction->setEnabled(false);
	delLastLineAction->setEnabled(false);
	last_line_index = ++line_index;
	last_polygon_index = ++polygon_index;
	line_index = 0;
	polygon_index = 0;
	addline = false;
	addpolygon = false;
	addLineAction->setEnabled(true);
	addPolygonAction->setEnabled(true);
	setCursor(Qt::ArrowCursor);

	camera->resetVideo();


}

void QCameraClient::cancel()
{
	if (addpolygon){
		addpolygon = false;
		delPolygonsAction->setEnabled(true);
		addPolygonAction->setEnabled(true);
		if(polygon_index < MAX_POLYGONS)
			polygon[polygon_index]= QPolygon(0);
	}
	if (addline){
		addline = false;
		init_line = false;
		delLinesAction->setEnabled(true);
		addLineAction->setEnabled(true);
		if(line_index < MAX_LINES)
			line[line_index]= QLine();
	}
	addPolygonAction->setEnabled(true);
	addPrivacityAction->setEnabled(true);
	addLineAction->setEnabled(true);
	applyAction->setEnabled(false);
	cancelAction->setEnabled(false);
	setCursor(Qt::ArrowCursor);
	update();
}

void QCameraClient::delLastPolygon()
{
	//addpolygon = false;
	if(polygon_index == 0)
		return;
	polygon_index--;
	if(polygon_index >= MAX_POLYGONS)
		return;
	polygon[polygon_index] = QPolygon(0);
	if (polygon_index){
		delLastPolygonAction->setEnabled(true);
		delPolygonsAction->setEnabled(true);
	}
	else{
		delLastPolygonAction->setEnabled(false);
		delPolygonsAction->setEnabled(false);
	}
	addPolygonAction->setEnabled(true);
	update();
}

void QCameraClient::delLastPrivacity()
{
	//addpolygon = false;

	if (privacity->isEmpty()){
		delLastPrivacityAction->setEnabled(false);
		delPrivacityAction->setEnabled(false);
	}
	else{
		delLastPrivacityAction->setEnabled(true);
		delPrivacityAction->setEnabled(true);
		//privacity->remove(privacity->size()-1);
		privacity->pop_back();
	}
	addPrivacityAction->setEnabled(true);
	avoDebug("QCameraClient::delLastPrivacity()") << "Last privacity deleted: " << privacity->size();
	update();
}

void QCameraClient::delLastLine()
{
	//addline = false;
	if(line_index == 0)
		return;
	line_index--;
	if(line_index >= MAX_LINES)
		return;
	line[line_index] = QLine();
	if (line_index){
		delLastLineAction->setEnabled(true);
		delLinesAction->setEnabled(true);
	}
	else{
		delLastLineAction->setEnabled(false);
		delLinesAction->setEnabled(false);
	}
	addLineAction->setEnabled(true);
	update();
}

void QCameraClient::delAllPolygons()
{
	//addpolygon = false;
	polygon_index = 0;
	for(unsigned int i = 0; i < MAX_POLYGONS; i++)
		polygon[i] = QPolygon(0);
	addPolygonAction->setEnabled(true);
	delPolygonsAction->setEnabled(false);
	delLastPolygonAction->setEnabled(false);
	update();
}

void QCameraClient::delAllPrivacities()
{
	//addpolygon = false;
	privacity_index = 0;

	while(privacity->size())
		privacity->pop_back();
	//privacity[i] = QPolygon(0);
	addPrivacityAction->setEnabled(true);
	delPrivacityAction->setEnabled(false);
	delLastPrivacityAction->setEnabled(false);
	avoDebug("QCameraClient::delAllPrivacities()") << "All privacities deleted: " << privacity->size();
	update();
}

void QCameraClient::AddPrivacityAreas(QVector<QPolygon> * p_area)
{
	for(int i = 0; i< p_area->size(); i++){
		privacity->append(p_area->at(i));
		avoDebug("QCameraClient::AddPrivacityAreas()") << "Add privacity: " << privacity->size() << p_area->at(i);
	}
	if(p_area->size())
		delPrivacityAction->setEnabled(true);//La activamos para poder borrarlas.
}

void QCameraClient::delAllLines()
{
	//addline = false;
	line_index = 0;
	for(unsigned int i = 0; i < MAX_LINES; i++)
		line[i] = QLine();
	addLineAction->setEnabled(true);
	delLinesAction->setEnabled(false);
	delLastLineAction->setEnabled(false);
	update();
}
void QCameraClient::enableQCameraFromServer(bool enable)
{
	if(enabled != enable)
	{
		enabled = enable;
		setDrawable(enable);
		camera->DrawFrame(enabled);
		emit cameraEnabled(enable, camera_index);
		if(enabled && camera_socket->getServerConnected())
		{
			camera_socket->askImageToServer();
			image_timer->start(1000);
		}
	}
	update();
}
void QCameraClient::enableQCamera(bool enable)
{
	if(enabled != enable)
	{
		enabled = enable;
		camera_socket->sendEnableCameraToServer(enable);
		setDrawable(enable);
		camera->DrawFrame(enabled);
		emit cameraEnabled(enable, camera_index);
		if(enable == false)
			image_timer->stop();
		else if(enabled && camera_socket->getServerConnected())
		{
			camera_socket->askImageToServer();
			image_timer->start(1000);
		}
	}
	update();
}

void QCameraClient::setDrawable(bool enable)
{
	if(enabled == false){
		drawable = false;
		return;
	}
	if(drawable != enable)
	{
		drawable = enable;
		emit cameraVisible(enable, camera_index);
		camera->DrawFrame(enable);
/*		if(enable == false)
			image_timer->stop();
		if(drawable&&enabled){
			camera_socket->askImageToServer();
			image_timer->start(1000);
		}*/
	}
	update();
}

void QCameraClient::enableSurveillance(bool enable)
{
	if ((enabled == false)||(surveillanceEnabled == enable))
		return;
	surveillanceEnabled = enable; 
	camera->GetVidSurvPipeline()->SetParam("enable", "%d",enable);
	if (enable)
		pAlarms = camera->GetVidSurvPipeline()->GetAlarmManager();
	last_line_index = pAlarms->Size(X7S_VS_ALR_TYPE_LINE);
	last_polygon_index = pAlarms->Size(X7S_VS_ALR_TYPE_POLY);
	addAlarmAction->setEnabled(enable);
	EnableImagePixelized(enable);
	camera_socket->sendEnableSurveillanceToServer(enable);
	emit cameraSurveillanceEnabled(enable, camera_index);

}

void QCameraClient::enableSurveillanceFromServer(bool enable)
{
	if ((enabled == false)||(surveillanceEnabled == enable))
		return;
	surveillanceEnabled = enable;
	camera->GetVidSurvPipeline()->SetParam("enable", "%d",enable);
	if (enable)
		pAlarms = camera->GetVidSurvPipeline()->GetAlarmManager();
	last_line_index = pAlarms->Size(X7S_VS_ALR_TYPE_LINE);
	last_polygon_index = pAlarms->Size(X7S_VS_ALR_TYPE_POLY);
	addAlarmAction->setEnabled(enable);
	emit cameraSurveillanceEnabled(enable, camera_index);
}

void QCameraClient::setCameraParams()
{
	setClientCamerasParams *camera_params = new setClientCamerasParams(0, this);
	camera_params->show();
	camera_params->ShowPushButtons();
	camera_params->exec();
	delete camera_params;
}

void QCameraClient::addAlarm()
{
	addingAlarm = true;
}

void QCameraClient::exitAddAlarm()
{
	addingAlarm = false;
}

void QCameraClient::paintEvent(QPaintEvent *event)
{
	//	avoDebug("QCameraClient::paintEvent():") << " Painting new image.";
	if(drawable == false)
		return;
	QPainter painter(this);
	if (privacity_mode == PM_VIEW_BLENDED_IMAGE)
		painter.setCompositionMode(QPainter::RasterOp_NotSourceAndNotDestination);
	//	QPainterPath path;


	if (/*(pixmap.isNull())*/ (images_received == false)|| (enabled == false) || (drawable == false)) {
		painter.drawImage(0,0, *noAvailableImage);
		painter.setPen(QPen(getAlarmLinePenQColor(camera_index), 3));
		painter.drawText(10, qcamera_size.height()-10, name);
		return;
	}

	if ((privacity_mode == PM_VIEW_BLOBS_AND_FIXED_IMAGE) ||(privacity_mode == PM_VIEW_BLENDED_IMAGE))
		painter.drawImage(0,0, fixedImage);
	if ((privacity_mode !=PM_VIEW_BLOBS_ONLY)&&(privacity_mode !=PM_VIEW_BLOBS_AND_FIXED_IMAGE))
		painter.drawImage(0,0, image_v);
	painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
	if ((viewSurveillanceData) || (privacity_mode != PM_PRIVACITY_DISABLED))
		painter.drawImage(0,0, image_d);
	if((line_index > MAX_LINES)||(polygon_index > MAX_POLYGONS))
		return;

	for(unsigned int i = 0; i < polygon_index; i++){
		painter.setPen(QPen(getAlarmPolyPenQColor(i), 3));
		painter.drawPolygon(polygon[i]);
		//painter.setClipRegion(polygon[i], Qt::IntersectClip);
	}
	if(addpolygon){
		if(!polygon[polygon_index].isEmpty()){
			painter.setPen(QPen(getAlarmPolyPenQColor(polygon_index), 3, Qt::DashDotLine, Qt::RoundCap));
			painter.drawPolyline(polygon[polygon_index]);
			painter.drawPolyline(last_line_point, 2);
		}
	}
	painter.setBrush(Qt::Dense1Pattern);
	if(viewPrivacityAreas)
		for(int i = 0; i < privacity->size(); i++){
			painter.setPen(QPen(getAlarmPolyPenQColor(i), 1));
			painter.drawPolygon(privacity->at(i), Qt::WindingFill);
			//painter.setClipRegion(polygon[i], Qt::IntersectClip);
		}
	for(unsigned int i = 0; i < line_index; i++){
		painter.setPen(QPen(getAlarmLinePenQColor(i), 3));
		QPoint point[2];
		point[0] = line[i].p1();
		point[1] = line[i].p2();
		painter.drawPolyline(point, 2);
		//Dibujamos los textos asociados
		QPoint point_t = (point[1] + point[0])/2;
		point_t.setX(point_t.x() - 50);
		//point_t.setY(point_t.y() - 20);
		if (point[1].y() >= point[0].y()){
			painter.drawText(point_t, tr("<--Out"));
			point_t.setX(point_t.x() + 70);
			//point_t.setY(point_t.y() + 40);
			painter.drawText(point_t, tr("In-->"));
		}
		else{
			painter.drawText(point_t, tr("<--In"));
			point_t.setX(point_t.x() + 70);
			//point_t.setY(point_t.y() + 40);
			painter.drawText(point_t, tr("Out-->"));
		}
	}
	if(addprivacity){
		if(!polygon_tmp.isEmpty()){
			painter.setPen(QPen(getAlarmPolyPenQColor(privacity_index), 3, Qt::DashDotLine, Qt::RoundCap));
			painter.drawPolyline(polygon_tmp);
			painter.drawPolyline(last_line_point, 2);
		}
	}
	if(addline && init_line){
		painter.setPen(QPen(getAlarmLinePenQColor(line_index), 3, Qt::DashDotLine, Qt::RoundCap));
		painter.drawPolyline (last_line_point, 2);
		//drawText ( const QPoint & position, const QString & text )
		QPoint point = (last_line_point[1] + last_line_point[0])/2;
		point.setX(point.x() - 50);
		//point.setY(point.y() - 20);
		if (last_line_point[1].y() >= last_line_point[0].y()){
			painter.drawText(point, tr("<--Out"));
			point.setX(point.x() + 70);
			//point.setY(point.y() + 40);
			painter.drawText(point, tr("In-->"));
		}
		else{
			painter.drawText(point, tr("<--In"));
			point.setX(point.x() + 70);
			//point.setY(point.y() + 40);
			painter.drawText(point, tr("Out-->"));
		}
	}
	painter.setPen(QPen(Qt::white, 3));
	painter.setBackgroundMode(Qt::OpaqueMode);
	QColor colortext = camera->GetColor();
	colortext.setAlpha(100);
	painter.setBackground(QBrush(colortext, Qt::SolidPattern));
#ifdef DEBUG
	painter.drawText(10, qcamera_size.height()-10, " "+ name + frame_rate+" ");
#else
	painter.drawText(10, qcamera_size.height()-10, " "+name+" ");
#endif
}

void QCameraClient::updateImage(const QByteArray &qmeta_jpeg)
{
	X7sByteArray metaJPEG;

	if (enabled == false)
		return;
	image_timer->start(1000);
	images_received = true;
	images_received_timeout = true;

	metaJPEG.type=X7S_CV_MJPEG_FRAMEDATA_TYPE;
	metaJPEG.data = (uint8_t*)qmeta_jpeg.data();
	metaJPEG.length = qmeta_jpeg.size();
	metaJPEG.capacity = qmeta_jpeg.capacity();

//	camera_socket->askImageToServer();//ask for other image
//	image_timer->start(1000);
	if(metaJPEG.length == 0 || metaJPEG.capacity == 0){
		avoDebug("QCameraClient::updateImage():") << " Bad size image";
		return;
	}
	frame_add++;
	bool ret = camera->SetMetaJPEGFrame(&metaJPEG);
	camera_socket->askImageToServer();//ask for other image
	if(ret==false){
		avoDebug("QCameraClient::updateImage():") << " SetMetaJPEGFrame() returned false.";
		return;
	}
	emit newFrameReceived(camera);
	if(drawable == false)
		return;
    if ((qcamera_size != camera->imageFrame().size())&&(camera->imageFrame().isNull()==false)) {
    	qcamera_size.setHeight(camera->imageFrame().size().height());
    	qcamera_size.setWidth(camera->imageFrame().size().width());
    	setMinimumSize(qcamera_size);
    	resize(qcamera_size);
    }
	if ((privacity_mode != PM_VIEW_BLOBS_AND_FIXED_IMAGE) && (privacity_mode != PM_VIEW_BLOBS_ONLY))
		image_v = QImage(camera->imageFrame());//.copy(0,0,PIXELS_PER_LINE, LINES_PER_FRAME);
	if ((viewSurveillanceData) || (privacity_mode != PM_PRIVACITY_DISABLED))
		image_d = camera->imageMData().copy(0,0,camera->imageFrame().width(), camera->imageFrame().height());//.copy(0,0,PIXELS_PER_LINE, LINES_PER_FRAME);

	/*if (images_received == false && timeout->IsStarted() == false)
		timeout->StartTimeout();
	images_received = true;
	images_received_timeout = true;*/
	//avoDebug("QCameraClient::updateImage():") << " New image received.";
	repaint();
	//emit newFrameReceived(camera);
	//update();
}

void QCameraClient::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton){
		if((line_index > MAX_LINES-1)||(polygon_index > MAX_POLYGONS-1))
			return;
		if((event->pos().x()>qcamera_size.width()) || (event->pos().y()>qcamera_size.height()))
			return;
		if (addpolygon){
			polygon[polygon_index] << event->pos();
			polygon_size++;
			if(polygon_size > 2)//At least 3 point for polygon
				applyAction->setEnabled(true);
			last_line_point[0] = event->pos();
			update();
		}
		if (addprivacity){
			polygon_tmp << event->pos();
			privacity_size++;
			last_line_point[0] = event->pos();
			update();
		}
		if (addline){
			if (init_line == false){
				line[line_index].setP1(event->pos());
				last_line_point[0] = event->pos();
			}
			init_line = true;
			applyAction->setEnabled(true);
			update();
		}
	}
	if (event->button() == Qt::RightButton){
		if((event->pos().x()>qcamera_size.width()) || (event->pos().y()>qcamera_size.height()))
			return;
		menu_enable->exec(event->globalPos());
	}
}

void QCameraClient::mouseDoubleClickEvent ( QMouseEvent * event ) 
{
	if (event->button() == Qt::LeftButton){
		if((event->pos().x() > qcamera_size.width()) || (event->pos().y() > qcamera_size.height()))
			return;
		if (addpolygon &&(polygon_size > 2)){
			addPolygonAction->setEnabled(false);
			addLineAction->setEnabled(true);
			applyAction->setEnabled(true);
			delPolygonsAction->setEnabled(true);
			delLastPolygonAction->setEnabled(true);
			cancelAction->setEnabled(false);
			if (polygon_index >= MAX_POLYGONS-1){
				addPolygonAction->setEnabled(false);
				addpolygon = false;
				setCursor(Qt::ArrowCursor);
			}
			polygon_index++;
			update();
			return;
		}
		if (addprivacity &&(polygon_tmp.size() > 2)){
			addPrivacityAction->setEnabled(true);
			addPolygonAction->setEnabled(true);
			addLineAction->setEnabled(true);
			delPrivacityAction->setEnabled(true);
			delLastPrivacityAction->setEnabled(true);
			addprivacity = false;
			setCursor(Qt::ArrowCursor);
			privacity_index++;
			privacity->append(polygon_tmp);
			update();
			return;
		}
		if (addline){
			line[line_index].setP2(event->pos());
			init_line = false;
			addLineAction->setEnabled(false);
			addPolygonAction->setEnabled(true);
			applyAction->setEnabled(true);
			delLinesAction->setEnabled(true);
			delLastLineAction->setEnabled(true);
			cancelAction->setEnabled(false);
			if (line_index >= MAX_LINES-1){
				addLineAction->setEnabled(false);
				addline = false;
				setCursor(Qt::ArrowCursor);
			}
			line_index++;
			update();
			return;
		}
		/*		if (admin_mode)
			setDrawable(enabled == false);*/
	}
}

void QCameraClient::mouseMoveEvent ( QMouseEvent * event ) 
{
	if((event->pos().x() > qcamera_size.width()) || (event->pos().y() > qcamera_size.height()))
		return;
	if (addpolygon|| addline || addprivacity){
		last_line_point[1] = event->pos();
		update();
	}
}

void QCameraClient::TimeoutError()
{

//	if((drawable == false)||(enabled == false))
	if(enabled == false)
		return;
	//update();
#ifdef DEBUG
	frame_rate = QString(" @ " + QString::number(float(1000.0*(float)frame_add/(float)timeout_ms),'f',2) + " fps");
#endif
	frame_add = 0;
	m_nTimeOut++;

	if(images_received_timeout){
		//if (drawable)
			//ActualizeFixedImage();
		images_received_timeout = false;
		m_nTimeOut=0;
		return;
	}
	//images_received = false;
	if(m_nTimeOut==12)
	{
		emit warningOnCamera(tr("TimeoutError: The camera '%1' did not received images from server during %2 minutes.").
				arg(name).arg(2*timeout_ms/10001,3));
		images_received = false;
	}
	else {
		emit notificationOnCamera(tr("The camera '%1' has a lousy connection.").arg(name));
	}

	if(camera_socket->getServerConnected())
		camera_socket->askImageToServer();
	update();

}

void QCameraClient::SetWatchDogTime(quint32 time){
	timeout_ms = time;
}

quint32 QCameraClient::GetWatchDogTime(){
	return timeout_ms/1000;
}

void QCameraClient::askImageByTimer(){
	if(camera_socket->getServerConnected()&& enabled)
		camera_socket->askImageToServer();
}

void QCameraClient::ActualizeFixedImage(){
	if ((privacity_mode == PM_VIEW_BLOBS_AND_FIXED_IMAGE) || (privacity_mode == PM_VIEW_BLENDED_IMAGE))
		fixedImage = QImage(camera->imageFrame());//.copy(0,0,PIXELS_PER_LINE, LINES_PER_FRAME);
}

void QCameraClient::emitWarningOnCamera(const QString &warning)
{
	emit warningOnCamera(warning);
}

void QCameraClient::SetPrivacityMode(PrivacityModes mode)
{
	privacity_mode = mode;
	if ((privacity_mode == PM_VIEW_BLOBS_AND_FIXED_IMAGE) || (privacity_mode == PM_VIEW_BLENDED_IMAGE))
		ActualizeFixedImage();
}


