#include "client_visor.h"

#include "vv_defines.h"

#include "Root.hpp"
#include "Devices.hpp"
#include "Log.hpp"
#include "Board.hpp"
#include "Camera.hpp"
#include "QCameraClient.h"
#include "Client_Socket.h"
#include "Connect_Dialog.h"
#include "LogConfig.hpp"
#include "QtBind.hpp"
#include "setClientVisorParams.h"
#include "EventListWidget.h"
#include "ViewerWidget.h"
#include "VideoInFile.hpp"
#include "Database.hpp"
#include "AlrTimeWidget.h"
#include "AuthenticationDialog.h"
#include "WarningWidget.h"
#include "CamEvtHandler.h"
#include "CamEvt.hpp"
#include "VideoHandler.hpp"
#include "TimeTicker.hpp"
#include "AboutUsWidget.h"
#include "AvoSysTrayIcon.h"

#include <QMessageBox>
#include <QTextEdit>
#include <QSplitter>
#include <QString>
#include <QColor>
#include <QTime>
#include <QVector>
#include <QPolygon>

QColor text_color_pen[] = { Qt::red, Qt::green, Qt::blue, Qt::cyan, Qt::magenta, Qt::yellow,
		Qt::gray, Qt::lightGray, Qt::white, Qt::black, };

using namespace std;

#define AVO_SERVER_MAINWIN_STATE_VER 0


Client_Visor::Client_Visor(QWidget *parent)
: QMainWindow(parent),
  locationLabel(NULL), socket_client(NULL), connectDialog(NULL),eventTextEdit(NULL),splitter(NULL),
  clientVisorParams(NULL),splitter_event(NULL),
  serv_root(NULL),client_database(NULL), vidHandler(NULL), camEvtHandler(NULL),
  eventList(NULL), videoViewer(NULL),  warningList(NULL), sysTrayIco(NULL)
  {

	X7S_FUNCNAME("Client_Visor::Client_Visor()");

	ui.setupUi(this);

	AboutUsWidget *aboutUs = new AboutUsWidget();
	aboutUs->hide();
	connect(ui.actionAbout_us,SIGNAL(triggered()),aboutUs,SLOT(show()));

	//Load default settings
	QSettings settings;
	server_address = settings.value("server/ip_address", 0).toUInt();
	if (server_address == 0){
		connectDialog = new Connect_Dialog(0, server_address);
		connectDialog->show();
		server_address = connectDialog->exec();
		delete connectDialog;
		if (server_address == 0)
			exit(0);
	}
	socket_client = new Client_Socket(0, quint32(server_address));
	connect(socket_client, SIGNAL(ServerConnectionError()), this, SLOT(ServerConnectionError()));
	connect(socket_client, SIGNAL(ServerConnectedAgain()), this, SLOT(ServerConnectedAgain()));


	socket_client->Connect();

	while (socket_client->getServerConnected() == false)
	{
		avoDebug(funcName) << "Error. Connecting to server timeout";
		int result = QMessageBox::warning(this,tr("Error"),
				tr("Error. Connecting to server timeout."),
				tr("&Try again"), tr("&Config parameters..."), tr("&Close application"),0,1);
		avoDebug(funcName) << quint32(result);
		if (result == 1){//Configurar par�metros
			connectDialog = new Connect_Dialog(0, server_address);
			connectDialog->show();
			server_address = connectDialog->exec();
			delete connectDialog;
			if (server_address == 0)
				exit(0);

			socket_client->setIP_Address(server_address);
			socket_client->Connect();
		}
		if (result == 0){//Reintentar
			socket_client->Connect();
		}
		if (result == 2)//Salimos
			exit(0);
	}

	avoDebug(funcName) << "Conectado al servidor";

	name = "";
	password = "";

	AuthenticationDialog *dialog = new AuthenticationDialog(0, &name, &password);
	dialog->show();
	int result = dialog->exec();
	delete dialog;
	if (result == 0)
		exit(0);
	avoDebug(funcName) << "User: " << name << " Password: " << password;

	socket_client->askServerToLog(name, password);
	admin_mode = false;
//	privacity_mode = false;
	user_mode = AVO_VISOR_MODE;
	ClientUsersMode user_mode_t;
	user_mode_t = AVO_VISOR_MODE;
	while (socket_client->getServerLogged() == false){
		avoDebug(funcName) << "Error. Logging not authorized by the server.";
		int result = QMessageBox::warning(this,tr("Error"),
				tr("Error. Logging not authorized by the server."),
				tr("&Try again"), tr("&Close application"), "",0,1);
		if (result == 0){
			AuthenticationDialog *dialog = new AuthenticationDialog(0, &name, &password);
			dialog->show();
			int result = dialog->exec();
			delete dialog;
			if (result == 0)
				exit(0);

			socket_client->askServerToLog(name, password);
		}
		if (result)
			exit(0);
	}

	if(socket_client->getUserLevel() == 0){//Warning si entra como adm�n.
		result = QMessageBox::question( this, tr("Admin Access"),tr("You have admin privileges, do you want to activate Admin Mode?"),
				QMessageBox::Yes,
				QMessageBox::No|QMessageBox::Default, QMessageBox::NoButton);
		if (result == QMessageBox::Yes){//Habilitamos men�s de admin
			admin_mode = true;
			user_mode_t = AVO_ADMIN_MODE;
		}

	}
	connect(ui.actionEnter_Admin_mode, SIGNAL(triggered()), this, SLOT(EnableAdminMode()));
	//connect(ui.actionEnable_Privacity_Mode, SIGNAL(toggled(bool)), this, SLOT(EnablePrivacityMode(bool)));
//	connect(ui.actionEnter_Privacity_Mode, SIGNAL(triggered()), this, SLOT(EnablePrivacityMode()));
	connect(ui.actionEnter_Visor_Mode, SIGNAL(triggered()), this, SLOT(EnableVisorMode()));
	connect(ui.actionFull_Screen, SIGNAL(toggled(bool)), this, SLOT(fullScreen(bool)));
	connect(ui.actionDefault_view, SIGNAL(triggered()), this, SLOT(SetDefaultView()));
	ui.actionFull_Screen->setShortcut(Qt::Key_F5);

	splitter = new QSplitter(Qt::Horizontal, this);
	splitter->setGeometry(7, 7, 1500, 1200);
	setCentralWidget(splitter);

	connect(ui.actionShow, SIGNAL(toggled(bool)), this, SLOT(ShowAlarmsWindows(bool)));
	connect(ui.actionVertical, SIGNAL(toggled(bool)), this, SLOT(ChangeAlarmsWindowOrientation(bool)));



	qcameras_gridLayout = new QGridLayout(ui.cameras);
	qcameras_gridLayout->setObjectName(QString::fromUtf8("qcameras_gridLayout"));

	events_gridLayout = new QGridLayout(ui.events);
	events_gridLayout->setObjectName(QString::fromUtf8("events_gridLayout"));

	//Event LIST
	eventList = new EventListWidget(this->parentWidget(),200);
	splitter_event = new QSplitter(Qt::Vertical, this);
	connect(eventList, SIGNAL(EventSelectedSignal(const CamEvtCarrier&, bool)), this, SLOT(EventSelected(const CamEvtCarrier&, bool)));

	warningList = new WarningWidget(this);
	connect(socket_client, SIGNAL(warningOnSocket(const QString &)), warningList, SLOT(AddWarning(const QString&)));
	//	connect(socket_client, SIGNAL(warningOnSocket(const QString &)), this, SLOT(Reco(const QString&)));
	this->setupSysTray();


	splitter->addWidget(ui.event_Widget);

	connect(ui.event_Widget, SIGNAL(currentChanged(int)), this, SLOT(RefreshAlarmTime(int)));

	splitter->addWidget(splitter_event);
	splitter_event->addWidget(eventList);
	splitter->show();
	splitter->setStretchFactor(0,1);

	images_timeout = new QTimer(this);

	client_database = new Database(NULL);
	client_database->SetParam("dbname", "%s", "advisor_client");

	if(client_database->Connect()== false){
		AVO_PRINT_DEBUG(funcName,"Error. Data Base Error");
		int result = QMessageBox::warning(this,tr("Data Base Error"),
				client_database->lastErrorStr() + tr(" Application will not save video. "),
				tr("  &Ignore error and continue  "), tr("  &Close application  "), "",0,1);
		AVO_PRINT_DEBUG(funcName,"result %d",result);
		if (result == 1){
			AVO_PRINT_DEBUG(funcName,"Data Base Error. Application closed by user");
			exit(0);
		}
	}

	videoViewer = new ViewerWidget(this);
	videoViewer->displayMessage(tr("Select a video by clicking on the \"Online Events Panel\" or by looking offline with the \"Alarms and Events\" panel"));

	splitter_event->addWidget(videoViewer);
	splitter_event->setStretchFactor(0,3);
	splitter_event->setStretchFactor(1,1);


	connect(videoViewer, SIGNAL(StatusMessage(const QString&,int)), ui.statusbar, SLOT(showMessage(const QString&,int)));

	connect(ui.actionConfig_Client_Parameters, SIGNAL(triggered()), this, SLOT(configClientParams()));
	connect(ui.actionSave_xml_file, SIGNAL(triggered()), this, SLOT(ServerSaveXMLFile()));
	connect(splitter, SIGNAL(splitterMoved(int, int)), this, SLOT(ResizeCameras(int, int)));

	ServerConnectedAgain();

	TimeTicker *tticker = new TimeTicker(this,true,true,true,false);
	connect(tticker,SIGNAL(completedDay(const QDateTime&)),this,SLOT(receivedDayTick(const QDateTime&)));
	connect(tticker,SIGNAL(completedHour(const QDateTime&)),this,SLOT(receivedDayTick(const QDateTime&)));
	connect(tticker,SIGNAL(completedQuater(const QDateTime&)),this,SLOT(receivedTick(const QDateTime&)));
	connect(tticker,SIGNAL(completedMinute(const QDateTime&)),this,SLOT(receivedTick(const QDateTime&)));

	SetUserMode(user_mode_t, false);

  }

Client_Visor::~Client_Visor()
{
	avoDebug("Client_Visor::~Client_Visor()") << " Start..";
	//X7S_DELETE_PTR(qboard);
	X7S_DELETE_PTR(serv_root);
	delete images_timeout;
	while(qcameras.size()){
		delete qcameras.last();
		qcameras.pop_back();
	}
	while(alrTime.size()){
		delete alrTime.last();
		alrTime.pop_back();
	}
	if(camEvtHandler)
		delete camEvtHandler;
	delete socket_client;
	delete videoViewer;
	delete eventList;
	delete splitter_event;
	delete splitter;
	delete warningList;
	avoDebug("Client_Visor::~Client_Visor()")<< " END";
}


void Client_Visor::setupSysTray()
{
	AVO_PRINT_DEBUG("Client_Visor::setupSysTray()");
	sysTrayIco = new AvoSysTrayIcon(this,"AVOClient");
}



void Client_Visor::fullScreen(bool full)
{
	if(full)
	{
		//Call the full screen function for QMainWindow
		this->showFullScreen();
		ui.statusbar->hide();
	}
	else
	{
		this->showNormal();
		ui.statusbar->show();
	}
}
void Client_Visor::SetDefaultView(){
	QList<int> sizes;
	sizes << 4*(PIXELS_PER_LINE + 10) << videoViewer->minimumWidth();
	splitter->setSizes(sizes);
	ResizeCameras(0, 1);
	RedrawCameras();
	sizes.clear();
	sizes << 1600 << videoViewer->minimumWidth();
	splitter_event->setSizes(sizes);

}
void Client_Visor::ServerSaveXMLFile()
{
	socket_client->askSaveXMLtoServer();
}

void Client_Visor::configClientParams()
{
	clientVisorParams = new setClientVisorParams(0, serv_root, &qcameras, user_mode == AVO_ADMIN_MODE);
	//	connect(visorParams, SIGNAL(SaveXML_FileSignal()), this, SLOT(saveXMLFile()));
	clientVisorParams->show();
	clientVisorParams->exec();

	delete clientVisorParams;
	writeSettings();
}


void Client_Visor::closeEvent(QCloseEvent *event)
{
	QMessageBox::StandardButton bu = QMessageBox::question(
			this,tr("Confirm Exit"),
			tr("Do you really want to close application ?"),
			QMessageBox::Ok | QMessageBox::Cancel,
			QMessageBox::Cancel);

	AVO_PRINT_DEBUG("Client_Visor::closeEvent()","bu=0x%08X OK=%d",(int)bu,bu == QMessageBox::Ok);

	if(bu == QMessageBox::Ok)
	{
		writeSettings();
		warningList->close();
		images_timeout->stop();
		event->accept();
	}
	else
	{
		event->ignore();
	}
}


void Client_Visor::writeSettings()
{
	QSettings settings;

	settings.setValue("mainwindow/state",saveState(AVO_SERVER_MAINWIN_STATE_VER));
	settings.setValue("mainwindow/geometry",saveGeometry());
	settings.setValue("server/ip_address", server_address);
	settings.setValue("alarms_window/visible", ui.actionShow->isChecked());
	settings.setValue("splitter/config", splitter->saveState());
	settings.setValue("graphics/numberOfColumms", numberOfColumms);

	//Config
	LogConfig *config = serv_root->logConfig();
	Devices *devices = serv_root->devices();
	settings.setValue("devices/video_path", QtBind::toQString(devices->GetParam("video_path")));
	settings.setValue("config/log_path", QtBind::toQString(config->GetParam("log_path")));
	settings.setValue("devices/video_path_isgeneral", devices->GetParam("video_path_isgeneral")->toIntValue());
	settings.setValue("devices/video_istowrite", devices->GetParam("video_istowrite")->toIntValue());
	settings.setValue("config/log", config->GetParam("log")->toIntValue());
	settings.setValue("config/log_cv", config->GetParam("log_cv")->toIntValue());
	settings.setValue("config/log_xml", config->GetParam("log_xml")->toIntValue());
	settings.setValue("config/log_vs", config->GetParam("log_vs")->toIntValue());
	settings.setValue("config/log_net", config->GetParam("log_net")->toIntValue());

	settings.setValue(QString("QCameras/WatchDogTime"),timeout_ms);

	//QCameras
	for(int j = 0; j < qcameras.size(); j++){
		if(qcameras[j]->isEnable())
		{
			settings.setValue(QString("QCameras/video_folder-%1").arg(qcameras[j]->getCamera()->GetSpecialID()),
					QtBind::toQString(qcameras[j]->getCamera()->GetParam("mjpeg_rootdir")));
			settings.setValue(QString("QCameras/mjpeg_write-%1").arg(qcameras[j]->getCamera()->GetSpecialID()),
					qcameras[j]->getCamera()->GetParam("mjpeg_write")->toIntValue());
			settings.setValue(QString("QCameras/isVisible-%1").arg(qcameras[j]->getCamera()->GetSpecialID()),
					qcameras[j]->IsDrawable());
			settings.setValue(QString("QCameras/ViewPrivacityAreas-%1").arg(qcameras[j]->getCamera()->GetSpecialID()),
					qcameras[j]->GetArePrivacityAreasShowed());
			settings.setValue(QString("QCameras/ViewSurveillanceData-%1").arg(qcameras[j]->getCamera()->GetSpecialID()),
					qcameras[j]->GetViewSurveillanceData());
			settings.setValue(QString("QCameras/PrivacityMode-%1").arg(qcameras[j]->getCamera()->GetSpecialID()),
					qcameras[j]->GetPrivacityMode());
			settings.setValue(QString("QCameras/ViewPixelatedImage-%1").arg(qcameras[j]->getCamera()->GetSpecialID()),
					qcameras[j]->GetIsImagePixelized());
			QVector<QPolygon> *privacity = qcameras[j]->GetPrivacityAreas();
			settings.setValue(QString("QCameras/Camera-%1").arg(qcameras[j]->getCamera()->GetSpecialID())
					+ "PrivacitySize", privacity->size());
			for(int i = 0; i < privacity->size(); i++)
			{
				settings.setValue(QString("QCameras/Camera-%1").arg(qcameras[j]->getCamera()->GetSpecialID())
						+ "Privacity" + QString::number(i), privacity->at(i));
			}

		}
	}
}
void Client_Visor::readSettings()
{
	QSettings settings;

	restoreState(settings.value("mainwindow/state").toByteArray(),AVO_SERVER_MAINWIN_STATE_VER);
	restoreGeometry(settings.value("mainwindow/geometry").toByteArray());


	ui.actionShow->setChecked(settings.value("alarms_window/visible", true).toBool());
	splitter->restoreState(settings.value("splitter/config").toByteArray());
	numberOfColumms = settings.value("graphics/numberOfColumms", 2).toInt();

	//Config
	Devices *devices = serv_root->devices();
	devices->SetParam("video_path", "%s", settings.value("devices/video_path", " ").toString().toStdString().c_str());
	devices->SetParam("video_path_isgeneral", "%d", settings.value("devices/video_path_isgeneral", 1).toInt());
	devices->SetParam("video_istowrite", "%d", settings.value("devices/video_istowrite", 1).toInt());

	LogConfig *config = serv_root->logConfig();
	config->SetParam("log_path", "%s", settings.value("config/log_path", " ").toString().toStdString().c_str());
	config->SetParam("log", "%d", settings.value("config/log", 2).toInt());
	config->SetParam("log_cv", "%d", settings.value("config/log_cv", 2).toInt());
	config->SetParam("log_xml", "%d", settings.value("config/log_xml", 2).toInt());
	config->SetParam("log_vs", "%d", settings.value("config/log_vs", 2).toInt());
	config->SetParam("log_net", "%d", settings.value("config/log_net", 2).toInt());

	config->Apply();
	timeout_ms = settings.value(QString("QCameras/WatchDogTime"), 10000).toInt();
	images_timeout->start(timeout_ms);

	//QCameras
	for(int j = 0; j < qcameras.size(); j++){
		if(qcameras[j]->isEnable())
		{
			qcameras[j]->getCamera()->SetParam("mjpeg_rootdir", "%s",
					settings.value(QString("QCameras/video_folder-%1").arg(qcameras[j]->getCamera()->GetSpecialID()), " ").toString().toStdString().c_str());

			qcameras[j]->getCamera()->SetParam("mjpeg_write", "%d",
					settings.value(QString("QCameras/mjpeg_write-%1").arg(qcameras[j]->getCamera()->GetSpecialID()), 1).toInt());
			qcameras[j]->setDrawable(settings.value(QString("QCameras/isVisible-%1").arg(qcameras[j]->getCamera()->GetSpecialID()), true).toBool());
			qcameras[j]->SetWatchDogTime(timeout_ms);
			QVector<QPolygon> *privacity = new QVector<QPolygon>();
			int psize = settings.value(QString("QCameras/Camera-%1").arg(qcameras[j]->getCamera()->GetSpecialID())
					+ "PrivacitySize", 0).toInt();
			for(int i = 0; i < psize; i++)
			{
				privacity->append(settings.value(QString("QCameras/Camera-%1").arg(qcameras[j]->getCamera()->GetSpecialID())
						+ "Privacity" + QString::number(i), QPolygon(0)).value<QPolygon>());
			}
			if (psize)
				qcameras[j]->AddPrivacityAreas(privacity);
			qcameras[j]->EnableShowPrivacityAreas(settings.value(QString("QCameras/ViewPrivacityAreas-%1").arg(qcameras[j]->getCamera()->GetSpecialID()), true).toBool());
			qcameras[j]->EnableViewSurveillanceData(settings.value(QString("QCameras/ViewSurveillanceData-%1").arg(qcameras[j]->getCamera()->GetSpecialID()), true).toBool());
			qcameras[j]->SetPrivacityMode(PrivacityModes(settings.value(QString("QCameras/PrivacityMode-%1").arg(qcameras[j]->getCamera()->GetSpecialID()), 0).toInt()));
			qcameras[j]->EnableImagePixelized(settings.value(QString("QCameras/ViewPixelatedImage-%1").arg(qcameras[j]->getCamera()->GetSpecialID()), true).toBool());
		}
	}
}

void Client_Visor::RefreshAlarmTime(int index)
{
	if (index == 1){
		setCursor(Qt::WaitCursor);
		for(int i = 0; i < alrTime.size(); i++)
			alrTime[i]->refresh();
		setCursor(Qt::ArrowCursor);
	}
}

void Client_Visor::ShowAlarmsWindows(bool enable){

	if (enable){
		splitter_event->show();
		//ui.actionVertical->setEnabled(true);
		if (numberOfColumms < 4){
			QList<int> sizes;
			sizes << numberOfColumms*(PIXELS_PER_LINE + 10) << 500;
			splitter->setSizes(sizes);
		}
		else{
			QList<int> sizes;
			sizes << numberOfArrows * (LINES_PER_FRAME + 7) << 500;
			splitter->setSizes(sizes);
		}
	}
	else{
		splitter_event->hide();
		ui.actionVertical->setEnabled(false);
	}
}

void Client_Visor::ResizeCameras(int pos, int index)
{

	X7S_FUNCNAME("Client_Visor::ResizeCameras()");

	if(index != 1)
		return;
/*	QList<int> size = splitter->sizes();
	//	avoDebug(funcName) << "ResizeCameras:" << size;
	quint32 size0 = size.at(0)- 50;
	quint32 size_c = 0;
	numberOfColumms = 0;
	for(int j = 0; j < qcameras.size(); j++){
		if(qcameras[j]->IsDrawable())
			size_c += qcameras[j]->GetQCameraSize().width();
		if (size_c <= size0)
			numberOfColumms++;
		else
			break;
	}
	if (numberOfColumms == 0)
		numberOfColumms++;*/
	/*
	if (splitter->orientation() == Qt::Horizontal){
		if (numberOfColumms != (size0/(PIXELS_PER_LINE + 5))){
			numberOfColumms = (size0/(PIXELS_PER_LINE + 5));
			if (numberOfColumms == 0)
				numberOfColumms++;
			//avoDebug(funcName) << size.at(0) << numberOfColumms;
		}
		else return;
	}
	else
	{
		if (numberOfColumms != (quint32)(this->size().width()/(PIXELS_PER_LINE + 5))){
			numberOfColumms = (this->size().width()/(PIXELS_PER_LINE + 5));
			if (numberOfColumms == 0)
				numberOfColumms++;
			//avoDebug(funcName) << size.at(0) << numberOfColumms;
		}
		else return;
	}*/
	avoDebug(funcName) << "ResizeCameras():: number of Columms: " << numberOfColumms;
	RedrawCameras();

}

void Client_Visor::ChangeAlarmsWindowOrientation(bool enable)
{
	if (enable){
		splitter->setOrientation(Qt::Horizontal);
	}
	else{
		splitter->setOrientation(Qt::Vertical);
	}
	RedrawCameras();
}

void Client_Visor::RedrawCameras()
{
	if (qcameras_gridLayout)
		delete qcameras_gridLayout;
	qcameras_gridLayout = new QGridLayout(ui.cameras);
	qcameras_gridLayout->setObjectName(QString::fromUtf8("qcameras_gridLayout"));

	if (events_gridLayout)
		delete events_gridLayout;
	events_gridLayout = new QGridLayout(ui.events);
	events_gridLayout->setObjectName(QString::fromUtf8("events_gridLayout"));

	quint32 x = 0, y = 0;
	QList<int> size = splitter->sizes();
	quint32 size0 = size.at(0)- 50;
	quint32 size_c = 0;

	for(int j = 0; j < qcameras.size(); j++){
		if(qcameras[j]->IsDrawable())
		{
			size_c += qcameras[j]->GetQCameraSize().width();
			if (size_c < size0){
				qcameras_gridLayout->addWidget(qcameras[j], y, x);
				events_gridLayout->addWidget(alrTime[j], y, x++);
			}
			else
			{
				size_c = qcameras[j]->GetQCameraSize().width();
				numberOfColumms = x;
				x = 0;
				qcameras_gridLayout->addWidget(qcameras[j], ++y, x);
				events_gridLayout->addWidget(alrTime[j], y, x++);
			}

			qcameras[j]->show();
			alrTime[j]->show();

		}
		else{
			qcameras[j]->hide();
			alrTime[j]->hide();
		}
	}

	/**********************************************/
/*	for(int j = 0; j < qcameras.size(); j++){
		if(qcameras[j]->IsDrawable())
		{
			if (x >= numberOfColumms){
				x = 0;
				y++;
			}
			qcameras[j]->show();
			qcameras_gridLayout->addWidget(qcameras[j], y-1, x);
			events_gridLayout->addWidget(alrTime[j], y-1, x);
			alrTime[j]->show();
			x++;
		}
		else{
			qcameras[j]->hide();
			alrTime[j]->hide();
		}
	}
*/
	numberOfArrows = y;
	//    qcameras_groupBox->setGeometry(QRect(7, 7, numberOfColumms*(PIXELS_PER_LINE + 7), numberOfArrows * (LINES_PER_FRAME + 7)));
	//qcameras_gridLayout->setGeometry(QRect(10, 10, numberOfColumms*(PIXELS_PER_LINE + 7), numberOfArrows * (LINES_PER_FRAME + 7)));
	//events_gridLayout->setGeometry(QRect(10, 10, numberOfColumms*(PIXELS_PER_LINE + 7), numberOfArrows * (LINES_PER_FRAME + 7)));

}
void Client_Visor::ServerConnectionError()
{
	if(qcameras.size()==0)
		return;
	for(int j = 0; j < qcameras.size(); j++){
		if(qcameras[j]->GetCameraSocketConnected())
			return;
	}
	writeSettings();
	//X7S_DELETE_PTR(serv_root);
	while(qcameras.size()){
		delete qcameras.last();
		qcameras.pop_back();
	}
	while(alrTime.size()){
		delete alrTime.last();
		alrTime.pop_back();
	}
	delete camEvtHandler;
	camEvtHandler = 0;
	avoDebug("Client_Visor::ServerConnectionError()")<< " All QCameras deleted.";
}
void Client_Visor::ServerConnectedAgain()
{
	QSettings settings;

	if(qcameras.size()!=0)
		return;

	socket_client->askXMLFileToServer();

	avoDebug("Client_Visor::ServerConnectedAgain()") << " askXMLFileToServer()";
	//Esperamos a que llegue el fichero

	if(socket_client->isXMLFileReceived() == false)
	{
		avoFatal("Client_Visor::ServerConnectedAgain()") << "Error. XML configuration file was not received. Closing Application";
	}
	else
		avoDebug("Client_Visor::ServerConnectedAgain()") << "XML configuration file is received";


	numberOfColumms = settings.value("graphics/numberOfColumms", 2).toInt();

	QString tmpFileName = socket_client->getFileName();
	if(tmpFileName.isEmpty())
	{
		avoFatal("Client_Visor::ServerConnectedAgain()") << "XML configuration file could not be created";
	}


	X7S_DELETE_PTR(serv_root);
	serv_root = new Root(tmpFileName);
	if(serv_root->LoadFile()==false)
	{
		avoFatal("Client_Visor::ServerConnectedAgain()") << "Bad XML format! Can not load XML file. Closing Application";
	}

	Devices *devices=serv_root->devices();
	if(devices->GetSize()==0)
	{
		avoError("Client_Visor::ServerConnectedAgain()") << "No board found in devices (loading from XML file)";
	}

	//--------------------- Create the CamEvtHandler
	serv_root->videoHandler()->AttachDatabase(client_database);
	camEvtHandler = new CamEvtHandler(client_database,serv_root->videoHandler());
	connect(camEvtHandler, SIGNAL(eventVSAlarmReceived(const CamEvtCarrier&)), eventList, SLOT(AddEvent(const CamEvtCarrier&)));
	connect(camEvtHandler, SIGNAL(eventVSAlarmReceived(const CamEvtCarrier&)), this, SLOT(UpdateQCameraFixedImage(const CamEvtCarrier&)));

	/**********************************************/
	for (int i = 0; i < devices->GetSize(); i++){
		Board *board_ptr = devices->GetBoard(i);
		int board_id = board_ptr->GetID();
		if(board_ptr)
		{
			Camera *camera;
			for(int j = 0; j < 4; j++){
				camera = board_ptr->GetCamera(j);
				qcameras.push_back(new QCameraClient(ui.cameras, camera, 4*board_id + j));
				connect(qcameras.last(), SIGNAL(cameraEnabled(bool, int)), this, SLOT(RedrawCameras()));
				connect(qcameras.last(), SIGNAL(cameraVisible(bool, int)), this, SLOT(RedrawCameras()));
				connect(qcameras.last(), SIGNAL(warningOnCamera(const QString&)), warningList, SLOT(AddWarning(const QString &)));
				connect(qcameras.last(),SIGNAL(notificationOnCamera(const QString&)),sysTrayIco,SLOT(notifyInfo(const QString &)));
				connect(images_timeout, SIGNAL(timeout()),qcameras.last(), SLOT(TimeoutError()));
				qcameras.last()->ClientSocketLog(name, password, quint32(server_address));
				qcameras.last()->SetAdminMode(admin_mode);
//				qcameras.last()->EnablePrivacityMode(privacity_mode);

				alrTime.push_back(new AlrTimeWidget(ui.events, client_database, camera));
				connect(alrTime.last(), SIGNAL(EventSelectedSignal(const CamEvtCarrier&, bool)), this, SLOT(EventSelected(const CamEvtCarrier&, bool)));
				connect(this,SIGNAL(playingEvt(const CamEvtCarrier&,bool)),alrTime.last(),SLOT(eventClicked(const CamEvtCarrier&,bool)));
				connect(qcameras.last(),SIGNAL(newFrameReceived(Camera *)),camEvtHandler,SLOT(processNewFrame(Camera *)));
			}
		}
	}
	readSettings();
	RedrawCameras();

}
/**
* @brief Process an event from the EventList or AlrTimeWidget and open its corresponding video.
*
* @note If we use a double-click event this function is called twice (one with play=false, and a second one with play=true).
* Therefore we should check if the event is already running.
*
* @param alrEvt The Camera event that we need to proceed.
*/
bool Client_Visor::EventSelected(const AVOAlrEvt& alrEvt, bool play)
{
	bool ret;
	X7S_FUNCNAME("Client_Visor::EventSelected()");
	AVO_CHECK_WARN(funcName,videoViewer,false,"VideoViewer is NULL");
	Database *db = client_database;


	QDateTime time= QDateTime::fromTime_t(alrEvt.t);
	avoDebug(funcName)<< "Start"  << time << alrEvt.cam << alrEvt.blob_ID << play;

	//First check if we have clicked on the same alarm
	if(AVO_STRUCT_EQUAL(alrEvt,videoViewer->getLastAlrEvt()))
	{
		//Do nothing
		avoDebug(funcName) << "Click on same event... do nothing";
	}
	else
	{
		//Check if the alrEvt correspond to video already opened
		const VideoInFile& vid = videoViewer->getVideo();
		if(vid.isValid() && vid.hasAlrEvent(alrEvt))
		{
			if(!vid.isOpened()) {
				ret = videoViewer->Open(videoViewer->getVideo());
				if(ret==false) return false;
			}
		}
		//Otherwise search a video in the database.
		else
		{
			VideoInFile videofile = db->GetVideo(alrEvt.cam,time);
			ret = videoViewer->Open(videofile);
			if(ret==false) return false;
		}

		//Then go to the correct frame.
		videoViewer->JumpToAlarm(alrEvt);
		videoViewer->displayFrame();
	}
	if(play) videoViewer->PlayPause(true);
	avoDebug(funcName) << "End" << time << alrEvt.cam << play;
	return true;

}

/**
* @brief Communicate that an event has been selected
*/
bool Client_Visor::EventSelected(const CamEvtCarrier& camEvt, bool play)
{
	X7S_FUNCNAME("Visor::EventSelec(camEvt)");
	avoDebug(funcName);
	bool ret=false;

	//Check if the CamEvtCarrier is from the good type.
	if(camEvt.isType(CamEvt::VSAlarm))
	{
		CamEvtVSAlarm *camEvtAlr=(CamEvtVSAlarm*)camEvt.getCamEvt();

		//Copy the new camAlrEvt to the old struct
		AVOAlrEvt avoAlrEvt;
		avoAlrEvt.t=camEvtAlr->getTime_t();
		avoAlrEvt.cam=camEvtAlr->getCamera();
		AVO_STRUCT_COPY(avoAlrEvt, camEvtAlr->vsAlrEvt);

		ret=this->EventSelected(avoAlrEvt,play);
	}
	else
	{
		avoWarning(funcName) << "Event is not of the correct type";
	}

	//Emit that the event open a file to be played.
	emit playingEvt(camEvt,ret);

	return ret;
}

void Client_Visor::receivedTick(const QDateTime& actual)
{
	avoDebug("Client_Visor::receivedTick()") << actual;

}

void Client_Visor::receivedDayTick(const QDateTime& actual)
{
	avoInfo("Client_Visor::receivedDayTick()") << actual;
	LogConfig* logConf=NULL;
	if(serv_root) logConf = serv_root->logConfig();
	if(logConf) logConf->UpdateLogFiles();
}


void Client_Visor::SetUserMode(ClientUsersMode mode, bool access)
{
	int result = 0;
	if ((mode < user_mode) && access){
		if (mode == AVO_ADMIN_MODE)
			ui.actionEnter_Admin_mode->setChecked(false);
		else
			ui.actionEnter_Privacity_Mode->setChecked(false);
		AuthenticationDialog accessDialog(0, &name, &password);
		accessDialog.show();
		result = accessDialog.exec();
		avoDebug("Client_Visor::SetUserMode()") << quint32(result) << name << password;
		if (result == 0){
			return;
		}
		socket_client->askServerToLog(name, password);
		result = socket_client->getUserLevel();
		if(result == -1)//No logged
		{
			QMessageBox::information(this,tr("User Error"),
					tr("User or Password not correct"),
					QMessageBox::Ok|QMessageBox::Default,
					QMessageBox::NoButton, QMessageBox::NoButton);
			return;
		}
		if((ClientUsersMode)result > mode)//No correct privilege
		{
			QMessageBox::information( this, tr("Command Error"),tr("You don't have correct privileges. Contact to your security manager."),
					QMessageBox::Ok|QMessageBox::Default,
					QMessageBox::NoButton, QMessageBox::NoButton);
			return;
		}
		for(int i = 0; i < qcameras.size(); i++){
			qcameras[i]->ChangeUser(name, password);
		}
	}

	user_mode = mode;

	switch (mode){
	case (AVO_ADMIN_MODE):
		ui.menuFile->setEnabled(true);
		//ui.menuPreference->setEnabled(enable);
		ui.actionConfig_Client_Parameters->setEnabled(true);
		ui.actionEnter_Admin_mode->setChecked(true);
		ui.actionEnter_Privacity_Mode->setChecked(false);
		ui.actionEnter_Visor_Mode->setChecked(false);
		for(int i = 0; i < qcameras.size(); i++){
			qcameras[i]->SetAdminMode(true);
//			qcameras[i]->EnablePrivacityMode(true);
		}
		break;
/*	case (AVO_PRIVACITY_MODE):
		socket_client->exitAdminMode();
		ui.menuFile->setEnabled(false);
		//ui.menuPreference->setEnabled(enable);
		ui.actionConfig_Client_Parameters->setEnabled(true);
		ui.actionEnter_Admin_mode->setChecked(false);
		ui.actionEnter_Privacity_Mode->setChecked(true);
		ui.actionEnter_Visor_Mode->setChecked(false);
		for(int i = 0; i < qcameras.size(); i++){
			qcameras[i]->SetAdminMode(false);
			qcameras[i]->EnablePrivacityMode(true);
		}
		break;*/
	case (AVO_VISOR_MODE):
		socket_client->exitAdminMode();
		ui.menuFile->setEnabled(false);
		//ui.menuPreference->setEnabled(enable);
		ui.actionConfig_Client_Parameters->setEnabled(false);
		ui.actionEnter_Admin_mode->setChecked(false);
		ui.actionEnter_Privacity_Mode->setChecked(false);
		ui.actionEnter_Visor_Mode->setChecked(true);
		for(int i = 0; i < qcameras.size(); i++){
			qcameras[i]->SetAdminMode(false);
//			qcameras[i]->EnablePrivacityMode(false);
		}
		break;
	default:
		break;
	}
}


void Client_Visor::UpdateQCameraFixedImage(const CamEvtCarrier &evt)
{
	Camera *cam = (Camera*)evt.getCamEvt()->getCamera();
	for(int i = 0; i < qcameras.size(); i++){
		if(qcameras[i]->getCamera() == cam){
			qcameras[i]->ActualizeFixedImage();
			return;
		}

	}
}


void Client_Visor::EnableAdminMode()
{
	SetUserMode(AVO_ADMIN_MODE, true);
}
/*
void Client_Visor::EnablePrivacityMode()
{
	SetUserMode(AVO_PRIVACITY_MODE, true);
}
*/
void Client_Visor::EnableVisorMode()
{
	SetUserMode(AVO_VISOR_MODE, false);
}
