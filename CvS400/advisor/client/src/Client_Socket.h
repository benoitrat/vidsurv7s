#ifndef CLIENT_SOCKET_H
#define CLIENT_SOCKET_H

//#include <QtGui/QWidget>
#include <QImage>
#include <QMutex>
#include <QWaitCondition>
#include <QTcpSocket>
#include <QtNetwork>
#include <QHostAddress>
#include <QString>
#include <QTimer>

#include "vv_defines.h"

#include "Connecting_Dialog.h"

class Client_Socket : public QTcpSocket
{
    Q_OBJECT

public:
    Client_Socket(QObject *parent = 0, quint32 ip_add = 0x70000001);
    ~Client_Socket();

private:
//    Ui::clientSocket_Class ui;

public:
	bool isXMLFileReceived(){return xml_file_received;};

	void ExecutePlay();
	void StopPlay();
	void Close();
	void sendPlayToServer();
	void sendStopToServer();
	void askImageToServer(int camera = 0);
	void sendEnableCameraToServer(bool enable, int camera = 0x0);
	void sendEnableSurveillanceToServer(bool enable, int camera = 0x0);
	void askXMLFileToServer();
	void askSaveXMLtoServer();
	void askServerToLog(const QString &name, const QString &password);
	void exitAdminMode();

	QString getFileName() const { return tmpFile.fileName(); }

	bool getServerConnected(){ return server_connected;}
	bool getServerLogged(){ return logged;}
	qint8 getUserLevel(){return user_level;}
	QString& getUserName(){return user_name;}
	bool Connect();
	void setIP_Address(quint32 server_address = 0x7F000001 ){serverAddress.setAddress(server_address);}
	void Abort_Connect(){abort();}

signals:
	void warningOnSocket(const QString &warning);
	void ServerConnectionError();
	void ServerConnectedAgain();

protected slots:
//	void enableCamera(bool enable, int camera);
	void dataReceivedFromServer();
	void serverConnected();
	void error();
	void sendNewAlarmsToServer(const QByteArray &alarm_array, quint8 camera_index);
	void Reconnect();


private:
    bool in_progress;
    bool closing;
    bool play_image;
    bool thread_run;
    bool server_connected;
    bool xml_file_received;
    bool logged;
    bool camerasEnabled[4];
//    QTcpSocket tcpSocket;
//	QWaitCondition dataIsArrived;
	QWaitCondition xmlFileReceived;
	QTemporaryFile tmpFile;
	QMutex mutex;
	quint32 nextBlockSize;
	QByteArray qmetaJPEG;
	QHostAddress serverAddress;
	Connecting_Dialog *connectingDialog;
	QString user_name;
	QString user_password;
	qint8  user_level;
	QTimer *timer;
};

#endif // CLIENT_SOCKET_H
