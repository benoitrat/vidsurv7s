/*
 * setCameraParams.h
 *
 *  Created on: 08/09/2009
 *      Author: Gabri
 */

#ifndef SETCLIENTCAMERASPARAMS_H_
#define SETCLIENTCAMERASPARAMS_H_

#include <QDialog>
#include "ui_setClientCamerasParams.h"
#include <x7svidsurv.h>
#include <QPainter>
#include <QImage>
#include <QPixmap>
#include <QString>

class EditQLabel;
class QCameraClient;
class Camera;

class setClientCamerasParams : public QDialog
{
    Q_OBJECT

public:
    setClientCamerasParams(QDialog *parent = 0, QCameraClient *qcamera_pt = 0);
    ~setClientCamerasParams();

    //Ui::setClientCamerasParamsClass ui;

public slots:
    void setImageToLabel();
    void ApplyNewValues();
    void ApplyNewValuesAndClose();
    void ShowPushButtons();
    void VideoPathIsGeneral(bool general);
    void WriteJPEGIsGeneral(bool general);
    void VideoSurveillanceEnabled(bool enable);



private:
    Ui::setClientCamerasParamsClass ui;

	QCameraClient 		*qcamera;
	Camera 			*camera;

	QImage 			image;
	QString 		videoPath;

	int bg_ccMinArea;
	int bg_insDelayLog;
	int bg_refBGDelay;
	int bg_refFGDelay;
	int bg_threshold;

	X7sVSAlrManager *alrMgr;
private slots:

	void BrowseVideoPath();
//	void ApplyCameraName();
	void DeleteAlarm();
//	void EnableNamePushButton();
	void ApplyNewBG_Values();
	void EnableAlarmPushButtons();

};

#endif /* SETCLIENTCAMERASPARAMS_H_ */
