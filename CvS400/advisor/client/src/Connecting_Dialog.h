/*
 * Connecting_Dialog.h
 *
 *  Created on: 01/09/2009
 *      Author: Gabri
 */

#ifndef CONNECTING_DIALOG_H_
#define CONNECTING_DIALOG_H_



#include <QtGui/QDialog>
#include <QHostAddress>
#include <QTcpSocket>
#include <QTimer>

#include "vv_defines.h"

#include "ui_connecting_dialog.h"

class Connecting_Dialog : public QDialog
{
    Q_OBJECT

public:
    Connecting_Dialog(QTcpSocket *tcpSocket = 0, QHostAddress *serv_add = 0);
    ~Connecting_Dialog();
    void Connecting();
//	void Connect();
//	void Connected();

private:
    Ui::Connecting_Dialog_Class ui;

    QTcpSocket *tcp_Socket;
    QHostAddress *server_add;
    bool connected;

protected slots:

	void Connected();
	void Abort();
	void Connect();

private:
	QTimer *timer;


};


#endif /* CONNECTING_DIALOG_H_ */
