#include "AvoSysTrayIcon.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include <QMenu>
#include "Log.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
AvoSysTrayIcon::AvoSysTrayIcon(QWidget *mainWindow, const QString& title)
:QSystemTrayIcon(mainWindow),
 m_mainWindow(mainWindow), m_title(title), m_msTimeOut(4000)
 {
	avoDebug("AvoSysTrayIcon::AvoSysTrayIcon()");

	if (!QSystemTrayIcon::isSystemTrayAvailable())
	{
		avoError("AvoSysTrayIcon::AvoSysTrayIcon()") <<  "I couldn't detect any system tray on this system";
	}

	if(m_mainWindow) this->setIcon(mainWindow->windowIcon());
	this->setToolTip(title);
	this->createMenu();
	this->setVisible(true);





 }

AvoSysTrayIcon::~AvoSysTrayIcon()
{

}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------
void AvoSysTrayIcon::notifyInfo(const QString& msg)
{
	avoInfo("AvoSysTrayIcon::notify()") << msg;
	this->notify(msg,QSystemTrayIcon::Information);

}

void AvoSysTrayIcon::notifyWarning(const QString& msg)
{
	avoWarning("AvoSysTrayIcon::notify()") << msg;
	this->notify(msg,QSystemTrayIcon::Warning);
}

void AvoSysTrayIcon::notify(const QString& msg, const MessageIcon& msgIcon)
{
	this->showMessage(m_title,msg,msgIcon, m_msTimeOut);
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------
void AvoSysTrayIcon::createMenu()
{
	if(m_mainWindow)
	{
		QAction * minimizeAction = new QAction(tr("Mi&nimize"), this);
		connect(minimizeAction, SIGNAL(triggered()), m_mainWindow, SLOT(hide()));

		QAction *  maximizeAction = new QAction(tr("Ma&ximize"), this);
		connect(maximizeAction, SIGNAL(triggered()), m_mainWindow, SLOT(showMaximized()));

		QAction *  restoreAction = new QAction(tr("&Restore"), this);
		connect(restoreAction, SIGNAL(triggered()), m_mainWindow, SLOT(showNormal()));

		QAction * quitAction = new QAction(tr("&Quit"), this);
		connect(quitAction, SIGNAL(triggered()), m_mainWindow, SLOT(close()));

		QMenu* trayIconMenu = new QMenu(m_mainWindow);
		trayIconMenu->addAction(minimizeAction);
		trayIconMenu->addAction(maximizeAction);
		trayIconMenu->addAction(restoreAction);
		trayIconMenu->addSeparator();
		trayIconMenu->addAction(quitAction);

		this->setContextMenu(trayIconMenu);
	}

}

