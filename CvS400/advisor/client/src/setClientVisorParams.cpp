/*
 * setBoardParams.cpp
 *
 *  Created on: 08/09/2009
 *      Author: Gabri
 */
#include "Log.hpp"
#include "setClientVisorParams.h"
#include "setClientCamerasParams.h"
#include "setClientConfigParams.h"
#include "QCameraClient.h"
#include "Camera.hpp"
#include "QtBind.hpp"
#include "Root.hpp"
#include "Devices.hpp"
#include "LogConfig.hpp"
#include "VideoHandlerScheduleWidget.h"
#include <QIcon>


setClientVisorParams::setClientVisorParams(QDialog *parent, Root *root_p, QVector<QCameraClient *> *qcameras_pt, bool admin_mode)
    : QDialog(parent)
{
	ui.setupUi(this);

	root = root_p;
	qcameras = qcameras_pt;
	admin = admin_mode;

	//Attach the database for all the camera.
	int index_stack = 0;
	if (admin_mode)
		configParams = new setClientConfigParams(this, root);
	for(int i=0;i<qcameras->size();i++)
	{
		cameraParams.append(new setClientCamerasParams(0, qcameras->at(i)));
		QListWidgetItem *item = new QListWidgetItem( QIcon(":/images/Camera_Icon_24.PNG"), qcameras->at(i)->getCamera()->GetName());
		//ui.listWidget->addItem(tr("-Camera: ") + qcameras->at(i)->getCamera()->GetName());
		ui.listWidget->insertItem(i,item);
		cameraParams.at(i)->VideoPathIsGeneral(root->devices()->GetParam("video_path_isgeneral")->toIntValue() == 1);
		cameraParams.at(i)->WriteJPEGIsGeneral(root->devices()->GetParam("video_istowrite")->toIntValue() == 1);
		if (admin_mode){
			connect(configParams, SIGNAL(VideoPathIsGeneralSignal(bool)), cameraParams.at(i), SLOT(VideoPathIsGeneral(bool)));
			connect(configParams, SIGNAL(WriteJPEGIsGeneralSignal(bool)), cameraParams.at(i), SLOT(WriteJPEGIsGeneral(bool)));
		}
		avoDebug("setClientVisorParams::setClientVisorParams()") <<"Stack Camera Index:"<< ui.stackedWidget->insertWidget(index_stack++, cameraParams.at(i));
	}
	if (admin_mode){
		QListWidgetItem *item_config = new QListWidgetItem( QIcon(":/images/24-settings-blue.png"), tr("Config"), ui.listWidget);
		ui.stackedWidget->insertWidget(index_stack++, configParams);

		video_schdler = new VideoHandlerScheduleWidget(this, root->videoHandler());
		QListWidgetItem *item_video_sch = new QListWidgetItem( QIcon(":/images/24-columns.png"), tr("Video Scheduler"), ui.listWidget);

		ui.stackedWidget->insertWidget(index_stack++, video_schdler);
	}
	connect(ui.listWidget, SIGNAL(currentRowChanged(int)),ui.stackedWidget, SLOT(setCurrentIndex(int)));
	ui.listWidget->setCurrentRow(0);

	connect(ui.apply_pushButton, SIGNAL(clicked()), this, SLOT(ApplyNewValues()));
	connect(ui.OK_pushButton, SIGNAL(clicked()), this, SLOT(ApplyNewValuesAndClose()));

}

setClientVisorParams::~setClientVisorParams()
{
	for(int i=0;i<cameraParams.size();i++)
	{
		if(cameraParams[i]) delete cameraParams[i];
	}
	if (admin){
		delete configParams;
		delete video_schdler;
	}
}

void setClientVisorParams::ApplyNewValues()
{
	for(int i=0;i<cameraParams.size();i++)
	{
		if(cameraParams[i])
			cameraParams.at(i)->ApplyNewValues();
	}
	if (admin){
		configParams->ApplyNewValues();
		video_schdler->applyParam();
	}
}
void setClientVisorParams::ApplyNewValuesAndClose()
{
	for(int i=0;i<cameraParams.size();i++)
	{
		if(cameraParams[i])
			cameraParams.at(i)->ApplyNewValues();
	}
	if (admin){
		configParams->ApplyNewValues();
		video_schdler->applyParam();
	}
	done(0);
}
