#include "CameraClient_Socket.h"

#include <QHostAddress>
#include <QtNetwork>
#include <QMessageBox>
#include <QTextStream>
#include <QFile>
#include <QThread>
#include <QDataStream>
#include <QAbstractSocket>
#include "vv_defines.h"
#include "QCameraClient.h"
#include "Camera.hpp"
#include "Log.hpp"

#define IX7S_SIZEIB_MJPEG_HDR 	65

CameraClient_Socket::CameraClient_Socket(QCameraClient *qcam, quint32 ip_add)
	: QTcpSocket(qcam)
{
	server_connected = false;

	play_image = false;

	logged = false;

	closing = false;

	qcamera = qcam;

	port_index = qcamera->GetCameraIndex();

	nextBlockSize = 0;

	camerasEnabled = false;

	serverAddress = QHostAddress(ip_add);

	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(Reconnect()));


    connect(this, SIGNAL(connected()), this, SLOT(serverConnected()));
    connect(this, SIGNAL(disconnected()), this, SLOT(error()));
    connect(this, SIGNAL(readyRead()), this, SLOT(dataReceivedFromServer()));
//    connect(&tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error(QAbstractSocket::SocketError)));

    //Connect();
}

CameraClient_Socket::~CameraClient_Socket()
{
	X7S_FUNCNAME("CameraClient_Socket::~CameraClient_Socket()");

	avoDebug(funcName) << "CameraClient_Socket " << port_index << " clossing...";
	timer->stop();
	delete timer;
	if (server_connected){
		server_connected = false;
		closing = true;
		close();
	}
	avoDebug(funcName) << "CameraClient_Socket" << port_index << " closed.";
}

bool CameraClient_Socket::Connect()
{
	X7S_FUNCNAME("CameraClient_Socket::Connect()");

	int timeout = 2;
	while(timeout)
	{
		connectToHost(serverAddress, quint16(6180 + port_index));
		avoDebug(funcName) << "Connecting...";
		if (waitForConnected(1000))
			return true;
		timeout--;
		if (timeout == 0)
			return false;
	}
	avoDebug(funcName) << "Error connecting";
	return false;
}

void CameraClient_Socket::serverConnected()
{
	server_connected = true;
	avoDebug("CameraClient_Socket::serverConnected()") << "CameraClient_Socket " << port_index << " Connected";
//	connectingDialog->Connected();
}



void CameraClient_Socket::Close()
{
	if (server_connected){
		server_connected = false;
		avoDebug("CameraClient_Socket::Close()") << "CameraClient_Socket " << port_index << " Closed";
		close();
	}

}

void CameraClient_Socket::askServerToLog(const QString &name, const QString &password)
{
	logged = false;

	QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('U') << quint8(AVO_SCOMMAND_USER_LOG);

    avoDebug("CameraClient_Socket::askServerToLog()") << "User: " << name << " Password: " << password;
    user_name = name;
    user_password = password;

    QCryptographicHash *hash = new QCryptographicHash(QCryptographicHash::Sha1);
	QByteArray data = QByteArray();
	data.append(user_password);
	hash->addData(data);
	QString hash_result = QString(hash->result().toHex());
    out << name << hash_result;

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);
    waitForReadyRead(3000);
}

void CameraClient_Socket::exitAdminMode()
{

/*	if ((logged == false)||(isValid()==false))
    	return;*/
	QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('U') << quint8(AVO_SCOMMAND_USER_NO_ADMIN);

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));

    write(block);

}


void CameraClient_Socket::askImageToServer()
{

/*	if (isValid()==false)
    	return;*/
//	avoDebug("CameraClient_Socket::askImageToServer()") << "askImageToServer: " << port_index;
	QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('C') << quint8(AVO_SCOMMAND_SEND_IMAGE);

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));

    write(block);


//    statusLabel->setText(tr("Sending request..."));
}

void CameraClient_Socket::sendEnableCameraToServer(bool enable)
{
    camerasEnabled = enable;
	QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    quint8 command;
    if (enable)
    	command = AVO_SCOMMAND_ENABLE_CAM;
    else
    	command = AVO_SCOMMAND_DISABLE_CAM;

    out << quint32(0) << quint8('C') << quint8(command);

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);

//    statusLabel->setText(tr("Sending request..."));
}

void CameraClient_Socket::sendEnableSurveillanceToServer(bool enable)
{
	QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    quint8 command;
    if (enable)
    	command = AVO_SCOMMAND_ENABLE_SURVEILLANCE;
    else
    	command = AVO_SCOMMAND_DISABLE_SURVEILLANCE;

    out << quint32(0) << quint8('C') << quint8(command);

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);

//    statusLabel->setText(tr("Sending request..."));
}

void CameraClient_Socket::sendNewAlarmsToServer(const QByteArray &alarm_array)
{
	QByteArray block;
	QByteArray array = QByteArray(alarm_array);
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);

    out << quint32(0) << quint8('C') << quint8(AVO_SCOMMAND_SEND_NEW_ALARMS) << array;

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);

//    statusLabel->setText(tr("Sending request..."));
}

void CameraClient_Socket::sendDeleteAlarmToServer(int ID)
{
	QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);

    out << quint32(0) << quint8('C') << quint8(AVO_SCOMMAND_SEND_DELETE_ALARM) << quint8(ID);

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);

}


void CameraClient_Socket::SetParamCamera(const QString &param, const QString &value)
{
	QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);

    out << quint32(0) << quint8('C') << quint8(AVO_SCOMMAND_SET_PARAM) << param << value;

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);

//    statusLabel->setText(tr("Sending request..."));
}

void CameraClient_Socket::dataReceivedFromServer()
{
	X7S_FUNCNAME("CameraClient_Socket::dataReceivedFromServer()");

    quint32 bytes_Available;

 	QString camStr;
    QByteArray alarm_array;
    QByteArray string_array;
	QString param;
	QString value;

	bool enableq;
// 	QFile *tmp;
// 	static quint32 i=0;


	QDataStream in(this);
    in.setVersion(QDataStream::Qt_4_3);
   // QTextStream *out;
    forever{
		if (nextBlockSize == 0) {
			bytes_Available = bytesAvailable();
	         if (bytesAvailable() < sizeof(quint32))
	             break;
	         in >> nextBlockSize;
	     }
		bytes_Available = bytesAvailable();
	     if (bytesAvailable() < nextBlockSize)
	         break;
	     bytes_Available = bytesAvailable();
	     nextBlockSize = 0;
	     quint8 command_type;
	     quint8 command;

	     in >> command_type;
	     in >> command;

	     switch(command_type){
		     case ('B'):
		    	 break;
		     case ('U'):
		    	 if (command == AVO_SCOMMAND_USER_LOGGED){
		    		 in >> logged;
		    		 avoDebug(funcName) << "Client Camera: "<< port_index <<" logged:" << logged;
		    		 avoDebug(funcName) << "Client Camera logged:" << logged;
		    	 }
				 if (command == AVO_SCOMMAND_PERMISSION_DENIED){
					 QMessageBox::information( 0, tr("Command Error"),tr("You don't have admin privileges. Contact to your security manager."),
							 QMessageBox::Ok|QMessageBox::Default,
							 QMessageBox::NoButton, QMessageBox::NoButton);
				 }
		    	 break;
		     case('C'):
		    	 switch(command){
					 case(AVO_SCOMMAND_SEND_NEW_ALARMS):
						//QByteArray alarm_array;
						quint32 size;// = alarm_array.size();
						in >> alarm_array;
						size = alarm_array.size();
						avoDebug(funcName)<< "new Alarm received from Server";
						qcamera->addNewAlarmFromServer(alarm_array);
						break;
					 case(AVO_SCOMMAND_SET_PARAM):
						//QString param;
						//QString value;
						in >> param >> value;
						string_array = QByteArray();
						string_array.append(param);
						qcamera->getCamera()->SetParam(string_array.data(), value.toStdString());
						string_array.clear();
						avoInfo("ClientSocket::readClient()") << "SetParam():" << param << " Value: " << value;
						break;
					case(AVO_SCOMMAND_SEND_DELETE_ALARM):
						quint8 ID;
						in >> ID;
						qcamera->DeleteAlarmID(ID);
						break;
					case(AVO_SCOMMAND_ENABLE_CAM):
						in >> enableq;
						qcamera->enableQCameraFromServer(enableq);
						break;
					case(AVO_SCOMMAND_ENABLE_SURVEILLANCE):
						in >> enableq;
						qcamera->enableSurveillanceFromServer(enableq);
						break;
		    	 }
		    	 break;
		     case('I'):
		    //	 if (play_image == false)
		    //		 break;
//		     	in >> camera;
		     	in >> qmetaJPEG;
				 //askImageToServer();
				 if(qmetaJPEG.size() > 98304 || qmetaJPEG.size() < IX7S_SIZEIB_MJPEG_HDR){
					 avoDebug(funcName) << "Client Camera " << port_index << " bad size image received: " << qmetaJPEG.size() << " bytes.";
					 askImageToServer();
					 break;
				 }


		     	/*camStr.setNum(i++);
		     	camStr += "_cam";
		     	camStr+=".jpg";

		     	avoDebug(funcName) << camStr;
		     	tmp = new QFile(camStr);
		     	tmp->open(QIODevice::ReadWrite);
		     	tmp->write(qmetaJPEG.data(),qmetaJPEG.size());
		     	tmp->close();
		     	delete tmp;*/


		     	//bytesAvailable = tcpSocket.bytesAvailable();

		     	emit CameraImageReceived(qmetaJPEG);
		     	//avoDebug(funcName)<<"Image received on camera:" << port_index;

				//askImageToServer();//ask for other image
		    	break;
	     }
    }
 }

void CameraClient_Socket::error()
{
    if (server_connected){
    	emit warningOnSocket(tr("Camera ") + QString::number(port_index) + tr(" error. Connecting to server.") + errorString());
    	Close();
    	if(timer->isActive() == false)
    		timer->start(3000);
    }

}

void CameraClient_Socket::Reconnect()
{
	X7S_FUNCNAME("CameraClient_Socket::Reconnect()");
	connectToHost(serverAddress, quint16(6180 + port_index));
	avoDebug(funcName) << "Connecting...";
	if (waitForConnected(200)){
		timer->stop();
		server_connected = true;
		avoDebug(funcName) << "Camera " << port_index << "Connected again to server";
		emit warningOnSocket(tr("Camera ") + QString::number(port_index) + tr(" Connected again to server."));
		askServerToLog(user_name, user_password);
		nextBlockSize = 0;
		return;
	}
	abort();
	avoDebug(funcName) << "Camera " << port_index << " Connection to server failed";
}
