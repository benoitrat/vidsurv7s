#ifndef CAMERACLIENT_SOCKET_H
#define CAMERACLIENT_SOCKET_H

//#include <QtGui/QWidget>
#include <QImage>
#include <QTcpSocket>
#include <QtNetwork>
#include <QHostAddress>
#include <QTimer>
#include "vv_defines.h"

class QCameraClient;

class CameraClient_Socket : public QTcpSocket
{
    Q_OBJECT

public:
    CameraClient_Socket(QCameraClient *qcam = 0, quint32 ip_add = 0x70000001);
    ~CameraClient_Socket();

private:
//    Ui::clientSocket_Class ui;

public:

	void Close();
	void askImageToServer();
	void sendEnableCameraToServer(bool enable);
	void sendEnableSurveillanceToServer(bool enable);
	void sendNewAlarmsToServer(const QByteArray &alarm_array);
	void sendDeleteAlarmToServer(int ID);
	bool getServerLogged(){ return logged;}
	bool getServerConnected(){ return server_connected;}
	bool Connect();
	void setIP_Address(quint32 server_address = 0x7F000001 ){serverAddress.setAddress(server_address);}
	void Abort_Connect(){abort();}
	void askServerToLog(const QString &name, const QString &password);
	void exitAdminMode();
	void SetParamCamera(const QString &param, const QString &value);
signals:
	void CameraImageReceived(const QByteArray &qmetaJPEG);
	void warningOnSocket(const QString &warning);

protected slots:
//	void enableCamera(bool enable, int camera);
	void dataReceivedFromServer();
	void serverConnected();
	void error();//QAbstractSocket::SocketError error);
	void Reconnect();



private:
    bool closing;
    bool play_image;
    bool server_connected;
    bool camerasEnabled;
    bool logged;
//    QTcpSocket tcpSocket;
	quint32 nextBlockSize;
	QByteArray qmetaJPEG;
	QHostAddress serverAddress;
	int port_index;
	QString user_name;
	QString user_password;
	QCameraClient *qcamera;
	QTimer	*timer;
};

#endif // CAMERACLIENT_SOCKET_H
