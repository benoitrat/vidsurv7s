/*
 * setClientVisorParams
 *
 *  Created on: 08/09/2009
 *      Author: Gabri
 */

#ifndef SETCLIENTVISORPARAMS_H_
#define SETCLIENTVISORPARAMS_H_

#include <QDialog>
#include <QSettings>
#include "ui_setClientVisorParams.h"


class setClientCamerasParams;
class setClientConfigParams;
class Root;
class QCameraClient;
class VideoHandlerScheduleWidget;

class setClientVisorParams : public QDialog
{
    Q_OBJECT

public:
    setClientVisorParams(QDialog *parent = 0, Root *root_p = 0, QVector<QCameraClient *> *qcamera_pt = 0, bool admin_mode = false);
    ~setClientVisorParams();


/*signals:
    void SaveINI_FileSignal();*/

private:
    Ui::setClientVisorParamsClass ui;

    QVector<QCameraClient *> *qcameras;
    QVector<setClientCamerasParams*>	cameraParams;
    setClientConfigParams *configParams;
    Root	*root;
    QSettings *settings;
    VideoHandlerScheduleWidget *video_schdler;
    bool admin;

private slots:
	void ApplyNewValues();
	void ApplyNewValuesAndClose();
//	void SaveINI_File();

};

#endif /* SETCLIENTVISORPARAMS_H_ */
