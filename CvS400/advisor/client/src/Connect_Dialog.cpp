/*
 * Connect_Dialog.cpp
 *
 *  Created on: 01/09/2009
 *      Author: Gabri
 */

#include "Connect_Dialog.h"

#include <QHostAddress>
#include <QFile>

#include "vv_defines.h"

Connect_Dialog::Connect_Dialog(QWidget *parent, unsigned int ip_add)
	: QDialog(parent)
{
	ui.setupUi(this);

	serverAddress = QHostAddress(ip_add);

	ui.addressLineEdit->setText(serverAddress.toString());

    connect(ui.connectButton, SIGNAL(clicked()), this, SLOT(Connect()));

	ui.addressLineEdit->setText(serverAddress.toString());

}
Connect_Dialog::~Connect_Dialog()
{

}

void Connect_Dialog::Connect()
{
	if (serverAddress.setAddress(ui.addressLineEdit->text()))
		done((int)serverAddress.toIPv4Address ());
}




