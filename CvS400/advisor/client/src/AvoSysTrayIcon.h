/**
 *  @file
 *  @brief Contains the class AvoSysTrayIcon.h
 *  @date Mar 10, 2010
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef AVOSYSTRAYICON_H_
#define AVOSYSTRAYICON_H_

#include <QSystemTrayIcon>

/**
 *	@brief The AvoSysTrayIcon.
 *	@ingroup 
 */
class AvoSysTrayIcon : public QSystemTrayIcon {
	Q_OBJECT

public:
	AvoSysTrayIcon(QWidget *mainWindow=NULL,const QString& title=QString("Advisor"));
	virtual ~AvoSysTrayIcon();

protected:
	void createMenu();

public Q_SLOTS:
	void notifyInfo(const QString& msg);
	void notifyWarning(const QString& msg);
	void notify(const QString& msg, const MessageIcon& msgIcon);




private:
	QWidget *m_mainWindow;
	QString m_title;
	int m_msTimeOut;

};

#endif /* AVOSYSTRAYICON_H_ */
