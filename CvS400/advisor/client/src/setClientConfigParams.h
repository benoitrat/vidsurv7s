#ifndef SETCLIENTCONFIGPARAMS_H
#define SETCLIENTCONFIGPARAMS_H

#include <QtGui/QWidget>
#include <QString>

class Root;
class LogConfig;
class Devices;

namespace Ui {
    class setClientConfigParams;
}

class setClientConfigParams : public QWidget {
    Q_OBJECT
    Q_DISABLE_COPY(setClientConfigParams)
public:
    explicit setClientConfigParams(QWidget *parent = 0, Root *root_p = 0);
    virtual ~setClientConfigParams();

signals:
	void VideoPathIsGeneralSignal(bool general);
	void WriteJPEGIsGeneralSignal(bool general);

public slots:
	void ApplyNewValues();

protected:
    virtual void changeEvent(QEvent *e);
private slots:
	void BrowseVideoPath();
    void BrowseLogPath();
    void VideoPathIsGeneral(bool general);
    void WriteJPEGIsGeneral(bool general);

private:
    void applyLanguages();

private:
    Ui::setClientConfigParams *m_ui;
    LogConfig *config;
    Root *root;
    Devices *devices;
    QString videoPath;
    QString logPath;
};

#endif // SETCLIENTCONFIGPARAMS_H
