/*
 * setCameraParams.cpp
 *
 *  Created on: 08/09/2009
 *      Author: Gabri
 */
#include <QColorDialog>
#include <QTextStream>
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QPainter>

#include "setClientCamerasParams.h"
#include "Camera.hpp"
#include "QCameraClient.h"
#include "QtBind.hpp"
#include "vv_defines.h"

setClientCamerasParams::setClientCamerasParams(QDialog *parent, QCameraClient *qcamera_pt)
    : QDialog(parent)
{
	ui.setupUi(this);

	qcamera = qcamera_pt;
	camera = qcamera->getCamera();

	bg_ccMinArea = camera->GetParam("bg_ccMinArea")->toIntValue();
	ui.bg_ccMinArea_SpinBox->setValue(bg_ccMinArea);
	bg_insDelayLog = camera->GetParam("bg_insDelayLog")->toIntValue();
	ui.bg_insDelayLog_SpinBox->setValue(bg_insDelayLog);
	bg_refBGDelay = camera->GetParam("bg_refBGDelay")->toIntValue();
	ui.bg_refBGDelay_SpinBox->setValue(bg_refBGDelay);
	bg_refFGDelay = camera->GetParam("bg_refFGDelay")->toIntValue();
	ui.bg_refFGDelay_SpinBox->setValue(bg_refFGDelay);
	bg_threshold = camera->GetParam("bg_threshold")->toIntValue();
	ui.bg_threshold_SpinBox->setValue(bg_threshold);

	ui.cameraName_lineEdit->setText(camera->GetName());
//	ui.watchDogTimer_spinBox->setValue(qcamera->GetWatchDogTime());
	ui.MJPEGWrite_checkBox->setChecked(camera->GetParam("mjpeg_write")->toIntValue() == 1);
	ui.isEnabled_checkBox->setChecked(qcamera->isEnable());
	ui.isVisible_checkBox->setChecked(qcamera->IsDrawable());
	ui.enableVideoSurveillance_checkBox->setChecked(camera->GetVidSurvPipeline()->GetParam("enable")->toIntValue() == 1);
	ui.showPrivacityAreas_checkBox->setChecked(qcamera->GetArePrivacityAreasShowed());
	ui.viewPixelized_checkBox->setChecked(qcamera->GetIsImagePixelized());
	ui.privacityMode_comboBox->setCurrentIndex(qcamera->GetPrivacityMode());
	ui.drawAlarmAndCounter_checkBox->setChecked(camera->GetVidSurvPipeline()->GetAlarmManager()->GetParam("draw")->toIntValue() == 1);
	ui.drawBlobAndTrajectory_checkBox->setChecked(camera->GetParam("draw_blob")->toIntValue() == 1);

//	connect(ui.bg_applyButton, SIGNAL(clicked()), this, SLOT(ApplyNewBG_Values()));
//	connect(ui.cameraName_lineEdit, SIGNAL(textChanged ( const QString &)), this, SLOT(EnableNamePushButton()));
//	connect(ui.applyName_pushButton, SIGNAL(clicked ()), this, SLOT(ApplyCameraName()));
	connect(ui.delAlarm_pushButton, SIGNAL(clicked()), this, SLOT(DeleteAlarm()));
	connect(ui.alarms_comboBox, SIGNAL(currentIndexChanged ( int )), this, SLOT(EnableAlarmPushButtons()));
	connect(ui.apply_pushButton, SIGNAL(clicked()), this, SLOT(ApplyNewValues()));
	connect(ui.OK_pushButton, SIGNAL(clicked()), this, SLOT(ApplyNewValuesAndClose()));
	connect(ui.enableVideoSurveillance_checkBox, SIGNAL(toggled ( bool)), this, SLOT(VideoSurveillanceEnabled(bool)));
//	connect(ui.privacityApply_pushButton, SIGNAL(clicked()), this, SLOT(ApplyNewValues()));
//	connect(ui.privacityOK_pushButton, SIGNAL(clicked()), this, SLOT(ApplyNewValuesAndClose()));

	if (qcamera->GetSurveillange()== false){
		ui.privacityMode_comboBox->removeItem(1);
		ui.privacityMode_comboBox->removeItem(1);
	}


	alrMgr = camera->GetVidSurvPipeline()->GetAlarmManager();
		for(int i=0;i<alrMgr->Size();i++){
			X7sVSAlrDetector *alrDtr = alrMgr->Get(i);

			CvScalar rgba=cvScalar(255,255,255,255);
			X7sXmlParamGetVector(&rgba,alrDtr->GetParam("color"));
			QColor color(rgba.val[2],rgba.val[1],rgba.val[0],rgba.val[3]);


			ui.alarms_comboBox->addItem(QtBind::toQString(alrDtr->GetParam("name")), QVariant(alrDtr->GetID()));
			ui.alarms_comboBox->setItemData(i+1, color, Qt::DecorationRole/*BackgroundRole, TextColorRole*/);
    }

	image = qcamera->image_v.copy(0,0,PIXELS_PER_LINE, LINES_PER_FRAME);
	setImageToLabel();

	ui.apply_pushButton->hide();
	ui.cancel_pushButton->hide();
	ui.OK_pushButton->hide();
	ui.editAlarm_pushButton->hide();
	ui.changeColor_pushButton->hide();
/*	ui.privacityApply_pushButton->hide();
	ui.privacityCancel_pushButton->hide();
	ui.privacityOK_pushButton->hide();*/

	videoPath = QtBind::toQString(camera->GetParam("mjpeg_rootdir"));
	ui.videoPath_lineEdit->setText(videoPath);
	connect(ui.videoPathBrowse_pushButton, SIGNAL(clicked()), this, SLOT(BrowseVideoPath()));
	if(qcamera->GetAdminMode()== false){
		ui.tabWidget->setTabEnabled(0, false);
		ui.tabWidget->setTabEnabled(2, false);
		ui.tabWidget->setTabEnabled(3, false);
		ui.tabWidget->setCurrentIndex(1);//Modo privacidad
	}
	else
		ui.tabWidget->setCurrentIndex(0);

	ui.tabWidget->removeTab(3);
}

setClientCamerasParams::~setClientCamerasParams()
{
	//delete painter;
	//delete noAvailableImage;
	//delete pixmap;
}

void setClientCamerasParams::ApplyNewBG_Values()
{
	if(bg_ccMinArea != ui.bg_ccMinArea_SpinBox->value()){
		camera->SetParam("bg_ccMinArea", "%d", ui.bg_ccMinArea_SpinBox->value());
		qcamera->SetParamCamera("bg_ccMinArea", QString::number(ui.bg_ccMinArea_SpinBox->value()));
	}
	if(bg_insDelayLog != ui.bg_insDelayLog_SpinBox->value())
	{
		camera->SetParam("bg_insDelayLog", "%d", ui.bg_insDelayLog_SpinBox->value());
		qcamera->SetParamCamera("bg_insDelayLog", QString::number(ui.bg_insDelayLog_SpinBox->value()));
	}
	if(bg_refBGDelay != ui.bg_refBGDelay_SpinBox->value())
	{
		camera->SetParam("bg_refBGDelay", "%d", ui.bg_refBGDelay_SpinBox->value());
		qcamera->SetParamCamera("bg_refBGDelay", QString::number(ui.bg_refBGDelay_SpinBox->value()));
	}
	if(bg_refFGDelay != ui.bg_refFGDelay_SpinBox->value())
	{
		camera->SetParam("bg_refFGDelay", "%d", ui.bg_refFGDelay_SpinBox->value());
		qcamera->SetParamCamera("bg_refFGDelay", QString::number(ui.bg_refFGDelay_SpinBox->value()));
	}
	if(bg_threshold != ui.bg_threshold_SpinBox->value())
	{
		camera->SetParam("bg_threshold", "%d", ui.bg_threshold_SpinBox->value());
		qcamera->SetParamCamera("bg_threshold", QString::number(ui.bg_threshold_SpinBox->value()));
	}

	//emit ValuesUpdated();

	//close();
}
/*
void setClientCamerasParams::ApplyCameraName()
{
	qcamera->setName(ui.cameraName_lineEdit->text());
	camera->SetParam("name","%s",ui.cameraName_lineEdit->text().toStdString().c_str());
	qcamera->SetParamCamera("name", (QString)ui.cameraName_lineEdit->text());
}
*/
void setClientCamerasParams::DeleteAlarm()
{
	int index = ui.alarms_comboBox->currentIndex();
	if (index == 0)
		return;
	ui.AlarmsImage_label->hide();
	bool ok=false;
	int ID = ui.alarms_comboBox->itemData(index).toInt(&ok);
	if(ok==false) return;
	alrMgr->Delete(ID);
	ui.alarms_comboBox->removeItem ( index );
	setImageToLabel();
	qcamera->SendDeleteAlarm(ID);
}
/*void setClientCamerasParams::EnableNamePushButton()
{
	ui.applyName_pushButton->setEnabled(true);
}
*/
void setClientCamerasParams::EnableAlarmPushButtons()
{
	int index = ui.alarms_comboBox->currentIndex();
	if (index == 0){
		ui.delAlarm_pushButton->setEnabled(false);
		setImageToLabel();
		return;
	}

	ui.delAlarm_pushButton->setEnabled(true);
//	ui.changeColor_pushButton->setEnabled(true);
//	ui.editAlarm_pushButton->setEnabled(true);
	//Dibujamos la alarma seleccionada

	QPixmap pixmap = QPixmap::fromImage(image);
	QPainter *painter = new QPainter(&pixmap);

	ui.AlarmsImage_label->hide();
	bool ok=false;
	int ID = ui.alarms_comboBox->itemData(index).toInt(&ok);
	if(ok==false) return;

	if (alrMgr->GetByID(ID)->GetType() == X7S_VS_ALR_TYPE_LINE){
		QColor color = QtBind::toQColor(alrMgr->GetByID(ID)->GetParam("color"));
		QLine line = QtBind::toQLine(alrMgr->GetByID(ID));
		painter->setPen(QPen(color, 3));
		if(!line.isNull()) painter->drawLine(line);
	}

	if (alrMgr->GetByID(ID)->GetType() == X7S_VS_ALR_TYPE_POLY){
		QColor color = QtBind::toQColor(alrMgr->GetByID(ID)->GetParam("color"));
		painter->setPen(QPen(color, 3));
		QPolygon poly = QtBind::toQPolygon(alrMgr->GetByID(ID));
		if(!poly.isEmpty()) painter->drawPolygon(poly);
	}

	ui.AlarmsImage_label->setBackgroundRole(QPalette::Dark);
	ui.AlarmsImage_label->setAutoFillBackground(true);
	ui.AlarmsImage_label->setAlignment(Qt::AlignLeft | Qt::AlignTop);
	ui.AlarmsImage_label->setPixmap(pixmap);
	ui.AlarmsImage_label->show();

	delete painter;
}

void setClientCamerasParams::setImageToLabel()
{
		QPixmap pixmap = QPixmap::fromImage(image);
		QPainter *painter = new QPainter(&pixmap);
		for(int i=0;i<alrMgr->Size();i++){
			if (alrMgr->Get(i)->GetType() == X7S_VS_ALR_TYPE_LINE){
				QColor color = QtBind::toQColor(alrMgr->Get(i)->GetParam("color"));
				QLine line = QtBind::toQLine(alrMgr->Get(i));
				painter->setPen(QPen(color, 3));
				if(!line.isNull()) painter->drawLine(line);
			}
			if (alrMgr->Get(i)->GetType() == X7S_VS_ALR_TYPE_POLY){
				QColor color = QtBind::toQColor(alrMgr->Get(i)->GetParam("color"));
				QPolygon poly = QtBind::toQPolygon(alrMgr->Get(i));
				painter->setPen(QPen(color, 3));
				if(!poly.isEmpty()) painter->drawPolygon(poly);
			}
		}

		ui.AlarmsImage_label->setBackgroundRole(QPalette::Dark);
		ui.AlarmsImage_label->setAutoFillBackground(true);
		ui.AlarmsImage_label->setAlignment(Qt::AlignLeft | Qt::AlignTop);
		ui.AlarmsImage_label->setPixmap(pixmap);
		ui.AlarmsImage_label->show();

		ui.CameraImage_label->setBackgroundRole(QPalette::Dark);
		ui.CameraImage_label->setAutoFillBackground(true);
		ui.CameraImage_label->setAlignment(Qt::AlignLeft | Qt::AlignTop);
		ui.CameraImage_label->setPixmap(pixmap);
		ui.CameraImage_label->show();

		ui.privacityCameraImage_label->setBackgroundRole(QPalette::Dark);
		ui.privacityCameraImage_label->setAutoFillBackground(true);
		ui.privacityCameraImage_label->setAlignment(Qt::AlignLeft | Qt::AlignTop);
		ui.privacityCameraImage_label->setPixmap(pixmap);
		ui.privacityCameraImage_label->show();

		delete painter;
	ui.alarms_comboBox->setCurrentIndex(0);
	ui.delAlarm_pushButton->setEnabled(false);
	ui.changeColor_pushButton->setEnabled(false);
	ui.editAlarm_pushButton->setEnabled(false);
}


void setClientCamerasParams::ApplyNewValues()
{
	ApplyNewBG_Values();
	qcamera->setName(ui.cameraName_lineEdit->text());
	camera->SetParam("name","%s",ui.cameraName_lineEdit->text().toStdString().c_str());
	qcamera->SetParamCamera("name", (QString)ui.cameraName_lineEdit->text());
	camera->SetParam("mjpeg_write", "%d",ui.MJPEGWrite_checkBox->isChecked());
	camera->SetParam("mjpeg_rootdir", "%s", videoPath.toStdString().c_str());
	qcamera->enableQCamera(ui.isEnabled_checkBox->isChecked());
	qcamera->setDrawable(ui.isVisible_checkBox->isChecked());
	qcamera->enableSurveillance(ui.enableVideoSurveillance_checkBox->isChecked());
//	qcamera->SetWatchDogTime(ui.watchDogTimer_spinBox->value());
	qcamera->EnableImagePixelized(ui.viewPixelized_checkBox->isChecked()&&ui.enableVideoSurveillance_checkBox->isChecked());
	qcamera->EnableShowPrivacityAreas(ui.showPrivacityAreas_checkBox->isChecked());
	if ((qcamera->GetSurveillange()== false)&&ui.privacityMode_comboBox->currentIndex())
		qcamera->SetPrivacityMode(PrivacityModes(3));//S�lo ver imagen coloreada
	else
		qcamera->SetPrivacityMode(PrivacityModes(ui.privacityMode_comboBox->currentIndex()));
	//qcamera->EnableViewSurveillanceData(ui.viewSurveillanceData_checkBox->isChecked());
	camera->SetParam("draw_blob", "%d", ui.drawBlobAndTrajectory_checkBox->isChecked());
	camera->GetVidSurvPipeline()->GetAlarmManager()->SetParam("draw", "%d", ui.drawAlarmAndCounter_checkBox->isChecked());

}

void setClientCamerasParams::ApplyNewValuesAndClose()
{
	ApplyNewValues();
	done(0);
}

void setClientCamerasParams::VideoSurveillanceEnabled(bool enable)
{
	if(enable){//Se activa la video-vigilancia
        ui.privacityMode_comboBox->clear();
        ui.privacityMode_comboBox->insertItems(0, QStringList()
         << QApplication::translate("setClientCamerasParamsClass", "Privacity disabled", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("setClientCamerasParamsClass", "View blobs only", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("setClientCamerasParamsClass", "View blobs and fixed image", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("setClientCamerasParamsClass", "View colored image", 0, QApplication::UnicodeUTF8)
        );
        ui.viewPixelized_checkBox->setEnabled(true);
	}
	else{//Se desactiva la video-vigilancia
		ui.privacityMode_comboBox->removeItem(1);
		ui.privacityMode_comboBox->removeItem(1);
		ui.viewPixelized_checkBox->setEnabled(false);
		ui.viewPixelized_checkBox->setChecked(false);
	}
}

void setClientCamerasParams::ShowPushButtons()
{
	ui.apply_pushButton->show();
	ui.cancel_pushButton->show();
	ui.OK_pushButton->show();
//	ui.privacityApply_pushButton->show();
//	ui.privacityCancel_pushButton->show();
//	ui.privacityOK_pushButton->show();
}
void setClientCamerasParams::BrowseVideoPath()
{
	QFileDialog dialog(this);
	dialog.setFileMode(QFileDialog::Directory);
	dialog.setOptions(QFileDialog::ShowDirsOnly);
	QStringList fileNames;
	if (dialog.exec())
		fileNames = dialog.selectedFiles();
	else
		return;
	videoPath = fileNames.at(0);
	ui.videoPath_lineEdit->setText(videoPath);
}
void setClientCamerasParams::VideoPathIsGeneral(bool general)
{
	ui.videoPathBrowse_pushButton->setEnabled(general==false);
	ui.videoPath_lineEdit->setEnabled(general==false);
}

void setClientCamerasParams::WriteJPEGIsGeneral(bool general)
{
	ui.MJPEGWrite_checkBox->setEnabled(general==false);
}
