/*
 * Connect_Dialog.h
 *
 *  Created on: 01/09/2009
 *      Author: Gabri
 */

#ifndef CONNECT_DIALOG_H_
#define CONNECT_DIALOG_H_


#include <QtGui/QDialog>
#include <QHostAddress>

#include "vv_defines.h"

#include "ui_connect_dialog.h"

class Connect_Dialog : public QDialog
{
    Q_OBJECT

public:
    Connect_Dialog(QWidget *parent = 0, unsigned int ip_add = 0x80010101);
    ~Connect_Dialog();

private:
    Ui::Connect_Dialog_Class ui;


protected slots:

	void Connect();

private:
	QHostAddress serverAddress;
};


#endif /* CONNECT_DIALOG_H_ */
