#include "client_visor.h"

#include <QtGui>
#include <QApplication>
#include <QTranslator>
#include <x7svidsurv.h>
#include <x7snet.h>
#include "Log.hpp"



int main(int argc, char *argv[])
{
	qInstallMsgHandler(AVOMsgOutput);
	QApplication a(argc, argv);

	//Setup the setting in all the application
	QCoreApplication::setOrganizationName("Seven Solutions");
	QCoreApplication::setOrganizationDomain("www.sevensols.com");
	QCoreApplication::setApplicationName("AVOClient");

	//Load language setting
	QSettings setting;
	bool ok;
	QLocale::Language langCode = (QLocale::Language)setting.value("lang",(int)QLocale::Spanish).toInt(&ok);
	if(ok) QLocale::setDefault(QLocale(langCode));

	//Load translation
	QString qmDir=a.applicationDirPath()+"/locale";

	//General QT translation
	QTranslator qtTranslator;
	qtTranslator.load("qt_"+QLocale().name(),qmDir);
	a.installTranslator(&qtTranslator);

	//AVOCore and AVOWidget translation
	QTranslator wgtTranslator;
	wgtTranslator.load("widgets_"+QLocale().name(),qmDir);
	a.installTranslator(&wgtTranslator);

	//AVOClient translation
	QTranslator translator;
	translator.load("client_"+QLocale().name(),qmDir);
	a.installTranslator(&translator);

	Client_Visor w;
	w.show();
	return a.exec();
}
