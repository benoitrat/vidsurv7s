#ifndef QCamera_H
#define QCamera_H

#include <QTimer>
#include "QCamera.h"
#include "CameraClient_Socket.h"


enum PrivacityModes
{
	PM_PRIVACITY_DISABLED,
	PM_VIEW_BLOBS_ONLY,
	PM_VIEW_BLOBS_AND_FIXED_IMAGE,
	PM_VIEW_BLENDED_IMAGE
};

#define MAX_POLYGONS 10
#define MAX_LINES	 10
#define MAX_PRIVACITY_AREAS 10

class QCameraClient : public QCamera
{
    Q_OBJECT

public:
	QCameraClient(QObject *parent = 0, Camera *camera_ptr = 0, int index = 0);
    ~QCameraClient();


signals:
    void backGroundParamsChanged(int index);
    void cameraSurveillanceEnabled(bool enable, int index);
    void newAlarmsAdded(const QByteArray &alarm_array, quint8 index);
    

public slots:
	void setDrawable(bool enable);
	void enableQCamera(bool enable);
	void enableQCameraFromServer(bool enable);
    void enableSurveillance(bool enable);
    void enableSurveillanceFromServer(bool enable);
    bool GetSurveillange(){return surveillanceEnabled;};
    void SetWatchDogTime(quint32 time);
    quint32 GetWatchDogTime();
    void ClientSocketLog(const QString &name, const QString &password, quint32 server_address );
    void ChangeUser(const QString &name, const QString &password);
    void SetAdminMode(bool enable);
//    void EnablePrivacityMode(bool enable);
    bool GetAdminMode(){return admin_mode;};
    QVector<QPolygon> *GetPrivacityAreas(){return privacity;};
    void AddPrivacityAreas(QVector<QPolygon> * p_area);
    bool GetIsImagePixelized(){return imagePixeled;};
    bool GetArePrivacityAreasShowed(){return viewPrivacityAreas;};
    void EnableImagePixelized(bool enable);
    void EnableShowPrivacityAreas(bool enable){viewPrivacityAreas = enable;};
    PrivacityModes GetPrivacityMode(){return privacity_mode;};
    void SetPrivacityMode(PrivacityModes mode);//{privacity_mode = mode;};
    void EnableViewSurveillanceData(bool enable){viewSurveillanceData = enable;};
    bool GetViewSurveillanceData(){return viewSurveillanceData;};
    void SetParamCamera(const QString &param, const QString &value);
    void SendDeleteAlarm(int ID){camera_socket->sendDeleteAlarmToServer(ID);};
    void DeleteAlarmID(quint8 ID){pAlarms->Delete(ID);};
    void addNewAlarmFromServer(QByteArray &alarm_array);
    void TimeoutError();
    bool GetCameraSocketConnected(){return camera_socket->getServerConnected();};
    void ActualizeFixedImage();
    QSize GetQCameraSize(){return qcamera_size;};
    
protected:
    virtual void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseDoubleClickEvent (QMouseEvent * event );
    void mouseMoveEvent (QMouseEvent * event );

private slots:
	void updateImage(const QByteArray &qmeta_jpeg);

	void addPolygon();
	void addLine();
	void apply();
	void cancel();
	void delLastPolygon();
	void delAllPolygons();
	void delLastLine();
	void delAllLines();
	void createActions();
	void createMenus();
	void addAlarm();
	void exitAddAlarm();

	void setCameraParams();

	void addPrivacity();
	void delLastPrivacity();
	void delAllPrivacities();
//	void ActualizeFixedImage();
	void emitWarningOnCamera(const QString &msg);
	void askImageByTimer();

//	void enableImagePixelized(bool enable);

private:

	CameraClient_Socket *camera_socket;
//	QPixmap pixmap;
//    QPoint pixmapOffset;
    QPoint last_line_point[2];
    QVector<QPolygon> *privacity;
    QPolygon polygon_tmp;
    QPolygon polygon[MAX_POLYGONS];
    QLine line[MAX_LINES];
    unsigned int polygon_index;
    unsigned int last_polygon_index;
    unsigned int polygon_size;
    unsigned int line_index;
    unsigned int last_line_index;
    unsigned int privacity_index;
    unsigned int privacity_size;

    
    QMenu	*menu_enable;
    QAction *addAlarmAction;
    QAction *setCameraParamsAction;

    QMenu	*privacity_menu;
    QAction *addPrivacityAction;
    QAction *delPrivacityAction;
    QAction *delLastPrivacityAction;
    bool 	addprivacity;
    bool 	draw_privacity;


    bool 	images_received;
    bool 	images_received_timeout;
   
    bool	imagePixeled;
    bool	viewPrivacityAreas;
    bool	viewSurveillanceData;
    
    //Privacity modes
    PrivacityModes		privacity_mode;
    QImage	*noAvailableImage;
    QImage	fixedImage;
//    QString name;
 //   Camera	*camera;
    X7sVSAlrManager *pAlarms;

    bool admin_mode;
    QString user_name;
    QString user_password;

    quint32 timeout_ms;
    quint32	frame_add;

    QString frame_rate;
    int m_nTimeOut;
    QTimer *image_timer;
    QSize 	qcamera_size;


};

#endif // QCamera_H
