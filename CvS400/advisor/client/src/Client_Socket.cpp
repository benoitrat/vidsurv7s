#include "Client_Socket.h"

#include <QHostAddress>
#include <QtNetwork>
#include <QMessageBox>
#include <QTextStream>
#include <QFile>
#include <QDataStream>
#include <QDebug>
#include <QCryptographicHash>

#include "vv_defines.h"
#include "Log.hpp"
//#include "useraccessdialog.h"

Client_Socket::Client_Socket(QObject *parent, quint32 ip_add)
	: QTcpSocket(parent), tmpFile(this)
{
	server_connected = false;
	closing = false;
	play_image = false;
	xml_file_received = false;
	logged = false;
	user_level = -1;
	user_name = "";
	user_password = "";

	nextBlockSize = 0;

	for(int i = 0; i < 4; i++)
		camerasEnabled[i] = false;

	serverAddress = QHostAddress(ip_add);
    connect(this, SIGNAL(connected()), this, SLOT(serverConnected()));
    connect(this, SIGNAL(disconnected()), this, SLOT(error()));
    connect(this, SIGNAL(readyRead()),this, SLOT(dataReceivedFromServer()));

	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(Reconnect()));
   // Connect();
}

Client_Socket::~Client_Socket()
{
	timer->stop();
	delete timer;
	if (server_connected){
		server_connected = false;
		closing = true;
		this->close();
	}
}

bool Client_Socket::Connect()
{
	connectingDialog = new Connecting_Dialog(this, &serverAddress);
	int result = connectingDialog->exec();
	connectingDialog->hide();
	connectingDialog->close();
/*	if(result)
		server_connected = true;
	else
		server_connected = false;*/
	delete connectingDialog;
	return result==QDialog::Accepted;
}

void Client_Socket::serverConnected()
{
	avoDebug("Client_Socket::serverConnected()");
	server_connected = true;
	if(user_name!="" && user_password!="")
		askServerToLog(user_name, user_password);
/*	connectingDialog->hide();
	connectingDialog->close();
	server_connected = true;
	delete connectingDialog;*/
}


void Client_Socket::ExecutePlay()
{
	play_image = true;
	sendPlayToServer();
}

void Client_Socket::StopPlay()
{
	play_image = false;
	sendStopToServer();
}

void Client_Socket::Close()
{
	if (server_connected){
		server_connected = false;
		close();
	}
}

void Client_Socket::sendPlayToServer()
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('B') << quint8(AVO_SCOMMAND_PLAY);

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);

}

void Client_Socket::sendStopToServer()
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('B') << quint8(AVO_SCOMMAND_STOP);

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);
}

void Client_Socket::askImageToServer(int camera)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('C') << quint8(AVO_SCOMMAND_SEND_IMAGE) << quint8(camera);

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);

}

void Client_Socket::askXMLFileToServer()
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    xml_file_received = false;
    out << quint32(0) << quint8('F') << quint8(AVO_SCOMMAND_SEND_XML_FILE);

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);
    for(int i = 0; i < 100; i++){
		if(waitForReadyRead(30000) == false || xml_file_received){
			return;
		}
    }
}

void Client_Socket::askSaveXMLtoServer()
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('F') << quint8(AVO_SCOMMAND_SAVE_XML_FILE);

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);
}

void Client_Socket::askServerToLog(const QString &name, const QString &password)
{
	logged = false;
	user_level = -1;

	QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('U') << quint8(AVO_SCOMMAND_USER_LOG);

    user_name = name;
    user_password = password;

    QCryptographicHash *hash = new QCryptographicHash(QCryptographicHash::Sha1);
	QByteArray data = QByteArray();
	data.append(user_password);
	hash->addData(data);
	QString hash_result = QString(hash->result().toHex());
    out << name << hash_result;
    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);
    waitForReadyRead(30000);
}

void Client_Socket::exitAdminMode()
{

	if ((logged == false)||(isValid()==false))
    	return;
	QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    out << quint32(0) << quint8('U') << quint8(AVO_SCOMMAND_USER_NO_ADMIN);

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));

    write(block);

}

void Client_Socket::sendEnableCameraToServer(bool enable, int camera)
{
    if (camera >= 4)
    	return;
    camerasEnabled[camera] = enable;
	QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    quint8 command;
    if (enable)
    	command = AVO_SCOMMAND_ENABLE_CAM;
    else
    	command = AVO_SCOMMAND_DISABLE_CAM;

    out << quint32(0) << quint8('C') << quint8(command) << quint8(camera);

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);
}

void Client_Socket::sendEnableSurveillanceToServer(bool enable, int camera)
{
    if (camera >= 4)
    	return;
	QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);
    quint8 command;
    if (enable)
    	command = AVO_SCOMMAND_ENABLE_SURVEILLANCE;
    else
    	command = AVO_SCOMMAND_DISABLE_SURVEILLANCE;

    out << quint32(0) << quint8('C') << quint8(command) << quint8(camera);

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);

}

void Client_Socket::sendNewAlarmsToServer(const QByteArray &alarm_array, quint8 camera_index)
{
	QByteArray block;
	QByteArray array = QByteArray(alarm_array);
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_3);

    out << quint32(0) << quint8('C') << quint8(AVO_SCOMMAND_SEND_NEW_ALARMS) << quint8(camera_index);
    out << array;

    out.device()->seek(0);
    out << quint32(block.size() - sizeof(quint32));
    write(block);
}

void Client_Socket::dataReceivedFromServer()
{
	in_progress = true;
    quint32 bytes_Available;

 	QString camStr;
// 	QFile *tmp;
// 	static quint32 i=0;


    thread_run = true;
	QDataStream in(this);
    in.setVersion(QDataStream::Qt_4_3);
    QTextStream *out;
    while(true){
		if (nextBlockSize == 0) {
			bytes_Available = bytesAvailable();
	         if (bytesAvailable() < sizeof(quint32))
	             break;
	         in >> nextBlockSize;
	     }
		bytes_Available = bytesAvailable();
	     if (bytesAvailable() < nextBlockSize)
	         break;
	     bytes_Available = bytesAvailable();
	     quint8 command_type;
	     quint8 command;

	     in >> command_type;
	     in >> command;

	     switch(command_type){
	     	case('F'):
	     		 if (command != AVO_SCOMMAND_SEND_XML_FILE)
		    		 break;
		     	quint16 num_lines;
		     	in >> num_lines;

		         if (!tmpFile.open())
		         {
		        	 avoWarning("Client_Socket::dataReceivedFromServer()")<<tr("Error. Can't write XML file.");
		        	 emit warningOnSocket(tr("Error. Can't write XML file."));
		        	 break;
		         }
		         out = new QTextStream(&tmpFile);
		         while (num_lines) {
		             QString line;
		             in >> line;
		             *out << line;
		             num_lines--;
		         }
		         //delete out;
		         //delete file;
		         xml_file_received = true;
		         tmpFile.close();
	//	         xmlFileReceived.wakeAll();
	//	         mutex.unlock();
		    	 break;
		     case ('B'):
		    	 break;
		     case ('U'):
		    	 if (command == AVO_SCOMMAND_USER_LOGGED){
		    		 in >> logged;
		    		 in >> user_level;
		    	 }
		    	 break;
		     case('C'):
		    	 break;
/*		     case('I'):
		    //	 if (play_image == false)
		    //		 break;
		     	in >> camera;
		     	in >> qmetaJPEG;



		     	camStr.setNum(i++);
		     	camStr += "_cam";
		     	camStr+=".jpg";

		     	avoDebug(funcName) << camStr;
		     	tmp = new QFile(camStr);
		     	tmp->open(QIODevice::ReadWrite);
		     	tmp->write(qmetaJPEG.data(),qmetaJPEG.size());
		     	tmp->close();
		     	delete tmp;


		     	bytes_Available = bytesAvailable();
		     	if (camera == 0)
	 				emit Cam1_ImageReceived(qmetaJPEG);
				if (camera == 1)
					emit Cam2_ImageReceived(qmetaJPEG);
				if (camera == 2)
					emit Cam3_ImageReceived(qmetaJPEG);
				if (camera == 3)
					emit Cam4_ImageReceived(qmetaJPEG);
				while(true){
					camera++;
					if (camera == 4)
						camera = 0;
					if(camerasEnabled[camera])
						break;
				}
				askImageToServer(camera);//ask for other image
		    	break;*/
	     }

	     nextBlockSize = 0;
    }
 }

void Client_Socket::error()
{
    if (server_connected){
    	emit warningOnSocket(tr("Error. Connecting to server.") + errorString());
    	Close();
    	if(timer->isActive() == false)
    		timer->start(3000);
    }
}
void Client_Socket::Reconnect(){
	X7S_FUNCNAME("Client_Socket::error()");
	connectToHost(serverAddress, quint16(6178));
	avoDebug(funcName) << "Connecting...";
	if (waitForConnected(200)){
		timer->stop();
		avoDebug(funcName) << "Connected again to server";
		emit warningOnSocket(tr("Connected again to server."));
		emit ServerConnectedAgain();
		//askServerToLog(user_name, user_password);
		return;
	}
	abort();
	emit ServerConnectionError();
	avoDebug(funcName) << "Connection to server failed";
}




