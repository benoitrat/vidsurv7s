#ifndef CLIENT_VISOR_H
#define CLIENT_VISOR_H

#include <QtGui/QMainWindow>
#include <QLabel>
#include <QTextEdit>
#include <QSplitter>
#include <QGroupBox>
#include <QGridLayout>
#include <QSettings>
#include <QCloseEvent>
#include <QTimer>

#include "ui_client_visor.h"
#include "Core.hpp"
#include "CamEvtCarrier.hpp"
#include "vv_defines.h"


class AvoSysTrayIcon;
class Root;
class QCameraClient;
class Camera;
class Socket_Devices;
class Client_Socket;
class Connect_Dialog;
class setClientVisorParams;
class EventListWidget;
class ViewerWidget;
class AlrTimeWidget;
class Database;
class WarningWidget;
class VideoHandler;
class CamEvtHandler;

class Client_Visor : public QMainWindow
{
    Q_OBJECT

public:
    Client_Visor(QWidget *parent = 0);
    ~Client_Visor();

	signals:
	void playingEvt(const CamEvtCarrier& camEvt, bool play);

protected:
	void closeEvent(QCloseEvent *event);

private:
	bool EventSelected(const AVOAlrEvt& alrEvt, bool play);	//!< @deprecated
	void setupSysTray();



	//----------- SLOTS

public slots:
	void fullScreen(bool full=true);

private slots:

	void configClientParams();
	void writeSettings();
	void readSettings();
	void ResizeCameras(int pos, int index);
	void RedrawCameras();
	void ChangeAlarmsWindowOrientation(bool enable);
	void ShowAlarmsWindows(bool enable);
	bool EventSelected(const CamEvtCarrier& camEvt, bool play);
	void SetUserMode(ClientUsersMode mode, bool access);
	void EnableAdminMode();
//	void EnablePrivacityMode();
	void EnableVisorMode();
	void ServerSaveXMLFile();
	void receivedTick(const QDateTime& actual);
	void receivedDayTick(const QDateTime& actual);
	void ServerConnectionError();
	void ServerConnectedAgain();
	void SetDefaultView();
	void RefreshAlarmTime(int index);
	void UpdateQCameraFixedImage(const CamEvtCarrier &evt);


	//----------- VARIABLE

private:
    Ui::ClientVisorClass ui;

//    QBoard	*qboard;
    QLabel *locationLabel;
 //   Thread_Socket *socket_thread;
    Client_Socket *socket_client;
    Connect_Dialog *connectDialog;
    QTextEdit	*eventTextEdit;
    QSplitter *splitter;
//    QGroupBox *qcameras_groupBox;
    QGridLayout *qcameras_gridLayout;
    QGridLayout *events_gridLayout;
    quint32 numberOfColumms;
    quint32 numberOfArrows;
    quint32 numberOfBoards;
    unsigned int server_address;
    setClientVisorParams *clientVisorParams;
    QSplitter *splitter_event;
    bool admin_mode;
    ClientUsersMode user_mode;

    Root *serv_root;
    Database	*client_database;
    VideoHandler *vidHandler;
    CamEvtHandler *camEvtHandler;

    QVector<QCameraClient *>	qcameras;
    QVector<AlrTimeWidget*> alrTime;
    EventListWidget *eventList;
    ViewerWidget *videoViewer;
    WarningWidget *warningList;
    AvoSysTrayIcon *sysTrayIco;

    QTimer *images_timeout;
    quint32 timeout_ms;

    QString name;
    QString password;

};

#endif // CLIENT_VISOR_H
