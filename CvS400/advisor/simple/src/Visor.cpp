#include "Visor.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------

#include <highgui.h>
#include "Camera.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

Visor::Visor(Board *board)
:board(board), life(false)
{

}

Visor::~Visor() {

}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------
void Visor::run()
{

	if(setup())
	{
		while(life)
		{
			life=loop();
		}
	}
}

bool Visor::loop()
{
	if(!board->IsAlive(0)) return true;
	if(!life) life=(board->Start()==X7S_RET_OK);

	char key;

	int cam_id=board->ProcessFrame();
	if(cam_id!=X7S_RET_ERR)
	{
		Camera *cam = board->GetCamera(cam_id);
		cvShowImage(cam->GetName().toStdString().c_str(),cam->GetFrame());
		key=cvWaitKey(2);
		if(key==27) {
			life = false;
			board->Stop();
			return false;
		}
	}
	return true;
}

bool Visor::setup()
{
	if(board->Connect() && board->Setup())
	{
		for(int c=0;c<AVO_BOARD_NUMCAM;c++)
		{
			Camera *cam = board->GetCamera(c);
			if(cam->GetParam("enable")->toIntValue())
			{
				cvNamedWindow(cam->GetName().toStdString().c_str(),CV_WINDOW_AUTOSIZE);
			}
		}
		return true;
	}
	return false;
}



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------
