/**
*  @file
*  @brief Main file of the AdViSor program
*  @date Jun 25, 2009
*  @author Benoit RAT.
*/

#include "Camera.hpp"
#include "Devices.hpp"
#include "Database.hpp"
#include "Visor.hpp"
#include "Log.hpp"
#include "Root.hpp"

//Standard library used
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <vector>

using namespace std;

#define AVO_SIMPLE_CONFIG_XML "advisor_simple.xml"


int main(int argc,const char *argv[]) {

	const char *xmlfname;
	if(argc<2){
		printf("Usage:\n");
		printf("%s <xml_config_file>\n\7",argv[0]);
		xmlfname=AVO_SIMPLE_CONFIG_XML;
	}
	else
	{
		xmlfname=argv[1];
	}

	Board *board = NULL;
	vector<Visor*> vecVisor;

	Root root(xmlfname);
	Devices *devices = root.devices();

	x7sXmlLog_SetLevel(X7S_LOGLEVEL_LOW);
	x7sVSLog_SetLevel(X7S_LOGLEVEL_MED);
	x7sNetLog_SetLevel(X7S_LOGLEVEL_MED);
	Log::SetLevel(X7S_LOGLEVEL_HIGH);

	//Then try to read
	if(!root.LoadFile() || root.devices()->GetSize()==0) { //If it is not possible just create it with default parameters.

		//Add the desired number of board
		board = new Board(devices);
		board->SetParam("ip","192.168.7.254");
		board->SetParam("rtp","50000");

		root.SaveFile();
		root.Print();

		cout << "Generation of the file:" << root.fileName().toStdString() << " succeed (Now reload the software)" << endl;
	}
	else {
		devices = root.devices();
		root.database()->Connect();


		for(int i=0;i<devices->GetSize();i++)
		{
			board = devices->GetBoard(i);
			if(board)
			{
				Visor *visor = new Visor(board);
				if(visor->setup())
				{
					vecVisor.push_back(visor);
				}
				else
				{
					delete visor;
					visor=NULL;
				}
			}
		}

		//If visor has its own thread maybe use visor->run()


		//Loop over the visor.
		size_t i=0;
		while(i<vecVisor.size())
		{
			if(vecVisor[i]->loop()==false) 	break;
			i=(i+1)%vecVisor.size();	//Go to the next board.
		}
	}


	for(size_t i=0;i<vecVisor.size();i++)
	{
		X7S_DELETE_PTR(vecVisor[i]);
	}

	//And finally write it, in XML node and save it.
	if(argc<2) root.SaveFile();

	return EXIT_SUCCESS; //And exit.
}


