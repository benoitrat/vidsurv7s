/**
 *  @file
 *  @brief Contains the class Visor.hpp
 *  @date Jul 14, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef VISOR_HPP_
#define VISOR_HPP_

#include "Board.hpp"

/**
 *	@brief The Visor.
 *	@ingroup avo_test
 */
class Visor {
public:
	Visor(Board *board);
	virtual ~Visor();

	void run();
	bool setup();
	bool loop();

private:
	Board* board;
	bool life;
	bool is_alive;
};

#endif /* VISOR_HPP_ */
