/**
*  @file
*  @brief Contains the class ViewerWindow.h
*  @date Aug 28, 2009
*  @author neub
*	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
*/

#ifndef MAINWINDOW_H_
#define MAINWINDOW_H_

class SceneWidget;


#include "ui_MainWindow.h"
#include <QMainWindow>
#include <QFile>
#include <QList>
#include <highgui.h>



/**
*	@brief The ViewerWindow.
*	@ingroup avo_viewer
*/
class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = 0);
	virtual ~MainWindow();
	bool OpenVideo(QString fname=QString());

public slots:
	bool OpenVidDialog() { return OpenVideo(QString()); };

protected slots:
	void displayMessage(const QString& msg,int ms_timeout=10000);
	void nextFrame();
	void prevFrame();

private:
	Ui::MainWindow ui;
	SceneWidget *sceneWidget;
	cv::VideoCapture m_vidCap;
	QList<QFile> imSeq;
};

#endif /* MEDIAPLAYER_H_ */
