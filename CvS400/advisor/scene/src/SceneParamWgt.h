#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>


//Forward declarations.
namespace Ui {
    class SceneParamWgt;
}

namespace x7s {
	class Plane2D3D;
	class Scene2D3D;
	class Polygon;

}

class SceneParamWgt : public QWidget {
    Q_OBJECT
public:
    SceneParamWgt(QWidget *parent = 0, const QSize& size=QSize() );
    ~SceneParamWgt();

private:

    void DatosEscena0(int i);
    void DatosEscena1(int i);
    void DatosEscena2(int i);
    void DatosEscena3(int i);
    void DatosEscena4(int i);


public slots:
	void BotonDisabled();
    void BotonClickedParametros();
    void BotonClickedAltura();
    void BotonClickedDistancia();
    void BotonClickedLineaHorizonte();

	void startEstimation();
	void endEstimation();

signals:
    void estimationStarting();
	void estimationEnded();


private:
    x7s::Scene2D3D *Scene;
    x7s::Polygon *m_pPoly;
    Ui::SceneParamWgt *ui;


};

#endif // MAINWINDOW_H
