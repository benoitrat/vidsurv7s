/*
 * PaintImageWidget.h
 *
 *  Created on: 30/03/2010
 *      Author: Ben
 */

#ifndef PAINTIMAGEWIDGET_H_
#define PAINTIMAGEWIDGET_H_

#include <QtGui>
#include <QWidget>
#include <QImage>
#include <QVector>
#include <QGraphicsScene>
#include "GraphicsShape.h"


class PaintImageWidget: public QWidget {

	Q_OBJECT
	Q_PROPERTY(bool m_keepRatio READ isKeepRatio WRITE setKeepRatio)
	Q_PROPERTY(bool m_resizeable READ isResizeable WRITE setResizeable)

public:
	PaintImageWidget(QWidget *parent=NULL);
	virtual ~PaintImageWidget();

	void setResizeable(bool resizeable) { m_resizeable = resizeable; };
	bool isResizeable() { return m_resizeable; };
	void setKeepRatio(bool keepRatio) { m_keepRatio = keepRatio; };
	bool isKeepRatio() { return m_keepRatio; };

	void addImage(const QImage& img);
	void addImages(const QVector<QImage>& img);
	bool delLastImage();
	void clearImages();

	void startDrawing(const GraphicsShape& shape);
	void endDrawing() { startDrawing(GraphicsShape()); }
	bool setShapeProperty(GraphicsShape::Properties prop, const QVariant& value);

	void	setScene(QGraphicsScene *scene) { m_gscene=scene; }
	QGraphicsScene* getScene() { return m_gscene; }


public slots:
	void saveScene(const QString& fname);
	void loadScene(const QString& fname);
	void clearScene();
	void removeLastGItem();


	void validateActualGItem();
	void ignoreActualGItem();

signals:
void addedToScene(int n);
void forwardMessage(const QString &msg);


protected:
virtual void paintEvent(QPaintEvent *event);
virtual void mousePressEvent(QMouseEvent *event);
virtual void mouseReleaseEvent(QMouseEvent *event);
virtual void mouseDoubleClickEvent (QMouseEvent * event );
virtual void mouseMoveEvent (QMouseEvent * event );
void calImMaxSize();

private:
QVector<QImage> m_imgs;
QPainter::CompositionMode m_mode;
QGraphicsScene *m_gscene;
QSize m_imMaxSize;

bool m_resizeable;
bool m_keepRatio;

GraphicsShape m_drawShape;	//Type of drawing to do.
QVector<QPoint> m_lastPoints; //The list of last points.
};

#endif /* PAINTIMAGEWIDGET_H_ */
