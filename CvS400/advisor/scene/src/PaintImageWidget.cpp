#include "PaintImageWidget.h"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Log.hpp"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
PaintImageWidget::PaintImageWidget(QWidget* parent)
:QWidget(parent), m_gscene(NULL), m_resizeable(false), m_keepRatio(true)
{
	 m_gscene = new QGraphicsScene(this);
	m_mode = QPainter::CompositionMode_SourceOver;
	this->setMinimumSize(30,20);
	this->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
}

PaintImageWidget::~PaintImageWidget() {

}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

void PaintImageWidget::addImage(const QImage& img)
{
	if(!img.isNull())
	{
		m_imgs.append(img);
		this->calImMaxSize();
	}
}

void PaintImageWidget::addImages(const QVector<QImage>& imgs)
{
	if(!imgs.isEmpty())
	{
		m_imgs.clear(); //Clear old vector
		m_imgs = imgs;
		this->calImMaxSize();
	}
}

bool PaintImageWidget::delLastImage()
{
	if(!m_imgs.isEmpty())
	{
		m_imgs.pop_back(); //Clear the last one
		this->calImMaxSize();
		return true;
	}
	return false;
}

void PaintImageWidget::clearImages()
{
	m_imgs.clear();
	this->calImMaxSize();
}


void PaintImageWidget::validateActualGItem()
{
	QSizeF size = m_drawShape.property(GraphicsShape::RealSize).toSizeF();

	if(size.isValid()  && size.width() > 0)
	{
		if(m_gscene) m_drawShape.addToScene(m_gscene);
		update();
		emit addedToScene(0);
	}
	else
	{
		emit forwardMessage("The real size of the shape need to be set correctly before validating it");
	}
}

void PaintImageWidget::ignoreActualGItem()
{
	m_drawShape.clear();
	update();
}

void PaintImageWidget::saveScene(const QString& fname)
{
        X7S_FUNCNAME("PaintImageWidget::saveScene()");

        QSettings *qSetting=NULL;
        if(!fname.isEmpty()) qSetting = new QSettings(fname,QSettings::IniFormat);
        else qSetting = new QSettings();
        qSetting->beginGroup("scene");
        qSetting->remove(""); //Remove all entry in this group


        QList<QGraphicsItem*> gItemList = m_gscene->items();

        AVO_PRINT_DEBUG(funcName,"nItems=%d %s",gItemList.size(),fname.toStdString().c_str());
        for(int i=0;i<gItemList.size();i++)
        {
                if(gItemList[i])
                {
                        QByteArray ba;
                        QDataStream stream(&ba,QIODevice::WriteOnly);
                        stream << gItemList[i];
                        qSetting->setValue(QString("item_%1").arg(i),ba);
                }
        }
        qSetting->endGroup();
        delete qSetting;
}

void PaintImageWidget::loadScene(const QString& fname)
{
        X7S_FUNCNAME("PaintImageWidget::loadScene()");

        QSettings *qSetting=NULL;
        if(!fname.isEmpty()) qSetting = new QSettings(fname,QSettings::IniFormat);
        else qSetting = new QSettings();
        qSetting->beginGroup("scene");
        m_gscene->clear(); //Clear the scene of the current object.

        QStringList keys = qSetting->allKeys();
        AVO_PRINT_DEBUG(funcName,"nItems=%d",keys.size());


        for(int i=0;i<keys.size();i++)
        {
                if(!keys[i].isEmpty())
                {
                        QByteArray ba = qSetting->value(keys[i]).toByteArray();
                        QDataStream stream(&ba,QIODevice::ReadOnly);
                        QGraphicsItem *gItem =NULL;
                        stream >> &gItem;
                        m_gscene->addItem(gItem);
                }
        }
        qSetting->endGroup();
        delete qSetting;

        update();
}

void PaintImageWidget::clearScene()
{
        int nItems=m_gscene->items().size();
        if(nItems>0)
        {

                int result = QMessageBox::question(this,
                                tr("Clear the Scene"),
                                tr("Are you sure to delete all the %n item(s) from the scene.","",nItems),
                                QMessageBox::Ok | QMessageBox::Cancel);
                if (result == QMessageBox::Ok)
                {
                        m_gscene->clear();
                        update();
                }
        }

}

void PaintImageWidget::removeLastGItem()
{
        if(!m_gscene->items().isEmpty())
        {
                QGraphicsItem* lastItem = m_gscene->items(Qt::DescendingOrder).first();
                m_gscene->removeItem(lastItem);
                delete lastItem;
                update();
        }
}


void PaintImageWidget::startDrawing(const GraphicsShape& shape)
{
	if(shape.isValid()) //Start drawing mode
	{
		setMouseTracking(true);
		setCursor(Qt::CrossCursor);

		emit forwardMessage("You should not forget to validate the shape and set a correct real size");
	}
	else //Stop the drawing mode
	{
		setMouseTracking(false);
		setCursor(Qt::ArrowCursor);
	}

	m_drawShape=shape;
	update();
}


bool PaintImageWidget::setShapeProperty(GraphicsShape::Properties prop, const QVariant& value)
{
	return m_drawShape.setProperty(prop,value);
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

void PaintImageWidget::calImMaxSize()
{
	m_imMaxSize=QSize(-1,-1);

	for(int i=0;i<m_imgs.size();i++)
	{
		const QImage& image = m_imgs[i];
		if(!image.isNull() &&
				(image.width() > m_imMaxSize.width()) &&
				(image.height() > m_imMaxSize.height()))
			m_imMaxSize= image.size();
		this->setFixedSize(m_imMaxSize);
		this->setMinimumSize(m_imMaxSize);
		if(m_gscene) m_gscene->setSceneRect(QRectF(0,0,m_imMaxSize.width(),m_imMaxSize.height()));
	}

}


void PaintImageWidget::paintEvent(QPaintEvent *event)
{

	QPainter painter(this);

	//========== Draw the loaded image in transparency.
	painter.setCompositionMode(m_mode);
	for(int i=0;i<m_imgs.size();i++)
		painter.drawImage(QPoint(),m_imgs[i]);


	//========== Then Draw the actual scene
	if(m_gscene) m_gscene->render(&painter);


	//=========== End finally draw the drawing shape
	m_drawShape.paint(&painter);
}



/**
* @brief Create or dump a GraphicShape when a click is done.
*
* 		- First click, create the Drawing shape.
* 		- Second click:
* 			- Dump the 2nd point.
* 			- Add one more point to the list of points.
*
* @param event
*/
void PaintImageWidget::mousePressEvent(QMouseEvent *event)
{
	this->m_drawShape.forwardMouseEvent(event);
}

void PaintImageWidget::mouseReleaseEvent(QMouseEvent *event)
{
	this->m_drawShape.forwardMouseEvent(event);

	GraphicsShape::Mode mode = this->m_drawShape.mode();

	if(mode==GraphicsShape::Editing || mode==GraphicsShape::Creating)
	{
		setCursor(Qt::CrossCursor);
	}
	else if(mode == GraphicsShape::Moving)
	{
		setCursor(Qt::OpenHandCursor);
	}
	else
	{
		//Obtain the actual mode and set the cursor
		setCursor(Qt::UpArrowCursor);
	}

	//Finally update the image
	update();


}


/**
* @brief Dump the points when the shape has more than two points
*
* For polygon, path, etc... there is more than two points to add, therefore
* to know when we don't need to add more point we just capture the double click event.
*
* @param event (A double click event)
*/
void PaintImageWidget::mouseDoubleClickEvent ( QMouseEvent * event )
{
	bool dump = this->m_drawShape.forwardMouseEvent(event);
	if(dump) {
		validateActualGItem();
	}
}

/**
* @brief Move the "editable/last" point of the drawingShape
* @param event A Mouse event.
*/
void PaintImageWidget::mouseMoveEvent(QMouseEvent * event )
{

	this->m_drawShape.forwardMouseEvent(event);

	if(m_drawShape.isValid() && !m_lastPoints.isEmpty())
	{
		//Set the last points to the mouse position
		//m_lastPoints.last()= event->pos();
	}
	update();
}


