#include "MainWindow.h"
#include "Log.hpp"

#include <QtGui>
#include <QApplication>



int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	Log::SetLevel(X7S_LOGLEVEL_HIGHEST);

	QCoreApplication::setOrganizationName("Seven Solutions");
	QCoreApplication::setApplicationName("AVOScene");
	QSettings::setDefaultFormat(QSettings::IniFormat);

	MainWindow win;
	win.show();
	win.OpenVideo(QString(argv[1]));
	return a.exec();
}
