/*
 * GraphicsShape.h
 *
 *  Created on: 31/03/2010
 *      Author: Ben
 */

#ifndef _GRAPHICSSHAPE_H_
#define _GRAPHICSSHAPE_H_

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QPoint>
#include <QVector>
#include <QHash>
#include <QMouseEvent>

class GraphicsShape
{
public:
	enum Properties
	{
		Subtype=Qt::UserRole+1,	//!< Subtype of the graphic shape
		FixedSize,	//!< Fixed size of the object
		RealSize,	//!< Real size of the object
	};


	enum Mode
	{
		Iddle=0,	//!< No mode defined.
		Creating,	//!< Create the shape (by adding or moving last point)
		Moving,		//!< Move the shape
		Rotate,		//!< Rotate the shape
		Editing,		//!< Editing the actual points
		Viewing,	//!< View the point
	};

	enum Type {
		None=0,
				Line=QGraphicsLineItem::Type,
				Rectangle=QGraphicsRectItem::Type,
				Ellipse=QGraphicsEllipseItem::Type,
				Path=QGraphicsPathItem::Type,
				Polygon=QGraphicsPolygonItem::Type,
				SimpleText=QGraphicsSimpleTextItem::Type,
				Text=QGraphicsTextItem::Type,
				Pixmap=QGraphicsPixmapItem::Type,
				Point=QGraphicsItem::UserType+1,
	};

public:
	GraphicsShape(GraphicsShape::Type type=None, int subtype=-1, const QColor& color=QColor());
	virtual ~GraphicsShape();


	bool setColor(const QColor& color, Mode mode=Iddle);
	bool setStyle(Mode mode, const QPen& pen=QPen(),const QBrush& brush=QBrush(), const QFont& font=QFont());

	bool setProperty(Properties prop, const QVariant& value);
	QVariant property(Properties prop) const;

	bool setMode(Mode mode);
	Mode mode() {return m_mode; }

	Type type()  { return m_type; }
	bool isType(Type type) const { return type==m_type; };
	bool isValid() const { return m_type!=None; }
	int  nPointsRequired() const;


	void clear();
	void paint(QPainter* painter) const;
	void addToScene(QGraphicsScene *scene);
	void updateFixedSize();

	QGraphicsItem* toQGraphicsItem(QGraphicsItem *parent=NULL) const;
	bool fromQGraphicsItem(QGraphicsItem *gitem);
	bool forwardMouseEvent(QMouseEvent *evt);


	static QString modeName(Mode mode);
	static QList<Properties> getPropertyNames();


	QLineF toLineF();
	QRectF toRectF();
	QPolygonF toPolygonF();



protected:
	void applyStyle(Mode mode,QGraphicsItem *gitem) const;


public:


private:
	Type m_type;
	Mode m_mode;
	QHash<Properties,QVariant> m_propList;
	QHash<Mode,QPen> m_qpens;
	QHash<Mode,QBrush> m_qbrushes;
	QHash<Mode,QFont> m_qfonts;

	bool m_fixedRatio;
	QPointF m_pDown, m_offset;
	QVector<QPointF> m_pts;
};

#ifndef QT_NO_DATASTREAM
QDataStream &operator>>(QDataStream &stream,QGraphicsItem** ppGItem);
QDataStream &operator<<(QDataStream &stream,const QGraphicsItem* ppGItem);
#endif /*QT_NO_DATASTREAM*/



#endif /* _GRAPHICSSHAPE_H_ */
