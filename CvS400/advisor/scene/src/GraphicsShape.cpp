#include "GraphicsShape.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Log.hpp"
#include <QGraphicsScene>
#include <QStyleOptionGraphicsItem>






//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
GraphicsShape::GraphicsShape(Type type, int subtype, const QColor& color)
: m_type(type), m_mode(Iddle), m_offset(0,0)
{
	setProperty(Subtype,subtype);

	QColor colBrush=color;

	colBrush.setAlpha(10);
	setStyle(Viewing,QPen(color,1,Qt::SolidLine), QBrush(colBrush));
	colBrush.setAlpha(50);
	setStyle(Creating,QPen(color,2,Qt::DashDotDotLine), QBrush(colBrush));
	setStyle(Iddle,QPen(color,2,Qt::DotLine), QBrush(colBrush)); //Iddle is default state.

}


GraphicsShape::~GraphicsShape()
{

}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------


int GraphicsShape::nPointsRequired() const
{
	switch(m_type)
	{
	case Point:
	case Text:
	case SimpleText:
		return 1;
	case Line:
	case Rectangle:
	case Ellipse:
		return 2;
	case Polygon:
	case Path:
		return 3; //!< Require at least 3 points (triangle).
	case None:
	default:
		return 0;
	}
}



bool GraphicsShape::setProperty(Properties prop,const QVariant& value)
{
	m_propList[prop]=value;
	return true;
}

QVariant GraphicsShape::property(Properties prop) const
{
	return m_propList[prop];
}

bool GraphicsShape::setMode(Mode mode)
{
	if(!(m_mode==Creating || m_mode==Iddle))
	{
		if(m_pts.size()<nPointsRequired()) {
			avoWarning("GraphicsShape::setMode()") << "Can not set the mode=" << mode;
			return false;
		}
	}

	m_mode=mode;
	return true;
}


bool GraphicsShape::setColor(const QColor& color, Mode mode)
{
	if(mode!=Iddle)
	{
		m_qpens[mode].setColor(color);
		m_qbrushes[mode].setColor(color);
	}
	else
	{

	}
	return true;
}


bool GraphicsShape::setStyle(Mode mode, const QPen& pen,const QBrush& brush, const QFont& font)
{
	m_qpens[mode]=pen;
	m_qbrushes[mode]=brush;
	m_qfonts[mode]=font;

	return true;
}


void GraphicsShape::clear()
{
	m_pDown=QPointF(0,0);
	m_offset=QPointF(0,0);
	m_pts.clear();
	m_mode=Creating;
}


void GraphicsShape::paint(QPainter *pPainter) const
{
	if(this->isValid() && !m_pts.empty())
	{
		QGraphicsItem* gItem = this->toQGraphicsItem();
		QStyleOptionGraphicsItem options;
		if(gItem && pPainter) gItem->paint(pPainter,&options);
		delete gItem;
	}
}

void GraphicsShape::addToScene(QGraphicsScene *scene)
{
	if(m_pts.size()>=nPointsRequired())
	{
		setMode(Viewing);
		this->updateFixedSize();
		QGraphicsItem *gItem = toQGraphicsItem();
		scene->addItem(gItem);
		clear();
	}
}


void GraphicsShape::updateFixedSize()
{

	X7S_FUNCNAME("GraphicsShape::updateFixedSize()");

	if(m_pts.size()>=nPointsRequired())
	{

		//Obtain the list of points to create a GraphicItems.
		if(m_type==GraphicsShape::Line)
		{
			QLineF line(m_pts[0],m_pts[1]);
			setProperty(FixedSize,QSizeF(line.length(),1));
		}
		else if(m_type==GraphicsShape::Rectangle)
		{
			QRectF rect(m_pts[0],m_pts[1]);
			setProperty(FixedSize,rect.size());
		}
		else
		{

		}
	}
	else
	{
		AVO_PRINT_INFO(funcName,"Not all the points are given");
	}


}



/**
* @brief Transform the actual GraphicShape to a QGraphicsItem
* @param parent Parent Item
* @return
*/
QGraphicsItem* GraphicsShape::toQGraphicsItem(QGraphicsItem *parent) const {

	X7S_FUNCNAME("GraphicsShape::toQGraphicsItem()");

	QGraphicsItem *gItem=NULL;

	QVector<QPointF> pts=m_pts;
	for(int i=0;i<pts.size();i++) pts[i]+=m_offset;


	if(pts.size() <2)
	{
		if(m_type==GraphicsShape::Point)
		{

		}
		else if(m_type==GraphicsShape::Text)
		{
			QGraphicsTextItem *gTxtItem= new QGraphicsTextItem("test",parent);
			if(gTxtItem)
			{
				gTxtItem->setPos(pts[0]);
			}
			gItem=gTxtItem;
		}
	}
	else
	{
		//Obtain the list of points to create a GraphicItems.
		if(m_type==GraphicsShape::Line)
		{
			QLineF line(pts[0],pts[1]);
			gItem=new QGraphicsLineItem(line,parent);
		}
		else if(m_type==GraphicsShape::Polygon)
		{
			QPolygonF poly(pts);
			gItem=new QGraphicsPolygonItem(poly,parent);
		}
		else if(m_type==GraphicsShape::Rectangle)
		{
			QRectF rect(pts[0],pts[1]);
			gItem=new QGraphicsRectItem(rect,parent);
		}
		else if(m_type==GraphicsShape::Ellipse)
		{
			QRectF rect(pts[0],pts[1]);
			gItem=new QGraphicsEllipseItem(rect,parent);
		}
		else
		{
			AVO_PRINT_INFO(funcName,"bad type %d",m_type);
		}

	}

	if(gItem) {

		applyStyle(m_mode,gItem);
		QHashIterator<Properties,QVariant> i(m_propList);
		while(i.hasNext())
		{
			i.next();
			gItem->setData(i.key(),i.value());
		}
	}
	else
	{
		AVO_PRINT_INFO(funcName,"can not create item (type=%d, nPts=%d)"
				,m_type,pts.size());
	}
	return gItem;
}

/**
* @brief Create the GraphicsShape from a QGraphicsItems
*
* This function obtain the point position but not the style of how the line is draw.
*
* @param gitem
* @return @true if the operation is performed correclty, @false otherwise.
*/
bool GraphicsShape::fromQGraphicsItem(QGraphicsItem *gItem)
{
	X7S_FUNCNAME("GraphicsShape::fromQGraphicsItem()");

	//Check first argument.
	AVO_CHECK_WARN(funcName,gItem,false,"gItem is null");

	//Clear actual value
	this->clear();

	QGraphicsLineItem 		*gLine=NULL;
	QGraphicsRectItem 		*gRect=NULL;
	QGraphicsEllipseItem 	*gElli= NULL;
	QGraphicsPolygonItem 	*gPoly=NULL;

	switch(gItem->type())
	{
	case QGraphicsLineItem::Type:
		gLine = (QGraphicsLineItem*)gItem;
		m_pts << gLine->line().p1();
		m_pts << gLine->line().p2();
		m_type=GraphicsShape::Line;
		break;
	case QGraphicsRectItem::Type:
		gRect = (QGraphicsRectItem*)gItem;
		m_pts << gRect->rect().topLeft();
		m_pts << gRect->rect().bottomRight();
		m_type=GraphicsShape::Rectangle;
		break;
	case QGraphicsEllipseItem::Type:
		gElli = (QGraphicsEllipseItem*)gItem;
		m_pts << gElli->rect().topLeft();
		m_pts << gElli->rect().bottomRight();
		m_type=GraphicsShape::Ellipse;
		break;
	case QGraphicsPolygonItem::Type:
		gPoly = (QGraphicsPolygonItem*)gItem;
		m_pts << gPoly->polygon();
		m_type=GraphicsShape::Polygon;
		break;
	case QGraphicsTextItem::Type:
	default:
		avoWarning(funcName) << "Shape type " << gItem->type() << " is not defined";
		return false;
	}


	QList<Properties> props = getPropertyNames();
	QListIterator<Properties> i(props);
	while(i.hasNext())
	{
		Properties prop = i.next();
		QVariant val = gItem->data(prop);
		if(!val.isNull()) m_propList[prop]=val;
	}


	return true;
}


/**
* @brief Take a mouse event forwarded and handle it appropriately
* @param evt
* @return
*/
bool GraphicsShape::forwardMouseEvent(QMouseEvent *event)
{
	bool ret=false;
	X7S_FUNCNAME("GraphicsShape::forwardMouseEvent()");
	AVO_CHECK_ERROR(funcName,event,ret,"event==NULL");
	AVO_CHECK_WARN(funcName,isValid(),ret,"Not valid GraphicShape")


	//================================================================================
	if(event->type()==QEvent::MouseMove)
	{
		if(m_pts.isEmpty()) return ret;
		else if(m_mode==Creating)
		{
			//Set the last points to the mouse position
			m_pts.last()= event->pos();
		}
		else if(m_mode==Moving)
		{
			//Add an offset to all the points
			if(!m_pDown.isNull())
			{
				m_offset = event->posF()-m_pDown;
			}
		}
	}
	//================================================================================
	else if(event->type()==QEvent::MouseButtonDblClick)
	{
		if(event->button()==Qt::LeftButton)
		{
			if(m_pts.size()>=0)
			{
				avoDebug(funcName) << m_pts.size();
				return true;
			}
		}
	}
	//================================================================================
	else if(event->type()==QEvent::MouseButtonPress)
	{
		avoDebug(funcName) << "press button:" << event->button() << "(mode:" << modeName(m_mode) << ")";

		if(event->button()==Qt::LeftButton)
		{

			if(m_mode==Creating || m_mode==Iddle)
			{
				//If its empty set the two first points
				if(m_pts.isEmpty()) {
					m_pts.push_back(event->posF()); //Point A (fixed)
					m_pts.push_back(event->posF()); //Point B (moving but initialized as A)
					setMode(Creating);
					avoDebug(funcName) << "Start: " << event->pos();
				}
				//If there is more than 2 points
				else  if(this->nPointsRequired()>2)
				{
					m_pts.push_back(event->posF()); //(last moving point)
				}
				else
				{
					setMode(Moving);
				}
			}
			else if(m_mode==Moving)
			{
				//Stop adding one points (can be moving points).
				m_pDown=event->posF();
			}
			else
			{

			}
		}
		else if(event->button()==Qt::MidButton) //Like a reset
		{
			//Clear when we press the middle button
			m_pts.clear();
		}
		else if(event->button()==Qt::RightButton)
		{
			//Change the mode with right button
			if(m_mode==Iddle) m_mode=Creating;
			else if(m_mode==Creating) m_mode=Moving;
			else if(m_mode==Moving) m_mode=Editing;
			else if(m_mode==Editing) m_mode=Viewing;
			else if(m_mode==Viewing) m_mode=Moving;
			else avoWarning(funcName) << "unknown mode" << m_mode;

		}
		else
		{
			avoDebug(funcName) << "unkown button" << event->button();
		}

	}
	//================================================================================
	else if(event->type()==QEvent::MouseButtonRelease)
	{
		m_pDown=QPointF();
		if(event->button()==Qt::LeftButton)
		{
			if(m_mode==Iddle) m_mode=Creating;
			else if(m_mode==Moving) m_mode=Viewing;
		}
		avoDebug(funcName) << "release button:" << event->button() << "(next mode:" << modeName(m_mode) << ")";

	}
	//================================================================================
	else
	{

		avoWarning(funcName) << "Bad type of event" << event->type();
	}

	return ret;

}



QLineF GraphicsShape::toLineF()
{
	if(m_pts.size()>=2)
	{
		return QLineF(m_pts[0], m_pts[1]);
	}
	return QLineF();
}

QRectF GraphicsShape::toRectF()
{
	if(m_pts.size()>=2)
	{
		return QRectF(m_pts[0], m_pts[1]);
	}
	return QRectF();
}

QPolygonF GraphicsShape::toPolygonF()
{
	if(m_pts.empty()==false)
	{
		return QPolygonF(m_pts);
	}
	return QPolygonF();
}



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Static Functions
//----------------------------------------------------------------------------------------

QString GraphicsShape::modeName(Mode mode)
{
	switch(mode)
	{
	case Iddle: return "Iddle";
	case Creating: return "Creating";
	case Moving: return "Moving";
	case Rotate: return "Rotate";
	case Editing: return "Editing";
	case Viewing: return "Viewing";
	default: return "Error";
	}
}


QList<GraphicsShape::Properties> GraphicsShape::getPropertyNames()
{
	QList<Properties> props;
	props << Subtype << FixedSize << RealSize;
	return props;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------


void GraphicsShape::applyStyle(Mode mode, QGraphicsItem *gItem) const
{
	X7S_FUNCNAME("GraphicsShape::applyStyle()");

	QGraphicsLineItem 		*gLine=NULL;
	QGraphicsRectItem 		*gRect=NULL;
	QGraphicsEllipseItem 	*gElli= NULL;
	QGraphicsPolygonItem 	*gPoly=NULL;
	//	QGraphicsTextItem 		*gText = NULL;

	QPen pen;
	QBrush brush;
	QFont font;

	AVO_CHECK_WARN(funcName,gItem,,"QGraphicsItem is NULL");


	if(m_qpens.find(mode)!=m_qpens.end()) pen=m_qpens[mode];
	else pen=m_qpens[Iddle];
	if(m_qbrushes.find(mode)!=m_qbrushes.end()) brush=m_qbrushes[mode];
	else brush=m_qbrushes[Iddle];
	if(m_qfonts.find(mode)!=m_qfonts.end()) font=m_qfonts[mode];
	else font=m_qfonts[Iddle];

	switch(gItem->type())
	{
	case QGraphicsLineItem::Type:
		gLine = (QGraphicsLineItem*)gItem;
		gLine->setPen(pen);
		break;
	case QGraphicsRectItem::Type:
		gRect = (QGraphicsRectItem*)gItem;
		gRect->setPen(pen);
		gRect->setBrush(brush);
		break;
	case QGraphicsEllipseItem::Type:
		gElli = (QGraphicsEllipseItem*)gItem;
		gElli->setPen(pen);
		gElli->setBrush(brush);
		break;
	case QGraphicsPolygonItem::Type:
		gPoly = (QGraphicsPolygonItem*)gItem;
		gPoly->setPen(pen);
		gPoly->setBrush(brush);
		break;
	default:
		avoWarning(funcName) << "Shape type " << gItem->type() << " is not defined";
	}
}






//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	External Functions
//----------------------------------------------------------------------------------------


QDataStream &operator<<(QDataStream &stream, const QGraphicsItem *gItem)
{
	X7S_FUNCNAME("GraphicsShape::&operator<<()");

	QGraphicsLineItem 		*gLine=NULL;
	QGraphicsRectItem 		*gRect=NULL;
	QGraphicsEllipseItem 	*gElli= NULL;
	QGraphicsPolygonItem 	*gPoly=NULL;
	QGraphicsTextItem 		*gText = NULL;

	if(gItem)
	{
		stream << gItem->type();

		switch(gItem->type())
		{
		case QGraphicsLineItem::Type:
			gLine = (QGraphicsLineItem*)gItem;
			stream << gLine->line();
			stream << gLine->pen();
			break;
		case QGraphicsRectItem::Type:
			gRect = (QGraphicsRectItem*)gItem;
			stream << gRect->rect();
			stream << gRect->pen();
			stream << gRect->brush();
			break;
		case QGraphicsEllipseItem::Type:
			gElli = (QGraphicsEllipseItem*)gItem;
			stream << gElli->rect();
			stream << gElli->pen();
			stream << gElli->brush();
			break;
		case QGraphicsPolygonItem::Type:
			gPoly = (QGraphicsPolygonItem*)gItem;
			stream << gPoly->polygon();
			stream << gPoly->pen();
			stream << gPoly->brush();
			break;
		case QGraphicsTextItem::Type:
			gText = (QGraphicsTextItem*)gItem;
			stream << gText->pos();
			stream << gText->toHtml();
			break;
		default:
			avoWarning(funcName) << "Shape type " << gItem->type() << " is not defined";
		}
		stream << gItem->data(Qt::UserRole);


		QList<GraphicsShape::Properties> props = GraphicsShape::getPropertyNames();
		QListIterator<GraphicsShape::Properties> i(props);
		QVariant val;
		while(i.hasNext())
		{
			GraphicsShape::Properties prop = i.next();
			stream << gItem->data(prop);
		}


	}
	else avoInfo(funcName) << "QGraphicsItem is NULL";

	return stream;

}


QDataStream &operator>>(QDataStream &stream,QGraphicsItem** ppGItem)
{
	X7S_FUNCNAME("GraphicsShape::&operator>>()");

	int type;
	QPen pen;
	QBrush brush;
	QString html;
	QPointF pos;
	QLineF line;
	QRectF rect;
	QPolygonF poly;
	QVariant subtype;

	QGraphicsLineItem 		*gLine=NULL;
	QGraphicsRectItem 		*gRect=NULL;
	QGraphicsEllipseItem 	*gElli= NULL;
	QGraphicsPolygonItem 	*gPoly=NULL;
	QGraphicsTextItem 		*gText = NULL;


	stream >> type;
	switch(type)
	{
	case QGraphicsLineItem::Type:
		stream >> line;
		stream >> pen;
		gLine = new QGraphicsLineItem(line);
		gLine->setPen(pen);
		*ppGItem = (QGraphicsItem*)gLine;
		break;
	case QGraphicsRectItem::Type:
		stream >> rect;
		stream >> pen;
		stream >> brush;
		gRect = new QGraphicsRectItem(rect);
		gRect->setPen(pen);
		gRect->setBrush(brush);
		*ppGItem = (QGraphicsItem*)gRect;
		break;
	case QGraphicsEllipseItem::Type:
		stream >> rect;
		stream >> pen;
		stream >> brush;
		gElli = new QGraphicsEllipseItem(rect);
		gElli->setPen(pen);
		gElli->setBrush(brush);
		*ppGItem = (QGraphicsItem*)gElli;
		break;
	case QGraphicsPolygonItem::Type:
		stream >> poly;
		stream >> pen;
		stream >> brush;
		gPoly = new QGraphicsPolygonItem(poly);
		gPoly->setPen(pen);
		gPoly->setBrush(brush);
		*ppGItem = (QGraphicsItem*)gPoly;
		break;
	case QGraphicsTextItem::Type:
		stream >> pos;
		stream >> html;
		gText = new QGraphicsTextItem(html);
		gText->setPos(pos);
		*ppGItem = (QGraphicsItem*)gText;
		break;
	default:
		avoWarning(funcName) << "Shape type " << type << " is not defined";
	}

	stream >> subtype;
	(*ppGItem)->setData(Qt::UserRole,subtype);
	avoDebug(funcName) << subtype;


	QList<GraphicsShape::Properties> props = GraphicsShape::getPropertyNames();
	QListIterator<GraphicsShape::Properties> i(props);
	QVariant val;
	while(i.hasNext())
	{
		GraphicsShape::Properties prop = i.next();
		stream >> val;
		if(!val.isNull()) (*ppGItem)->setData(prop,val);
		avoDebug(funcName) << prop << val;
	}


	return stream;
}
