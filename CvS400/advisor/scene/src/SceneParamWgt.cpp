#include "SceneParamWgt.h"

#include "ui_SceneParamWgt.h"



#include <QErrorMessage>
#include <QProgressDialog>
#include <QString>
#include <cstdlib>
#include <QDebug>
#include "Log.hpp"


#include "x7svscv.h"



//-------------------------------------------------------------
// Variables usadas
//-------------------------------------------------------------
QErrorMessage *WinMsg;



SceneParamWgt::SceneParamWgt(QWidget *parent, const QSize& size) :
    		QWidget(parent),
    		ui(new Ui::SceneParamWgt)
{
	Scene= new x7s::Scene2D3D(size.height(),size.width());
	int i = Scene->CreatePlane("Plano1");
	ui->setupUi(this);

	avoDebug("SceneParamWgt::SceneParamWgt");

	connect(this,SIGNAL(estimationStarting()),this,SLOT(startEstimation()));
	connect(this,SIGNAL(estimationEnded()),this,SLOT(endEstimation()));

}
//-------------------------------------------------------------
SceneParamWgt::~SceneParamWgt()
{
	delete ui;
	delete Scene;
}


//-------------------------------------------------------------
void SceneParamWgt::BotonDisabled(){
	ui->Boton->setEnabled(false);
}



void SceneParamWgt::startEstimation()
{
	float MSE,ME;
	QString Message,Aux;
	WinMsg=new QErrorMessage();


	setCursor(Qt::WaitCursor);

	int i=Scene->FindPlane("Plano1");
	if(i==-1) return;

	DatosEscena3(i);



	Aux=ui->LEAlturaCamaraReal->text();
	bool EsNumero;

	float AlturaCamara=Aux.toFloat(&EsNumero);

	if(EsNumero){
		Scene->PlaneList.at(i)->CalibrateSystem(MSE,ME,AlturaCamara);
	}
	else{
		Scene->PlaneList.at(i)->CalibrateSystem(MSE,ME,-1);
	}

	//Escribimos los parametros de la camara
	ui->LEAlturaCamara->setText(Aux.setNum(Scene->PlaneList.at(i)->GetCameraHeight()));
	ui->LEDistanciaFocal->setText(Aux.setNum(Scene->PlaneList.at(i)->GetFocalDistance()));
	ui->LEAngulo->setText(Aux.setNum(Scene->PlaneList.at(i)->GetCameraAngle()));

	ui->LEMSE->setText(Aux.setNum(MSE));
	ui->LEME->setText(Aux.setNum(ME));

	setCursor(Qt::ArrowCursor);
	emit estimationEnded();
}

void SceneParamWgt::endEstimation()
{
	ui->Boton->setEnabled(true);
}


void SceneParamWgt::BotonClickedParametros()
{
	ui->Boton->setEnabled(false);
	ui->BotonAltura->setEnabled(false);
	emit estimationStarting();

}


//-------------------------------------------------------------
void SceneParamWgt::BotonClickedAltura(){

	QString Aux;
	int AlturaPixeles,PosY;

	Aux=ui->LEAlturaPixeles->text();
	AlturaPixeles=Aux.toInt();

	Aux=ui->LEPosYAltura->text();
	PosY=Aux.toInt();

	Aux.setNum(Scene->PlaneList.at(Scene->FindPlane("Plano1"))->GetDimension(PosY,AlturaPixeles));
	ui->LEAlturamm->setText(Aux);

}
//-------------------------------------------------------------
void SceneParamWgt::BotonClickedLineaHorizonte(){
	QString Aux;

	ui->LELineaHorizonte->setText(Aux.setNum(Scene->PlaneList.at(Scene->FindPlane("Plano1"))->CalculateHorizonLine()));
}
//-------------------------------------------------------------
void SceneParamWgt::BotonClickedDistancia(){

	QString Aux;
	int PosX,PosY;

	Aux=ui->LEPosX->text();
	PosX=Aux.toInt();
	Aux=ui->LEPosY->text();
	PosY=Aux.toInt();

	Aux.setNum(Scene->PlaneList.at(Scene->FindPlane("Plano1"))->GetDistance(PosX,PosY));
	ui->LEDistanciaTotal->setText(Aux);

	Aux.setNum(Scene->PlaneList.at(Scene->FindPlane("Plano1"))->GetDistanceX(PosX,PosY));
	ui->LEDistanciaX->setText(Aux);

	Aux.setNum(Scene->PlaneList.at(Scene->FindPlane("Plano1"))->GetDistanceY(PosY));
	ui->LEDistanciaY->setText(Aux);

}
//-------------------------------------------------------------
void SceneParamWgt::DatosEscena0(int i){
	//----------------------------------------------------------------
	//Introducimos los datos
	//----------------------------------------------------------------
	//Insertamos la alturas reales en mm
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectRealHeightVector.clear();
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(1790);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(1630);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(1790);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(1630);
	//----------------------------------------------------------------
	//Insertamos la alturas en pixeles
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.clear();
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(212.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(145.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(122.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(86.0);
	//----------------------------------------------------------------
	//Insertamos la posicion en el eje y del objeto
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectPositionVector.clear();
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(532.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(366.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(238.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(164.0);
	//----------------------------------------------------------------
	//Insertamos las coordenadas de los dos puntos de la distancia
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->DistancePositionsVector.clear();
	//Scene->PlaneList.at(i)->AddDistanceVector(360.0,570.0,360.0,550.0);
	//Scene->PlaneList.at(i)->AddDistanceVector(360.0,552.0,360.0,378.0);
	Scene->PlaneList.at(i)->AddDistanceVector(360.0,378.0,360.0,238.0);
	Scene->PlaneList.at(i)->AddDistanceVector(360.0,238.0,360.0,159.0);
	//----------------------------------------------------------------
	//Insertamos la distancia en mm
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->DistancesVector.clear();
	//Scene->PlaneList.at(i)->DistancesVector.push_back(3270);
	Scene->PlaneList.at(i)->DistancesVector.push_back(5000);
	Scene->PlaneList.at(i)->DistancesVector.push_back(5000);
}
//----------------------------------------------------------------
void SceneParamWgt::DatosEscena1(int i){
	//----------------------------------------------------------------
	//Introducimos los datos
	//----------------------------------------------------------------
	//Insertamos la alturas reales en mm
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectRealHeightVector.clear();
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(2720);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(2960);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(3040);

	//----------------------------------------------------------------
	//Insertamos la alturas en pixeles
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.clear();
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(81.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(84.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(78.0);
	//----------------------------------------------------------------
	//Insertamos la posicion en el eje y del objeto
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectPositionVector.clear();
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(409.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(325.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(280.0);
	//----------------------------------------------------------------
	//Insertamos las coordenadas de los dos puntos de la distancia
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->DistancePositionsVector.clear();
	//Scene->PlaneList.at(i)->AddDistanceVector(360.0,570.0,360.0,550.0);
	Scene->PlaneList.at(i)->AddDistanceVector(530.0,323.0,434.0,317.0);
	Scene->PlaneList.at(i)->AddDistanceVector(523.0,189.0,600.0,194.0);
	Scene->PlaneList.at(i)->AddDistanceVector(589.0,94.0,652.0,98.0);
	int num=Scene->PlaneList.at(i)->DistancePositionsVector.size();
	qDebug() <<"num=" <<num;

	//----------------------------------------------------------------
	//Insertamos la distancia en mm
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->DistancesVector.clear();
	//Scene->PlaneList.at(i)->DistancesVector.push_back(6730);
	Scene->PlaneList.at(i)->DistancesVector.push_back(4900);
	Scene->PlaneList.at(i)->DistancesVector.push_back(4900);
	Scene->PlaneList.at(i)->DistancesVector.push_back(4900);
}
//-------------------------------------------------------------
//----------------------------------------------------------------
void SceneParamWgt::DatosEscena2(int i){
	//----------------------------------------------------------------
	//Introducimos los datos
	//----------------------------------------------------------------
	//Insertamos la alturas reales en mm
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectRealHeightVector.clear();
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(4110.0);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(3060.0);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(1900.0);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(900.0);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(1165.0);

	//----------------------------------------------------------------
	//Insertamos la alturas en pixeles
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.clear();
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(204.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(112.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(56.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(19.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(24.0);
	//----------------------------------------------------------------
	//Insertamos la posicion en el eje y del objeto
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectPositionVector.clear();
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(358.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(248.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(182.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(140.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(109.0);
	//----------------------------------------------------------------
	//Insertamos las coordenadas de los dos puntos de la distancia
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->DistancePositionsVector.clear();
	Scene->PlaneList.at(i)->AddDistanceVector(354.0,139.0,401.0,181.0);
	Scene->PlaneList.at(i)->AddDistanceVector(403.0,182.0,478.0,246.0);
	Scene->PlaneList.at(i)->AddDistanceVector(484.0,248.0,615.0,353.0);
	int num=Scene->PlaneList.at(i)->DistancePositionsVector.size();
	qDebug() <<"num=" <<num;
	//----------------------------------------------------------------
	//Insertamos la distancia en mm
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->DistancesVector.clear();
	//Scene->PlaneList.at(i)->DistancesVector.push_back(6730);
	Scene->PlaneList.at(i)->DistancesVector.push_back(8520);
	Scene->PlaneList.at(i)->DistancesVector.push_back(8520);
	Scene->PlaneList.at(i)->DistancesVector.push_back(8520);
}
//-------------------------------------------------------------
void SceneParamWgt::DatosEscena3(int i){
	//----------------------------------------------------------------
	//Introducimos los datos
	//----------------------------------------------------------------
	//Insertamos la alturas reales en mm
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectRealHeightVector.clear();
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(1900.0);
	//    Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(1800.0);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(1900.0);
	//    Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(1900.0);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(1900.0);
	//    Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(1800.0);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(1800.0);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(1800.0);
	//    //----------------------------------------------------------------
	//    //Insertamos la alturas en pixeles
	//    //----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.clear();
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(70.0);
	//    Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(64.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(42.0);
	//    Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(53.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(74.0);
	//    Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(71.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(72.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(63.0);
	//    //----------------------------------------------------------------
	//    //Insertamos la posicion en el eje y del objeto
	//    //----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectPositionVector.clear();
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(528.0);
	//    Scene->PlaneList.at(i)->ObjectPositionVector.push_back(404.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(214.0);
	//    Scene->PlaneList.at(i)->ObjectPositionVector.push_back(304.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(565.0);
	//    Scene->PlaneList.at(i)->ObjectPositionVector.push_back(544.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(562.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(473.0);
	//----------------------------------------------------------------
	//Insertamos las coordenadas de los dos puntos de la distancia
	//----------------------------------------------------------------
	//Scene->PlaneList.at(i)->DistancePositionsVector.clear();
	//Scene->PlaneList.at(i)->AddDistanceVector(430.0,498.0,408.0,447.0);
	//    Scene->PlaneList.at(i)->AddDistanceVector(406.0,447.0,387.0,400.0);
	Scene->PlaneList.at(i)->AddDistanceVector(338.0,290.0,325.0,263.0);
	Scene->PlaneList.at(i)->AddDistanceVector(338.0,290.0,353.0,324.0);

	//----------------------------------------------------------------
	//Insertamos la distancia en mm
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->DistancesVector.clear();
	//Scene->PlaneList.at(i)->DistancesVector.push_back(6730);
	//Scene->PlaneList.at(i)->DistancesVector.push_back(2600);
	//Scene->PlaneList.at(i)->DistancesVector.push_back(2600);
	Scene->PlaneList.at(i)->DistancesVector.push_back(2600);
	Scene->PlaneList.at(i)->DistancesVector.push_back(2600);
}
//-------------------------------------------------------------
void SceneParamWgt::DatosEscena4(int i){
	//----------------------------------------------------------------
	//Introducimos los datos
	//----------------------------------------------------------------
	//Insertamos la alturas reales en mm
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectRealHeightVector.clear();
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(2410.0);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(2120.0);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(2410.0);
	Scene->PlaneList.at(i)->ObjectRealHeightVector.push_back(2020.0);
	//    //----------------------------------------------------------------
	//    //Insertamos la alturas en pixeles
	//    //----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.clear();
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(422.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(445.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(161.0);
	Scene->PlaneList.at(i)->ObjectPixelHeightVector.push_back(109.0);
	//    //----------------------------------------------------------------
	//    //Insertamos la posicion en el eje y del objeto
	//    //----------------------------------------------------------------
	Scene->PlaneList.at(i)->ObjectPositionVector.clear();
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(509.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(563.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(341.0);
	Scene->PlaneList.at(i)->ObjectPositionVector.push_back(323.0);
	//----------------------------------------------------------------
	//Insertamos las coordenadas de los dos puntos de la distancia
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->DistancePositionsVector.clear();
	Scene->PlaneList.at(i)->AddDistanceVector(252.0,509.0,261.0,413.0);
	Scene->PlaneList.at(i)->AddDistanceVector(261.0,413.0,289.0,369.0);
	Scene->PlaneList.at(i)->AddDistanceVector(420.0,499.0,386.0,413.0);

	//----------------------------------------------------------------
	//Insertamos la distancia en mm
	//----------------------------------------------------------------
	Scene->PlaneList.at(i)->DistancesVector.clear();
	Scene->PlaneList.at(i)->DistancesVector.push_back(4210);
	Scene->PlaneList.at(i)->DistancesVector.push_back(4070);
	Scene->PlaneList.at(i)->DistancesVector.push_back(3925);

}
//-------------------------------------------------------------



