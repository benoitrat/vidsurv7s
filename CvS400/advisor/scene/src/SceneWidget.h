#ifndef SCENEWIDGET_H
#define SCENEWIDGET_H

#include <QWidget>
#include <QImage>

//#include "SceneManager.h"

class PaintImageWidget;

namespace Ui {
    class SceneWidget;
}


namespace x7s {
	class Polygon;
}

class SceneWidget : public QWidget {
    Q_OBJECT
    Q_DISABLE_COPY(SceneWidget)
public:
    explicit SceneWidget(QWidget *parent = 0);
    virtual ~SceneWidget();
public slots:
    void drawImage(const QImage& image);
    void setDrawTool(QWidget* wgt);
    void saveScene();
    void loadScene();
    void itemAddedToScene(int n);
    void rcvMessage(const QString& string, int timeout=-1);

    void clickEstimateParams();
    void clickCheckParams();
    void estimateScene();

    void changeRealSize();
    void changeRealDist();

protected slots:
	virtual void accept();

private:
	enum SubType {  Sup, Dist, Peo, Car, NoGeo };

	QImage m_im;
    Ui::SceneWidget *m_ui;
    PaintImageWidget *m_paintImWgt;
    //SceneManager m_sceneMan;

};

#endif // SceneWidget_H
