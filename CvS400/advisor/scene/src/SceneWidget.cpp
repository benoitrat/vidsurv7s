#include "SceneWidget.h"
#include "ui_SceneWidget.h"


#include "SceneParamWgt.h"
#include "GraphicsShape.h"
#include "PaintImageWidget.h"
#include <QDateTime>
#include <QString>
#include <Log.hpp>
#include <QApplication>

#include <x7svscv.h>





SceneWidget::SceneWidget(QWidget *parent) :
QWidget(parent),  m_ui(new Ui::SceneWidget)
{
	m_ui->setupUi(this);
	m_paintImWgt = new PaintImageWidget(m_ui->mainFrame);

	QSignalMapper* mapper = new QSignalMapper(this);

	connect(m_ui->sel_tb,SIGNAL(pressed()),mapper,SLOT(map())); //Select tool button
	mapper->setMapping(m_ui->sel_tb,m_ui->sel_tb);
	connect(m_ui->dist_tb,SIGNAL(pressed()),mapper,SLOT(map())); //Car
	mapper->setMapping(m_ui->dist_tb,m_ui->dist_tb);
	connect(m_ui->peo_tb,SIGNAL(pressed()),mapper,SLOT(map())); //People
	mapper->setMapping(m_ui->peo_tb,m_ui->peo_tb);

	connect(m_ui->clearAll_pb,SIGNAL(pressed()),m_paintImWgt,SLOT(clearScene()));
	connect(m_ui->delLast_tb,SIGNAL(pressed()),m_paintImWgt,SLOT(removeLastGItem()));

	connect(m_ui->eleIgnore,SIGNAL(pressed()),m_paintImWgt,SLOT(ignoreActualGItem()));
	connect(m_ui->eleValidate,SIGNAL(pressed()),m_paintImWgt,SLOT(validateActualGItem()));

	connect(m_ui->peopleW_dsb,SIGNAL(valueChanged(double)),this,SLOT(changeRealSize()));
	connect(m_ui->peopleH_dsb,SIGNAL(valueChanged(double)),this,SLOT(changeRealSize()));
	connect(m_ui->dist_dsb,SIGNAL(valueChanged(double)),this,SLOT(changeRealDist()));

	connect(mapper,SIGNAL(mapped(QWidget*)),this,SLOT(setDrawTool(QWidget*)));
	connect(	m_paintImWgt,SIGNAL(addedToScene(int)),this,SLOT(itemAddedToScene(int)));
	connect(	m_paintImWgt,SIGNAL(forwardMessage(const QString&)),this,SLOT(rcvMessage(const QString&)));

	itemAddedToScene(0);

}

SceneWidget::~SceneWidget()
{
	delete m_ui;
}



void SceneWidget::drawImage(const QImage& im)
{
	if(!im.isNull())
	{
		m_im = im.copy();	//Make a copy

		m_paintImWgt->clearImages();
		m_paintImWgt->addImage(m_im);
		m_paintImWgt->update();
	}
	else
	{
		avoWarning("SceneWidget::drawImage()") << "the given image is NULL";
	}
}


void SceneWidget::itemAddedToScene(int n)
{
	m_ui->toolsOptWgt->hide();
	m_ui->toolsOptFrame->hide();
	m_paintImWgt->endDrawing();
	rcvMessage("");
}


void SceneWidget::rcvMessage(const QString& msg, int timeout)
{
	if(msg.isEmpty())
	{
		m_ui->mainMsgLabel->hide();
		m_ui->mainMsgLabel->setText(QString());
	}
	else
	{
		m_ui->mainMsgLabel->show();
		m_ui->mainMsgLabel->setText(msg);
//		QPalette plt;
//		QColor col = Qt::red;
//		plt.setColor(QPalette::Base, col); //background normal
//		m_ui->mainMsgLabel->setPalette(plt);
	}
}


void SceneWidget::setDrawTool(QWidget* wgt)
{
	X7S_FUNCNAME("SceneWidget::setDrawTool()");

	GraphicsShape shape;

	if(wgt==NULL)
	{
		AVO_PRINT_DEBUG(funcName,"wgt is NULL");
		itemAddedToScene(-1);
		return;
	}
	else if(wgt==m_ui->dist_tb)
	{
		m_ui->toolsOptFrame->show();
		m_ui->toolsOptWgt->setCurrentWidget(m_ui->distOpt);
		AVO_PRINT_DEBUG(funcName,"button is distance");
		shape = GraphicsShape(GraphicsShape::Line,Dist,Qt::green);
	}
	else if(wgt==m_ui->peo_tb)
	{
		m_ui->toolsOptFrame->show();
		m_ui->toolsOptWgt->setCurrentWidget(m_ui->peopleOpt);
		AVO_PRINT_DEBUG(funcName,"button is people");
		shape = GraphicsShape(GraphicsShape::Rectangle,Peo,Qt::yellow);

		QSizeF realSize;
		realSize.setHeight(m_ui->peopleH_dsb->value());
		realSize.setWidth(m_ui->peopleW_dsb->value());
		shape.setProperty(GraphicsShape::RealSize,realSize);

	}
	//	else if(wgt==m_ui->sup_tb)
	//	{
	//		AVO_PRINT_DEBUG(funcName,"button is superficy");
	//		shape = GraphicsShape(GraphicsShape::Polygon,Sup,
	//				QPen(Qt::green, 3, Qt::DashDotDotLine,Qt::RoundCap));
	//	}
	//	else if(wgt==m_ui->ngeo_tb)
	//	{
	//		AVO_PRINT_DEBUG(funcName,"button is non geometrical");
	//		shape = GraphicsShape(GraphicsShape::Ellipse,NoGeo,
	//				QPen(Qt::red, 3, Qt::SolidLine, Qt::RoundCap),
	//				QBrush(Qt::red));
	//	}
	else
	{
		AVO_PRINT_DEBUG(funcName,"wgt is 'others'");
	}

	m_paintImWgt->startDrawing(shape);
}


void SceneWidget::changeRealSize()
{
	X7S_FUNCNAME("SceneWidget::changeRealSize()");
	QSizeF realSize;
	realSize.setHeight(m_ui->peopleH_dsb->value());
	realSize.setWidth(m_ui->peopleW_dsb->value());
	if(m_paintImWgt->setShapeProperty(GraphicsShape::RealSize,realSize))
	{
		avoDebug(funcName) << realSize;
	}
	else
	{
		avoWarning(funcName) << "Error while setting property" << realSize;
	}
}


void SceneWidget::changeRealDist()
{
	X7S_FUNCNAME("SceneWidget::changeRealDist()");
	double dist = m_ui->dist_dsb->value();
	if(m_paintImWgt->setShapeProperty(GraphicsShape::RealSize,QSizeF(dist,1)))
	{
		avoDebug(funcName) << dist;
	}
	else
	{
		avoWarning(funcName) << "Error while setting property" << dist;
	}
}

void SceneWidget::clickCheckParams()
{
	QDialog *dial;
	dial = new QDialog(this);
	//dial=NULL;
	SceneParamWgt *pScenePrmWgt = new SceneParamWgt(dial,QSize(704,576));
	pScenePrmWgt->setVisible(true);
	dial->show();
	dial->setModal(true);
}

void SceneWidget::clickEstimateParams()
{
	QList<QGraphicsItem*> listGItem = m_paintImWgt->getScene()->items();
	QGraphicsItem* gItem;

	x7s::Scene2D3D  *scene= new x7s::Scene2D3D(m_im.height(),m_im.width());
	int i = scene->CreatePlane("Plano1");
	x7s::Plane2D3D *plane = scene->PlaneList.at(i);

	plane->ObjectRealHeightVector.clear();
	plane->ObjectPixelHeightVector.clear();
	plane->ObjectPositionVector.clear();
	plane->DistancePositionsVector.clear();
	plane->DistancesVector.clear();


	int type;
	QSizeF realSize;
	QSizeF pixSize;


	for(int i=0;i<listGItem.size();i++)
	{
		GraphicsShape gShape;
		if(gShape.fromQGraphicsItem(listGItem[i]))
		{
			type=gShape.property(GraphicsShape::Subtype).toInt();
			realSize = gShape.property(GraphicsShape::RealSize).toSizeF();
			pixSize = gShape.property(GraphicsShape::FixedSize).toSizeF();


			qDebug() << i << gShape.type() << type << realSize << pixSize;
			if(type==SceneWidget::Peo)
			{
				plane->ObjectRealHeightVector.push_back(realSize.height()*1000);
				plane->ObjectPixelHeightVector.push_back(pixSize.height());
				//plane->ObjectPositionVector.push_back(10);


			}
			else if(type==SceneWidget::Dist)
			{
				plane->AddDistanceVector(530.0,323.0,434.0,317.0);
				plane->DistancesVector.push_back(realSize.height());
			}
		}
	}





}

void SceneWidget::estimateScene()
{

}



void SceneWidget::saveScene()
{
	QString fname = QFileDialog::getSaveFileName(
			this, tr("Save an INI File"),
			"",
			tr("INI Files (*.ini);;All Files (*.*)"));

	m_paintImWgt->saveScene(fname);
}

void SceneWidget::loadScene()
{
	QString fname = QFileDialog::getOpenFileName(
			this, tr("Open an INI File"),
			"",
			tr("INI Files (*.ini);;All Files (*.*)"));

	m_paintImWgt->loadScene(fname);
}


void SceneWidget::accept()
{
	AVO_PRINT_DEBUG("SceneWidget::accept()","Not Implemented");
}
