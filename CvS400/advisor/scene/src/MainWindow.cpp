#include "MainWindow.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Log.hpp"
#include "SceneWidget.h"
#include "QtBind.hpp"

#include <x7sio.h>

#include <QFileDialog>
#include <QStatusBar>
#include <QPainter>
#include <QTest>
#include <QSettings>
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent)
:QMainWindow(parent)
 {

	//First setup
	ui.setupUi(this);
	sceneWidget = new SceneWidget(this);
	ui.mainLayout->addWidget(sceneWidget);

	QSettings settings;
	restoreState(settings.value("mainwindow/state").toByteArray());
	restoreGeometry(settings.value("mainwindow/geometry").toByteArray());


	ui.actionQuit->setShortcut(Qt::CTRL + Qt::Key_Q);
	ui.actionNext->setShortcut(Qt::CTRL+ Qt::Key_N);
	ui.actionPrev->setShortcut(Qt::CTRL + Qt::Key_P);


	connect(ui.actionOpenVid,SIGNAL(triggered()),this, SLOT(OpenVidDialog()));
	connect(ui.actionSave,SIGNAL(triggered()),sceneWidget,SLOT(saveScene()));
	connect(ui.actionLoad,SIGNAL(triggered()),sceneWidget,SLOT(loadScene()));
	connect(ui.actionQuit,SIGNAL(triggered()),this,SLOT(close()));
	connect(ui.actionNext,SIGNAL(triggered()),this,SLOT(nextFrame()));
	connect(ui.actionPrev,SIGNAL(triggered()),this,SLOT(prevFrame()));


	this->setStatusBar(new QStatusBar(this));
	this->statusBar()->showMessage("Open a new file");

	Log::SetLevel(X7S_LOGLEVEL_HIGH);
 }

MainWindow::~MainWindow()
{

	QSettings settings;
	settings.setValue("mainwindow/state",saveState());
	settings.setValue("mainwindow/geometry",saveGeometry());

	delete sceneWidget;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

/**
* @brief Open the MJPEG file and return the string
* @return
*/
bool MainWindow::OpenVideo(QString fileName)
{
	if(fileName.isEmpty())
	{
		fileName = QFileDialog::getOpenFileName(
				this, tr("Open Video File"),
				"",
				tr("Video Files (*.avi *.mpeg *.mpg *.xvid *.mjpeg);;All Files (*.*)"));


	}

	avoDebug("MainWindow::Open()") << fileName;

	if(!fileName.isNull() && QFile::exists(fileName))
	{
		m_vidCap.release(); //Release old video
		if(m_vidCap.open(fileName.toStdString()))
		{
			//Play the first frame
			nextFrame();
		}
		else
		{
			displayMessage(tr("Failed to open the video"));
		}
	}
	else {
		displayMessage(tr("Failed to open the file"));
	}
	return m_vidCap.isOpened();

}


void MainWindow::nextFrame()
{
	avoDebug("MainWindow::nextFrame()");
	if(m_vidCap.isOpened())
	{
		cv::Mat frame_bgr;
		m_vidCap >> frame_bgr;
		if(frame_bgr.empty())
		{
			avoWarning("MainWindow::nextFrame()") << "Frame is not valid";
		}
		cv::Mat frame_bgra(frame_bgr.size(),CV_8UC4);

		IplImage im_bgr = frame_bgr;
		IplImage im_bgra = frame_bgra;
		x7sCvtColor(&im_bgr,&im_bgra,CV_BGR2BGRA);
		x7sCvtColor(&im_bgra,&im_bgra,X7S_CV_BGRA2BGRA255);

		sceneWidget->drawImage(QtBind::toQImage(&im_bgra));
	}
	sceneWidget->update();

}


void MainWindow::prevFrame()
{
	avoDebug("MainWindow::prevFrame()") << "NOT IMPLEMENTED";
}


void MainWindow::displayMessage(const QString& msg,int ms_timeout)
{
	if(!msg.isEmpty())
	{
		this->setStatusTip(msg);
	}
}
