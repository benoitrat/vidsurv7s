#include "TestWindow.h"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include <QFile>
#include <QByteArray>
#include "Camera.hpp"


bool test_CvMJPEGFrame(TestWindow *pTestWindow)
{

	X7S_FUNCNAME("test_CVMJPEGFrame()");
	avoDebug(funcName);

	QVariant f1 = TestWindow::askArgs(1,TestWindow::OFILENAME,"Select the MJPEG frame data");
	AVO_CHECK_WARN(funcName,(!f1.isNull()),false,"Input is NULL");
	avoDebug(funcName) << f1;

	QFile file(f1.toString());
	file.open(QIODevice::ReadOnly);
	QByteArray qmeta_jpeg = file.readAll();



	X7sByteArray metaJPEG;
	metaJPEG.type=X7S_CV_MJPEG_FRAMEDATA_TYPE;
	metaJPEG.data = (uint8_t*)qmeta_jpeg.data();
	metaJPEG.length = qmeta_jpeg.size();
	metaJPEG.capacity = qmeta_jpeg.capacity();
	if(metaJPEG.length == 0 || metaJPEG.capacity == 0){
		avoDebug("QCameraClient::updateImage()") << "QCameraClient: Bad size image";
		return false;
	}

	Camera *camera = new Camera(NULL,0);
	bool ret = camera->SetMetaJPEGFrame(&metaJPEG);

	return ret;
}
