#include "TestWindow.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Log.hpp"
#include "ViewerWidget.h"

#include <QTest>
#include <QInputDialog>

#define TEST_VARIOUS "test_Various"
#define TEST_VIDEOINFILE "test_VideoInFile"
#define TEST_VIDEOLIST "test_VideoList"
#define TEST_QFILESYSTEMINFO "test_QFileSystemInfo"
#define TEST_QXMLOBJWIDGET "test_XmlObjWidget"
#define TEST_SCHEDULEWIDGET "test_ScheduleWidget"
#define TEST_CVMJPEGFRAME "test_CvMJPEGFrame"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
TestWindow::TestWindow(QWidget *parent)
:QMainWindow(parent)
 {
	QListWidget *listWgt=NULL;

	listWgt= new QListWidget(this);
	listWgt->addItem(TEST_VARIOUS);
	listWgt->addItem(TEST_VIDEOINFILE);
	listWgt->addItem(TEST_QFILESYSTEMINFO);
	listWgt->addItem(TEST_QXMLOBJWIDGET);
	listWgt->addItem(TEST_SCHEDULEWIDGET);
	listWgt->addItem(TEST_CVMJPEGFRAME);
	listWgt->addItem(TEST_VIDEOLIST);

	QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	listWgt->setMinimumSize(300,800);
	listWgt->setSizePolicy(sizePolicy);

	this->setSizePolicy(sizePolicy);

	QHBoxLayout *lyt = new QHBoxLayout();
	lyt->addWidget(listWgt);
	this->setLayout(lyt);

	connect(listWgt, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(runFunction(QListWidgetItem*)));

 }

TestWindow::~TestWindow()
{
}


QVariant TestWindow::askArgs(int pos, ArgType type, const QString& cmt, const QVariant& defValue)
{
	bool ok;
	QString arg;
	QVariant ret=defValue;

	if(0<= pos && pos < QCoreApplication::arguments().size())
	{
		arg= QCoreApplication::arguments().at(pos);
	}

	int iVal;
	double dVal;
	QFileInfo fInfo;
	QWidget *win=QMainWindow::find(0);


	switch(type)
	{
	case TestWindow::INTEGER:
		iVal=arg.toInt(&ok);
		if(ok==false) iVal = QInputDialog::getInt(win,"Enter Integer",cmt);
		ret = QVariant(iVal);
		break;
	case TestWindow::DOUBLE:
		dVal=arg.toDouble(&ok);
		if(ok==false) dVal = QInputDialog::getDouble(win,"Enter Double",cmt);
		ret = QVariant(dVal);
		break;
	case TEXT:
		if(arg.isEmpty()) arg = QInputDialog::getText(win,"Enter Text",cmt);
		if(!arg.isEmpty()) ret = QVariant(arg);
		break;
	case DIR:
		fInfo = QFileInfo(arg);
		if(fInfo.isDir()==false) arg = QFileDialog::getExistingDirectory(win,cmt,cmt,QFileDialog::DontResolveSymlinks | QFileDialog::ShowDirsOnly);
		if(!arg.isEmpty()) ret = QVariant(arg);
		break;
	case OFILENAME:
		fInfo = QFileInfo(arg);
		if(fInfo.isFile()==false) arg=QFileDialog::getOpenFileName(win,cmt,cmt);
		if(!arg.isEmpty()) ret = QVariant(arg);
		break;
	case SFILENAME:
		if(arg.isEmpty()) arg=QFileDialog::getSaveFileName(win,cmt,cmt);
		if(!arg.isEmpty()) ret = QVariant(arg);
		break;
	}
	return ret;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Private SLOTS
//----------------------------------------------------------------------------------------

bool TestWindow::runFunction(QListWidgetItem *item)
{
	bool ret=false;

	if(item)
	{
		QString funcName = item->text();
		ret=true;

		qDebug();
		qDebug() << "--------------------------------------------------------------------";
		qDebug();

		if(funcName.isEmpty()) ret=false;
		else if(funcName==TEST_VARIOUS) 			test_Various(this);
		else if(funcName==TEST_VIDEOINFILE) 			test_VideoInFile(this);
		else if(funcName==TEST_VIDEOLIST) 			test_VideoList(this);
		else if(funcName==TEST_QFILESYSTEMINFO) 	test_QFileSystemInfo(this);
		else if(funcName == TEST_QXMLOBJWIDGET) test_QXmlObjWidget(this);
		else if(funcName == TEST_SCHEDULEWIDGET) test_ScheduleWidget(this);
		else if(funcName == TEST_CVMJPEGFRAME) test_CvMJPEGFrame(this);
		else ret=false;
	}
	return ret;
}


