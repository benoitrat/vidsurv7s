#include "TestWindow.h"

#include <QtGui>
#include <QApplication>




int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	Log::SetLevel(X7S_LOGLEVEL_HIGHEST);

	TestWindow win;
	win.show();
	return a.exec();
}
