#include "TestWindow.h"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include <QDir>
#include <QDirIterator>
#include <QDate>
#include "Log.hpp"
#include "cv.h"
#include "highgui.h"


void addVideoList(QFileInfoList &videoList, const QFileInfo& fInfo, const QStringList& nameFilters);


bool test_VideoList(TestWindow *pTestWindow)
{
	X7S_FUNCNAME("test_VideoList()");


	QVariant f1 = TestWindow::askArgs(1,TestWindow::DIR,"Select the root dir","D:/DATABASE/7S09");
	QVariant f2 = TestWindow::askArgs(1,TestWindow::TEXT,"Write a filter","*.avi *.mpg *.mpeg");


	QStringList nameFilters = f2.toString().split(" ");
	QDir rootDir(f1.toString());
	QString dateStr = QDate::currentDate().toString("dd/MM/yyyy");

	avoInfo(funcName) << nameFilters << rootDir.path();
	if(rootDir.exists())
	{

		QString imPath=rootDir.path()+"/images";
		QFile csvFile(imPath+"/list.csv");
		if(!rootDir.exists("images"))
		{
			if(!rootDir.mkdir("images")) return false;
		}

		if(!csvFile.open(QIODevice::WriteOnly | QIODevice::Text)) return false;
		QTextStream csvTxtStream(&csvFile);


		QDirIterator itDir(rootDir.path(),nameFilters,QDir::Files,QDirIterator::Subdirectories);
		cv::VideoCapture vid;
		cv::Mat im, imr;


		//Write a line in the text stream
		csvTxtStream << "Main::Title" <<";";
		csvTxtStream << "Main::Filename" <<";";
		csvTxtStream << "Main::Added" <<";";
		csvTxtStream << "Main::Dataset"<<";";
		csvTxtStream << "Main::Preview"<<";";
		csvTxtStream << "Preview::Start"<<";";
		csvTxtStream << "Preview::Middle"<<";";
		csvTxtStream << "Preview::Last"<<";";
		csvTxtStream << "\n";


		while (itDir.hasNext()) {
			QFileInfo fInfo = QFileInfo(itDir.next());
			QFileInfo out(imPath,fInfo.fileName());
			qDebug() << "\n\n------------------------------------\n" << fInfo.filePath();

			//Write a line in the text stream
			csvTxtStream << fInfo.fileName()<<";";
			csvTxtStream << fInfo.filePath()<<";";
			csvTxtStream << dateStr<<";";
			csvTxtStream << fInfo.fileName().split("_").at(0)<<";";
			csvTxtStream << QString("%1-%2.jpg").arg(out.filePath()).arg(0)<<";";
			csvTxtStream << QString("%1-%2.jpg").arg(out.filePath()).arg(1)<<";";
			csvTxtStream << QString("%1-%2.jpg").arg(out.filePath()).arg(2)<<";";
			csvTxtStream << QString("%1-%2.jpg").arg(out.filePath()).arg(3)<<";";
			csvTxtStream << "\n";

			//Then try to create the image
			try {
				vid.open(fInfo.filePath().toStdString().c_str());


				if(vid.isOpened())
				{
					double nFrames = vid.get(CV_CAP_PROP_FRAME_COUNT);
					qDebug() << "nFrames=" << nFrames;
					if(nFrames>9)
					{
						QList<int> pos;
						pos << cvRound(5*(nFrames/9.)) << cvRound((nFrames/9.)) << cvRound(3*(nFrames/9.)) << cvRound(7*(nFrames/9.));
						qDebug() << "pos=" << pos;

						for(int i=0;i<pos.size();i++)
						{
							if(vid.set(CV_CAP_PROP_POS_FRAMES,pos[i]))
							{
								vid.grab(); //Grab a frame
								vid >> im;
								if(im.empty()) {
									avoWarning(funcName) << "im is empty";
									continue; //Skip the resize
								}

								cv::resize(im,imr,cv::Size(176,144));
								QString fNameOut = QString("%1-%2.jpg").arg(out.filePath()).arg(i);
								if(cv::imwrite(fNameOut.toStdString(),imr))
								{
									qDebug() << "Write (" << pos[i] <<") :"<< fNameOut;
								}
								else
									avoWarning(funcName) << "Error with: " << fNameOut;
							}
							else
							{
								avoWarning(funcName) << "pos[" << i << "]=" << pos[i];
							}
						}
					}
					vid.release();
				}
			}
			catch ( const std::exception & e )
			{
				avoError(funcName) << QString(e.what());
			}
			catch ( ... ) // traite toutes les autres exceptions
			{
				avoError(funcName) << "Unknown Error";
			}
		}
	}

	return true;
}

