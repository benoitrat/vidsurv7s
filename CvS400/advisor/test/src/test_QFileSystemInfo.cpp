#include "TestWindow.h"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "QFileSystemInfo.hpp"


bool test_QFileSystemInfo(TestWindow *pTestWindow)
{
	X7S_FUNCNAME("test_QFileSystemInfo()");

	//Obtain args
	QVariant f1 = TestWindow::askArgs(1,TestWindow::OFILENAME,"Select a random file");
	QVariant f2 = TestWindow::askArgs(2,TestWindow::OFILENAME,"Select another random file on other FS");

	AVO_CHECK_WARN(funcName,(!f1.isNull() && !f2.isNull()),false,"Input are NULL");
	qDebug() << f1 << f2;


	//Process args

	QList<QFileSystemInfo> mntPoints;

	QFileSystemInfo fsInfo1(f1.toString());
	QFileSystemInfo fsInfo2(f2.toString());
	qDebug() << fsInfo1 << fsInfo2 << fsInfo1.isOnSameMntPoint(fsInfo2);

	mntPoints = QFileSystemInfo::getMntPoints();
	foreach(QFileSystemInfo mntPoint, mntPoints)
	{
		bool smp1 = mntPoint.isOnSameMntPoint(fsInfo1);
		bool smp2 = mntPoint.isOnSameMntPoint(fsInfo2);
		qDebug() << mntPoint << smp1 << smp2;
	}

	return true;
}
