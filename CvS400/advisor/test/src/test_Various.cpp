#include "TestWindow.h"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "VideoInFile.hpp"
#include <QApplication>
#include <QSound>


bool funcSound()
{
	avoDebug("funcSound()") << QFile::exists("beep.wav") << QSound::isAvailable();
	QApplication::beep();

	QSound::play("beep.wav");

	std::cout << "\a";
	return true;
}


bool funcVideo() {
	VideoInFile v1("output.mjpeg");
	VideoInFile v2("test.mjpeg");
	VideoInFile *pv3 = new VideoInFile(v1);
	v2.Open();

	if(true)
	{
		VideoInFile v4 = v1;
		v2= v1;
		delete pv3;
	}

	return true;
}


bool test_Various(TestWindow *pTestWindow)
{

	QVariant var = TestWindow::askArgs(0,TestWindow::INTEGER,
			"Select a number: \n0 -> Sound, \n1 -> VideoFile",QVariant(0));

	switch(var.toInt())
	{
	case 0:
		return funcSound();
	case 1:
	default:
		return funcVideo();
	}
}
