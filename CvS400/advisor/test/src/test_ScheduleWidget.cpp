#include "TestWindow.h"


#include "VideoHandlerScheduleWidget.h"
#include "VideoHandler.hpp"
#include <QDialog>
#include <QLabel>
#include <QCheckBox>
#include <QWindowsStyle>


bool test_ScheduleWidget(TestWindow *pTestWindow)
{
	X7S_FUNCNAME("test_ScheduleWidget()");


	qDebug() << funcName;


	VideoHandler *vidHandler = new VideoHandler(NULL);

	QDialog *dial = new QDialog(pTestWindow);
	dial->setLayout(new QHBoxLayout());
	VideoHandlerScheduleWidget *sched = new VideoHandlerScheduleWidget(dial,vidHandler);
	dial->layout()->addWidget(sched);
	dial->exec();

	vidHandler->Print(stdout);


	QDialog *dial2 = new QDialog(pTestWindow);
	dial2->setLayout(new QVBoxLayout());

	QCheckBox *cb;
	QStyle *cb_style = new QWindowsStyle();	//Each role can have different effect according to a style so we force a style.
	QColor col;

	QList<QPalette::ColorRole> listRole;
	listRole << QPalette::Window << QPalette::WindowText << QPalette::Base
			<< QPalette::AlternateBase << QPalette::ToolTipBase << QPalette::ToolTipText
			<< QPalette::Text << QPalette::Button << QPalette::ButtonText
			<< QPalette::BrightText << QPalette::Light	<< QPalette::Midlight
			<< QPalette::Dark << QPalette::Mid	<<	QPalette::Shadow
			<< QPalette::Highlight	<< QPalette::HighlightedText
			<< QPalette::Link << QPalette::LinkVisited;



	QListIterator<QPalette::ColorRole> it(listRole);
	while(it.hasNext())
	{
		QPalette::ColorRole cr = it.next();
		col.setHsv((int)cr*10,255,255);

		cb = new QCheckBox("CheckBox "+QString::number(cr)+" ("+col.name()+")",dial2);
		QPalette plt;
		plt.setColor(cr,col);
		cb->setPalette(plt);
		cb->setStyle(cb_style);
		dial2->layout()->addWidget(cb);
	}
	dial2->exec();



	//delete board;

	return true;

}
