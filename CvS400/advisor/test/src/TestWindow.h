/**
*  @file
*  @brief Contains the class TestWindow.h
*  @date Aug 28, 2009
*  @author neub
*	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
*/

#ifndef TESTWINDOW_H_
#define TESTWINDOW_H_

#include <QtGui/QMainWindow>
#include <QListWidget>
#include "Log.hpp"

/**
#define TEST_DECLARE_FUNC(funcName) \
	bool funcName(TestWindow* pTestWindow); \
	#define funcName #(funcName)
**/


/**
*	@brief The TestWindow.
*	@ingroup avo_test
*/
class TestWindow : public QMainWindow
{
	Q_OBJECT

public:
	enum ArgType {INTEGER,DOUBLE,TEXT,DIR,OFILENAME,SFILENAME};

public:
	TestWindow(QWidget *parent = 0);
	virtual ~TestWindow();
	static QVariant askArgs(int pos, ArgType type, const QString& cmt,const QVariant& defValue=QVariant());


private slots:
	bool runFunction(QListWidgetItem *item);

private:
	QList<QString> argsList;
};

//Declare the function we are going to use.
bool test_Various(TestWindow *pTestWindow);
bool test_VideoInFile(TestWindow *pTestWindow);
bool test_VideoList(TestWindow *pTestWindow);
bool test_QFileSystemInfo(TestWindow *pTestWindow);
bool test_QXmlObjWidget(TestWindow *pTestWindow);
bool test_ScheduleWidget(TestWindow *pTestWindow);
bool test_CvMJPEGFrame(TestWindow *pTestWindow);


#endif /* TESTWINDOW_H_ */
