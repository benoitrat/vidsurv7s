/**
 *  @file
 *  @brief Contains the class CamEvtCarrier.hpp
 *  @date Nov 24, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef CAMEVT_HPP_
#define CAMEVT_HPP_

#include <QDateTime>
#include <x7svidsurv.h>

#include "Core.hpp"


class Camera;
class VideoFile;


/**
 * @brief Generic and abstract class of the camera events
 *  The camera event is given by a pointer on a camera, and the datetime on which this event was generated.
 */
class CamEvt {
public:
	enum State { Start, Update, Finish, Default=-1};
	enum Type { NewFrame, Activity, Video, Net, VSAlarm, User=0xF0, Unknow=-1 };

public:
	CamEvt(Type type, const Camera *cam,const QDateTime& dtime, State state=Default);
	virtual ~CamEvt();
	virtual CamEvt* getCopy() const = 0 ;

	bool isType(CamEvt::Type type) const { return type==m_type; };
	const CamEvt::Type& getType() const { return m_type; }
	const char* getTypeDesc() const;

	virtual void setState(CamEvt::State state) { m_state=state; }
	bool isState(CamEvt::State state) const { return state==m_state; }
	const CamEvt::State& getState() const { return m_state; }
	const char* getStateDesc() const;

	bool isValid() const { return m_cam; }
	bool isNull() const { return !isValid(); }
	const QDateTime& getDateTime() const { return m_dtime; }
	time_t getTime_t() const { return m_dtime.toTime_t(); }
	const Camera* getCamera() const  { return m_cam; }

	virtual QString toString() const;

#if !defined(QT_NO_DEBUG_STREAM) && !defined(QT_NO_DATESTRING)
	friend QDebug operator<< (QDebug, const CamEvt* camEvt);
#endif

private:
	Type m_type;					//!< The type of the camera event
	State m_state;					//!< Get the state of this camera Event.
	const Camera *m_cam;	//!< Pointer on the camera.
	QDateTime m_dtime;			//!< Datetime when was generated this camera event
};




class CamEvtVSAlarm: public CamEvt {
public:
	CamEvtVSAlarm(const Camera *cam,const QDateTime& dtime,const X7sVSAlarmEvt& alrEvt);
	virtual ~CamEvtVSAlarm() {};
	CamEvt* getCopy() const { return new CamEvtVSAlarm(*this); }
	virtual QString toString() const;


	static CamEvtVSAlarm* fromAVOAlrEvt(const AVOAlrEvt& avoAlrEvt);

public:
	const static Type class_type=VSAlarm;
	X7sVSAlarmEvt vsAlrEvt;
};


class CamEvtActivity: public CamEvt  {
public:
	const static Type class_type=Activity;

public:
	CamEvtActivity( const Camera *cam,const QDateTime& dtime,State state);
	virtual ~CamEvtActivity() {};
	CamEvt* getCopy() const { return new CamEvtActivity(*this); }
	virtual QString toString() const;

public:
	QDateTime start;
};


class CamEvtVideo: public CamEvt {
public:
	CamEvtVideo( const Camera *cam,const QDateTime& dtime,const VideoFile* vidFile,State state);
	virtual ~CamEvtVideo() {};
	CamEvt* getCopy() const { return new CamEvtVideo(*this); }
	virtual QString toString() const;

public:
	const static Type class_type=Video;
	const VideoFile *m_vidFile;
};


#endif /* CAMEVT_HPP_ */
