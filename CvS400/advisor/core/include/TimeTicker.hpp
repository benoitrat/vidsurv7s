/**
*  @file
*  @brief Contains the class TimeTicker.h
*  @date Jan 11, 2010
*  @author neub
*	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
*/

#ifndef TIMETICKER_H_
#define TIMETICKER_H_

#include <QObject>
#include <QDateTime>

/**
*	@brief The TimeTicker send signal when we have a full minute, quater, hour and/or day.
*
*	This class send signal to another QObject when a minute,... has been completed.
*	The sampling rate to tick() this function is about 1 second.
*
*	@ingroup avocore
*/
class TimeTicker : public QObject {

	Q_OBJECT

public:
	TimeTicker(QObject *parent=0,bool day=true,bool hour=false, bool quater=false, bool minute=false);
	virtual ~TimeTicker();
	void enableDayTicking(bool enable=true) 		{ tickDay=enable; }	//!< Enable the day ticking signal
	void enableHourTicking(bool enable=true) 		{ tickHour=enable; } //!< Enable the hour ticking signal
	void enableQuaterTicking(bool enable=true) 	{ tickQuater=enable; } //!< Enable the quater ticking signal
	void enableMinuteTicking(bool enable=true) 	{ tickMinute=enable; } //!< Enable the minute ticking signal
	void reset();

public slots:
	void tick();

	signals:
	void completedDay(const QDateTime& tactual);
	void completedHour(const QDateTime& tactual);
	void completedQuater(const QDateTime& tactual);
	void completedMinute(const QDateTime& tactual);


private:
	QDateTime dtStart;

	bool tickDay;
	QDateTime lastDay;

	bool tickHour;
	QDateTime lastHour;

	bool tickQuater;
	QDateTime lastQuater;

	bool tickMinute;
	QDateTime lastMinute;

};

#endif /* TIMETICKER_H_ */
