/**
 *  @file
 *  @brief Contains the class VideoFile.hpp
 *  @date Oct 16, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef VIDEOFILE_HPP_
#define VIDEOFILE_HPP_

#include <QDateTime>
#include <QFileInfo>
#include <QLinkedList>
#include <QDir>

#include <x7scv.h>
#include <cstdio>

class Camera;
#include "Core.hpp"


/**
 *	@brief The VideoFile.
 *	@ingroup avo_core
 */
class VideoFile {
public:
	enum Type { MJPEG };

	//------------------------------------- Methods

public:
	VideoFile(VideoFile::Type type);
	VideoFile(const VideoFile& copy);
	VideoFile& operator=(const VideoFile& copy);
	virtual ~VideoFile() { this->Release(); }


	virtual bool Release();
	virtual bool Close();

	virtual bool isEmpty() const { return m_isEmpty; }	//!< Constructed with empty constructor
	virtual bool isValid() const = 0;
	virtual bool isOpened() const;
	virtual bool isClosed() const;
	virtual bool hasAlrEvent(const AVOAlrEvt& alrEvt)  const;

	const VideoFile::Type& getType() const { return m_type; }
	const QDateTime& getTimeStart() const { return m_tStart; }
	const QDateTime& getTimeLast() const { return m_tLast; }
	const QFileInfo& getFileInfo() const { return m_fileInfo; }
	QString getPath() const { return m_fileInfo.path();}
	QString getFilename() const { return m_fileInfo.fileName(); }
	qint64 getSize() const { return (m_file)?ftell(m_file):-1; }
	qint64 getLastSize() const { return m_lastSize; }	//!< Return the last file size before it was closed (-1 if it was never opened)
	const Camera* getCamera() const { return m_cam; }
	virtual qint64 getSize(bool force) = 0;
	virtual int getSeconds() const { return m_tStart.secsTo(m_tLast); }
	virtual int getCamID() const;


protected:
	virtual void ResetVariable();
	virtual VideoFile* Clone() const = 0;

	//------------------------------------- Variables

private:
	VideoFile::Type m_type;
	int *m_pRef;		//A pointer to the number of reference of this class.

protected:
	bool m_isEmpty;	//!< Must be set when the constructor is not empty.
	const Camera *m_cam;
	QDateTime m_tStart;
	QDateTime m_tLast;
	QFileInfo m_fileInfo;
	qint64 m_lastSize;		//!< Size of the file before it was close.
	FILE *m_file;


};

#endif /* VIDEOFILE_HPP_ */
