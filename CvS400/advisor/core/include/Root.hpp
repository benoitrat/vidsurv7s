/**
 *  @file
 *  @brief Contains the class Root
 *  @date Sep 24, 2009
 *  @author Benoit Rat
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */
#ifndef ROOT_HPP_
#define ROOT_HPP_

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes/Defines/Prototype
//----------------------------------------------------------------------------------------


#include <x7sxml.h>
#include <QString>

#define AVO_TAGNAME_ROOT "root"

class Devices;		//Forward declaration: this object is included in the .cpp
class Database;		//Forward declaration: this object is included in the .cpp
class LogConfig; 	//Forward declaration: this object is included in the .cpp
class VideoHandler;	//Forward declaration: this object is included in the .cpp

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	The Main class
//----------------------------------------------------------------------------------------

/**
 * @brief Class used by Advisor to load all the parameters on the server.
 *
 * This class create automatically:
 * 		- A class to handle the general configuration of the server.
 * 		- The devices class.
 * 		- A database class that correspond to the database on the server.
 *
 * @ingroup avo_core
 */
class Root: public X7sXmlObject {
public:
	Root(const QString& fname=QString());
	virtual ~Root();
	bool LoadFile();
	bool LoadFile(const QString& fname);
	bool SaveFile();
	bool SaveFile(const QString& fname);

	//! Return the Devices loaded by XML.
	Devices* devices() const { return m_devices; }
	//! Return the Database class loaded by XML (By default its attached to the devices).
	Database* database() const { return m_database; }
	//!< Return the General configuration parameters loaded by XML.
	LogConfig* logConfig() const { return m_logconf; }
	//!< Return the associate fileName of the Root.
	const QString& fileName() const {return m_fname; }
	//!< Return the associate VideoHandler
	VideoHandler* videoHandler() const { return m_vidHandler; }

private:
	QString m_fname;
	Devices *m_devices;
	Database *m_database;
	LogConfig *m_logconf;
	VideoHandler *m_vidHandler;
};

#endif /* ROOT_HPP_ */
