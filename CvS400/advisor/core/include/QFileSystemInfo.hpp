/**
 *  @file
 *  @brief Contains the class QFileSystemInfo.hpp
 *  @todo Create a shared QMountPoint : public QSharedData {}, and each QFileSystem should link to this using
 *  the dev_id, and update the disk capacity each time.
 *  @date Nov 6, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef QFILESYSTEMINFO_HPP_
#define QFILESYSTEMINFO_HPP_


#include <QHash>
#include <QString>
#include <QFileInfo>
#include <QDebug>


/**
 *	@brief The QFileSystemInfo.
 *	@ingroup avocore
 */
class QFileSystemInfo: public QFileInfo {
public:
	QFileSystemInfo(): QFileInfo() { Init(); };
	QFileSystemInfo( const QString & file ): QFileInfo(file) { Init(); };
	QFileSystemInfo( const QFile & file ): QFileInfo(file) { Init(); };
	QFileSystemInfo( const QDir & dir, const QString & file ): QFileInfo(dir,file) { Init(); };
	QFileSystemInfo( const QFileInfo& fileinfo ): QFileInfo(fileinfo) { Init(); };
	virtual ~QFileSystemInfo() {};
	void setFile ( const QString & file )	 { QFileInfo::setFile(file); Init(); };
	void setFile ( const QFile & file ) { QFileInfo::setFile(file); Init(); };
	void setFile ( const QDir & d, const QString & fileName ) { QFileInfo::setFile(d,fileName); Init(); };

	bool Update(bool upMntPoint=false);	//!< Update the disk capacity.

	qint64 getUsedSpace() const {return isValid()?(totalSpace-freeSpace):-1; } ;	//!<Return used space in bytes.
	qint64 getFreeSpace() const {return freeSpace; } ;		//!< Return free space in bytes.
	qint64 getTotalSpace() const {return totalSpace; } ;	//!< Return total space in bytes
	double getFreeRatio() const {return (double)freeSpace/(double)totalSpace; } ;
	const QString& getMntPoint() const {return mnt_dir; }
	static QList<QFileSystemInfo> getMntPoints();

	bool isValid() const { return m_isValid; }
	bool isOnSameMntPoint(const QString& file) const ;
	bool isOnSameMntPoint(const QFileSystemInfo& fileSysInfo) const;

#if !defined(QT_NO_DEBUG_STREAM) && !defined(QT_NO_DATESTRING)
	friend QDebug operator<< (QDebug, const QFileSystemInfo & fileSysInfo );
#endif

protected:
	void Init();
	static bool UpdateMntPoint();

private:
	QFileSystemInfo(const QString& fs_name, const QString& fs_type, const QString& mnt_dir, qint64 fs_id, qint64 dev_id);

private:
	static QHash<qint64,QFileSystemInfo> mntPointList;	//!< static list of mount point.

	bool m_isValid;		//!< This file system is valid.

	QString fs_name;	//!< name of the file system
	QString fs_type;	//!< Type of file system
	QString mnt_dir;	//!< mount point (unix) or drive letter (windows)
	qint64 fs_id;			//!< FileSystem ID (Not valid without root privilege and NTFS)
	qint64 dev_id;		//!< Devices ID (given by stat under linux)

	qint64 freeSpace;	//!< Actual free space of the mount point
	qint64 totalSpace;//!< Actual total space of the mount point


};

#endif /* QFILESYSTEMINFO_HPP_ */
