/**
 *  @file
 *  @brief Contains the class Database.hpp
 *  @date Aug 25, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef VIDSURVSQL_HPP_
#define VIDSURVSQL_HPP_

#include <QtSql>
#include <QString>
#include <QVector>
#include <QObject>

#include "x7sxml.h"
#include "x7svidsurv.h"

#include "VideoInFile.hpp"
#include "Core.hpp"
#include "CamEvtCarrier.hpp"


#define AVO_TAGNAME_DATABASE "database"


class Camera;
class VideoOutFile;




/**
 *	@brief The Database.
 *	@ingroup avo_core
 */
class Database: public X7sXmlObject {
public:
	enum DBType { All=-1, Video, Activity, Event, Alarm };

	Database(X7sXmlObject *parent=NULL);
	virtual ~Database();

	bool Connect(bool restart=false);
	bool Create();
	bool IsOk();
	bool Insert(const Camera *cam, const X7sVSAlarmEvt& alrEvt);
	bool Insert(const VideoOutFile* video);
	bool Insert(const Camera *cam);
	bool Update(const Camera *cam, DBType type, bool closing=false);
	bool Update(const VideoOutFile* video);
	bool Delete(const VideoInFile& videoIn);

	QString lastErrorStr() const;

	QVector<AVOAlrEvt> GetAlarmEvt(const Camera *cam,QDateTime start, double hour=1);
	QVector<AVOInterval> GetMJPEG(const Camera *cam, QDateTime start, double hour=1);
	QVector<AVOInterval> GetActivity(const Camera *cam, QDateTime start, double hour=1);
	QDateTime GetLastHour(const Camera *cam, DBType type=All);
	QString GetVideoPath(const Camera *cam, const QDateTime& dtime);
	VideoInFile GetVideo(const Camera *cam, const QDateTime& dtime);
	QVector<VideoInFile> GetVideos(const QString& querySuffix);
	QVector<VideoInFile> GetVideos(const Camera *cam, const QDateTime& dtime, int nVideosMax);
	int countElements(const Camera *cam, DBType type,const QDateTime &start,const QDateTime &end);
	void PrintTable(const QString & tablename);
	bool ProcessCamEvt(const CamEvtCarrier& camEvt);

protected:
	bool ProcessCamEvt(const CamEvtActivity* camEvt);
	bool ProcessCamEvt(const CamEvtVSAlarm* camEvt);
	bool ProcessCamEvt(const CamEvtVideo* camEvt);

protected:
	static QSqlDatabase GetDefault(const QString& type);
	static QString GetTableType(DBType type);
	bool CreateDatabase();
	bool CreateTable();

private:
	QString m_lastErrStr;
	int m_nOkError;
	QSqlDatabase m_DB;
};





#endif /* VIDSURVSQL_HPP_ */
