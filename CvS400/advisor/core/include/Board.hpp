/**
 *  @file
 *  @brief Contains the class Board
 *  @date Jun 25, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef BOARD_H_
#define BOARD_H_

#include "x7sxml.h"
#include "x7snet.h"
#include "x7svidsurv.h"

#include <QString>
#include <vector>
using std::vector;

#define AVO_BOARD_NUMCAM 4
#define AVO_TAGNAME_BOARD "board"

class Camera; //Forward declaration: this object is included in the .cpp
class Devices; //Forward declaration: this object is included in the .cpp

/**
 *	@brief The Board.
 *	@ingroup avo_core
 */
class Board: public X7sXmlObject  {
public:
	Board(Devices *parent);
	virtual ~Board();
    //State function...
	bool Connect();
	bool Ping();
	bool Setup();
	bool Update();
	bool Start();
	bool Stop();
	bool ToggleLED();
	bool Close();
	int ProcessFrame();
	bool IsAlive(int nof_tries);
	bool IsEnable() const { return m_enable; }
	bool IsRecording(bool* valueChanged);
	Camera* GetCamera(int index);
	int GetSpecialID(int cam_index=-1);
	Devices* GetDevices() { return dev; }
	const X7sNetSession *GetNetSession() const { return &nets; }
	QString GetFWVersion() const;
	QString GetStats() const;

protected:
	std::vector<Camera*> vcam;
	X7sNetSession nets;
	void Init();
	bool Apply(int depth);
	Devices *dev;

private:
	bool m_isRecording;
	bool m_wasRecording;
	bool m_enable;
	int actual_camid;
	int frame_count;
	uint32_t m_fwVersion;
	Board(const Board& copy); 		// declared private, but not defined, to prevent copying
	Board& operator=(const Board&); // declared private, but not defined, to prevent copying
};

#endif /* BOARD_H_ */
