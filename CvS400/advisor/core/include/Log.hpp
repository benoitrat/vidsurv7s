/**
 *  @file
 *  @brief Contains the class Log.hpp
 *  @date Jul 17, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef LOG_HPP_
#define LOG_HPP_

#include <x7sdef.h>

#include <cstdarg> 	//!< Use va_list(),...
#include <cstdio> 	//!< Use vprintf(),...
#include <ctime>	//!< Use clock(),...
#include <cstring>	//!< Use strrchr()
#include <string>	//!< Use string during debugging
#include <sstream>	//!< Use string stream

#include <QtDebug>
#include <QFileInfo>

void AVOMsgOutput(QtMsgType type, const char *msg);	//Function called by QT to handle the Msg in the system

/**
 *	@brief The Log.
 *	@ingroup avo_core
 */
class Log {
public:
	static void PrintLevel(const char *funcname,const char *file, int line, int level, const char* szFormat=NULL, ...);
	static bool SetLevel(int log_level);
	static bool IsDisplay(int level);
	static bool Config(int log_level, FILE *f_out=stdout, FILE *f_err=stderr, int no_error=X7S_LOG_NOERROR);
	static FILE* ferr;
	static FILE* fout;
private:
	static int level;
	static int noError;
	static void Print(bool with_time, const char *prefix, const char *funcname,const char *file, int line, FILE *stream, const char *szFormat, va_list args);
};


class AVOLogLine : public QDebug
{
	QString str;

public:
    AVOLogLine(const char *file, int line, int level,bool space, const QString& funcName=QString());
    ~AVOLogLine();

protected:
    QFileInfo fileInfo;
    int line;
    int level;
};



#define   AVO_LOG(funcname,lvl,...)      if (Log::IsDisplay(lvl)) Log::PrintLevel(funcname,__FILE__, __LINE__ ,lvl,##__VA_ARGS__)

//! Print the error like printf() function with its corresponding function name
#define   AVO_PRINT_ERROR(funcname,...)      AVO_LOG(funcname,X7S_LOGLEVEL_LOWEST,##__VA_ARGS__)

//! Print the warning like printf() function with its corresponding function name
#define   AVO_PRINT_WARN(funcname,...)       AVO_LOG(funcname,X7S_LOGLEVEL_LOW,##__VA_ARGS__)

//! Print the info like printf() function with its corresponding function name
#define   AVO_PRINT_INFO(funcname,...)       AVO_LOG(funcname,X7S_LOGLEVEL_MED,##__VA_ARGS__)
#ifdef DEBUG
//! Print the debug like printf() function with its corresponding function name
#define   AVO_PRINT_DEBUG(funcname,...)      AVO_LOG(funcname,X7S_LOGLEVEL_HIGH,##__VA_ARGS__)
//! Print the dump like printf() function with its corresponding function name
#define   AVO_PRINT_DUMP(funcname,...)       AVO_LOG(funcname,X7S_LOGLEVEL_HIGHEST,##__VA_ARGS__)
#else
//! Unused function if not in DEBUG mode.
#define AVO_PRINT_DEBUG(funcname,...)
//! Unused function if not in DEBUG mode
#define AVO_PRINT_DUMP(funcname,...)
#endif

#define AVO_CHECK_WARN(funcname,condition,ret,...) \
	if(!(condition)) { \
		AVO_LOG(funcname,X7S_LOGLEVEL_LOW,##__VA_ARGS__); \
		return ret; \
	}

#define AVO_CHECK_ERROR(funcname,condition,ret,...) \
	if(!(condition)) { \
		AVO_LOG(funcname,X7S_LOGLEVEL_LOWEST,##__VA_ARGS__); \
		return ret; \
	}

#define avoLogLine(lvl,funcName) 	AVOLogLine(__FILE__,__LINE__,lvl,true,funcName)

#define avoCvExc(exception) AVOLogLine(e.file.c_str(),e.line,(int)X7S_LOGLEVEL_LOWEST,true,e.func.c_str()) << e.err.c_str()

#define avoFatal(funcName) avoLogLine(0,funcName)
#define avoError(funcName)  avoLogLine((int)X7S_LOGLEVEL_LOWEST,funcName)
#define avoWarning(funcName) avoLogLine((int)X7S_LOGLEVEL_LOW,funcName)
#define avoInfo(funcName) avoLogLine((int)X7S_LOGLEVEL_MED,funcName)
#ifdef DEBUG
#define avoDebug(funcName) avoLogLine((int)X7S_LOGLEVEL_HIGH,funcName)
#define avoDump(funcName) avoLogLine((int)X7S_LOGLEVEL_HIGHEST,funcName)
#else
#define avoDebug(funcName) QNoDebug()
#define avoDump(funcName) QNoDebug()
#endif


#endif /* LOG_HPP_ */
