/**
 *  @file
 *  @brief Contains the class Core.hpp
 *  @date Oct 30, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef CORE_HPP_
#define CORE_HPP_

#include <QtSql>
#include <QString>
#include <QVector>

#include "x7sxml.h"
#include "x7svidsurv.h"

class Camera;


struct AVOAlrEvt: public X7sVSAlarmEvt
{
public:
	const Camera *cam;
	time_t t;
};

typedef QPair<QDateTime, QDateTime> AVOInterval;

/**
 * @brief Check if the memory of struct1 and struct2 are the same.
 *
 * only the size of struct2 is taken in account therefore struct1 can inherit struct2.
 *
 */
#define AVO_STRUCT_EQUAL(struct1, struct2) \
		(memcmp(&struct1,&struct2,sizeof(struct2))==0)?true:false

#define AVO_STRUCT_COPY(structDst, structSrc) \
		(memcpy(&structDst,&structSrc,sizeof(structSrc)))

#define AVO_STRUCT_RESET(struct0) \
	(memset(&struct0,0,sizeof(struct0)))




#endif /* CORE_HPP_ */
