/**
 *  @file
 *  @brief Contains the class VideoOutFile.hpp
 *  @date Oct 14, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef VIDEOOUTFILE_HPP_
#define VIDEOOUTFILE_HPP_

#include "VideoFile.hpp"

#define AVO_VIDEO_OFILE_MAXBUFF 100
#define AVO_VIDEO_OFILE_MAXDUR 600
#define AVO_VIDEO_OFLE_MAXSIZE X7S_CV_MJPEG_MAXFILESIZE


struct VideoFrame
{
	X7sByteArray *data;
	QDateTime time;
};


/**
 *	@brief The VideoOutFile.
 *	@ingroup avo_core
 */
class VideoOutFile: public VideoFile {
public:
	VideoOutFile(const Camera *cam=NULL);
	virtual ~VideoOutFile();

	bool SetCamera(const Camera *cam);
	bool Open(const Camera *cam);
	bool Close();
	bool AutoClose();
	bool WriteFrame(const X7sByteArray *frame, const QDateTime& datetime);
	bool BufferizeFrame(const X7sByteArray *frame,bool flush);
	bool FlushBuffer();
	bool EraseBuffer();

	void setBufferSize(int nFrames) { m_maxBuffer = nFrames; }
	void setMaxDuration(int seconds) { m_maxDuration=seconds; }

	bool wasAutoClosed() const { return m_wasAutoClosed; } //!< Return @true when the file reach its maximum size or maximum duration.
	bool isValid() const { return (m_cam!=NULL); }	//!< return @true if the file has been opened at least once.
	qint64 getSize(bool force); //!< return size of the VideoFile (-1 if the file is not opened).
	const Camera* getCamera() const { return m_cam; }

protected:
	bool CheckMaxSize();
	virtual VideoFile* Clone() const { return (VideoFile*)new VideoOutFile(*this); }
	static QString GenerateFilePath(const Camera *cam,const QString &pattern);


private:
	QLinkedList<VideoFrame> m_buffer;
	bool m_wasAutoClosed;	//!< The file was automatically closed.
	int m_maxBuffer;		//!< Maximum number of frame to keep in the buffer
	int m_maxDuration;	//!< Maximum duration of the video.
	int m_count;			//!< Continue flushing until the count is zeros.
};

#endif /* VIDEOOUTFILE_HPP_ */
