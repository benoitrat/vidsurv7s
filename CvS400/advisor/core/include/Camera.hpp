/**
 *  @file
 *  @brief Contains the class Camera.hpp
 *  @date Jun 25, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef CAMERA_HPP_
#define CAMERA_HPP_


#include "x7sxml.h"
#include "x7snet.h"
#include "x7svidsurv.h"


#include <string>
using std::string;

#include <QObject>
#include <QString>
#include <QImage>
#include <QDateTime>
#include <QMutex>

#include "CamEvtCarrier.hpp"

class Devices; //Forward declaration: this object is included in the .cpp
class Board; //Forward declaration: this object is included in the .cpp
class Database; //Forward declaration: this object is included in the .cpp
class CamStats; 	//Forward declaration: this object is included in the .cpp

#define AVO_TAGNAME_CAM "camera"
#define AVO_CAM_MAX_MJPEG_LENGTH 600 //!< Maximum length of the MJPEG video in seconds.


/**
 *	@brief The Camera.
 *	@ingroup avo_core
 */
class Camera: public X7sXmlObject {
public:
	enum EventType { Activity, Video, AlrEvts };

public:
	Camera(Board* board, int ID);
	Camera(Board* board, const TiXmlNode* cnode);
	virtual ~Camera();

	QMutex& mutex() { return m_mutex; }
	bool IsEnable() { return (m_params["enable"]->toIntValue()!=0); };
	bool IsToWrite() const;
	bool IsRecording() const { return isRecording; }
	void SetRecording(bool enable) { isRecording=enable; }
	bool isResetVideo() const { return m_isResetVideo; }
	void resetVideo(bool enable=true);

	bool HasActivity() const { return hasActivity; }
	Board* GetBoard() const { return board; }
	Devices* GetDevices() const;

	bool Update();
	void DrawFrame(bool enable) { isDisplayed=enable; }

	IplImage* GetFrame() { return imCvFrame; }	//!< @deprecated Use imageFrame() and imageMData()
	const QImage& imageFrame() {return imQtFrame; }
	const QImage& imageMData() {return imQtMData; }

	bool ProcessNetFrame(X7sNetFrame *netFrame);
	bool SetMetaJPEGFrame(const X7sByteArray* pData);
	const X7sByteArray *GetMetaJPEGFrame() const;
	bool ChangeVidSurvType(int type);

	QDateTime GetTime() const { return QDateTime::fromTime_t(metaJpegFrame.time); }
	X7sVidSurvPipeline* GetVidSurvPipeline() { return vidsurv; };
	X7sVSAlrManager* GetAlarmManager() const;

	QString GetName() const;
	const QColor& GetColor() const;
	int GetSpecialID() const;
	QString GetVideoRootDir() const;
	QString GetStats() const;
	const CamStats* GetCamStats() const { return stats; }

	const QVector<CamEvtCarrier>& GetEventsList() const { return camEvtList; }

public:
	bool ReadFromXML(const TiXmlNode *cnode);

protected:
	void Init();
	bool ProcessFrame();
	bool DecompressJPEG();
	bool Pixelate();
	bool DrawMDataImage(const CvSize& size);
	bool GenerateCamEvt(bool isUpdate, bool isClosing=false);

private:
	X7sXmlObject* ChildTypeReadFromXML(const TiXmlElement* childNode, X7sXmlObject *childObj);


private:
	static int nInstances;

	Board *board;
	X7sVidSurvPipeline *vidsurv;

	CamStats *stats;		//!< Gather some statistic about the camera.

	//Mutex to make the operation thread safe
	QMutex m_mutex;		//!< Mutex is done by the board and the CamEvtHandler.

	//Image for the client.
	QImage 		imQtFrame;		//!< QT Image of the @ref imCvFrame.
	QImage 		imQtMData;		//!< QT Image of the @ref imCvMData.
	IplImage*	imCvFrame;			//!< Image of the JPEG frame in OpenCV format (RGBA).
	IplImage*	imCvMData;			//!< Image where mdata are draw in OpenCV format (RGBA).
	IplImage*	imCvFGMask;		//!< Image of the foreground (BW) (Only for server)

	bool hasActivity;		//!< Tell if at least one blobs is present in the image.
	bool isDisplayed;		//!< Decompress JPEG image and draw metaData.
	bool isRecording;		//!< Its a parameters set to know it the board is recording or not.
	bool m_isResetVideo;	//!< The video must be refreshed when a new alarm is setup.

	//List of events occurs during one process.
	QVector<CamEvtCarrier> camEvtList;

	X7sMJPEGFrame metaJpegFrame;	//!< Structure with JPEG and its metadata.
	X7sByteArray* metaJpegData;		//!< JPEG+Metadata encoded in binary format.
	bool serverMode;			//!< Tell is the metaJpegData need to be released
};

#endif /* CAMERA_HPP_ */
