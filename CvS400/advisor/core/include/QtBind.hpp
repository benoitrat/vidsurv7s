/**
 *  @file
 *  @brief Contains the class QtBind.h
 *  @date Sep 11, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef QTBIND_H_
#define QTBIND_H_

#include <x7svidsurv.h>
#include <QString>
#include <QColor>
#include <QImage>
#include <QLine>
#include <QPolygon>

/**
 *	@brief The QtBind.
 *	@ingroup avocore
 */
class QtBind {
public:

	//Bind To QT
	static QString	toQString(const X7sParam *pParam);
	static QColor 	toQColor(const X7sParam *pParam);
	static QImage	toQImage(IplImage *pImg,const QImage::Format& format=QImage::Format_ARGB32);
	static QLine  	toQLine(X7sVSAlrDetector *pAlrDtr);
	static QPolygon toQPolygon(X7sVSAlrDetector *pAlrDtr);

	//Bind From QT
	static X7sVSAlrDetector*	toX7sVSAlrDetector(X7sVSAlrManager *parent, const QPolygon& poly,const QColor& color=QColor());
	static X7sVSAlrDetector*	toX7sVSAlrDetector(X7sVSAlrManager *parent, const QLine&    line,const QColor& color=QColor());

};


namespace AvoBind {
	inline static std::string StdString(const QString &tmp) { return tmp.toStdString(); };
};
#endif /* QTBIND_H_ */
