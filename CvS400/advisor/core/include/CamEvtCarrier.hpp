/**
 *  @file
 *  @brief Contains the class CamEvtHolder3.h
 *  @date Nov 24, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef CAMEVTHOLDER_H_
#define CAMEVTHOLDER_H_

#ifndef CAMEVT_HPP_
class CamEvt;
class CamEvtActivity;
class CamEvtVSAlarm;
class CamEvtVideo;
#endif

/**
 *	@brief Carrier of a camera event.
 *
 * The camera event class use polymorphism, and is therefore must be use as a pointer if we want its generic definition.
 * The CamEvtCarrier let us manage easily the copy and destruction of the CamEvt object, to ease its transportation between
 * different class and thread.
 *
 *	@ingroup avo_core
 */
class CamEvtCarrier {

public:
	CamEvtCarrier();
	CamEvtCarrier(CamEvt* camEvt);
	CamEvtCarrier(const CamEvtCarrier& copy);
	CamEvtCarrier& operator=(const CamEvtCarrier& copy);
	void Release();
	virtual ~CamEvtCarrier();

	bool isValid() const;
	bool isType(int type) const;
	bool isNull() const { return !isValid(); }
	const CamEvt* getCamEvt() const { return m_camEvt; }

private:
	CamEvt *m_camEvt;		//!< Pointer on the data of the camera event.
};


#endif /* CAMEVTHOLDER_H_ */
