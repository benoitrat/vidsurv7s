/*
 * QSqlTableFactory.hpp
 *
 *  Created on: 29/09/2009
 *      Author: Andrea
 */

#ifndef QTABLEFACTORY_HPP_
#define QTABLEFACTORY_HPP_

#include <QtSql>
#include <QString>


namespace QSql {
enum DataType { Serial, Bool, SmallInt, Integer,BigInt, Real, Timestamp, VarChar };
enum DataProp { Null, NotNull };
};


struct QSqlDataCol {
	QString name;
	QSql::DataType type;
	QVariant defVal;
	int length;
	bool isnull;
};


class QSqlDataTable {
	friend class QSqlTableFactory;

public:
	QSqlDataTable(const QString& name);
	virtual ~QSqlDataTable();
	bool addColumn(const QString& name,const QSql::DataType& type, const QVariant& defVal=QVariant(),int length=-1);
	bool addColumn(const QString& name,const QSql::DataType& type, const QSql::DataProp& prop, int length=-1);
	bool addColumn(const QString& name,const QSql::DataType& type, const QSql::DataProp& prop, const QVariant& defVal, int length=-1);
	bool setColumnProperties(const QString& name, const QSql::DataProp& prop);
	bool addPrimaryKey(const QString& name);
	bool addForeignKey(const QString &colname, const QString& tablename);
	bool addIndex(const QString &name);
	bool contains(const QString &name, int *index=NULL);


	const QString& getTableName() const { return m_name; }
	const QStringList& getPrimaryKeys() const { return primaKey; }
	const QStringList& getIndexes() const { return indexKey; }
	const QList<QSqlDataCol>& getColumns() const { return m_listCols; }
	const QSqlDataCol* getColumnPtr() const;

private:
	QString m_name;
	QList<QSqlDataCol> m_listCols;
	QStringList primaKey;
	QList< QPair<QString,QString> > foreiKey;
	QStringList indexKey;
};



class QSqlTableFactory {
public:
	QSqlTableFactory(const QSqlDatabase& db);
	virtual ~QSqlTableFactory();
	bool CreateTable(const QSqlDataTable& table);

	static QSqlTableFactory * createTableFactory(const QSqlDatabase &db);

protected:
	virtual QString GetDefaultValue(const QSqlDataCol& col)=0;
	virtual QString GetPrimaryDef(const QString &tbname)=0;
	virtual QString GetDataType(const QSqlDataCol& col)=0;
	virtual QString Protect(const QString& name)=0;


private:
	QSqlDatabase m_db;
};


class QPSqlTableFactory : public QSqlTableFactory
{
public:
	QPSqlTableFactory(const QSqlDatabase& db) : QSqlTableFactory(db) {};
	virtual ~QPSqlTableFactory() {};

protected:
	QString GetDefaultValue(const QSqlDataCol& col);
	QString GetPrimaryDef(const QString &tbname);
	QString GetDataType(const QSqlDataCol& col);
	QString Protect(const QString& name) { return "\""+name+"\""; };
};


class QMySqlTableFactory : public QSqlTableFactory
{
public:
	QMySqlTableFactory(const QSqlDatabase& db) : QSqlTableFactory(db) {};
	virtual ~QMySqlTableFactory() {};

protected:
	QString GetDefaultValue(const QSqlDataCol& col);
	QString GetPrimaryDef(const QString &tbname);
	QString GetDataType(const QSqlDataCol& col);
	QString Protect(const QString& name) { return "`"+name+"`"; };
};

#endif /* QTABLEFACTORY_HPP_ */
