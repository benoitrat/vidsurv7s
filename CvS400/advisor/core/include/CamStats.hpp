/**
 *  @file
 *  @brief Contains the class CamStats.h
 *  @date Feb 16, 2010
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef CAMSTATS_H_
#define CAMSTATS_H_


#include <ctime>
#include <x7sdef.h>
#include <QLinkedList>
#include <QDateTime>


/**
 *	@brief The CamStats.
 *	@ingroup 
 */
class CamStats {
public:
	CamStats();
	virtual ~CamStats();

	void setup(bool isTime, int value);

	void reset();
	bool receivedFrame(int id);
	float updateFPS();
	float getFPS() { return m_fps; }
	float getFPSAvgM() { return m_fpsAvgM; }
	float getFPSAvgH() { return m_fpsAvgH; }
	int getLostNum(bool reset=true, QDateTime* last=NULL);




protected:
	void init();
	float updateFPST();
	float updateFPSF();

	void updateMinute();
	void updateHour();


private:

	QLinkedList<float> m_fpsAvgMList;
	QLinkedList<float> m_fpsAvgHList;
	int m_countM;
	int m_countH;
	float m_fpsSumM;
	float m_fpsSumH;
	float m_fpsAvgM;
	float m_fpsAvgH;


	bool m_isTimeUpdate;
	float m_fps;

	clock_t m_timeInterval;
	clock_t m_timePrev;
	int m_nFramesInter;
	int m_nFramesPrev;



	int previous_id;
	uint32_t previous_ts;


	int m_nLosts;	//!< Number of frame lost
	int m_nFrames;	//!< Number of frames
	QDateTime m_timeInit;		//!<Time when the counter was reinit-

};

#endif /* CAMSTATS_H_ */
