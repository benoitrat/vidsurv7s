/**
 *  @file
 *  @brief Contains the class Devices
 *  @date Jul 13, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef DEVICES_HPP_
#define DEVICES_HPP_

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes/Defines/Prototype
//----------------------------------------------------------------------------------------

#include "x7sxml.h"
#include "x7snet.h"
#include "x7svidsurv.h"

#include <vector>
#include <cstddef>
using std::vector;

#define AVO_TAGNAME_DEVICES "devices"

class Database;	//Forward declaration: this object is included in the .cpp
class Board;	//Forward declaration: this object is included in the .cpp
class Camera;	//Forward declaration: this object is included in the .cpp



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	The Main class
//----------------------------------------------------------------------------------------


/**
 *	@brief The Devices Manager.
 *	@ingroup avo_core
 */
class Devices: public X7sXmlObject {
public:
	Devices(X7sXmlObject *parent=NULL);
	virtual ~Devices();

	virtual void CheckConnection();

	//Usefull function
	bool Start();
	bool Stop();

	//Getters
	int GetSize() const { return m_vecBoard.size(); };	//!< Return the number of Board in the Devices manager.
	Board* GetBoard(int index) const { return m_vecBoard[index]; };	//!< Return a pointer on the board with the corresponding index
	Board* GetBoardByID(int ID) const;
	Camera* GetCameraByID(int ID) const;
	Camera* GetNextCamera(Camera *cam);


	//Setter
	bool AddBoard(Board *board);
	bool DelBoardByID(int ID);


protected:
	void DeleteAll();

private:
	virtual bool CreateChildFromXML(const TiXmlElement *child);
	Devices(const Devices& copy); 		// declared private, but not defined, to prevent copying
	Devices& operator=(const Devices&);  // declared private, but not defined, to prevent copying

private:
	std::vector<Board*> m_vecBoard;

};

#endif /* DEVICES_HPP_ */
