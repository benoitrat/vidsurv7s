/**
 *  @file
 *  @brief Contains the class VideoManager.hpp
 *  @date Oct 14, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef VIDEOHANDLER_HPP_
#define VIDEOHANDLER_HPP_




#include <x7sxml.h>
#include <QMap>
#include <QDateTime>
#include <QStringList>
#include <QDir>

#include "VideoInFile.hpp"

class Database;
class Camera;
class VideoOutFile;

#define AVO_TAGNAME_VIDMAN "vidhandler"
#define VIDEOHANDLER_RECORD_DEFVAL true

/**
 *	@brief The VideoManager.
 *	@ingroup avo_core
 */
class VideoHandler: public X7sXmlObject {
public:
	enum Mode {FULL, ACTIVITY, EVENT, ALARM};

public:
	VideoHandler(X7sXmlObject *parent);
	virtual ~VideoHandler();

	bool setParams();
	bool Apply(int depth);

	void AttachDatabase(Database* db) { m_db=db; }
	bool WriteFrame(const Camera *cam,const X7sByteArray *frameData);
	bool Update(const Camera *cam,bool isUpdate, bool isClosing);
	bool CtrlVideosDiskCapacity(const Camera *cam);
	bool DeleteOldVideo(const QDateTime& time, bool physical=true, const QString& pathPrefix=QString());
	bool IsTooFlush(const Camera *cam);

	static QStringList getModeNames();
	bool isRecordEnable(int dayOfWeek, int hour) const;
	bool isRecordEnable(const QDateTime& dtime) const;
	bool setRecordEnable(int dayOfWeek,int hour,bool enable);
	bool setRecordEnable(const X7sParam* pParam);
	bool setRecordEnable(bool enable);


	static bool FindVideos(QVector<VideoInFile>& vecVideo, const QDir &dir, bool recursive);
protected:
	void Init();


private:
	bool m_closing;
	bool m_recordsOn[7][24];	//Files to select all modes
	Database *m_db;
	QMap<const Camera *,VideoOutFile *> camFile;

};

#endif /* VIDEOMANAGER_HPP_ */
