/**
 *  @file
 *  @brief Contains the class VideoInFile.hpp
 *  @date Oct 16, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef VIDEOINFILE_HPP_
#define VIDEOINFILE_HPP_

/**
 *	@brief The VideoInFile.
 *	@ingroup avo_core
 */
#include "VideoFile.hpp"

#include <x7svidsurv.h>

#include <QDateTime>
#include <QImage>

class VideoInFile: public VideoFile {
public:
	VideoInFile();
	VideoInFile(const QString& FPath, const Camera *cam=NULL);
	VideoInFile(int camId, const QString& FPath,const QDateTime& tStart, const QDateTime &tLast, int video_id, qint64 size, bool ended, bool locked);


	virtual ~VideoInFile();

	bool Open();
	bool Open(const QString& FPath);
	bool Close();
	bool Delete(bool force=false);
	int GrabFrame(int jump=1);
	bool EmphasisAlarm(const AVOAlrEvt& alrEvt);

	bool JumpToAlarm(int jump=1);
	bool JumpToTime(const QDateTime& time,bool stayRF=false);
	bool Rewind();

	const QImage& getFrame() const { return imQtFrame; };
	const QImage& getMData() const { return imQtMData; };
	const QDateTime& getTimeEnd() const { return m_tEnd; }
	const QDateTime& getTime() const { return m_tActual; }
	qint64 getSize(bool force=false);
	int getDatabaseID() const { return m_dbID; }
	int getCamID() const { return m_cam?VideoFile::getCamID():m_camID; }
	const X7sVSAlrManager* getAlrManager() const { return pAlrMan; }

	bool isValid() const { return m_fileInfo.exists(); }
	bool isEnded() const { return m_isEnded; }
	bool isLocked() const { return m_isLocked; }


protected:
	void Init();
	bool ExtractHeader();
	bool ExtractFrame(int jump=1);
	virtual VideoFile* Clone() const { return (VideoFile*)new VideoInFile(*this); }


private:
	int m_dbID;
	int m_camID;
	int m_dbSize;
	bool m_isEnded;
	bool m_isLocked;

	int search_frame;
	X7sMJPEGFrame mjpegFrame;
	X7sMJPEGFrameRate mjpegFPS;
	X7sByteArray *frameData;
	X7sByteArray *pHeader;
	X7sBlobTrajGenerator *pTrajGen;
	X7sVSAlrManager *pAlrMan;

	QDateTime m_tEnd;
	QDateTime m_tActual;
	QImage imQtFrame;
	QImage imQtMData;
	IplImage *imCvMData;
	IplImage *imCvFrame;
};

#endif /* VIDEOINFILE_HPP_ */
