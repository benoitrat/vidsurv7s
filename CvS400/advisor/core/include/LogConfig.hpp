/**
 *  @file
 *  @brief Contains the class GeneralConf
 *  @date Sep 25, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */
#ifndef GENERALCONFIG_HPP_
#define GENERALCONFIG_HPP_

#include <x7sxml.h>
#include <QPair>
#include <QFileInfo>

#define AVO_TAGNAME_GENCONF "config"
#define AVO_LOG_MAX_FISZE 10485760	//!< Maximum file size for a log is about ~10MB

/**
 *	@brief Used to save and manage the general parameters of Advisor.
 *	@ingroup avo_core
 */
class LogConfig: public X7sXmlObject {
public:
	LogConfig(X7sXmlObject *parent);
	virtual ~LogConfig();
	bool Apply(int depth=-1);
	bool UpdateLogFiles();

private:
	FILE* CreateLogFile(const QFileInfo& newFileInfo, QPair<QFileInfo,FILE*>& logFile);

private:
	QString lastFilePattern;
	int lastFilePatternNum;
	QPair<QFileInfo,FILE*> log_avoOut;
	QPair<QFileInfo,FILE*> log_avoErr;
	QPair<QFileInfo,FILE*> log_x7sOut;
	QPair<QFileInfo,FILE*> log_x7sErr;
};

#endif /* GENERALCONFIG_HPP_ */
