#################################
# AVOCore Library
#
#
# This library define the following variable that must be reused.
#
# 		- The name of the library: 					AVO_CORE_LIBRARY
#		- The include header of the library:	AVO_CORE_INCLUDE_DIR
#
#	Versions:
#		- 091001: Creation of the core library 	  
#		- 091029: Add the variable as internal cache variable.
#
#	Created by Benoit RAT 
#################################


SET(TARGETNAME "AVOCore")
project(${TARGETNAME})


MESSAGE(STATUS  "===============================================================================")
MESSAGE(STATUS  "Configure ${EXECNAME}:")
MESSAGE(STATUS  "-------------------------------------------------------------------------------")

## Find QT4 librarie and set important variables.
FIND_PACKAGE( Qt4 REQUIRED )


## Add the core directory to the project 
SET(HDR_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include)
SET(SRC_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/src)  
	
## Add the core source files to the project
SET(CPP_SRCS
${SRC_DIRECTORY}/Log.cpp
${SRC_DIRECTORY}/LogConfig.cpp
${SRC_DIRECTORY}/Camera.cpp
${SRC_DIRECTORY}/CamEvt.cpp
${SRC_DIRECTORY}/CamStats.cpp
${SRC_DIRECTORY}/CamEvtCarrier.cpp
${SRC_DIRECTORY}/Board.cpp
${SRC_DIRECTORY}/Devices.cpp
${SRC_DIRECTORY}/Database.cpp
${SRC_DIRECTORY}/QSqlTableFactory.cpp
${SRC_DIRECTORY}/QFileSystemInfo.cpp
${SRC_DIRECTORY}/TimeTicker.cpp
${SRC_DIRECTORY}/Root.cpp
${SRC_DIRECTORY}/VideoFile.cpp
${SRC_DIRECTORY}/VideoOutFile.cpp
${SRC_DIRECTORY}/VideoInFile.cpp
${SRC_DIRECTORY}/VideoHandler.cpp
${SRC_DIRECTORY}/QtBind.cpp
)

## Add the list of header files that should be treated with moc
## (all headers that have Q_OBJECT directive should be treated as MOC_HDRS)
SET(MOC_HDRS
${HDR_DIRECTORY}/TimeTicker.hpp
)


# The .cpp files:
file(GLOB HDR_FILES ${HDR_DIRECTORY}/*.h*)


# ----------------------------------------------------------------------------------
# 				Define the library target:
# ----------------------------------------------------------------------------------

# Set include dir for QT
SET( QT_USE_QTCORE TRUE )
SET( QT_USE_QTSQL TRUE )
SET( QT_USE_QTGUI FALSE )
INCLUDE( ${QT_USE_FILE} )

## Generate moc source:
QT4_WRAP_CPP(MOC_SRCS ${MOC_HDRS} )


# Set compilation options
include_directories(${HDR_DIRECTORY} ${X7S_INCLUDES} ${3RDPARTY_INCLUDES})
add_library(${TARGETNAME} STATIC ${CPP_SRCS} ${MOC_SRCS} ${HDR_FILES} )
add_dependencies(${TARGETNAME} ${X7S_LIBRARIES})
target_link_libraries(${TARGETNAME} ${X7S_LIBRARIES} ${OpenCV_LIBS} ${QT_LIBRARIES})

SET(AVO_CORE_LIBRARY ${TARGETNAME} CACHE INTERNAL "")
SET(AVO_CORE_INCLUDE_DIR ${HDR_DIRECTORY} CACHE INTERNAL "")

MESSAGE(STATUS  "AVO_CORE_LIBRARY=${AVO_CORE_LIBRARY}")
MESSAGE(STATUS  "AVO_CORE_INCLUDE_DIR=${AVO_CORE_INCLUDE_DIR}")

MESSAGE(STATUS  "-------------------------------------------------------------------------------")
MESSAGE(STATUS  "")