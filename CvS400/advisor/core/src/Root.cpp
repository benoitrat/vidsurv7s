#include "Root.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------

#include "Devices.hpp"
#include "Database.hpp"
#include "VideoHandler.hpp"
#include "LogConfig.hpp"
#include "Log.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------


Root::Root(const QString& fname)
:X7sXmlObject(NULL,AVO_TAGNAME_ROOT),m_fname(fname)
{
	AVO_PRINT_DEBUG("Root::Root()");
	m_logconf = new LogConfig(this);
	m_database = new Database(this);
	m_devices = new Devices(this);
	m_vidHandler= new VideoHandler(this);
}

/**
 * @brief Default destructor (Order is important)
 *
 * @warning The order of deleting object must be respected!
 * 		- First the video handler (use the camera and database)
 * 		- Then the devices , boards and cameras.
 * 		- Finally, the database which write the last action.
 */
Root::~Root()
{
	AVO_PRINT_DEBUG("Root::~Root()");
	X7S_DELETE_PTR(m_vidHandler);
	X7S_DELETE_PTR(m_devices);
	X7S_DELETE_PTR(m_logconf);
	X7S_DELETE_PTR(m_database);	//Destroy at the end the database to be sur that everybody could write inside correctly.

}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

bool Root::LoadFile()
{
	return LoadFile(m_fname);
}

bool Root::LoadFile(const QString& fname)
{
	if(fname.isEmpty()) return false;

	TiXmlDocument doc;
	FILE *fp = fopen( fname.toStdString().c_str(), "rb");

	//Load the document with the filename.
	if(doc.LoadFile(fp)==false) return false;

	//Obtain the root node.
	TiXmlNode *xml_root=doc.RootElement();

	//And read the XML value (recursively)
	bool ret= this->ReadFromXML(xml_root);

	fclose( fp );
	return ret;
}

bool Root::SaveFile()
{
	return SaveFile(m_fname);
}

bool Root::SaveFile(const QString& fname)
{
	if(fname.isEmpty()) {
		AVO_PRINT_WARN("Root::SaveFile()","fname is empty");
		return false;
	}

	//Create a document with a root node
	TiXmlDocument doc(fname.toStdString());
	TiXmlElement *root = new TiXmlElement(AVO_TAGNAME_ROOT);
	doc.LinkEndChild(new TiXmlDeclaration( "1.0", "", "" ));
	doc.LinkEndChild(root);

	//Write in the root node all the configuration
	this->WriteToXML(root);

	//Debugging message
	AVO_PRINT_INFO("Root::SaveFile()","saving in %s",doc.Value());

	//Save the root node to the given file.
	bool ret = doc.SaveFile();
	if(!ret) 	AVO_PRINT_WARN("Root::SaveFile()","could not save in %s",doc.Value());
	return ret;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

