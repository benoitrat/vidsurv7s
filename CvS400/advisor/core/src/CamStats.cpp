#include "CamStats.hpp"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
CamStats::CamStats() {
	init();
}

CamStats::~CamStats() {
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------
void CamStats::reset()
{
	//And finally reset the time.
	m_nFramesPrev=m_nFrames;
	m_timePrev=clock();
}


bool CamStats::receivedFrame(int id)
{
	int diff=id-previous_id;
	if(diff > 1 && previous_id!=-1)
	{
		m_nLosts+=(diff-1);
	}


	m_nFrames++;
	previous_id=id;

	updateFPS();

	return true;
}


int CamStats::getLostNum(bool reset, QDateTime* last)
{
	//Take the current value
	int nLost=m_nLosts;
	if(last) *last=m_timeInit;

	//Reset them if necessary
	if(reset)
	{
		m_timeInit=QDateTime::currentDateTime();
		m_nLosts=0;
	}

	//And return the number lost.
	return nLost;
}


float CamStats::updateFPS()
{
	return (m_isTimeUpdate)?updateFPST():updateFPSF();
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

void CamStats::init()
{
	m_isTimeUpdate=true;
	previous_id=-1;
	previous_ts=0;
	m_nLosts=0;
	m_nFrames=0;

	m_timeInterval = CLOCKS_PER_SEC;
	m_nFramesInter=10;
	m_timeInit=QDateTime::currentDateTime();
}



/**
 * @brief Update FPS using time interval
 * @return
 */
float CamStats::updateFPST()
{
	clock_t elap_time = clock() - m_timePrev;
	if(elap_time >= m_timeInterval)
	{
		m_fps=((float)(m_nFrames-m_nFramesPrev))*((float)CLOCKS_PER_SEC/(float)elap_time);

		//And finally reset the time.
		m_nFramesPrev=m_nFrames;
		m_timePrev=clock();

		updateMinute();
	}
	return m_fps;
}


float CamStats::updateFPSF()
{
	int elap_frame= m_nFrames-m_nFramesPrev;
	if(elap_frame >= m_nFramesInter)
	{
		m_fps= ((float)elap_frame)*((float)CLOCKS_PER_SEC/(float)(clock()-m_timePrev));

		//And finally reset the time.
		m_nFramesPrev=m_nFrames;
		m_timePrev=clock();
	}
	return m_fps;
}


void CamStats::updateMinute()
{
	float firstFPS=0;

	if(m_fpsAvgMList.size()>=60) firstFPS = m_fpsAvgMList.takeFirst();
	m_fpsAvgMList.push_back(m_fps);
	m_fpsSumM+=(m_fps-firstFPS);
	m_fpsAvgM = m_fpsSumM/m_fpsAvgMList.size();

	m_countM++;
	if(m_countM%60==0) updateHour();
}


void CamStats::updateHour()
{
	float firstFPS=0;

	if(m_fpsAvgHList.size()>=60) firstFPS = m_fpsAvgHList.takeFirst();
	m_fpsAvgHList.push_back(m_fps);
	m_fpsSumH+=(m_fps-firstFPS);
	m_fpsAvgH = m_fpsSumH/m_fpsAvgHList.size();
}
