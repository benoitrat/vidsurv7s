#include <Log.hpp>
#include <QDateTime>

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Initialization of static (global) variable X7sNetLog.
//---------------------------------------------------------------------------------------
#if (defined WIN32 || defined WIN64)
#define ESCAPE_CHAR '\\'
#else
#define ESCAPE_CHAR '/'
#endif


/**
* @brief The default value of the X7sIoLog::level
*/
int Log::level = X7S_LOGLEVEL_MED;
int Log::noError= X7S_LOGLEVEL_MED;
FILE* Log::ferr=stderr;
FILE* Log::fout=stdout;


/**
* @brief The array to convert the log level in a c-string.
*/
const char* LogName[X7S_LOGLEVEL_END] =
{
		"Fatal",
		"Error",
		"Warn ",
		"Info ",
		"Debug",
		"DebuG",
};

const QtMsgType types[X7S_LOGLEVEL_END] =
{
		QtFatalMsg,
		QtCriticalMsg,
		QtWarningMsg,
		QtDebugMsg,
		QtDebugMsg,
		QtDebugMsg,
};





//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Static public functions
//----------------------------------------------------------------------------------------


AVOLogLine::AVOLogLine(const char *file, int line, int level,bool space,const QString& funcName)
:QDebug(&str), fileInfo(file), line(line), level(level)
 {
	bool with_time=true;
	char buffer [80];
	if(with_time)
	{
		time_t rawtime;
		struct tm * timeinfo;
		time ( &rawtime );
		timeinfo = localtime ( &rawtime );
		strftime(buffer,80,"[%H:%M:%S]",timeinfo);
	}

	this->nospace();
	*this <<  buffer << " ";
	*this << (const char*)LogName[level];
	*this << " > ";
	*this << funcName.toStdString().c_str();
	*this << ":";

	if(space) this->space();
	else this->nospace();
 }

AVOLogLine::~AVOLogLine()
{
	if(Log::IsDisplay(level))
	{
		this->nospace();
		*this << "(" << fileInfo.fileName().toStdString().c_str() << ":" << line << ")";
		qt_message_output(types[level],str.toLocal8Bit().data());
	}
}


void AVOFPrintf(FILE *logfile, FILE *console, const char *msg)
{
#ifdef DEBUG
	if(logfile!=console)
	{
		fprintf(console,"%s\n",msg);
		fflush(console);
	}
#endif
	fprintf(logfile,"%s\n",msg);
	fflush(logfile);
}


void AVOMsgOutput(QtMsgType type, const char *msg)
{
	switch (type) {
	case QtDebugMsg:
		AVOFPrintf(Log::fout,stdout,msg);
		break;
	case QtWarningMsg:
		AVOFPrintf(Log::ferr,stderr,msg);
		break;
	case QtCriticalMsg:
		AVOFPrintf(Log::ferr,stderr,msg);
		break;
	case QtFatalMsg:
		AVOFPrintf(Log::ferr,stderr,msg);
		AVOFPrintf(Log::ferr,stderr,"ERROR IS FATAL => abort()");
		abort();
	}
}

bool Log::Config(int log_level, FILE *f_out, FILE *f_err, int noError)
{
	bool ret=Log::SetLevel(log_level);
	Log::noError=noError;

	//Check if the file is valid.
	if(f_out==NULL) f_out=stdout;
	if(f_err==NULL) f_err=stderr;

	//Set the new one
	Log::fout=f_out;
	Log::ferr=f_err;

	return ret;
}

/**
* @brief Set the level of print for this static class.
* @param log_level The level given by @ref X7S_LOGLEVEL_xxx
* @return @true if the operation performs correctly.
*/
bool Log::SetLevel(int log_level)
{
	if(X7S_LOGLEVEL_LOWEST <= log_level && log_level < X7S_LOGLEVEL_END)
	{
		Log::level=log_level;
		return true;
	}
	return false;
}

/**
* @brief If the given level is going to be display.
*
* The given level is compared with the level set with SetLevel()
* @note SetLevel(), x7sIoLog_SetLevel()
*/
bool Log::IsDisplay(int level)
{
	return (level <= Log::level);
}

/**
* @brief Print Log message with a specific level
*
* @param funcname The name of the function that we want to be displayed
* @param file	The name of the file that call this function.
* @param line	The line of the file where the call of this function was produced.
* @param level The log level (e.i.@ref X7S_LOGLEVEL_xxx)
* @param szFormat The format of the output.
* @param ... The variable argument list (va_list).
*
*/
void Log::PrintLevel(const char *funcname,const char *file, int line, int level, const char* szFormat, ...) {

	if(X7S_LOGLEVEL_LOWEST <= level && level < X7S_LOGLEVEL_END)
	{
		va_list args;
		va_start(args,szFormat);
		FILE *output;
		if(level < noError) output=Log::ferr;
		else 	output=Log::fout;

#ifdef DEBUG
		if(level < noError)
		{
			if(output!=stderr) Log::Print(true,(const char*)LogName[level],funcname,file,line,stderr,szFormat,args);
		}
		else
		{
			if(output!=stdout) Log::Print(true,(const char*)LogName[level],funcname,file,line,stdout,szFormat,args);
		}
#endif
			Log::Print(true,(const char*)LogName[level],funcname,file,line,output,szFormat,args);
	}
}




//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Static private functions
//----------------------------------------------------------------------------------------

/**
* @brief Print a message with the following options:
*
* @param with_time	Add the time to the print.
* @param prefix	Add a prefix to the print (Debug, Warn, ...)
* @param funcname	Add the funcname to the print (if @NULL nothing is added)
* @param filepath 	Add the filepath at the end of the print (if @NULL nothing is added)
* @param line		Add the line number at the end of the print (if @NULL nothing is added).
* @param stream	In which stream to print the message (stdout, stderr, ...).
* @param szFormat	The format of the message
* @param args		The variable argument list.
*/
void Log::Print(bool with_time, const char *prefix, const char *funcname,const char *filepath, int line, FILE *stream, const char *szFormat, va_list args)
{
	char buffer [80];
	const char *filename;
	if(with_time)
	{
		time_t rawtime;
		struct tm * timeinfo;
		time ( &rawtime );
		timeinfo = localtime ( &rawtime );
		strftime(buffer,80,"[%H:%M:%S]",timeinfo);
	}
	if(funcname==NULL) fprintf(stream,"%s %s: ",buffer,prefix);
	else fprintf(stream,"%s %s > %s: ",buffer,prefix,funcname);

	if(szFormat) vfprintf(stream,szFormat,args);
	if(filepath)
	{
		filename = strrchr(filepath,ESCAPE_CHAR);
		if(filename) filename++;
		else filename=filepath;
		fprintf(stream," (%s:%d)",filename,line);
	}
	fprintf(stream,"\n");
	fflush(stream);
	va_end(args);
}




