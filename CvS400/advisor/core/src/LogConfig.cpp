#include "LogConfig.hpp"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------


#include "QtBind.hpp"
#include "Log.hpp"
#include <x7svidsurv.h>
#include <x7sxml.h>
#include <x7snet.h>
#include <x7scv.h>
#include <QString>
#include <QDir>
#include <QDateTime>

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

LogConfig::LogConfig(X7sXmlObject *parent)
:X7sXmlObject(parent,AVO_TAGNAME_GENCONF)
 {

	log_avoOut.second=NULL;
	log_avoErr.second=NULL;
	log_x7sOut.second=NULL;
	log_x7sErr.second=NULL;
	lastFilePatternNum=0;

	int log_min=X7S_LOGLEVEL_LOWEST;
	int log_max=X7S_LOGLEVEL_HIGHEST;

	//Log parameters
	m_params.Insert(X7sParam("log",X7S_XML_ANYID,X7S_LOGLEVEL_MED,X7STYPETAG_INT,log_min,log_max));
	m_params.Insert(X7sParam("log_vs",X7S_XML_ANYID,X7S_LOGLEVEL_LOW,X7STYPETAG_INT,log_min,log_max));
	m_params.Insert(X7sParam("log_net",X7S_XML_ANYID,X7S_LOGLEVEL_LOW,X7STYPETAG_INT,log_min,log_max));
	m_params.Insert(X7sParam("log_xml",X7S_XML_ANYID,X7S_LOGLEVEL_LOW,X7STYPETAG_INT,log_min,log_max));
	m_params.Insert(X7sParam("log_cv",X7S_XML_ANYID,X7S_LOGLEVEL_LOWEST,X7STYPETAG_INT,log_min,log_max));
	m_params.Insert(X7sParam("log_path",X7S_XML_ANYID,"",X7STYPETAG_STRING));

	//Misc parameters
	m_params.Insert(X7sParam("misc_lang",X7S_XML_ANYID,"",X7STYPETAG_STRING));
 }

LogConfig::~LogConfig()
{

}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

/**
* @brief Apply the parameters to the object after writing them with SetParams().
*
* First, A call to UpdateLogFiles() to open the log files if a new logpath was set.
* Then, we set and update the level of log if they have been changed.
*
* @return
*/
bool LogConfig::Apply(int depth)
{
	AVO_PRINT_DEBUG("GeneralConf::Apply()","depth=%d",depth);

	//First apply to the child.
	X7sXmlObject::Apply(depth);

	bool ret = UpdateLogFiles();

	//Then apply each parameters.
	Log::SetLevel(m_params["log"]->toIntValue());
	x7sVSLog_SetLevel(m_params["log_vs"]->toIntValue());
	x7sNetLog_SetLevel(m_params["log_net"]->toIntValue());
	x7sXmlLog_SetLevel(m_params["log_xml"]->toIntValue());
	x7sCvLog_SetLevel(m_params["log_cv"]->toIntValue());

	return ret;
}

/**
* @brief Update the log file
* Update using the @tt{log_path} parameter. If this parameter is empty or contains only spaces,
* the log files are not created.
* Otherwise, we create the directory and the corresponding logFile @see CreateLogFile.
* then each of these log files is attached to the corresponding log object.
* And the old log file are finally closed.
*
* @return @true
*/
bool LogConfig::UpdateLogFiles()
{
	QString dayStrNum;
	QString logPath = QtBind::toQString(m_params["log_path"]);


	QFileInfo newFinfo_avoOut, newFinfo_avoErr, newFinfo_x7sOut, newFinfo_x7sErr;

	QRegExp rx("\\s*");
	if(logPath.isEmpty() || rx.exactMatch(logPath))  return false;

	//Check the new file info
	QDir logDir = QDir(logPath);
	if(logDir.exists())
	{
		QString dayStr = QDateTime::currentDateTime().toString("yyMMdd");

		//Check if we have already this file pattern used
		if(log_avoOut.second && lastFilePattern == dayStr)
		{
			//If the file is too long set the next file pattern num.
			long int nbytes = ftell(log_avoOut.second);
			if(nbytes > AVO_LOG_MAX_FISZE) lastFilePatternNum++;
		}
		else
		{
			lastFilePattern = dayStr;
			lastFilePatternNum=0;
		}
		
		dayStrNum = dayStr+"-"+QString::number(lastFilePatternNum);

		newFinfo_avoOut=QFileInfo(logDir,dayStrNum+"_avo_out.log");
		//		newFinfo_avoErr=QFileInfo(logDir,dayStr+"_avo_err.log");
		//		newFinfo_x7sOut=QFileInfo(logDir,dayStr+"_x7s_out.log");
		//		newFinfo_x7sErr=QFileInfo(logDir,dayStr+"_x7s_err.log");

		avoDebug("LogConfig::UpdateLogFiles()") << dayStrNum << newFinfo_avoOut.filePath() << log_avoOut.second;
	}

	//Create the new logFile.
	FILE *oldFile_avoOut = CreateLogFile(newFinfo_avoOut,log_avoOut);
	//	FILE *oldFile_avoErr = CreateLogFile(newFinfo_avoErr,log_avoErr);
	//	FILE *oldFile_x7sOut = CreateLogFile(newFinfo_x7sOut,log_x7sOut);
	//	FILE *oldFile_x7sErr = CreateLogFile(newFinfo_x7sErr,log_x7sErr);



	avoDebug("LogConfig::UpdateLogFiles()") << log_avoOut.first.filePath() << log_avoOut.second;

	//Same log file for everything.
	log_avoErr = log_avoOut;
	log_x7sOut = log_avoOut;
	log_x7sErr = log_avoOut;


	//Attach the new logFile.
	Log::Config(m_params["log"]->toIntValue(),log_avoOut.second,log_avoErr.second);
	x7sVSLog_Config(m_params["log_vs"]->toIntValue(),log_x7sOut.second,log_x7sErr.second);
	x7sCvLog_Config(m_params["log_cv"]->toIntValue(),log_x7sOut.second,log_x7sErr.second);
	x7sNetLog_Config(m_params["log_net"]->toIntValue(),log_x7sOut.second,log_x7sErr.second);
	x7sXmlLog_Config(m_params["log_xml"]->toIntValue(),log_x7sOut.second,log_x7sErr.second);

	//And finally close the old file (if valid).
	if(oldFile_avoOut) fclose(oldFile_avoOut);
	//	if(oldFile_avoErr) fclose(oldFile_avoErr);
	//	if(oldFile_x7sOut) fclose(oldFile_x7sOut);
	//	if(oldFile_x7sErr) fclose(oldFile_x7sErr);

	return true;


}


/**
* @brief Create a log file by checking the previous one.
*
*
*
* @note The logFile is given by reference and therefore can be modified
* @param newFileInfo The QFileInfo of the "new" LogFile
* @param logFile the current logFile (a tupple of FILE* and QFileInfo) that can be changed if
* the newFileInfo is valid.
*
* @return a pointer on the FILE* to close. If no file need to be close it returns NULL.
*/
FILE* LogConfig::CreateLogFile(const QFileInfo& newFileInfo, QPair<QFileInfo,FILE*>& logFile)
{
	FILE *oldFile=NULL;
	FILE *newFile=NULL;


	//Check if the new FileInfo is different from the actual one
	if(logFile.first.absoluteFilePath()!=newFileInfo.absoluteFilePath())
	{
		//Keep the old file to close it.
		oldFile=logFile.second;

		//Check if it's possible to write in this new file
		if(!newFileInfo.absoluteFilePath().isEmpty())
		{
			//Create new file
			newFile=fopen(newFileInfo.absoluteFilePath().toStdString().c_str(),"w");
		}
		else
		{
			avoWarning("LogConfig::CreateLogFile()") << "Empty file path" << newFileInfo.absoluteFilePath();
		}

		//Error at opening new file.
		if(newFile==NULL) {
			avoWarning("LogConfig::CreateLogFile()") << "Could not open file"<< newFileInfo.absoluteFilePath();
			return NULL;
		}

		//Than attach the new one
		logFile.first=newFileInfo;
		logFile.second=newFile;
	}
	//Return the old-file that must be deleted.
	return oldFile;
}
