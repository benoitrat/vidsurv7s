#include "VideoHandler.hpp"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Camera.hpp"
#include "Log.hpp"
#include "Database.hpp"
#include "VideoOutFile.hpp"
#include "QFileSystemInfo.hpp"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
VideoHandler::VideoHandler(X7sXmlObject *parent)
:X7sXmlObject(parent,AVO_TAGNAME_VIDMAN)
 {
	Init();

 }

VideoHandler::~VideoHandler()
{
	AVO_PRINT_DEBUG("VideoHandler::~VideoHandler()");
	m_closing=true;

	QMap<const Camera *, VideoOutFile*>::iterator i;
	for (i = camFile.begin(); i != camFile.end(); ++i)
	{
		VideoOutFile *file = i.value();
		file->Close();
		m_db->Update(file);
		X7S_DELETE_PTR(file);
	}
	camFile.clear();
}

void VideoHandler::Init()
{
	m_closing=false;
	m_params.Insert(X7sParam("records_mon",	1,"",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("records_tue",		2,"",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("records_wed",	3,"",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("records_thu",		4,"",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("records_fri",		5,"",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("records_sat",		6,"",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("records_sun",		7,"",X7STYPETAG_STRVEC));
	m_params.Insert(X7sParam("mode",	X7S_XML_ANYID,0,X7STYPETAG_INT,0,sizeof(VideoHandler::Mode)-1));
	//m_params.Insert(X7sParam("general_mode",	X7S_XML_ANYID,1,X7STYPETAG_BOOL));
	this->setRecordEnable(VIDEOHANDLER_RECORD_DEFVAL);
	this->setParams();
}

bool VideoHandler::Apply(int depth)
{
	bool ret=true;

	for(int d=1;d<=7;d++) {
		if(!m_params[d]->IsNull() && m_params[d]->GetType()==X7STYPETAG_STRVEC)
		{
			ret = ret & setRecordEnable(m_params[d]);
		}
	}

	//Call the children
	ret= ret & X7sXmlObject::Apply(depth);
	return ret;
}


bool VideoHandler::setParams()
{
	bool ret=true;
	for(int d=1;d<=7;d++) {
		if(!m_params[d]->IsNull() && m_params[d]->GetType()==X7STYPETAG_STRVEC)
		{
			QString string;
			QTextStream textstr(&string);
			for(int h=0;h<24;h++) textstr << (int)isRecordEnable(d,h) << " ";
			ret = ret & m_params[d]->SetValue(string.toStdString());
		}
	}
	return ret;
}


bool VideoHandler::isRecordEnable(const QDateTime& dtime) const {
	return isRecordEnable(dtime.date().dayOfWeek(),dtime.time().hour());
}

bool VideoHandler::isRecordEnable(int dayOfWeek,int hour) const {

	if(X7S_CHECK_RANGE(dayOfWeek,1,8) && X7S_CHECK_RANGE(hour,0,24))
	{
		return m_recordsOn[dayOfWeek-1][hour];
	}
	return false;
}

QStringList VideoHandler::getModeNames() {
	QStringList strListIn, strListOut;

	strListIn <<  QT_TRANSLATE_NOOP("VideoHandler","Full")
			<< QT_TRANSLATE_NOOP("VideoHandler","Activity")
			<< QT_TRANSLATE_NOOP("VideoHandler","Event")
			<< QT_TRANSLATE_NOOP("VideoHandler","Alarm");
	QStringListIterator i(strListIn);
	while(i.hasNext())
	{
		strListOut << qApp->translate("VideoHandler",i.next().toStdString().c_str());
	}


	return strListOut;
}

bool VideoHandler::setRecordEnable(int dayOfWeek,int hour,bool enable)
{
	if(X7S_CHECK_RANGE(dayOfWeek,1,8) && X7S_CHECK_RANGE(hour,0,24))
	{
		m_recordsOn[dayOfWeek-1][hour]=enable;
		return true;
	}
	return false;
}

bool VideoHandler::setRecordEnable(const X7sParam* pParam)
{
	AVO_CHECK_WARN("VideoHandler::setModes()",pParam,false,"pParam is NULL");
	int d=pParam->GetID();
	if(X7S_CHECK_RANGE(d,1,8))
	{
		//void *tmp=(void*)pParam;
		//std::cout << pParam->GetName() << ": "<< pParam << tmp << std::endl;
		int vecSize=pParam->GetLength();
		for(int h=0;h<24;h++) {
			double mode;
			if(h < vecSize && pParam->GetRealValue(h,&mode))
			{
				m_recordsOn[d-1][h]=(mode!=0);
			}
			else
			{
				m_recordsOn[d-1][h]=(bool)VIDEOHANDLER_RECORD_DEFVAL;
			}
		}
		return true;
	}
	return false;
}

bool VideoHandler::setRecordEnable(bool enable)
{
	for(int d=1;d<=7;d++)
		for(int h=0;h<24;h++)
			m_recordsOn[d-1][h]=enable;
	return true;
}



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

bool VideoHandler::WriteFrame(const Camera *cam,const X7sByteArray *frameData)
{
	X7S_FUNCNAME("VideoHandler::WriteFrame()");

	int ret=X7S_RET_ERR;
	VideoOutFile *video=NULL;
	bool flush=true;


	if(cam->IsToWrite() && m_closing==false)
	{

		//Check if the video file exist
		if(camFile.find(cam) == camFile.end()) camFile[cam] = new VideoOutFile(cam);

		//Obtain the video file
		video=camFile[cam];

		//Check if the video is correctly set.
		AVO_CHECK_WARN(funcName,(video && !video->isEmpty()),false,"Video is NULL or empty");

		flush = IsTooFlush(cam);

		//Check if we need to record the frame.
		bool isInRecordingTime =this->isRecordEnable(cam->GetTime());
		if(isInRecordingTime==false || cam->isResetVideo())
		{
			if(video->isOpened())
			{
				if(flush) video->FlushBuffer();
				video->Close();
				((Camera*)cam)->resetVideo(false);
			}
		}
		if(isInRecordingTime==false) return false; //Exit if we are in recording time.

		//Tell the camera that it is recorded.
		((Camera*)cam)->SetRecording(flush);

		// Autoclose the file if it's necessary
		if(video->AutoClose())	m_db->Update(video);


		bool wasClosed=video->isClosed();
		bool isOk = video->BufferizeFrame(frameData,flush);
		if(!isOk)
		{
			//If an error has occured and the file was auto close.
			if(video->wasAutoClosed()) 	{
				m_db->Update(video);
				return WriteFrame(cam,frameData); //Recall the function
			}
		}
		else
		{
			if(video->isOpened())
			{
				if(wasClosed) {
					this->CtrlVideosDiskCapacity(cam);
					m_db->Insert(video);
				}
				else {
					if(video->getSeconds()%60==0) m_db->Update(video);
					if(cam->GetAlarmManager()->HasEvent()) {
						//avoDebug(funcName) << "cam->hasEvent => db->Update";
						m_db->Update(video);
					}
				}
			}
		}
	}
	return (ret==X7S_RET_OK);
}



bool VideoHandler::IsTooFlush(const Camera *cam)
{
	AVO_CHECK_WARN("VideoHandler::IsTooFlush",cam,false,"cam is NULL");

	X7sVSAlrManager *alrMgr=NULL;
	alrMgr = cam->GetAlarmManager();

	VideoHandler::Mode mode = (VideoHandler::Mode)m_params["mode"]->toIntValue();
	switch(mode)
	{
	case FULL:
		return true;
	case ACTIVITY:
		return cam->HasActivity();
	case EVENT:
		return (alrMgr)?alrMgr->HasEvent():false;
	case ALARM:
		return (alrMgr)?alrMgr->HasAlarm():false;
	}
	return false;
}


/**
* @brief Control the Videos' Disk Capacity (Check and Erase if necessary).
*
* This function erase the oldest videos in the disk.
*
*
* @param cam The camera on which we want to control disk capacity.
* @return @true if some files have been erased.
*/
bool VideoHandler::CtrlVideosDiskCapacity(const Camera *cam)
{

	X7S_FUNCNAME("VideoHandler::CtrlVideosDiskCapacity()");

	bool ret=false;
	qint64 toFreeByte=0;
	double minFreeRatio=0.2;

	AVO_CHECK_WARN(funcName,cam,false,"Camera is not valid pointer");


	//First obtain the space we need to liberate.
	QFileSystemInfo fSysInfo( cam->GetVideoRootDir());


	if(fSysInfo.isValid())
	{

		avoDebug(funcName) << "cam_id=" << cam->GetSpecialID() << fSysInfo;
		ret=true;

		//This is the minimum condition to keep (always)
		if(fSysInfo.getFreeSpace() < 2.0*X7S_CV_MJPEG_MAXFILESIZE)
		{
			toFreeByte= 2*(qint64)X7S_CV_MJPEG_MAXFILESIZE;	//in bytes.
		}

		//This is the desired condition.
		if(X7S_INRANGE(minFreeRatio,0.0,1.0) && (fSysInfo.getFreeRatio() < minFreeRatio))
		{
			toFreeByte = fSysInfo.getTotalSpace()*minFreeRatio*1.5;	//Free a little more than the ratio to be sure to not always free.
		}
	}
	else 	avoError(funcName) << "Can not get free disk space";

	//Then free some space if it is necessary.
	if(toFreeByte>0)
	{
		avoInfo(funcName) << "To free" << toFreeByte << "B, " << toFreeByte/((double)X7S_NBYTES_GIB) << "GiB" << fSysInfo;

		QVector<VideoInFile> vecVideos =
				m_db->GetVideos("WHERE path LIKE '"+fSysInfo.getMntPoint()+"%' ORDER BY tstart ASC LIMIT 200");

		qint64 sum=0;
		for(int i=0;i<vecVideos.size();++i)
		{
			VideoInFile& video = vecVideos[i];

			QFileInfo fDbInfo(video.getFileInfo());

			if(fDbInfo.exists())
			{

				QFileSystemInfo fDbSysInfo(fDbInfo);
				if(fSysInfo.isOnSameMntPoint(fDbSysInfo))
				{

					//Obtain the correct video size (looking at database)
					qint64 vidSize=-1;
					if(video.isEnded()) 	vidSize=video.getSize(false);
					if(vidSize==-1) vidSize=video.getSize(true);	//Force opening the video if it doesn't close correclty in database.

					if(vidSize>0) sum+=vidSize;

					avoInfo(funcName) << "Deleting" << video.getCamID() << vidSize/((double)X7S_NBYTES_MIB) << "MiB" << fDbSysInfo;

					//Delete database entry that correspond to database file
					m_db->Delete(video);

					//Then delete the file
					video.Delete();

					if(sum>=toFreeByte) break;
				}
				else
				{
					avoDebug(funcName) << fDbSysInfo << "Not on same mounting point";
				}
			}
			else //The video has been already deleted (Delete the corresponding database entry).
			{
				avoInfo(funcName) << "Not exist anymore, Deleting from Db" << video.getFileInfo().filePath();
				m_db->Delete(video);
			}
		}
		if((sum+1024)<toFreeByte)
		{
			avoWarning(funcName) << "Deleted only " << sum/((double)X7S_NBYTES_GIB) << "GiB"
					<< " of " << sum/((double)X7S_NBYTES_GIB) << "GiB that needed to be free";
			ret=false;
		}
	}
	return ret;
}

/**
*  @brief Delete the old video
*
*  The video are deleted from the database and physically
*
*  @todo This function should be run with its own thread.
*
*  @param time Look only video older than time
 * @param physical Also delete physical file on hard drive.
 * @param pathPrefix Set a path prefix to delete only from one video.
 *
 * @return @true if no errors occurs.
 */
bool VideoHandler::DeleteOldVideo(const QDateTime& time, bool physical, const QString& pathPrefix )
{
	X7S_FUNCNAME("VideoHandler::DeleteOldVideo()");

	QVector<VideoInFile> vecVideos;
	bool ret=true;

	QString prefixSQL;
	QString timeStr="'"+time.toString(Qt::ISODate)+"'";
	if(!pathPrefix.isEmpty()) prefixSQL= " AND WHERE path = '"+pathPrefix+"%'";

	vecVideos = m_db->GetVideos("WHERE tstart < "+timeStr+prefixSQL+" ORDER BY tstart ASC");

	QDir lastDir;

	for(int i=0;i<vecVideos.size();++i)
	{

		VideoInFile& video = vecVideos[i];
		const QFileInfo& fInfo =  vecVideos[i].getFileInfo();

		if(video.getTimeStart() < time) {

			avoInfo(funcName) << "Deleting" << fInfo.filePath() << fInfo.lastModified();
			ret = ret & m_db->Delete(video);

			if(physical && fInfo.exists())
			{
				//Then delete the file
				video.Delete();

				//Check when the directory is not used anymore empty dir.
				if(lastDir.exists() && lastDir!=fInfo.dir())
				{
					//Obtain the list of older
					QStringList list = lastDir.entryList(QDir::NoDotAndDotDot);
					if(list.isEmpty()) {
						lastDir.rmdir(lastDir.dirName());
						avoInfo(funcName) << "Delete Directory" << lastDir.dirName();
						QDir upDir = lastDir;

						//Two levels is the default creation, so i think it's OK.
						if(upDir.cdUp() && upDir.entryList(QDir::NoDotAndDotDot).isEmpty())
						{
							upDir.rmdir(upDir.dirName());
							avoInfo(funcName) << "Delete Directory" << upDir.dirName();
						}
					}
					else {
						avoDebug(funcName) << list;
					}


					lastDir=fInfo.dir();


				}
			}
		}
	}
	return ret;
}

/**
* @brief  Obtain a list of all the video files found in the path (and its recursive child)
*
* @param vecVideos A vector passed by reference to write inside the different videoFile
* @param dir The directory where we start the search.
* @param recursive In we look into the child.
* @return @true if some videos where find, @false otherwise
*/
bool VideoHandler::FindVideos(QVector<VideoInFile>& vecVideos, const QDir &dir, bool recursive)
{
	//X7S_FUNCNAME("VideoHandler::FindVideos()");
	bool has_err = false;

	if (dir.exists())
	{
		QDir::Filters filters;
		if(recursive) filters = (QDir::Dirs | QDir::Files);
		else filters = QDir::Files;

		QFileInfoList entries = dir.entryInfoList(QDir::NoDotAndDotDot | filters);
		int count = entries.size();
		for (int idx = 0; idx < count; idx++)
		{
			QFileInfo entryInfo = entries[idx];
			if (entryInfo.isDir())
			{
				has_err = FindVideos(vecVideos,QDir(entryInfo.absoluteFilePath()),recursive);
			}
			else
			{
				if(entryInfo.suffix()==".mjpeg")
				{
					vecVideos.push_back(VideoInFile(entryInfo.absoluteFilePath()));
				}
			}
		}
	}
	return(has_err);
}


