#include "VideoInFile.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Log.hpp"
#include "QtBind.hpp"
#include <QtDebug>
#include <x7snet.h>
#include <cv.h>
#include <highgui.h>




//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
VideoInFile::VideoInFile()
:VideoFile(MJPEG)
 {
	Init();
 }


VideoInFile::VideoInFile(const QString& FPath, const Camera* cam)
:VideoFile(MJPEG)
 {
	Init();
	m_fileInfo=QFileInfo(FPath);
	m_isEmpty=FPath.isEmpty();
	m_cam=cam;
 }


VideoInFile::VideoInFile(int cam_id, const QString& FPath,const QDateTime& tStart, const QDateTime &tLast, int video_id, qint64 size, bool isFull, bool isLocked)
:VideoFile(MJPEG)
 {
	Init();
	m_dbID= video_id;
	m_dbSize=size;
	m_isEnded=isFull;
	m_isLocked=isLocked;
	m_fileInfo=QFileInfo(FPath);
	m_camID=cam_id;
	m_tStart=tStart;
	m_tLast=tLast;
	m_isEmpty=FPath.isEmpty();
 }




VideoInFile::~VideoInFile()
{
	avoDump("VideoInFile::~VideoInFile()");
}


void VideoInFile::Init()
{
	m_camID=-1;
	m_dbID= -1;
	m_dbSize=-1;
	m_isEnded=true;
	m_isLocked=false;


	frameData = NULL;
	pHeader = NULL;
	pTrajGen = NULL;
	pAlrMan = NULL;

	mjpegFrame.pAlarm=NULL;
	mjpegFrame.pBlobs=NULL;
	mjpegFrame.pJpeg=NULL;

	imCvFrame=NULL;
	imCvMData=NULL;
}
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------


bool VideoInFile::Open(const QString& FPath)
{
	m_fileInfo=QFileInfo(FPath);
	m_isEmpty=false;
	return Open();
}


bool VideoInFile::Open()
{
	X7S_FUNCNAME("VideoInFile::Open()");

	if(!isValid())
	{
		avoError(funcName) << "Could not find the video file" << m_fileInfo.filePath();
		return false;
	}
	AVO_CHECK_WARN(funcName,!isOpened(),false,"First Close() previous video file");



	if(frameData==NULL) frameData = x7sCreateByteArray(X7S_CV_MJPEG_MAXFRAMESIZE);
	if(pHeader==NULL) pHeader = x7sCreateByteArray(X7S_VS_ALRMAN_DATA_SIZE);
	imCvFrame=NULL;
	imCvMData=NULL;
	frameData->type=X7S_CV_MJPEG_FRAMEDATA_TYPE;

	//Then open the new one.
	m_file = x7sOpenMJPEG(m_fileInfo.filePath().toStdString().c_str(),pHeader,1);

	if(m_file==NULL)
	{
		avoError("VideoInFile::Open()") << "Error while opening" << m_fileInfo.filePath().toStdString().c_str();
		return false;
	}

	avoDump(funcName) << "End" << m_fileInfo.filePath() << "File:" << m_file <<", fData:"<< frameData << ",pHeader:"<<pHeader ;
	bool ret = this->ExtractHeader();

	return ret;
}



bool VideoInFile::Close()
{
	avoDebug("VideoInFile::Close()")<< "imCV:" << imCvFrame << imCvMData << "; imQt:" << &imQtFrame << &imQtMData;

	imQtFrame = QImage();
	imQtMData = QImage();
	cvReleaseImage(&imCvMData);
	cvReleaseImage(&imCvFrame);
	x7sReleaseByteArray(&frameData);
	x7sReleaseByteArray(&pHeader);
	x7sReleaseByteArray(&mjpegFrame.pAlarm);
	x7sReleaseByteArray(&mjpegFrame.pBlobs);
	x7sReleaseByteArray(&mjpegFrame.pJpeg);

	X7S_DELETE_PTR(pTrajGen);
	X7S_DELETE_PTR(pAlrMan);



	return VideoFile::Close();
}

bool VideoInFile::Rewind()
{

	//Close and Load.
	if(m_file) {
		X7S_DELETE_PTR(pTrajGen);
		X7S_DELETE_PTR(pAlrMan);
		rewind(m_file);
		return this->ExtractHeader();
	}
	return false;
}

bool VideoInFile::Delete(bool force)
{
	bool ret=false;
	if(force) if(isOpened()) Close();

	if(isClosed()) {
		if(!isLocked() || force)
		{
			ret=QFile::remove(m_fileInfo.absoluteFilePath());
			ret=true;
			if(ret) this->ResetVariable();		//Set this file file to return true while calling isNull().
			else avoWarning("VideoInFile::Delete()") << "Can not delete filename";
		}
	}
	return ret;
}

bool VideoInFile::JumpToAlarm(int jump)
{
	AVO_CHECK_WARN("VideoInFile::JumpToAlarm()",
			(jump==1),false,"Not implemented with jump=%d",jump);

	while(ExtractFrame(1))
	{
		if(pAlrMan->GetAlarmEvents().size()!=0) {
			AVO_PRINT_INFO("VideoInFile::GoToAlarm()","Find alarm event at frame %d",mjpegFrame.id);
			return true;
		}
	}
	return false;
}

/**
* @brief Jump to the frame given by time
*
* @note As there is various frame in the same seconds we stop at the first one.
*
*
* @param time
* @param stayRF Stay at previous reference frame (When we jump to a time we look to the nearest reference frame).
* @return
*/
bool VideoInFile::JumpToTime(const QDateTime& time, bool stayRF)
{
	X7S_FUNCNAME("VideoInFile::JumpToTime()");


	int ret, mswait;
	if(m_tStart<time && time < m_tEnd)
	{
		time_t t=X7S_MAX(time.toTime_t()-5,m_tStart.toTime_t());	// Take the time 5 seconds before (reference frame synchro).
		ret = x7sFindTimeMJPEG(frameData,m_file,t);

		avoDebug(funcName) << "ret = "<< ret << ": act=" << t << " in videos = ["<< getTimeStart().toTime_t() << "," << getTimeEnd().toTime_t() << "]";

		if(ret==X7S_RET_OK)
		{
			ret = x7sFindTagMJPEG(frameData,m_file,X7S_CV_MJPEG_FRAMETAG_REFFR);
			if(ret==X7S_RET_OK) {
				mswait = GrabFrame(0);
				bool loop=!stayRF;
				t=time.toTime_t();
				while(mswait > 0 && loop)
				{
					mswait = GrabFrame(1);
					if(mjpegFrame.time>=t)
						loop=false;
				}
				return mswait;

			}
		}
	}
	else
	{
		if(time==m_tStart)
		{
			this->Rewind();
			return GrabFrame(1);
		}
		else avoWarning(funcName) << "time" << time << "doesn't exist for this videos";
	}
	return false;
}

/**
* @brief Grabs the frame from the file.
* @param jump Grab the next "jump" frame from the previous one (by default=1).
* @return The time to wait in milli-seconds to read the file at correct FPS. If an errors occur the time to wait is negative. If the wait is zero,
* this mean that it's the end of the file (jump>0) or at the beginning (jump<0).
*/
int VideoInFile::GrabFrame(int jump)
{
	X7S_FUNCNAME("VideoInFile::GrabFrame()");
	AVO_CHECK_WARN(funcName,isOpened(),-1,"Not opened");

	bool extracted = ExtractFrame(jump);
	if(extracted==false)
	{
		AVO_PRINT_INFO(funcName,"Frame can not be extracted (EOF or BOF)");
		return 0;	//No more frame to read.
	}

	IplImage jpeg=x7sByteArrayToIplImage(mjpegFrame.pJpeg);
	int ret=x7sCvtColorRealloc(&jpeg,&imCvFrame,X7S_CV_JPEG2BGRA);
	if(ret==X7S_CV_RET_ERR)
	{
		AVO_PRINT_ERROR(funcName, "Could not decode image %d",mjpegFrame.id);
		return -1;
	}
	imQtFrame = QtBind::toQImage(imCvFrame);

	//Create and/or fill the MData image.
	if(imCvMData==NULL || X7S_ARE_IMSIZEEQ(imCvFrame,imCvMData)==false) {
		if(imCvMData) cvReleaseImage(&imCvMData);
		imCvMData = cvCreateImage(cvGetSize(imCvFrame),IPL_DEPTH_8U,4);
		imQtMData = QtBind::toQImage(imCvMData);
		cvSet(imCvMData,cvScalarAll(0));
		avoDebug("VideoInFile::GrabFrame()") << "Create MData image" << &imQtMData << "(imQTFrame" << &imQtFrame << ")";

	}

	if(imCvMData)
	{
		cvSet(imCvMData,cvScalarAll(0));
		pTrajGen->Draw(imCvMData);
		pAlrMan->Draw(imCvMData);
	}

	return  x7sGetFpsMJPEG(&mjpegFrame,&mjpegFPS);
}

bool VideoInFile::EmphasisAlarm(const AVOAlrEvt& alrEvt)
{
	X7S_FUNCNAME("VideoInFile::EmphasisAlarm()");
	AVO_CHECK_WARN(funcName,(imCvMData && pAlrMan),false,"NULL pointers");

	if((alrEvt.t-1)==mjpegFrame.time || alrEvt.t == mjpegFrame.time)
	{
		X7sVSAlrDetector *alrDtr = pAlrMan->GetByID(alrEvt.alarm_ID);
		if(alrDtr) {
			alrDtr->Draw(imCvMData,-1,2);
			return true;
		}
	}
	return false;
}


qint64 VideoInFile::getSize(bool force)
{
	if(force)
	{
		qint64 size=-1;
		if(m_fileInfo.exists()) size=m_fileInfo.size();
		return size;
	}
	else  return m_dbSize;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

/**
 * @brief Extract the MJPEG data frame into a mjpegFrame.
 * @param jump
 * @return
 */
bool VideoInFile::ExtractFrame(int jump)
{
	X7S_FUNCNAME("VideoInFile::ExtractFrame()");

	AVO_CHECK_WARN(funcName,m_file,"m_file is NULL");

	int ret;
	if(jump!=0)
	{
		search_frame=jump-1;
		if(jump>0)
		{
			if(feof(m_file)) return false;
		}
		else {
			if(ftell(m_file)==0) return false;
		}
		//Read the MJPEG frame
		ret = x7sReadFileMJPEG(frameData,m_file, search_frame);
		if(ret != X7S_RET_OK) return false;
	}


	//Extract the frame in a structure.
	ret = x7sExtractMJPEG(&mjpegFrame, frameData);
	AVO_CHECK_WARN(funcName,ret==X7S_RET_OK,false);
	m_tActual.setTime_t(mjpegFrame.time);

	//Set alarm and trajectory data.
	if(mjpegFrame.pBlobs) pTrajGen->SetData(mjpegFrame.pBlobs,mjpegFrame.id);
	if(mjpegFrame.pAlarm) pAlrMan->SetData(mjpegFrame.pAlarm,mjpegFrame.id);


	return true;
}

bool VideoInFile::ExtractHeader()
{
	pAlrMan = new X7sVSAlrManager(NULL,pHeader);
	pTrajGen = new X7sBlobTrajGenerator();

	if(mjpegFrame.pAlarm==NULL) mjpegFrame.pAlarm = x7sCreateByteArray(X7S_VS_ALRMAN_DATA_SIZE);
	if(mjpegFrame.pBlobs==NULL) mjpegFrame.pBlobs = x7sCreateByteArray(X7S_VS_BLOBTRAJ_DATA_SIZE);
	if(mjpegFrame.pJpeg==NULL) mjpegFrame.pJpeg = x7sCreateByteArray(X7S_NET_RTP_MSIZEIB_JPG);

	//Init the structure for the first fps computation.
	mjpegFPS.first_t0=true;
	mjpegFPS.t0=0;
	mjpegFPS.fps=24.f;
	mjpegFPS.t0_id=-1;

	search_frame = 0;

	//Read the first frame
	x7sReadFileMJPEG(frameData,m_file,0);
	x7sExtractMJPEG(&mjpegFrame, frameData);
	m_tStart.setTime_t(mjpegFrame.time);

	//Go to the end
	fseek(m_file,0,SEEK_END);

	//Read the last frame
	x7sReadFileMJPEG(frameData,m_file,-1);
	x7sExtractMJPEG(&mjpegFrame, frameData);
	m_tEnd.setTime_t(mjpegFrame.time);

	//Go back to start
	fseek(m_file,0,SEEK_SET);
	m_tActual=m_tStart;

	return true;
}






//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------
