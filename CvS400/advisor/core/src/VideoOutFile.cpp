#include "VideoOutFile.hpp"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------

#include "Camera.hpp"
#include "Board.hpp"
#include "Log.hpp"




//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
VideoOutFile::VideoOutFile(const Camera *cam)
:VideoFile(MJPEG)
{
	m_count=0;
	m_wasAutoClosed=false;
	m_maxDuration=AVO_VIDEO_OFILE_MAXDUR; //Video is at maximum 10 minutes.
	m_maxBuffer=AVO_VIDEO_OFILE_MAXBUFF;
	m_buffer.clear();

	if(cam) this->SetCamera(cam);
}




VideoOutFile::~VideoOutFile()
{

}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------
bool VideoOutFile::SetCamera(const Camera *cam)
{
	if(cam)
	{
		if(isValid()==false) {
			m_cam=cam;
			m_isEmpty=false;
			return true;
		}
		else
		{
			if(isClosed())
			{
				m_cam=cam;
				this->ResetVariable();
				return true;
			}
		}
	}
	return false;
}



bool VideoOutFile::Open(const Camera *cam)
{
	if(this->isOpened())
	{
		qWarning() << "VideoOutFile::Open(): File is not close can not perform another open";
		return false;
	}

	AVO_CHECK_WARN("VideoOutFile::Open()",cam,false,"cam==NULL");


	QString filePath = VideoOutFile::GenerateFilePath(cam,"%_D/Cam%U/%D_%T.mjpeg");
	if(filePath.isNull() || filePath.isEmpty()) return false;

	m_wasAutoClosed=false;

	X7sVSAlrManager *alrMan = cam->GetAlarmManager();
	if(alrMan) {
		m_file = x7sOpenMJPEG(filePath.toAscii().data(),(X7sByteArray*)alrMan->GetDefinition(),0);
		if(m_file)
		{
			m_cam=cam;
			m_lastSize=-1;
			m_fileInfo=QFileInfo(filePath);
			m_tStart=cam->GetTime();
			m_tLast=cam->GetTime();

			AVO_PRINT_INFO("VideoOutFile::Open()","Create MJPEG %s",filePath.toAscii().data());

			return true;
		}
	}

	return false;
}

bool VideoOutFile::Close()
{
	VideoFile::Close();
	return this->EraseBuffer();
}


/**
* @brief Automatically close when the file reach its maximum size, maximum duration or because
* it doesn't write anymore frame.
* @note When the file is AutoClose() it doesn't delete the Buffer of frame.
*
* @return @true if the file was automatically close.
*/
bool VideoOutFile::AutoClose()
{
	if(isOpened())
	{
		const char *desc;
		if(m_tStart.secsTo(m_tLast) >= m_maxDuration)
		{
			m_wasAutoClosed=true;
			desc="Reach maximum duration";
		}
		if(m_count <=0)
		{
			m_wasAutoClosed=true;
			desc="The file doesn't flush anymore";
		}
		if(m_file==NULL)
		{
			m_wasAutoClosed=true;
			desc="Reach maximum size";
		}
		if(m_wasAutoClosed)
		{
			m_count=0;
			AVO_PRINT_INFO("VideoOutFile::AutoClose()","video file %d (%s)",m_cam->GetSpecialID(),desc);
			return VideoFile::Close();
		}
	}
	return false;
}

/**
* @brief Write the frame in the video file
*
* The video file must be opened.
*
* @param frame The data of the frame.
* @param time The time of the frame.
*/
bool VideoOutFile::WriteFrame(const X7sByteArray *frame, const QDateTime& time)
{
	int code;
	bool ret=false;
	if(isOpened())
	{
		code=x7sWriteMJPEG(frame,m_file);
		if(code==X7S_RET_OK)
		{
			if(time.isValid()) m_tLast=time;
			if(m_count>0) m_count--;
			m_lastSize=getSize(false);
			ret=true;
		}
		else 	AutoClose();
	}
	return ret;
}


/**
* @brief Write the frame in the buffer
*
* @note This method calls automatically to Open() and AutoClose().
* Therefore, you should check wasAutoClosed() method when this method return @false.

* @param frame
* @param flush
* @return
*/
bool VideoOutFile::BufferizeFrame(const X7sByteArray *frame,bool flush)
{
	bool ret=false;
	if(isValid()==false) return ret;

	//Set the maximum count while we are flushing
	if(flush) m_count=m_maxBuffer;

	//We need to write in the file.
	if(m_count > 0)
	{
		//Open the file if it is closed
		if(isClosed()) {
			ret = this->Open(m_cam);
			if(ret==false) return false;
		}


		//Then flush the buffer
		if(FlushBuffer())
		{
			//And write the last frame (do not copy it)
			return WriteFrame(frame,m_cam->GetTime());
		}
	}
	else //Otherwise, we copy the frame in the memory and put it in a buffer list.
	{
		VideoFrame frameLast;

		frameLast.data = x7sCreateByteArray(frame->length);
		if(x7sCopyByteArray(frame,frameLast.data)==X7S_RET_OK)
		{

			//Delete the oldest frame from the memory
			while(m_buffer.size() >= m_maxBuffer)
			{
				VideoFrame frameOldest = m_buffer.takeFirst();
				x7sReleaseByteArray(&frameOldest.data);
			}

			frameLast.time=m_cam->GetTime();

			//And put the last new one.
			m_buffer.push_back(frameLast);

			return true;
		}
	}
	return false;
}

bool VideoOutFile::FlushBuffer()
{
	bool ret=false;
	int code;

	if(isOpened())
	{
		ret=true;
		while(m_buffer.size() > 0)
		{
			//Take the oldest frame
			VideoFrame frame =m_buffer.first();
			//Write it in JPEG
			code =x7sWriteMJPEG(frame.data,m_file);
			//Delete it from the memory.
			x7sReleaseByteArray(&(frame.data));
			//Check if an error occurs
			if(code !=X7S_RET_OK) {
				AVO_PRINT_WARN("VideoOutFile::FlushBuffer()","Could not flush frame");
				if(m_file) m_wasAutoClosed=true;
				return false;
			}
			else 	{
				m_tLast=frame.time; //Write the time when the last frame was
				m_buffer.removeFirst();
			}
		}
	}
	return ret;
}



bool VideoOutFile::EraseBuffer()
{
	VideoFrame frame;
	while (!m_buffer.isEmpty())
	{
		frame =m_buffer.takeFirst();
		x7sReleaseByteArray(&(frame.data));
	}
	m_buffer.clear();
	return true;
}


qint64 VideoOutFile::getSize(bool force)
{
	qint64 ret=-1;
	if(force && isClosed()) {
		this->Open(m_cam);
		ret= VideoFile::getSize();
		this->Close();
	}
	else ret = VideoFile::getSize();
	return ret;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

/**
* @brief Generate the path for teh video file.
*
* This function replace the pattern with the parameter of the camera.
*
* 		- %U: Unique BoardCamera ID {101,102,103,104,201,202,...}.
* 		- %C: Camera ID [0-1].
* 		- %B: Board ID [0-X].
* 		- %D: Date in the compact format yyyyMMdd
* 		- %_D: Date in expend format: yyyy_MM_dd
* 		- %T: Time in the compact format HHmmSS
* 		- %_T: Time in the expend format: HH_mm_SS
*
* @param cam			Camera use to generate filePath
* @param pattern	pattern use for generation.
* @return The Absolute file path
*/
QString VideoOutFile::GenerateFilePath(const Camera *cam,const QString &pattern)
{

	Board *board = cam->GetBoard();

	QDateTime time=cam->GetTime();
	QString tmp = pattern;
	tmp = tmp.replace("%C",QString("%1").arg(cam->GetID()));
	tmp = tmp.replace("%B",QString("%1").arg(board->GetID()));
	tmp = tmp.replace("%U",QString("%1").arg(cam->GetSpecialID()));
	tmp = tmp.replace("%_D",time.toString("yyyy_MM_dd"));
	tmp = tmp.replace("%D",time.toString("yyyyMMdd"));
	tmp = tmp.replace("%_T",time.toString("hh_mm_ss"));
	tmp = tmp.replace("%T",time.toString("hhmmss"));

	//Generate the file info and file path
	QString filePath =  cam->GetVideoRootDir()+"/"+tmp;
	QFileInfo fileInfo = QFileInfo(filePath);
	QDir fileDir = fileInfo.dir();

	//Check if directory exist or can be created.
	if(fileDir.exists()==false)
	{
		if(fileDir.mkpath(fileDir.absolutePath())==false)
		{
			AVO_PRINT_WARN("VideoOutFile::GenerateFilePath()","Could not create directory %s",filePath.toAscii().data());
			return QString();
		}
	}

	//Check if filename is valid.
	if(fileInfo.baseName().isEmpty() || fileInfo.suffix().isEmpty())
	{
		AVO_PRINT_WARN("VideoOutFile::GenerateFilePath()","Filename is not valid %s",filePath.toAscii().data());
		return QString();
	}

	return fileInfo.absoluteFilePath();
}







