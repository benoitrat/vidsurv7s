#include "Board.hpp"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Camera.hpp"
#include "Devices.hpp"
#include "Log.hpp"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------


/**
* @brief Create a board linked to the given devices.
*
* If the same ID exist in the list the board is not added in the list and therefore can't be destroy
* with the destructor of Devices.
*
* @param parent A pointer on the board manager also called Devices.
*/
Board::Board(Devices *parent)
:X7sXmlObject((X7sXmlObject*)parent,AVO_TAGNAME_BOARD), dev(parent)
 {
	Init();
 }

/**
* @brief Init the parameters and force the construction of 4 cameras.
*/
void Board::Init()
{
	actual_camid=CAMID_1;

	m_params.Insert(X7sParam("enable",X7S_XML_ANYID,true,X7STYPETAG_BOOL));
	m_params.Insert(X7sParam("ip",X7S_XML_ANYID,"192.168.7.254"));
	m_params.Insert(X7sParam("port",X7S_XML_ANYID,18000,X7STYPETAG_U32,1,65535));
	m_params.Insert(X7sParam("rtp",S7P_PRM_BRD_PORT_RTP,50000,X7STYPETAG_U32,1,65535));
	//m_params.Insert(X7sParam("modes",S7P_PRM_BRD_MODES,0,X7STYPETAG_U32,0,10));

	for(int c=0;c<AVO_BOARD_NUMCAM;c++)
	{
		Camera *cam = new Camera(this,c);
		vcam.push_back(cam);
	}
	if(dev) dev->AddBoard(this);	//Add the this board to the list of board in Devices.

	m_enable=true;
	m_isRecording=false;
	m_wasRecording=false;
	m_fwVersion=0;
}

/**
* @brief Default destructor.
* @note Camera are deleted in the X7sXmlObject::~X7sXmlObject().
*/
Board::~Board()
{
	AVO_PRINT_DEBUG("Board::~Board()","ID=%d",GetSpecialID());
	vcam.clear();
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------



/**
* @brief Connect this object to the remote board.
* @return @true if the connection has been established, @false otherwise.
*/
bool Board::Connect()
{
	AVO_CHECK_WARN("Board::Connect()",m_enable,false,"board #%d is not enable (IP=%s)",this->GetSpecialID(),m_params["ip"]->toString().c_str());

	uint32_t ip_remote=X7sNet_GetIP(m_params["ip"]->toString().c_str());
	uint16_t port_remote=m_params["port"]->toIntValue();
	uint16_t port_local=m_params["rtp"]->toIntValue();

	QTime time=QTime::currentTime();
	int ret = nets.Connect(ip_remote,port_remote,port_local);
	if(ret==X7S_NET_RET_OK) {
		AVO_PRINT_INFO("Board::Connect()","ID=%d OK (%s)",this->GetSpecialID(),m_params["ip"]->toString().c_str());
		return true;
	}
	else 
	{
		AVO_PRINT_ERROR("Board::Connect()","ID=%d Error: Timeout %dms (IP=%s)",this->GetSpecialID(),time.elapsed(),m_params["ip"]->toString().c_str());
		return false;
	}
}

/**
* @brief Setup the parameters of the board
*
* After this function we must perform a STOP/START to configure correctly the new random SSCRID
*
* @return
*/
bool Board::Setup()
{
	uint32_t val;
	int ret;
	int id=this->GetID()+1;

	uint32_t randnum = (uint32_t)qrand();
	randnum = (randnum << 16) + ((id & 0xFF) << 8);

	AVO_PRINT_INFO("Board::Setup()","ID=%d (SSRCID=%X)",this->GetSpecialID(),randnum);

	ret = nets.SetParam(S7P_PRM_BRD_SSRCID,randnum);
	X7sParam *pParam=NULL;
	while((pParam=m_params.GetNextParam(pParam))!=NULL) {
		if(pParam->GetID()!=X7S_XML_ANYID && pParam->GetID()!=X7S_XML_NULLID)
		{
			val=(uint32_t)pParam->toIntValue();
			nets.SetParam(pParam->GetID(),val,CAMID_ALL);
		}
	}

	nets.GetParam((int)S7P_PRM_RO_VFW,m_fwVersion);

	return (ret==X7S_NET_RET_OK) && this->Update();
}


/**
* @brief Toggle the recording LED on/off of the board.
*
* This function must be called by a thread to avoid waiting
* for the reply.
*/
bool Board::ToggleLED()
{
	uint32_t on=(uint32_t)m_isRecording;
	int ret = nets.SetParam(S7P_PRM_CAM_SWITCH_LED,on,CAMID_1);	//We put CAMID_1 to put a valid value but we don't care :)
	return (ret==X7S_NET_RET_OK);
}


QString Board::GetStats() const {
	QString txt;
	Camera *cam;
	for(size_t i=0;i<vcam.size();++i)
	{
		cam = vcam[i];
		if(cam && cam->IsEnable()) txt+=cam->GetStats()+"; ";
	}
	return txt;
}


/**
* @brief Update the parameter of each of the camera.
* @return
*/
bool Board::Update()
{
	AVO_PRINT_INFO("Board::Update()","ID=%d",this->GetSpecialID());

	//	uint32_t val;
	//	int ret, is_enable=false;
	//	const X7sParam *pParam;

	//Send the parameters of the board...


	for(int c=0;c<AVO_BOARD_NUMCAM;c++)
	{
		Camera *cam = vcam[c];
		cam->Update();
	}
	return true;
}

/**
* @brief Sent start command.
*
* If this command is correctly executed, the board start sending images on the RTP port.
*
* @return @true if the board acknowledge the start command.
*/

/**
* @brief Start sending the data from the board.
* @return @true if the command has been correctly received at the board.
*/
bool Board::Start()
{
	int retcode;
	X7S_FUNCNAME("Board::Start()");

	AVO_PRINT_INFO(funcName,"ID=%d",this->GetSpecialID());
	retcode=nets.Start();
	if(retcode==X7S_NET_RET_OK) return true;
	else
	{
		AVO_PRINT_WARN(funcName,"Start failed! Error:#%d",X7sNetSession::GetErrorMsg(retcode),retcode);
		return false;
	}
}

/**
* @brief Sent stop command
* @return @true if the board acknowledge the start command.
*/

bool Board::Stop()
{
	return (nets.Stop()==X7S_RET_OK);
}

/**
* @brief Close the connection with the netsession
* @note You must call Connect to restablish it.
*/
bool Board::Close()
{
	AVO_PRINT_DEBUG("Board::Close()","ID=%d",GetSpecialID());
	return (nets.Close()==X7S_RET_OK);
}

/**
* @brief Process the a frame
*
* This function must be call by a thread, It will first check if a new frame has arrived,
* then we lock external access to the data with a mutex so that we can capture correctly the frame
* and then process it.
*
* @return @true, if a new and valid frame arrived.
*/


int Board::ProcessFrame()
{
	int ret=X7S_RET_ERR;
	actual_camid=abs((actual_camid+1)%AVO_BOARD_NUMCAM);

	Camera *cam=vcam[actual_camid];

	if(nets.IsNewFrame(actual_camid))
	{
		if(cam->mutex().tryLock(5)) { //Wait maximum 5 ms
			X7sNetFrame *netFrame = nets.CaptureMetaFrame(actual_camid);
			if(cam->ProcessNetFrame(netFrame))
			{
				frame_count++;
				ret=actual_camid;
			}
			cam->mutex().unlock();	//Unlock the mutex
		}
		else {
			AVO_PRINT_DEBUG("Board::ProcessFrame()","mutex is lock (Cam %d)",this->GetSpecialID(actual_camid));
		}
	}
	else {
		//AVO_PRINT_DUMP("Board::ProcessFrame()","IsNewFrame()==false (Cam %d)",this->GetSpecialID(actual_camid));
	}
	return ret;
}

/**
* @brief Check if the board is alive
*
* @param nof_tries
* if N==0 it check the previous state;
* if N<0, it send at most N pings until the board respond;
* if N>0, it returns @true if the previous state was connected, otherwise it performs N pings.
*
* @see X7sNetSession::IsAlive().
*/
bool Board::IsAlive(int nof_tries)
{
	AVO_CHECK_WARN("Board::IsAlive()",m_enable,false,"board #%d is not enable (IP=%s)",this->GetSpecialID(),m_params["ip"]->toString().c_str());
	return nets.IsAlive(nof_tries);
}

/**
* @brief Get the camera given its index.
*
* @note If the index is not valid it returns @NULL.
*/
Camera* Board::GetCamera(int index)
{
	if(X7S_CHECK_RANGE(index,0,AVO_BOARD_NUMCAM))
	{
		return vcam[index];
	}
	return NULL;
}

/**
* @brief Get SpecialID of the board (or Camera)
* @param cam_index if negative, or not a valid camera index it returns the special ID of the board,
*  otherwise it returns the special ID of the camera
* @return The special ID (i.e., 100 (boards[0]), 103 (boards[0].cams[2]), ... )
*/
int Board::GetSpecialID(int cam_index)
{
	Camera* cam= this->GetCamera(cam_index);
	if(cam) return cam->GetSpecialID();
	else return 100*(this->GetID()+1);
}

/**
* @brief Tell if one of the camera is recording the images.
*/
bool Board::IsRecording(bool *valueChanged)
{
	m_isRecording=false;
	for(int c=0;c<AVO_BOARD_NUMCAM;c++)
	{
		if(vcam[c]->IsRecording())
		{
			m_isRecording=true;
			break;
		}
	}
	//If valueChange is set than update the waRecording value.
	if(valueChanged)
	{
		*valueChanged = (m_isRecording != m_wasRecording);
		m_wasRecording=m_isRecording;
	}
	return m_isRecording;
}

QString Board::GetFWVersion() const {
	if(m_fwVersion)
	{
		uint8_t array[4];
		X7sNet_SetIP(m_fwVersion,array);	//Convert 32-bits to array
		return QString("HW: %1.%2 SW: %3.%4").arg(array[0]).arg(array[1]).arg(array[2]).arg(array[3]);
	}
	else return "";
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

/**
* @brief Apply parameter changed to the object.
*
* Apply the enable state.
*/

bool Board::Apply(int depth)
{
	bool ret = X7sXmlObject::Apply(depth);
	ret = ret & (!m_params["enable"]->IsNull());
	m_enable=m_params["enable"]->IsOk();
	return ret;
}
