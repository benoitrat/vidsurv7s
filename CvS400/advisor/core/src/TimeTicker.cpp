#include "TimeTicker.hpp"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include <QTimer>
#include "Log.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------


/**
 * Construct the TimeTicker object
 *
 * This object has an internal timer that call the tick method each seconds.
 * You can call yourself the tick method if you want more precision.
 *
 * @param parent The given parents used to destroy this object.
 * @param day when @true send a signal when a day is completed.
 * @param hour when @true send a signal when an hour is completed.
 * @param quater when @true send a signal when 15mn are completed.
 * @param minute when @true send asignal when 1mn is completed.
  */
TimeTicker::TimeTicker(QObject *parent,bool day,bool hour, bool quater, bool minute)
:tickDay(day), tickHour(hour), tickQuater(quater), tickMinute(minute)
 {
	this->reset();
	avoInfo("TimeTicker::TimeTicker()") << lastDay << lastHour << lastQuater << lastMinute;

	//Add a timer to call tick method each seconds.
	QTimer *timer = new QTimer(this);
	timer->start(1000);
	connect(timer,SIGNAL(timeout()),this,SLOT(tick()));
 }

/**
 * @brief Destroy the timer.
 * @return
 */
TimeTicker::~TimeTicker()
{
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------
void TimeTicker::reset()
{
	dtStart = QDateTime::currentDateTime();
	time_t t = dtStart.toTime_t();

	//Setup begin of the time
	lastMinute= QDateTime::fromTime_t(t - t%60);
	lastQuater= QDateTime::fromTime_t(t - t%900);
	lastHour=	QDateTime::fromTime_t(t - t%3600);

	//Setup the begin of the day
	lastDay = QDateTime(dtStart.date());
}

/**
 * @brief Tick the time and see if we have a completed minute, quater, hour or day....
 */
void TimeTicker::tick()
{
	//Obtain the actual datetime rounded at the seconds.
	QDateTime dtActual =QDateTime::currentDateTime();


	//Check overflow
	if(dtActual<lastMinute.addSecs(-120)) {
				avoDebug("TimeTicker::tick()")
				<< "actu=" << dtActual
				<< "last=" << lastMinute;
		this->reset();
		QDateTime dtActual =QDateTime::currentDateTime();
	}

	if(lastMinute.secsTo(dtActual)>=60)	// 1 minutes
	{
		//Round time to the last minute
		time_t t = dtActual.toTime_t();
		time_t t_rounded = (t-(t%60));

		//Setup time correctly
		dtActual = QDateTime::fromTime_t(t_rounded);	//Rounded to the minutes.
		
		lastMinute=dtActual;
		if(tickMinute) emit completedMinute(dtActual);

		if(lastQuater.secsTo(dtActual)>=900)	//15 minutes
		{
			lastQuater=dtActual;
			if(tickQuater) emit completedQuater(dtActual);

			if(lastHour.secsTo(dtActual)>=3600)	//1 hour
			{
				lastHour=dtActual;
				if(tickHour) emit completedHour(dtActual);

				if(lastDay.daysTo(dtActual)>=1) // 1 day
				{
					lastDay=dtActual;
					if(tickDay) emit completedDay(dtActual);
				}
			}	// End hour
		} // End quater
	}	//End minute
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------
