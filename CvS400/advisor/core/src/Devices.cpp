#include "Devices.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------

#include "Camera.hpp"
#include "Board.hpp"
#include "Log.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------


/**
* @brief Add a board in the vector of devices and also add it as an XML child.
*
* If the same ID exist in the list the board is not added in the list and therefore can't be destroy
* with the destructor of Devices.
*
* @param parent A pointer to the parent X7sXmlObject (see @ref Root)
* @return @true if the board has been correctly added, @false if it's ID is already taken by another Board.
*
*/
Devices::Devices(X7sXmlObject *parent)
:X7sXmlObject(parent,AVO_TAGNAME_DEVICES,true)
{
	AVO_PRINT_DEBUG("Devices::Devices()");

	//Video file.
	m_params.Insert(X7sParam("video_path",X7S_XML_ANYID,".",X7STYPETAG_STRING));
	m_params.Insert(X7sParam("video_path_isgeneral",X7S_XML_ANYID,true,X7STYPETAG_BOOL));
	m_params.Insert(X7sParam("video_istowrite",X7S_XML_ANYID,false,X7STYPETAG_BOOL));
}

/**
* @brief This default destructor.
*/
Devices::~Devices()
{
	AVO_PRINT_DEBUG("Devices::~Devices()");
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

bool Devices::Start()
{
	bool ret=true;
	for(size_t i=0;i<m_vecBoard.size();i++)
	{
		if(m_vecBoard[i]) ret = ret && m_vecBoard[i]->Start();
	}
	return ret;
}

bool Devices::Stop()
{
	bool ret=true;
	for(size_t i=0;i<m_vecBoard.size();i++)
	{
		if(m_vecBoard[i]) ret = ret && m_vecBoard[i]->Stop();
	}
	return ret;
}

void Devices::CheckConnection()
{
	for(size_t i=0;i<m_vecBoard.size();i++)
	{
		if(m_vecBoard[i]) m_vecBoard[i]->IsAlive(-1);
	}
}

/**
* @brief Obtain a board given its ID.
* @param ID  the identifier of the board. Board::GetID().
* @return If found, a pointer on the board. Otherwise @NULL.
*/
Board* Devices::GetBoardByID(int ID) const
{
	for(size_t i=0;i<m_vecBoard.size();i++)
	{
		if(m_vecBoard[i] && m_vecBoard[i]->GetID()==ID) {
			return m_vecBoard[i];
		}
	}
	return NULL;
}

Camera* Devices::GetCameraByID(int ID) const
{
	ID--;
	int index_cam =  (ID%100);
	int ID_board=(ID-index_cam)/100-1;
	Board *board = this->GetBoardByID(ID_board);
	if(board) return board->GetCamera(index_cam);
	else return NULL;
}

/**
* @brief Delete board given its ID.
* @param ID the identificator of the board. Board::GetID().
* @return @true if the board has been found and deleted, @false otherwise.
*/
bool Devices::DelBoardByID(int ID)
{

	std::vector<Board*>::iterator it=m_vecBoard.begin();
	while(it != m_vecBoard.end())
	{
		if((*it) && (*it)->GetID()==ID) {
			delete (*it);
			(*it)=NULL;
			m_vecBoard.erase(it);
			return true;
		}
		++it;
	}
	return false;
}

/**
* @brief Obtain the Camera using the previous Camera obtained.
*
* For example you can use it like this:
*
* @code
* 	Camera *cam=NULL;
*	while((cam=GetNextCamera(cam)) != NULL) {
*		cam->Print();
*	}
* @endcode
*
* @param cam The actual camera (if it is NULL we will get the first camera)
* @return A pointer on the next camera or NULL if there is no more.
*/
Camera* Devices::GetNextCamera(Camera *cam)
{
	Board *b=NULL;
	if(cam==NULL) //Take the first camera.
	{
		if(m_vecBoard.size()>0) b=m_vecBoard[0];
		if(b) return b->GetCamera(0);
	}
	else	//Otherwise
	{
		if(cam->GetID()<(AVO_BOARD_NUMCAM-1))	//Get the next camera in the same board
		{
			b=cam->GetBoard();
			return b->GetCamera(cam->GetID()+1);
		}
		else {	//Or go to the next board and obtain first camera.
			b=cam->GetBoard();
			for(size_t i=1;i<m_vecBoard.size();i++) {
				if(b==m_vecBoard[i-1]) {
					b=m_vecBoard[i];
					if(b) return b->GetCamera(0);
				}
			}
		}
	}
	return NULL;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

/**
* @brief Delete all boards assigned to the Devices.
* This function is calling delete for each board pointer present inside
* the list of devices.
*/
void Devices::DeleteAll()
{
	for(size_t i=0;i<m_vecBoard.size();i++)
	{
		delete m_vecBoard[i];
	}
	m_vecBoard.clear();
}

/**
 * @brief Add a board to the devices list.
 * @note This method is automatically called, when a child is added.
 * @param board A new board in the devices list.
 * @return
 */
bool Devices::AddBoard(Board *board)
{
	if(board) {
		const X7sXmlObject* parent = board->GetParent();
		if(this==parent)
		{
				m_vecBoard.push_back(board);
				return true;
		}
		else if(parent==NULL)
		{
			AVO_PRINT_WARN("Devices::AddBoard()","The board (ID=%d) doesn't belong to any parent",board->GetID());
		}
		else
		{
			AVO_PRINT_WARN("Devices::AddBoard()","The board (ID=%d) already belongs to another parent",board->GetID());
		}
	}
	return false;
}

/**
* @brief Create the children while receiving the XML child node.
* @note This function might be overloaded by another one if we don't want to create Board object.
* @param child The node with the corresponding tag. It is always called one time with a NULL value.
* @return @true if a child as been correctly created.
*/
bool Devices::CreateChildFromXML(const TiXmlElement *child)
{
	if(IsFirstChild()) this->DeleteAll();

	if(X7S_XML_ISEQNODENAME(child,AVO_TAGNAME_BOARD))
	{
		Board *board = new Board(this);
		if(board->ReadFromXML(child)){
			return true;
		}
	}
	return false;
}
