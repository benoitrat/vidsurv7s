#include "CamEvt.hpp"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include <QTextStream>
#include "Camera.hpp"
#include "VideoFile.hpp"
#include "Log.hpp"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
CamEvt::CamEvt(Type type, const Camera *cam,const QDateTime& dtime, State state):
m_type(type), m_state(state), m_cam(cam), m_dtime(dtime)
{
	//Do nothing here
	if(m_dtime.isValid()==false)
	{
		AVO_PRINT_WARN("CamEvt::CamEvt()","m_dtime is not valid (Take the actual time)");
		m_dtime=QDateTime::currentDateTime();
	}
}


CamEvt::~CamEvt()
{
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

const char* CamEvt::getStateDesc() const
		{
	switch(m_state)
	{
	case Start: return "Start";
	case Update: return "Update";
	case Finish: return "Finish";
	case Default: return "Default";
	default: return "Undefined";
	}
		}

const char* CamEvt::getTypeDesc() const
		{
	switch(m_type)
	{
	case CamEvt::Activity: return "Activity";
	case CamEvt::Video: return "Video";
	case CamEvt::VSAlarm: return "VSAlarm";
	case CamEvt::Net: return "Net";
	case CamEvt::NewFrame: return "NewFrame";
	case CamEvt::Unknow: return "Unknown";
	default: return "Undefined";
	}
		}


QString CamEvt::toString() const
{
	QString string;
	QTextStream txtstr(&string);

	if(this->isValid())
	{
		txtstr << m_cam->GetName() << "@" << m_dtime.toString("HH:mm:ss") <<" > ";
		txtstr << getTypeDesc() << " ";
	}
	else txtstr << "NULL";

	return string;
}

QDebug operator<< (QDebug out, const CamEvt* camEvt)
{
	if(camEvt && camEvt->isValid())
	{
		out.nospace();
		out << camEvt->toString() << " ";
		out.nospace();
	}
	return out;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------


CamEvtVSAlarm::CamEvtVSAlarm(const Camera *cam,const QDateTime& dtime,const X7sVSAlarmEvt& alrEvt)
:CamEvt(VSAlarm,cam,dtime), vsAlrEvt(alrEvt)
 {
 }

QString CamEvtVSAlarm::toString() const {
	QString ret=CamEvt::toString();
	ret.sprintf("%s ID:%d from blob:%d (%d,%d)",
			ret.toStdString().c_str(),
			vsAlrEvt.alarm_ID, vsAlrEvt.blob_ID,
			vsAlrEvt.warn, vsAlrEvt.val);
	return ret;
}

/**
 * @brief This method is used for the conversion from the old system
 * @param alrEvt
 * @return
 * @deprecated
 */
CamEvtVSAlarm* CamEvtVSAlarm::fromAVOAlrEvt(const AVOAlrEvt& avoAlrEvt)
{
	CamEvtVSAlarm *ret=NULL;
	X7sVSAlarmEvt x7sAlrEvt;
	memcpy(&x7sAlrEvt,&avoAlrEvt,sizeof(x7sAlrEvt));
	ret= new CamEvtVSAlarm(avoAlrEvt.cam,QDateTime::fromTime_t(avoAlrEvt.t),x7sAlrEvt);
	return ret;
}



CamEvtActivity::CamEvtActivity( const Camera *cam,const QDateTime& dtime,State state)
:CamEvt(Activity,cam,dtime,state)
 {
 }

QString CamEvtActivity::toString() const {
	QString ret=CamEvt::toString();
	ret += "(" + QString(getStateDesc()) +")";
	return ret;
}

CamEvtVideo::CamEvtVideo( const Camera *cam,const QDateTime& dtime,const VideoFile* vidFile,State state)
:CamEvt(class_type,cam,dtime,state), m_vidFile(vidFile)
 {
 }


QString CamEvtVideo::toString() const {
	QString ret=CamEvt::toString();
	ret += "(" + QString(getStateDesc()) +")";
	return ret;
}
