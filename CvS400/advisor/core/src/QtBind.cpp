//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------

#include "QtBind.hpp"

/**
* @brief Convert the value of a parameters to QString.
*
* If the pParam is not valid it return an empty QString().
*/
QString	QtBind::toQString(const X7sParam *pParam)
{
	if(pParam)
	{
		return QString::fromStdString(pParam->toString());
	}
	return QString();
}

/**
* @brief Transform a BGRA color X7sParam to a QColor
*
* If an error occurs during the setup it returns a invalid QColor.
* (QColor::isValid() return @false)
*
*/
QColor 	QtBind::toQColor(const X7sParam *pParam)
{
	QColor color;
	if(pParam)
	{
		double tmp;
		//Obtain the point from the parameters.
		if(pParam->GetRealValue(0,&tmp)) color.setBlue((int)tmp);
		if(pParam->GetRealValue(1,&tmp)) color.setGreen((int)tmp);
		if(pParam->GetRealValue(2,&tmp)) color.setRed((int)tmp);
		if(pParam->GetRealValue(3,&tmp)) color.setAlpha((int)tmp);
	}
	return color;
}


/**
* @brief Create a QImage from an IplImage
*
* The memory is shared by the QImage, and must be deleted using the IplImage.
*
*  If an error occurs during the setup it returns a Null QLine.
* (QColor::isNull() return @true)
*
*/
QImage	QtBind::toQImage(IplImage *pImg,const QImage::Format& format)
{
	if(pImg) {
		return 	QImage((uchar*)pImg->imageData,pImg->width,pImg->height,pImg->widthStep,format);
	}
	else
	{
		return QImage();
	}
}


/**
* @brief Obtain a Qline from an X7sVSAlrDtrLine.
* @param pAlrDtr An alarm detector with type = @ref X7S_VS_ALR_TYPE_LINE.
* If an error occurs during the setup it returns a Null QLine.
* (QLine::isNull() return @true)
*/
QLine  	QtBind::toQLine(X7sVSAlrDetector *pAlrDtr)
{
	if(pAlrDtr && pAlrDtr->GetType()==X7S_VS_ALR_TYPE_LINE)
	{
		double x0,y0,x1,y1;

		//Obtain the point from the parameters.
		bool ok=(pAlrDtr->GetParam("p0")->GetRealValue(0,&x0)
				&& pAlrDtr->GetParam("p0")->GetRealValue(1,&y0)
				&& pAlrDtr->GetParam("p1")->GetRealValue(0,&x1)
				&& pAlrDtr->GetParam("p1")->GetRealValue(1,&y1)
		);

		if(ok) return QLine(qRound(x0),qRound(y0),qRound(x1),qRound(y1));
	}
	return QLine();
}

/**
* @brief Obtain a QPolygon from an X7sVSAlrDtrPoly.
* @param pAlrDtr An alarm detector with type = @ref X7S_VS_ALR_TYPE_POLY.
* If an error occurs during the setup it returns a empty QPolygon.
* (QPolygon::isEmpty() return @true)
*/
QPolygon QtBind::toQPolygon(X7sVSAlrDetector *pAlrDtr)
{
	QPolygon poly;


	if(pAlrDtr && pAlrDtr->GetType()==X7S_VS_ALR_TYPE_POLY)
	{
		const X7sParam *pParamX=pAlrDtr->GetParam("ptsX");
		const X7sParam *pParamY=pAlrDtr->GetParam("ptsY");
		if(pParamX->IsNull() || pParamY->IsNull()) return poly;

		int size=X7S_MIN(pParamX->GetLength(),pParamY->GetLength());
		double x,y;

		poly = QPolygon(size);

		for(int i=0;i<size;i++)
		{
			if(pParamX->GetRealValue(i,&x) && pParamY->GetRealValue(i,&y))
			{
				poly.setPoint(i,qRound(x),qRound(y));
			}
			else return QPolygon();
		}
	}
	return poly;
}


//------------------------------------------------


/**
 * @brief Create a X7sVSAlrDtrPoly from a QPolygon
 * @param parent The parent alarm manager that handle each alarm detector.
 * @param poly A not-empty polygon.
 * @param color	A specific color to draw the polygon.
 * @return A pointer on a generic X7sVSAlrDetector if no error occurs, otherwise @NULL.
 */
X7sVSAlrDetector*	QtBind::toX7sVSAlrDetector(X7sVSAlrManager *parent,const QPolygon& poly, const QColor& color)
{
	X7sVSAlrDetector* alrDtr=NULL;
	if(!poly.isEmpty())
	{

		int *x = new int[poly.size()];
		int *y = new int[poly.size()];
		for(int i = 0; i< poly.size(); i++){
			poly.point(i, &x[i], &y[i]);
		}
		alrDtr = x7sCreateVSAlarmPoly(parent,poly.size(), x, y);

		delete [] x;
		delete [] y;

		if(alrDtr && color.isValid())
		{
			alrDtr->SetParam("color","%d %d %d %d",color.blue(), color.green(), color.red(), color.alpha());
			alrDtr->Reload();
		}
	}
	return alrDtr;
}

/**
 * @brief Create a X7sVSAlrDtrLine from a QLine
 * @param parent The parent alarm manager that handle each alarm detector.
 * @param line A not-null line.
 * @param color	A specific color to draw the line.
 * @return A pointer on a generic X7sVSAlrDetector if no error occurs, otherwise @NULL.
 */
X7sVSAlrDetector*	QtBind::toX7sVSAlrDetector(X7sVSAlrManager *parent, const QLine& line, const QColor& color)
{
	X7sVSAlrDetector* alrDtr=NULL;
	if(!line.isNull())
	{
		alrDtr= x7sCreateVSAlarmLine(parent,line.p1().x(),line.p1().y(),line.p2().x(),line.p2().y());
		if(alrDtr && color.isValid())
		{
			alrDtr->SetParam("color","%d %d %d %d",color.blue(), color.green(), color.red(), color.alpha());
			alrDtr->Reload();
		}
	}
	return alrDtr;
}

