#include "QFileSystemInfo.hpp"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include <QDir>
#include "Log.hpp"

//Find disk space
#ifdef _WIN32
#include <windows.h>
#else // linux stuff
#include <sys/stat.h>
#include <sys/statvfs.h>		//< Use POSIX function.
#include <mntent.h>			//Know the mount point
#endif // _WIN32


QHash<qint64,QFileSystemInfo> QFileSystemInfo::mntPointList;	//Declare the static hash map


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor
//----------------------------------------------------------------------------------------

QFileSystemInfo::QFileSystemInfo(const QString& fs_name, const QString& fs_type, const QString& mnt_dir, qint64 fs_id, qint64 dev_id)
:QFileInfo(mnt_dir),
 fs_name(fs_name),fs_type(fs_type),mnt_dir(mnt_dir),fs_id(fs_id),dev_id(dev_id)
 {

 }


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

bool QFileSystemInfo::isOnSameMntPoint(const QString& file) const
		{
#ifdef _WIN32
	return (file.startsWith(mnt_dir));
#else
	QFileSystemInfo fileSysInfo(file);
	return isOnSameMntPoint(fileSysInfo);
#endif
		}

bool QFileSystemInfo::isOnSameMntPoint(const QFileSystemInfo& fileSysInfo) const
		{
	if(m_isValid) return fileSysInfo.dev_id==dev_id;
	else return false;
		}


bool QFileSystemInfo::Update(bool upMntPoint)
{
	m_isValid=false;
	qint64 diskid;

	if(upMntPoint) QFileSystemInfo::UpdateMntPoint();

	if(exists())
	{

#ifdef _WIN32

		ULARGE_INTEGER free,total;

		QString sCurDir = QDir::current().absolutePath();
		QDir::setCurrent(this->absoluteFilePath());
		bool bRes = ::GetDiskFreeSpaceExA( 0 , &free , &total , NULL );
		QDir::setCurrent( sCurDir );

		if ( !bRes ) {
			m_isValid=false;
		}
		else
		{
			this->freeSpace  = ((qint64)(free.QuadPart) );
			this->totalSpace = ((qint64)(total.QuadPart) );
			this->m_isValid=true;

			diskid=(qint64)this->absoluteFilePath().at(0).toAscii();		//!< Use the drive letter as a disk ID.
		}

#else //linux

		struct statvfs stvfs;
		struct stat st;
		const char* szDirPath = this->absoluteFilePath().toStdString().c_str();

		if ( ::statvfs(szDirPath,&stvfs) == -1 || ::stat(szDirPath,&st) == -1) {
			m_isValid=false;
		}
		else
		{
			this->freeSpace = (qint64)stvfs.f_bavail *  (qint64)stvfs.f_bsize;	//Obtain the size in bytes.
			this->totalSpace = (qint64)stvfs.f_blocks * (qint64)stvfs.f_bsize;	//Obtain the size in bytes.
			this->m_isValid=true;

			diskid = (qint64)st.st_dev;			//Disk ID
		}


#endif // _WIN32

		if(m_isValid)
		{
			//Then search the corresponding mountPoint
			if(QFileSystemInfo::mntPointList.contains(diskid)==false)	QFileSystemInfo::UpdateMntPoint();	//First update the mount point
			const QFileSystemInfo& mntPoint = QFileSystemInfo::mntPointList.value(diskid);

			this->fs_id=mntPoint.fs_id;
			this->dev_id= mntPoint.dev_id;
			this->mnt_dir=mntPoint.mnt_dir;
			this->fs_name=mntPoint.fs_name;
			this->fs_type=mntPoint.fs_type;
		}
	}
	return m_isValid;
}


QList<QFileSystemInfo> QFileSystemInfo::getMntPoints()
{
	if(mntPointList.isEmpty()) UpdateMntPoint();
	return mntPointList.values();
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Friend Methods
//----------------------------------------------------------------------------------------

QDebug operator<< (QDebug out, const QFileSystemInfo & fileSysInfo )
{
	out.nospace();
	QString tmp;
	tmp.sprintf("space %.2f/%.2f GiB (%.2f%%), ID=0x%0X",fileSysInfo.freeSpace/(1024.*1024.*1024.),fileSysInfo.totalSpace/(1024.*1024.*1024.),
			100*fileSysInfo.getFreeRatio(),(unsigned int)fileSysInfo.dev_id);
	out << "FileSystemInfo("<< fileSysInfo.filePath() << ", "<< tmp << ", mnt="<<fileSysInfo.mnt_dir<<", type=" <<fileSysInfo.fs_type ;
	out.space();
	return out;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------
void QFileSystemInfo::Init()
{
	freeSpace=-1;
	totalSpace=-1;
	fs_id=-1;
	dev_id=-1;

	if(mntPointList.size()==0) UpdateMntPoint();
	Update();

}





bool QFileSystemInfo::UpdateMntPoint()
{
	bool ret=false;
	qint64 diskid;

#ifdef _WIN32
	QFileInfoList fInfoList = QDir::drives();
	 foreach (QFileInfo fInfo, fInfoList)
	{
		 if(fInfo.exists())
		 {
			QString mntDir=fInfo.absoluteFilePath();
			diskid=(qint64)fInfo.absoluteFilePath().at(0).toAscii();		//!< Use the drive letter as a file system ID.
			 mntPointList.insert(diskid,QFileSystemInfo(mntDir,"",mntDir,diskid,diskid));
			 mntPointList[diskid].Update();
			 ret=true;
		 }
	}

#else

	int buflen=500;
	char buf[buflen];
	struct mntent mntbuf;
	struct mntent *mnt;
	const char *mntfname= "/etc/mtab";
	FILE* mntfp = setmntent(mntfname, "r" );
	AVO_CHECK_WARN("QFileSystemInfo::UpdateMntPoint()",mntfp,false,"Could not read file %s",mntfname);


	while((mnt = getmntent_r(mntfp,&mntbuf,(char*)&buf,buflen)) != NULL)
	{
		struct statvfs stvfs;
		if (::statvfs(mnt->mnt_dir,&stvfs) == -1 ) continue;

		struct stat st;
		if(::stat(mnt->mnt_dir,&st) == -1) continue;

		//Remove 0 size mnt point and temporary filesystem
		if(stvfs.f_blocks==0 || QString("tmpfs")==QString(mnt->mnt_type)) continue;

		diskid=(qint64)st.st_dev;
		mntPointList.insert(diskid,QFileSystemInfo(mnt->mnt_fsname,mnt->mnt_type,mnt->mnt_dir,stvfs.f_fsid,st.st_dev));
		mntPointList[diskid].Update();
		ret=true;
	}
	endmntent(mntfp);
#endif

	return ret;
}

