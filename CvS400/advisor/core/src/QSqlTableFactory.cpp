#include "QSqlTableFactory.hpp"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Log.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
QSqlDataTable::QSqlDataTable(const QString& name)
:m_name(name)
{
}

QSqlDataTable::~QSqlDataTable()
{

}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

bool QSqlDataTable::addColumn(const QString& name,const QSql::DataType& type, const QVariant& defVal, int length)
{
	if(this->contains(name)==false)
	{
		QSqlDataCol col;
		col.name=name;
		col.type=type;
		col.isnull=true;
		col.defVal=defVal;
		if(type==QSql::VarChar) col.length=250;
		else col.length=-1;

		m_listCols << col;
		return true;
	}
	qWarning() << name << "is already a column";
	return false;
}

bool QSqlDataTable::addColumn(const QString& name,const QSql::DataType& type, const QSql::DataProp& prop, int length)
{
	if(addColumn(name,type,QVariant(),length)) return setColumnProperties(name,prop);
	else return false;
}

bool QSqlDataTable::addColumn(const QString& name,const QSql::DataType& type, const QSql::DataProp& prop, const QVariant& defVal, int length)
{
	if(addColumn(name,type,defVal,length)) return setColumnProperties(name,prop);
	else return false;
}

bool QSqlDataTable::setColumnProperties(const QString& name, const QSql::DataProp& prop)
{
	int index;
	if(this->contains(name,&index))
	{
		switch(prop)
		{
		case QSql::Null: 	m_listCols[index].isnull=true; break;
		case QSql::NotNull:
			if(m_listCols[index].type != QSql::Serial) {
				m_listCols[index].isnull=false; break;
			}
		default:
			qWarning() << "property is not valid for " << name;
			return false;
		}
		return true;
	}
	qWarning() << "Column " << name << "does not exist";
	return false;
}

bool QSqlDataTable::addPrimaryKey(const QString& name)
{
	if(this->contains(name))
	{
		if(!primaKey.contains(name))
		{
			primaKey << name;
			return true;
		}
		qWarning() << name << "already inside the list";
	}
	qWarning() << "Column " << name << "does not exist";
	return false;
}

bool QSqlDataTable::addForeignKey(const QString &tablename, const QString&  colname)
{
	if(this->contains(colname))
	{
		QPair<QString,QString> pair = qMakePair(tablename,colname);
		if(!foreiKey.contains(pair))
		{
			foreiKey << pair;
			return true;
		}
		qWarning() << QString("%1.%2").arg(tablename).arg(colname) << "already inside the list";
	}
	qWarning() << "Column " << colname << "does not exist";
	return false;
}

bool QSqlDataTable::addIndex(const QString &name)
{
	if(this->contains(name))
	{
		if(!indexKey.contains(name))
		{
			indexKey.push_back(name);
			return true;
		}
		qWarning() << name << "already inside the list";
	}
	qWarning() << "Column " << name << "does not exist";
	return false;
}

bool QSqlDataTable::contains(const QString& column, int *index)
{
	for(int i=0;i<m_listCols.size();i++)
	{
		if(m_listCols[i].name==column) {
			if(index) *index=i;
			return true;
		}
	}
	if(index) *index=-1;
	return false;
}


//=====================================================================


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	General SQL Table Factory
//----------------------------------------------------------------------------------------

QSqlTableFactory::QSqlTableFactory(const QSqlDatabase& db)
:m_db(db)
{
}

QSqlTableFactory::~QSqlTableFactory() {

}

bool QSqlTableFactory::CreateTable(const QSqlDataTable& table)
{
	QString sql;
	QTextStream tstr(&sql);

	const char *funcName = "QSqlTableFactory::CreateTable()";


	tstr << "CREATE TABLE "<< Protect(table.m_name) << "\n( ";
	for (int i=0; i< table.m_listCols.size(); ++i)
	{
		tstr << (i==0?"":",") << "\n\t";
		tstr << Protect(table.m_listCols[i].name) << " ";
		tstr << GetDataType(table.m_listCols[i]) << " ";
		tstr << GetDefaultValue(table.m_listCols[i]) << " ";
		tstr << ((table.m_listCols[i].isnull)?"":"NOT NULL") << " ";
	}

	if(!table.primaKey.isEmpty())
	{
		tstr  << ",\n\t" << GetPrimaryDef(table.m_name) << "(";
		for (int i = 0; i < table.primaKey.size(); ++i)
		{
			tstr << (i==0?"":",") << Protect(table.primaKey[i]);
		}
		tstr << ")";
	}
	tstr << "\n);\n";

	avoDump(funcName) << sql;

	QSqlQuery query = m_db.exec(sql);
	if(!query.lastError().isValid()) return true;
	else{
		avoWarning(funcName) << sql << "Error:" << query.lastError();
		return false;
	}
}

QSqlTableFactory* QSqlTableFactory::createTableFactory(const QSqlDatabase& db)
{
	QSqlTableFactory* tabFacto=NULL;
	if(db.driverName()=="QPSQL") tabFacto= (QSqlTableFactory*)new QPSqlTableFactory(db);
	else if(db.driverName()=="QMYSQL")	tabFacto=(QSqlTableFactory*)new QMySqlTableFactory(db);

	return tabFacto;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	PSQl Table Factory
//----------------------------------------------------------------------------------------

QString QPSqlTableFactory::GetPrimaryDef(const QString &tbname)
{
	return " CONSTRAINT "+ Protect(tbname+"_pkey")+" PRIMARY KEY ";
}

QString QPSqlTableFactory::GetDataType(const QSqlDataCol& col)
{

	switch(col.type)
	{
	case QSql::Serial: return "serial";
	case QSql::Bool: return "bool";
	case QSql::SmallInt: return "smallint";
	case QSql::Integer: return "integer";
	case QSql::BigInt: return "bigint";
	case QSql::Real: return "real";
	case QSql::Timestamp: return "timestamp";
	case QSql::VarChar: return (QString("varchar (%1)").arg(col.length));
	default: return "integer";
	}
}

QString QPSqlTableFactory::GetDefaultValue(const QSqlDataCol& col)
{
	QString ret= "";
	if(col.defVal.isNull()==false) ret = "DEFAULT " + col.defVal.toString();
	return ret;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	MySQl Table Factory
//----------------------------------------------------------------------------------------


QString QMySqlTableFactory::GetPrimaryDef(const QString &tbname)
{
	return " PRIMARY KEY ";
}

QString QMySqlTableFactory::GetDataType(const QSqlDataCol& col)
{
	switch(col.type)
	{
	case QSql::Serial: return "INT AUTO INCREMENT PRIMARY KEY";
	case QSql::Bool: return "BOOL";
	case QSql::SmallInt: return "SMALLINT";
	case QSql::Integer: return "INT";
	case QSql::BigInt: return "BIGINT";
	case QSql::Real: return "DOUBLE";
	case QSql::Timestamp: return "DATETIME";
	case QSql::VarChar: return (QString("VARCHAR (%1)").arg(col.length));
	default: return "INT";
	}
}

QString QMySqlTableFactory::GetDefaultValue(const QSqlDataCol& col)
{
	QString ret= "";
	if(col.defVal.isNull()) return ret;
	else
	{
		ret = "default " + col.defVal.toString();;
	}
	return ret;
}
