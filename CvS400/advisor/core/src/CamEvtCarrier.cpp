#include "CamEvtCarrier.hpp"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "CamEvt.hpp"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
CamEvtCarrier::CamEvtCarrier()
:m_camEvt(NULL)
 {
	//Do nothing here
 }


CamEvtCarrier::CamEvtCarrier(CamEvt *camEvtData)
:m_camEvt(camEvtData)
 {
	//Do nothing here
 }


CamEvtCarrier::CamEvtCarrier(const CamEvtCarrier& copy)
:m_camEvt(NULL)
 {
	m_camEvt = copy.m_camEvt->getCopy();	//Get a copy of the data.
 }


CamEvtCarrier& CamEvtCarrier::operator=(const CamEvtCarrier& copy)
{
	this->Release();	//Release the old data.
	m_camEvt = copy.m_camEvt->getCopy();	//Get a copy of the data.
	return (*this);
}


CamEvtCarrier::~CamEvtCarrier()
{
	this->Release();
}

void CamEvtCarrier::Release()
{
	if(m_camEvt) delete m_camEvt;
	m_camEvt=NULL;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public method
//----------------------------------------------------------------------------------------


bool CamEvtCarrier::isValid() const 	{
	return (m_camEvt && m_camEvt->isValid());
}

bool CamEvtCarrier::isType(int type) const
{
	return (isValid() && m_camEvt->isType((CamEvt::Type)type));
}
