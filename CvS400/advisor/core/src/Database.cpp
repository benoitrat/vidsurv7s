#include "Database.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Camera.hpp"
#include "VideoOutFile.hpp"
#include "Board.hpp"
#include "Log.hpp"
#include "QtBind.hpp"
#include "QSqlTableFactory.hpp"
#include "CamEvt.hpp"

#define AVO_DB_CHECKERR(badval,valreturn) \
		{ if(!IsOk() || badval) { return valreturn; } }


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

Database::Database(X7sXmlObject *parent)
:X7sXmlObject(parent,AVO_TAGNAME_DATABASE),
 m_nOkError(0)
 {
	m_params.Insert(X7sParam("type",X7S_XML_ANYID,"QPSQL"));
	m_params.Insert(X7sParam("host",X7S_XML_ANYID,"localhost"));
	m_params.Insert(X7sParam("port",X7S_XML_ANYID,5432,X7STYPETAG_U16));
	m_params.Insert(X7sParam("dbname",X7S_XML_ANYID,"advisor"));
	m_params.Insert(X7sParam("options",X7S_XML_ANYID,""));
	m_params.Insert(X7sParam("username",X7S_XML_ANYID,"advisor"));
	m_params.Insert(X7sParam("password",X7S_XML_ANYID,"advisor"));
	m_params.Insert(X7sParam("enable",X7S_XML_ANYID,false,X7STYPETAG_INT,0,1));
 }

Database::~Database()
{
	AVO_PRINT_DEBUG("Database::~Database()");
	m_DB.close();
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

/**
* @brief Connect to the database using the XML parameters
* @return @true if the connection is done, @false otherwise
*/
bool Database::Connect(bool restart)
{
	X7S_FUNCNAME("Database::Connect()");
	AVO_PRINT_DEBUG(funcName);

	bool ret=false;
	if(m_DB.open()) {
		if(restart) m_DB.close();
		else return true;
	}

	//Really slow to check
	this->CreateDatabase();

	//Connect to the database
	m_DB = QSqlDatabase::addDatabase(m_params["type"]->toString().c_str());
	m_DB.setHostName(m_params["host"]->toString().c_str());
	if(m_params["port"]->IsOk()) m_DB.setPort(m_params["port"]->toIntValue());
	if(m_params["options"]->IsOk()) m_DB.setConnectOptions(m_params["options"]->toString().c_str());

	m_DB.setDatabaseName(m_params["dbname"]->toString().c_str());
	m_DB.setUserName(m_params["username"]->toString().c_str());
	m_DB.setPassword(m_params["password"]->toString().c_str());

	if(m_params["type"]->toString()=="PSQL")
	{
		//Set connection timeout
		m_DB.setConnectOptions("connect_timeout=15");
	}


	if(m_DB.drivers().contains(m_DB.driverName()))
	{

		ret = m_DB.open();
		if(ret==false)
		{
			avoWarning(funcName) << "Can not connect to database="<< m_DB.databaseName() <<" with user=" << m_DB.userName() << ", password="<< m_DB.password() << ", host=" << m_DB.hostName();
			avoWarning(funcName) <<  m_DB.lastError() << "(type=" << m_DB.lastError().type() << ")";
		}
		else AVO_PRINT_INFO("Database::Connect()","OK");
	}
	else
	{
		ret=false;
		QString errStr = m_DB.driverName()+ " driver not available ";
		avoWarning(funcName) << errStr << "("<< m_DB.drivers()<<")";
		m_DB.lastError().setDatabaseText(errStr);
	}
	return ret;
}

bool Database::IsOk()
{
	if(m_DB.isValid())
		return m_DB.isOpen();
	else {
		if(m_nOkError<5)
		{
			AVO_PRINT_WARN("Database::IsOk()","Database is not valid (%d) or not open (%d)",m_DB.isValid(),m_DB.isOpen());
			m_nOkError++;
		}
		return false;
	}
}


/**
* @brief Insert an Alarm Event to the database.
* @param cam	A pointer on the camera that generate this alarm event.
* @param alr_events The Alarm Event.
* @return @true if the insertion has been performed correctly.
*/
bool Database::Insert(const Camera *cam, const X7sVSAlarmEvt& alr_events)
{
	//Check the value
	AVO_DB_CHECKERR((cam==NULL),false);
	AVO_PRINT_DUMP("Database::Insert(X7sVSAlarmEvt)",  "%s alarm %d from blob %d (%d,%d)",
			cam->GetName().toStdString().c_str(),
			alr_events.alarm_ID, alr_events.blob_ID,
			alr_events.warn, alr_events.val);

	//Init value with the camera
	int cam_id=cam->GetSpecialID();
	QDateTime time=cam->GetTime();

	//Prepare the SQL query
	QSqlQuery query;
	query.prepare("INSERT INTO alr_events (cam_id, alarm_id, time, blob_id, warn, val) "
			"VALUES (?,?,?,?,?,?) ");
	query.addBindValue(cam_id);
	query.addBindValue((int)alr_events.alarm_ID);
	query.addBindValue(time.toString(Qt::ISODate));	//SQL normally use ISO 8601 time format.
	query.addBindValue(alr_events.blob_ID);
	query.addBindValue((bool)alr_events.warn);
	query.addBindValue((int)alr_events.val);

	//Execute the query.
	if(!query.exec()) {
		qWarning() << "Database::Insert(X7sAlrEvt)" << query.lastError();
	}
	return query.last();
}

/**
* @brief Insert that a video is recording into the database.
* @param video The VideoOutFile
* @return @true if the SQL query has been correctly executed.
*/
bool Database::Insert(const VideoOutFile* video)
{
	//Check the value
	AVO_DB_CHECKERR((video==NULL),false);
	AVO_CHECK_WARN("Database::Insert()",video->isValid(),false,"Video is not valid");
	AVO_PRINT_DEBUG("Database::Insert()", "%s (videos)",video->getFileInfo().filePath().toStdString().c_str());


	//Init value with the camera
	int cam_id=video->getCamera()->GetSpecialID();

	//Prepare the SQL query
	QSqlQuery query;
	query.prepare("INSERT INTO videos (cam_id, tstart, tlast, path) VALUES (?,?,?,?)");
	query.addBindValue(cam_id);
	query.addBindValue(video->getTimeStart());
	query.addBindValue(video->getTimeLast());
	query.addBindValue(video->getFileInfo().filePath());

	//Execute the query.
	if(!query.exec()) {
		qWarning() << "Database::Insert(Videos)" << query.lastError();
	}
	return query.last();
}

/**
* @brief Insert a row in the activity table for a given camera.
*
* @todo Take care of the error.
* @deprecated
*
* @code
* QSqlError(-1, "QPSQL: Unable to create query",
* "ERROR:  llave duplicada viola restricción de unicidad «activity_pkey»).
* @endcode
*
*
*
* @param cam	A pointer to the camera.
* @return @true if the Insertion has been done correctly.
*/
bool Database::Insert(const Camera *cam)	//TOREMOVE
{
	//Check the value
	AVO_DB_CHECKERR((cam==NULL),false);
	AVO_PRINT_DEBUG("Database::Insert()", "%s (activity)",cam->GetName().toStdString().c_str());

	//Init value with the camera
	int cam_id=cam->GetSpecialID();
	QDateTime time=cam->GetTime();

	//Prepare SQL query
	QSqlQuery query;
	query.prepare("INSERT INTO activity (cam_id, tstart, tlast) VALUES (?,?,?)");
	query.addBindValue(cam_id);
	query.addBindValue(time);
	query.addBindValue(time);

	//Execute SQL query and return it.
	if(!query.exec()) {
		qWarning() << "Database::Insert(Activity)" << query.lastError();
	}
	return query.last();
}

/**
* @brief Update the last datetime in a interval on a database.
* @note This function could be called only one however if the program crash we
* don't know how many frame did we save, therefore we can update the last datetime
* but not closing it.
* @param cam A pointer to the corresponding camera.
* @param type The type of Database table.
* @param closing If the Video or Activity is ending, closing must be @true.
* @deprecated
* @return @true if The update has been performed correctly.
*/
bool Database::Update(const Camera *cam, DBType type, bool closing) //TOREMOVE
{
	//Check the value
	AVO_DB_CHECKERR((cam==NULL),false);

	//Init value with the camera
	int cam_id=cam->GetSpecialID();
	QDateTime time=cam->GetTime();


	//Prepare SQL query according to the type of query.
	QSqlQuery query;
	if(type==Activity)
	{
		query.prepare("UPDATE activity SET tlast=?, ended=?"
				" WHERE cam_id=? AND tstart=(SELECT tstart from activity"
				" WHERE cam_id=? AND NOT ended"
				" ORDER BY tstart DESC LIMIT 1)");
		query.addBindValue(time);
		query.addBindValue(closing);
		query.addBindValue(cam_id);
		query.addBindValue(cam_id);
	}
	else {
		qWarning() << "Update does not work for this table";
		return false;
	}
	if(!query.exec()) {
		qWarning() << "Database::Update()" << GetTableType(type) <<":"<< query.lastError();
	}
	return query.last();

}

bool Database::Update(const VideoOutFile* video)
{
	X7S_FUNCNAME("Database::Update(video)");

	//Init value with the camera
	if(!video->isValid()) return false;


	int cam_id=video->getCamera()->GetSpecialID();

	//Prepare SQL query according to the type of query.
	QSqlQuery query;

	query.prepare("UPDATE videos SET tlast=?, ended=?, size=?"
			" WHERE video_id=(SELECT video_id from videos"
			" WHERE cam_id=? AND NOT ended"
			" ORDER BY tstart DESC LIMIT 1)");
	query.addBindValue(video->getTimeLast());
	query.addBindValue(video->isClosed());
	query.addBindValue((int)video->getLastSize());
	query.addBindValue(cam_id);

	if(!query.exec())
	{
		avoWarning(funcName) << query.lastError();
	}

	return query.last();
}

/**
* @brief Process the various camera event using the camera event holder
* @param holderCamEvt The camera event holder let us not preoccupied about deleting the camEvt.
* @return
*/
bool Database::ProcessCamEvt(const CamEvtCarrier& holderCamEvt)
{
	X7S_FUNCNAME("Database::ProcessCamEvt()");
	AVO_DB_CHECKERR(holderCamEvt.isNull(),false);

	const CamEvt *camEvt = holderCamEvt.getCamEvt();

	switch(camEvt->getType())
	{
	case CamEvt::Activity: return 	this->ProcessCamEvt((const CamEvtActivity*)camEvt);
	case CamEvt::VSAlarm: return this->ProcessCamEvt((const CamEvtVSAlarm*)camEvt);
	case CamEvt::Video: 	return this->ProcessCamEvt((const CamEvtVideo*)camEvt);
	default:
		AVO_PRINT_WARN(funcName,"Unknown camEvt type %d",camEvt->getType());
		break;
	}
	return false;
}

//TODO:Error in comment.
/**
* @brief Process a camera activity event.
*
* @todo Take care of the error.
* @code
* QSqlError(-1, "QPSQL: Unable to create query",
* "ERROR:  llave duplicada viola restricción de unicidad «activity_pkey»).
* @endcode
*
* @param camEvt The Generic camera event.
* @return
*/
bool Database::ProcessCamEvt(const CamEvtActivity* camEvt)
{
	X7S_FUNCNAME("Database::ProcessCamEvt(Activity)");
	AVO_CHECK_WARN(funcName,IsOk(),false);
	AVO_CHECK_WARN(funcName,camEvt,false,"CamEvt is NULL");
	AVO_CHECK_WARN(funcName,camEvt->isType(camEvt->class_type),false,"Wrong type");
	AVO_CHECK_WARN(funcName,camEvt->getCamera(),false,"Camera is not valid");
	AVO_CHECK_WARN(funcName,camEvt->getDateTime().isValid(),false,"QDateTime is not valid");

	int cam_id = camEvt->getCamera()->GetSpecialID();

	avoDebug(funcName) << camEvt->getCamera()->GetName();

	//Prepare SQL query
	QSqlQuery query;

	if(camEvt->isState(CamEvt::Start))
	{
		query.prepare("INSERT INTO activity (cam_id, tstart, tlast) VALUES (?,?,?)");
		query.addBindValue(cam_id);
		query.addBindValue(camEvt->getDateTime());
		query.addBindValue(camEvt->getDateTime());
	}
	else
	{
		bool closing;
		if(camEvt->isState(CamEvt::Update)) closing=false;
		else if(camEvt->isState(CamEvt::Finish)) closing=true;
		else AVO_CHECK_WARN(funcName,false,false,"Unknown state %s (%d)",camEvt->getStateDesc(),camEvt->getState());

		query.prepare("UPDATE activity SET tlast=?, ended=?"
				" WHERE cam_id=? AND tstart=(SELECT tstart from activity"
				" WHERE cam_id=? AND NOT ended"
				" ORDER BY tstart DESC LIMIT 1)");
		query.addBindValue(camEvt->getDateTime());
		query.addBindValue(closing);
		query.addBindValue(cam_id);
		query.addBindValue(cam_id);

	}

	//Execute SQL query and return it.
	if(!query.exec()) {
		avoWarning(funcName) << camEvt->getTypeDesc() << camEvt->getStateDesc() << query.lastError();
	}
	return query.last();
}


/**
* @brief Process a camera video event
*
* @param camEvt A pointer on a valid camera video event.
* @return @true if the SQL query has been correctly executed.
* @see Database::ProcessCamEvt()
*/
bool Database::ProcessCamEvt(const CamEvtVideo* camEvt)
{
	X7S_FUNCNAME("Database::ProcessCamEvt(Video)");
	AVO_CHECK_WARN(funcName,IsOk(),false);
	AVO_CHECK_WARN(funcName,camEvt,false,"CamEvt is NULL");
	AVO_CHECK_WARN(funcName,camEvt->isType(camEvt->class_type),false,"Wrong type");
	AVO_CHECK_WARN(funcName,camEvt->getDateTime().isValid(),false,"QDateTime is not valid");

	int cam_id = camEvt->getCamera()->GetSpecialID();

	//Take the video from the event.
	const VideoFile *video=camEvt->m_vidFile;
	if(!video->isValid()) return false;

	avoDebug(funcName) << camEvt->getCamera()->GetName();

	//Prepare SQL query
	QSqlQuery query;

	if(camEvt->isState(CamEvt::Start))
	{
		query.prepare("INSERT INTO activity (cam_id, tstart, tlast) VALUES (?,?,?)");
		query.addBindValue(cam_id);
		query.addBindValue(camEvt->getDateTime());
		query.addBindValue(camEvt->getDateTime());
	}
	else
	{
		bool closing;
		if(camEvt->isState(CamEvt::Update)) closing=false;
		else if(camEvt->isState(CamEvt::Finish)) closing=true;
		else AVO_CHECK_WARN(funcName,false,false,"Unknown state %s (%d)",camEvt->getStateDesc(),camEvt->getState());

		query.prepare("UPDATE videos SET tlast=?, ended=?, size=?"
				" WHERE video_id=(SELECT video_id from videos"
				" WHERE cam_id=? AND NOT ended"
				" ORDER BY tstart DESC LIMIT 1)");
		query.addBindValue(camEvt->getDateTime());
		query.addBindValue(video->isClosed());
		query.addBindValue((int)video->getLastSize());
		query.addBindValue(cam_id);

	}

	//Execute SQL query and return it.
	if(!query.exec()) {
		avoWarning(funcName) << camEvt->getTypeDesc() << camEvt->getStateDesc() << query.lastError();
	}
	return query.last();
}


/**
* @brief Process a camera video-surveillance alarm event.
*
* @param camEvt A pointer on a valid camera Video-surveillance alarm event.
* @return @true if the SQL query has been correctly executed.
* @see Database::ProcessCamEvt()
*/
bool Database::ProcessCamEvt(const CamEvtVSAlarm* camEvt)
{
	X7S_FUNCNAME("Database::ProcessCamEvt(VSAlarm)");
	AVO_CHECK_WARN(funcName,IsOk(),false);
	AVO_CHECK_WARN(funcName,camEvt,false,"CamEvt is NULL");
	AVO_CHECK_WARN(funcName,camEvt->isType(camEvt->class_type),false,"Wrong type");
	AVO_CHECK_WARN(funcName,camEvt->getDateTime().isValid(),false,"QDateTime is not valid");


	int cam_id = camEvt->getCamera()->GetSpecialID();
	const X7sVSAlarmEvt& alr_events = camEvt->vsAlrEvt;


	avoDebug(funcName) << camEvt << "   (" << cam_id << ")";



	//Prepare SQL query
	QSqlQuery query;

	if(camEvt->isState(CamEvt::Default))
	{
		//Prepare the SQL query
		query.prepare("INSERT INTO alr_events (cam_id, alarm_id, time, blob_id, warn, val) VALUES (?,?,?,?,?,?)");
		query.addBindValue(cam_id);
		query.addBindValue((int)alr_events.alarm_ID);
		query.addBindValue(camEvt->getDateTime().toString(Qt::ISODate));	//SQL normally use ISO 8601 time format.
		query.addBindValue(alr_events.blob_ID);
		query.addBindValue((bool)alr_events.warn);
		query.addBindValue((int)alr_events.val);

	}
	else
	{
		avoWarning(funcName) << "CamEvt state=" << camEvt->getStateDesc() << "is not correct";
		return false;
	}

	//Execute SQL query and return it.
	if(!query.exec()) {
		avoWarning(funcName) << camEvt << camEvt->getStateDesc() << query.lastError();
	}
	return query.last();
}




bool Database::Delete(const VideoInFile& videoIn)
{
	X7S_FUNCNAME("Database::Delete()");
	AVO_DB_CHECKERR(videoIn.getDatabaseID()==-1,false);

	QSqlQuery query;
	query.prepare("DELETE from videos WHERE video_id = ?;");
	query.addBindValue(videoIn.getDatabaseID());
	if(!query.exec())
	{
		avoWarning(funcName) << "video_id= " << videoIn.getDatabaseID() << query.lastError();
	}
	else avoDebug(funcName) << "video_id=" << videoIn.getDatabaseID();

	return query.last();


}


QVector<AVOAlrEvt> Database::GetAlarmEvt(const Camera *cam,QDateTime start, double hour)
{
	QVector<AVOAlrEvt> vecAlrEvt;
	QSqlQuery query;
	QDateTime end;
	int cam_id;

	AVO_DB_CHECKERR((cam==NULL),vecAlrEvt);

	cam_id=cam->GetSpecialID();

	if(start.isNull())
	{
		start = this->GetLastHour(cam,Alarm);
		if(start.isNull()) return vecAlrEvt;
	}

	end=start.addSecs((int)hour*3600);

	query.prepare("SELECT time, alarm_id, blob_id, warn, val  FROM alr_events WHERE cam_id =? AND time >= ? AND time < ? ORDER BY time ASC;");
	query.addBindValue(cam_id);
	query.addBindValue(start.toString(Qt::ISODate));
	query.addBindValue(end.toString(Qt::ISODate));

	if(!query.exec()) qWarning() << "Database::GetAlarmEvt()" << query.lastError();
	while (query.next()) {
		AVOAlrEvt alr_events;

		alr_events.cam=cam;

		QDateTime dtime=QDateTime::fromString(query.value(0).toString(),Qt::ISODate);
		bool ok=dtime.isValid();

		if(ok) alr_events.t 		= 	dtime.toTime_t();
		if(ok) alr_events.alarm_ID	=	query.value(1).toInt(&ok);
		if(ok) alr_events.blob_ID	=	query.value(2).toInt(&ok);
		if(ok) alr_events.warn		=	query.value(3).toInt(&ok);
		if(ok) alr_events.val		=	query.value(4).toInt(&ok);

		if(ok) vecAlrEvt.push_back(alr_events);
	}

	return vecAlrEvt;
}

QVector<AVOInterval> Database::GetMJPEG(const Camera *cam, QDateTime start, double hour)
{
	QVector<AVOInterval> vecItrv;
	QDateTime end;
	int cam_id;

	AVO_DB_CHECKERR((cam==NULL),vecItrv);
	if(start.isNull()) return vecItrv;

	end=start.addSecs((int)(hour*3600));
	cam_id=cam->GetSpecialID();

	QSqlQuery query;
	query.prepare("SELECT tstart, tlast FROM videos WHERE cam_id=? AND tlast>=? AND tstart <= ?");
	query.addBindValue(cam_id);
	query.addBindValue(start.toString(Qt::ISODate));
	query.addBindValue(end.toString(Qt::ISODate));

	if(!query.exec()) qWarning() << "Database::GetAlarmEvt()" << query.lastError();
	while (query.next()) {
		//Obtain the interval
		QDateTime t0,t1;
		t0 = QDateTime::fromString(query.value(0).toString(),Qt::ISODate);
		t1 = QDateTime::fromString(query.value(1).toString(),Qt::ISODate);

		//Cut the value into the hour.
		t0 = qMax(start,t0);
		t1 = qMin(end,t1);

		//If value are valid put the corresponding interval in the vector.
		if(t0.isValid() && t1.isValid())
			vecItrv.push_back(qMakePair(t0,t1));
	}

	return vecItrv;

}

QVector<AVOInterval> Database::GetActivity(const Camera *cam, QDateTime start, double hour)
{
	QVector<AVOInterval> vecItrv;
	QDateTime end;
	int cam_id;

	AVO_DB_CHECKERR((cam==NULL),vecItrv);
	if(start.isNull()) return vecItrv;

	end=start.addSecs((int)(hour*3600));
	cam_id=cam->GetSpecialID();

	QSqlQuery query;
	query.prepare("SELECT tstart, tlast FROM activity WHERE cam_id=? AND tlast>=? AND tstart <= ?");
	query.addBindValue(cam_id);
	query.addBindValue(start.toString(Qt::ISODate));
	query.addBindValue(end.toString(Qt::ISODate));

	if(!query.exec()) qWarning() << "Database::GetActivity()" << query.lastError();
	while (query.next()) {
		//Obtain the interval
		QDateTime t0,t1;
		t0 = QDateTime::fromString(query.value(0).toString(),Qt::ISODate);
		t1 = QDateTime::fromString(query.value(1).toString(),Qt::ISODate);

		//Cut the value into the hour.
		t0 = qMax(start,t0);
		t1 = qMin(end,t1);

		//If value are valid put the corresponding interval in the vector.
		if(t0.isValid() && t1.isValid())
			vecItrv.push_back(qMakePair(t0,t1));
	}


	return vecItrv;
}

QVector<VideoInFile> Database::GetVideos(const QString& querySuffix)
{
	QVector<VideoInFile> vecVids;
	AVO_DB_CHECKERR(querySuffix.isNull(),vecVids);

	QString funcName="Database::GetVideoPath()";

	QSqlQuery query;
	QString qStr = "SELECT video_id, cam_id, tstart, tlast, ended,path, size, locked FROM videos ";
	qStr+=querySuffix + ";";

	avoDump(funcName) << "Query:"<< qStr;

	if(query.exec(qStr)==false)
	{
		avoError(funcName) << qStr << query.lastError();
	}
	else {
		int size=query.size();
		vecVids.reserve(size);
		while(query.next()) {
			int ID = query.value(0).toInt();
			int cam_id = query.value(1).toInt();
			QDateTime tStart = query.value(2).toDateTime();
			QDateTime tEnd = query.value(3).toDateTime();
			bool ended=query.value(4).toBool();
			QString fpath = query.value(5).toString();
			qint64 size=(qint64)query.value(6).toLongLong();
			bool locked=query.value(7).toBool();

			vecVids.push_back(VideoInFile(cam_id,fpath,tStart,tEnd,ID,size,ended,locked));

			avoDump(funcName) << "Video #" << ID <<":" << tStart << tEnd << ended <<"; "<< fpath << size << locked;
		}
		if(vecVids.size()==0) avoWarning(funcName) << "No video for SQL Query" << qStr;
	}
	return vecVids;
}


/**
* @brief Get all the videos that end after the given time.
* @param cam Video that correspond to this camera.
* @param dtime	the given time.
* @param nVideosMax	The number maximum of video in the vector.
* @return A vector of VideoInFile.
*/
QVector<VideoInFile> Database::GetVideos(const Camera *cam, const QDateTime& dtime, int nVideosMax)
{
	int cam_id;
	QVector<VideoInFile> vecVids;
	X7S_FUNCNAME("Database::GetVideos()");

	AVO_DB_CHECKERR((cam==NULL),vecVids);

	cam_id=cam->GetSpecialID();

	QSqlQuery query;
	query.prepare("SELECT video_id, tstart, tlast, ended,path, size, locked "
			"FROM videos WHERE cam_id=? AND tlast >=? "
			"ORDER BY tstart ASC LIMIT ?");
	query.addBindValue(cam_id);
	query.addBindValue(dtime.toString(Qt::ISODate));
	query.addBindValue(nVideosMax);


	if(query.exec())
	{
		int size=query.size();
		if(size>0) {
			vecVids.reserve(size);
			while(query.next()) {
				int ID = query.value(0).toInt();
				QDateTime tStart = query.value(1).toDateTime();
				QDateTime tEnd = query.value(2).toDateTime();
				bool ended=query.value(3).toBool();
				QString fpath = query.value(4).toString();
				qint64 size=(qint64)query.value(5).toLongLong();
				bool locked=query.value(6).toBool();
				vecVids.push_back(VideoInFile(cam_id,fpath,tStart,tEnd,ID,size,ended,locked));

				avoDebug(funcName) << "Video #" << ID <<":" << tStart << tEnd << ended <<"; "<< fpath << size << locked;
			}
		}
		else {
			avoWarning(funcName) << "No videos found in database for camera" << cam->GetSpecialID() << "and time" << dtime;
		}
	}
	else
	{
		avoWarning(funcName) << "Query failed:" << query.lastError();
	}
	return vecVids;
}

/**
* @brief Return the video that start before dtime and end after it.
*
* @note This function is not only a calls to GetVideos() with nVideosMax=1, but also check if
* the VideoInFile::getTimeStart() is lesser than dtime.
* @see VideoInFile::isNull(), VideoInFile::getTimeStart(), VideoInFile::getTimeEnd().
*
* @param cam The video corresponds to this camera.
* @param dtime the desired time.
* @return A VideoFile (If an error occurs or not video are found it return a null VideoFile).
*/
VideoInFile Database::GetVideo(const Camera *cam, const QDateTime& dtime)
{
	X7S_FUNCNAME("Database::GetVideo()");

	QVector<VideoInFile> vecVids=GetVideos(cam,dtime,1);
	if(vecVids.size()>0)
	{
		avoDebug(funcName) << vecVids[0].getFileInfo().fileName() << vecVids[0].getTimeStart();

		if(vecVids[0].getTimeStart()<=dtime)
			return vecVids[0];
	}
	avoWarning(funcName) << "No video found in database for camera" << cam->GetSpecialID() << "and time" << dtime;
	return VideoInFile();
}


QString Database::GetVideoPath(const Camera *cam, const QDateTime& dtime)
{
	int cam_id;
	QString fpath;

	AVO_DB_CHECKERR((cam==NULL),fpath);

	cam_id=cam->GetSpecialID();

	QSqlQuery query;
	query.prepare("SELECT path FROM videos WHERE cam_id=? AND tstart <=? AND ? <= tlast");
	query.addBindValue(cam_id);
	query.addBindValue(dtime.toString(Qt::ISODate));
	query.addBindValue(dtime.toString(Qt::ISODate));


	if(query.exec()==false)
	{
		qWarning() << "	Database::GetVideoPath()" << query.lastError();
	}
	else {
		if(query.next()) {
			fpath = query.value(0).toString();
		}
		else {
			AVO_PRINT_WARN("Database::GetVideoPath()",
					"No video for camera %d at datetime=%s",
					cam_id,dtime.toString(Qt::ISODate).toStdString().c_str());
		}
	}
	return fpath;

}

int Database::countElements(const Camera *cam, DBType type,const QDateTime &start,const QDateTime &end)
{
	const char *tmp;
	int nElements=-1;
	int cam_id;

	AVO_DB_CHECKERR((cam==NULL),-1);

	if(type==Database::All)
	{
		if(nElements<=0) nElements=countElements(cam,Database::Event,start,end);
		if(nElements<=0) nElements=countElements(cam,Database::Activity,start,end);
		if(nElements<=0) nElements=countElements(cam,Database::Video,start,end);
	}
	else
	{
		QSqlQuery query;
		switch(type)
		{
		case Database::Alarm:
			query.prepare("SELECT COUNT(*) FROM alr_events WHERE cam_id=? AND warn=1 AND ? <= time AND time <= ?;");
			tmp="Alarm";
			break;
		case Database::Event:
			query.prepare("SELECT COUNT(*) FROM alr_events WHERE cam_id=? AND ? <= time AND time <= ?;");
			tmp="Event";
			break;
		case Database::Activity:
			query.prepare("SELECT COUNT(*) FROM activity WHERE cam_id=? AND ? <= tlast AND tstart <= ?;");
			tmp="Activity";
			break;
		case Database::Video:
		default:
			query.prepare("SELECT COUNT(*) FROM videos WHERE cam_id=? AND ? <= tlast AND tstart <= ?;");
			tmp="Video";
			break;
		}
		cam_id=cam->GetSpecialID();
		query.addBindValue(cam_id);
		query.addBindValue(start.toString(Qt::ISODate));
		query.addBindValue(end.toString(Qt::ISODate));


		if(query.exec()==false)
		{
			qWarning() << "	Database::GetLastHour()" << query.lastError();
		}
		else {
			if(query.next()) {
				bool ok;
				nElements = query.value(0).toInt(&ok);
				if(ok) return nElements;
			}
			else {
				AVO_PRINT_WARN("Database::GetLastHour()","No %s for camera %d",tmp,cam_id);
			}
		}
	}
	return nElements;
}


QDateTime Database::GetLastHour(const Camera *cam, DBType type)
{
	const char *tmp;
	QDateTime dtime;
	int cam_id;

	AVO_DB_CHECKERR((cam==NULL),dtime);

	if(type==All)
	{
		if(dtime.isNull()) dtime=GetLastHour(cam,Alarm);
		if(dtime.isNull()) dtime=GetLastHour(cam,Event);
		if(dtime.isNull()) dtime=GetLastHour(cam,Activity);
		if(dtime.isNull()) dtime=GetLastHour(cam,Video);
	}
	else
	{
		QSqlQuery query;
		switch(type)
		{
		case Alarm:
			query.prepare("SELECT time FROM alr_events WHERE cam_id=? AND warn=true ORDER BY time DESC LIMIT 1;");
			tmp="Alarm";
			break;
		case Event:
			query.prepare("SELECT time FROM alr_events WHERE cam_id=? ORDER BY time DESC LIMIT 1;");
			tmp="Event";
			break;
		case Activity:
			query.prepare("SELECT tstart FROM activity WHERE cam_id=? ORDER BY tstart DESC LIMIT 1;");
			tmp="Activity";
			break;
		case Video:
		default:
			query.prepare("SELECT tstart FROM videos WHERE cam_id=? ORDER BY tstart DESC LIMIT 1;");
			tmp="Video";
			break;
		}
		cam_id=cam->GetSpecialID();
		query.addBindValue(cam_id);

		if(query.exec()==false)
		{
			qWarning() << "	Database::GetLastHour()" << query.lastError();
		}
		else {
			if(query.next()) {
				QString dtimeStr = query.value(0).toString();
				if(dtimeStr.isNull()) return QDateTime();
				dtime = QDateTime::fromString(dtimeStr,Qt::ISODate);
				dtimeStr=dtime.toString("yyyy-MM-dd hh:00:00");
				dtime = QDateTime::fromString(dtimeStr,"yyyy-MM-dd hh:00:00");
			}
			else {
				AVO_PRINT_WARN("Database::GetLastHour()","No %s for camera %d",tmp,cam_id);
			}
		}


	}
	return dtime;
}


/**
* @brief Method to print the value of a table
* @param tablename the name of the table.
*/
void Database::PrintTable(const QString & tablename)
{
	QSqlQuery query;
	QString queryStr;

	queryStr += "SELECT * FROM `";
	queryStr += tablename;
	queryStr += "` LIMIT 0,30;";
	query.exec(queryStr);
	QSqlRecord rec = query.record();

	QString rowStr;
	int nCols = rec.count(); //Obtain the number of column

	for(int col=0; col<nCols; col++) {
		rowStr += rec.fieldName(col);
		rowStr += ", ";
	}
	avoDebug("Database::PrintTable ") << rowStr;
	rowStr ="";

	while (query.next()) {
		for (int col=0; col<nCols; ++col)
		{
			rowStr += query.value(col).toString();
			rowStr += ", ";
		}
		avoDebug("Database::PrintTable ") << rowStr;
		rowStr ="";
	}
}


QString Database::lastErrorStr() const {
	return m_DB.lastError().databaseText();
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

/**
* @brief Create the database if it doesn't exist.
* @return
*/
bool Database::CreateTable()
{
	//Keep a list of all the mapTable.
	QMap<QString,QSqlDataTable*> mapTable;

	//Creation of alarm Evt table
	QSqlDataTable tabAlrEvt("alr_events");
	tabAlrEvt.addColumn("evt_id",QSql::Serial);
	tabAlrEvt.addColumn("cam_id",QSql::Integer,QSql::NotNull);
	tabAlrEvt.addColumn("alarm_id",QSql::Integer,QSql::NotNull);
	tabAlrEvt.addColumn("time",QSql::Timestamp,QSql::NotNull);
	tabAlrEvt.addColumn("blob_id",QSql::Integer,QSql::NotNull);
	tabAlrEvt.addColumn("warn",QSql::Bool,QSql::NotNull);
	tabAlrEvt.addColumn("val",QSql::SmallInt,QSql::NotNull);
	tabAlrEvt.addIndex("cam_id");
	tabAlrEvt.addIndex("alarm_id");
	tabAlrEvt.addIndex("time");
	mapTable["alr_events"] = &tabAlrEvt;

	QSqlDataTable tabVids("videos");
	tabVids.addColumn("video_id",QSql::Serial);
	tabVids.addColumn("cam_id",QSql::Integer,QSql::NotNull);
	tabVids.addColumn("tstart",QSql::Timestamp,QSql::NotNull);
	tabVids.addColumn("tlast",QSql::Timestamp,QSql::Null);
	tabVids.addColumn("ended",QSql::Bool,QSql::Null,QVariant(false));
	tabVids.addColumn("path",QSql::VarChar,QSql::NotNull,255);
	tabVids.addColumn("size",QSql::BigInt,QSql::Null);
	tabVids.addColumn("locked",QSql::Bool,QSql::Null,QVariant(false));
	mapTable["videos"] = &tabVids;

	QSqlDataTable tabActy("activity");
	tabActy.addColumn("cam_id",QSql::Integer,QSql::NotNull);
	tabActy.addColumn("tstart",QSql::Timestamp,QSql::NotNull);
	tabActy.addColumn("tlast",QSql::Timestamp,QSql::Null);
	tabActy.addColumn("ended",QSql::Bool,QSql::Null,QVariant(false));
	tabActy.addPrimaryKey("cam_id");
	tabActy.addPrimaryKey("tstart");
	mapTable["activity"]  = &tabActy;

	QSqlTableFactory * tabFac= QSqlTableFactory::createTableFactory(m_DB);

	bool ret=true;
	QStringList tableList = m_DB.tables(QSql::Tables );
	QMapIterator<QString,QSqlDataTable *> i(mapTable);
	while(i.hasNext()) {
		i.next();
		//If the tableList doesn't have the table create it.
		if(!tableList.contains(i.key(),Qt::CaseInsensitive))
		{
			avoInfo("Database::CreateTable()") << "Creating table" << i.key();
			ret = ret & tabFac->CreateTable(*(i.value()));
		}

	}

	X7S_DELETE_PTR(tabFac);

	return ret;
}

/**
* @brief Return the default parameters for a database.
* @param type Should be QPSQL or QMYSQL
* @warning This function only fill the parameters of the database, im order to use it
* you must add it to the list of connection.
*
* QSqlDatabase::addDatabase(type,db.connectionName());
*
*
* @return A non valid database
*/
QSqlDatabase Database::GetDefault(const QString& type)
{
	QSqlDatabase db;
	db.setHostName("localhost");

	if(type=="QPSQL")
	{
		db.setPort(5432);
		db.setDatabaseName("postgres");
		db.setUserName("postgres");
	}
	else if(type=="QMYSQL")
	{
		db.setPort(3306);
		db.setDatabaseName("test");
		db.setUserName("root");
	}
	else
	{
		qWarning() << "No default config for this type of database";	//Return an empty map when the type is unknown.
	}
	return db;
}

/**
* @brief Create the database given the value in the
* @return
*/
bool Database::CreateDatabase()
{
	QSqlDatabase db;

	//Chek Type and defVal.
	QString type=QtBind::toQString(m_params["type"]);
	if(!QSqlDatabase::isDriverAvailable(type))
	{
		qWarning() << "Driver:" << type << "is not available";
		qWarning() << QSqlDatabase::drivers();
		return false;
	}
	else {

		//Obtain the default database for this type
		db = Database::GetDefault(type);
		db = QSqlDatabase::addDatabase(type,db.connectionName());

		//Change to the user value (but keep default database in order to create a new one).
		db.setHostName(QtBind::toQString(m_params["host"]));
		if(m_params["port"]->IsOk()) db.setPort(m_params["port"]->toIntValue());
		if(m_params["options"]->IsOk()) db.setConnectOptions(QtBind::toQString(m_params["options"]));
		db.setUserName(QtBind::toQString(m_params["username"]));
		db.setPassword(QtBind::toQString(m_params["password"]));

		QSqlError err;

		if(db.open()) {
			QString dbname=QtBind::toQString(m_params["dbname"]);
			QSqlQuery query = db.exec("CREATE DATABASE "+dbname);
			err=query.lastError();
			db.close();
			m_DB=db;
			db.setDatabaseName(dbname);
			if(db.open()) return CreateTable();
			else {
				if(query.isValid()) {
					qWarning() << "Database already exist but can not connect to it" << dbname;
					err=db.lastError();
				}
				else {
					qWarning() << "Can not create database " << dbname;
				}
			}
		}
		else {
			qWarning() << QString("Can not connect to %1:%2@%3:%4\n").arg(db.userName()).arg(db.password()).arg(db.hostName()).arg(db.port());
			err = db.lastError();
		}
		qWarning() << err << "(" << err.type() << "," << err.number() << ")";
	}
	QSqlDatabase::removeDatabase(db.connectionName());
	return false;
}

QString Database::GetTableType(DBType type)
{
	switch(type)
	{
	case All: 		return "All";
	case Video:		return "Video";
	case Activity:	return "Activity";
	case Event:		return "Event";
	case Alarm: 	return "Alarm";
	default: 		return "Unknown";
	}
}
