#include "Camera.hpp"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Board.hpp"
#include "Devices.hpp"
#include "Log.hpp"
#include "QtBind.hpp"
#include "CamEvt.hpp"
#include "CamStats.hpp"


#include <x7svidsurv.h>
#include <cv.h>
#include <highgui.h>
#include <ctime>
#include <sstream>
using namespace std;

#include <QTextStream>
#include <QDate>
#include <QDir>
#include <QtDebug>

int Camera::nInstances=0;

QColor cam_color[] = {
		Qt::red,
		Qt::green,
		Qt::blue,
		Qt::cyan,
		Qt::magenta,
		Qt::yellow,
		QColor(255,128,0),
		QColor(128,255,0),
		QColor(0,255,128),
		QColor(0,128,255),
		QColor(128,0,255),
		QColor(255,0,128),
};


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

/**
* @brief Construct the Camera with a specific ID.
*
* The ID,
*
* @param board A pointer
* @param ID
* @return
*/
Camera::Camera(Board *board,int ID)
:X7sXmlObject(board,AVO_TAGNAME_CAM,X7S_XML_ANYTYPE,ID),
 board(board)
{
	Init();
	vidsurv = x7sCreateVidSurvPipelineFG(this);
}

Camera::~Camera()
{
	AVO_PRINT_DEBUG("Camera::~Camera()","ID=%d",GetID());

	//Close all the field in the database.
	this->GenerateCamEvt(true,true);

	X7S_DELETE_PTR(vidsurv);
	X7S_DELETE_PTR(stats);

	if(serverMode) x7sReleaseByteArray(&metaJpegData);
	else {
		x7sReleaseByteArray(&(metaJpegFrame.pJpeg));
		x7sReleaseByteArray(&(metaJpegFrame.pBlobs));
		x7sReleaseByteArray(&(metaJpegFrame.pAlarm));
	}
	cvReleaseImage(&imCvFrame);
	cvReleaseImage(&imCvMData);
	cvReleaseImage(&imCvFGMask);

}




void Camera::Init()
{
	//Init the member values
	vidsurv=NULL;

	imCvFrame=NULL;
	imCvFGMask=NULL;
	imCvMData=NULL;

	hasActivity=false;
	isDisplayed=true;
	isRecording=false;
	m_isResetVideo=false;

	//Init the JPEG + metadata memory
	metaJpegData=NULL;
	serverMode=false;

	stats = new CamStats();

	metaJpegFrame.id=-1;
	metaJpegFrame.tag=X7S_CV_MJPEG_FRAMETAG_NORMAL;
	metaJpegFrame.pAlarm=NULL;
	metaJpegFrame.pJpeg=NULL;
	metaJpegFrame.pBlobs=NULL;
	metaJpegFrame.time=NULL;

	//Create the list of parameters

	//Parameters of the camera
	m_params.Insert(X7sParam("name",X7S_XML_ANYID,"",X7STYPETAG_STRING));
	m_params.Insert(X7sParam("draw_fg",X7S_XML_ANYID,0,X7STYPETAG_INT,-1,255));
	m_params.Insert(X7sParam("pixelate",X7S_XML_ANYID,0,X7STYPETAG_INT,-1,50));
	m_params.Insert(X7sParam("draw_blob",X7S_XML_ANYID,true,X7STYPETAG_BOOL));
	m_params.Insert(X7sParam("mjpeg_write",X7S_XML_ANYID,true,X7STYPETAG_BOOL));
	m_params.Insert(X7sParam("mjpeg_restart",X7S_XML_ANYID,30,X7STYPETAG_INT,1,500));
	m_params.Insert(X7sParam("mjpeg_rootdir",X7S_XML_ANYID,"",X7STYPETAG_STRING));

	//Parameters of the camera to send.
	m_params.Insert(X7sParam("enable",S7P_PRM_CAM_ENABLE,true,X7STYPETAG_BOOL));
	m_params.Insert(X7sParam("bg_threshold",S7P_PRM_CAM_BG_THRS,30,X7STYPETAG_U32,5,255));
	m_params.Insert(X7sParam("bg_insDelayLog",S7P_PRM_CAM_BG_INSERT,8,X7STYPETAG_U32,1,8));
	m_params.Insert(X7sParam("bg_refFGDelay",S7P_PRM_CAM_BG_REFRESH,256,X7STYPETAG_U32,1,1024));
	m_params.Insert(X7sParam("bg_refBGDelay",S7P_PRM_CAM_FG_REFRESH,8,X7STYPETAG_U32,1,1024));
	m_params.Insert(X7sParam("bg_ccMinArea",S7P_PRM_CAM_CC_MINAREA,64,X7STYPETAG_U32));
	m_params.Insert(X7sParam("bg_tmp1",S7P_PRM_CAM_TMP1,8,X7STYPETAG_U32));
	m_params.Insert(X7sParam("bg_tmp2",S7P_PRM_CAM_TMP2,8,X7STYPETAG_U32));
	m_params.Insert(X7sParam("bg_tmp3",S7P_PRM_CAM_TMP3,8,X7STYPETAG_U32));


	//Setup the step.
	m_params["pixelate"]->SetStep(5);
	m_params["bg_threshold"]->SetStep(10);
	m_params["bg_insDelayLog"]->SetStep(10);
	m_params["bg_refFGDelay"]->SetStep(10);
	m_params["bg_refBGDelay"]->SetStep(10);
	m_params["bg_ccMinArea"]->SetStep(5);
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------


bool Camera::ProcessNetFrame(X7sNetFrame *netFrame)
{
	X7S_FUNCNAME("Camera::ProcessNetFrame");

	bool fulldata=false;
	int ret=X7S_RET_ERR;

	try {

		if(netFrame)
		{



			metaJpegFrame.id=netFrame->counter;
			time(&(metaJpegFrame.time));
			metaJpegFrame.pJpeg=netFrame->data;
			CvSize sizeFrame=cvSize(netFrame->width,netFrame->height);
			serverMode=true;

			//Compute stats.
			stats->receivedFrame(metaJpegFrame.id);

			//Decompress the JPEG image
			if(DecompressJPEG()) sizeFrame=cvGetSize(imCvFrame);

			//Obtain the FGMask
			if(netFrame->mdtype!=S7P_MTYPE_NONE)
			{
				IplImage lrle=x7sByteArrayToIplImage(netFrame->mdata);
				lrle.width=sizeFrame.width;
				lrle.height=sizeFrame.height;
				ret = x7sCvtColorRealloc(&lrle,&imCvFGMask,X7S_CV_LRLE2GREY);
				X7S_CV_SET_CMODEL(imCvFGMask,X7S_CV_HNZ2BGRLEGEND);	// Important for vidsurv->Process().
			}
			else ret=X7S_RET_ERR;

			if(ret==X7S_RET_OK && vidsurv)
			{

				//Process the tracking.
				vidsurv->Process(imCvFrame,imCvFGMask);

			}


			//Ask if we need to save full data or not.
			fulldata = (metaJpegFrame.id%(m_params["mjpeg_restart"]->toIntValue())==0);


			//Obtain blob position/trajectory data
			X7sBlobTrajGenerator *trajGen = vidsurv->GetTrajGenerator();
			if(trajGen==NULL) metaJpegFrame.pBlobs=NULL;
			else metaJpegFrame.pBlobs = (X7sByteArray*)trajGen->GetData(fulldata);

			//Obtain alarms event and counter data
			X7sVSAlrManager *pAlrMan = vidsurv->GetAlarmManager();
			if(pAlrMan==NULL) metaJpegFrame.pAlarm = NULL;
			else metaJpegFrame.pAlarm = (X7sByteArray*)pAlrMan->GetData(fulldata);

			//Check if we have in this frame some events
			bool hasEvent;
			if(pAlrMan && pAlrMan->HasEvent()) hasEvent=true;
			else hasEvent=false;

			//Then set the tag for the frame
			X7S_CV_TAG_RST(metaJpegFrame.tag);
			X7S_CV_TAG_SET(metaJpegFrame.tag,X7S_CV_MJPEG_FRAMETAG_REFFR,fulldata);
			X7S_CV_TAG_SET(metaJpegFrame.tag,X7S_CV_MJPEG_FRAMETAG_ALARM,hasEvent);

			if(X7S_CV_TAG_IS(metaJpegFrame.tag,X7S_CV_MJPEG_FRAMETAG_REFALR))
			{
				AVO_PRINT_INFO("Camera::ProcessNetFrame()","tag is REFALR (0x%x)",metaJpegFrame.tag);
			}


			//Fill the JPEG Data
			if(metaJpegData==NULL) {
				metaJpegData=x7sCreateByteArray(X7S_CV_MJPEG_MAXFRAMESIZE);
			}
			x7sFillMJPEG(&metaJpegFrame,metaJpegData);

			//Write the data in the database (And only perform update each 100 frames)
			this->GenerateCamEvt((metaJpegFrame.id%100)==0);

			//Pixelate the image to keep the privacy.
			this->Pixelate();

			//Draw the alarm and the blob on the image.
			this->DrawMDataImage(sizeFrame);



			return true;
		}
	}
	catch ( const std::exception & e )
	{
		avoError(funcName) << e.what();
	}
	catch ( ... ) // traite toutes les autres exceptions
	{
		avoError(funcName) << "Unknown Error";
	}
	return false;
}


bool Camera::ReadFromXML(const TiXmlNode *cnode)
{
	return X7sXmlObject::ReadFromXML(cnode);
}

/**
* Check if the type is the same and reload the default value.
*
* @param childNode The
* @param childObj Point on vidsurv object.
* @return
*/
X7sXmlObject* Camera::ChildTypeReadFromXML(const TiXmlElement* childNode, X7sXmlObject *childObj)
{
	X7S_FUNCNAME("Camera::ChildTypeReadFromXML()");

	AVO_CHECK_WARN(funcName,childNode,false,"child is NULL");

	if(X7S_XML_ISEQNODENAME(childNode,X7S_VS_TAGNAME_PIPELINE))
	{
		X7sVidSurvPipeline *vsPipeline = x7sCreateVidSurvPipeline(this,childNode);
		if(vsPipeline) {
			if(vidsurv) delete vidsurv;
			vidsurv=vsPipeline;
		}
		return vidsurv;
	}
	else {
		AVO_PRINT_WARN(funcName,"child name is not %s",X7S_VS_TAGNAME_PIPELINE);
	}
	return NULL;
}


int Camera::GetSpecialID() const {
	if(board) { return (board->GetID()+1)*100+(this->GetID()+1); }
	else return this->GetID()+1;
}


const QColor& Camera::GetColor() const {
	int camera_index = this->GetID() + 4* (board->GetID()) % sizeof(cam_color);
	return cam_color[camera_index];
}


QString Camera::GetName() const {
	Camera *tmp = (Camera*)this;
	QString str = QtBind::toQString(tmp->GetParam("name"));
	if(str.isEmpty())
	{
		QTextStream txts(&str);
		txts << "Cam " << this->GetSpecialID();
	}
	return str;
}



Devices* Camera::GetDevices() const {
	return board?board->GetDevices():NULL;
}


QString Camera::GetStats() const
{
	QString txt;

	QDateTime lastTime;
	float lost = (float)stats->getLostNum(true,&lastTime);
	float elapsedMin = (float)lastTime.secsTo(QDateTime::currentDateTime())/60.0;

	txt = QString("Cam %1: FPS %2 (%3,%4) LPM %5")
											.arg(GetSpecialID())
											.arg(stats->getFPS()).arg(stats->getFPSAvgM()).arg(stats->getFPSAvgH())
											.arg(lost/elapsedMin);


	return txt;

}


/**
* @brief Get the video root dir of the camera.
*
* This directory can be set using the general devices parameters
* or using the camera parameter.
*
*/
QString Camera::GetVideoRootDir() const {
	//Obtain the root directory
	QString rootDir;
	Q_CHECK_PTR(board);
	Devices *dev=board->GetDevices();
	Q_CHECK_PTR(dev);
	Camera *cam = (Camera*)this;

	QRegExp rx("\\s*");

	if(dev->GetParam("video_path_isgeneral")->IsOk()==false)
	{
		rootDir=QtBind::toQString(cam->GetParam("mjpeg_rootdir"));
	}
	if(rootDir.isEmpty() || rx.exactMatch(rootDir))   rootDir= QtBind::toQString(dev->GetParam("video_path"));
	if(rx.exactMatch(rootDir)) rootDir=".";

	return rootDir;
}

/**
* @brief Tell if the frame of this camera need to be written in a video file or not.
* @see VideoHandler::WriteFrame()
*/
bool Camera::IsToWrite() const {
	bool ret;
	Q_CHECK_PTR(board);
	Devices *dev=board->GetDevices();
	Q_CHECK_PTR(dev);
	Camera *cam = (Camera*)this;

	ret = dev->GetParam("video_istowrite")->IsOk();
	if(ret==false)
	{
		ret = cam->GetParam("mjpeg_write")->IsOk();
	}
	return ret;
}


void Camera::resetVideo(bool enable) {
	if(enable)
	{
		if(isRecording) 	m_isResetVideo=true;
	}
	else m_isResetVideo=false;
}


/**
* @brief Obtain a MJPEG frame in a binary array.
*
* This array is not a JPEG image because it contains the MJPEG separator header
* @code
--myboundary
Content-Type: image/jpeg
Content-Length: 19667
JFIF ....
@endcode
*/
const X7sByteArray * Camera::GetMetaJPEGFrame() const {
	return metaJpegData;
}






/**
* @brief Set the camera MetaJPEG frame by the client
* @param pData
* @return
*/
bool Camera::SetMetaJPEGFrame(const X7sByteArray* pData)
{
	X7S_FUNCNAME("Camera::SetMetaJPEGFrame");
	AVO_CHECK_ERROR(funcName,pData,false,"Cam %d: pData is NULL",this->GetSpecialID());

	int ret;
	if(metaJpegFrame.pJpeg==NULL)  metaJpegFrame.pJpeg  = x7sCreateByteArray(X7S_NET_RTP_MSIZEIB_JPG);
	if(metaJpegFrame.pBlobs==NULL) metaJpegFrame.pBlobs = x7sCreateByteArray(X7S_VS_BLOBTRAJ_DATA_SIZE);
	if(metaJpegFrame.pAlarm==NULL) metaJpegFrame.pAlarm = x7sCreateByteArray(X7S_VS_ALRMAN_DATA_SIZE);

	if(serverMode) {
		x7sReleaseByteArray(&metaJpegData);
		serverMode=false;
	}
	metaJpegData=(X7sByteArray*)pData;

	//Extract the byte data to a structure.
	ret=x7sExtractMJPEG(&metaJpegFrame,pData);
	if(ret!=X7S_RET_OK)
	{
		QString filename = QString("error_%1_%2").arg(this->GetSpecialID()).arg(QDateTime::currentDateTime().toString(Qt::ISODate));
		QFile tmpfile(filename);
		if(tmpfile.open(QIODevice::WriteOnly)) 	tmpfile.write((const char*)pData->data,pData->length);
		AVO_PRINT_WARN(funcName,"Cam %d: Could not extract MJPEG data", this->GetSpecialID());
		x7sExtractMJPEG(&metaJpegFrame,pData);
		return false;
	}

	stats->receivedFrame(metaJpegFrame.id);

	//Process the blob on client side
	X7sBlobTrajGenerator *trajGen = vidsurv->GetTrajGenerator();
	if(metaJpegFrame.pBlobs && trajGen)
	{
		trajGen->SetData(metaJpegFrame.pBlobs,metaJpegFrame.id);
	}

	//Process the alarm on client side.
	X7sVSAlrManager *pAlrMan = vidsurv->GetAlarmManager();
	if(metaJpegFrame.pAlarm && pAlrMan)
	{
		pAlrMan->SetData(metaJpegFrame.pAlarm,metaJpegFrame.id);
	}


	//Once all the data are set draw the image for QT.
	DecompressJPEG();
	Pixelate();
	if(imCvFrame) DrawMDataImage(cvGetSize(imCvFrame));

	//Write the data in the database (And only perform update each 100 frames)
	this->GenerateCamEvt((metaJpegFrame.id%100)==0);


	return true;
}


X7sVSAlrManager* Camera::GetAlarmManager() const {
	if(vidsurv) return vidsurv->GetAlarmManager();
	else return NULL;
}



/**
* @brief Update the parameters of a camera to the corresponding board.
* @return @true if all the parameters were send to the board correctly
*/
bool Camera::Update()
{
	X7sNetSession *pNet=(X7sNetSession*) board->GetNetSession();
	const X7sParam *pParam=NULL;
	Camera *cam=this;
	bool is_enable=false;
	bool is_ok=true;
	uint32_t val;
	int ret;

	pParam = cam->GetParam("enable");
	is_enable = pParam->toIntValue();
	pNet->SetParam(pParam->GetID(),(uint32_t)is_enable,cam->GetID());

	if(is_enable)
	{
		pParam=NULL;
		while((pParam=cam->GetNextParam(pParam))!=NULL)
		{
			int ID=pParam->GetID();
			if(ID!=X7S_XML_ANYID && ID!=X7S_XML_NULLID)
			{
				val=(uint32_t)pParam->toIntValue();
				ret=pNet->SetParam(ID,val,cam->GetID());
				is_ok &= (ret==X7S_NET_RET_OK);
			}
		}
	}
	return is_ok;
}


bool Camera::ChangeVidSurvType(int type)
{
	X7S_FUNCNAME("Camera::ChangeVidSurvType()");

	bool ret=false;
	if(vidsurv && vidsurv->GetType() != type && (
			type==X7S_VS_PIPELINE_FG ||
			type==X7S_VS_PIPELINE_FEATURES))
	{

		avoInfo(funcName) << type;

		//Obtain the vidsurv value
		const X7sParam *pParam = vidsurv->GetParam("enable");
		if(pParam->IsNull()) return ret;

		//Keep the old isEnable value
		int isEnable = pParam->toIntValue();

		//Keep the definition of alarms in a TiXmlNode.
		TiXmlElement cnode("tmp");
		X7sVSAlrManager *alrMgr=vidsurv->GetAlarmManager();
		if(alrMgr)
		{
			cnode = TiXmlElement(alrMgr->GetTagName());
			alrMgr->WriteToXML((TiXmlElement*)&cnode,true);
		}


		//Locker when enter this function
		QMutexLocker locker(&m_mutex);

		//Delete the old x7svidsurv
		X7S_DELETE_PTR(vidsurv);

		//Create the new vidsurv
		vidsurv=x7sCreateVidSurvPipeline(this,type);
		if(vidsurv==NULL) avoFatal(funcName) << "Could not create VidSurvPipeline";

		//Set enable or disable according to the old value.
		vidsurv->SetParam("enable","%d",isEnable);

		vidsurv->GetAlarmManager()->ReadFromXML((const TiXmlElement*)&cnode);



		ret=true;
	}
	return ret;
}



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------



bool Camera::DecompressJPEG()
{
	int type=-1;
	if(vidsurv) type=vidsurv->GetType();


	if(isDisplayed==false && type != X7S_VS_PIPELINE_FEATURES) return false;

	//Check that we have at lease jpeg data.
	if(metaJpegFrame.pJpeg==NULL)
	{
		return false;
	}

	IplImage jpeg=x7sByteArrayToIplImage(metaJpegFrame.pJpeg);

	//Transform JPEG memory buffer to QT image.
	int ret = x7sCvtColorRealloc(&jpeg,&imCvFrame,X7S_CV_JPEG2BGRA);
	if(ret==X7S_CV_RET_ERR)
	{
		AVO_PRINT_ERROR("MjpegHandler::ReadFrame()", "Could not decode image %d",metaJpegFrame.id);
		return false;
	}
	imQtFrame = QtBind::toQImage(imCvFrame,QImage::Format_ARGB32);
	return true;
}

bool Camera::DrawMDataImage(const CvSize& size)
{
	if(isDisplayed==false) return false;

	if(vidsurv->GetParam("enable")->toIntValue()==false)
	{
		if(!imQtMData.isNull()) imQtMData.fill(0x00000000);
	}
	//Then draw the MetaData image.
	else if(metaJpegFrame.pBlobs || metaJpegFrame.pAlarm)
	{
		//Create and/or fill the MData image.
		if(imCvMData==NULL || X7S_ARE_SIZESEQ(size,cvGetSize(imCvMData))==false) {
			if(imCvMData) cvReleaseImage(&imCvMData);
			imCvMData = cvCreateImage(size,IPL_DEPTH_8U,4);
			imQtMData = QtBind::toQImage(imCvMData,QImage::Format_ARGB32);
		}

		imQtMData.fill(0x00000000);

		//Draw the trajectory on the metaData image
		X7sBlobTrajGenerator *trajGen = vidsurv->GetTrajGenerator();
		if(metaJpegFrame.pBlobs && trajGen &&
				this->GetParam("draw_blob")->toIntValue()==true)
		{
			trajGen->Draw(imCvMData);
		}

		//Draw the alarm on the metaData image
		X7sVSAlrManager *pAlrMan = vidsurv->GetAlarmManager();
		if(metaJpegFrame.pAlarm && pAlrMan)
		{
			pAlrMan->Draw(imCvMData);
		}
		return true;
	}
	return false;
}


bool Camera::Pixelate()
{
	if(isDisplayed==false || vidsurv->GetParam("enable")->IsOk()==false) return false;

	bool ok;
	int pix_size=GetParam("pixelate")->toIntValue(&ok);
	if(ok && pix_size>=5)
	{
		X7sBlobTrajGenerator *trajGen = vidsurv->GetTrajGenerator();
		if(trajGen && trajGen->GetBlobTrajNum()>0)
		{
			IplImage *imCvMask = cvCreateImage(cvGetSize(imCvFrame),IPL_DEPTH_8U,1);
			cvSet(imCvMask,cvScalarAll(0));
			trajGen->Draw(imCvMask,1,255,false,CV_FILLED);
			int ret =x7sPixelate(imCvFrame,imCvFrame,imCvMask,pix_size,pix_size);
			cvReleaseImage(&imCvMask);
			return (ret==X7S_CV_RET_OK);
		}
	}
	return false;
}


bool Camera::GenerateCamEvt(bool isUpdate, bool isClosing)
{
	bool ret=true;

	camEvtList.clear();	//!< Clear old list of event

	//Write the activity.
	X7sBlobTrajGenerator *trajGen = vidsurv->GetTrajGenerator();
	if(trajGen)
	{

		bool isActiveNow = (trajGen->GetBlobTrajNum()>0);
		if(isActiveNow != hasActivity) { //If new value different from old value.
			hasActivity=isActiveNow;
			CamEvt::State state;
			if(isActiveNow) state=CamEvt::Start;
			else state=CamEvt::Finish;
			camEvtList.append(CamEvtCarrier(new CamEvtActivity(this,GetTime(),state)));
		}
		else if(isUpdate && hasActivity)
		{
			CamEvt::State state;
			if(isClosing) state=CamEvt::Finish;
			else state=CamEvt::Update;
			camEvtList.append(CamEvtCarrier(new CamEvtActivity(this,GetTime(),state)));
		}
	}



	//Write the alarm
	X7sVSAlrManager *pAlrMan = vidsurv->GetAlarmManager();
	if(pAlrMan) {
		vector<X7sVSAlarmEvt> alrEvts = pAlrMan->GetAlarmEvents();
		if(alrEvts.size() > 0) {
			for(size_t i=0;i<alrEvts.size();i++) {
				camEvtList.append(CamEvtCarrier(new CamEvtVSAlarm(this,GetTime(),alrEvts[i])));
			}
		}
	}
	return ret;
}
