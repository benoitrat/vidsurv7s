#include "VideoFile.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
#include "Camera.hpp"
#include "Log.hpp"


VideoFile::VideoFile(VideoFile::Type type)
:m_type(type), m_pRef(new int(1)), m_file(NULL)
 {
	this->ResetVariable();
 }

VideoFile::VideoFile(const VideoFile &copy)
:m_type(copy.m_type), m_pRef(copy.m_pRef),
m_isEmpty(copy.m_isEmpty), m_cam(copy.m_cam),
m_fileInfo(copy.m_fileInfo), m_lastSize(copy.m_lastSize), m_file(copy.m_file)
 {
	if(!copy.m_tStart.isNull()) m_tStart=copy.m_tStart;
	if(!copy.m_tLast.isNull()) m_tLast=copy.m_tLast;

	(*m_pRef)++;
	avoDump("VideoFile::VideoFile(const VideoFile &copy)") << "ref=" << *m_pRef << getFilename();
	if(copy.isOpened()) avoWarning("VideoFile::VideoFile(const VideoFile &copy)") << "copy.isOpened() == true";
 }

VideoFile& VideoFile::operator=(const VideoFile& copy)
{
	this->Release();
	m_type=copy.m_type;
	m_pRef = copy.m_pRef;
	m_isEmpty = copy.m_isEmpty;
	m_cam = copy.m_cam;
	m_tStart = copy.m_tStart;
	m_tLast = copy.m_tLast;
	m_fileInfo = copy.m_fileInfo;
	m_lastSize = copy.m_lastSize;
	m_file = copy.m_file;
	(*m_pRef)++;
	avoDump("VideoFile: operator=()") << "ref=" << *m_pRef << getFilename();
	if(copy.isOpened()) avoWarning("VideoFile::VideoFile(const VideoFile &copy)") << "copy.isOpened() == true";
	return *this;
}


bool VideoFile::Release()
{
	if((--(*m_pRef)) == 0) {
		avoDump("VideoFile::Release()") << true << getFilename();
		X7S_DELETE_PTR(m_pRef);
		return this->Close();
	}
	else avoDump("VideoFile::Release()") << false << *m_pRef << getFilename();
	return false;
}

//! Method to close the file.
bool VideoFile::Close()
{
	int ret=X7S_RET_OK;
	if(m_file)
	{
		m_lastSize=getSize();
		ret=x7sReleaseFILE(&m_file);
		avoDebug("VideoFile::Close()") << "ret="<< ret<< "(" << m_lastSize/(float)X7S_NBYTES_MIB << "MiB )" << getFilename();
	}
	return (ret==X7S_RET_OK);
}


//! return @true if the file is opened.
bool VideoFile::isOpened() const
		{
	return (isValid() && m_file != NULL);
		}

//! return @true if the file was closed.
bool VideoFile::isClosed() const
		{
	return ( isValid() && m_file==NULL);
		}

int VideoFile::getCamID() const
		{
	if(m_cam) return m_cam->GetSpecialID();
	else return -1;
		}

bool VideoFile::hasAlrEvent(const AVOAlrEvt& alrEvt)  const 	{
	if(isValid() && m_tStart.isValid() && m_tLast.isValid())
	{
		QDateTime alrTime = QDateTime::fromTime_t(alrEvt.t);
		return (alrEvt.cam==m_cam && m_tStart <= alrTime && alrTime <= m_tLast);
	}
	else
	{
		avoDebug("VideoFile::hasAlrEvent()") << "Not valid" << isValid() << m_tStart << m_tLast;
	}
	return false;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------


void VideoFile::ResetVariable()
{
	m_isEmpty=true;
	m_cam=NULL;
	m_lastSize=-1;
	m_tStart=QDateTime();
	m_tLast=QDateTime();
	m_fileInfo=QFileInfo();
	this->Close(); //Call close method on implemented class.

}
