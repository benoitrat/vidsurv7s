/**
*  @file
*  @brief Contains the class QCamera.hpp
*  @date Nov 25, 2009
*  @author neub
*	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
*/

#ifndef QCAMERA_HPP_
#define QCAMERA_HPP_

#include <QWidget>
#include <QPixmap>
#include <QMenu>
#include <QAction>
#include <QMouseEvent>
#include <QColor>

#include <x7svidsurv.h>
#include "Core.hpp"

class Camera;

static QColor line_qcolor_pen[] = { Qt::red, Qt::green, Qt::blue, Qt::cyan, Qt::magenta, Qt::yellow,
		Qt::gray, Qt::lightGray, Qt::white, Qt::black, };
#define LINE_COLOR_PEN_SIZE 10
static QColor polygon_qcolor_pen[] = { Qt::darkYellow, Qt::darkMagenta,
		Qt::darkCyan, Qt::darkBlue, Qt::darkGreen, Qt::darkRed, Qt::darkGray, Qt::white, Qt::black,};
#define POLYGON_COLOR_PEN_SIZE 9
/**
*	@brief The QCamera.
*	@ingroup avo_widget
*/
class QCamera: public QWidget {

	Q_OBJECT

public:
	QCamera(QWidget *parent, Camera *cam, int index);
	virtual ~QCamera();

    Camera * getCamera() const {return camera; };
	const QString& getName() const { return name; };
	bool isEnable() const {return enabled; };
    bool IsDrawable() const { return drawable; };
    int GetCameraIndex(){return camera_index;};

	signals:
	void cameraEnabled(bool enable, int index);
	void cameraVisible(bool enable, int index);
	void warningOnCamera(const QString &warning);
	void notificationOnCamera(const QString& msg);
    void newFrameReceived(Camera *cam);

public slots:
	void setName(const QString &name_pt){name = name_pt;};
    virtual void setDrawable(bool enable)=0;	//Must be defined in inherited class

protected:
    static QColor getAlarmLinePenQColor(unsigned int index)
    {
    	return line_qcolor_pen[index%LINE_COLOR_PEN_SIZE];
    };
    static QColor getAlarmPolyPenQColor(unsigned int index)
    {
    	return polygon_qcolor_pen[index%POLYGON_COLOR_PEN_SIZE];
    };


public:
	QImage 	image_v;	// used in setClientCamerasParams or setCamarasParams.
	QImage 	image_d;

protected:
	Camera *camera;
	int camera_index;

	bool 	enabled;
    bool 	drawable;
    bool 	surveillanceEnabled;
    bool 	addingAlarm;
    bool 	enableTimeOut;


	QString name;
	QImage *noAvailableImage;

	QMenu *menu;
	QAction *addLineAction;
	QAction *delLinesAction;
	QAction *delLastLineAction;
	QAction *addPolygonAction;
	QAction *delPolygonsAction;
	QAction *delLastPolygonAction;
	QAction *applyAction;
	QAction *cancelAction;
	QAction *exitAction;

    bool		addpolygon;
    bool 	addline;
    bool 	init_line;
    bool 	drawpolygon;

};

#endif /* QCAMERA_HPP_ */
