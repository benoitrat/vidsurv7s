#include <XmlObjTreeConfWidget.h>
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include <QVBoxLayout>
#include <QFormLayout>
#include <QTextStream>
#include <QLabel>

#include "QtBind.hpp"
#include "Log.hpp"
#include "XmlObjConfigWidget.h"

#define XMLOBJITEMROLE Qt::UserRole

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
XmlObjTreeConfWidget::XmlObjTreeConfWidget(X7sXmlObject *xmlObj, QWidget *parent)
:TreeConfigWidget(parent), m_xmlObj(xmlObj)
 {

	treeWgt = m_ui.treeWidget;
	treeWgt->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

//	lyt->addWidget(createNodeWidget(m_xmlObj));

	this->addXmlObj(m_xmlObj,NULL,NULL);
	m_ui.stackedWidget->setCurrentIndex(3);

	connect(treeWgt, SIGNAL(itemClicked(QTreeWidgetItem*, int )), this, SLOT(showItem(QTreeWidgetItem*, int)));

 }

XmlObjTreeConfWidget::~XmlObjTreeConfWidget() {
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

bool XmlObjTreeConfWidget::showItem(QTreeWidgetItem* item,int column)
{
	bool ret=false;
	int index = item->data(0,XMLOBJITEMROLE).toInt(&ret);
	avoDebug("XmlObjTreeConfWidget::showItem()") << item->text(column) <<  index;
	if(ret) m_ui.stackedWidget->setCurrentIndex(index);
	return ret;
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

QTreeWidgetItem* XmlObjTreeConfWidget::addXmlObj(X7sXmlObject *node, QTreeWidgetItem* item, QTreeWidgetItem *after, int max_level)
{
		if(max_level>0) max_level--;
		else return NULL;

	if(node)
	{


		QTreeWidgetItem *treeItem=NULL;
		if(item==NULL)
		{
			treeItem =  new QTreeWidgetItem(treeWgt);
			treeItem->setExpanded(true);
			treeWgt->insertTopLevelItem(0,treeItem);
		}
		else
		{
			treeItem = new QTreeWidgetItem(item,after);
		}

		XmlObjConfigWidget *xmlObjConf = new XmlObjConfigWidget(node);
		connect(this,SIGNAL(applyAllClicked()),xmlObjConf,SLOT(clickOnApply()));	//FIXME: Resolve the click and the last change
		connect(this,SIGNAL(refreshAllClicked()),xmlObjConf,SLOT(reloadAutoParamWgt()));
		int index = m_ui.stackedWidget->addWidget(xmlObjConf);
		treeItem->setData(0,XMLOBJITEMROLE,QVariant(index));


		//-------- Set the node name
		QString nodeName;
		QTextStream txtStream(&nodeName);
		txtStream << node->GetTagName();
		if(node->GetID()!=X7S_XML_ANYID) txtStream<< " #" << node->GetID();
		treeItem->setText(0,nodeName);

		//-------- Recursive call

		X7sXmlObject *childObj=NULL;
		QTreeWidgetItem *childItem=NULL;

		while( (childObj=node->GetNextChild(childObj)) !=NULL)
		{
			childItem = addXmlObj(childObj,treeItem,childItem);
		}



		return treeItem;
	}

	return false;
}
