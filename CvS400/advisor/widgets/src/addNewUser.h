#ifndef ADDNEWUSER_H
#define ADDNEWUSER_H

#include <QtGui/QWidget>
#include <QList>
#include <QString>
#include "ui_addNewUser.h"

namespace Ui {
    class addNewUser;
}

class addNewUser : public QWidget {
    Q_OBJECT
    Q_DISABLE_COPY(addNewUser)
public:
    explicit addNewUser(QWidget *parent = 0, QList<QString> *user_pt = 0);
    virtual ~addNewUser();
signals:
	void newUserAdded();
protected:
    virtual void changeEvent(QEvent *e);
protected slots:
    void CancelAndClose();
    void ApplyNewValues();
    void EnableOK_pushButton();

private:
    Ui::addNewUser *m_ui;
    QList<QString> *user;
};

#endif // ADDNEWUSER_H
