#include "WaitingDialog.h"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include <qmath.h>
#include "Log.hpp"

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

WaitingDialog::WaitingDialog(QWidget *parent,const QString& title, double timeout, int numTries)
:QDialog(parent), m_ui(new Ui::WaitingDialog),
m_timeout(timeout),m_numTries(numTries), m_actTry(1)
{
    m_ui->setupUi(this);
    this->setWindowTitle(title);
    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}


WaitingDialog::~WaitingDialog()
{
    delete m_ui;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------
void WaitingDialog::setText(const QString& text)
{
	if(m_ui) m_ui->mainLabel->setText(text);
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Slots
//----------------------------------------------------------------------------------------

int WaitingDialog::exec()
{
    if(m_numTries<0) m_ui->progressLabel->setVisible(false);
    m_ui->progressLabel->setText(tr("try %1/%2").arg(m_actTry).arg(m_numTries));

    //Try one time at least without time checking
    if(trying()) return QDialog::Accepted;
    m_actTry++;

    //Setup the timer for the next try
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(calling()));
    connect(this,SIGNAL(finished(int)),timer,SLOT(stop()));
    connect(this,SIGNAL(finished(int)),this,SLOT(test()));
    int timeout = qCeil(m_timeout*1000.0);
    timer->start(timeout);

    //Call the exec function of the dialog
    return QDialog::exec();
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected Methods
//----------------------------------------------------------------------------------------
void WaitingDialog::calling()
{
	X7S_FUNCNAME("WaitingDialog::calling");
	avoDebug(funcName) << m_actTry << "/" << m_numTries;


	m_ui->progressLabel->setText(tr("try %1/%2").arg(m_actTry).arg(m_numTries));
	bool ret = trying();	//Call virtual function
	if(ret)
	{
		this->done((int)QDialog::Accepted);
	}
	else
	{
		m_actTry++;
	}

	if(m_numTries>0 && m_actTry>m_numTries)
	{
		avoWarning(funcName) << "Maximum number of try reached";
		this->done((int)QDialog::Rejected);
	}
}

