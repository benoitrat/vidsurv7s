#ifndef WARNINGWIDGET_H
#define WARNINGWIDGET_H

#include <QtGui/QDialog>

namespace Ui {
    class WarningWidget;
}

class WarningWidget : public QDialog {
    Q_OBJECT
    Q_DISABLE_COPY(WarningWidget)
public:
    explicit WarningWidget(QWidget *parent = 0, int nMaxItems=50);
    virtual ~WarningWidget();
public slots:
    void AddWarning(const QString & warning);

protected slots:
	virtual void accept();

protected:
    virtual void changeEvent(QEvent *e);

private:
    Ui::WarningWidget *m_ui;
    int nMaxItems;
};

#endif // WARNINGWIDGET_H
