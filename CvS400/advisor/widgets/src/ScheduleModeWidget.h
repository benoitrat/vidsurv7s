/**
 *  @file
 *  @brief Contains the class ScheduleModeWidget.h
 *  @date Nov 27, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef SCHEDULEMODEWIDGET_H_
#define SCHEDULEMODEWIDGET_H_

#include <QtGui>
#include <QWidget>
#include <QPushButton>

class ScheduleModeWidget;

class ScheduleButton: public QPushButton
{
	Q_OBJECT

public:
	ScheduleButton(ScheduleModeWidget *parent,	int dayOfWeek, int hour, int mode,int modeMin, int modeMax);
	virtual ~ScheduleButton() {};

	bool setMode(int mode);

	int dayOfWeek() const { return m_dayOfWeek; }
	int hour() const { return m_hour; }
	int mode() const { return m_mode; }

	signals:
	void clicked(int dayOfWeek, int hour, int mode);

private:
	void updateState();

	private slots:
	void clickReceived();

private:
	ScheduleModeWidget *m_parent;
	QString m_toolTip;
	int m_dayOfWeek;
	int m_hour;
	int m_mode;
	int m_modeMin;
	int m_modeMax;
};



/**
 *	@brief The ScheduleModeWidget.
 *	@ingroup avo_widget
 */
class ScheduleModeWidget : public QWidget {

	Q_OBJECT

public:
	ScheduleModeWidget(QWidget *parent, const QStringList& strList, int defMode=0);
	virtual ~ScheduleModeWidget();

	QColor getColor(int mode) const;
	const QString& getName(int mode) const { return m_legend.at(mode); }

	signals:
	void modeChanged(int dayOfWeek, int hour, int mode);

public slots:
	bool setMode(int dayOfWeek, int hour, int mode);

private slots:
	void clickOnButton(int dow, int hour, int mode) { emit modeChanged(dow,hour,mode); }
	void clickOnDay(int dOw);
	void clickOnHour(int hour);
	void clickOnMode(int mode);

private:
	ScheduleButton* getButton(int dOw, int hour) const;

private:
	QStringList m_legend;
	int m_defMode;
	ScheduleButton* m_butMx[7][24];
	QGridLayout *m_gridLayout;
};

#endif
