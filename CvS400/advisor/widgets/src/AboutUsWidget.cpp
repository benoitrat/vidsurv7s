/*
 * AboutUsWidget.cpp
 *
 *  Created on: 25/01/2010
 *      Author: Ben
 */

#include "AboutUsWidget.h"

#include <cv.h>
#define TIXML_USE_STL
#include <tinyxml.h>
#include <x7sver.h>

#include "avover.h"


AboutUsWidget::AboutUsWidget(QWidget *parent):
QWidget(parent),  m_ui(new Ui::AboutUsWidget)
{
	m_ui->setupUi(this);

	m_ui->avo_version->setText(QString("Advisor %1.%2 build %3").arg((int)AVO_MAJOR_VERSION).arg((int)AVO_MINOR_VERSION).arg((int)AVO_PATCH_VERSION));
	m_ui->x7s_version->setText(X7S_VERSION);
	m_ui->cv_version->setText(CV_VERSION);
	m_ui->qt_version->setText(QT_VERSION_STR);
	m_ui->txml_version->setText(QString("%1.%2.%3").arg(TIXML_MAJOR_VERSION).arg(TIXML_MINOR_VERSION).arg(TIXML_PATCH_VERSION));

}

AboutUsWidget::~AboutUsWidget() {
}
