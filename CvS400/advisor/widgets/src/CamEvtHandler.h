/**
*  @file
*  @brief Contains the class CamEvtHandler.h
*  @date Nov 25, 2009
*  @author neub
*	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
*/

#ifndef CAMEVTHANDLER_H_
#define CAMEVTHANDLER_H_

#include <QObject>
#include "CamEvtCarrier.hpp"

class Camera;
class Database;
class VideoHandler;

/**
*	@brief The CamEvtHandler.
*	@ingroup avo_widget
*/
class CamEvtHandler: public QObject {

	Q_OBJECT

public:
	CamEvtHandler(Database *database=NULL, VideoHandler *vidHandler=NULL);
	virtual ~CamEvtHandler();

public slots:
	bool processNewFrame(Camera *cam);

signals:
	    void eventReceived(const CamEvtCarrier &camEvt);
	    void eventVSAlarmReceived(const CamEvtCarrier& camEvt);

private:
	Database *m_database;
	VideoHandler *m_vidHandler;
};

#endif /* CAMEVTHANDLER_H_ */
