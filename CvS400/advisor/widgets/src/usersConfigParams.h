#ifndef USERSCONFIGPARAMS_H
#define USERSCONFIGPARAMS_H

#include <QtGui/QDialog>

namespace Ui {
    class usersConfigParams;
}

class usersConfigParams : public QDialog {
    Q_OBJECT
    Q_DISABLE_COPY(usersConfigParams)
public:
    explicit usersConfigParams(QWidget *parent = 0, QList<QString> *user_pt = 0);
    virtual ~usersConfigParams();
signals:
    void userModifiedSignal();

protected slots:
	void EnableApply_pushButton();
	void ApplyNewValues();
	void UserChanged();
	void DeleteUser();

protected:
    virtual void changeEvent(QEvent *e);
//    void ChangeUserParams();
//    void Close();

private:
    Ui::usersConfigParams *m_ui;
    QList<QString> *user;
};

#endif // USERSCONFIGPARAMS_H
