/*
 * EventListWidget.h
 *
 *  Created on: 21/10/2009
 *      Author: Gabri
 */

#ifndef EVENTLISTWIDGET_H_
#define EVENTLISTWIDGET_H_



#include <QWidget>
#include <QListWidget>
#include <QDateTime>
#include <QString>
#include <QMouseEvent>
#include "CamEvtCarrier.hpp"


class EventListWidgetItem : public QListWidgetItem
{
public:
	enum ItemType { CamEventType=1001 };
	static bool isType(const QListWidgetItem* item) { return (item && item->type()==CamEventType); }

public:
	EventListWidgetItem(const CamEvtCarrier &camEvt,QListWidget * parent = 0);
	const CamEvtCarrier& getCameraEvt() const { return camEvt; }

private:
	CamEvtCarrier camEvt;
};





class EventListWidget : public QListWidget
{
    Q_OBJECT

public:
    EventListWidget(QWidget *parent = 0, int nMaxItems=1000);
    ~EventListWidget();
public slots:
	void AddEvent(const CamEvtCarrier &camEvt);
signals:
    void EventSelectedSignal(const CamEvtCarrier &camEvt, bool play);

protected slots:
	void clickOnEvent(QListWidgetItem * item,bool dblClick=false);
	void doubleClickOnEvent(QListWidgetItem * item) { clickOnEvent(item,true); }

private:
	int nMaxItems;
};

#endif /* EVENTLISTWIDGET_H_ */
