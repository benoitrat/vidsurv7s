/**
 *  @file
 *  @brief Contains the class ImageWidget.h
 *  @date Aug 31, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef IMAGEFRAME_H_
#define IMAGEFRAME_H_

#include <QtGui>
#include <QWidget>
#include <QImage>
#include <QVector>


/**
 *	@brief The ImageWidget.
 *	@ingroup avo_viewer
 */
class ImageWidget : public QWidget {

	Q_OBJECT
	Q_PROPERTY(bool keepRatio READ isKeepRatio WRITE setKeepRatio)
	Q_PROPERTY(bool resizeable READ isResizeable WRITE setResizeable)


public:
	ImageWidget(QWidget *parent=0, bool resizeable=false);
	virtual ~ImageWidget();
	void paintImage(const QImage& image,bool clear);
	void setResizeable(bool resizeable) { this->resizeable = resizeable; };
	bool isResizeable() { return this->resizeable; };
	void setKeepRatio(bool keepRatio) { this->keepRatio = keepRatio; };
	bool isKeepRatio() { return this->keepRatio; };
	void clear() { imgsVec.clear(); }

protected:
	virtual void paintEvent(QPaintEvent *event);

private:
    QVector<QImage> imgsVec;
    QPainter::CompositionMode mode;
    QSize maxSize;
    bool resizeable;
    bool keepRatio;

};

#endif /* IMAGEFRAME_H_ */
