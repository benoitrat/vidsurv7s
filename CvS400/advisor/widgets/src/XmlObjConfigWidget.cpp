#include <XmlObjConfigWidget.h>
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Log.hpp"
#include "QtBind.hpp"
#include <QRegExp>
#include <QLabel>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------

/**
* @brief Create a widget to configure a XML node.
*
* @note The widget are automatically place and used, however you can create an inherited class,
* that set the m_xmlObj to NULL.
*
* @param xmlObj
* @param parent
* @return
*/
XmlObjConfigWidget::XmlObjConfigWidget(X7sXmlObject *xmlObj,QWidget *parent)
:QWidget(parent), m_xmlObj(xmlObj)
 {

	//General parameters
	m_ui.setupUi(this);
	m_signalMapper = new QSignalMapper(this);
	connect(m_ui.buttonBox, SIGNAL(clicked(QAbstractButton *)), this, SLOT(buttonClick(QAbstractButton * )));
	connect(m_signalMapper,SIGNAL(mapped(QWidget*)),this,SLOT(paramWgtChange(QWidget*)));

	reloadAutoParamWgt();


 }


XmlObjConfigWidget::~XmlObjConfigWidget()
{

}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------
void XmlObjConfigWidget::clickOnApply()
{
	QPushButton *button =m_ui.buttonBox->button(QDialogButtonBox::Apply);
	if(button) button->click();
}

bool XmlObjConfigWidget::restoreDefault()
{
	X7S_FUNCNAME("XmlObjConfigWidget::restoreDefault()");
	avoInfo(funcName) << "Not Implemented";

	return false;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

bool XmlObjConfigWidget::applyParam()
{
	X7S_FUNCNAME("XmlObjConfigWidget::applyParam()");

	bool ret=false;
	AVO_CHECK_WARN(funcName,m_xmlObj,false,"m_xmlObj is NULL");

	avoDump(funcName) << m_hashTable.size();

	foreach(QString key, m_hashTable.uniqueKeys())
	{
		QString val = m_hashTable.value(key);
		m_xmlObj->SetParam(key.toStdString().c_str(),val.toStdString());
		avoDebug(funcName) << "value " << key << "=" << val;
	}

	if(m_hashTable.size()>0)
	{
		m_hashTable.clear();
		ret=m_xmlObj->Apply(0);
	}

	return ret;
}


void XmlObjConfigWidget::clearAutoParamWgt()
{
	X7S_FUNCNAME("XmlObjConfigWidget::clearAutoParamWgt()");
	avoDump(funcName);

	//First clear the has table
	m_hashTable.clear();

	//Take the automatic layout.
	QLayout *layout=m_ui.paramBoxLyt;

	if(layout==NULL) return;

	//Then clear the each item
	QLayoutItem *child;
	while((child = layout->itemAt(0)) != 0)
	{
		QWidget *wgt =child->widget();
		if(wgt)
		{
			layout->removeWidget(wgt);
			wgt->deleteLater();
		}
	}
}


void XmlObjConfigWidget::reloadAutoParamWgt()
{
	X7S_FUNCNAME("XmlObjConfigWidget::reloadAutoParamWgt()");
	//first delete the old value
	this->clearAutoParamWgt();
	avoDump(funcName);



	//Then create the widget
	if(m_xmlObj)
	{
		//Setup title
		QString tagName=m_xmlObj->GetTagName();
		tagName.replace("_"," ");
		tagName.replace(0,1,tagName.at(0).toUpper());
		m_ui.title->setText(tagName);

		//Add empty comment
		m_ui.comment->setText("");

		//Add arguments
		m_ui.idLineEdit->setText(QString("%1").arg(m_xmlObj->GetID()));
		m_ui.typeLineEdit->setText(QString("%1").arg(m_xmlObj->GetType()));

		//Add all the parameters
		const X7sParam* pParam=NULL;
		while((pParam=m_xmlObj->GetNextParam(pParam)) != NULL)
		{
			addAutoParamWgt((X7sParam*)pParam);
		}

	}
}


void XmlObjConfigWidget::buttonClick(QAbstractButton *button)
{
	X7S_FUNCNAME("TreeConfigWidget::buttonClick()");

	switch(m_ui.buttonBox->standardButton(button))
	{
	case QDialogButtonBox::Apply: this->applyParam(); break;
	case QDialogButtonBox::RestoreDefaults:	this->restoreDefault();	break;
	default: 	avoDebug(funcName) << "Unknow button"; break;
	}
}

void XmlObjConfigWidget::paramWgtChange(QWidget *valWgt)
{
	X7S_FUNCNAME("XmlObjConfigWidget::paramWgtChange()");
	avoWarning(funcName);
	if(valWgt)
	{
		QString key, val;
		key = valWgt->accessibleName();

		if(valWgt->inherits("QLineEdit"))
		{
			QLineEdit *lineEdit = static_cast<QLineEdit *>(valWgt);
			val = lineEdit->text();
		}
		else if(valWgt->inherits("QDoubleSpinBox"))
		{
			QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox *>(valWgt);
			val = spinBox->text();
		}
		else if(valWgt->inherits("QSpinBox"))
		{
			QSpinBox *spinBox = static_cast<QSpinBox *>(valWgt);
			val = spinBox->text();
		}
		else if(valWgt->inherits("QCheckBox"))
		{
			QCheckBox *checkBox = static_cast<QCheckBox *>(valWgt);
			if(checkBox->isChecked()) val="1";
			else val ="0";
		}
		else if(valWgt->inherits("QComboBox"))
		{
			QComboBox*comboBox = static_cast<QComboBox *>(valWgt);
			val=QString("%1").arg(comboBox->currentIndex());
		}
		else 	return;

		avoDump(funcName) << "Add to hash table: " << key << "=>" << val;
		m_hashTable.insert(key,val);
	}
}


void XmlObjConfigWidget::addStrListParamWgt(X7sParam *pParam,const QStringList &list)
{
	X7S_FUNCNAME("XmlObjConfigWidget::addAutoParamWgt()");

	if(pParam)
	{
		QWidget *wgt = m_ui.paramGroupBox;
		QWidget *valWgt = NULL;

		if(pParam->IsInteger())
		{
			int val,min,max,step;
			pParam->GetValue(&val,&min,&max,&step);
			if(step==1 && min==0 && max==list.size()-1)
			{

				QComboBox *comboBox = new QComboBox(wgt);
				comboBox->addItems(list);
				comboBox->setCurrentIndex(val);
				connect(comboBox, SIGNAL(currentIndexChanged(int)),m_signalMapper, SLOT (map()));
				valWgt=comboBox;

				valWgt->setAccessibleName(pParam->GetName().c_str());
				m_ui.paramBoxLyt->addRow(pParam->GetName().c_str(),valWgt);
				m_signalMapper->setMapping(valWgt,valWgt);
			}
			else
			{
				AVO_PRINT_WARN(funcName,"pParam (%d,%d,%d) and stringList (%d) are incompatible ",min,max,step,list.size());
				return;
			}
		}
		else
		{
			AVO_PRINT_WARN(funcName,"pParam has unknow type %d",pParam->GetType());
			return;
		}
	}
}





void XmlObjConfigWidget::addAutoParamWgt(X7sParam *pParam)
{
	X7S_FUNCNAME("XmlObjConfigWidget::addAutoParamWgt()");

	if(pParam)
	{
		QWidget *wgt = m_ui.paramGroupBox;
		QWidget *valWgt = NULL;

		if(pParam->IsString())
		{
			QLineEdit* lineEdit= new QLineEdit(QtBind::toQString(pParam),wgt);
			connect(lineEdit,SIGNAL(editingFinished()),m_signalMapper, SLOT (map()));
			valWgt =lineEdit;
		}
		else if(pParam->IsReal())
		{
			double val,min,max,step;
			pParam->GetValue(&val,&min,&max,&step);
			QDoubleSpinBox *spinBox=new QDoubleSpinBox(wgt);

			spinBox->setValue(val);
			spinBox->setSingleStep(step);
			spinBox->setRange(min,max);
			valWgt = spinBox;
			connect(spinBox, SIGNAL(editingFinished()),m_signalMapper, SLOT (map()));



		}
		else if(pParam->IsInteger())
		{
			if(pParam->GetType()==X7STYPETAG_BOOL)
			{
				QCheckBox* checkBox = new QCheckBox(wgt);
				checkBox->setChecked(pParam->IsOk());
				connect(checkBox, SIGNAL(stateChanged(int)),m_signalMapper, SLOT (map()));
				valWgt=checkBox;
			}
			else
			{
				int val,min,max,step;
				pParam->GetValue(&val,&min,&max,&step);
				QSpinBox *spinBox=new QSpinBox(wgt);

				qDebug() << pParam->GetName().c_str() << val << min << max << step;

				spinBox->setRange(min,max);
				spinBox->setValue(val);
				spinBox->setSingleStep(step);
				valWgt =spinBox;

				connect(spinBox, SIGNAL(editingFinished()),m_signalMapper, SLOT (map()));
			}
		}
		else
		{
			AVO_PRINT_WARN(funcName,"pParam has unknow type %d",pParam->GetType());
			return;
		}
		valWgt->setToolTip(pParam->getComment().c_str());
		valWgt->setAccessibleName(pParam->GetName().c_str());
		QLabel *lblWgt = new QLabel(pParam->GetName().c_str(),this);
		lblWgt->setToolTip(pParam->getComment().c_str());
		m_ui.paramBoxLyt->addRow(lblWgt,valWgt);
		m_signalMapper->setMapping(valWgt,valWgt);
	}
}
