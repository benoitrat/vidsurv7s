#include <ScheduleModeWidget.h>
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Log.hpp"
#include "x7scv.h"

#ifdef WINDOWS
#define IAVO_QT_SETWINDOWSSTYLE(pWidget) pWidget->setStyle(new QWindowsStyle());
#else
#define IAVO_QT_SETWINDOWSSTYLE(pWidget)
#endif

#define IAVO_BUTTON_W 15
#define IAVO_BUTTON_H 20


//========================================================================çç

//------------------ Begin class ScheduleButton

ScheduleButton::ScheduleButton(ScheduleModeWidget *parent,	int dayOfWeek, int hour, int mode,int modeMin, int modeMax)
:QPushButton(parent), m_parent(parent), m_dayOfWeek(dayOfWeek), m_hour(hour),
 m_mode(mode), m_modeMin(modeMin), m_modeMax(modeMax)
 {
	this->setMinimumSize(IAVO_BUTTON_W,IAVO_BUTTON_H);
	this->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
	connect(this,SIGNAL(clicked()),this,SLOT(clickReceived()));

	m_toolTip.sprintf("%02dh-%02dh",hour,hour+1);
	m_toolTip=QDate::longDayName(dayOfWeek)+": "+m_toolTip;


	//this->setAutoFillBackground(true);
	//this->setFlat(enable);
	this->updateState();
	IAVO_QT_SETWINDOWSSTYLE(this);
 };


void ScheduleButton::clickReceived()
{
	Qt::KeyboardModifiers modifier = QApplication::keyboardModifiers();
	if(modifier==Qt::ShiftModifier) m_mode--;
	else m_mode++;
	if(m_mode>=m_modeMax) m_mode=m_modeMin;
	if(m_mode<m_modeMin) m_mode=(m_modeMax-1);


	this->updateState();

	avoDump("ScheduleButton::clickReceived()") << m_dayOfWeek << m_hour << "=>"<<m_mode << modifier;
	emit clicked(m_dayOfWeek,m_hour,m_mode);
}

void ScheduleButton::updateState()
{
	//Obtain a new color
	QColor qtCol = m_parent->getColor(m_mode);
	QString toolTip = m_toolTip + " ("+m_parent->getName(m_mode)+")";
	this->setToolTip(toolTip);

	//Set this new color
	QPalette pal;
	pal.setColor(QPalette::Base,qtCol);
	pal.setColor(QPalette::Background,qtCol);
	pal.setColor(QPalette::Button,qtCol);
	this->setPalette(pal);
	this->update();
}


bool ScheduleButton::setMode(int mode)
{
	if(X7S_CHECK_RANGE(mode,m_modeMin,m_modeMax))
	{
		m_mode=mode;
		this->updateState();
		return true;
	}
	return false;
}

//------------------ End class ScheduleButton

//========================================================================çç

//------------------ Begin class ScheduleModeWidget

ScheduleModeWidget::ScheduleModeWidget(QWidget *parent, const QStringList& strList, int defMode)
:QWidget(parent),  m_legend(strList), m_defMode(defMode)
 {

	QSignalMapper* signalMapMode = new QSignalMapper(this);
	QSignalMapper* signalMapDoW = new QSignalMapper(this);
	QSignalMapper* signalMapHour = new QSignalMapper(this);

	//Add a grid layout
	this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	QVBoxLayout *mainLayout = new QVBoxLayout(this);


	m_gridLayout=new QGridLayout();
	m_gridLayout->setSpacing(0);


	this->setMinimumSize(400,200);

	int minMode=0;
	int maxMode=strList.size();

	if(maxMode>minMode)
	{
		m_defMode=X7S_INRANGE(m_defMode,minMode,maxMode);

		for(int h=0;h<24;h++)
		{
			if(h%1==0)
			{
				QString hourStr;
				hourStr.sprintf("%02d",h);
				QPushButton *hourLabel = new QPushButton(hourStr,this);
				QFont font = hourLabel->font();
				font.setPointSize(6);
				hourLabel->setFont(font);
				hourLabel->setFlat(true);
				hourLabel->setMinimumSize(IAVO_BUTTON_W,IAVO_BUTTON_H);
				hourLabel->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
				hourLabel->setFlat(true);
				m_gridLayout->addWidget(hourLabel,0,h+1,Qt::AlignCenter);
				connect(hourLabel,SIGNAL(clicked()),signalMapHour,SLOT(map()));
				signalMapHour->setMapping(hourLabel, h);
			}
			m_gridLayout->setColumnStretch(h+1,1);
		}
		m_gridLayout->setColumnStretch(0,0);


		for(int d=1;d<=7;d++)
		{

			QPushButton *dowLabel = new QPushButton(QDate::shortDayName(d)+": ",this);
			dowLabel->setFlat(true);
			dowLabel->setFixedSize(40,IAVO_BUTTON_H);
			dowLabel->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Expanding);
			m_gridLayout->addWidget(dowLabel,d,0);
			connect(dowLabel,SIGNAL(clicked()),signalMapDoW,SLOT(map()));
			signalMapDoW->setMapping(dowLabel, d);

			for( int h=0;h<24;h++)
			{
				ScheduleButton *button = new ScheduleButton(this,d,h,m_defMode,minMode,maxMode);
				m_gridLayout->addWidget(button,d,h+1);
				m_butMx[d-1][h]=button;
				connect(button,SIGNAL(clicked(int, int, int)),this,SLOT(clickOnButton(int,int,int)));
			}

		}

		QHBoxLayout *legendLayout = new QHBoxLayout();
		legendLayout->addStretch(1);

		for(int i=0;i<strList.size();i++)
		{
			QAbstractButton *butt = new QPushButton(strList[i],this);
			connect(butt,SIGNAL(clicked()),signalMapMode,SLOT(map()));
			signalMapMode->setMapping(butt, i);

			IAVO_QT_SETWINDOWSSTYLE(butt);


			//Set this new color
			QColor qtCol = getColor(i);
			QPalette pal;
			pal.setColor(QPalette::Base,qtCol);
			pal.setColor(QPalette::Background,qtCol);
			pal.setColor(QPalette::Button,qtCol);
			pal.setColor(QPalette::Text,qtCol);
			butt->setPalette(pal);

			legendLayout->addWidget(butt);
		}


		connect(signalMapMode, SIGNAL(mapped(int)), this, SLOT(clickOnMode(int)));
		connect(signalMapHour, SIGNAL(mapped(int)), this, SLOT(clickOnHour(int)));
		connect(signalMapDoW, SIGNAL(mapped(int)), this, SLOT(clickOnDay(int)));



		mainLayout->setSpacing(5);
		mainLayout->addLayout(m_gridLayout,1);
		mainLayout->addLayout(legendLayout,0);
		//mainLayout->addStretch(1);
	}
 }

ScheduleModeWidget::~ScheduleModeWidget()
{
	AVO_PRINT_DEBUG("ScheduleModeWidget::~ScheduleModeWidget()");
}
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------


QColor ScheduleModeWidget::getColor(int mode) const {
	int mod=6;
	int max=X7S_MAX(m_legend.size(),1);
	if(max<3) mod=3;
	CvScalar cvCol = x7sCvtHue2RGBScalar(mode,max,mod);
	return QColor(cvCol.val[0],cvCol.val[1],cvCol.val[2],255);
}


bool ScheduleModeWidget::setMode(int dayOfWeek, int hour, int mode)
{
	if(X7S_CHECK_RANGE(dayOfWeek,1,8))
		if(X7S_CHECK_RANGE(hour,0,24))
		{
			ScheduleButton *butt = this->getButton(dayOfWeek,hour);
			if(butt)
			{
				return butt->setMode(mode);
			}
		}
	avoDebug("ScheduleModeWidget::setMode()") << "Bad parameters: " << dayOfWeek << hour << mode;
	return false;
}

ScheduleButton* ScheduleModeWidget::getButton(int dOw, int hour) const 	{
	ScheduleButton* but=NULL;

	//	if(m_gridLayout)
	//	{
	//		QLayoutItem *lytItem = m_gridLayout->itemAtPosition(dOw,hour+1);
	//		if(lytItem->widget() && lytItem->widget()->inherits("ScheduleButton"))
	//		{
	//			but = static_cast<ScheduleButton*>(lytItem->widget());
	//			if(but->dayOfWeek()!=dOw || but->hour()!=hour) but = NULL;
	//		}
	//	}
	if(X7S_CHECK_RANGE(dOw,1,8))
		if(X7S_CHECK_RANGE(hour,0,24))
		{
			but=m_butMx[dOw-1][hour];
			if(but && (but->dayOfWeek()!=dOw || but->hour()!=hour)) but = NULL;
		}


	return but;
}


void ScheduleModeWidget::clickOnDay(int dOw)
{
	avoDump("ScheduleModeWidget::clickOnHour()") << dOw;

	if(X7S_CHECK_RANGE(dOw,1,8))
	{
		for(int h=0;h<24;h++)
		{
			ScheduleButton* but =  getButton(dOw,h);
			if(but) 	but->click();
		}
	}
}

void ScheduleModeWidget::clickOnHour(int hour)
{
	avoDump("ScheduleModeWidget::clickOnHour()") << hour;

	if(X7S_CHECK_RANGE(hour,0,24))
	{
		for(int dOw=1;dOw<=7;dOw++)
		{
			ScheduleButton* but =  getButton(dOw,hour);
			if(but) 	but->click();
		}
	}

}

void ScheduleModeWidget::clickOnMode(int mode)
{
	avoDump("ScheduleModeWidget::clickOnMode()") << mode;

	for(int dOw=1;dOw<=7;dOw++)
	{
		for(int h=0;h<24;h++)
		{
			ScheduleButton* but =  getButton(dOw,h);
			if(but) 	but->setMode(mode);
		}
	}
}

//------------------ End class ScheduleModeWidget

//========================================================================çç

