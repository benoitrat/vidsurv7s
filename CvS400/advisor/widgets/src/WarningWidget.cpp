#include "WarningWidget.h"
#include "ui_WarningWidget.h"

#include <QDateTime>
#include <QString>
#include <Log.hpp>
#include <QApplication>

WarningWidget::WarningWidget(QWidget *parent, int nMaxItems) :
    QDialog(parent),  m_ui(new Ui::WarningWidget), nMaxItems(nMaxItems)
{
    m_ui->setupUi(this);
    connect(m_ui->close_pushButton, SIGNAL(clicked()), this, SLOT(accept()));
    this->hide();

    //Force default behavior
    m_ui->listWidget->sortItems();
    m_ui->listWidget->setSortingEnabled(false);

}

WarningWidget::~WarningWidget()
{
    delete m_ui;
}

void WarningWidget::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void WarningWidget::AddWarning(const QString &warning)
{

	//First remove the oldest item
	int count = m_ui->listWidget->count();
	if(count >= nMaxItems)
	{
		QListWidgetItem* item = m_ui->listWidget->takeItem(0); // or count-1
		X7S_DELETE_PTR(item);
	}

	QString text = QDateTime::currentDateTime().toString("HH:mm:ss");
	text += " "+ warning;
	QListWidgetItem *listItem = new QListWidgetItem(text,m_ui->listWidget);
	listItem->setToolTip(QDateTime::currentDateTime().toString()+" "+text);
	m_ui->listWidget->addItem(listItem);
	if(m_ui->lockCheck->isChecked()==false) m_ui->listWidget->scrollToBottom();

	avoWarning("WarningWidget::AddWarning") << warning;

	if(this->isVisible()== false)
		this->show();

	if(m_ui->muteCheck->isChecked()==false) QApplication::beep();
}

void WarningWidget::accept()
{
	m_ui->listWidget->clear();//borramos los warnings
	this->hide();
}
