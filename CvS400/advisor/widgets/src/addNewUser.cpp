#include "addNewUser.h"

#include <QDebug>
#include <QMessageBox>
#include <QCryptographicHash>
#include <QByteArray>

addNewUser::addNewUser(QWidget *parent, QList<QString>*user_pt) :
    QWidget(parent),
    m_ui(new Ui::addNewUser)
{
    m_ui->setupUi(this);
    user = user_pt;
    connect(m_ui->OK_pushButton, SIGNAL(clicked()), this, SLOT(ApplyNewValues()));
    //connect(m_ui->cancel_pushButton, SIGNAL(clicked()), this, SLOT(CancelAndClose()));
    connect(m_ui->userRepeatPassword_lineEdit, SIGNAL(textEdited(const QString &)), this, SLOT(EnableOK_pushButton()));
    connect(m_ui->userPassword_lineEdit, SIGNAL(textEdited(const QString &)), this, SLOT(EnableOK_pushButton()));
    connect(m_ui->userName_lineEdit, SIGNAL(textEdited(const QString &)), this, SLOT(EnableOK_pushButton()));
    m_ui->userRepeatPassword_lineEdit->setEchoMode(QLineEdit::Password);
    m_ui->userPassword_lineEdit->setEchoMode(QLineEdit::Password);
}

addNewUser::~addNewUser()
{
    qDebug() << "~addNewUser() closing";
	delete m_ui;
	qDebug() << "~addNewUser() closed";
}

void addNewUser::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
void addNewUser::EnableOK_pushButton()
{
	if ((m_ui->userRepeatPassword_lineEdit->text() != m_ui->userPassword_lineEdit->text()) ||
			(m_ui->userName_lineEdit->text() == "")){
		m_ui->OK_pushButton->setEnabled(false);
		return;
	}
	m_ui->OK_pushButton->setEnabled(true);
}

void addNewUser::ApplyNewValues()
{
	for(int i = 0; i < user->size(); i = i +3)
	{
		if (user->at(i) == m_ui->userName_lineEdit->text()){
			QMessageBox::warning( this, tr("User error"),tr("An user with name ") + m_ui->userName_lineEdit->text() + tr(" already exist."),
					QMessageBox::Ok|QMessageBox::Default,
					QMessageBox:: NoButton, QMessageBox::NoButton);
			return;
		}
	}
	qDebug() << m_ui->userName_lineEdit->text();
	user->append(m_ui->userName_lineEdit->text());
	qDebug() << m_ui->userPassword_lineEdit->text();


	QCryptographicHash *hash_p = new QCryptographicHash(QCryptographicHash::Sha1);
	QByteArray data_p = QByteArray();
	data_p.append(m_ui->userPassword_lineEdit->text());
	hash_p->addData(data_p);
	QByteArray hash_result_p = hash_p->result().toHex();



	QCryptographicHash *hash = new QCryptographicHash(QCryptographicHash::Sha1);
	QByteArray data = QByteArray();
	data.append(m_ui->userName_lineEdit->text());
	//data.append(m_ui->userPassword_lineEdit->text());
	data.append(hash_result_p);
	data.append(QString::number(m_ui->userLevel_comboBox->currentIndex()));
	hash->addData(data);
	QByteArray hash_result = hash->result().toHex();
	qDebug() << "Hash-1 result: " << hash_result;
	user->append(hash_result);
	qDebug() << m_ui->userLevel_comboBox->currentText();
	user->append(QString::number(m_ui->userLevel_comboBox->currentIndex()));
	qDebug() << "Clossing..." << this->close();

	qDebug() << "Closed.";
	emit newUserAdded();
}
void addNewUser::CancelAndClose()
{
	this->close();
}
