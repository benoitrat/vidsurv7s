/**
 *  @file
 *  @brief Contains the class XmlObjConfigWidget.h
 *  @date Nov 19, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef XMLOBJCONFIGWIDGET_H_
#define XMLOBJCONFIGWIDGET_H_

#include <QWidget>
#include <QSignalMapper>
#include <x7sxml.h>
#include "ui_ConfigWidget.h"

namespace Ui {
    class ConfigWidget;
}

/**
 *	@brief The XmlObjConfigWidget.
 *	@ingroup avo_widget
 */
class XmlObjConfigWidget: public QWidget {

	Q_OBJECT

public:
	XmlObjConfigWidget(X7sXmlObject *xmlObj,QWidget *parent=NULL);
	virtual ~XmlObjConfigWidget();

public slots:
	void clickOnApply();
	virtual void reloadAutoParamWgt();
	virtual bool restoreDefault();

protected slots:
	void buttonClick(QAbstractButton *button);
	void paramWgtChange(QWidget *valWgt);
	virtual void clearAutoParamWgt();

protected:
	virtual bool applyParam();
	void addAutoParamWgt(X7sParam *pParam);
	void addStrListParamWgt(X7sParam *pParam,const QStringList &list);

protected:
    Ui::ConfigWidget m_ui;
    X7sXmlObject *m_xmlObj;
private:
	QSignalMapper *m_signalMapper;
	QHash<QString,QString> m_hashTable;
};

#endif /* XMLOBJCONFIGWIDGET_H_ */
