/**
 *  @file
 *  @brief Contains the class AlrLineWidget.h
 *  @date Aug 31, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef ALRLINEWIDGET_H_
#define ALRLINEWIDGET_H_

#include <QtGui>
#include <QWidget>
#include <QVector>
#include "Database.hpp"

#define AVO_ALRLINE_COL_NULL Qt::gray
#define AVO_ALRLINE_COL_VID QColor(0,200,0)
#define AVO_ALRLINE_COL_ACT Qt::yellow
#define AVO_ALRLINE_COL_EVT QColor(255,127,0)
#define AVO_ALRLINE_COL_ALR Qt::red



class AlrTimeWidget;
//class Camera;
//struct AVOAlrEvt;


/**
 *	@brief The AlrLineWidget.
 *	@ingroup avo_widget
 */
class AlrLineWidget : public QWidget {

	Q_OBJECT
	Q_PROPERTY(bool keepRatio READ isKeepRatio WRITE setKeepRatio)
	Q_PROPERTY(bool resizeable READ isResizeable WRITE setResizeable)


public:
	AlrLineWidget(AlrTimeWidget *parent,const Camera *cam,const QDateTime& dtime);
	virtual ~AlrLineWidget();
	void setResizeable(bool resizeable) { this->resizeable = resizeable; };
	bool isResizeable() { return this->resizeable; };
	void setKeepRatio(bool keepRatio) { this->keepRatio = keepRatio; };
	bool isKeepRatio() { return this->keepRatio; };
	QString GetHourStr() { return dtime0.toString("hh:mm"); }
	const QDateTime& getDateTime() const { return dtime0; }
	const QVector<AVOAlrEvt>& getAlrEvts() { return vecAlrEvt; }
	bool hasDateTime(const QDateTime& dtime) { return (dtime0 <= dtime && dtime <= dtime0.addSecs(3600)); };
	bool findBestAlrEvt(AVOAlrEvt& alrEvt,time_t time, time_t delta_t=10);
	QRect lineGeo() const;

signals:
	void released(time_t sec, bool play);


public slots:
	void clickOnDate(const QDateTime& dtime, bool played);

protected:
	virtual void paintEvent(QPaintEvent *event);
	int getPosition(time_t time, time_t t0, time_t t1, int width);
	void mouseReleaseEvent ( QMouseEvent * event );
	void mouseMoveEvent ( QMouseEvent * event );
	void mouseDoubleClickEvent ( QMouseEvent * event );

private:
	AlrTimeWidget *alrParent;
	const Camera *cam;
	QDateTime dtime0;
	QDateTime dtLast;		//!< Last datetime updated.

	QDateTime dtMouse;		//!< Datatime of the mouse.
	QColor colMouse;		//!< Color of the mouse

	QVector<AVOAlrEvt> vecAlrEvt;
	QVector<AVOInterval> vecMjpeg;
	QVector<AVOInterval> vecActiv;
    QSize maxSize;
    bool resizeable;
    bool keepRatio;

    QLabel *label;
    QWidget *line;

};

#endif /* IMAGEFRAME_H_ */
