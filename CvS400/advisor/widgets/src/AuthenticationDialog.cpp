#include "AuthenticationDialog.h"

AuthenticationDialog::AuthenticationDialog(QWidget *parent, QString *user_pt, QString *password_pt) :
    QDialog(parent),
    m_ui(new Ui::AuthenticationDialog)
{
    m_ui->setupUi(this);
    m_ui->password_lineEdit->setEchoMode(QLineEdit::Password);
    user = user_pt;
    password = password_pt;
}

AuthenticationDialog::~AuthenticationDialog()
{
    delete m_ui;
}

void AuthenticationDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void AuthenticationDialog::accept()
{
	user->clear();
	password->clear();
	user->append(m_ui->userName_lineEdit->text());
	password->append(m_ui->password_lineEdit->text());
	done(1);
}
