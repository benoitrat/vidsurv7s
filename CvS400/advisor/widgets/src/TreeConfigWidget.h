/**
 *  @file
 *  @brief Contains the class TreeConfigWidget.h
 *  @date Nov 19, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef TREECONFIGWIDGET_H_
#define TREECONFIGWIDGET_H_

#include <QWidget>
#include "ui_TreeConfigWidget.h"


namespace Ui {
    class TreeConfigWidget;
}


/**
 *	@brief The TreeConfigWidget.
 *	@ingroup avo_widget
 */
class TreeConfigWidget: public QWidget {

	Q_OBJECT

public:
	TreeConfigWidget(QWidget *parent=NULL);
	virtual ~TreeConfigWidget();

signals:
    void refreshAllClicked();
    void applyAllClicked();
    void saveAllClicked();
    void closedWidget();

protected slots:
	virtual void buttonClick(QAbstractButton *button);
	virtual bool refresh();

protected:
	virtual bool save();
	virtual bool ok();


protected:
    Ui::TreeConfigWidget m_ui;

};

#endif /* TREECONFIGWIDGET_H_ */
