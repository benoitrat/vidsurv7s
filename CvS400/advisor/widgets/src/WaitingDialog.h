#ifndef WAINTINGDIALOG_H
#define WAINTINGDIALOG_H

#include "ui_WaitingDialog.h"
#include <QString>
#include <QDialog>
#include <QTimer>

namespace Ui {
class WaitingDialog;
}


/**
*	@brief The AlrTimeWidget.
*	@ingroup avo_widget
*/
class WaitingDialog : public QDialog {
	Q_OBJECT
	Q_DISABLE_COPY(WaitingDialog)
public:
	WaitingDialog(QWidget *parent,const QString& title=QString(),double timeout=3.0, int numTries=5);
	virtual ~WaitingDialog();
	void setText(const QString& text);


public Q_SLOTS:
	int exec();

protected:
	virtual bool trying()=0;

private Q_SLOTS:
	void calling();

private:
	Ui::WaitingDialog *m_ui;
	QTimer* timer;	//!< Timer to stop when done is called
	double m_timeout; 	//!< Timeout between each tries
	int m_numTries; //!< Number of tries to do
	int m_actTry;	//!< Number of tries done
};

#endif // WAINTINGDIALOG_H
