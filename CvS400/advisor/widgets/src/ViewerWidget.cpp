#include "ViewerWidget.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Log.hpp"
#include "ImageWidget.h"
#include <highgui.h>

#include <QFileDialog>
#include <QStatusBar>
#include <QPainter>

QImage ViewerWidget::img = QImage();

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
ViewerWidget::ViewerWidget(QWidget *parent)
:QWidget(parent)
{
	ui.setupUi(this);


	imgFrame = new ImageWidget(ui.frame,true);
	ui.layout_fr->addWidget(imgFrame,0,Qt::AlignCenter);

	if(img.isNull()) img = QPixmap(QString::fromUtf8(":/images/advisor_big.png")).toImage();
	ui.frMsg->hide();
	imgFrame->paintImage(img,false);

	//	ui.frame->setStyleSheet("background-color: yellow;");
	//	ui.centralwidget->setStyleSheet("background-color:green;");
	//	 ui.wgtButton->setStyleSheet("background-color:blue;");

	connect(ui.playButton, SIGNAL(clicked()),this, SLOT(PlayPause()));
	connect(ui.stopButton, SIGNAL(clicked()),this, SLOT(Stop()));
	connect(ui.nextButton, SIGNAL(clicked()),this, SLOT(Next()));
	connect(ui.prevButton, SIGNAL(clicked()),this, SLOT(Prev()));
	connect(ui.jumpButton, SIGNAL(clicked()),this, SLOT(JumpToNextAlarm()));
	connect(ui.tslider, SIGNAL(sliderReleased()),this,SLOT(JumpToTime()));
	connect(ui.tslider,SIGNAL(sliderMoved(int)),this,SLOT(UpdateTimeLabel(int)));

	icon_play.addPixmap(QPixmap(QString::fromUtf8(":/images/play.png")), QIcon::Normal, QIcon::Off);
	icon_pause.addPixmap(QPixmap(QString::fromUtf8(":/images/pause.png")), QIcon::Normal, QIcon::Off);
	icon_wait.addPixmap(QPixmap(QString::fromUtf8(":/images/wait_16.gif")), QIcon::Normal, QIcon::Off);

	displayMessage(tr("No video is opened..."));

	Log::SetLevel(X7S_LOGLEVEL_HIGH);

	m_showMData=true;
	m_timeout=3000;
	lastAlrEvt.blob_ID=-1;

	viewer_thread = new ViewerThread(this, this);
	connect(viewer_thread,SIGNAL(stateChanged(int)),this,SLOT(stateChanged(int)));

}

ViewerWidget::~ViewerWidget()
{
	delete imgFrame;
	delete viewer_thread;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------

/**
* @brief Open the MJPEG file and return the string
* @return
*/
bool ViewerWidget::Open(QString fileName,bool toplay)
{

	if(fileName.isNull())
	{
		fileName = QFileDialog::getOpenFileName(
				this, tr("Open File"),
				"",
				tr("MJPEG (*.mjpeg)"));
	}

	if(!fileName.isNull() && QFile::exists(fileName))
	{
		this->CloseFile();
		if(vidFile.Open(fileName)) {
			this->videoOpened();
			if(toplay) this->PlayPause();
			return true;
		}
		else
		{
			displayMessage(tr("Failed to open the file"),m_timeout);
		}
	}
	else {
		displayMessage(tr("Failed to open the file"),m_timeout);
	}
	return false;
}


bool ViewerWidget::Open(const VideoInFile& videoFile,bool toplay)
{
	X7S_FUNCNAME("ViewerWidget::Open()");

	avoDebug(funcName) << videoFile.getFileInfo().filePath();


	if(videoFile.isValid())
	{
		if(videoFile.isClosed())
		{
			this->CloseFile(); //Stop thread and close mjpeg videos.
			vidFile=videoFile;
			if(vidFile.Open())
			{
				this->videoOpened();
				if(toplay) this->PlayPause();
				return true;
			}
			else{
				displayMessage(tr("Failed to open the file"),m_timeout);
			}
		}
		else {
			displayMessage(tr("Video is already opened"),m_timeout);
		}
	}
	else{
		displayMessage(tr("Video file does not exist on hard drive ")+vidFile.getFilename(),m_timeout);
	}
	return false;
}


void ViewerWidget::videoOpened()
{
	QString txtTimeStr;
	QTextStream txtTimeSstr(&txtTimeStr);
	txtTimeSstr << vidFile.getTimeStart().toString("ddd dd MMM yyyy");
	txtTimeSstr << tr(" from ") << vidFile.getTimeStart().toString("hh:mm:ss");
	txtTimeSstr << tr(" to ") << vidFile.getTimeEnd().toString("hh:mm:ss");


	this->displayMessage();	//Display empty message to remove information
	ui.jumpButton->setEnabled(true);
	ui.lbl_fname->setText(vidFile.getFileInfo().fileName());
	ui.lbl_date->setText(txtTimeStr);
	ui.tslider->setMinimum(vidFile.getTimeStart().toTime_t());
	ui.tslider->setMaximum(vidFile.getTimeEnd().toTime_t());
	ui.tslider->setTickInterval((vidFile.getTimeEnd().toTime_t()-vidFile.getTimeStart().toTime_t())/6);
	displayFrame();
}


bool ViewerWidget::CloseFile()
{
	bool ret;
	this->Stop();	//Stop the thread
	if(imgFrame) imgFrame->clear();	//Clear image frame
	ret= vidFile.Close(); //Close the file
	return ret;
}


void ViewerWidget::PlayPause(bool onlyPlay)
{
	X7S_FUNCNAME("ViewerWidget::Play()");
	AVO_CHECK_WARN(funcName, vidFile.isValid(),,"Video is not valid");

	//If the thread is not running, start it.
	if(isRunning()==false)
	{
		if(vidFile.isOpened()) {
			viewer_thread->PlayPause();
			emit StatusMessage(tr("Playing"),m_timeout);
			tooglePlayPauseButton(false);
		}
	}
	//Otherwise play/pause the thread.
	else
	{
		//Check if we can use the play/pause functionality
		if(!onlyPlay)
		{
			viewer_thread->PlayPause();

			//Modify the button and message
			if(isPaused()) {
				emit StatusMessage(tr("Paused"),m_timeout);
				tooglePlayPauseButton(false);
			}
			else
			{
				emit StatusMessage(tr("Play"),m_timeout);
				tooglePlayPauseButton(true);
			}
		}
	}
}


bool ViewerWidget::isRunning() const {
	return  viewer_thread?viewer_thread->isRunning():false;
}

bool ViewerWidget::isPaused() const {
	return viewer_thread?viewer_thread->isPaused():false;
}

void ViewerWidget::Next()
{
	viewer_thread->Stop();
	tooglePlayPauseButton(true);
	if (vidFile.isValid() == false)
		return;
	vidFile.GrabFrame(1);
	displayFrame();
}

void ViewerWidget::Prev()
{
	viewer_thread->Stop();
	tooglePlayPauseButton(true);
	if (vidFile.isValid() == false)
		return;
	vidFile.GrabFrame(-2);
	displayFrame();
}

void ViewerWidget::Stop()
{
	avoDump("ViewerWidget::Stop()");

	viewer_thread->Stop();
	emit StatusMessage(tr("Stop"),m_timeout);
	tooglePlayPauseButton(true);
	ui.jumpButton->setEnabled(true);
	if (vidFile.isValid()){
		vidFile.Rewind();
		vidFile.GrabFrame(1);
		displayFrame();
	}
}

bool ViewerWidget::JumpToAlarm(const AVOAlrEvt& alrEvt)
{
	//Declaration and initialisation
	bool ret;
	QDateTime alrTime = QDateTime::fromTime_t(alrEvt.t);
	AVO_STRUCT_COPY(lastAlrEvt,alrEvt);

	//When this camera event does not correspond to an alarm event.
	if(alrEvt.blob_ID==-1)	{
		//First go to the alarm time
		ret = this->JumpToTime(alrTime);
	}
	else
	{
		//Jump 1 second before.
		ret = this->JumpToTime(alrTime.addSecs(-1));
	}

	if(ret) {
		avoDebug("ViewerWidget::JumpToAlarm()") <<  "display=" << vidFile.getTime() << ", alarm="<<alrTime;
		displayFrame();
	}
	else
	{
		avoInfo("ViewerWidget::JumpToAlarm()") <<  "Can not jump to alarm :" << alrTime;
		emit StatusMessage(tr("Can not jump to this alarm"),m_timeout);
		ui.jumpButton->setEnabled(false);
	}

	return ret;
}


bool ViewerWidget::JumpToNextAlarm()
{
	bool ret=false;
	setWaitingIcon(true);
	if (vidFile.isValid() == false)
	{
		avoInfo("ViewerWidget::JumpToNextAlarm()") << "vidFile is not valid";
		emit StatusMessage(tr("Next alarm does not exist"),m_timeout);	
	}
	else
	{
		ret= vidFile.JumpToAlarm();
		if(ret) displayFrame();
		else
		{
			avoInfo("ViewerWidget::JumpToNextAlarm()") <<  "Can not jump to next alarm";
			emit StatusMessage(tr("Next alarm does not exist"),m_timeout);
			ui.jumpButton->setEnabled(false);
		}
	}
	setWaitingIcon(false);
	return ret;
}

/**
* @brief Jump to the time given or the time set in the slide bar.
* @param dtime (I isNull() we use the value in the time bar), otherwise it must be between t_start and t_end.
* @return @true if this function really jump
*/
bool ViewerWidget::JumpToTime(QDateTime dtime)
{
	//Jump to the time given by the "time" slide bar
	if(dtime.isNull())
	{
		dtime.setTime_t((uint)ui.tslider->value());
		return vidFile.JumpToTime(dtime);
	}
	//Jump to the given time and change the slide bar value.
	else
	{
		if(vidFile.getTimeStart() <= dtime && dtime <= vidFile.getTimeEnd())
		{
			if(vidFile.JumpToTime(dtime))
			{
				ui.tslider->setValue(dtime.toTime_t());
				return true;
			}
		}
		else {
			avoWarning("ViewerWidget::JumpToTime()") << "time:" << dtime << "> last:"<< vidFile.getTimeEnd();
		}

	}
	return false;
}

void ViewerWidget::UpdateTimeLabel(int value)
{
	QDateTime dtime;
	dtime.setTime_t((uint)value);
	ui.tactuLabel->setText(dtime.toString("hh:mm:ss"));
}

void ViewerWidget::showMetaData(bool enable)
{
	m_showMData=enable;
	imgFrame->paintImage(vidFile.getFrame(),true);
	if(m_showMData) imgFrame->paintImage(vidFile.getMData(),false);
	imgFrame->update();
}

int ViewerWidget::displayFrame()
{
	X7S_FUNCNAME("ViewerWidget::displayFrame()");

	QMutexLocker mutexLock(&m_mutex);

	int wait_ms=-1;
	if((wait_ms=vidFile.GrabFrame())>0)
	{
		try
		{

			//Draw the last alarm
			if(lastAlrEvt.blob_ID!=-1) vidFile.EmphasisAlarm(lastAlrEvt);

			//Draw the frame
			imgFrame->paintImage(vidFile.getFrame(),true);
			if(m_showMData) imgFrame->paintImage(vidFile.getMData(),false);
			imgFrame->update();

			setVideoTime(vidFile.getTime());

			wait_ms = X7S_MIN(wait_ms,500);	// Force to be at maximum 0.5s to wait.
		}

		catch (const cv::Exception & e )
		{
			QString efunc(e.func.c_str());
			QString emsg(e.err.c_str());
			AVOLogLine(e.file.c_str(),e.line,(int)X7S_LOGLEVEL_LOWEST,true,efunc) << emsg;
		}
		catch ( const std::exception & e )
		{
			avoError(funcName) << e.what();
		}
		catch ( ... ) // traite toutes les autres exceptions
		{
			avoError(funcName) << "Unknown Error";
		}

		return wait_ms;


	}
	else return 0;
}

void ViewerWidget::setVideoTime(QDateTime dtime)
{
	X7S_FUNCNAME("ViewerWidget::setVideoTime()");
	AVO_CHECK_WARN(funcName,ui.tslider && ui.tactuLabel,,"Ptr are NULL");
	AVO_CHECK_WARN(funcName,dtime.isValid(),,"dtime is not valid");

	//Obtain the time from the browser.
	if(!ui.tslider->isSliderDown())
	{
		ui.tactuLabel->setText(dtime.toString("hh:mm:ss"));
		ui.tslider->setValue(dtime.toTime_t());
	}

}

void ViewerWidget::setWaitingIcon(bool wait)
{
	if(wait) this->setCursor(Qt::BusyCursor);
	else this->setCursor(Qt::ArrowCursor);;
}


void ViewerWidget::displayMessage(const QString& msg,int ms_timeout)
{
	if(msg.isEmpty())
	{
		ui.frInfo->show();
		ui.frMsg->hide();
	}
	else
	{
		ui.frMsg->setPlainText(msg);
		ui.frMsg->setAlignment(Qt::AlignCenter);
		emit StatusMessage(msg,m_timeout);

		if(ms_timeout>0 && ui.frInfo->isVisible())	QTimer::singleShot(ms_timeout,this,SLOT(displayMessage()));

		ui.frInfo->hide();
		ui.frMsg->show();
	}
}


/**
* @brief Message receive when a state has been changed.
* @param state
*/
void ViewerWidget::stateChanged(int state)
{
	avoDebug("ViewerWidget::stateChanged()") << "State:" << state;

	ViewerThread::State threadState=(ViewerThread::State)state;

	switch(threadState)
	{
	case ViewerThread::STOP:
		this->Stop();
		break;
	default:
		break;
	}
}


void ViewerWidget::tooglePlayPauseButton(bool play)
{
	if(play)
	{
		ui.playButton->setText(tr("Play"));
		ui.playButton->setToolTip(tr("Play"));
		ui.playButton->setIcon(icon_play);
	}
	else
	{
		ui.playButton->setText(tr("Pause"));
		ui.playButton->setToolTip(tr("Pause"));
		ui.playButton->setIcon(icon_pause);
	}
}
//====================================================================


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
ViewerThread::ViewerThread(QObject *parent, ViewerWidget *viewer_ptr)
:QThread(parent)
{

	viewer = viewer_ptr;

	sPrev=IDDLE;
	sAct=IDDLE;
	thread_run = false;

}


ViewerThread::~ViewerThread()
{
	changeState(STOP);
	wait();
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

void ViewerThread::Stop()
{
	changeState(STOP);
	if (thread_run)
		wait();
}


void ViewerThread::PlayPause()
{
	if (thread_run) {
		if(sAct==PLAY) changeState(PAUSE);
		else changeState(PLAY);
	}
	else
	{
		start();
	}
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------


/**
* @brief
*/
void ViewerThread::run()
{
	thread_run = true;
	changeState(PLAY);
	int jpeg_ms;
	while(sAct!=STOP){

		if (sAct==PAUSE) {
			QThread::msleep(100);
		}
		else {
			jpeg_ms =viewer->displayFrame();
			if( jpeg_ms<= 0)
			{
				avoInfo("ViewerThread::run()") << "jpeg_ms=" << jpeg_ms << "Stop";
				break;
			}
			QThread::msleep(jpeg_ms);
		}
	}
	thread_run = false;
	changeState(STOP);
}


void ViewerThread::changeState(const State& state)
{
	if(state!=sAct)
	{
		sPrev=sAct;
		sAct=state;
		emit stateChanged((int)sAct);
	}


}


