#include "ImageWidget.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
ImageWidget::ImageWidget(QWidget *parent,bool resizeable)
:QWidget(parent), maxSize(356,288), resizeable(resizeable)
{
	mode = QPainter::CompositionMode_SourceOver;
	this->setStyleSheet("background-color: red;");

	QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	this->setMinimumSize(maxSize);
	this->setSizePolicy(sizePolicy);

	keepRatio=true;
}

ImageWidget::~ImageWidget()
{

}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------
void ImageWidget::paintImage(const QImage& image, bool clear)
{
	if(clear) {
		imgsVec.clear();
		maxSize = QSize(0,0);
	}
	imgsVec.push_back(image);
	if((image.width() > maxSize.width()) && (image.height() > maxSize.height())) maxSize= image.size();
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------
void ImageWidget::paintEvent(QPaintEvent *event)
{

	QPainter painter(this);
	painter.setCompositionMode(mode);

	QPoint origin;
	QSize size=maxSize;
	if(resizeable)
	{
		if(keepRatio) size.scale(this->size(),Qt::KeepAspectRatio);
		else size.scale(this->size(),Qt::IgnoreAspectRatio);
	}
	else {
		resize(maxSize);
	}

//	//Print debug rectangle to see the maximum size!.
//	QString sizeStr = QString("%1x%2").arg(this->size().width()).arg(this->size().height());
//	painter.drawRect(QRect(origin,size));
//	painter.drawText(QPoint(10,10),sizeStr);

	for(int i=0;i<imgsVec.size();i++)
		painter.drawImage(QPoint(),imgsVec[i]);
}
