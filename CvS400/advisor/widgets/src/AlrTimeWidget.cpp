#include "AlrTimeWidget.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "AlrLineWidget.h"
#include "Database.hpp"
#include "Camera.hpp"
#include "Devices.hpp"
#include "Log.hpp"
#include "CamEvt.hpp"

#define IAVO_BU_W 16
#define IAVO_BU_H 16

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
AlrTimeWidget::AlrTimeWidget(QWidget *parent, Database *db, const Camera *cam)
:QWidget(parent), db(db), cam(cam)
 {

	QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	this->setSizePolicy(sizePolicy);
	this->setMinimumSize(200,100);

	lytMain = new QVBoxLayout(this);
	lytMain->setSpacing(20);

	lytLine = new QVBoxLayout();
	lytLine->setAlignment(Qt::AlignVCenter);
	lytLine->setSpacing(7);
	lytLine->setMargin(5);

	QHBoxLayout *lytTime = new QHBoxLayout();
	QHBoxLayout *lytLegend = new QHBoxLayout();

	//----------------------------------------------------

	dtedit = new QDateTimeEdit(this);
	dtedit->setCalendarPopup(true);
	dtedit->setDisplayFormat("dd/MM/yyyy hh:00");

	QPushButton *pb_now = new QPushButton(QIcon(":/images/refresh_8.png"),"",this);
	pb_now->setFixedSize(IAVO_BU_W,IAVO_BU_H);
	pb_now->setToolTip(tr("Actual Time"));


	QToolButton *tb_up = new QToolButton(this);
	tb_up->setArrowType(Qt::UpArrow);
	tb_up->setFixedSize(IAVO_BU_W,IAVO_BU_H);
	tb_up->setToolTip(tr("Next Hour"));

	QToolButton *tb_down = new QToolButton(this);
	tb_down->setArrowType(Qt::DownArrow);
	tb_down->setFixedSize(IAVO_BU_W,IAVO_BU_H);
	tb_down->setToolTip(tr("Previous Hour"));

	lb_title = new QLabel(this);
	lb_title->setText(cam->GetName());

	lytTime->addWidget(dtedit,0,Qt::AlignLeft);
	lytTime->addWidget(pb_now,0,Qt::AlignLeft);
	lytTime->addWidget(lb_title,2, Qt::AlignCenter);
	lytTime->addWidget(tb_up,0);
	lytTime->addWidget(tb_down,0);


	//----------------------------------------------------


	QPalette plt;	//Use Qt Designer to know the meaning of each ColorRole in QPalette
	plt.setColor(QPalette::Light,Qt::darkGray);
	cb_style = new QWindowsStyle();	//Each role can have different effect according to a style so we force a style.


	cb_vid = new QCheckBox(tr("videos"),this);
	cb_vid->setChecked(true);
	plt.setColor(QPalette::Midlight, AVO_ALRLINE_COL_VID); //bottom-right corner
	plt.setColor(QPalette::Dark, AVO_ALRLINE_COL_VID); //top-left corner
	plt.setColor(QPalette::Base, AVO_ALRLINE_COL_VID); //background normal
	plt.setColor(QPalette::Button, AVO_ALRLINE_COL_VID); //background clicking
	cb_vid->setPalette(plt);
	cb_vid->setStyle(cb_style);

	cb_act = new QCheckBox(tr("activity"),this);
	cb_act->setChecked(true);
	plt.setColor(QPalette::Midlight, AVO_ALRLINE_COL_ACT); //bottom-right corner
	plt.setColor(QPalette::Dark, AVO_ALRLINE_COL_ACT); //top-left corner
	plt.setColor(QPalette::Base, AVO_ALRLINE_COL_ACT); //background normal
	plt.setColor(QPalette::Button, AVO_ALRLINE_COL_ACT); //background clicking
	cb_act->setPalette(plt);
	cb_act->setStyle(cb_style);

	cb_evt = new QCheckBox(tr("events"),this);
	cb_evt->setChecked(true);
	plt.setColor(QPalette::Midlight, AVO_ALRLINE_COL_EVT); //bottom-right corner
	plt.setColor(QPalette::Dark, AVO_ALRLINE_COL_EVT); //top-left corner
	plt.setColor(QPalette::Base, AVO_ALRLINE_COL_EVT); //background normal
	plt.setColor(QPalette::Button, AVO_ALRLINE_COL_EVT); //background clicking
	cb_evt->setPalette(plt);
	cb_evt->setStyle(cb_style);

	cb_alr = new QCheckBox(tr("alarms"),this);
	cb_alr->setChecked(true);

	plt.setColor(QPalette::Midlight, AVO_ALRLINE_COL_ALR);	//bottom-right corner
	plt.setColor(QPalette::Dark, AVO_ALRLINE_COL_ALR);	//top-left corner
	plt.setColor(QPalette::Base, AVO_ALRLINE_COL_ALR);  //background normal
	plt.setColor(QPalette::Button, AVO_ALRLINE_COL_ALR); //background clicking

	cb_alr->setPalette(plt);
	cb_alr->setStyle(cb_style);

	lytLegend->addStretch(1);
	lytLegend->addWidget(cb_vid,0,Qt::AlignCenter);
	lytLegend->addWidget(cb_act,0,Qt::AlignCenter);
	lytLegend->addWidget(cb_evt,0,Qt::AlignCenter);
	lytLegend->addWidget(cb_alr,0,Qt::AlignCenter);
	lytLegend->setSpacing(2);

	//----------------------------------------------------

	lytMain->addLayout(lytTime);
	lytMain->addLayout(lytLine);
	lytMain->addLayout(lytLegend);
	lytMain->addStretch(1);

	//-----------------------------------------------------

	connect(pb_now,SIGNAL(released()),this,SLOT(refresh()));
	connect(tb_up, SIGNAL(pressed()),this, SLOT(nextHour()));
	connect(tb_down, SIGNAL(pressed()),this, SLOT(prevHour()));
	connect(dtedit, SIGNAL(dateTimeChanged(QDateTime)),this, SLOT(updateLine(QDateTime)));
	connect(cb_vid, SIGNAL(clicked()),this, SLOT(repaint()));
	connect(cb_act, SIGNAL(clicked()),this, SLOT(repaint()));
	connect(cb_evt, SIGNAL(clicked()),this, SLOT(repaint()));
	connect(cb_alr, SIGNAL(clicked()),this, SLOT(repaint()));


	//----------------------------------------------------
	refresh();
	



 }

AlrTimeWidget::~AlrTimeWidget()
{

}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------

bool AlrTimeWidget::isToDraw(Database::DBType type)
{
	switch(type)
	{
	case Database::Video: return cb_vid->isChecked();
	case Database::Activity: return cb_act->isChecked();
	case Database::Event: return cb_evt->isChecked();
	case Database::Alarm: return cb_alr->isChecked();
	default: return false;
	}

}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Slots
//----------------------------------------------------------------------------------------

void AlrTimeWidget::refresh() 
{
	setBusy(true);
	lb_title->setText(cam->GetName());

	if(db->IsOk()==false)
	{
		lytMain->insertWidget(2,new QLabel(tr("Could not connect to database"),this),1,Qt::AlignCenter);
	}
	else
	{
		setDateTime(db->GetLastHour(cam,Database::Video));
	}
	setBusy(false);

}

void AlrTimeWidget::setDateTime(const QDateTime& dtime)
{
	dtedit->setDateTime(dtime);
}

bool AlrTimeWidget::addAlrEvent(const AVOAlrEvt* avoAlrEvt)
{
	if(avoAlrEvt==NULL) return false;

	QDateTime alrEvtTime=QDateTime::fromTime_t(avoAlrEvt->t);
	if(lastEvt <= alrEvtTime)
	{
		if(alrEvtTime < lastEvt.addSecs(3600))
		{
			avoDebug("AlrTimeWidget::addAlarmEvent()");
		}
	}

	return false;
}

bool AlrTimeWidget::eventClicked(const CamEvtCarrier& camEvt, bool played)
{

	AlrLineWidget *lineWgt;
	QLinkedListIterator<AlrLineWidget*> i(lineList);

	if(!camEvt.isValid()) return false;

	bool isCam= (camEvt.getCamEvt()->getCamera()==cam);
	QDateTime dtime=camEvt.getCamEvt()->getDateTime();
	QDateTime empty;

	while(i.hasNext())
	{
		lineWgt=i.next();
		if(isCam && lineWgt->hasDateTime(dtime))
		{
			lineWgt->clickOnDate(dtime,played);
		}
		else
		{
			lineWgt->clickOnDate(empty,played);
		}
	}
	return isCam;
}


void AlrTimeWidget::setBusy(bool enable)
{
	enable?setCursor(Qt::BusyCursor):setCursor(Qt::ArrowCursor);
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected Methods
//----------------------------------------------------------------------------------------


void AlrTimeWidget::paintEvent(QPaintEvent *event)
{
	QWidget::paintEvent(event);

	QPainter painter(this);

	if(!lineList.empty())
	{
		QRect lineRect = this->lineList.front()->lineGeo();
		QRect geo = lytLine->geometry();

		//painter.drawRect(geo);

		int x=0;
		int w=lineRect.width();
		int y=geo.top();
		int h=geo.bottom();

		painter.setPen(QPen(Qt::lightGray, 1));
		for(int i=1;i<4;i++) {
			x=lineRect.x()+(i*w)/4;
			painter.drawLine(x,y,x,h);
		}
	}
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Slots
//----------------------------------------------------------------------------------------



void AlrTimeWidget::nextHour()
{
	setBusy(true);
	this->setDateTime(lastEvt.addSecs(3600));
	setBusy(false);
}

void AlrTimeWidget::prevHour()
{
	setBusy(true);
	this->setDateTime(lastEvt.addSecs(-3600));
	setBusy(false);
}

void AlrTimeWidget::updateLine(const QDateTime& dtime, int nof_hours)
{
	clearLayout(lytLine);
	lineList.clear();

	lastEvt=dtime;


	for(int i=0;i<6;i++)
	{
		QDateTime start = lastEvt.addSecs(-3600*i);
		AlrLineWidget *alrLine = new AlrLineWidget(this,cam,start);
		connect(alrLine,SIGNAL(released(time_t, bool)),this,SLOT(setVideoTime(time_t, bool)));
		this->lineList.push_back(alrLine);
		lytLine->addWidget(alrLine);
	}
}

/**
* @brief Set the time of the video.
*
* This function emit a signal with the corresponding video name for the given time
* @param time
*/
void AlrTimeWidget::setVideoTime(time_t time, bool play)
{
	AVOAlrEvt avoAlrEvt;
	AVO_STRUCT_RESET(avoAlrEvt);

	//Search if an alarm is near this value
	AlrLineWidget *alrWgt=getAlrLineWidget(time);
	if(alrWgt && alrWgt->findBestAlrEvt(avoAlrEvt,time))
	{
		lastAlrEvt = CamEvtCarrier(CamEvtVSAlarm::fromAVOAlrEvt(avoAlrEvt));
	}
	//Otherwise obtain the file that correspond to this time
	else
	{
		//Create a special avoAlrEvt with no alarm associated
		avoAlrEvt.cam=cam;
		avoAlrEvt.t=time;
		avoAlrEvt.blob_ID=-1;

		lastAlrEvt = CamEvtCarrier(CamEvtVSAlarm::fromAVOAlrEvt(avoAlrEvt));
	}
	emit EventSelectedSignal(lastAlrEvt,play);
}


void AlrTimeWidget::clearLayout(QLayout *layout)
{
	QLayoutItem *child;
	while ((child = layout->takeAt(0)) != 0) {
		delete child->widget();
		delete child;
	}

}


AlrLineWidget* AlrTimeWidget::getAlrLineWidget(time_t time)
{
	AlrLineWidget *lineWgt;
	QLinkedListIterator<AlrLineWidget*> i(lineList);
	QDateTime dtime=QDateTime::fromTime_t(time);

	while(i.hasNext())
	{
		lineWgt=i.next();
		if(lineWgt->hasDateTime(dtime)) return lineWgt;
	}
	return NULL;
}


