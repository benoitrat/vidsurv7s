#include <TreeConfigWidget.h>

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Log.hpp"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
TreeConfigWidget::TreeConfigWidget(QWidget *parent)
:QWidget(parent)
 {

	m_ui.setupUi(this);
	connect(m_ui.buttonBox, SIGNAL(clicked(QAbstractButton *)), this, SLOT(buttonClick(QAbstractButton * )));
	connect(m_ui.refreshButton,SIGNAL(clicked(bool)),this, SLOT(refresh()));
	m_ui.splitter->setStretchFactor(0,1);
	m_ui.splitter->setStretchFactor(1,3);

 }

TreeConfigWidget::~TreeConfigWidget() {
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------
bool TreeConfigWidget::save()
{
	qDebug() << "save";
	emit applyAllClicked();
	emit saveAllClicked();
	return false;
}


bool TreeConfigWidget::ok()
{
	qDebug() << "ok";
	emit applyAllClicked();
	emit closedWidget();
	this->close();
	return true;
}


bool TreeConfigWidget::refresh()
{
	qDebug() << "Refresh";
	emit refreshAllClicked();
	return false;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------
void TreeConfigWidget::buttonClick(QAbstractButton *button)
{
	X7S_FUNCNAME("TreeConfigWidget::buttonClick");

	switch(m_ui.buttonBox->standardButton(button))
	{
	case QDialogButtonBox::Save:	this->save(); break;
	case QDialogButtonBox::Ok:	this->ok();	break;
	case QDialogButtonBox::Cancel: emit closedWidget(); this->close(); break;
	default:
		avoDebug(funcName) << "Unknow button";
	}
}
