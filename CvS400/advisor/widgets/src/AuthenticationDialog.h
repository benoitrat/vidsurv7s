#ifndef AUTHENTICATIONDIALOG_H
#define AUTHENTICATIONDIALOG_H

#include "ui_AuthenticationDialog.h"
#include <QtGui/QDialog>

namespace Ui {
    class AuthenticationDialog;
}

class AuthenticationDialog : public QDialog {
    Q_OBJECT
    Q_DISABLE_COPY(AuthenticationDialog)
public:
    explicit AuthenticationDialog(QWidget *parent = 0, QString *user_pt = 0, QString *password_pt = 0);
    virtual ~AuthenticationDialog();

protected:
    virtual void changeEvent(QEvent *e);
    virtual void accept();

private:
    Ui::AuthenticationDialog *m_ui;
    QString *user;
    QString *password;
};

#endif // AUTHENTICATIONDIALOG_H
