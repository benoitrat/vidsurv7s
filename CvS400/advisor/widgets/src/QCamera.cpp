#include "QCamera.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Camera.hpp"
#include "CamEvt.hpp"

/*
QColor line_color_pen[] = { Qt::red, Qt::green, Qt::blue, Qt::cyan, Qt::magenta, Qt::yellow,
		Qt::gray, Qt::lightGray, Qt::white, Qt::black, };
#define LINE_COLOR_PEN_SIZE 10
QColor polygon_color_pen[] = { Qt::darkYellow, Qt::darkMagenta,
		Qt::darkCyan, Qt::darkBlue, Qt::darkGreen, Qt::darkRed, Qt::darkGray, Qt::white, Qt::black,};
#define POLYGON_COLOR_PEN_SIZE 9
*/
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
QCamera::QCamera(QWidget *parent, Camera *cam, int index)
:QWidget(parent), camera(cam), camera_index(index)
 {


 }

QCamera::~QCamera()
{
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------
