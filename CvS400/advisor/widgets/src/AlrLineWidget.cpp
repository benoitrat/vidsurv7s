#include "AlrLineWidget.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "AlrTimeWidget.h"
#include "Database.hpp"
#include "Log.hpp"

#define IAVO_ALRLINE_HEIGHT 15
#define IAVO_ALRLINE_LINEW 7
#define IAVO_ALRLINE_LINEY 	7
#define IAVO_ALRLINE_PTW	2
#define IAVO_ALRLINE_PTY0	(IAVO_ALRLINE_LINEY-2)
#define IAVO_ALRLINE_PTY1	(IAVO_ALRLINE_LINEY+3)
#define IAVO_ALRLINE_TRY	4
#define IAVO_ALRLINE_BLY	10

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
AlrLineWidget::AlrLineWidget(AlrTimeWidget *parent,const Camera *cam,const QDateTime& dtime)
:QWidget(parent),  alrParent(parent), cam(cam), dtime0(dtime), colMouse(Qt::black)
{
	//this->setStyleSheet("background-color: blue;");

	QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
	this->setMinimumSize(200,20);
	this->setSizePolicy(sizePolicy);

	this->setMouseTracking(false);

	label = new QLabel(dtime.toString("hh:00"),this);

	line = new QWidget(this);
	line->setMinimumSize(200,11);
	line->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);

	QBoxLayout *layout = new QHBoxLayout(this);
	layout->setSpacing(10); //Set horizontal spacing between label and line.
	layout->addWidget(label,0,Qt::AlignLeft|Qt::AlignVCenter);
	layout->addWidget(line,1,Qt::AlignVCenter);
	layout->setMargin(0);

	vecActiv	=	parent->database()->GetActivity(cam,dtime);
	vecMjpeg	=	parent->database()->GetMJPEG(cam,dtime);
	vecAlrEvt	=	parent->database()->GetAlarmEvt(cam,dtime);
}

AlrLineWidget::~AlrLineWidget()
{

}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------
QRect AlrLineWidget::lineGeo() const
{
	QRect rect = line->geometry();
	rect.translate(this->geometry().topLeft());
	return rect;
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------
void AlrLineWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	time_t t0=dtime0.toTime_t();
	time_t t1=t0+3600;


//		painter.setPen(QPen(Qt::black, 1));
//		painter.drawRect(0,0,this->width()-1,this->height()-1);
//		painter.drawRect(label->x(),label->y(),label->width()-1,label->height()-1);
//		painter.drawRect(line->x(),line->y(),line->width()-1,line->height()-1);

	//Set the coordinate.
	painter.translate(line->x(),line->y());
	int h=line->height();
	int w=line->width();


	//Paint the line in grey
	painter.fillRect(QRect(QPoint(0,0),QPoint(w,h)),
			AVO_ALRLINE_COL_NULL);


	//Paint it in green if thereis a video saved.
	if(alrParent->isToDraw(Database::Video)) {
		for(int i=0;i<vecMjpeg.size();i++)
		{
			int x0 = getPosition(vecMjpeg[i].first.toTime_t(),t0,t1,w);
			int x1 = getPosition(vecMjpeg[i].second.toTime_t(),t0,t1,w);
			if(x0>=0 && x1 >=0)
				painter.fillRect(
						QRect(QPoint(x0,0),QPoint(x1,h)),
						AVO_ALRLINE_COL_VID);

		}
	}

	//Paint it in yellow if thereis activity.
	if(alrParent->isToDraw(Database::Activity)) {
		for(int i=0;i<vecActiv.size();i++)
		{
			int x0 = getPosition(vecActiv[i].first.toTime_t(),t0,t1,w);
			int x1 = getPosition(vecActiv[i].second.toTime_t(),t0,t1,w);
			if(x0>=0 && x1 >=0)
				painter.fillRect(
						QRect(QPoint(x0,0),QPoint(x1,h)),
						AVO_ALRLINE_COL_ACT);
		}
	}


	//paint the alarm.
	for(int i=0;i<vecAlrEvt.size();i++)
	{
		if(vecAlrEvt[i].warn) {
			painter.setPen(QPen(AVO_ALRLINE_COL_ALR, IAVO_ALRLINE_PTW));
			if(alrParent->isToDraw(Database::Alarm)==false) continue;
		}
		else {
			painter.setPen(QPen(AVO_ALRLINE_COL_EVT, IAVO_ALRLINE_PTW));
			if(alrParent->isToDraw(Database::Event)==false) continue;
		}
		int x=getPosition(vecAlrEvt[i].t,t0,t1,w);
		if(x>=0) painter.drawLine(x,1,x,h);
	}

	//Paint the last click.
	if(dtMouse.isValid())
	{
		painter.setPen(QPen(colMouse, IAVO_ALRLINE_PTW));
		int x=getPosition(dtMouse.toTime_t(),t0,t1,w);
		if(x>=0) painter.drawLine(x,1,x,h);
	}
}


int AlrLineWidget::getPosition(time_t time, time_t t0, time_t t1, int width)
{
	if(t0 <=time && time <= t1)
	{
		double tmp=(double)(time-t0);
		tmp/=(t1-t0);
		tmp*=width;
		return qRound(tmp);
	}
	return -1;
}


void AlrLineWidget::mouseMoveEvent(QMouseEvent *event)
{
	QRect lineGeo = line->geometry();
	QPoint pos=event->globalPos();
	QDateTime mouseDT;
	if(lineGeo.contains(event->pos())) {
		double tmp = (event->x()-lineGeo.x());
		tmp/=lineGeo.width();
		tmp*=3600;
		mouseDT = this->dtime0.addSecs(qRound(tmp));
		QToolTip::showText(pos,mouseDT.toString("hh:mm"),this);
	}
}

void AlrLineWidget::mouseReleaseEvent ( QMouseEvent * event )
{
	QRect lineGeo = line->geometry();
	if(lineGeo.contains(event->pos())) {
		double tmp = (event->x()-lineGeo.x());
		tmp/=lineGeo.width();
		tmp*=3600;
		dtMouse = this->dtime0.addSecs(qRound(tmp));
		repaint();
		avoDebug("AlrLineWidget::mouseReleaseEvent()") << dtMouse;
		emit released(dtMouse.toTime_t(), false);
	}
}

void AlrLineWidget::mouseDoubleClickEvent ( QMouseEvent * event )
{
	if (event->button() == Qt::LeftButton){
		QRect lineGeo = line->geometry();
		if(lineGeo.contains(event->pos())) {
			double tmp = (event->x()-lineGeo.x());
			tmp/=lineGeo.width();
			tmp*=3600;
			dtMouse = this->dtime0.addSecs(qRound(tmp));
			avoDebug("AlrLineWidget::mouseDoubleClickEvent()")  << dtMouse;
			emit released(dtMouse.toTime_t(), true);
		}
	}
}

/**
 * @brief Find an alarm event near clicking zone.
 *
 * @todo Call this method from mouseReleaseEvent(), and improve delta_t according to the size of line widget.
 * @param alrEvt	The alarm event that we want to set.
 * @param time	The time when we have clicked.
 * @param delta_t The zone in which we are near an event.
 * @return
 */
bool AlrLineWidget::findBestAlrEvt(AVOAlrEvt& alrEvt,time_t time, time_t delta_t)
{
	X7S_FUNCNAME("AlrLineWidget::findBestAlrEvt()");

	bool ret=false;
	if(vecAlrEvt.isEmpty()) return ret;

	int i=0,i_min=-1;
	int i_max=(int)vecAlrEvt.size()-1;
	time_t dtmin=delta_t;
	time_t t_min=time-delta_t;
	time_t t_max=time+delta_t;

	//Set up on the first interesting alarm event.
	//TODO: Dichotomize to search faster.
	while(i<=i_max) {
		if(vecAlrEvt[i].t>=t_min) break;	//End looping when we are above t_min.
		i++;
	}

	//Then until we are less than t_max.
	while(i<=i_max)
	{
		if(vecAlrEvt[i].t>=t_max) break;	//Break the loop if we are above the t_max
		time_t atdiff=(time_t)abs((long)(vecAlrEvt[i].t-time)); //Absolute time difference.
		if(atdiff<dtmin) {
			dtmin=atdiff;
			i_min=i;
		}

		i++;
	}
	if(0<= i_min && i_min <=i_max) {
		alrEvt=vecAlrEvt[i_min];
		avoInfo(funcName) << "Find alarm at time" << QDateTime::fromTime_t(alrEvt.t) << "while clicking at" << dtMouse;
		colMouse=Qt::darkGray;
		dtMouse = QDateTime::fromTime_t(alrEvt.t);	//Replace the new dtMouse
		this->update();	// Update the new graphical position.
		return true;
	}
	return false;
}

void AlrLineWidget::clickOnDate(const QDateTime& dtime, bool played)
{
	if(dtMouse.isValid() && dtime0 <= dtMouse && dtMouse <= dtime0.addSecs(3600))
	{
		if(played) colMouse=Qt::black;
		else colMouse=Qt::darkGray;
		dtMouse = dtime;	//Replace the new dtMouse
	}
	else dtMouse=QDateTime();
	this->update();	// Update the new graphical position.

}

