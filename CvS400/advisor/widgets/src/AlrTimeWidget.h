/**
*  @file
*  @brief Contains the class AlrTimeWidget.h
*  @date Aug 31, 2009
*  @author neub
*	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
*/

#ifndef ALRTIMEWIDGET_H_
#define ALRTIMEWIDGET_H_

#include <QtGui>
#include <QWidget>
#include <QVector>

#include "Database.hpp"

class Camera;
class AlrLineWidget;

/**
*	@brief The AlrTimeWidget.
*	@ingroup avo_widget
*/
class AlrTimeWidget : public QWidget {

	Q_OBJECT
	Q_PROPERTY(bool resizeable READ isResizeable WRITE setResizeable)


public:
	AlrTimeWidget(QWidget *parent, Database *db, const Camera *cam);
	virtual ~AlrTimeWidget();
	void setResizeable(bool resizeable) { this->resizeable = resizeable; };
	bool isResizeable() { return this->resizeable; };
	bool isToDraw(Database::DBType type);
	Database * database() { return db; };
	const Camera * camera() { return cam; }
	static void clearLayout(QLayout *l);

	void setBusy(bool enable=true);


signals:
	void EventSelectedSignal(const CamEvtCarrier& camEvt, bool play);


protected:
	void paintEvent(QPaintEvent *event);
	AlrLineWidget* getAlrLineWidget(time_t time);

private:
	QBoxLayout *lytMain;
	QBoxLayout *lytLine;
	QDateTimeEdit *dtedit;
	QDateTime lastEvt;
	CamEvtCarrier lastAlrEvt;

	QCheckBox *cb_vid, *cb_act, *cb_evt, *cb_alr;
	QLabel *lb_title;
	QStyle *cb_style;	//!< Style for all the checkbox

	Database *db;
	const Camera *cam;
	QLinkedList<AlrLineWidget*> lineList;
	QSize maxSize;
	bool resizeable;

public slots:
	void refresh();
	void setDateTime(const QDateTime& dtime);
	bool eventClicked(const CamEvtCarrier& camEvt, bool played);
	bool addAlrEvent(const AVOAlrEvt* avoAlrEvt);


private slots:
	void updateLine(const QDateTime& dtime, int nof_hours=6);
	void nextHour();
	void prevHour();
	void setVideoTime(time_t time, bool play);
};

#endif /* IMAGEFRAME_H_ */
