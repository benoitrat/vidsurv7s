/**
 *  @file
 *  @brief Contains the class XmlObjTreeConfWidget.h
 *  @date Nov 18, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef XMLOBJTREECONFWIDGET_H_
#define XMLOBJTREECONFWIDGET_H_

#include <QTreeWidget>
#include <x7sxml.h>

#include "TreeConfigWidget.h"



/**
 *	@brief The XmlObjTreeConfWidget.
 *	@ingroup avo_widget
 */
class XmlObjTreeConfWidget:  public  TreeConfigWidget {

	Q_OBJECT

public:

	XmlObjTreeConfWidget(X7sXmlObject *xmlObj, QWidget *parent=NULL);
	virtual ~XmlObjTreeConfWidget();

	X7sXmlObject* getXmlObjRoot() const { return m_xmlObj; }

public slots:
	bool showItem(QTreeWidgetItem* item,int column);

protected:
	QTreeWidgetItem* addXmlObj(X7sXmlObject *node, QTreeWidgetItem* item, QTreeWidgetItem *after, int max_level=INT_MAX);

private:
	QTreeWidget *treeWgt;
	X7sXmlObject *m_xmlObj;
};

#endif /* X7SXMLOBJWIDGET_H_ */
