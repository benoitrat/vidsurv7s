/**
 *  @file
 *  @brief Contains the class VideoHandlerScheduleWidget.h
 *  @date Nov 30, 2009
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef VIDEOHANDLERSCHEDULEWIDGET_H_
#define VIDEOHANDLERSCHEDULEWIDGET_H_

#include <QWidget>
#include "XmlObjConfigWidget.h"

class VideoHandler;
class ScheduleModeWidget;

/**
 *	@brief The VideoHandlerScheduleWidget.
 *	@ingroup avo_widget
 */
class VideoHandlerScheduleWidget : public XmlObjConfigWidget {

	Q_OBJECT

public:
	VideoHandlerScheduleWidget(QWidget *parent, VideoHandler *viHandler);
	virtual ~VideoHandlerScheduleWidget();

	bool restoreDefault();

public slots:
	bool applyParam();

private:
	bool reloadValues();

private slots:
	bool changeMode(int dOw, int hour, int mode);

private:
	VideoHandler *m_vidHandler;
	ScheduleModeWidget *m_schedWgt;
};

#endif /* VIDEOHANDLERSCHEDULEWIDGET_H_ */
