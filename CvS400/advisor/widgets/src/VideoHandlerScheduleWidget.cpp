#include <VideoHandlerScheduleWidget.h>
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "VideoHandler.hpp"
#include "ScheduleModeWidget.h"
#include "Log.hpp"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
VideoHandlerScheduleWidget::VideoHandlerScheduleWidget(QWidget *parent, VideoHandler *vidHandler)
:XmlObjConfigWidget(NULL,parent), m_vidHandler(vidHandler), m_schedWgt(NULL)
 {
	if(vidHandler)
	{
		m_ui.attrGroupBox->hide();
		m_ui.title->setText(tr("Grabbing Schedule"));
		m_ui.comment->setText(tr("Select the hour when the grabbing must be activated"));

		QStringList strgList;
		strgList << tr("Off") << tr("On");
		m_schedWgt = new ScheduleModeWidget(m_ui.scrollArea,strgList,VIDEOHANDLER_RECORD_DEFVAL);
		connect(m_schedWgt,SIGNAL(modeChanged(int, int, int)),this,SLOT(changeMode(int,int,int)));
		m_ui.scrollAreaLayout->insertWidget(1,m_schedWgt);
		this->reloadValues();

		X7sParam *pParam = NULL;
		pParam = (X7sParam*)m_vidHandler->GetParam("mode");
		this->addStrListParamWgt(pParam,m_vidHandler->getModeNames());
		//pParam = (X7sParam*)m_vidHandler->GetParam("general_mode");
		//this->addAutoParamWgt(pParam);

		m_xmlObj=vidHandler;
	}
 }

VideoHandlerScheduleWidget::~VideoHandlerScheduleWidget()
{
	AVO_PRINT_DEBUG("VideoHandlerScheduleWidget::~VideoHandlerScheduleWidget()");
}


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods/Slots
//----------------------------------------------------------------------------------------
bool VideoHandlerScheduleWidget::restoreDefault()
{
	X7S_FUNCNAME("VideoHandlerScheduleWidget::restoreDefault()");

	bool ret=false;

	if(m_vidHandler->setRecordEnable(VIDEOHANDLER_RECORD_DEFVAL))

	avoInfo(funcName) << ret;
	return ret;
}

bool VideoHandlerScheduleWidget::reloadValues()
{
	AVO_CHECK_WARN("VideoHandlerScheduleWidget::reloadValues()",m_vidHandler,false,"vidHandler is NULL");
	AVO_CHECK_WARN("VideoHandlerScheduleWidget::reloadValues()",m_schedWgt,false,"schedWgt is NULL");

	bool ret=true;
	for(int dOw=1;dOw<=7;dOw++)
	{
		for(int h=0;h<24;h++)
		{
				int mode = (int)m_vidHandler->isRecordEnable(dOw,h);
				ret = ret & m_schedWgt->setMode(dOw,h,mode);
		}
	}
	return ret;
}



//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods/Slots
//----------------------------------------------------------------------------------------

bool VideoHandlerScheduleWidget::applyParam()
{
	bool ret=false;

	if(m_vidHandler)
	{
		ret=m_vidHandler->setParams();
		avoDebug("VideoHandlerScheduleWidget::applyParam()") << ret;
	}
	XmlObjConfigWidget::applyParam();
	return false;
}



bool VideoHandlerScheduleWidget::changeMode(int dOw,int hour, int mode)
{
	if(m_vidHandler)
	{
		return m_vidHandler->setRecordEnable(dOw,hour,(bool)mode);
	}
	return false;
}

