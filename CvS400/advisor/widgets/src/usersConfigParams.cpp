#include "usersConfigParams.h"
#include "ui_usersConfigParams.h"
#include <QCryptographicHash>
#include <QByteArray>
#include <QDebug>

usersConfigParams::usersConfigParams(QWidget *parent, QList<QString>*user_pt) :
    QDialog(parent),
    m_ui(new Ui::usersConfigParams)
{
    m_ui->setupUi(this);
    user = user_pt;
    connect(m_ui->apply_pushButton, SIGNAL(clicked()), this, SLOT(ApplyNewValues()));
//    connect(m_ui->cancel_pushButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(m_ui->userRepeatPassword_lineEdit, SIGNAL(textEdited(const QString &)), this, SLOT(EnableApply_pushButton()));
    connect(m_ui->userPassword_lineEdit, SIGNAL(textEdited(const QString &)), this, SLOT(EnableApply_pushButton()));
    connect(m_ui->userName_comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(UserChanged()));
    connect(m_ui->deleteUser_pushButton, SIGNAL(clicked()), this, SLOT(DeleteUser()));
    m_ui->userRepeatPassword_lineEdit->setEchoMode(QLineEdit::Password);
    m_ui->userPassword_lineEdit->setEchoMode(QLineEdit::Password);

    m_ui->params_groupBox->hide();

    if (user->size() == 0)
    	m_ui->userName_comboBox->addItem(tr("Warning. No users defined."));
    for(int i = 0; i < user->size(); i = 3 + i)
    {
    	m_ui->userName_comboBox->addItem(user->at(i));
    	//m_ui->userLevel_lineEdit->setText(user->at(i+2));
    }
    UserChanged();
}

usersConfigParams::~usersConfigParams()
{
    delete m_ui;
}

void usersConfigParams::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void usersConfigParams::EnableApply_pushButton()
{
	if ((m_ui->userRepeatPassword_lineEdit->text() != m_ui->userPassword_lineEdit->text())){
		m_ui->apply_pushButton->setEnabled(false);
		return;
	}
	m_ui->apply_pushButton->setEnabled(true);
}

void usersConfigParams::ApplyNewValues()
{
	int user_index = 3* m_ui->userName_comboBox->currentIndex();

	QCryptographicHash *hash = new QCryptographicHash(QCryptographicHash::Sha1);
	QByteArray data = QByteArray();
	data.append(user->at(user_index));
	data.append(m_ui->userPassword_lineEdit->text());
	data.append(QString::number(m_ui->userLevel_comboBox->currentIndex()));
	hash->addData(data);
	QByteArray hash_result = hash->result().toHex();
	qDebug() << "Hash-1 result: " << hash_result;
	user->replace(user_index + 1, QString(hash_result));
	qDebug() << m_ui->userLevel_comboBox->currentText();
	user->replace(user_index + 2, QString::number(m_ui->userLevel_comboBox->currentIndex()));
	qDebug() << "Clossing..." << this->close();

	qDebug() << "Closed.";

	m_ui->params_groupBox->hide();
	emit userModifiedSignal();
}
void usersConfigParams::UserChanged()
{
	m_ui->changeUserParams_checkBox->setChecked(false);
	m_ui->params_groupBox->hide();
	if(user->size() == 0)
		return;
	m_ui->userLevel_comboBox->setCurrentIndex(user->at(3 * m_ui->userName_comboBox->currentIndex() + 2).toInt());
	m_ui->userLevel_lineEdit->setText(m_ui->userLevel_comboBox->currentText());
}

void usersConfigParams::DeleteUser()
{
	if (user->size() == 0)
		return;
	int user_index = m_ui->userName_comboBox->currentIndex();

	user->removeAt(3*user_index);
	user->removeAt(3*user_index);
	user->removeAt(3*user_index);
	emit userModifiedSignal();
	if (user->size() == 0)
		this->done(1);

	m_ui->userName_comboBox->setCurrentIndex(0);
	m_ui->userName_comboBox->removeItem(user_index);
}
