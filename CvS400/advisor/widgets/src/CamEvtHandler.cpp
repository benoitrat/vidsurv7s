#include "CamEvtHandler.h"
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Includes done in cpp file to accelerate compilation.
//----------------------------------------------------------------------------------------
#include "Camera.hpp"
#include "CamEvt.hpp"
#include "Database.hpp"
#include "VideoHandler.hpp"
#include "Log.hpp"


//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Constructor/Destructor
//----------------------------------------------------------------------------------------
CamEvtHandler::CamEvtHandler(Database *database, VideoHandler *vidHandler)
:QObject(NULL),
 m_database(database), m_vidHandler(vidHandler)
 {

 }

CamEvtHandler::~CamEvtHandler()
{
}

//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Public Methods
//----------------------------------------------------------------------------------------
bool CamEvtHandler::processNewFrame(Camera *cam)
{
	X7S_FUNCNAME("CamEvtHandler::processNewFrame()");
	AVO_CHECK_WARN(funcName,cam,false,"Camera is NULL");

	//Copy the camera event during the lock.
	cam->mutex().lock();
	QVector<CamEvtCarrier> camEvtList;
	for(int i=0;i<cam->GetEventsList().size();i++)
		camEvtList.push_back(cam->GetEventsList().at(i));
	cam->mutex().unlock();

	//Print the event
	bool dbg=true;
	for(int i=0;i<camEvtList.size() && dbg;i++)
	{
		const CamEvt *camEvt = NULL;
		if(camEvtList[i].isValid()) camEvt=camEvtList[i].getCamEvt();
		avoDebug(funcName) << camEvt;
	}

	//Write the event in the database
	if(m_database && m_database->IsOk())
	{
		for(int i=0;i<camEvtList.size();i++)
		{
			m_database->ProcessCamEvt(camEvtList[i]);
		}
	}

	//Emit the event to the other QObject.
	for(int i=0;i<camEvtList.size();i++)
	{
		if(camEvtList[i].isType(CamEvt::VSAlarm))
		{
			emit eventVSAlarmReceived(camEvtList[i]);
		}
		else if(camEvtList[i].isType(CamEvt::Video))
		{

		}
		emit eventReceived(camEvtList[i]);
	}

	//IMPROVE: This should be done with event.
	//Write the Frame
	if(m_vidHandler)
	{
		m_vidHandler->WriteFrame(cam,cam->GetMetaJPEGFrame());
	}

	return true;
}
//----------------------------------------------------------------------------------------
// >>>>>>>>>>>>>>>>>>	Protected/Private Methods
//----------------------------------------------------------------------------------------
