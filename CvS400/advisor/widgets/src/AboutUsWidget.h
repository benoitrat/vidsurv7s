/*
 * AboutUsWidget.h
 *
 *  Created on: 25/01/2010
 *      Author: Ben
 */

#ifndef _ABOUTUSWIDGET_H_
#define _ABOUTUSWIDGET_H_

#include <QWidget>
#include <QDialog>
#include "ui_AboutUsWidget.h"

namespace Ui {
    class AboutUsWidget;
}

class AboutUsWidget : public QWidget {

	Q_OBJECT
    Q_DISABLE_COPY(AboutUsWidget)

public:
	AboutUsWidget(QWidget *parent=0);
	virtual ~AboutUsWidget();


private:
    Ui::AboutUsWidget *m_ui;
};

#endif /* ABOUTUSDIALOG_H_ */
