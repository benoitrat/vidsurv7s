/**
*  @file
*  @brief Contains the class ViewerWindow.h
*  @date Aug 28, 2009
*  @author neub
*	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
*/

#ifndef VIEWERWIDGET_H_
#define VIEWERWIDGET_H_

class ImageWidget;
class ViewerWidget;
class ViewerThread;

#include "VideoInFile.hpp"

#include "ui_ViewerWidget.h"
#include <QWidget>
#include <QFileDialog>
#include <QThread>





/**
*	@brief The ViewerWindow.
*	@ingroup avo_viewer
*/
class ViewerWidget : public QWidget
{
	Q_OBJECT

public:
	ViewerWidget(QWidget *parent = 0);
	virtual ~ViewerWidget();
	int displayFrame();
	const VideoInFile& getVideo() const { return vidFile; }
	const VideoInFile* getPVideo() const { return (const VideoInFile*)&vidFile; }
	const AVOAlrEvt& getLastAlrEvt() const { return lastAlrEvt; }
	bool isMetaDataShown() { return m_showMData; }
	bool isRunning() const;
	bool isPaused() const;
	bool isPlaying() const { return (isRunning() && !isPaused()); }

	public slots:
	void displayMessage(const QString& msg=QString(),int timeout=0);
	void setWaitingIcon(bool wait=true);

	signals:
	void StatusMessage(const QString &str, int timeout=0);
	void timeLabelChanged(const QString& text);

protected:
	void videoOpened();
	void tooglePlayPauseButton(bool play);

protected slots:
	void setVideoTime(QDateTime dtime);


private:
	QMutex m_mutex;
	Ui::ViewerWidget ui;
	VideoInFile vidFile;
	ImageWidget *imgFrame;
	ViewerThread *viewer_thread;
	QIcon icon_play;
	QIcon icon_pause;
	QIcon icon_wait;
	bool m_showMData;
	int m_timeout;
	AVOAlrEvt lastAlrEvt;

	static QImage img;

public slots:
bool JumpToTime(QDateTime dtime=QDateTime());
bool JumpToAlarm(const AVOAlrEvt& alrEvt);
void showMetaData(bool enable=true);
bool Open(QString fname=QString(),bool play=false);
bool Open(const VideoInFile& videoFile, bool play=false);
void PlayPause(bool onlyPlay=false);
void Stop();


private slots:
bool CloseFile();
void Next();
void Prev();
bool JumpToNextAlarm();
void UpdateTimeLabel(int value);
void stateChanged(int state);


};



class ViewerThread: public QThread {

	Q_OBJECT

public:
	enum State { IDDLE, PLAY, STOP, PAUSE };

public:
	ViewerThread(QObject *parent=0, ViewerWidget *viewer_ptr = 0);
	~ViewerThread();

	bool isPaused() const { return isRunning() && sAct==PAUSE; }	//!< True if the thread is paused during a running
	bool isState(const ViewerThread::State& state) const { return sAct==state; }
	bool isPrevState(const ViewerThread::State& state) const { return sPrev==state; }
	void PlayPause();
	void Stop();

	signals:
	void stateChanged(int state);

protected:
	void run();
	void changeState(const ViewerThread::State& state);

private:
	ViewerWidget *viewer;
	bool thread_run;
	State sPrev, sAct;
};




#endif /* VIEWERWIDGET_H_ */
