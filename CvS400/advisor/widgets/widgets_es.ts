<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>AboutUsWidget</name>
    <message>
        <location filename="src/AboutUsWidget.ui" line="14"/>
        <source>About Us</source>
        <translation>Sobre nosotros</translation>
    </message>
    <message>
        <location filename="src/AboutUsWidget.ui" line="158"/>
        <source>&lt;a href=&quot;http://www.sevensols.com&quot;&gt;www.sevensols.com&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/AboutUsWidget.ui" line="189"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://www.grinninglizard.com/tinyxml&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;www.grinninglizard.com/tinyxml&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/AboutUsWidget.ui" line="203"/>
        <source>OpenCV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/AboutUsWidget.ui" line="217"/>
        <source>Libraries</source>
        <translation>Librerías</translation>
    </message>
    <message>
        <location filename="src/AboutUsWidget.ui" line="227"/>
        <source>&lt;a href=&quot;http://opencv.willowgarage.com&quot;&gt;http://opencv.willowgarage.com&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/AboutUsWidget.ui" line="237"/>
        <location filename="src/AboutUsWidget.ui" line="258"/>
        <location filename="src/AboutUsWidget.ui" line="275"/>
        <location filename="src/AboutUsWidget.ui" line="289"/>
        <source>v0.0.0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/AboutUsWidget.ui" line="244"/>
        <source>TinyXML</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/AboutUsWidget.ui" line="251"/>
        <source>Qt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/AboutUsWidget.ui" line="265"/>
        <source>&lt;a href=&quot;http://qt.nokia.com&quot;&gt;http://qt.nokia.com&lt;/a&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/AboutUsWidget.ui" line="282"/>
        <source>x7s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/AboutUsWidget.ui" line="321"/>
        <source>Advisor v0.0.0</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="src/AboutUsWidget.ui" line="333"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright © 2009 Seven Solutions S.L., &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Avda. de Andalucía, Granada 18015, Spain&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;All rights reserved.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Seven Solutions S.L.  has intellectual property rights relating to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;technology embedded in the product that is described in this document.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Authors :&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;--------------------&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Benoit Rat&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Gabriel Ramírez&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Rafael Rodriguez&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Miguel Mendez&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Third Party :&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;--------------------&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;THIS PRODUCT USE THIRD PARTY LIBRARY UNDER VARIOUS LICENCE. ALL THESE LICENCES&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;ARE COMPATIBLE WITH OUR PRODUCT.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; - LibJPEG	: See doc/licences/lic_jpeg.txt&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; - OpenCV Libray : See doc/licences/lic_opencv_bsd.txt&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; - PostGreSQL: See doc/licences/lic_postgresql_bsd.txt&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; - Qt 4.x Library: See doc/licences/lic_qt_lgpl.txt&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; - TinyXMl 	: See doc/licences/lic_tinyxml_zlib.txt&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AlrTimeWidget</name>
    <message>
        <location filename="src/AlrTimeWidget.cpp" line="45"/>
        <source>Actual Time</source>
        <translation>Hora Actual</translation>
    </message>
    <message>
        <location filename="src/AlrTimeWidget.cpp" line="51"/>
        <source>Next Hour</source>
        <translation>Proxima Hora</translation>
    </message>
    <message>
        <location filename="src/AlrTimeWidget.cpp" line="56"/>
        <source>Previous Hour</source>
        <translation>Hora Previa</translation>
    </message>
    <message>
        <location filename="src/AlrTimeWidget.cpp" line="76"/>
        <source>videos</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/AlrTimeWidget.cpp" line="85"/>
        <source>activity</source>
        <translation>actividad</translation>
    </message>
    <message>
        <location filename="src/AlrTimeWidget.cpp" line="94"/>
        <source>events</source>
        <translation>eventos</translation>
    </message>
    <message>
        <location filename="src/AlrTimeWidget.cpp" line="103"/>
        <source>alarms</source>
        <translation>alarmas</translation>
    </message>
    <message>
        <location filename="src/AlrTimeWidget.cpp" line="181"/>
        <source>Could not connect to database</source>
        <translation>No se puede conectar a la base de datos</translation>
    </message>
</context>
<context>
    <name>AuthenticationDialog</name>
    <message>
        <location filename="src/AuthenticationDialog.ui" line="14"/>
        <source>User Authentication</source>
        <translation>Autenticación de Usurario</translation>
    </message>
    <message>
        <location filename="src/AuthenticationDialog.ui" line="68"/>
        <source>User name:</source>
        <translation>Nombre usuario:</translation>
    </message>
    <message>
        <location filename="src/AuthenticationDialog.ui" line="78"/>
        <source>Password:</source>
        <translation>Contraseña:</translation>
    </message>
</context>
<context>
    <name>ConfigWidget</name>
    <message>
        <location filename="src/ConfigWidget.ui" line="20"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/ConfigWidget.ui" line="54"/>
        <source>Title</source>
        <translation>Titulo</translation>
    </message>
    <message>
        <location filename="src/ConfigWidget.ui" line="115"/>
        <source>comment</source>
        <translation>comentario</translation>
    </message>
    <message>
        <location filename="src/ConfigWidget.ui" line="125"/>
        <source>Attributes </source>
        <translation>Atributos</translation>
    </message>
    <message>
        <location filename="src/ConfigWidget.ui" line="143"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/ConfigWidget.ui" line="160"/>
        <source>Type</source>
        <translation>#Tipo</translation>
    </message>
    <message>
        <location filename="src/ConfigWidget.ui" line="186"/>
        <source>Parameters </source>
        <translation>Parámetros</translation>
    </message>
</context>
<context>
    <name>TreeConfigWidget</name>
    <message>
        <location filename="src/TreeConfigWidget.ui" line="26"/>
        <source>TreeConfigWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/TreeConfigWidget.ui" line="59"/>
        <source>Refresh All</source>
        <translation>Refrescar Todo</translation>
    </message>
    <message>
        <location filename="src/TreeConfigWidget.ui" line="107"/>
        <source>Expand All</source>
        <translation>Expandir Todo</translation>
    </message>
    <message>
        <location filename="src/TreeConfigWidget.ui" line="110"/>
        <source>ExpandAll</source>
        <translation>ExpandirTodo</translation>
    </message>
    <message>
        <location filename="src/TreeConfigWidget.ui" line="126"/>
        <source>Collapse All</source>
        <translation>Contraer Todo</translation>
    </message>
    <message>
        <location filename="src/TreeConfigWidget.ui" line="129"/>
        <source>collapseAll</source>
        <translation>contraerTodo</translation>
    </message>
</context>
<context>
    <name>VideoHandler</name>
    <message>
        <location filename="../core/src/VideoHandler.cpp" line="103"/>
        <source>Full</source>
        <translation>Total</translation>
    </message>
    <message>
        <location filename="../core/src/VideoHandler.cpp" line="104"/>
        <source>Activity</source>
        <translation>Actividad</translation>
    </message>
    <message>
        <location filename="../core/src/VideoHandler.cpp" line="105"/>
        <source>Event</source>
        <translation>Eventos</translation>
    </message>
    <message>
        <location filename="../core/src/VideoHandler.cpp" line="106"/>
        <source>Alarm</source>
        <translation>Alarmas</translation>
    </message>
</context>
<context>
    <name>VideoHandlerScheduleWidget</name>
    <message>
        <location filename="src/VideoHandlerScheduleWidget.cpp" line="19"/>
        <source>Grabbing Schedule</source>
        <translation>Calendario de Grabación</translation>
    </message>
    <message>
        <location filename="src/VideoHandlerScheduleWidget.cpp" line="20"/>
        <source>Select the hour when the grabbing must be activated</source>
        <translation>Selecciones la hora a la que se activará la grabación</translation>
    </message>
    <message>
        <location filename="src/VideoHandlerScheduleWidget.cpp" line="23"/>
        <source>Off</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/VideoHandlerScheduleWidget.cpp" line="23"/>
        <source>On</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ViewerWidget</name>
    <message>
        <location filename="src/ViewerWidget.cpp" line="46"/>
        <source>No video is opened...</source>
        <translation>Ningún vídeo abierto...</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.cpp" line="83"/>
        <source>Open File</source>
        <translation>Abrir Fichero</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.cpp" line="85"/>
        <source>MJPEG (*.mjpeg)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.cpp" line="98"/>
        <location filename="src/ViewerWidget.cpp" line="102"/>
        <location filename="src/ViewerWidget.cpp" line="128"/>
        <source>Failed to open the file</source>
        <translation>Error al abrir el fichero</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.cpp" line="132"/>
        <source>Video is already opened</source>
        <translation>El vídeo ya está abierto</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.cpp" line="136"/>
        <source>Video file does not exist on hard drive </source>
        <translation>El fichero de vídeo no existe en el disco duro</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.cpp" line="147"/>
        <source> from </source>
        <translation>de</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.cpp" line="148"/>
        <source> to </source>
        <translation>a</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.cpp" line="181"/>
        <source>Playing</source>
        <translation>Reproduciendo</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.cpp" line="195"/>
        <source>Paused</source>
        <translation>Pausado</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="170"/>
        <location filename="src/ViewerWidget.cpp" line="200"/>
        <location filename="src/ViewerWidget.cpp" line="405"/>
        <location filename="src/ViewerWidget.cpp" line="406"/>
        <source>Play</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="187"/>
        <location filename="src/ViewerWidget.ui" line="190"/>
        <location filename="src/ViewerWidget.cpp" line="241"/>
        <source>Stop</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.cpp" line="411"/>
        <location filename="src/ViewerWidget.cpp" line="412"/>
        <source>Pause</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="26"/>
        <source>Advisor MJPEG Viewer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="29"/>
        <source>The Video Viewer</source>
        <translation>El reproductor de video</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="32"/>
        <source>The video viewer</source>
        <translation>El reproductor de video</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="147"/>
        <source>Previous Frame</source>
        <translation>Imagen Previa</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="150"/>
        <source>Prev</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="167"/>
        <source>Play/Pause</source>
        <translation>Reproducir/Pausa</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="213"/>
        <source>Next Frame</source>
        <translation>Proxima Imagen</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="216"/>
        <source>Next</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="240"/>
        <source>Jump to the next alarm event</source>
        <translation>Saltar a la proxima alarma</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="246"/>
        <source>The Advisor MJPEG video can be browse only by looking to the alarm events produced.</source>
        <translation>El reproductor de video MJPEG puede desplazarse en el video utilizando solo los eventos.</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="249"/>
        <source>Jump to Alarm</source>
        <translation>Proxima Alarma</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="277"/>
        <source>00:00:00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="362"/>
        <source>background-color: rgb(255, 255, 255);
border-color: rgb(0, 0, 0);</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="410"/>
        <source>File Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="463"/>
        <source>Date Infos:</source>
        <translation>Fecha:</translation>
    </message>
    <message>
        <location filename="src/ViewerWidget.ui" line="511"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Click on the event list or on alarm and events to open a video&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>WaitingDialog</name>
    <message>
        <location filename="src/WaitingDialog.cpp" line="44"/>
        <location filename="src/WaitingDialog.cpp" line="72"/>
        <source>try %1/%2</source>
        <translation>tentativa %1/%2</translation>
    </message>
    <message>
        <location filename="src/WaitingDialog.ui" line="20"/>
        <source>Waiting ...</source>
        <translation>Connectando ...</translation>
    </message>
    <message>
        <location filename="src/WaitingDialog.ui" line="34"/>
        <source>Wait a moment please ...</source>
        <translation>Esperar un momento, por favor ...</translation>
    </message>
    <message>
        <location filename="src/WaitingDialog.ui" line="70"/>
        <source>try 1/1</source>
        <translation>tentativa 1/1</translation>
    </message>
</context>
<context>
    <name>WarningWidget</name>
    <message>
        <location filename="src/WarningWidget.ui" line="14"/>
        <source>Warnings</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="src/WarningWidget.ui" line="113"/>
        <source>scroll lock</source>
        <translation>bloq despl</translation>
    </message>
    <message>
        <location filename="src/WarningWidget.ui" line="128"/>
        <source>mute</source>
        <translation>silenciar</translation>
    </message>
    <message>
        <location filename="src/WarningWidget.ui" line="151"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
</context>
<context>
    <name>addNewUser</name>
    <message>
        <location filename="src/addNewUser.ui" line="14"/>
        <source>Add User</source>
        <translation>Añadir Usuario</translation>
    </message>
    <message>
        <location filename="src/addNewUser.ui" line="49"/>
        <source>User name:</source>
        <translation>Nombre usuario:</translation>
    </message>
    <message>
        <location filename="src/addNewUser.ui" line="63"/>
        <source>User password:</source>
        <translation>Contraseña:</translation>
    </message>
    <message>
        <location filename="src/addNewUser.ui" line="77"/>
        <source>Repeat password:</source>
        <translation>Repita contraseña:</translation>
    </message>
    <message>
        <location filename="src/addNewUser.ui" line="91"/>
        <source>Select User Level:</source>
        <translation>Seleccione nivel usuario:</translation>
    </message>
    <message>
        <location filename="src/addNewUser.ui" line="105"/>
        <source>Admin User</source>
        <translation>Usuario Administrador</translation>
    </message>
    <message>
        <location filename="src/addNewUser.ui" line="110"/>
        <source>Privacity User Level</source>
        <translation>Usuario nivel Privacidad</translation>
    </message>
    <message>
        <location filename="src/addNewUser.ui" line="115"/>
        <source>Visor Only User Level</source>
        <translation>Usuario nivel Visor</translation>
    </message>
    <message>
        <location filename="src/addNewUser.ui" line="162"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="src/addNewUser.ui" line="169"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="src/addNewUser.cpp" line="56"/>
        <source>User error</source>
        <translation>Error de usuario</translation>
    </message>
    <message>
        <location filename="src/addNewUser.cpp" line="56"/>
        <source>An user with name </source>
        <translation>Un usuario con el nombre</translation>
    </message>
    <message>
        <location filename="src/addNewUser.cpp" line="56"/>
        <source> already exist.</source>
        <translation>ya existe.</translation>
    </message>
</context>
<context>
    <name>usersConfigParams</name>
    <message>
        <location filename="src/usersConfigParams.ui" line="14"/>
        <source>User Config Params</source>
        <translation>Configuración de Usuario</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.ui" line="42"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.ui" line="77"/>
        <source>User</source>
        <translation>Usuario</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.ui" line="95"/>
        <source>User Params</source>
        <translation>Parámetros usuario</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.ui" line="113"/>
        <source>User password:</source>
        <translation>Contraseña:</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.ui" line="127"/>
        <source>Repeat password:</source>
        <translation>Repita contraseña:</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.ui" line="134"/>
        <source>Select User Level:</source>
        <translation>Seleccione nivel usuario:</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.ui" line="148"/>
        <source>Admin User</source>
        <translation>Usuario Administrador</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.ui" line="153"/>
        <source>Privacity User Level</source>
        <translation>Usuario nivel Privacidad</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.ui" line="158"/>
        <source>Visor Only User Level</source>
        <translation>Usuario nivel Visor</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.ui" line="184"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.ui" line="226"/>
        <source>Select user:</source>
        <translation>Seleccionar usuario:</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.ui" line="236"/>
        <source>User Level:</source>
        <translation>Nivel del usuario:</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.ui" line="258"/>
        <source>Change user params</source>
        <translation>Cambiar parámetros del usuario</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.ui" line="278"/>
        <source>Delete Users</source>
        <translation>Eliminar usuario</translation>
    </message>
    <message>
        <location filename="src/usersConfigParams.cpp" line="25"/>
        <source>Warning. No users defined.</source>
        <translation>Atención. No se han definido usuarios.</translation>
    </message>
</context>
</TS>
