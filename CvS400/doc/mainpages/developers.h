/**
 * @page page_developers  Developers: Code Specification
 *
 * If some developers wants to better understand the source code to update it
 * or to modify it this is important because it describes all the specific commands
 * which may be difficult to understand.
 *
 *@li @ref page_developers_fastcompil
 *@li @ref page_developers_friendship
 *@li @ref page_developers_doxygen
 *@li @ref page_developers_wireshark
 *@li @ref page_developers_endianness
 *
 *<hr>
 *
 * @section page_developers_schemas Schemas of the program.
 *
 * @section page_developers_fastcompil Speed up the compilation time
 *
 * To speed the compilation we use the forward declaration:\n
 *
 * When in "B.hpp" there is a pointer to an object A, we can use a forward declaration because
 * we don't need to include the whole "A.hpp" at this moment.\n
 * The object will be therefore completely declared in the file "B.cpp" by writing \#include "A.hpp".\n
 *
 * This will in turn speed a little bit the compilation, because if object A is modified only object B
 * is compiled not all the others file which include object B.
 *
 * B.hpp:
 * \code
 * class A ;
 * class B {
 *    private:
 *       A* ptrA ;
 *    public:
 *       void mymethod(const& A) const ;
 * };
 * \endcode
 *
 * B.cpp:
 * \code
 * #include "A.hpp"
 * B::mymethod() {
 *       A->othermethod();
 * }
 * ...
 * \endcode
 *
 *
 * @see http://en.wikipedia.org/wiki/Forward_declaration
 * @see http://www-subatech.in2p3.fr/~photons/subatech/soft/carnac/CPP-INC-1.shtml
 * @sepline
 *
 *
 *
 * @section page_developers_friendship Relationship Between Objects and their Graphical Interface.
 *
 * In order to improve the code understanding we have separate some objects (e.i., Camera, s400Board)
 * from their corresponding graphical configuration panel (e.i.,PropCamera, PropS400Board). \n
 *
 * However to be able to communicate between these two objects we have added an interface class as  @b friend of the object class.
 * This operation is simply done by adding the @b "friend" keyword in the Object.hpp, such that GraphicalObject.cpp
 * could access to the private member of Object.hpp
 *
 *  @see http://www.cpp-tutorial.com/friend-classes.html
 *  @sepline
 *
 *
 * @sepline
 *
 *
 *
 * @section page_developers_doxygen Doxygen documentation
 *
 * Doxygen is a tool for writing software reference documentation. The documentation is
 * written within code, and is thus relatively easy to keep up to date.
 * Doxygen can crossreference documentation and code, so that the reader
 *  of a document can easily refer to the actual code.
 *  @see http://en.wikipedia.org/wiki/Doxygen
 *
 *
 * @subsection page_developers_doxygen_intall How to install.
 *
 * See the page: @ref page_install_plugins_doxy
 *
 * @subsection page_developers_doxygen_use How to use doxygen.
 *
 *
 * This software use doxygen to provide the documentation you are reading.
 * Some important rules need to be respected.
 *
 * @subsection page_developers_doxygen_own Own Commands.
 *
 * Some aliases has been created to ease the way to write comment. It is advised to use them!
 *
 * @li <tt>\@path:</tt> Create a link from the main directory of VICIA.
 * @li <tt>\@false, \@true, \@NULL:</tt> when you write true, false and NULL with their C++ semantic meaning.
 * @li <tt>\@sepline:</tt> Used to create a separating line between the different sections.
 * @li <tt>\@param_parent and \@param_parent_null:</tt> Print the default text when we use a wxWindow *parent.
 *
 * In order to build the documentation you need to integrate the following code in your Doxyfile.
 * \code
ALIASES += img{1}="<img src='../images/\1'>"
ALIASES += thumbs{1}="<div align='center' onclick=\"javascript:window.open('../images/\1')\">
<img id=\"\1\" src='../images/thumbs/\1'><a class='internal' href='../images/\1' target='about:blank'>Click here top open</a></div>"
ALIASES += tt{1}="<tt>\1</tt>"
ALIASES += ttb{1}="<tt><b>\1</b></tt>"
ALIASES += sepline="<div class=\"sepline\"></div>"
ALIASES += br="<br>"
ALIASES += n="<br>"
ALIASES += t="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
ALIASES += param_parent="@param parent Parent window. Should not be @NULL."
ALIASES += param_parent_null="@param parent Parent window.	Can be @NULL."
ALIASES += true="<span class='literal'>true</span>"
ALIASES += false="<span class='literal'>false</span>"
ALIASES += NULL="<span class='literal'>NULL</span>"
 *
 *
 * \endcode
 *
 * @sepline
 *
 *
 * @section page_developers_wireshark Wireshark protocol dissector
 *
 * Wireshark is a well known network protocol analyzer.
 *
 * @subsection page_developers_wireshark_install Install Wireshark.
 *
 * First download and install the program: http://www.wireshark.org/download.html.
 *
 * If your are using a standard unix distribution you find standard package.
 *
 * e.g. for debian/ubuntu:
 * @code sudo apt-get install wireshark @endcode
 *
 * @note Under Unix, wireshark need to grant root privilege to capture packet on the ethernet interface.
 * If you want to remove the password for wireshark just add the line:
 * @code%admin ALL=NOPASSWD: /usr/bin/wireshark@endcode
 * to the file @tt{/etc/sudoers}
 *
 * @subsection page_developers_wireshark_copy Copy s4n.lua protocol.
 *
 * Then if you want to read the lua protocol you need to copy the file @path{devtools/s4n.lua} to the wireshark directory
 *
 * - Under windows: @tt{C:\\Archivos de programa\\Wireshark}
 * - Under ubuntu: @tt{/usr/share/wireshark/}
 *
 * You can also copy the @path{devtools/rtp.lua} script which automatically decode the UDP packet using port [50000,50011] as RTP.
 *
 * @subsection page_developers_wireshark_activate Activate LUA script.
 *
 * Go to the wireshark directory and edit the file: @tt{init.lua}
 *
 * Replace the line:
 * @code disable_lua = true; do return end; @endcode
 * By the line:
 * @code --disable_lua = true; do return end; @endcode
 *
 * And add to the end of the file the line
 * @code
 * dofile("s4n.lua")
 * dofile("rtp.lua")
 *  @endcode
 *
 *
 * @sepline
 *
 * @section page_developers_endianness Endianness problem.
 *
 * As this software use network communication, we are faced to the endianness problem.
 *
 * We have 3 different numbers to write in the memory: @tt{2696004307} (0xA0B1C2D3), @tt{41137} (0xA0B1), @tt{160} (0xA0).
 * This number can be written in memory in different way.
 *
 * 	- Big endian (Network,SUN,Sparc,...) :
 *
 * 	The most significant byte (MSB) value, which is 0xA0 in our example, is stored at the memory location with the lowest address, the next byte value in significance,
 * 0xB1, is stored at the following memory location and so on.
 * This is akin to Left-to-Right reading order in hexadecimal.
 *
 * 	@code
 *  2696004307  =>  | A0 | B1 | C2 | D3 |
 *  0000041137  =>  | 00 | 00 | A0 | B1 |
 *  0000000160  =>  | 00 | 00 | 00 | A0 |
 * @endcode
 *
 * An often cited argument in favor of big-endian is that it is consistent with the ordering
 * commonly used in natural languages. During debugging the value in hexadecimal in the memory
 * is exactly the same as the one which can be printed with printf("%X",val);
 *
 *
 * 	- Little endian (x86, ...)
 *
 * The least significant byte (LSB) value, 0xD3, is at the lowest address.
 * The other bytes follow in increasing order of significance.
 *
 * 	@code
 *  2696004307  =>  | D3 | C2 | B1 | A0 |
 *  0000041137  =>  | B1 | A0 | 00 | 00 |
 *  0000000160  =>  | A0 | 00 | 00 | 00 |
 *  @endcode
 *
 * The little-endian system has the property that, in the absence of alignment restrictions, the same value can be read from memory at different lengths without using different addresses.
 * For example, a 32-bit memory location with content @tt{| A0 | 00 | 00 | 00 |} can be read at the same address as either 8-bit (value = 0xA0), 16-bit (0x00A0), or 32-bit (0x000000A0),
 * all of which retain the same numeric value (160).
 *
 *
 * @see http://en.wikipedia.org/wiki/Endianess
 *
 * @subsection page_developers_endianess_solution The Endianness solution.
 *
 *  As we have seen the network, and the x86 processor doesn't store in the same way the information.
 *  Therefore we need to find a solution to solved this problem:
 *
 *  The Berkeley sockets API defines a set of functions to convert 16- and 32-bit integers to
 *  and from network byte order: the htonl (host-to-network-long) and htons (host-to-network-short)
 *  functions convert 32-bit and 16-bit values respectively from machine (host) to network order.
 *
 *  The other problem is the use of the bit notation in C.
 *  For example this is the bit notation on 32-bits of the part of the RTP header.
 *
 * @code
 *  struct uint32_le {
 *	unsigned int x:4;			//!< header extension flag
 *	unsigned int cc:4;			//!< CSRC count
 *	unsigned int m:1;			//!< marker bit
 *	unsigned int pt:7;			//!< payload type
 *	unsigned int seq:16;		//!< sequence number
 *	};
 *	@endcode
 *
 * Therefore, as we use the ntohl() function we need to inverse the field to obtain
 * a correct conversion from the network notation to the x86 notation.
 *
 *  @code
 *  struct uint32_le {
 *	unsigned int seq:16;		//!< sequence number
 *	unsigned int pt:7;			//!< payload type
 *	unsigned int m:1;			//!< marker bit
 *	unsigned int cc:4;			//!< CSRC count
 *	unsigned int x:4;			//!< header extension flag
 *	};
 *	@endcode
 *
 * @see S4NSocket::pbyte_ntohl(),  S4NSocket::buff_ntohl.
 * @see rtp_hdr_t, jpeghdr_s, jpeghdr_rst_s, ...
 *
 *
 *
 */
