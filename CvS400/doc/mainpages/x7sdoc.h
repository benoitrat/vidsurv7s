/**
 *  @page page_x7s The X7S libraries (eXtend 7even Solutions).
 *
 *
 * The library used for communication, computer vision processing and parameters manipulation
 *
 * @dotfile x7s_dependency.dot "X7S Dependencies"
 *
 * @section page_x7s_net libx7snet
 *
 *
 *
 * @section page_x7s_cv libx7scv
 *
 * This library extends the OpenCV library:
 *
 * The functionalities of this library are divided in the following groups:
 *
 *		- @ref page_x7s_cv_type
 *		- @ref page_x7s_cv_core
 *		- @ref page_x7s_cv_colcvt
 *		- @ref page_x7s_cv_bg
 *		- @ref page_x7s_cv_cc
 *		- @ref page_x7s_cv_mjpeg
 *		- @ref page_x7s_cv_filter
 *		- @ref page_x7s_cv_miscip
 *		- @ref page_x7s_cv_drawdisp
 *		- @ref page_x7s_cv_tools
 *

 *

 *
 * @sepline
 *
 * @section page_x7s_xml libx7sxml
 *
 *
 * @section page_x7s_io libx7sio

 *
 * @section page_x7s_vidsurv libx7svidsurv

 *
 *
 *
 *
 *
 *
 *
 * @defgroup x7snet libx7snet
 * @brief Handle the reception of X7sNetFrame from a board though internet.
 *
 * @defgroup x7scv libx7scv
 * @brief This library extends the OpenCV library.
 *
 * The functionalities of this library are divided in the following groups:
 *
 *		- @ref page_x7s_cv_type
 *		- @ref page_x7s_cv_core
 *		- @ref page_x7s_cv_colcvt
 *		- @ref page_x7s_cv_bg
 *		- @ref page_x7s_cv_cc
 *		- @ref page_x7s_cv_mjpeg
 *		- @ref page_x7s_cv_filter
 *		- @ref page_x7s_cv_miscip
 *		- @ref page_x7s_cv_drawdisp
 *		- @ref page_x7s_cv_tools
 *
 * @note All the code of this library is written in C.
 *
 * @sepline
 *
 *
 * @section page_x7s_cv_type  The type of image/matrix/data.
 *
 *  The type of the image/matrix elements is given like in OpenCV library with the following form:
 *
 *  @b CV_XCYZ, where :
 *  	- X is the number of channel (X=[1,4]).
 *  	- Y if the number of bit for one element (Y={8,16,32,64}).
 *  	- Z correspond to the bits representation : S=signed, U=unsigned, F=float.
 * CV_8UC1 means the elements are 8-bit unsigned and the there is 1 channel, and CV_32SC2 means the elements are 32-bit signed and there are 2 channels.
 *
 *  For example the following type represent typical images:
 *		- @anchor CV_8U1C CV_8UC1: Use in grey mask.
 *		- @anchor CV_8U2C CV_8U2C: Use for YUV422 images.
 *		- @anchor CV_8U3C CV_8U3C: use as BGR,RGB,HSV,... image
 *		- @anchor CV_8U4C CV_8U4C: use as BGRA,RGBA,... image.
 *		- @anchor CV_32FC3 CV_32FC3: use to compute the mean of BGR image.
 * @sepline
 *
 * @section page_x7s_cv_core Core/Memory Managment
 *
 * Functions used to create simple memory buffer, read memory to specific buffer or convert memory structures:
 *
 * @ref x7sCreateByteArray(), x7sReleaseByteArray(), x7sByteArray(), x7sByteArrayToCvMat(),
 * @ref x7sMemWrite(), x7sMemRead()
 * @see x7sCore.c
 * @sepline
 *
 *
 * @section page_x7s_cv_colcvt Color Convertion
 *
 * Color convertion and creation of the IplImage to be converted.
 *
 * @ref x7sCvtColor(), x7sCreateCvtImage(), x7sCvtHue2BGRScalar(), x7sCvtHue2RGBScalar()
 *
 * The convertion is given by a convertion code @ref  X7S_CV_xxx
 * @see x7sCvtColor.c, @cv{cvCvtColor}
 * @sepline
 *
 *
 * @section page_x7s_cv_bg Background/Foreground models.
 *
 * A collection of functions and structures to use BG/FG modeling:
 *
 *		- Creation of the parameters: @ref x7sCreateBGStatModelParams(), x7sReleaseBGStatModelParams().
 *		- Generic method for creation of models: x7sCreateBGModelType(), x7sCreateBGModel().
 *		- Creation of out own models: @ref x7sCreateClusterBGModel(), x7sCreateBCDistBGModel().
 *
 * @see http://opencv.willowgarage.com/wiki/VideoSurveillance
 * @see x7sBGCommon.h, x7sBGCommon.c, x7sBGBCDist.c, x7sBGCluster.c
 * @sepline
 *
 *
 * @section page_x7s_cv_cc Connected Component
 *
 * Functions used to create/use/delete connected components @ref X7sConCompo and the @ref X7sConCompoList
 *
 * @ref x7sCreateConCompoList(), x7sCreateConCompoList(), x7sReleaseConCompoList() x7sResetConCompoList(),
 * @ref x7sFillConCompoList(), x7sUpdateConCompoList(), x7sSortConCompoList(),  x7sPrintConCompo()
 *
 * @see x7sConCompo.c
 * @sepline
 *
 *
 *
 * @section page_x7s_cv_filter Filtering
 *
 * Specific filtering on Image:
 * @ref x7sPseudoMed(), x7sErodeFast(), x7sDilateFast(), x7sFillHoles(), x7sFillLineHoles(), x7sFillEdges()
 *
 *
 * @see x7sMorphological.c, x7sPseudoMed.c, x7sFill.c
 * @sepline
 *
 *
 * @section page_x7s_cv_mjpeg MJPEG and Special JPEG functions.
 *
 * The following can be divided in three groups:
 * 		- MJPEG file IO: @ref x7sOpenMJPEG(), x7sReleaseMJPEG(), x7sWriteMJPEG(), x7sReadFileMJPEG().
 * 		- MJPEG file manipulation: @ref x7sCTime(), x7sFindTimeMJPEG(), x7sFindTagMJPEG(),  x7sGetFpsMJPEG().
 * 		- Create special JPEG: @ref x7sFillMJPEG(), x7sExtractMJPEG().
 *
 *
 * @see x7sCvMJPEG.c
 * @sepline
 *
 *
 * @section page_x7s_cv_miscip Misc Image Processing
 *
 * Several processing function on image:
 * @ref x7sMakeBlobs(), x7sFindMax(), x7sDownSample()
 *
 * @see x7sFindMax.c, x7sDownSample.c
 * @sepline
 *
 *
 * @section page_x7s_cv_drawdisp Draw/Display.
 *
 * 	The following functions are used :
 *		- To play with transparency (alpha layer): @ref x7sAddWeightedScalar(), x7sAddAlphaLayer().
 *		- To draw on images: @ref x7sDrawLegend(), x7sDrawCross(), x7sDrawConCompoList(), x7sDrawMultiImages().
 *		- To easily display images in specific format: x7sShowImage(), x7sShowImageRescale(), x7sShowHistogram().
 * @see x7sCvAdd.c, x7sDraw.c,  x7sDisplay.c
 * @sepline
 *
 *

 *
 * @section page_x7s_cv_tools OpenCV Tools
 *
 * A collection of several tools that improve OpenCV library:
 * 		-	@ref x7sCvLog_SetLevel()
 * 		-	@ref x7sReadUIntByName(), x7sIplPrintInfos(), x7sIplPrintValues(), x7sIplPrintValuesFull().
 *
 * @see x7sCvLog.c, x7sTools.c
 * @sepline
 *
 *
 *
 *
 * @defgroup x7sxml libx7sxml
 * @brief Library to manage parameters easily from an XML file.
 *
 * @defgroup x7sio libx7sio
 * @brief Extend the openCV highgui library for input/output.
 *
 * @defgroup x7svidsurv libx7svidsurv
 * @brief Extend the openCV vidsurv library with our definition.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
