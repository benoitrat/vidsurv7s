/**
 *  @page page_avo The Advisor Software (Advanced Vision Organizer)
 *
 * All the sub-softwares in AdVisOr use the QT4 library to:
 * 		- Manage different threads.
 * 		- Connect through internet.
 * 		- Connect to the database.
 * 		- Display the widgets (GUI).
 *
 *
 * The structure of the main software:
 *
 * @section page_avo_core AdVisOr Core
 *
 * This branch of the code contains the usefull fonction to build any type of
 * software to manage the different Board, Camera.
 *
 * @dotfile avo_structure.dot ""
 *
 *
 * All these entity can be configure through Devices Class and with an XML file.
 * An example of the XML file is given :
 *
 * @code
 *  <devices ID="-1" type="-1">
 *       <board ID="0" type="-1">
 *           <camera ID="0" type="-1">...</camera>
 *           <camera ID="1" type="-1">...</camera>
 *           <camera ID="2" type="-1">...</camera>
 *           <camera ID="3" type="-1">...</camera>
 *       </board>
 *       <board ID="1" type="-1">
 *           <camera ID="0" type="-1">...</camera>
 *           <camera ID="1" type="-1">...</camera>
 *           <camera ID="2" type="-1">...</camera>
 *           <camera ID="3" type="-1">...</camera>
 *       </board>
 * </devices>
 * @endcode
 *
 * @subsection page_avo_core_log The Log class.
 *
 * The @ref avo_core provides log functions with different level.
 *
 * These functions used the Log class but it is recommended to use the @tt{\#define} function instead:
 *
 *		- @ref AVO_PRINT_ERROR()  Print an error message in stderr.
 *		- @ref AVO_PRINT_WARN()	Print an warning message in stdout.
 *		- @ref AVO_PRINT_INFO()	Print an info message (trace).
 *		- @ref AVO_PRINT_DEBUG()  (Only used when the macro DEBUG if defined at precompilation).
 *		- @ref AVO_PRINT_DUMP()	(Only used when the macro DEBUG if defined at precompilation).
 *
 *
 *
 * @subsection page_avo_core_other The Other classes.
 *
 * 	The other classes are:
 * 		- Database
 * 		- MjpegHandler
 *
 *
 *
 * @sepline
 *
 * @section page_avo_simple AdVisOr Simple
 *
 * The @ref avo_simple software contains simple code to test the core functions. It has the following properties:
 *
 * 		- It does not used any GUI from QT.
 * 		- It does not used any thread.
 * 		- It is not focus on client/server model.
 * 		- It can only be used with 1 Board (sequentially).
 *
 *
 *
 * @sepline
 *
 *
 * @section page_avo_server AdVisOr Server
 *
 * The Server software has the following properties:
 *		- Use QT library.
 *		- Use two threads for each Board.
 *		- Must open a RTP port for each Board.
 *		- Must open a port to communicate with the @ref avo_client.
 *		- Communicate with the @ref avo_client using TCP/IP socket.
 *
 * @sepline
 *
 * @section page_avo_client AdVisOr Client
 *
 * The Client software has the following properties:
 *		- Use QT library.
 *		- Use one thread for display and one for the communication.
 *		- Communicate with the @ref avo_server using TCP/IP socket.
 *
 *
 * @sepline
 *
 * @section page_avo_viewer AdVisOr Viewer
 *
 * The Viewer software has the following properties:
 *		- Use QT library.
 *		- Open a *.mjpeg file
 *		- Visualize the video with the metadata.
 *
 *
 * @sepline
 *
 *
 * @defgroup avo_core AdVisOr Core
 * @brief Contains the "tool" objects used by the server, client, player, ...
 *
 * @defgroup avo_widget AdVisOr Widgets
 * @brief Contains some graphical widget that can be used by the server, client, player ...
 *
 * @defgroup avo_simple AdVisOr Simple
 * @brief Contains a simple server/client for test purpose.
 *
 * @defgroup avo_server AdVisOr Server
 * @brief Contains the file for the Server
 *
 * @defgroup avo_client AdVisOr Client
 * @brief Contains the file for the Client.
 *
 * @defgroup avo_player AdVisOr Player
 * @brief Contains the file for the Player
 *
 * @defgroup avo_viewer AdVisOr Viewer
 * @brief Contains the file for the Viewer
 *
 *
 *
 *
 *
 *
 *
 */
