/**
 * @page page_config XML Configuration Files
 *
 * The program is going to look for 2 XML files.
 *
 * @section page_config_global The global configuration
 *
 * This configuration file contains the standard parameters which are used
 * for all the videos. It it can be useful to set up the default file path for
 * video if you are using the same type of database from different location.
 *
 * A sample of this file can be found in @path{main_confsample.xml}.
 *
 * @section page_config_media The media configuration
 *
 * This file can set specific parameters for one video.  The parameters set
 * here will overwrite the one from the general config.xml.
 *
 * A sample of this file can be found in @path{media_confsample.xml}.
 *
 * Here you can look at an example of this configuration file that you can find in @path{test_set/}:
 *
 * @code
<opencv_storage>
	<input>
		<type>3</type>  <!-- SEQ=1,FILE=2,VIDEO=3,CAMERA=4 -->
		<fpath>D:\DATABASE\PETS2001\DS01_Te_cam1.avi</fpath>
		<f_start>20</f_start>
		<f_end>-1</f_end>
		<width>360</width>
		<height>288</height>
		<nMaxErrors>10</nMaxErrors>
	</input>
	<background>
		<type>12</type> <!-- FGD=0,MOG=1,MOC=10,HW_MOC=11,HOR=12 -->
		<bg_cluster>
			<K>3</K>
			<T>20</T>
			<distance>14</distance>
			<insDelay>1</insDelay> <!-- SLOW=0, NORMAL=1, FAST=2 -->
		</bg_cluster>
		<bg_horprarset>
			<threshold>25</threshold>
			<refreshDelay>65</refreshDelay>
			<insDelay>0</insDelay> <!-- SLOW=0, NORMAL=1, FAST=2 -->
		</bg_horprarset>
		<bg_gauss>
			<bg_threshold>0.7</bg_threshold>
			<std_threshold>5</std_threshold>
			<minArea>15</minArea>
		</bg_gauss>
	</background>
</opencv_storage>
@endcode
 *
 *
 *
 */
