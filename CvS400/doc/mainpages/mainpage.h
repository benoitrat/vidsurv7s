/**
 *  @mainpage
 *  @date 21-jan-2009
 *  @author Benoit RAT
 *
 * @image html logo.png
 *
 * - #1. @ref page_install
 * 		-# @ref page_install_lib
 * 		-# @ref page_eclipse
 * 		-# @ref page_install_cmake
 *
 * - #2. @anchor OS Specific
 * 		-# @ref page_win
 * 		-# @ref page_unix
 *
 * - #3. @ref page_x7s
 *		-	@ref x7scv
 *		-	@ref x7snet
 *		-	@ref x7sxml
 *		-	@ref x7sio
 *		-	@ref x7svidsurv
 *
 * - #4. @anchor The Samples Programs
 *		-# @ref samples
 *
 * - #5. @ref page_config
 *		-# @ref page_config_global
 *		-# @ref page_config_media
 *
 *	-#6. @ref page_stats
 *
 *
 * @section extra_doc Extra documentation:
 *
 * There is an extra documentation that you can find in the directory:
 *
 *	-	The old roadmap of the project: @path{doc/roadmap.htm}
 * 	-	The documentation for the first stage: @path{doc/stage_1.pdf}.
 * 	-	The schema of the tracking: @path{doc/schema.ppt}
 * 	-	The bibliography for the background substraction: @path{doc/bibliography/}.
 *
 *
 * @defgroup samples Samples
 * @brief Some sample program to understand and verify how work the X7S library.
 *
 *
 */
