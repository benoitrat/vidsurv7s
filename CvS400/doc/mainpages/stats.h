/**
 * @page page_stats Network statistic and devices capabilities.
 *
 *  This page resume the statistic of transmission/emission.
 *
 *
 * @li @ref page_stats_bw
 * @li @ref page_stats_cpu
 * @li @ref page_stats_dspace
 *
 * @sepline
 *
 * @section page_stats_cpu  CPU power
 *
 * 		- Advisor Server:
 * 				- 1 Board, 4 cam Vidsurv, 2 alarms, no GUI < 30 % of Atom 2x1.60GHz
 *
 *
 * @section page_stats_bw  Network bandwidth
 *
 * 		- 1 Board => Server : ~900 KiB/s (< 1 MiB/s)
 *     - Client <=  Server (1 Board) : ~500 KiB/s
 *
 *     @note KiB is the unity for kibibyte (kilo binary byte), 1 Kib <=> 1024 bytes.
 *
 * @sepline
 * @section page_stats_dspace  Disk space
*
* Maximum size for one camera (limited by JPEG size, and MData size):
* @code
* max_jpegsize (100) x max_fps (15) x 3600 = 5.2 GB/h (~ 125 GB/day)
* @endcode
*
* Average+ size for one camera:
* @code
* 	600 MB/h <=> 15 GB/day <=> 105 GB/Week
* @endcode
*
*
*
*
* @sepline


