/**
 * @page page_unix  How to use the applications under unix (Ubuntu,Debian,...)
 *
 *
 * All the test has been done under Ubuntu 9.04, if you have some problem with other system please contact us.
 *
 *@li @ref page_deb_opencv
 *@li @ref page_deb_qt4
 *@li @ref page_deb_others
 *
* @section page_deb_opencv OpenCV
 *
 * The installation of OpenCV must still be done by compiling the sourcefile.
 *
 * Be sure to have at least the 2.0 version or more recent.
 *
 * You might need also to install:
 * libgtk2.0-dev.
 *
 *
 * @sepline
 * @section page_deb_qt4 Qt4 library.
 *
	-	libqwt5-qt4
	-	libqwt5-qt4-dev
	-	libqt4-sql-psql

		@code
	sudo apt-get install libqwt5-qt4 libqwt5-qt4-dev libqt4-sql-psql
	@endcode


    @sepline
	@section page_deb_others Other components

	@subsection page_deb_others_pgsql PostgreSQL

	You should install PostgreSQL database and configure it (look at https://help.ubuntu.com/community/PostgreSQL).

	@code
	## Download postgres
	sudo apt-get install postgresql

	## Login as postgres user
	sudo -u postgres psql template1

	## Setup the password
	\password postgres
	@endcode

	You may also install the admistration tool
	@code
	sudo apt-get install pgadmin3
	@endcode

	You should create an advisor user with its own advisor database, this could be done using SQL query or with pgadmin3

	Without tablespace:
	@code
	CREATE USER advisor WITH PASSWORD 'advisor' NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE;
	CREATE DATABASE advisor WITH OWNER = advisor ENCODING = 'UTF8';
	@endcode

	With tablespace
	@code
	CREATE USER advisor WITH PASSWORD 'advisor' NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE;
	CREATE TABLESPACE avo_space1 OWNER advisor LOCATION '/media/DATA1/postgresql';
	CREATE DATABASE advisor WITH OWNER = advisor ENCODING = 'UTF8' TABLESPACE avo_space1;
	@endcode

*/
