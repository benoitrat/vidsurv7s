/**
 * @page page_netp  Internet protocol (RTP, RTSP, UDP).
 *
 * This page resume the protocol used to build the vicia application.
 *
 *@li @ref page_netp_udp
 *@li @ref page_netp_rtp
 *@li @ref page_netp_rtsp
 *@li @ref page_netp_ex
 *@li @ref page_netp_tools

 *
 *<hr>
 *
 * @section page_netp_udp UDP protocol in Vicia
 *
 * can be
 * The UDP protocol works over the IP and MAC layer. Here we have a sample of the UDP/IP
 * header used in our application (7x32 = 224 bits = 28 bytes).
 *
 *
 @code
 ||----------------------------------------------------------------------------------------------------------||
 || 00 01 02 03 04 05 06 07 || 08 09 10 11 12 13 14 15 || 16 17 18 19 20 21 22 23 || 24 25 26 27 28 29 30 31 ||
 ||-------------------------||-------------------------||-------------------------||-------------------------||
 ||  version   |    IHL     ||          TOS            ||                   Total Length                     ||
 ||-------------------------||-------------------------||-------------------------||-------------------------||
 ||                   Identification                   ||  Flags  |        Fragment offset                   ||
 ||-------------------------||-------------------------||-------------------------||-------------------------||
 ||           TTL           ||         Protocol        ||                  Header Checksum                   ||
 ||----------------------------------------------------------------------------------------------------------||
 ||                                             Source IP Protocol                                           ||
 ||----------------------------------------------------------------------------------------------------------||
 ||                                          Destination IP Protocol                                         ||
 ||----------------------------------------------------------------------------------------------------------||
 ||                    Source Port                     ||                  Destination Port                  ||
 ||-------------------------||-------------------------||-------------------------||-------------------------||
 ||                     Length                         ||                    Data Checksum                   ||
 ||----------------------------------------------------------------------------------------------------------||
 ||                                                   Data                                                   ||
 ||                                                   ....                                                   ||
 ||                                                   ....                                                   ||
 ||----------------------------------------------------------------------------------------------------------||
 @endcode
 *
 *
 *
 * @section page_netp_rtp RTP protocol (Unicast)
 *
 * The Real-time Transport Protocol (or RTP) defines a standardized
 * packet format for delivering audio and video over the Internet.
 * RTP was originally designed as a multicast protocol, but has since
 * been applied in many unicast applications. For host-to-host transport,
 * RTP and RTCP use the User Datagram Protocol (UDP) predominantly,
 * although other Transport Layer protocols can be used.
 *
 * According to RFC 1889, the services provided by RTP include:
 *
 *		- Payload-type identification - Indication of what kind of content is being carried
 *		- Sequence numbering - PDU sequence number
 *		- Time stamping - allow synchronization and jitter calculations
 *		- Delivery monitoring
 *
 *
 * RTP does not operate on assigned or standardized TCP or UDP ports. However,
 * usually it uses even port numbers. RTCP uses the next higher odd port number.
 * Although there are no standardized recommendations, RTP used the port 5004 as default port.
 * (e.g. 5004 for Video, 5006 for audio)
 *
 *
 * @see http://tools.ietf.org/html/rfc3550#section-5
 * @see http://www.networksorcery.com/enp/protocol/rtp.htm
 *
 *
 @code
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | V |P|X|  CC   |M|     PT      |       sequence number         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                           timestamp                           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |           synchronization source (SSRC) identifier            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   @endcode
 *
 *		- Version (V): 2 bits.
 *	 		V=2 (Version 2 of RTP)
 *
 *		- Padding (P): 1 bit.
 *			P=0 (No padding).
 *
 *		- Extension (X) : 1 bit.
 *	 		X=0 because we don't use extension.
 *
 *		- CSRC count (CC): 4 bits.
 *			In Unicast, there are no other contributing sources so we put this field to 0.
 *
 *		- Marker bit (M): 1 bit.
 *			- For MJPEG: M=0 normally, but M=1 for end of frame.
 *			- For H263: ....
 *
 *		- Payload type (PT): 7 bits
 *			- For MJPEG: PT=26
 *			- For H263:  PT=34
 *
 *		- Sequence number: 16 bits
 *			The sequence number increments by one for each RTP data packet
 *			sent, and may be used by the receiver to detect packet loss and to
 *			restore packet sequence.
 *
 *		- Timestamp:  32 bits.
 *			The timestamp reflects the sampling instant of the first octet in the RTP
 *			data packet. Timestamp is the SAME for all the packets that correspond to
 *			the same frame.
 *
 * 		- The Synchronization Source (SSRC): 32 bits
 *			The source of a stream of RTP packets, identified by a 32-bit numeric
 *			SSRC identifier carried in the RTP header so as not to be dependent
 *			upon the network address. The This identifier SHOULD be chosen randomly.
 *			All packets from a synchronization source form part of the same timing and
 *			sequence number space, so a receiver groups packets by synchronization source
 *			for playback.
 *			In our case, each camera SHOULD have a different SSRC so that we could identify
 *			a frame packet received without looking at the IP address.
 *
 * @subsection page_netp_rtp_rtpc Real-time control protocol.
 *
 * RTCP provides out-of-band control information for an RTP flow. It partners RTP in the
 * delivery and packaging of multimedia data, but does not transport any data itself.
 * When used in conjunction with RTCP, the RTP specifications mandate that the overall
 * bandwidth required for RTCP does not exceed 5% of the RTP traffic.
 *
 * Moreover RTCP, in mainly used for multicast flow control. As our application focus only
 * on unicast packet, we only need few packets of RTCP to control the flow and avoid the saturation of
 * the bandwidth.
 *
 * The two types of packet that we SHOULD use are :
 * 		- Sender Report: The sender report is sent periodically by the active senders in a conference to report transmission and reception statistics for all RTP packets sent during the interval. The sender report includes an absolute timestamp, which is the number of seconds elapsed since midnight on January 1, 1900. The absolute timestamp allows the receiver to sychronize different RTP messages. It is particularly important when both audio and video are transmitted (audio and video transmissions use separate relative timestamps).
 *		- Application-Specific Message:  The application-specific message is a packet for an application that wants to use new applicatons (not defined in the standard). It allows the definition of a new message type.
 *
 *
 * @subsection page_netp_rtp_mjepg RTP with JPEG frames.
 *
 * In the JPEG format, most of the table-specification data rarely changes from
 * frame to frame within a single video stream.  Therefore RTP/JPEG data
 * is represented in abbreviated format, with all of the tables omitted
 * from the bit stream where possible.  Each frame begins immediately
 * with the (single) entropy-coded scan.  The information that would
 * otherwise be in both the frame and scan headers is represented
 * entirely within the RTP/JPEG header (defined below) that lies between
 * the RTP header and the JPEG payload.
 *
 *
 * Restart mode:
 *  To support partial frame decoding, the frame is broken into "chunks"
 *  each containing an integral number of restart intervals.
 *
 *
 @code
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | Type-specific |              Fragment Offset                  |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |      Type     |       Q       |     Width     |     Height    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |       Restart Interval        |F|L|       Restart Count       | (Optional)
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 @endcode
 *
 *  	- Type-Specific: 8 bits.
 *	 		Depends on the value of the type fields.
 *	 		- If Type in {0,1,64,65}
 *	 			- 0 => progressively scanned (what we use).
 *	 			- 1 => odd interlaced.
 *	 			- 2 => even interlaced.
 *
 *		- Fragment offset: 24 bits.
 *			The offset in the JPEG Payload.
 *
 *		- Type : 8 bits.
 *			- 0 =>  YUV color space interleaved 4:2:1 , one scan, progressive scan, sequential mode, DCT.
 *			- 1 =>  YUV color space interleaved 4:2:2 , one scan, progressive scan, sequential mode, DCT.
 *			- 64 => YUV color space interleaved 4:2:1 , one scan, progressive scan, sequential mode, DCT, restart.
 *			- 65 => YUV color space interleaved 4:2:2 , one scan, progressive scan, sequential mode, DCT, restart.
 *
 *		- Quantification table index (Q):	8 bits.
 *			- X in [1,99] where 1 is low quality and 99 is high quality.
 *			- X can also be 100,101,102 for the moment (but this is not standard)
 *
 *		- Width (multiple of 8): 8 bits.
 *			The value for our image is 352/8 = 44 = 0x2C.
 *
 *		- Height (multiple of 8): 8 bits.
 *			The value for our image is 288/8 = 36 = 0x24.
 *
 * Then if use the restart mode we have:
 *
 *		- Restart Interval: 16 bits.
 *
 *			The size of the chunk 	<= MAX_PACKET_PAYLOAD 	\n\t\t
 *									<= MTU  - PACKET_HEADER	\n\t\t
 *									<= 1500 - 3*4 (RTPJPEG) + 3*4 (RTP) + 7*4 (UDP/IP) + 14 (MAC) \n\t\t
 *									<= 1500 - 66			\n\t\t
 *									<= 1434 .				\n
 *
 *		- First MCU in this packet (F): 1 bit.
 *		- Last MCU in the packet (L): 1 bit.
 *			In our case we have a complete restart chunk so we have the first and last MCU of this chunk.
 *
 *		- Restart count:
 *			Count of the restart chunk.
 *
 * @see http://tools.ietf.org/html/rfc2435 http://www.cs.sfu.ca/CC/365/mark/material/notes/Chap4/Chap4.2/Chap4.2.html
 *
 *
 *
 *
 * @section page_netp_rtsp RTSP protocol.
 *
 *
 * The protocol is similar in syntax and operation to HTTP, but RTSP adds new requests.
 * While HTTP is stateless, RTSP is a stateful protocol. A session ID is used to keep track
 * of sessions when needed. This way, no permanent TCP connection is needed.
 * RTSP messages are sent from client to server, although some exceptions exist where the
 * server will send to the client. Below are the basic RTSP requests.
 * A number of typical HTTP requests, like an OPTION request, are also frequently used.
 * The default port is 554
 *
 * @see http://tools.ietf.org/html/rfc2326#ref-H14.8, http://en.wikipedia.org/wiki/Real_Time_Streaming_Protocol
 *
 *
 * 	-	Init:
 *  		The initial state, no valid SETUP has been received yet.
 *
 * 	-	Ready:
 * 		Last SETUP received was successful, reply sent or after
 * 		playing, last PAUSE received was successful, reply sent.
 *
 * 	-	Playing:
 * 		Last PLAY received was successful, reply sent. Data is being
 * 		sent.
 *
 * The state machine on server:
 *
@code
     state           message received  next state
---------------------------------------------------
     Init            SETUP             Ready
                     TEARDOWN          Init
     Ready           PLAY              Playing
                     SETUP             Ready
                     TEARDOWN          Init
                     RECORD            Recording
     Playing         PLAY              Playing
                     PAUSE             Ready
                     TEARDOWN          Init
                     SETUP             Playing


@endcode
 *
 * 	- @b The_setup:
 *
 * Client -> Servers
@code
SETUP rtsp:<filename> RTSP/1.0
CSeq: 1
Transport: MP2T/H221/UDP;unicast;destination=000!192.168.0.1!192.168.0.2!1234
@endcode
 *
 *
 * @section page_netp_tools Network tools:
 *
 * - Wireshark (Windows & Linux)
 * Capture, monitor and analyse the network traffic.
 *
 * - TCPReplay	(Linux & windows with cygwin)
 * Replay a flow of packet captured by wireshark.
 *
 * Need to be use from another computer, if you want to use the socket.
 * @code
 * sudo tcpreplay -i eth0 -loop=1000 -o capture.pcap //Wait the user key to send packet.
 * @endcode
 *
 * - Netdude 	(Linux)
 * Edit the MAC, IP, UDP layer of many packet in a libpcap capture.
 * Edit the payload of a packet.
 *
 *
 * @section page_netp_ex Example of Network protocol:
 *
 *  - Video-On-Demand over RTSP/UDP (H.264 Video).
 *
 * Go to the webpage to generate the packet: http://www.streamzilla.eu/samples/QuickTime_H264_RTSP_Web.html
 * Or try
 *  @code
 *  mplayer rtsp://qt3.streamzilla.jet-stream.nl/jet-stream/demostreams/goldfish_H264_Web.mp4
 *  @endcode
 *
 *  The capture of the packet by wireshark 1.0.3 can be found on @path{doc/external/network/vod-rtsp_udp.pcap}
 *
 *  - Voice-IP over H.245/RTP.
 *
 *  This packet is generate by the wireshark team: http://wiki.wireshark.org/RTCP
 *
 *  The capture of the packets by wireshark 1.0.3 can be found on @path{doc/external/network/voip-h245_rtp.pcap}
 *
 *
 *
 *
 */

