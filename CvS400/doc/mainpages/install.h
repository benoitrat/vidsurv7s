/**
 * @page page_install Installation of the project.
 *
 *
 *	@section page_install_lib Installation of the libraries
 *
 *	@subsection page_install_lib_opencv OpenCV
 * The installation should be done with at least OpenCV 2.0.0, previous
 * version are not supported.
 * You can find all information at : http://opencv.willowgarage.com/
 *
 *	@subsection page_install_lib_pcap 	LibPcap
 * This library is optional, because this software normally use the UDP
 * layer and the socket to communicate to the board. However the @ref x7snet
 * library can also use a "X7sMACSocket" to communicate to the board.
 *	@see X7sNetSession::Connect(const char*, const char*)
 *
 *	@subsection page_install_lib_3rdparty ThirdParty Libraries.
 * This contains @anchor page_install_lib_jpeg LibJPEG and @anchor page_install_lib_tinyxml TinyXML
 *
 *
 *
 *	@section page_install_sdk	Eclipse/CDT
 * We suggest you to install @ref page_eclipse.
 *
 *	@section page_install_cmake	Cmake compilation.
 * 		Install cmake to generate the project files or makefile.
 *
 *  @section page_install_plugins Plugins
 *
 *  @subsection page_install_plugins_doxy Doxygen
 *  Documentation generator.
 *  	- Installation: http://www.stack.nl/~dimitri/doxygen/download.html
 *  	- Documentation: http://www.stack.nl/~dimitri/doxygen/commands.html
 *
 *  @subsection page_install_plugins_dot GraphViz.
 *  Figure generator for dot files.
 *
 *	@subsection page_install_plugins_valgring ValGrind
 *	Memory leak check under linux.
 *		- Installation: @code apt-get install valgrind @endcode.
 *		- QuickStart: http://valgrind.org/docs/manual/quick-start.html
 *
 *
 *
 */
