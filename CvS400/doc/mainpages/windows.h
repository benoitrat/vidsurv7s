/**
* @page page_win  How to use the applications under windows.
*
*
* All the test has been done under Windows XP SP2, if you have some problem with other system please contact us.
*
* @li @ref page_win_opencv
* @li @ref page_win_qt
* @li @ref page_win_pgsql
* @li @ref page_win_codec
* @li @ref page_win_msvc
*
*
*<hr>
*

*
*
*
*
* @anchor opencv-win
* @section page_win_opencv OpenCV
*
* The installation of OpenCV can be done by running the executable that you can find on OpenCV website.
*  The package for OpenCV 2.0 is prepared to work directly with MinGW.
* @note  If you want to use OpenCV with MSVC you must compil it.
* @see http://opencv.willowgarage.com/wiki/InstallGuide
*
*
*
* @sepline
* @section page_win_codec Windows codec for OpenCV.
*
* Problems can occur while reading or saving video using the @ref x7sio library.
*
* The best way to handle them in windows is to download the <b>@tt{K-Lite Codec Pack}</b>.
* @note The version 4.1.0 has been tested and works well under WinXP SP2.
*
* @see http://www.codecguide.com/
* @see http://en.wikipedia.org/wiki/K-Lite_Codec_Pack
*
*
* @sepline
* @section page_win_qt Qt for windows
*
* This project have been tested with Qt 4.4.x, you should download the Qt SDK package, and install it.
* Nothing more should be done to make it work.
*
* @sepline
* @section page_win_pgsql  PostGreQL
*
*
* @subsection page_win_pgsql_install Installation of PostGreSQL.
*
* Download the last version of postgreSQL and install it http://www.postgresql.org/download/windows
* You should use the one click installer. During installation a password is asked for the super-user postgres.
* You should set a strong password, this password will be use only to create the @tt{advisor}
* user with its corresponding table.
*
*
* @subsubsection page_win_pgsql_create Creation of 'advisor' user/table
*
* 		-# Open the pgAdmin III
* 		-# connect to the localhost (use your the password previsouly set).
* 		-# Select the postgres database (Servers > Localhost > Databases > postgres)
*		-# click on Tools > Query Tools and then execute the following line:
*
*
*	Without tablespace (database save on your system hard drive):
	@code
	CREATE USER advisor WITH PASSWORD 'advisor' NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE;
	CREATE DATABASE advisor WITH OWNER = advisor ENCODING = 'UTF8';
	@endcode

	With tablespace (database save on a desired location)
	@code
	CREATE USER advisor WITH PASSWORD 'advisor' NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE;
	CREATE TABLESPACE avo_space1 OWNER advisor LOCATION 'D:/DB/postgresql';
	CREATE DATABASE advisor WITH OWNER = advisor ENCODING = 'UTF8' TABLESPACE avo_space1;
	@endcode
*
*
* @subsection page_win_pgsql_qt SQL plugins for QT (MinGW32)
*
* First take a look at :  http://doc.trolltech.com/4.4/sql-driver.html
* We propose a solution without the recompilation of QT, using only shared dll
*
* @subsubsection page_win_pgsql_qt_compil Compilation of the plugins
*
@code
cd %QTDIR%\src\plugins\sqldrivers\psql
qmake -configure release, debug -o Makefile
"INCLUDEPATH+=\"C:\Program Files\PostgreSQL\x.x\include\"" "LIBS+=\"C:\Program Files\PostgreSQL\x.x\lib\libpq.lib\"" psql.pro
make
make install
@endcode

You should replace @tt{x.x} by your version of PostgreSQL.
 If an error occurs during the compilation (@tt{make} command), it might be that the include directory or the library is
 not correct.

* @note Take care of space by adding the internal quote @tt{\"C:\Program Files\...\"}.
*
*
* @subsubsection page_win_pgsql_qt_install Installation of the plugins
*
* Now that the compilation of the plugins has been done you may need to install it:
*	-# Adding as a library in your project
* 	(You must link your project with the .lib or .a)
*	-# Check that %QTDIR%/plugins is in the %PATH% or copy the %QTDIR%/plugins/sqldrivers
* in your binary folder (you must put the dll inside the sqldrivers folder).
*	-# Check the dependencies of the dll using  for example (Dependency Walker).
*	If some dll are missing you should add them to the %PATH% or copy them in the binary folder.
*
*
* @sepline
* @section page_win_msvc Microsoft Visual C++ Express
* @subsection page_win_msvc_2008 Microsoft Visual C++ Express 2008
*
* The application can be compiled under windows using the Microsoft Visual C++ Express 2008.
* However, to deploy on a PC which doesn't have MVC2008, you need to install the following dll
*
* @code
* msvcm90.dll
* msvcp90.dll
* msvcr90.dll
* @endcode
*
* This can be done by downloading the Microsoft Visual C++ 2008 Redistributable
* Package (vcredist_x86.exe)
*
* @note To export the application you also need to build it in @b Release mode.
* @see http://www.microsoft.com/downloads/details.aspx?FamilyID=9b2da534-3e03-4391-8a4d-074b9f2bc1bf&displaylang=en
*
* @subsection page_win_msvc_2005 Microsoft Visual C++ Express 2005
*
* Same problem then @ref page_win_msvc_2008. But you need to download the Microsoft Visual C++ 2005 Redistributable
* Package (vcredist_x86.exe)
* @see http://www.microsoft.com/downloads/details.aspx?familyid=32BC1BEE-A3F9-4C13-9C99-220B62A191EE&displaylang=en
*
*
*
*/
