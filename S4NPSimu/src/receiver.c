/**
 *  @file
 *  @brief Contains the class receiver
 *  @date 27-nov-2008
 *  @author Benoit RAT
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */


#include <stdio.h>
#include <stdlib.h>
#include "globals.h"
#include "UDP_Packet.h"
#include "typedefs.h"
#include "S4N_Packet.h"
#include "S400Board.h"


UDP_Socket g_sock;
S400Board g_s400;
u32 local_ip = 0xC0A80707; //192=0xC0,168=0xA8,7=0x07,07=0x07
u16 local_port = 43210;


int main(int argc,const char *argv[]) {

	g_s400.remote_ip = 0xC0A807C8; //192=0xC0,168=0xA8,7=0x07,200=0xC8
	g_s400.remote_port=18000;


	if(argc>1) {
		if(strcmp(argv[1],"--lua") == 0) {

			S4N_Packet_PrintLuaCmd();
			return EXIT_SUCCESS;
		}
		else if(strcmp(argv[1],"--help") == 0) {
			printf("\n Usage:\n");
			printf("-------\n\n");
			printf("%s --udp IP PORT    \t Configure the udp socket\n",argv[0]);
			printf("%s --help        \t Print this message\n",argv[0]);
			printf("%s               \t Default mode (%s --udp %d)\n",argv[0],argv[0],g_regs.local_port);
			printf("\n");
			fflush(stdout);
			return EXIT_SUCCESS;
		}
		else if(strcmp(argv[1],"--udp") == 0) {
			if(argc>=3) {
				g_s400.remote_ip=atoi(argv[2]);
				g_s400.remote_port=atoi(argv[3]);
			}
		}
	}

	printf("Welcome: (Try --help to obtain more information)\n\n");
	fflush(stdout);
	InitUDPSocket(&(g_sock),g_s400.remote_ip, g_s400.remote_port);
	s400Server();

	return EXIT_SUCCESS;
}


void receiver() {

	u16 rcvd_size=0,snd_size=0;
	u32 nB_edata=0;
	u8 cam_id=CAMID_NONE;
	u8 cam_id_stop=CAMID_NONE;
	bool life=TRUE;
	u32 *p_image=(u32*)im_buffer;
	int win_sup = 0, win_inf = 0, saturate = 0;
	u32 frame_id = 0;

	//Init the pointers on the packet
	UDP_Packet *my_udp_pkt, *xil_udp_pkt;
	my_udp_pkt = (UDP_Packet*)malloc(sizeof(UDP_Packet));
	xil_udp_pkt = (UDP_Packet*)malloc(sizeof(UDP_Packet));

	my_udp_pkt->ip_src_addr=local_ip;
	my_udp_pkt->udp_src_port

	UDP_Packet_SetDest(my_udp_pkt,xil_udp_pkt);;

	S4N_Packet *my_s4n_pkt, *xil_s4n_pkt;
	my_s4n_pkt = (S4N_Packet*)malloc(sizeof(S4N_Packet));
	xil_s4n_pkt = (S4N_Packet*)malloc(sizeof(S4N_Packet));





	while(life) { //---> Beginning of life loop

		//Listen to the socket for incoming packets.
		rcvd_size=UDP_Packet_Recv(xil_udp_pkt,(u32*)(xil_s4n_pkt),SIZEIB_DATA_UDP);


		//If a packet is received (rcvd_size == -1 is equivalent to timeout)
		if(rcvd_size > 0 && rcvd_size != 0xFFFF) {

#if __DEBUG__ >= 1
			printf("\nPC->S400 received (Length:%d):\n",rcvd_size);
			UDP_Packet_Show(xil_udp_pkt);
			S4N_Packet_Show(xil_s4n_pkt);
#endif

			//Set the direction (IP and port) on which we need to reply
			UDP_Packet_SetDest(xil_udp_pkt,my_udp_pkt);
			snd_size=0;

			//Look at the command of the packet.
			switch(xil_s4n_pkt->cmd) {
			case S4NPC_S400_ASK:		//!<------------------------------------------<<<
				my_s4n_pkt->cmd=S4NPC_S400_ACK;
				//Send the packet using udp headers and s4n_packet.
				snd_size = UDP_Packet_Send(my_udp_pkt,(u32*)(my_s4n_pkt),SIZEIB_HEAD_S4NP);
				break;

			case S4NPC_SEND_START: 		//!<------------------------------------------<<<
				//Set the camera id.
				SetCamID(my_s4n_pkt->camID, &cam_id);
				//Reset the windows flags.
				cam_id_stop = CAMID_NONE;
				win_sup = 0;
				win_inf = 0;
				printf("\n\nstart cam_id=%d\n",cam_id);
				fflush(stdout);
				break;

			case S4NPC_SEND_STOP:		//!<------------------------------------------<<<
				//Set on which camera ID we are going to stop.
				SetCamID(my_s4n_pkt->camID, &cam_id_stop);
				break;

			case S4NPC_FRAME_ACKS:		//!<------------------------------------------<<<
				win_inf++;
				break;

			case S4NPC_PARAM_GET:		//!<------------------------------------------<<<
				if(GetParam(xil_s4n_pkt,my_s4n_pkt)) {
					snd_size = UDP_Packet_Send(my_udp_pkt,(u32*)(my_s4n_pkt),SIZEIB_HEAD_S4NP);
				}
				break;

			default:					//!<------------------------------------------<<<
				my_s4n_pkt->cmd=S4NPC_UNKOWN;
				snd_size = UDP_Packet_Send(my_udp_pkt,(u32*)(my_s4n_pkt),SIZEIB_HEAD_S4NP);
				nB_edata=0;
				break;
			}
#if __DEBUG__ >= 1
			if(snd_size > 0) {
				printf("\nS400->PC sent (Length:%d):\n",snd_size);
				UDP_Packet_Show(my_udp_pkt);
				S4N_Packet_Show(my_s4n_pkt);
			}
#endif
		}



	}



