/**
 *  @file
 *  @brief Contains the class UDPPacket
 *  @date 17-nov-2008
 *  @author Benoit RAT
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#include "registers.h"
#include "UDP_Packet.h"

void UDP_Packet_Init(UDP_Packet *packet) {
	packet->ip_src_addr = g_regs.local_ip;
	packet->udp_src_port = g_regs.local_port;
	packet->ip_dst_addr=0xFFFFFFFF; //Default value.
	packet->udp_dst_port=0;			//Default value.
	UDP_Packet_Show(packet);
}

int UDP_Packet_Send(UDP_Packet *pkt, u32 *buffer, int nBytes) {
	int ret;
	u8 *u8buff = (u8*)buffer;

	//Check the maximum number nof of bytes.
	nBytes=CheckSize(nBytes);

	//Check the data buffer.
	pkt->p_data=buffer;

	ret = SendToUDPSocket(&(g_sock),pkt->ip_dst_addr,pkt->udp_dst_port,u8buff,nBytes);

	return ret;
}

int UDP_Packet_Recv(UDP_Packet *pkt, u32 *buffer, int nBytes) {
	int ret;
	u8 *u8buff = (u8*)buffer;

	//Check the maximum number nof of bytes.
	nBytes=CheckSize(nBytes);

	//Check the data buffer.
	pkt->p_data=buffer;

	ret = RecvFromUDPSocket(&(g_sock),&(pkt->ip_src_addr),&(pkt->udp_src_port),u8buff,nBytes);
	return ret;
}

int CheckSize(int nBytes) {
	int tmp,zeros;
//	u8 *u8buff = (u8*)buffer;
//
//	if(nBytes > SIZEIB_DATA_UDP) nBytes = SIZEIB_DATA_UDP;
//
//	zeros=(4-(nBytes%4));
//	while(zeros>0) {	//Add an offset of n zero bytes to complete packet by 32.
//		u8buff[nBytes]=0x00;
//		nBytes++;
//		zeros--;
//	}

	tmp=nBytes%4;
	if(tmp>0) {
		zeros=(4-(nBytes%4));
		nBytes+=zeros;
	}

	//Check if the size correspond to the limits.
	if(nBytes > SIZEIB_DATA_UDP) nBytes = SIZEIB_DATA_UDP;

	return nBytes;
}

void UDP_Packet_SetDest(const UDP_Packet *rcvd_pkt,UDP_Packet *my_pkt) {

	//Set the destination with the source IP and port of the received packet
	my_pkt->ip_dst_addr	=rcvd_pkt->ip_src_addr;
	my_pkt->udp_dst_port=rcvd_pkt->udp_src_port;

}



u32 UDP_Packet_8to32IP(const u8* ip8) {
	u32 ip = 0;
	ip = (ip8[0] << 24) | (ip8[1] << 16) | (ip8[2] << 8) | (ip8[3] << 0);
	return ip;
}

void UDP_Packet_32to8IP(const u32 ip32, u8* ip8) {
	ip8[0]=(ip32 &  0xFF000000) >> 24;
	ip8[1]=(ip32 &  0x00FF0000) >> 16;
	ip8[2]=(ip32 &  0x0000FF00) >>  8;
	ip8[3]=(ip32 &  0x000000FF);
}

void UDP_Packet_Show(UDP_Packet *pkt) {
	if(pkt==NULL) printf("pkt is null");

	u8 ip8_dst[4];
	u8 ip8_src[4];

	UDP_Packet_32to8IP(pkt->ip_src_addr, ip8_src);
	UDP_Packet_32to8IP(pkt->ip_dst_addr, ip8_dst);

	printf("UDP: %d.%d.%d.%d:%d -> %d.%d.%d.%d:%d\n",
			ip8_src[0],ip8_src[1],ip8_src[2],ip8_src[3],pkt->udp_src_port,
			ip8_dst[0],ip8_dst[1],ip8_dst[2],ip8_dst[3],pkt->udp_dst_port);
	fflush(stdout);
}
