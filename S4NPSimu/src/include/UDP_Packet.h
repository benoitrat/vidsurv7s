/**
 *  @file
 *  @brief Contains the class UDPPacket
 *  @date 17-nov-2008
 *  @author Benoit RAT
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */


#ifndef UDP_PACKET_H_
#define UDP_PACKET_H_

#include "typedefs.h"
#include "globals.h"

#define SIZEIB_DATA_S4NP	1440 	//!< Size of two lines of 2 bytes per pixels in bytes: 2 lines * (2 pixel * 340 cols).
#define SIZEIB_HEAD_S4NP 	4		//!< Size of
#define SIZEIB_DATA_UDP     SIZEIB_HEAD_S4NP + SIZEIB_DATA_S4NP


typedef struct _UDP_Packet {
	u32 ip_src_addr;
	u32 ip_dst_addr;
	u16 udp_dst_port;
	u16 udp_src_port;
	u16 udp_length;

	u32* p_data;

} UDP_Packet;


int CheckSize(int nBytes);
void UDP_Packet_Init(UDP_Packet* pkt);
int UDP_Packet_Send(UDP_Packet* pkt, u32* buffer, int nBytes);
int UDP_Packet_Recv(UDP_Packet* pkt, u32* buffer, int nBytes);
void UDP_Packet_SetDest(const UDP_Packet *rcvd_pkt,UDP_Packet *my_pkt);
u32 UDP_Packet_8to32IP(const u8* ip8);
void UDP_Packet_32to8IP(const u32 ip32, u8* ip8);
void UDP_Packet_Show(UDP_Packet *pkt);





#endif /* UDP_PACKET_H_ */
