/**
 *  @file
 *  @brief Contains the class globals
 *  @date 18-nov-2008
 *  @author Benoit RAT
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */


#ifndef GLOBALS_H_
#define GLOBALS_H_

#include "registers.h"
#include "UDP_Socket.h"

extern Registers 	g_regs;
extern UDP_Socket 	g_sock;


#endif /* GLOBALS_H_ */
