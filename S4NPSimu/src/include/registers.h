/**
 *  @file
 *  @brief Contains the class registers
 *  @date 17-nov-2008
 *  @author Benoit RAT
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */


#ifndef REGISTERS_H_
#define REGISTERS_H_

#include "typedefs.h"


enum {
		UNKNOWN=0,	//!< The ID that correspond to the unkown ID.

		/* S400 Fixed value */
		VERSION,	//!< Version of the firmware 8 bits (8 bits)
		VS400P,		//!< Version of the S400P Protocol (8 bits)
		EHA,		//!< Ethernet Hardware Address (48 bits)

		/* S400 Parameters */
		IP,			//!< IP address of the  S400 Boards. (32 bits)
		PORT,		//!< Listening port of the S400 Boards (16 bits)
		MODES,		//!< Modes (8 bits) -> |SENDING IMG|SEQ/RATE MODE|.
		RATE,		//!< Sending rates mode (8 bits).
		WINSIZE,	//!< Windows Size for the hacking reply (like tcp/ip).
		CAM_ON,		//!< If camera is available or not (8 bits) -> 0x0000|CAM1|CAM2|CAM3|CAM4|

		/* Camera Parameters */
		RESO,		//!< Resolution of image (32 bits) -> |IM_WIDTH|IM_HEIGHT|
		QUALITY,	//!< Quality of image (8 bits)
		REFRESH,	//!< Refresh time in number of frame (8 bits)
		THRESHOLD	//!< Threshold of image (8 bit
};


typedef struct _RegCamera {
	u32 reso; 			//Resolution of image (32 bits) -> |IM_WIDTH|IM_HEIGHT|.
	u8 quality; 		//QUALITY 	Quality of image (8 bits).
	u8 refresh; 		//Refresh time in number of frame (8 bits).
	u8 threshold; 		//Threshold of image (8 bits).
} RegCamera;


typedef struct _Registers {

	const u8 ver_s4fw; 		//Version of the firmware 8 bits (8 bits).
	const u8 ver_s4np;  	//Version of the S400P Protocol (8 bits).
	const u8 eha[6]; 		//Ethernet Hardware Address (48 bits).
	u32	local_ip;			//IP address of the S400 Boards. (32 bits).
	u16 local_port;			//Port of the s400 boards
	u8 modes; 				//Modes (8 bits) -> |SENDING IMG|SEQ/RATE MODE|.
	u8 rate; 				//Sending rates mode (8 bits).
	u8 win_size;			//The size of the windows
	u8 cam_on; 				//If camera is available or not (16 bits) -> |CAM1|CAM2|CAM3|CAM4|.

	RegCamera regCam[4];
} Registers;

#endif /* REGISTERS_H_ */
