/*
 * frames.h
 *
 *  Created on: 1-Dec-08
 *      Author: neub
 */

#ifndef FRAMES_H_
#define FRAMES_H_

#include <cv.h>
#include <highgui.h>
#include <stdio.h>
#include <stdlib.h>
#include "typedefs.h"



enum {
	FPT_VIDEO,
	FPT_GRABBER,
	FPT_FILESEQ,
	FPT_SEQNAME,
};



typedef struct _FrameProcessor {

	//The global parameters.
	int type;
	int nErrors, nMaxErrors;
	int width, height, nChannels;
	int frame_id, f_start,f_end;
	char *fpath;
	int waitN;
	bool is_ok;

	//The capture source on which we loop
	char *fname;
	FILE* fIn;
	CvCapture* capture;

	//The output frame
	IplImage* frame;


} FrameProcessor;



FrameProcessor* FrameProc_InitSeqName(const char *fpath,const char *fname,int frame_start,int frame_end);
void FrameProc_Release(FrameProcessor **fProc);
int FrameProc_GetNextYUV422(FrameProcessor* fProc, u8* buffer);
int FrameProc_GetNextJPEG(FrameProcessor* fProc, u8* buffer);
void FrameProc_Show(FrameProcessor* fProc);





#endif /* FRAMES_H_ */
