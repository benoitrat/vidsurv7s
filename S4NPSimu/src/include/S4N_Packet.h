/**
 *  @file
 *  @brief Contains the class S4N_Packet.h
 *  @date 18-nov-2008
 *  @author neub
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#ifndef S4N_PACKET_H_
#define S4N_PACKET_H_

#include "typedefs.h"


/**
 * @brief The maximum size that the data should reach.
 *
 * S4NPKT_DATA_MAXSIZE + S4NPKT_HEADER_SIZE + UDP_HEADER_SIZE + IP_HEADER_SIZE + MAC_HEADER_SIZE < MTU_SIZE.
 * In bytes this give
 * @code
 * 1440 + 4 + 8 + 20 + 14 = 1440 + 4 + 42 = 1440 + 46 = 1486 < 1500 //default MTU size.
 * @endcode
 */
#define S4NPKT_DATA_MAXBSIZE 1440
#define S4NPKT_HEADER_BSIZE 4 //!< The S4NPacket layer is on 32 bits (4 bytes).


enum {
	S4NPC_UNKOWN=0x00,

	/* board  0x1? */
	S4NPC_S400_ASK=0x10,
	S4NPC_S400_ACK=0x11,

	/* parameters 0x2? */
	S4NPC_PARAM_RST=0x20, 		//!< Ask to reset to the default parameter.
	S4NPC_PARAM_SET,			//!< Ask to set the parameter(s)
	S4NPC_PARAM_GET,			//!< Ask to get the parameter(s)
	S4NPC_PARAM_ACK,			//!< Announce the acknowledge of the parameter(s) set.

	/* network 0x8? */
	S4NPC_SEND_SLOW=0x80,		//!<
	S4NPC_SEND_FAST,
	S4NPC_SEND_STOP,			//!< Ask the S400 to stop sending on the next round (camID can stop when reach the frame).
	S4NPC_SEND_START,			//!< Ask the S400 to send a frame (Cam ID can start at one frame).

	/* frames 0xA? */
	S4NPC_FRAME_SETUP=0xA0,		//!< Announce the beginning of a frame and setup the buffer.
	S4NPC_FRAME_BLOCK,			//!< Announce that a frame block is sent.
	S4NPC_FRAME_LAST,			//!< Announce that is the last block sent
	S4NPC_FRAME_ACKS,			//!< Acknowledge the good reception of the complete frame.

	/* Metadata 0xB? */
	S4NPC_MDATA_FIRST=0xB0,		//!< Announce that the first block of metadata is sent.
	S4NPC_MDATA_BLOCK,			//!< Announce that a block of metadata is sent.
	S4NPC_MDATA_LAST,			//!< Announce that the last block of metadata is sent.
	S4NPC_MDATA_ACK,			//!< Acknowledge the goog reception of the metadats


};




enum {
	CAMID_NONE=0,
	CAMID_1=1,
	CAMID_2,
	CAMID_3,
	CAMID_4,
	CAMID_ALL = 0xFF,
};


typedef struct _S4N_Packet{
	/**
	 * @brief This field correspond to a specific command (action) to perform.
	 *
	 * @see S4NPCmd to obtain more information about the various commands.
	 */
	u8 cmd;

	/**
	 * @brief This fields represent the camera ID:
	 *
	 * It generally takes the following values: @tt{CAMID_1\,CAMID_2\,CAMID_3\,CAMID_4},
	 * but it can also take the special value @tt{CAMID_ALL}.
	 */
	u8 camID;


	/**
	 * @brief This field represents the block address offset
	 * or the parameters address.
	 *
	 * - In the case of the block address offset in a frame image, we use this address
	 * as an identifier of the block number. Multiplying this value with the BLOCK_LENGTH
	 * we obtain an offset on the pointed memory.
	 *
	 * @code
	 * u_char buff[FRAME_LENGTH_IN_BYTE];
	 * u_char *p_block; //Pointer on the beginning of a frame block.
	 * ...
	 * p_block = buff+(addr*BLOCK_LENGTH); //Correspond to the beginning of the frame block
	 * memcpy(p_block,this->data,BLOCK_LENTGH); //Copy the encapsulated data (frame block sent) in the buffer.
	 * @endcode
	 *
	 * - In the case of the parameters address you need to look at the S4NPID intelligent enum,
	 * to obtain more information about what parameters means what.
	 *
	 * @see S4NPID.
	 *
	 */
	u16 addr;



	/**
	 * @brief The encapsulated data.
	 *
	 * @see S4NPKT_DATA_MAXBSIZE.
	 */
	u32 data[(S4NPKT_DATA_MAXBSIZE << 2)];

} S4N_Packet;

void S4N_Packet_Show(S4N_Packet* pkt);
const char* S4N_Packet_GetCmdName(S4N_Packet *s4np);
void S4N_Packet_PrintLuaCmd();

#endif /* S4N_PACKET_H_ */
