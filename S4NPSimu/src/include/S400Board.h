/**
 *  @file
 *  @brief Contains the class S400Board
 *  @date 27-nov-2008
 *  @author Benoit RAT
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */


#ifndef S400BOARD_H_
#define S400BOARD_H_

#include "typedefs.h"

#define NOF_CAMBYS400 4
#define IM_WIDTH 360
#define IM_HEIGHT 288
#define CAM_BUFF_SIZE



typedef struct _Camera {
	u8 *buffer;
	char name[255];
} Camera;


typedef struct _S400Board {
	u8 eha[MAC_NOFEXA];
	u32 remote_ip;
	u16 remote_port;
	Camera cam[NOF_CAMBYS400];
	int cam_id;
	int win_rcv, win_send;
	int send_loop;
	int frame_id;
} S400Board;





#endif /* S400BOARD_H_ */
