/**
 *  @file
 *  @brief Contains the class UDPSocket
 *  @date 17-nov-2008
 *  @author Benoit RAT
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */


#ifndef UDPSOCKET_H_
#define UDPSOCKET_H_

#include "typedefs.h"
#include "stdio.h"


#ifdef WIN32 /* If you use windows */

#include <windows.h>
#include <winsock2.h>
typedef int socklen_t;



#elif defined (linux) /* if you are under linux */

#include <errno.h>			//variable errno
#include <string.h>			//function memset
#include <stdlib.h>			//exit(),free(),...
#include <unistd.h> 		//close(),...
#include <netdb.h> 			//gethostbyname(),...
#include <asm/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket(s) close(s)
typedef unsigned int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

#else /* Not supported platform */

#error not defined for this platform

#endif



typedef struct _UDPSocket {
	SOCKET sock;
	SOCKADDR_IN sin_local;
} UDP_Socket;

int InitUDPSocket(UDP_Socket* udp_sock,u32 ip, u16 port);
int InitUDPSocketServer(UDP_Socket* udp_sock, u16 port);
int SendToUDPSocket(UDP_Socket* udp_skt, u32 ip_remote, u16 port_remote,u8* buffer, int nBytes);
int RecvFromUDPSocket(UDP_Socket* socket, u32* ip_remote, u16* port_remote,u8* buffer, int nBytes);

#endif /* UDPSOCKET_H_ */
