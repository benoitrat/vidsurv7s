/*
 * frames.c
 *
 *  Created on: 1-Dec-08
 *      Author: neub
 */

#include "frames.h"

#define R  0
#define G  1
#define B  2
#define Y  0
#define Cr 1
#define Cb 2


//Private prototype (only visible in this file)
bool CheckFrame(FrameProcessor *fProc, IplImage *frame_in);
int GetSeqNameFrame(FrameProcessor *fProc);
FrameProcessor* FrameProc_InitMem();
void pRGB2YCrCb(const u8* p_rgb, u8* p_yrb);
void pYCrCb2RGB(const u8* p_yrb, u8* p_rgb);
void TransformYUV4222RGB(const u8* im_yuv422, u8* im_rgb, int nof_pixels);
void TransformRGB2YUV422(const u8* im_rgb, u8* im_yuv422, int nof_pixels);


FrameProcessor* FrameProc_InitSeqName(const char *fpath,const char *fname,int frame_start, int frame_end) {
	FrameProcessor *fProc = FrameProc_InitMem();
	fProc->type=FPT_SEQNAME;
	fProc->fpath=(char*)fpath;
	fProc->fname=(char*)fname;
	fProc->f_start=frame_start;
	fProc->f_end=frame_end;
	return fProc;
}

FrameProcessor* FrameProc_InitMem() {
	FrameProcessor *fProc = malloc(sizeof(FrameProcessor));

	fProc->type=-1;

	fProc->width=-1;
	fProc->height=-1;
	fProc->nChannels=-1;

	fProc->frame_id=0;
	fProc->f_start=0;
	fProc->f_end=100000;

	fProc->nErrors=0;
	fProc->nMaxErrors=3;
	fProc->waitN=2;
	fProc->is_ok=TRUE;

	fProc->fpath=NULL;
	fProc->fname=NULL;
	fProc->frame=NULL;
	fProc->capture=NULL;
	fProc->fIn=NULL;

	return fProc;
}


void FrameProc_Release(FrameProcessor **fProc) {

	cvReleaseImage( &((*fProc)->frame) );

	free((*fProc));
	*fProc=NULL;
}


void FrameProc_Show(FrameProcessor* fProc) {

	switch(fProc->type) {
		case FPT_SEQNAME:
			printf("fProc is type = FPT_SEQNAME\n");
			printf("\t* fpath=%s; fname=%s\n",fProc->fpath,fProc->fname);
			break;
	}
	printf("\t* width=%d; height=%d; nChannels=%d; \n",fProc->width,fProc->height,fProc->nChannels);
	printf("\t* frame_id=%d; f_start=%d; f_end=%d; \n",fProc->frame_id,fProc->f_start,fProc->f_end);
	printf("\t* nErrors=%d; nMaxErrors=%d; waitN=%d \n",fProc->nErrors,fProc->nMaxErrors,fProc->waitN);

	if(fProc->frame==NULL) {
		printf("\t* frame -> NULL\n\n");
	}
	else {
		printf("\n");
		cvNamedWindow("Show_tmp",CV_WINDOW_AUTOSIZE);
		cvShowImage("Show_tmp",fProc->frame);
		cvWaitKey(fProc->waitN);
	}
}


/**
 * @return the number of of pixels in the buffer.
 */
int FrameProc_GetNextYUV422(FrameProcessor* fProc, u8* buffer) {

	if(fProc == NULL || buffer == NULL) return 0;
	if(!GetSeqNameFrame(fProc)) return 0;
	TransformRGB2YUV422((u8*)fProc->frame->imageDataOrigin,buffer,fProc->width*fProc->height);
	return fProc->width*fProc->height*2;
}


/**
 * @return the number of of pixels in the buffer.
 */
int FrameProc_GetNextJPEG(FrameProcessor* fProc, u8* buffer) {
	if(fProc == NULL || fProc->frame ==NULL || buffer == NULL) return 0;
	return 0;
}




int GetSeqNameFrame(FrameProcessor *fProc) {
	char im_path[256];
	IplImage *frame_in=NULL;


	if(fProc->frame_id <= fProc->f_end)  {
		sprintf(im_path,fProc->fname,fProc->fpath,fProc->frame_id);
		frame_in = cvLoadImage(im_path,CV_LOAD_IMAGE_COLOR);
		if(frame_in==NULL) { printf("Can't load the file %s\n",im_path); 	}
		fProc->frame_id++;

		//Check the first frame and if the frame exist...
		if(!CheckFrame(fProc,frame_in)) return FALSE;

		//Resize frame to the size in the ImgSet memory
		cvResize(frame_in,fProc->frame,CV_INTER_LINEAR);

		//Release image
		cvReleaseImage(&frame_in);

		return TRUE;
	}
	else {
		fProc->is_ok=FALSE;
	}

	return FALSE;
}




bool CheckFrame(FrameProcessor *fProc, IplImage *frame_in) {


	//Check if a frame has been loaded.
	if(frame_in == NULL) {
		fProc->nErrors++;
		if(fProc->nErrors > fProc->nMaxErrors) {
			printf("Number of errors = %d > MAX_ERROR\n",fProc->nErrors);
			fProc->frame_id=fProc->f_end+1; //Jump to the next last frame
		}
		return FALSE;
	}

	//Check if our output frame has not been created.
	if(fProc->frame == NULL) {
		if(fProc->width==-1) fProc->width=frame_in->width;
		if(fProc->height==-1) fProc->height=frame_in->height;
		if(fProc->nChannels==-1) fProc->nChannels=frame_in->nChannels;
		fProc->frame = cvCreateImage(cvSize(fProc->width,fProc->height),IPL_DEPTH_8U,fProc->nChannels);
	}
	return TRUE;
}



/**
	Y <- 0.299*R + 0.587*G + 0.114*B
	Cr <- (R-Y)*0.713 + delta
	Cb <- (B-Y)*0.564 + delta

     R_1  B_1 G_1  R_2  B_2  G_2
	 Y_1 Cr_1 Cb_1 Y_2 Cr_2 Cb_2
	     U  Y_1  V  Y_2
 */
void TransformRGB2YUV422(const u8* im_rgb, u8* im_yuv422, int nof_pixels) {

	u_char *p_rgb, *p_yuv,*end;
	u_char p_ycrcb[6]; //Temporary memory for two pixels

	p_rgb = (u_char*) im_rgb;
	p_yuv = im_yuv422;
	end = p_rgb + nof_pixels*3; //3 Channels for RGB

	//loop by 2 pixels
	while(p_rgb<end) {

		pRGB2YCrCb(p_rgb, p_ycrcb);  		//Convert the 1st pixel
		pRGB2YCrCb(p_rgb+3, p_ycrcb+3);		//Convert the 2nd pixel

		p_yuv[0] = p_ycrcb[2];
		p_yuv[1] = p_ycrcb[0];
		p_yuv[2] = p_ycrcb[1];
		p_yuv[3] = p_ycrcb[3];

		//Jump to the next pair of pixel.
		p_yuv+=4;
		p_rgb+=6;
	}
}

/**
	R <- Y + 1.403*(Cr - delta)
	G <- Y - 0.344*(Cr - delta) - 0.714*(Cb - delta)
	B <- Y + 1.773*(Cb - delta)

	Y_1   U   V   Y_2  U    V
	Y_1 Cr_1 Cb_1 Y_2 Cr_2 Cb_2
	R_1  B_1 G_1  R_2  B_2  G_2
 */

void TransformYUV4222RGB(const u8* im_yuv422, u8* im_rgb, int nof_pixels) {

	u_char *p_rgb, *p_yuv,*end;
	u_char p_ycrcb[6]; //Temporary memory for two pixels

	p_rgb = im_rgb;
	p_yuv = (u_char*)im_yuv422;
	end = p_rgb + nof_pixels*3; //3 Channels for RGB

	//loop by 2 pixels
	while(p_rgb<end) {

		p_ycrcb[0] = p_yuv[1];
		p_ycrcb[1] = p_yuv[2];
		p_ycrcb[2] = p_yuv[0];
		p_ycrcb[3] = p_yuv[3];
		p_ycrcb[4] = p_yuv[2];
		p_ycrcb[5] = p_yuv[0];

		pYCrCb2RGB(p_ycrcb, p_rgb);  		//Convert the 1st pixel
		pYCrCb2RGB(p_ycrcb+3,p_rgb+3);		//Convert the 2nd pixel

		//Jump to the next pair of pixel.
		p_yuv+=4;
		p_rgb+=6;
	}
}



void pRGB2YCrCb(const u8* p_rgb, u8* p_yrb) {
	p_yrb[Y]  = (u8)(0.299f*(float)p_rgb[R] + 0.587f*(float)p_rgb[G] + 0.114f*(float)p_rgb[B]);
	p_yrb[Cr] = (u8)(128.0f + ((float)(p_rgb[R]-p_yrb[Y]))*0.713f);
	p_yrb[Cb] = (u8)(128.0f + ((float)(p_rgb[B]-p_yrb[Y]))*0.564f);
}

void pYCrCb2RGB(const u8* p_yrb, u8* p_rgb) {
	p_rgb[R] = (u8)( (float)p_yrb[Y] + 1.403f*(((float)p_yrb[Cr])-128.f) );
	p_rgb[G] = (u8)( (float)p_yrb[Y] - 0.344f*(((float)p_yrb[Cr])-128.f) - 0.714f*(((float)p_yrb[Cb])-128.f) );
	p_rgb[B] = (u8)( (float)p_yrb[Y] + 1.773f*(((float)p_yrb[Cb])-128.f) );
}
