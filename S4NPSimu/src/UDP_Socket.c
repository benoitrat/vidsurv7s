/**
 *  @file
 *  @brief Contains the class UDPSocket
 *  @date 17-nov-2008
 *  @author Benoit RAT
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */
#include "UDP_Socket.h"

int is_socketinit=FALSE;

static void initSockets(void)
{
#ifdef WIN32
	if(!is_socketinit) {
		WSADATA wsa;
		int err = WSAStartup(MAKEWORD(2, 2), &wsa);
		if(err < 0)
		{
			perror("WSAStartup failed !");
			exit(EXIT_FAILURE);
		}
		is_socketinit=TRUE;
	}
#endif
}


static void endSockets(void)
{
#ifdef WIN32
	if(is_socketinit) {
		WSACleanup();
		is_socketinit=FALSE;
	}
#endif
}


SOCKADDR_IN CreateUDPSockAddr(u32 ip, u16 port) {
	SOCKADDR_IN sin;

	// Fill the strcuture with 0
	memset(&sin,'\x0',sizeof(sin));
	//Then fill the important fields
	sin.sin_family = AF_INET;
#ifdef WIN_32
	sin.sin_addr.S_un.S_addr=htonl(ip); //host to network (long) bytes conversion
#else
	sin.sin_addr.s_addr=htonl(ip);	//host to network (long) bytes conversion
#endif
	sin.sin_port=htons(port); //host to network (short) bytes convertion
	return sin;
}


int InitUDPSocket(UDP_Socket* udp_sock,u32 ip, u16 port) {
	initSockets();
	udp_sock->sock = socket(AF_INET, SOCK_DGRAM, 0);	//Socket Diagram for UDP.
	udp_sock->sin_local = CreateUDPSockAddr(ip,port);

	if(udp_sock->sock  == INVALID_SOCKET)
	{
	    perror("CreateUDPSocket()");
	    exit(errno);
	    return 0;
	};
	return 1;
}


int InitUDPSocketServer(UDP_Socket* udp_sock, u16 port) {
	int ret;
	InitUDPSocket(udp_sock,INADDR_ANY,port); // Let WinSock supply address
	ret = bind(udp_sock->sock, (SOCKADDR *) &(udp_sock->sin_local),sizeof(udp_sock->sin_local));
	//Bind a listening port to a specific interface.
	if(ret == SOCKET_ERROR)
	{
	    fprintf(stderr,"The Socket can't be opened on port %i\n",ntohs(udp_sock->sin_local.sin_port));
	    exit(errno);
	}
	else {
		printf("Waiting for UDP packets on port %i\n",ntohs(udp_sock->sin_local.sin_port));
	}
	return 1;
}


void ReleaseUDPSocket(UDP_Socket *udp_sock) {
	closesocket(udp_sock->sock);
	free(udp_sock);
	endSockets();
}


int SendToUDPSocket(UDP_Socket* udp_skt, u32 ip_remote, u16 port_remote,
		u8* buffer, int nBytes) {
	SOCKADDR_IN sin_remote = CreateUDPSockAddr(ip_remote,port_remote);

	int ret= sendto(udp_skt->sock, buffer, nBytes,0,(SOCKADDR *)&(sin_remote),sizeof(sin_remote));
	if(ret < 0) {
	    perror("SendToUDPSocket()");
	}
	return ret;
}


int RecvFromUDPSocket(UDP_Socket* udp_skt, u32* ip_remote, u16* port_remote,
		u8* buffer, int nBytes) {

	SOCKADDR_IN sin_from = {0};
	socklen_t size = sizeof(sin_from);
	int flags=0;

#ifdef WIN32
    // Set the socket I/O mode; iMode = 0 for blocking; iMode != 0 for non-blocking
    int iMode = 1;
    ioctlsocket(udp_skt->sock, FIONBIO, (u_long FAR*) &iMode);
    flags = 0;
#else
    //for linux
    flags = MSG_DONTWAIT;
#endif


	int ret= recvfrom(udp_skt->sock, buffer, nBytes, flags,(SOCKADDR *)&(sin_from),&size);

	if(ret < 0) {
//	    perror("RecvFromUDPSocket()");
	    return ret;
	}

#ifdef WIN32
	*ip_remote    =	(u32)(ntohl(sin_from.sin_addr.S_un.S_addr));
#else
	*ip_remote    =	(u32)(ntohl(sin_from.sin_addr.s_addr));
#endif
	*port_remote  = ntohs(sin_from.sin_port);

	return ret;
}
