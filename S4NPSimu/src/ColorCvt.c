#define R  0
#define G  1
#define B  0
#define Y  0
#define Cr 1
#define Cb 2

typedef unsigned char  u8;


/**
	Y <- 0.299*R + 0.587*G + 0.114*B
	Cr <- (R-Y)*0.713 + delta
	Cb <- (B-Y)*0.564 + delta

     R_1  B_1 G_1  R_2  B_2  G_2
	 Y_1 Cr_1 Cb_1 Y_2 Cr_2 Cb_2
	     U  Y_1  V  Y_2
*/
void TransformRGB2YUV422(const u8* im_rgb, u8* im_yuv422, int nof_pixels) {

	u_char *p_rgb, *p_yuv,*end;
	u_char p_ycrcb[6]; //Temporary memory for two pixels

	p_rgb = im_rgb;
	p_yuv = im_yuv422;
	end = rgb + nof_pixels*3; //3 Channels for RGB

	//loop by 2 pixels
	while(rgb<end) {

		pRGB2YCrCb(p_rgb, p_ycrcb);  		//Convert the 1st pixel
		pRGB2YCrCb(p_rgb+3, p_ycrcb+3);		//Convert the 2nd pixel

		p_yuv[0] = p_ycrcb[2];
		p_yuv[1] = p_ycrcb[0];
		p_yuv[2] = p_ycrcb[1];
		p_yuv[3] = p_ycrcb[3];

		//Jump to the next pair of pixel.
		p_yuv+=4;
		p_rgb+=6;
	}
}

/**
	R <- Y + 1.403*(Cr - delta)
	G <- Y - 0.344*(Cr - delta) - 0.714*(Cb - delta)
	B <- Y + 1.773*(Cb - delta)

	Y_1   U   V   Y_2  U    V
	Y_1 Cr_1 Cb_1 Y_2 Cr_2 Cb_2
	R_1  B_1 G_1  R_2  B_2  G_2
*/

void TransformYUV4222RGB(const u8* im_yuv422, u8* im_rgb, int nof_pixels) {

		u_char *p_rgb, *p_yuv,*end;
		u_char p_ycrcb[6]; //Temporary memory for two pixels

		p_rgb = im_rgb;
		p_yuv = im_yuv422;
		end = rgb + nof_pixels*3; //3 Channels for RGB

		//loop by 2 pixels
		while(rgb<end) {

			p_ycrcb[0] = p_yuv[1];
			p_ycrcb[1] = p_yuv[2];
			p_ycrcb[2] = p_yuv[0];
			p_ycrcb[3] = p_yuv[3];
			p_ycrcb[4] = p_yuv[2];
			p_ycrcb[5] = p_yuv[0];

			pYCrCb2RGB(p_ycrcb, p_rgb);  		//Convert the 1st pixel
			pYCrCb2RGB(p_ycrcb+3,p_rgb+3);		//Convert the 2nd pixel

			//Jump to the next pair of pixel.
			p_yuv+=4;
			p_rgb+=6;
		}
	}



void pRGB2YCrCb(const u8* p_rgb, u8* p_yrb) {
	p_yrb[Y]  = (u8)(0.299f*(float)p_rgb[R] + 0.587f*(float)p_rgb[G] + 0.114f*(float)p_rgb[B]);
	p_yrb[Cr] = (u8)(128.0f + ((float)(p_rgb[R]-p_yrb[Y]))*0.713f);
	p_yrb[Cb] = (u8)(128.0f + ((float)(p_rgb[B]-p_yrb[Y]))*0.564f);
}

void pYCrCb2RGB(const u8* p_yrb, u8* p_rgb) {
	p_rgb[R] = (u8)( (float)p_yrb[Y] + 1.403f*(((float)p_yrb[Cr])-128.f) );
  	p_rgb[G] = (u8)( (float)p_yrb[Y] - 0.344f*(((float)p_yrb[Cr])-128.f) - 0.714f*(((float)p_yrb[Cb])-128.f) );
  	p_rgb[B] = (u8)( (float)p_yrb[Y] + 1.773f*(((float)p_yrb[Cb])-128.f) );
}
