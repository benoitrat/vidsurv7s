/**
 *  @file
 *  @brief Contains the class s400server
 *  @date 17-nov-2008
 *  @author Benoit RAT
 *	@note Includes done in cpp file to accelerate compilation. @ref page_developers_fastcompil
 */

#include <stdio.h>
#include <stdlib.h>
#include "globals.h"
#include "UDP_Packet.h"
#include "typedefs.h"
#include "S4N_Packet.h"
#include "frames.h"


u8 im_buffer[(288*360*2)];
Registers g_regs;
UDP_Socket g_sock;
FrameProcessor *g_fProc;

int s400Server();
void SetCamID(const u8 cid_in, u8* cid_out);
bool GetParam(const S4N_Packet* s4np_in,S4N_Packet* s4np_out);
bool SetParam(const S4N_Packet* s4np_in,S4N_Packet* s4np_out);

int main(int argc,const char *argv[]) {

	g_regs.local_ip=0xC0A807C8; //192=0xC0,168=0xA8,7=0x07,200=0xC8
	g_regs.local_port=18000;
	g_regs.cam_on=(0x1 << 3) + (0x1 << 2) + (0x1 << 1) + 0x1;
	g_regs.rate=00;
	g_regs.modes=(0xF0 + 0x0F);
	g_regs.win_size=10;

	if(argc>1) {
		if(strcmp(argv[1],"--lua") == 0) {

			S4N_Packet_PrintLuaCmd();
			return EXIT_SUCCESS;
		}
		else if(strcmp(argv[1],"--help") == 0) {
			printf("\n Usage:\n");
			printf("-------\n\n");
			printf("%s --lua         \t Print lua config\n",argv[0]);
			printf("%s --udp PORT    \t Configure the udp socket\n",argv[0]);
			printf("%s --help        \t Print this message\n",argv[0]);
			printf("%s               \t Default mode (%s --udp %d)\n",argv[0],argv[0],g_regs.local_port);
			printf("\n");
			fflush(stdout);
			return EXIT_SUCCESS;
		}
		else if(strcmp(argv[1],"--udp") == 0) {
			if(argc>=2) {
				g_regs.local_port=atoi(argv[2]);

			}
		}
	}

	printf("Welcome: (Try --help to obtain more information)\n\n");
	fflush(stdout);
	g_fProc = FrameProc_InitSeqName(
			"/media/SHARED/DATABASE/PETS2006/S1-T1-C/video/pets2006/S1-T1-C/3",
			"%s/S1-T1-C.%05d.jpeg",
			0,1000);
	//We want to obtain file with the following size.
	g_fProc->width=360;
	g_fProc->height=280;
	InitUDPSocketServer(&(g_sock),g_regs.local_port);
	s400Server();

	return EXIT_SUCCESS;
}



int s400Server() {

	u16 rcvd_size=0,snd_size=0;
	u32 nB_edata=0;
	u8 cam_id=CAMID_NONE;
	u8 cam_id_stop=CAMID_NONE;
	bool life=TRUE;
	u32 *p_image=(u32*)im_buffer;
	int win_sup = 0, win_inf = 0, saturate = 0;
	u32 frame_id = 0;
	u32 last_addrjpg;
	u16 nof_blocks=0, last_block_size=0,b=0;

	//Init the pointers on the packet
	UDP_Packet *my_udp_pkt, *xil_udp_pkt;
	my_udp_pkt = (UDP_Packet*)malloc(sizeof(UDP_Packet));
	xil_udp_pkt = (UDP_Packet*)malloc(sizeof(UDP_Packet));
	UDP_Packet_Init(my_udp_pkt);
	UDP_Packet_SetDest(my_udp_pkt,xil_udp_pkt);;

	S4N_Packet *my_s4n_pkt, *xil_s4n_pkt;
	my_s4n_pkt = (S4N_Packet*)malloc(sizeof(S4N_Packet));
	xil_s4n_pkt = (S4N_Packet*)malloc(sizeof(S4N_Packet));

	//Ask for the first frame.
	last_addrjpg = FrameProc_GetNextYUV422(g_fProc,im_buffer); //360x288x2 pixels (207360)

	while(life) { //---> Beginning of life loop

		//Listen to the socket for incoming packets.
		rcvd_size=UDP_Packet_Recv(xil_udp_pkt,(u32*)(xil_s4n_pkt),SIZEIB_DATA_UDP);


		//If a packet is received (rcvd_size == -1 is equivalent to timeout)
		if(rcvd_size > 0 && rcvd_size != 0xFFFF) {

#if __DEBUG__ >= 1
			printf("\nPC->S400 received (Length:%d):\n",rcvd_size);
			UDP_Packet_Show(xil_udp_pkt);
			S4N_Packet_Show(xil_s4n_pkt);
#endif

			//Set the direction (IP and port) on which we need to reply
			UDP_Packet_SetDest(xil_udp_pkt,my_udp_pkt);
			snd_size=0;

			//Look at the command of the packet.
			switch(xil_s4n_pkt->cmd) {
			case S4NPC_S400_ASK:		//!<------------------------------------------<<<
				my_s4n_pkt->cmd=S4NPC_S400_ACK;
				//Send the packet using udp headers and s4n_packet.
				snd_size = UDP_Packet_Send(my_udp_pkt,(u32*)(my_s4n_pkt),SIZEIB_HEAD_S4NP);
				break;

			case S4NPC_SEND_START: 		//!<------------------------------------------<<<
				//Set the camera id.
				SetCamID(my_s4n_pkt->camID, &cam_id);
				//Reset the windows flags.
				cam_id_stop = CAMID_NONE;
				win_sup = 0;
				win_inf = 0;
				printf("\n\nstart cam_id=%d\n",cam_id);
				fflush(stdout);
				break;

			case S4NPC_SEND_STOP:		//!<------------------------------------------<<<
				//Set on which camera ID we are going to stop.
				SetCamID(my_s4n_pkt->camID, &cam_id_stop);
				break;

			case S4NPC_FRAME_ACKS:		//!<------------------------------------------<<<
				win_inf++;
				break;

			case S4NPC_PARAM_GET:		//!<------------------------------------------<<<
				if(GetParam(xil_s4n_pkt,my_s4n_pkt)) {
					snd_size = UDP_Packet_Send(my_udp_pkt,(u32*)(my_s4n_pkt),SIZEIB_HEAD_S4NP);
				}
				break;

			default:					//!<------------------------------------------<<<
				my_s4n_pkt->cmd=S4NPC_UNKOWN;
				snd_size = UDP_Packet_Send(my_udp_pkt,(u32*)(my_s4n_pkt),SIZEIB_HEAD_S4NP);
				nB_edata=0;
				break;
			}
#if __DEBUG__ >= 1
			if(snd_size > 0) {
				printf("\nS400->PC sent (Length:%d):\n",snd_size);
				UDP_Packet_Show(my_udp_pkt);
				S4N_Packet_Show(my_s4n_pkt);
			}
#endif
		}
		//If no packet is received we may send an image.
		else {
			if(cam_id==cam_id_stop) continue; //If the cam_id is the cam_id to stop continue looping.
			if((g_regs.win_size==0) || (win_sup - win_inf) < g_regs.win_size) { //Check the numbers of frames acknowledged.


				//Remove the saturation when the ACK_windows decrease to the half of its maximum.
				if ( (g_regs.win_size==0) || (win_sup - win_inf) <= (g_regs.win_size/2)) {
					saturate = FALSE;
					if(saturate) printf("ACK Windows (Unsaturated): ||%d,%d|| < %d (%d/2)\n",
							win_inf,win_sup,g_regs.win_size/2,g_regs.win_size);
				}

				//Call the frame processor and collect the YUV422 in the image buffer.
				last_addrjpg = FrameProc_GetNextYUV422(g_fProc,im_buffer); //360x288x2 pixels (207360)
				//FrameProc_Show(g_fProc);

				//The size and number of blocks
				nof_blocks=0, last_block_size=0,b=0;


				//this size and the number of blocks are given by the JPEG compression.
				last_block_size=(last_addrjpg%SIZEIB_DATA_S4NP);  //Compute some bytes need to be put in an extra packet.
				nof_blocks=last_addrjpg/SIZEIB_DATA_S4NP; //(Total size)/(maximum packet size)
				if(last_block_size==0) last_block_size=SIZEIB_DATA_S4NP; //If there is no extra packet the last packet is full
				else nof_blocks++;	//If there is an extra packet add it.


				printf("S400->PC Send frame %d from cam_%d in %d blocks\n",frame_id,cam_id,nof_blocks);

				//Send the frame setup.
				my_s4n_pkt->cmd=S4NPC_FRAME_SETUP;
				my_s4n_pkt->camID	= cam_id;
				my_s4n_pkt->addr	= SIZEIB_DATA_S4NP; //The first frame contains the size of a block
				my_s4n_pkt->data[0]	= frame_id;
				UDP_Packet_Send(my_udp_pkt,(u32*)(my_s4n_pkt),SIZEIB_HEAD_S4NP+4);
				p_image = (u32*)im_buffer;

				//Send the N-1 block packet except the last one.
				for(b=0;b<nof_blocks-1;++b) {
					memcpy(my_s4n_pkt->data,p_image,SIZEIB_DATA_S4NP);
					my_s4n_pkt->cmd=S4NPC_FRAME_BLOCK;
					my_s4n_pkt->addr=b;
					UDP_Packet_Send(my_udp_pkt,(u32*)(my_s4n_pkt),SIZEIB_HEAD_S4NP+SIZEIB_DATA_S4NP);

					if((g_regs.rate != 0) && (b > 0) && (b%g_regs.rate==0)) {
						char c;
						printf("Stop at block %d... touch any key (or n)\n",b);
						scanf("%c",&c);
						if(c == 'n') exit(0);
					}
					p_image+=(SIZEIB_DATA_S4NP/4);
				}

				//Create the last block packet for this frame with last_block_size
				memcpy(my_s4n_pkt->data,p_image,last_block_size);
				my_s4n_pkt->cmd=S4NPC_FRAME_LAST;
				my_s4n_pkt->addr=last_block_size;
				UDP_Packet_Send(my_udp_pkt,(u32*)(my_s4n_pkt),SIZEIB_HEAD_S4NP+last_block_size);


				//Modify the windows
				win_sup++;

				//Jump to the next ID.
				cam_id++;
				if(cam_id > CAMID_4) {
					cam_id=CAMID_1;
					//Ask for a frame each 4 Camera.
					last_addrjpg = FrameProc_GetNextYUV422(g_fProc,im_buffer);
					//Update the frame ID.
					frame_id++;
				}
			}
			else{
				if(saturate==FALSE) printf("ACK Windows (Saturated): ||%d,%d|| >= %d\n",win_inf,win_sup,g_regs.win_size);
				saturate = TRUE;	//Set that the sending saturates the bandwidth.
			}
		}
		//		life++;
		//		if(life>=1000) {
		//			life=FALSE;
		//			return EXIT_FAILURE;
		//		}
		life=(life & g_fProc->is_ok); //Check if life is okay before closing.
	} //<--- End of life loop
	return EXIT_SUCCESS;
}


bool GetParam(const S4N_Packet* s4np_in,S4N_Packet* s4np_out) {
	bool is_ok=FALSE;

	return is_ok;
}


bool SetParam(const S4N_Packet* s4np_in,S4N_Packet* s4np_out) {
	bool is_ok=FALSE;

	return is_ok;
}


void SetCamID(const u8 cid_in, u8* cid_out) {
	//Check the camera ID.
	if(CAMID_1 <= cid_in && cid_in <= CAMID_4) {
		(*cid_out)=cid_in;
	}
	else {
		(*cid_out)++; //(CAMID_NONE+1)<=>CAMID_1
		if((*cid_out) > CAMID_4) (*cid_out)=CAMID_1;
	}
}



