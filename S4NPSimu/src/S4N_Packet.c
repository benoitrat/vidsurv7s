/*
 * S4N_Packet.c
 *
 *  Created on: 24-Nov-08
 *      Author: neub
 */

#include "S4N_Packet.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


void S4N_Packet_Show(S4N_Packet* pkt) {
	printf("cmd:%s\t(0x%02X); cam_id:%d; addr=0x%04X\n",S4N_Packet_GetCmdName(pkt),pkt->cmd,pkt->camID,pkt->addr);
	fflush(stdout);
}



const char* S4N_Packet_GetCmdName(S4N_Packet *s4np) {
	const char *ret;

	switch(s4np->cmd) {
			/* board  0x1? */
		case S4NPC_S400_ASK: 	ret="S4NPC_S400_ASK"; break;
		case S4NPC_S400_ACK:	ret="S4NPC_S400_ACK"; break;

			/* parameters 0x2? */
		case S4NPC_PARAM_RST: 	ret="S4NPC_PARAM_RST"; break;
		case S4NPC_PARAM_SET: 	ret="S4NPC_PARAM_SET"; break;
		case S4NPC_PARAM_GET: 	ret="S4NPC_PARAM_GET"; break;
		case S4NPC_PARAM_ACK: 	ret="S4NPC_PARAM_ACK"; break;

			/* network 0x8? */
		case S4NPC_SEND_SLOW: 	ret="S4NPC_SEND_SLOW"; break;
		case S4NPC_SEND_FAST: 	ret="S4NPC_SEND_FAST"; break;
		case S4NPC_SEND_STOP: 	ret="S4NPC_SEND_STOP"; break;
		case S4NPC_SEND_START: 	ret="S4NPC_SEND_START"; break;

			/* frames 0xA? */
		case S4NPC_FRAME_SETUP: ret="S4NPC_FRAME_SETUP"; break;
		case S4NPC_FRAME_BLOCK: ret="S4NPC_FRAME_BLOCK"; break;
		case S4NPC_FRAME_LAST: 	ret="S4NPC_FRAME_LAST"; break;
		case S4NPC_FRAME_ACKS: 	ret="S4NPC_FRAME_ACKS"; break;

			/* Metadata 0xB? */
		case S4NPC_MDATA_FIRST: ret="S4NPC_MDATA_FIRST"; break;
		case S4NPC_MDATA_BLOCK: ret="S4NPC_MDATA_BLOCK"; break;
		case S4NPC_MDATA_LAST: 	ret="S4NPC_MDATA_LAST"; break;
		case S4NPC_MDATA_ACK: 	ret="S4NPC_MDATA_ACK"; break;

		case S4NPC_UNKOWN: 		ret="S4NPC_UNKOWN"; break;
		default: 				ret="UNKOWN"; break;
	}
	return ret;
}


void S4N_Packet_PrintLuaCmd() {
	int i=0;
	S4N_Packet *s4np = (S4N_Packet*)malloc(sizeof(S4N_Packet));
	const char *name;
	printf("\n\tar_cmd = {");
	for(i=0;i<256;i++) {
		s4np->cmd=i;
		name= S4N_Packet_GetCmdName(s4np);
		if(strcmp(name,"UNKOWN")!=0) {
			if(i%16==0) printf("\n\t\t");
			printf("%s = 0x%02X, ",name,i);
		}
	}
	printf("\n\t}\n\n\n");
	for(i=1;i<256;i++) {
		s4np->cmd=i;
		name= S4N_Packet_GetCmdName(s4np);
		if(strcmp(name,"UNKOWN")!=0) {
			printf("elseif cmd == ar_cmd.%s\t then cmd_str = \"%s\"\n",name,name);
		}
	}


	free(s4np);
}
