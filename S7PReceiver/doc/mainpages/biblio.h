/**
 @page page_biblio Bibliography


<DL>

<DT>[ABD<sup>+</sup>08]</DT>
<DD>
\anchor Anisetti2008
Marco Anisetti, Valerio Bellandi, Ernesto Damiani, Luigi Arnone, and Benoit
  Rat.
 <em>Signal Processing for Image Enhancement and Multimedia
  Processing</em>, volume&nbsp;31, chapter A3FD: Accurate 3D Face Detection, pages
  155-165.
 Springer US, 2008.
[&nbsp;<a href="http://dx.doi.org/10.1007/978-0-387-72500-0">DOI</a>&nbsp;|
<a href="http://dx.doi.org/10.1007/978-0-387-72500-0_14">http</a>&nbsp;]

</DD>


<DT>[AHDO08]</DT>
<DD>
\anchor Appiah2008
Kofi Appiah, Andrew Hunter, Patrick Dickinson, and Jonathan Owens.
 A run-length based connected component algorithm for fpga
  implementation.
 In <em>International Conference on Field-Programmable Technology
  2008</em>, 2008.

</DD>


<DT>[ASAO96]</DT>
<DD>
\anchor Aoki1996
Y.&nbsp;Aoki, A.&nbsp;Shio, H.&nbsp;Arai, and K.&nbsp;Odaka.
 A prototype system for interpreting hand-sketched floor plans.
 <em>icpr</em>, 03:747, 1996.


</DD>


<DT>[Bad03]</DT>
<DD>
\anchor Badawy2003
Wael&nbsp;M. Badawy.
 A vlsi architecture for video object motion estimation using a novel
  2-d hierarchical mesh.
 <em>Journal of Systems Architecture</em>, 49(7-9):331-344, 2003.

</DD>


<DT>[BFF06]</DT>
<DD>
\anchor Berclaz2006
Jerome Berclaz, Francois Fleuret, and Pascal Fua.
 Robust people tracking with global trajectory optimization.
 In <em>Conference on Computer Vision and Pattern Recognition</em>, 2006.

</DD>


<DT>[BIM07]</DT>
<DD>
\anchor BenIsrael2007
Elad Ben-Israel and Dr.&nbsp;Yael Moses.
 Tracking of humans using masked histograms and mean shift.
 Introductory Project in Computer Vision – Summary, March 2007.
 Efi Arazi School of Computer Science ⋅ The Interdisciplinary Center
  Herzliya ⋅ March.

</DD>


<DT>[BK08]</DT>
<DD>
\anchor Bradski2008
Gary Bradski and Adrian Kaehler.
 <em>Learning OpenCV: Computer Vision with the OpenCV Library</em>.
 O'Reilly Media, Inc., 1st edition edition, 2008.
 0596516134.

</DD>


<DT>[BTG06]</DT>
<DD>
\anchor Bay2006
Herbert Bay, Tinne Tuytelaars, , and Luc&nbsp;Van Gool.
 Surf: Speeded up robust features.
 In <em>Proceedings of the ninth European Conference on Computer
  Vision</em>, May 2006.

</DD>


<DT>[CE01]</DT>
<DD>
\anchor Cavallaro2001
A.&nbsp;Cavallaro and T.&nbsp;Ebrahimi.
 Change detection based on color edges.
 In <em>Proc. of IEEE Interna-tional Symposium on Circuits
  and Systems (ISCAS-2001)</em>, Proc. of IEEE. SPIE, 2001.

</DD>


<DT>[CE02]</DT>
<DD>
\anchor Cavallaro2002
A.&nbsp;Cavallaro and T.&nbsp;Ebrahimi.
 Accurate video object segmentation through change detection.
 <em>Multimedia and Expo, 2002. ICME '02. Proceedings. 2002 IEEE
  International Conference on</em>, 1:445-448 vol.1, 2002.


</DD>


<DT>[CK04]</DT>
<DD>
\anchor Cheung2004
C&nbsp;Cheung and C.&nbsp;Kamath.
 Robust techniques for background subtraction in urban traffic video.
 In S.&nbsp;Panchanathan and B.&nbsp;Vasudev, editors, <em>Visual
  Communications and Image Processing 2004. Edited by Panchanathan, Sethuraman;
  Vasudev, Bhaskaran. Proceedings of the SPIE, Volume 5308, pp. 881-892
  (2004).</em>, volume 5308 of <em>Presented at the Society of Photo-Optical
  Instrumentation Engineers (SPIE) Conference</em>, pages 881-892, January 2004.


</DD>


<DT>[CLK99]</DT>
<DD>
\anchor Collins1999
R.&nbsp;Collins, A.&nbsp;Lipton, and T.&nbsp;Kanade.
 A system for video surveillance and monitoring.
 In <em>American Nuclear Society 8th Internal Topical Meeting on
  Robotics and Remote Systems</em>, 1999.


</DD>


<DT>[COPS99]</DT>
<DD>
\anchor Cucchiara1999
R.&nbsp;Cucchiara, P.&nbsp;Onfiani, A.&nbsp;Prati, and N.&nbsp;Scarabottolo.
 Segmentation of moving objects at frame rate: A dedicated hardware
  solution.
 <em>Image Processing And Its Applications, 1999. Seventh
  International Conference on (Conf. Publ. No. 465)</em>, vol. 1:138 - 142, 1999.


</DD>


<DT>[dOPF<sup>+</sup>06]</DT>
<DD>
\anchor Oliveira2006
Jozias&nbsp;Parente de&nbsp;Oliveira, Andr&eacute;&nbsp;Luiz Printes, Raimundo
  Carlos&nbsp;Silv&eacute;rio Freire, Elmar Uwe&nbsp;Kurt Melcher, and
  Ivan&nbsp;Sebasti&atilde;o de&nbsp;Souza&nbsp;e Silva.
 FPGA architecture for object segmentation in real time.
 In <em>Eusipco</em>. XIV European Signal Processing Conference,
  September 4 - 8, 2006, Florence, Italy, 2006.

</DD>


<DT>[DZL07]</DT>
<DD>
\anchor Dai2007
Congxia Dai, Yunfei Zheng, and Xin Li.
 Pedestrian detection and tracking in infrared imagery using shape and
  appearance.
 <em>Comput. Vis. Image Underst.</em>, 106(2-3):288-299, 2007.


</DD>


<DT>[EG05]</DT>
<DD>
\anchor Ekinci2005
Murat Ekinci and Eyüp Gedikli.
 Silhouette based human motion detection and analysis for real-time
  automated video surveillance.
 <em>Turkish Journal Electronic Engineering</em>, 13(2):199-229, 2005.

</DD>


<DT>[FBLF06]</DT>
<DD>
\anchor Fleuret2006
Francois Fleuret, Jerome Berclaz, Richard Lengagne, and Pascal Fua.
 Multi-camera people tracking with a probabilistic occupancy map,.
 Technical Report 606, EPFL/CVLab, 2006.

</DD>


<DT>[FBLF08]</DT>
<DD>
\anchor Fleuret2008
F.&nbsp;Fleuret, J.&nbsp;Berclaz, R.&nbsp;Lengagne, and P.&nbsp;Fua.
 Multi-camera people tracking with a probabilistic occupancy map.
 <em>IEEE Transactions on Pattern Analysis and Machine Intelligence</em>,
  30(2):267-282, February 2008.

</DD>


<DT>[FR98]</DT>
<DD>
\anchor Ford1998
Adrian Ford and Alan Roberts.
 Colour space conversions.
 University of Westminster., August 11 1998.
 Cited in openCV.

</DD>


<DT>[FRS06]</DT>
<DD>
\anchor Forsyth2006
David Forsyth, Deva Ramanan, and Cristian Sminchisescu.
 People tracking tutorial.
 In <em>IEEE CVPR</em>, New York, June 2006.

</DD>


<DT>[FYN<sup>+</sup>04]</DT>
<DD>
\anchor Fang2004
Yajun Fang, Keiichi Yamada, Yoshiki Ninomiya, Berthold Horn, and Ichiro Masaki.
 A shape-independent-method for pedestrian detection with
  far-infrared-images, 2004.

</DD>


<DT>[GVP<sup>+</sup>03]</DT>
<DD>
\anchor Gabriel2003
P.&nbsp;F. Gabriel, J.&nbsp;G. Verly, J.&nbsp;H. Piater, , and A.&nbsp;Genon.
 The state of the art in multiple object tracking under occlusion in
  video sequences.
 In <em>in Proceedings of the Advanced Concepts for Intelligent
  Vision Systems (ACIVS '03), Ghent, Belgium</em>, pages pp. 166-173, September
  2003.

</DD>


<DT>[Har98]</DT>
<DD>
\anchor Haritaoglu1998
Ismail Haritaoglu.
 W4s: A real time system for detection and tracking of people and
  recognizing their activities.
 In <em>In Eurepean Conference on Computer Vision</em>, pages 877-892,
  1998.

</DD>


<DT>[HHD99]</DT>
<DD>
\anchor Horprasert1999
T.&nbsp;Horprasert, D.&nbsp;Harwood, and L.S. Davis.
 A statistical approach for real-time robust background subtraction
  and shadow detection.
 In <em>Frame-Rate 99</em>, 1999.

</DD>


<DT>[IBL98]</DT>
<DD>
\anchor Ivanov1998
Yuri&nbsp;A. Ivanov, Aaron&nbsp;F. Bobick, and John Liu.
 Fast lighting independent background subtraction.
 <em>International Journal of Computer Vision</em>, 37(2):199-207, 1998.


</DD>


<DT>[ISL05]</DT>
<DD>
\anchor Islam2005
M.S. Islam, A.&nbsp;Sluzek, and Z.&nbsp;Lin.
 Towards invariant interest point detection of an objects.
 <em>Proc. 13th International Conference in Central Europe on
  Computer Graphics, Visualization and Computer Vision</em>, pages 101-104, 2005.

</DD>


<DT>[KCCK03]</DT>
<DD>
\anchor Kim2003
Jae-Won Kim, Kang-Sun Choi, Byeong-Doo Choi, and Sung-Jea Ko.
 Real-time vision-based people counting system for the security door.
 <em>Lecture Notes in Computer Science</em>, vol. 2781:pp. 466-473,
  2003.

</DD>


<DT>[KGM07]</DT>
<DD>
\anchor Kumar2007
Pabboju&nbsp;Sateesh Kumar, Prithwijit Guha, and Amitabha Mukerjee.
 Colour and feature based multiple object tracking under heavy
  occlusions.
 In <em>Proceedings of the Sixth International Conference on Advances
  in Pattern Recognition, Calcutta (India)</em>, January 2-4 2007.

</DD>


<DT>[KGYS05]</DT>
<DD>
\anchor Karaman2005
Mustafa Karaman, Lutz Goldmann, Da&nbsp;Yu, and Thomas Sikora.
 Comparison of static background segmentation methods.
 In Shipeng Li, Fernando Pereira, Heung-Yeung Shum, and Andrew&nbsp;G.
  Tescher, editors, <em>Visual Communications and Image Processing 2005</em>,
  volume 5960, page 596069. SPIE, 2005.
[&nbsp;<a href="http://dx.doi.org/10.1117/12.633437">DOI</a>&nbsp;|
<a href="http://link.aip.org/link/?PSI/5960/596069/1">http</a>&nbsp;]

</DD>


<DT>[KM96]</DT>
<DD>
\anchor Kameda1996
Yoshinari Kameda and Michihiko Minoh.
 A human motion estimation method using 3-successive video frames.
 <em>Proceedings of International Conference on Virtual Systems and
  Multimedia</em>, pp.135-140:9/18-20, 1996.


</DD>


<DT>[KM05]</DT>
<DD>
\anchor Krahnstoever2005
Nils Krahnstoever and Paulo R.&nbsp;S. Mendonça.
 Bayesian autocalibration for surveillance.
 <em>iccv</em>, 2:1858-1865, 2005.


</DD>


<DT>[LDDD07]</DT>
<DD>
\anchor Lin2007
Zhe Lin, Larry&nbsp;S. Davis, David Doermann, and Daniel DeMenthon.
 Simultaneous appearance modeling and segmentation for matching people
  under occlusion.
 In <em>ACCV 2007</em>, 2007.

</DD>


<DT>[LHGT03]</DT>
<DD>
\anchor Li2003
Liyuan Li, Weimin Huang, Irene&nbsp;Y.H. Gu, and Qi&nbsp;Tian.
 Foreground object detection from videos containing complex
  background.
 <em>ACM MM</em>, page&nbsp;9p, 2003.


</DD>


<DT>[LTR<sup>+</sup>05]</DT>
<DD>
\anchor Liu2005
X.&nbsp;Liu, P.H. Tu, J.&nbsp;Rittscher, A.&nbsp;Perera, and N.&nbsp;Krahnstoever.
 Detecting and counting people in surveillance applications.
 <em>avss</em>, 0:306-311, 2005.


</DD>


<DT>[LZN02]</DT>
<DD>
\anchor Lv2002
Fengjun Lv, Tao Zhao, and Ram Nevatia.
 Self-calibration of a camera from video of a walking human.
 In <em>ICPR '02: Proceedings of the 16 th International Conference
  on Pattern Recognition (ICPR'02) Volume 1</em>, page 10562, Washington, DC, USA,
  2002. IEEE Computer Society.

</DD>


<DT>[May79]</DT>
<DD>
\anchor Maybeck1979
Peter&nbsp;S. Maybeck.
 <em>Stochastic models, estimation, and control</em>, volume 141 of <em>
  Mathematics in Science and Engineering</em>, chapter Chapter 1.
 1979.

</DD>


<DT>[MS08]</DT>
<DD>
\anchor Molina2008
Rafael Molina&nbsp;Soriano.
 Seguimiento de rasgos en secuencias de vídeo. el filtro de kalman.
 Depto Ciencias de la Computación e IA Universidad de Granada, 2008.

</DD>


<DT>[NLC05]</DT>
<DD>
\anchor Nair2005
Vinod Nair, Pierre-Olivier Laprise, and James&nbsp;J. Clark.
 An fpga-based people detection system.
 <em>EURASIP J. Appl. Signal Process.</em>, 2005(1):1047-1061, 2005.

</DD>


<DT>[ORP00]</DT>
<DD>
\anchor Oliver2000
Nuria&nbsp;M. Oliver, Barbara Rosario, and Alex&nbsp;P. Pentland.
 A bayesian computer vision system for modeling human interactions.
 <em>IEEE Transactions on Pattern Analysis and Machine Intelligence</em>,
  22(8):831-843, 2000.


</DD>


<DT>[Pic04]</DT>
<DD>
\anchor Piccardi2004
Massimo Piccardi.
 Background subtraction techniques: a review.
 Computer Vision Research Group (CVRG) University of Technology,
  Sydney (UTS), 2004.

</DD>


<DT>[Por01]</DT>
<DD>
\anchor Porikli2001
Fatih&nbsp;Murat Porikli.
 <em>Video Object Segmentation</em>.
 PhD thesis, POLYTECHNIC UNIVERSITY, Brooklyn, NY, April 2001.

</DD>


<DT>[Por05a]</DT>
<DD>
\anchor Porikli2005a
F.&nbsp;Porikli.
 Integral histogram: a fast way to extract histogram features.
 In <em>IEEE Int., Conference on Computer Vision and Pattern
  Recognition (CVPR), Santa Barbara</em>, 2005.

</DD>


<DT>[Por05b]</DT>
<DD>
\anchor Porikli2005
F.&nbsp;Porikli.
 Multiplicative background-foreground estimation under uncontrolled
  illumination using intrinsic images.
 <em>Proceedings of IEEE International Multi-Workshop Motion</em>, 2005.
 Breckenridge.

</DD>


<DT>[Por06]</DT>
<DD>
\anchor Porikli2006
Fatih Porikli.
 Achieving real-time object detection and tracking under extreme
  conditions.
 <em>Journal of Real-Time Image Processing</em>, 1(1):33-40, October
  2006.


</DD>


<DT>[PP06]</DT>
<DD>
\anchor Pnevmatikakis2006
Aristodemos Pnevmatikakis and Lazaros Polymenakos.
 2d person tracking using kalman filtering and adaptive background
  learning in a feedback loop.
 In <em>CLEAR Workshop</em>, 2006.

</DD>


<DT>[PR00]</DT>
<DD>
\anchor Pavesic2000
N.&nbsp;Pavesic and S.&nbsp;Ribaric.
 Gray level thresholding using the havrda and charvat entropy.
 <em>Electrotechnical Conference, 2000. MELECON 2000. 10th
  Mediterranean</em>, 2:631-634 vol.2, 2000.


</DD>


<DT>[PT03]</DT>
<DD>
\anchor Porikli2003
F.&nbsp;Porikli and O.&nbsp;Tuzel.
 Human body tracking by adaptive background models and mean-shift
  analysis.
 In <em>Proceedings of IEEE Intl. Conference on Computer Vision
  Systems (ICVS), Workshop on PETS</em>, 2003.

</DD>


<DT>[Rat08]</DT>
<DD>
\anchor Rat2008
Benoit Rat.
 Semantic images annotation and retrieval.
 Master's thesis, EPFL, LCAV, EPFL, 1015 Lausanne, March 2008.
 Tutor: Jesus Chamorro Martinez, Clement Fredembach; Supervisor:
  Sabine Süsstrunk.

</DD>


<DT>[Ros98]</DT>
<DD>
\anchor Rosin1998
Paul&nbsp;L. Rosin.
 Thresholding for change detection.
 In <em>ICCV</em>, 1998.


</DD>


<DT>[RTK05]</DT>
<DD>
\anchor Rittscher2005
Jens Rittscher, Peter&nbsp;H. Tu, and Nils Krahnstoever.
 Simultaneous estimation of segmentation and shape.
 In <em>CVPR '05: Proceedings of the 2005 IEEE Computer Society
  Conference on Computer Vision and Pattern Recognition (CVPR'05) - Volume 2</em>,
  pages 486-493, Washington, DC, USA, 2005. IEEE Computer Society.


</DD>


<DT>[SA05]</DT>
<DD>
\anchor Sluzek2005
Andrzej Sluzek and Palaniappan Annamalai.
 Development of a reconfigurable sensor network for intrusion
  detection.
 Nanyang Technological University and Intelligent Systems Centre
  Singapore, 2005.

</DD>


<DT>[Sen02]</DT>
<DD>
\anchor Senior2002
A.&nbsp;W. Senior.
 Tracking with probabilistic appearance models.
 <em>In ECCV workshop on Performance Evaluation of Tracking and
  Surveillance Systems</em>, page p 48–55, 2002.

</DD>


<DT>[SG99]</DT>
<DD>
\anchor Stauffer1999
C.&nbsp;Stauffer and W.E.L. Grimson.
 Adaptive background mixture models for real-time tracking.
 <em>Computer Vision and Pattern Recognition, 1999. IEEE Computer
  Society Conference on.</em>, 2:-252 Vol. 2, 1999.


</DD>


<DT>[She04]</DT>
<DD>
\anchor Shen2004
J.&nbsp;Shen.
 Motion detection in color image sequence and shadow elimination.
 In S.&nbsp;Panchanathan and B.&nbsp;Vasudev, editors, <em>Visual
  Communications and Image Processing 2004. Edited by Panchanathan, Sethuraman;
  Vasudev, Bhaskaran. Proceedings of the SPIE, Volume 5308, pp. 731-740
  (2004).</em>, volume 5308 of <em>Presented at the Society of Photo-Optical
  Instrumentation Engineers (SPIE) Conference</em>, pages 731-740, January 2004.


</DD>


<DT>[Sie03]</DT>
<DD>
\anchor Siebel2003
Nils&nbsp;T Siebel.
 <em>Design and Implementation of People Tracking Algorithms for
  Visual Surveillance Applications</em>.
 PhD thesis, The University of Reading, Department of Computer
  Science, Reading, UK, March 2003.


</DD>


<DT>[SR07]</DT>
<DD>
\anchor Sundaraj2007
K.&nbsp;Sundaraj and V.&nbsp;Retnasamy.
 Fast background subtraction for real time monitoring.
 pages 382-387, 2007.

</DD>


<DT>[SWFS03]</DT>
<DD>
\anchor Seki2003
Makito Seki, Toshikazu Wada, Hideto Fujiwara, and Kazuhiko Sumi.
 Background subtraction based on cooccurrence of image variations.
 <em>cvpr</em>, 02:65, 2003.


</DD>


<DT>[TR04]</DT>
<DD>
\anchor Tu2004
Peter&nbsp;H. Tu and Jens Rittscher.
 Crowd segmentation through emergent labeling.
 pages 187-198. 2004.


</DD>


<DT>[TSH06]</DT>
<DD>
\anchor Tsai2006
Yao-Te Tsai, Huang-Chia Shih, and Chung-Lin Huang.
 Multiple human objects tracking in crowded scenes.
 <em>Pattern Recognition, 2006. ICPR 2006. 18th International
  Conference on</em>, 3:51-54, 0-0 2006.


</DD>


<DT>[WB06]</DT>
<DD>
\anchor Welch2006
Greg Welch and Gary Bishop.
 An introduction to the kalman filter.
 Technical report, University of North Carolina at Chapel Hill, Chapel
  Hill, NC, USA, 2006.


</DD>


<DT>[YLPL05]</DT>
<DD>
\anchor Yang2005
Tao Yang, Stan&nbsp;Z. Li, Quan Pan, and Jing Li.
 Real-time multiple objects tracking with occlusion handling in
  dynamic scenes.
 In <em>CVPR '05: Proceedings of the 2005 IEEE Computer Society
  Conference on Computer Vision and Pattern Recognition (CVPR'05) - Volume 1</em>,
  pages 970-975, Washington, DC, USA, 2005. IEEE Computer Society.


</DD>


<DT>[Zel98]</DT>
<DD>
\anchor Zeltkevic1998
Michael Zeltkevic.
 Variance, standard deviation and coefficient of variation.

  http://web.mit.edu/10.001/Web/Course_Notes/Statistics_Notes/Visualization/node4.html, Apr 1998.

</DD>


<DT>[Zha03]</DT>
<DD>
\anchor Zhao2003a
Tao Zhao.
 <em>Model-based Segmentation and Tracking of Multiple Humans in
  Complex Situations</em>.
 PhD thesis, Computer Science Department, University of Southern
  California, Dec 2003.

</DD>


<DT>[Ziv04]</DT>
<DD>
\anchor Zivkovic2004
Z.&nbsp;Zivkovic.
 Improved adaptive gausian mixture model for background subtraction.
 In <em>Proceedings of the International Conference on Pattern
  Recognition</em>, 2004.

</DD>


<DT>[ZN03]</DT>
<DD>
\anchor Zhao2003
Tao Zhao and Ram Nevatia.
 Bayesian human segmentation in crowded situations.
 <em>cvpr</em>, 02:459, 2003.


</DD>


<DT>[ZN04]</DT>
<DD>
\anchor Zhao2004
Tao Zhao and Ram Nevatia.
 Tracking multiple humans in complex situations.
 <em>IEEE trans. PAMI</em>, 26:1208- 1221, 2004.

</DD>


<DT>[ZvdH06]</DT>
<DD>
\anchor Zivkovic2006
Z.&nbsp;Zivkovic and F.&nbsp;van&nbsp;der Heijden.
 Efficient adaptive density estimation per image pixel for the task of
  background subtraction.
 <em>Pattern Recognition Letters</em>, 27(7):773-780, 2006.

</DD>
</DL>
**/
