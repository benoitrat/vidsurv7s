/**
 *  @mainpage
 *  @date 21-jan-2009
 *  @author Benoit RAT
 *
 * @img{logo.png}
 *
 * - #1. @ref page_install
 * 		-# @ref page_install_libraries
 * 		-# @ref page_install_sdk
 * 		-# @ref page_install_cmake

 * - #2. @ref page_config
 *		-# @ref page_network
 *		-# @ref page_board
 *		-# @ref page_processor

 * - #3. @ref page_s7p
 *
 * - #4 @anchor Workflow @ref Workflow
 * 		-	main():
 * 			-	readConfigFromXML()
 *			-	bindSocket()
 * 			-	runS7Protocol()
 * 			-	closeSocket()
 *
 */
