/**
 * @page page_install Installation of the project.
 *
 *
 *	@section page_install_libraries Installation of the libraries
 *			- First install LibPCAP.
 *			- Then OpenCV
 * 
 *	@section page_install_sdk	Eclipse/CDT
 *		We suggest you to install Eclipse/CDT SDK or Wascana,
 *
 *	@section page_install_cmake	Cmake compilation.
 * 		Install cmake to generate the project files or makefile.
 * 
 * 
 *
 *
 *
 *
 *
 *
 */