Last Changes: 04/09/2008
-------------------------

- Software Name: 
	S7PReceiver.

- Platform:
	WinXP, Ubuntu (8.10).

- Library:
	OpenCV, pcap (Winpcap or LibPcap).

- Install:
	Read the documention in doc/hmtl/index.html (The documentation can be find only in the -dev packet)

- Run:  
	Run from a console to load the XML file!
	Or use the run.bat

- Goal: 
	This software communicate with the BOARD by the S7P (Smart Sevensols Protocol) to receive frames.

- Warning:
	The libpcap may not work properly under linux and packet can be lost!
	The microblace need the following files:
			* devtools/src_microblace/TestApp_Peripheral.c
			* include/s7p_hdr.h


