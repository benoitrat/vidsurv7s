% read_rawimg function - Read image in RAW format.
%
% Usage:                 
% Arguments:   
%           fname   - The path of the raw image
%           width   - The width size of the image
%           height  - The height size of the image
%           fig     - The figure number if the user want to display the image
%			is_RG	- The bayer pattern (by default the first pixel is Red and the second Green)
% Returns:
%           imRGB   - An image in RGB format (height,width,3). 
%           imRAW   - An image in structured RAW format (height,width,1).
% References:
%
% Developped and Tested under MATLAB:
%				- R2007b (Windows).
%				- R2008a (Unix).
%
% Copyright (c) 2009 Benoit Rat
% Seven Solutions S.L.
% www.neub.co.nr | www.sevensols.com
% 
% The above copyright notice and this permission notice shall be included in 
% all copies or substantial portions of the Software.

function [imRGB imRAW]=read_rawimg(fname,width,height,fig,is_RG)

%% Set the default params
if nargin < 4, fig = 0; end
if nargin < 5, is_RG = 1; end

%% Open the fname, get the respahe the bin value and close it.
fs = fopen(fname);
if fs ~= -1
    fprintf('open %s\n',fname);
end
bin = fread(fs,width*height,'uint8');

try 
    imRAW = reshape(bin,width,height)';
catch ME
    imRAW = zeros(height,width);
    fprintf('Error: not enough bytes in %s\n',fname);
    fclose(fs);
    return;
end
fclose(fs);


%% RG Bayer.
% R G R G ...
% G B G B ...

if is_RG == 1
%% Red mask
mR=ones(height,width);
mR(1:2:end,2:2:end)=0;
mR(2:2:end,1:end)=0;

%% Green channel (1pt)
mG=ones(height,width);
mG(1:2:end,1:2:end)=0;
mG(2:2:end,2:2:end)=0;

% Blue channel (1pt)
mB=ones(height,width);
mB(1:2:end,2:2:end)=0; 
mB(1:end,1:2:end)=0;

%% BG Bayer.
% B G B G ...
% G R G R ...
else
%% Red mask
mR=ones(height,width);
mR(1:2:end,1:end)=0;    %black odd lines and all cols
mR(2:2:end,2:2:end)=0;  %black even lines and even cols

%% Green mask
mG=ones(height,width);
mG(1:2:end,2:2:end)=0;  %black odd lines and even cols
mG(2:2:end,1:2:end)=0;  %black even lines and odd cols
%imshow(mG(1:10,1:10));

%% Blue mask
mB=ones(height,width);
mB(1:2:end,1:2:end)=0; %black odd lines and odd cols
mB(2:2:end,1:end)=0;   %black even lines and odd cols 
%imshow(mB(1:10,1:10));
end




mask(:,:,1) = mR;
mask(:,:,2) = mG;
mask(:,:,3) = mB;

%% RGB Subsampled
imS = [];
imS(:,:,1)=imRAW(:,:).*mR;
imS(:,:,2)=imRAW(:,:).*mG;
imS(:,:,3)=imRAW(:,:).*mB;


%% Demosaicing by bilinear interpolation
Frb=[1 2 1;2 4 2;1 2 1]./4;
Fg=[0 1 0;1 4 1;0 1 0]./4;
im_bi = imS;

%Apply the filters (be careful of border condition: symetric doesn't work)
im_bi(:,:,1)=imfilter(im_bi(:,:,1),Frb,'symmetric','same');
im_bi(:,:,3)=imfilter(im_bi(:,:,3),Frb,'symmetric','same');
im_bi(:,:,2)=imfilter(im_bi(:,:,2),Fg,'symmetric','same');


%% Show the image if fig > 0
if fig > 0
    figure(fig); fig=fig+1;
    imshow(uint8(imS))
    title('mosaiced image')
    
    figure(fig); fig=fig+1;
    imshow(uint8(im_bi))
    title('Bilinear demosaicing')
end
imRGB=uint8(im_bi); 
