dir='D:\\DATABASE\\0o.RESULTS.o0\\S400_PAL\\'    %Directory
max=-1;     %Maximum number of file to read
width=720;
height=576;
figN=1;


%%List directory
list=ls([dir,'*.bin']);

%%Find the maximum number
if max == -1 
    max = size(list,1);
else
    max = min(max,size(list,1));
end
    
for fnum=1:max
    im = read_yuv422(list(fnum,:),width,height,figN);  
    pause;
end