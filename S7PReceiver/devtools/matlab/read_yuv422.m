% read_rawimg function - Read image in RAW format.
%
% Usage:                 
% Arguments:   
%           fname   - The path of the YUV422 image
%           width   - The width size of the image (multiple of 2)
%           height  - The height size of the image (multiple of 2)
%           fig     - The figure number if the user want to display the
%           image
% Returns:
%           im      - An image in RGB format. 
%
% References: 
%
% Developped and Tested under MATLAB:
%				- R2007b (Windows).
%				- R2008a (Unix).
%
% Copyright (c) 2009 Benoit Rat
% Seven Solutions S.L.
% www.neub.co.nr | www.sevensols.com
% 
% The above copyright notice and this permission notice shall be included in 
% all copies or substantial portions of the Software.

function im=read_yuv422(fname,width,height,fig)

%% Set the default params
if nargin < 4, fig = 0; end

%% Open the fname, get the respahe the bin value and close it.
fs = fopen(fname);
if fs ~= -1
    fprintf('open %s\n',fname);
end

try 
    bin = fread(fs,width*height*2,'uint8');
    fclose(fs);
catch ME
    bin = zeros(height*width,2);
    fprintf('Error: not enough bytes in %s\n',fname);
    fclose(fs);
    return;
end

im_Y4=reshape(bin(2:2:end),width,height)';
im_U2=reshape(bin(1:4:end),width/2,height)';
im_V2=reshape(bin(3:4:end),width/2,height)';

im_U4= imresize(im_U2, [height width]);
im_V4= imresize(im_V2, [height width]);

im_YUV(:,:,1) = im_Y4;
im_YUV(:,:,2) = im_U4;
im_YUV(:,:,3) = im_V4;

%%Convert into RGB
im_RGB = 255*ycbcr2rgb(im_YUV/255);


%% Show the image if fig > 0
if fig > 0
    figure(fig); fig=fig+1;
    subplot(1,3,1);
    imshow(uint8(im_Y4));
    title('im Y');
    subplot(1,3,2);
    imshow(uint8(im_U4));
    title('im U');
    subplot(1,3,3);
    imshow(uint8(im_V4));
    title('im V');
    
    figure(fig); fig=fig+1;
    imshow(uint8(im_RGB));
    title('image RGB');
end
im=uint8(im_RGB); 
