dir='D:\\DATABASE\\0o.RESULTS.o0\\S400_PAL\\'    %Directory
max=-1;     %Maximum number of file to read
figN=1;


%%List directory
list=ls([dir,'*.bmp']);

%%Find the maximum number
if max == -1 
    max = size(list,1);
else
    max = min(max,size(list,1));
end


a= 5; b= 8; c= 12;
h3x3_HW_int =[a b a; b c b; a b a];



h3x3_HW=h3x3_HW_int/sum(h3x3_HW_int(:));
h3x3_med = fspecial('gaussian', 3,0.5);
h3x3_norm = fspecial('gaussian', 3,1);



    
for fnum=1:max
	close all;

	fname=[dir,list(fnum,:)];
    im_pal   =  imread(fname);
	fprintf('\nOpen %s ... ',fname);
	
	%%Make the filters
	im_pal_g_norm = imfilter(im_pal,h3x3_norm);
	im_pal_g_med  = imfilter(im_pal,h3x3_med);
	im_pal_g_HW   = imfilter(im_pal,h3x3_HW);
	
	%%Show the images
	figure(figN); figN=figN+1;
	imshow(im_pal);
	title('PAL');
	
	figure(figN); figN=figN+1;
	imshow(im_pal_g_norm);
	title('PAL Gaussian(\sigma=1)');

	figure(figN); figN=figN+1;
	imshow(im_pal_g_med);
	title('PAL Gaussian(\sigma=0.5)');

	figure(figN); figN=figN+1;
	imshow(im_pal_g_HW);
	title('PAL Gaussian for HW');

	fprintf('Press a key ... ');
	pause;
	
	%%Decimate images
    im_QCIF   =   im_pal(1:2:end,1:2:end,:);
	im_QCIF_g_norm = im_pal_g_norm(1:2:end,1:2:end,:);
	im_QCIF_g_med  = im_pal_g_med(1:2:end,1:2:end,:);
	im_QCIF_g_HW   = im_pal_g_HW(1:2:end,1:2:end,:);
	
	%%Print small images.
	figure(figN); figN=figN+1;
	
	subplot(2,2,1);
	imshow(im_QCIF);
	title('QCIF');
	
	subplot(2,2,2);
	imshow(im_QCIF_g_norm);
	title('QCIF Gaussian(\sigma=1)');

	subplot(2,2,3);
	imshow(im_QCIF_g_med);
	title('QCIF Gaussian(\sigma=0.5)');

	subplot(2,2,4);
	imshow(im_QCIF_g_HW);
	title('QCIF Gaussian for HW');
	
	
	
	fprintf('Press a key ... ');
	pause;
	
end