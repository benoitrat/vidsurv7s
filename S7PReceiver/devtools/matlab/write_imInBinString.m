% write_imImBinString function - write color image in binary (24bits) string representation.
%
% Usage:                 
% Arguments: 
%			fname_im   - The path of the image
%           fname_txt  - The path of the text file with binary code.
% Returns:
%           
% Be careful of the endians conversion and look if you may use swapbytes() command.
%
%
% Developped and Tested under MATLAB:
%				- R2007b (Windows).
%				- R2008a (Unix).
%
% Copyright (c) 2009 Benoit Rat
% Seven Solutions S.L.
% www.neub.co.nr | www.sevensols.com
% 
% The above copyright notice and this permission notice shall be included in 
% all copies or substantial portions of the Software.



function write_imInBinString(fname_im,fname_txt)

%% Number of bits to write (32, 24, 8)
nof_bits=24;

%% Load image
im=imread(fname_im);
imshow(im);
fprintf('Press a key to continue\n');
pause;

%% convert it to n_channels [R,G,B] x n_pixels matrix (Row scanning)
im_p=permute(im,[3 2 1]);
im_mx=reshape(im_p,size(im_p,1),size(im_p,2)*size(im_p,3));
bin_mx=uint32(im_mx);

%% Obtain the size
nof_chan=size(bin_mx,1);
nof_pix=size(bin_mx,2);

%% Write in a text file in 
fs = fopen(fname_txt,'w');
if fs ~= -1
    fprintf('write in %s\n',fname_txt);
end

fprintf(fs,'Number of Channels = %d and Number of Pixels = %d \n',nof_chan,nof_pix);
for i=1:nof_pix;
   val=uint32(0);
   for c=1:nof_chan
       val = val + bitshift(bin_mx(c,i),(c-1)*8);
   end
   %fprintf(fs,[dec2hex(val,6),',\n']);
  fprintf(fs,[dec2bin(val,nof_bits),',\n']);
end
fprintf(fs,'\n};\n');
fclose(fs);