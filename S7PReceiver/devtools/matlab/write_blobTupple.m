% write_rawimg function - write image in RAW format.
%
% Usage:                 
% Arguments:   
%           fname   - The path of the raw image to write
%           im_RGB  - The image to write.
% Returns:
%           im      - An image in RGB format. 
%
% References:
%
% Developped and Tested under MATLAB:
%				- R2007b (Windows).
%				- R2008a (Unix). 
%
% Copyright (c) 2009 Benoit Rat
% Seven Solutions S.L.
% www.neub.co.nr | www.sevensols.com
% 
% The above copyright notice and this permission notice shall be included in 
% all copies or substantial portions of the Software.

function [id_vec, s_vec, e_vec, r_vec, ser_vec] = write_BlobTupple(fname,im_RGB,fig)

%% Set the default params
if nargin < 3, fig = 0; end


[height width nChannels] = size(im_RGB);
im_RGB=double(im_RGB);
im_label = bwlabel(im_RGB(:,:,1)>200);

id_vec = [];
s_vec = [];
e_vec = [];
r_vec = [];

id=1;
count=0;
for r=1:height
	for c=1:width
		% If white pixel
		if im_label(r,c) > 0
			if count == 0
				id_vec(id)=im_label(r,c);
				r_vec(id)=r;
				s_vec(id)=c;
			end
			count=count+1;
		% else black pixel	
		else
			if count > 0
				e_vec(id)=c;
				id=id+1;
			end
			count=0;
		end
	end
end

ser_vec = zeros(1,length(s_vec));
ser_vec = ser_vec + bitshift(bitand(s_vec,2^9-1),18);
ser_vec = ser_vec + bitshift(bitand(e_vec,2^9-1),9);
ser_vec = ser_vec + bitshift(bitand(r_vec,2^9-1),0);

bin_vec = uint32(zeros(1,2*length(s_vec)));
bin_vec(1:2:end) = uint32(id_vec(:));
bin_vec(2:2:end) = uint32(ser_vec(:));

%% Write in a text file in 
fname_txt=[fname,'.txt'];
fs = fopen(fname_txt,'w');
if fs ~= -1
    fprintf('write in %s\n',fname_txt);
end
fprintf(fs,'unsigned int rle[%d];\n\n',length(bin_vec));
for i=1:length(bin_vec);
    if mod(i,2)==0
       format='%d'; 
    else
        fprintf(fs,'  ');
        format='%d';
    end
    
   fprintf(fs,['rle[%d]=',format,'; '],i-1,bin_vec(i));
   if mod(i,6) == 0
       fprintf(fs,'\n',bin_vec(i));
   end
end
fprintf(fs,'\n};\n');
fclose(fs);
    
%%Write in a binary file in big endian
fname_bin=[fname,'_be.bin'];
fs = fopen(fname_bin,'wb');
if fs ~= -1
    fprintf('write in %s\n',fname_bin);
end
fwrite(fs,swapbytes(bin_vec(:)),'uint32');
fclose(fs);
