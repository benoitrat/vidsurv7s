% write_binStream function - write image in binary string representation.
%
% Usage:                 
% Arguments:   
%           fname   - The path of the text file with binary code.
%           vec   	- A vector in uint8.
% Returns:
%           
%
% The software doesn't work for the moment with uint32 or uint16.
% Be careful of the endians conversion and look if you may use swapbytes() command.
%
% Developped and Tested under MATLAB:
%				- R2007b (Windows).
%				- R2008a (Unix).
%
% Copyright (c) 2009 Benoit Rat
% Seven Solutions S.L.
% www.neub.co.nr | www.sevensols.com
% 
% The above copyright notice and this permission notice shall be included in 
% all copies or substantial portions of the Software.

function write_binString(fname,vec)


%% convert in uint8
bin_vec=uint8(vec);

%% Write in a text file in 
fs = fopen(fname,'w');
if fs ~= -1
    fprintf('write in %s\n',fname);
end
fprintf(fs,'unsigned int rle[%d] = {\n',length(bin_vec));
for i=1:length(bin_vec);
   fprintf(fs,[dec2bin(bin_vec(i),8),',\n']);
end
fprintf(fs,'\n};\n');
fclose(fs);