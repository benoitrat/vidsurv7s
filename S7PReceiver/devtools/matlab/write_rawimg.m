% write_rawimg function - write image in RAW format.
%
% Usage:                 
% Arguments:   
%           fname   - The path of the raw image to write
%           im_RGB  - The image to write.
% Returns:
%           im      - An image in RGB format. 
%
% References:
%
% Developped and Tested under MATLAB:
%				- R2007b (Windows).
%				- R2008a (Unix). 
%
% Copyright (c) 2009 Benoit Rat
% Seven Solutions S.L.
% www.neub.co.nr | www.sevensols.com
% 
% The above copyright notice and this permission notice shall be included in 
% all copies or substantial portions of the Software.

function imRAW=write_rawimg(fname,im_RGB,fig,is_RG)

%% Set the default params
if nargin < 3, fig = 0; end
if nargin < 4, is_RG = 1; end

[height width nChannels] = size(im_RGB);
im_RGB=double(im_RGB);

%% RG Bayer.
% R G R G ...
% G B G B ...

if is_RG == 1
%% Red mask
mR=ones(height,width);
mR(1:2:end,2:2:end)=0;
mR(2:2:end,1:end)=0;

%% Green channel (1pt)
mG=ones(height,width);
mG(1:2:end,1:2:end)=0;
mG(2:2:end,2:2:end)=0;

% Blue channel (1pt)
mB=ones(height,width);
mB(1:2:end,2:2:end)=0; 
mB(1:end,1:2:end)=0;

%% BG Bayer.
% B G B G ...
% G R G R ...
else
%% Red mask
mR=ones(height,width);
mR(1:2:end,1:end)=0;    %black odd lines and all cols
mR(2:2:end,2:2:end)=0;  %black even lines and even cols

%% Green mask
mG=ones(height,width);
mG(1:2:end,2:2:end)=0;  %black odd lines and even cols
mG(2:2:end,1:2:end)=0;  %black even lines and odd cols
%imshow(mG(1:10,1:10));

%% Blue mask
mB=ones(height,width);
mB(1:2:end,1:2:end)=0; %black odd lines and odd cols
mB(2:2:end,1:end)=0;   %black even lines and odd cols 
%imshow(mB(1:10,1:10));
end



%%Create the mask
mask(:,:,1) = mR;
mask(:,:,2) = mG;
mask(:,:,3) = mB;

%% RGB Subsampled
imS = [];
imS(:,:,1)=im_RGB(:,:,1).*mR;
imS(:,:,2)=im_RGB(:,:,2).*mG;
imS(:,:,3)=im_RGB(:,:,3).*mB;

%%Concatenate all the channel in only one.
imRAW = sum(imS,3);


%% Show the image if fig > 0
if fig > 0
    figure(fig); fig=fig+1;
    imshow(uint8(imS))
    title('mosaiced image')
    
    figure(fig); fig=fig+1;
    imshow(uint8(imRAW))
    title('RAW image')
end
imRAW=uint8(imRAW); 

bin=imRAW';

fs = fopen(fname,'wb');
if fs ~= -1
    fprintf('write in %s\n',fname);
end
fwrite(fs,bin(:),'uint8');
fclose(fs);
