dir='./'    %Directory
max=-1;     %Maximum number of file to read
width=640;
height=480;
fig=1;


%%List directory
list=ls([dir,'*.bin']);

%%Find the maximum number
if max == -1 
    max = size(list,1);
else
    max = min(max,size(list,1));
end
    
for fnum=1:max
    im = read_rawimg(list(fnum,:),width,height,fig);  
    pause;
end