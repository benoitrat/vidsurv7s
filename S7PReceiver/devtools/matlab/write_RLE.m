% write_RLE function - write Run-Length Encoding format.
%
% Usage:                 
% Arguments:   
%           fname   - The path of the raw image to write
%           im_RGB  - The image to write.
% Returns:
%           im      - An image in RGB format. 
%
% References:
%
% Developped and Tested under MATLAB:
%				- R2007b (Windows).
%
% Copyright (c) 2009 Benoit Rat
% Seven Solutions S.L.
% www.neub.co.nr | www.sevensols.com
% 
% The above copyright notice and this permission notice shall be included in 
% all copies or substantial portions of the Software.

function [id_vec, s_vec, e_vec, r_vec] = write_RLE(fname,im_RGB)

%% Set the default params
if nargin < 3, fig = 0; end


[height width nChannels] = size(im_RGB);
im_RGB=double(im_RGB);
im_label = bwlabel(im_RGB(:,:,1)>200);

id_vec = [];
s_vec = [];
e_vec = [];
r_vec = [];

id=1;
count=0;
for r=1:height
	for c=1:width
		% If white pixel
		if im_label(r,c) > 0
			if count == 0
				id_vec(id)=im_label(r,c);
				r_vec(id)=r;
				s_vec(id)=c;
			end
			count=count+1;
		% else black pixel	
		else
			if count > 0
				e_vec(id)=c;
				id=id+1;
			end
			count=0;
		end
	end
end


%% Write in a text file in 
fname_txt=[fname,'.txt'];
fs = fopen(fname_txt,'w');
if fs ~= -1
    fprintf('write in %s\n',fname_txt);
end
for i=1:length(s_vec);
   fprintf(fs,'%3d \t%3d \t%3d \n',s_vec(i),e_vec(i),r_vec(i));
end
fprintf(fs,'\n');
fclose(fs);  
