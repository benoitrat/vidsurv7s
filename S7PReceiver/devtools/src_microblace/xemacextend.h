#ifndef _XEMAC_EXTEND_H_
#define _XEMAC_EXTEND_H_

#include "xemaclite_l.h"
#include "xemaclite.h"
#include "xdmacentral.h"
#include "xdmacentral_l.h"



#define XEmacLite_mGetReceiveDataLength(BaseAddress)			\
((XEmacLite_mReadReg((BaseAddress), XEL_HEADER_OFFSET + XEL_RXBUFF_OFFSET) >> \
	XEL_HEADER_SHIFT) & (XEL_RPLR_LENGTH_MASK_HI | XEL_RPLR_LENGTH_MASK_LO))
	

//====================================================
//  Prototype of C function

int Emac_Send_DMA(XDmaCentral *InstancePtr_DMA, XEmacLite * InstancePtr, u8 *FramePtrHeader, u16 LengthHeader, u32 *DataDMA, u16 LengthData);
int transfer_DMA(XDmaCentral *InstancePtr_DMA, void *SrcPtr, void *DestPtr, u32 ByteCount);
u16 XEmacExtend_Recv(XEmacLite * InstancePtr, u8 *framePtr, u16 ByteCount);

#endif
