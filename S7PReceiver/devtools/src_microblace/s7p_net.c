#include "s7p_net.h"
//====================================================

/*
 * Instance of the driver
 */
static XEmacLite EmacLite;
static XDmaCentral DmaCentral;



void start_S7Protocol(Parameters *params) {


	XStatus status;
	XEmacLite_Config *ConfigPtr;
	XEmacLite *InstancePtr = &EmacLite;
	XDmaCentral_Config *ConfigPtr_DMA;
	XDmaCentral *InstancePtr_DMA = &DmaCentral;

	int i, j, k, p;

	u8 camID;
	u8 camID_stop;
	int stop_flag = 1, send_flag = 1;
	int win_sup = 0, win_inf = 0, saturate = 0;
	ushort life;

	u32 bytes_rcv;
	u32 nof_blocks=0, last_block_size, b, mem_size, offset;
	u32 *p_image, *rle;	//Pointer on the image memory
	u8 *p_u8;

	Xuint8 eha_BC[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
	CamParams *cam;
	
//	s7p_pkt_t *mypkt=0x847FFa00;
//	s7p_pkt_t *pcpkt=0x847FFb00 ;
	
	s7p_pkt_t *mypkt, *pcpkt;
	s7p_pkt_t pcpkt_buff, mypkt_buff;
	pcpkt = (s7p_pkt_t*)&pcpkt_buff;
	mypkt = (s7p_pkt_t*)&mypkt_buff;
	
	
	
 //=========================================================================================================
	//MANDAR POR ETHERNET
	
	status = XDmaCentral_Initialize(InstancePtr_DMA, XPAR_XPS_CENTRAL_DMA_0_DEVICE_ID);

//		if (status == 0) {
//			xil_printf("Cfg DMA PASSED\r\n");
//		}
//		else {
//			xil_printf("Cfg DMA FAILED\r\n");
//		}



	//Initialize the EmacLite device.
	//ConfigPtr = XEmacLite_LookupConfig(XPAR_XPS_ETHERNETLITE_0_DEVICE_ID);
	ConfigPtr = XEmacLite_LookupConfig(XPAR_EMACLITE_0_DEVICE_ID);

//		if (ConfigPtr == NULL) {
//			print("Emaclite LookupConfig FAILED\r\n");
//		}
//		else {
//			print("Emaclite LookupConfig PASSED\r\n");
//		}

	status = XEmacLite_CfgInitialize(InstancePtr, ConfigPtr, ConfigPtr->BaseAddress);
//	      if (status == 0) {
//	         print("CfgInitialize PASSED\r\n");
//	      }
//	      else {
//	         print("CfgInitialize FAILED\r\n");
//	      }

	for (i = 0; i < 6; i++) {
		mypkt->mac_src[i] = params->mac_src[i];
		mypkt->mac_dst[i] = eha_BC[i];
	}
	XEmacLite_SetMacAddress(InstancePtr, params->mac_src);
	
	
	camID=CAMID_1;
	camID_stop=CAMID_1;
	mypkt->type=MAC_TYPE_S7P;
	mypkt->zeros=0x0000;

	while(TRUE) {

		bytes_rcv = XEmacExtend_Recv(InstancePtr,(u8*)pcpkt,SIZEIB_PKT_HDR);

		if(bytes_rcv > 0) { //Continue listening on the interface.
			if(pcpkt->type == MAC_TYPE_S7P) {	
				switch(pcpkt->cmd) {
	
				/* PC -> BOARD: Are you connected? */
				case S7P_CMD_BOARD_ASK:
					for (i = 0; i < 6; i++){ //CONFIG MAC PC DEST
						mypkt->mac_dst[i] = pcpkt->mac_src[i];
					}
					mypkt->camID=CAMID_ALL;
					mypkt->cmd = S7P_CMD_BOARD_ACK; //BOARD -> PC: Yes, I'am connected!
					mypkt->val=pcpkt->val; //Copy the ID of the pkt sent
					status = XEmacLite_Send(InstancePtr, (u8*)mypkt,SIZEIB_PKT_HAD); //Send packet.
					break;
	
					/* PC -> BOARD: Give me parameters! */
				case S7P_CMD_PARAM_GET:
					mypkt->cmd=S7P_CMD_PARAM_ACK; //BOARD -> PC: The parameters is:
					param_GetSet(mypkt,pcpkt,params);
					status = XEmacLite_Send(InstancePtr, (u8*)mypkt,SIZEIB_PKT_HAD); //Send packet.
					break;
	
					/* PC -> BOARD: Start sending image */
				case S7P_CMD_SEND_START:
					win_sup = 0;
					win_inf = 0;
					camID=CAMID_1;
					while(!params->ar_cam[camID].is_enable) {
						if(camID<NOF_CAMERA) camID++;
						else {
							camID=CAMID_1;
							params->ar_cam[camID].is_enable=0xFF;
						}
					}
					camID_stop = CAMID_NONE;
					break;
	
					/* PC -> BOARD: Stop sending image */
				case S7P_CMD_SEND_STOP:
					camID_stop=camID; //Set on which camera we stop.
					break;
	
					/* PC -> BOARD: A frame has been acked */
				case S7P_CMD_FRAME_ACKS:
					win_inf++;
					break;
	
					/* PC -> BOARD: ????? */
				default:
					if(pcpkt->type==MAC_TYPE_S7P) {
						mypkt->cmd = S7P_CMD_UNKOWN;
						mypkt->camID=pcpkt->cmd; //For debugging reply with the unknown cmd.
						status = XEmacLite_Send(InstancePtr, (u8*)mypkt,SIZEIB_PKT_HAD); //Send packet.
					}
					break;
	
	
				} //End(switch(pcpkt->cmd))
			}	//End(if(pcpkt->type == MAC_TYPE_S7P))
		}	//End(if(bytes_rcv > 0))


		if(camID!=camID_stop) { //If the cam_id is not the camID_stop we enter, otherwise looping.
			if((params->win_size==0) || (win_sup - win_inf) < params->win_size) { //Check the numbers of frames acknowledged.


				//Remove the saturation when the ACK_windows decrease to the half of its maximum.
				if ( (params->win_size==0) || (win_sup - win_inf) <= (params->win_size/2)) {
					saturate = FALSE;
				}

				//Select the parameters of the Camera
				cam = &(params->ar_cam[camID]);

				//The size and number of blocks
				nof_blocks=0, last_block_size=0,b=0;

				//this size and the number of blocks is given by mem_size.
				last_block_size=(cam->mem_size%SIZEIB_PKT_DATA);  //Compute some bytes need to be put in an extra packet.
				nof_blocks=cam->mem_size/SIZEIB_PKT_DATA; //(Total size)/(maximum packet size)
				if(last_block_size==0) last_block_size=SIZEIB_PKT_DATA; //If there is no extra packet the last packet is full
				else nof_blocks++;	//If there is an extra packet add it.


				//--------------------------------------
				//                Debug packets
				//--------------------------------------
				
				//DEBUG BOARD
				mypkt->type=MAC_TYPE_S7P;
				mypkt->zeros=0x0000;
				mypkt->cmd = S7P_CMD_DEBUG;
				mypkt->camID	 = CAMID_NONE;
				mypkt->fpID=0xABCD;
				memcpy(mypkt->data,params,sizeof(Parameters));
				mypkt->val=(u32)params;
				while (!XEmacLite_TxBufferAvailable(InstancePtr));
				XEmacLite_Send(InstancePtr, (u8*)mypkt,SIZEIB_PKT_HAD+sizeof(Parameters)); //Send packet.


				//DEBUG CAMERA
				mypkt->type=MAC_TYPE_S7P;
				mypkt->cmd = S7P_CMD_DEBUG;
				mypkt->camID	 = camID;
				mypkt->fpID=nof_blocks;
				memcpy(mypkt->data,cam,sizeof(CamParams));
				mypkt->val=(u32)params;
				while (!XEmacLite_TxBufferAvailable(InstancePtr));
				XEmacLite_Send(InstancePtr, (u8*)mypkt,SIZEIB_PKT_HAD+sizeof(CamParams)); //Send packet.

				//-----------------------------------------


				mypkt->cmd=S7P_CMD_FRAME_SETUP;
				mypkt->camID	 = camID;
				if(params->dtype >= 0x100) mypkt->val	= cam->im_resolution;
				else mypkt->val= cam->mem_size;
				mypkt->fpID 	 = cam->frame_id;

				p_image = (u32*)(cam->mem_start);
				offset=0;

				if(nof_blocks==0 || cam->mem_size==0 ) {
					camID=CAMID_1;
					camID_stop=CAMID_1;
				}
				if(nof_blocks>5000) nof_blocks=5000;

				//Send the N-1 blocks (except the last one).
				for(b=0; b < nof_blocks-1; b++) {

					//Send a block of the frame.
					while (!XEmacLite_TxBufferAvailable(InstancePtr));
					status = Emac_Send_DMA(InstancePtr_DMA, InstancePtr, (u8*)mypkt, SIZEIB_PKT_HDR, (u32*)p_image, SIZEIB_PKT_DATA);

					//Set the next block of the frame
					p_image+=(SIZEIB_PKT_DATA/4);
					offset+=(u32)SIZEIB_PKT_DATA;			//Set the next position read
					mypkt->cmd=S7P_CMD_FRAME_BLOCK;	//Set that we send normal block of frame.
					mypkt->val = offset;				//Set the offset in the packet
				}
				//Send the last block of this frame with last_block_size.
				mypkt->cmd=S7P_CMD_FRAME_LAST;
				while (!XEmacLite_TxBufferAvailable(InstancePtr));
				status = Emac_Send_DMA(InstancePtr_DMA, InstancePtr, (u8*)mypkt, SIZEIB_PKT_HDR, (u32*)p_image, last_block_size);

				//Increment the limit superior of the windows.
				win_sup++;
				cam->frame_id++;

				//Jump to the next ID.
				do {
					camID++;
					if(camID >= NOF_CAMERA) camID=CAMID_1;
				}
				while(!params->ar_cam[camID].is_enable);

			} //End((params->win_size==0) || (win_sup - win_inf) < params->win_size)
			else{
				saturate = TRUE;	//Set that the sending saturates the bandwidth.
			}
		}
	} //End(while(TRUE))
}

//========================================================================================


int param_GetSet(s7p_pkt_t *mypkt, s7p_pkt_t *xilpkt, Parameters *params) {
	u8 to_set = (xilpkt->cmd==S7P_CMD_PARAM_SET);
	u32 *ptr;
	int i,i_start,i_end, pID;

	pID=xilpkt->fpID;
	mypkt->fpID=xilpkt->fpID;

	//If read-only
	if(S7P_PRM_RO_VFW <= pID && pID < S7P_END_PRM_RO) {
		if(pID == S7P_PRM_RO_NOFCAM) mypkt->val=NOF_CAMERA;
		else if(pID == S7P_PRM_RO_VFW) mypkt->val=VFIRMWARE;
		else if(pID == S7P_PRM_RO_VS7P) mypkt->val=VS7PROTO;
	}
	//If boards parameters
	else if(S7P_PRM_BRD_IP <= pID && pID < S7P_END_PRM_BRD) {
		ptr = &(params->ip_src) + (pID-S7P_PRM_BRD_IP);
		if(to_set) {
			*ptr = xilpkt->val;
		}
		mypkt->val=*ptr;
	}
	//If camera parameters
	else if(S7P_PRM_CAM_ENABLE <= pID && pID < S7P_PRM_UNKNOWN) {

		//Set on which camera we are going to loop.
		if(xilpkt->camID==CAMID_ALL) {
			i_start=CAMID_1;
			i_end=NOF_CAMERA;
		}
		else {
			i_start=xilpkt->camID;
			i_end=xilpkt->camID+1;
		}

		//Loop on all the camera or only on the specific one.
		for(i=i_start;i<i_end;i++) {
			if(params->ar_cam[i].is_enable) {
				if(S7P_PRM_CAM_ENABLE <= pID && pID < S7P_END_PRM_CAM) {
					ptr = &(params->ip_src) + (pID-S7P_PRM_CAM_ENABLE);
					if(to_set) {
						*ptr = xilpkt->val;
					}
					mypkt->val=*ptr;
				}
//				else if(S7P_PRM_IIDC_BRIGHTNESS <= pID && pID < S7P_END_PRM_IIDC) {
//					if(to_set) {
//						firecam_set_param_status(i,param_id,xilpkt->val);
//					}
//					mypkt->val = firecam_get_param_status(i,param_id);
//				}
				else {
					mypkt->cmd=S7P_CMD_PARAM_ERR;
					mypkt->val=S7P_ERR_PRM_NOTDEF;
					return -1;
				}
			}
		}//End for
	}
	//Parameters is unknown.
	else {
		mypkt->cmd=S7P_CMD_PARAM_ERR;
		mypkt->val=S7P_ERR_PRM_NOTDEF;
		return -1;
	}
	return 1;
}



//========================================================================================

void init_CamParams(CamParams *cam, u32 mem_start, u32 mem_size, u16 width, u16 height) {

	//Set the register parameters.
	cam->is_enable=TRUE;
	cam->im_resolution=height & 0x00000FFFF;
	cam->im_resolution+=(width << 16);
	cam->quality=JPEG_Q_HIGH;
	cam->bg_refresh=50;
	cam->bg_thrs=10;
	cam->cc_minarea=5;
	cam->prm_debug=0;

	//Set processor parameters
	cam->mem_start=mem_start;
	cam->mem_size=mem_size;
	cam->frame_id=0;
}
