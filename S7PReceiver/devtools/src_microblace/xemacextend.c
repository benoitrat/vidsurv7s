#include "xemacextend.h"

int transfer_DMA(XDmaCentral *InstancePtr_DMA, void *SrcPtr, void *DestPtr, u32 ByteCount){

	u32 RegValue;

	XDmaCentral_Reset(InstancePtr_DMA);

	/* Setup the DMA Control register to be:
	 *	- Source address incrementing
	 *	- Destination address incrementing
	 */
	XDmaCentral_SetControl(InstancePtr_DMA,
			XDMC_DMACR_SOURCE_INCR_MASK |
			XDMC_DMACR_DEST_INCR_MASK);

	/*
	 * Flush the Data Cache in the case the Data Cache is enabled.
	 */
	XCACHE_FLUSH_DCACHE_RANGE(&SrcPtr,  ByteCount);
	XCACHE_FLUSH_DCACHE_RANGE(&DestPtr, ByteCount);

	/*
	 * Start the DMA transfer.
	 */
	XDmaCentral_Transfer(InstancePtr_DMA, SrcPtr, DestPtr, ByteCount);

	/*
	 * Wait until the DMA transfer is done
	 */
	do {
		/*
		 * Poll DMA status register
		 */
		RegValue = XDmaCentral_GetStatus(InstancePtr_DMA);
	}
	while ((RegValue & XDMC_DMASR_BUSY_MASK) == XDMC_DMASR_BUSY_MASK);


	/*
	 * If Bus error occurs, reset the device and return the error code.
	 */
	if (RegValue & XDMC_DMASR_BUS_ERROR_MASK) {
		XDmaCentral_Reset(InstancePtr_DMA);
		return XST_FAILURE;
	}


	/*
	 * Destination buffer's contents are the same as the source buffer
	 * Reset the device again and return success code.
	 */
	XDmaCentral_Reset(InstancePtr_DMA);

	return XST_SUCCESS;

}

/**
 * @brief Copy the S7P_PKT_header and send it to the Tx buffer, than used the DMA to copy BRAM memory to the Tx buffer.
 * @note The size of the packet header is given by SIZEIB_PKT_HDR.
 */ 
int Emac_Send_DMA(XDmaCentral *InstancePtr_DMA, XEmacLite * InstancePtr, u8 *FramePtrHeader, u16 LengthHeader, u32 *DataDMA, u16 LengthData)
{
	u32 Register;
	u32 BaseAddress;
	XStatus status;

	/*
	 * Verify that each of the inputs are valid.
	 */
	XASSERT_NONVOID(InstancePtr != NULL);

	/*
	 * Determine the expected TX buffer address
	 */
	BaseAddress = XEmacLite_mNextTransmitAddr(InstancePtr);

	/*
	 * Check the Length if it is too large, truncate it.
	 * The maximum Tx packet size is
	 * Ethernet header (14 Bytes) + Maximum MTU (1500 bytes)
	 */
	if (LengthData > (XEL_MAX_TX_FRAME_SIZE-LengthHeader)) {

		LengthData = (XEL_MAX_TX_FRAME_SIZE-LengthHeader);
	}

	/*
	 * Determine if the expected buffer address is empty
	 */
	Register = XEmacLite_mGetTxStatus(BaseAddress);

	/*
	 * If the expected buffer is available, fill it with the provided data
	 * Align if necessary.
	 */
	if ((Register & (XEL_TSR_XMIT_BUSY_MASK |
			XEL_TSR_XMIT_ACTIVE_MASK)) == 0) {

		/*
		 * Switch to next buffer if configured
		 */
		if (InstancePtr->EmacLiteConfig.TxPingPong != 0) {
			InstancePtr->NextTxBufferToUse ^= XEL_BUFFER_OFFSET;
		}

		/*
		 * Write the frame to the buffer.
		 */

		XEmacLite_AlignedWrite(FramePtrHeader, (u32 *) BaseAddress, LengthHeader);

		//		xil_printf("base address %x\r\n", BaseAddress);
		status = transfer_DMA(InstancePtr_DMA, DataDMA, (u32 *) (BaseAddress+LengthHeader), LengthData);
		if (status != 0) {
			return status;
		}
		//if(status == 0) {
		//			print("Transfer DMA PASSED\r\n");
		//		}
		//		else {
		//			print("Transfer DMA FAILED\r\n");
		//		}

		/*
		 * The frame is in the buffer, now send it
		 * Start to send at BaseAddress and size of data to send: (ByteCount+SIZEIB_PKT_HDR).
		 */
		XEmacLite_mWriteReg(BaseAddress, XEL_TPLR_OFFSET,
				((LengthHeader+LengthData) & (XEL_TPLR_LENGTH_MASK_HI |
						XEL_TPLR_LENGTH_MASK_LO)));

		/*
		 * Update the Tx Status Register to indicate that there is a
		 * frame to send.
		 * If the interrupts are enabled then set the
		 * XEL_TSR_XMIT_ACTIVE_MASK flag which is used by the interrupt
		 * handler to call the callback function provided by the user
		 * to indicate that the frame has been transmitted.
		 */
		Register = XEmacLite_mGetTxStatus(BaseAddress);
		Register |= XEL_TSR_XMIT_BUSY_MASK;
		if ((Register & XEL_TSR_XMIT_IE_MASK) != 0) {
			Register |= XEL_TSR_XMIT_ACTIVE_MASK;
		}
		XEmacLite_mSetTxStatus(BaseAddress, Register);

		return XST_SUCCESS;
	}

	/*
	 * If the expected buffer was full, try the other buffer if configured
	 */
	if (InstancePtr->EmacLiteConfig.TxPingPong != 0) {

		BaseAddress ^= XEL_BUFFER_OFFSET;

		/*
		 * Determine if the expected buffer address is empty
		 */
		Register = XEmacLite_mGetTxStatus(BaseAddress);

		/*
		 * If the next buffer is available, fill it with the provided
		 * data
		 */
		if ((Register & (XEL_TSR_XMIT_BUSY_MASK |
				XEL_TSR_XMIT_ACTIVE_MASK)) == 0) {

			/*
			 * Write the frame to the buffer.
			 */
			//			XEmacLite_AlignedWrite(FramePtr, (u32 *) BaseAddress,
			//					       ByteCount);

			XEmacLite_AlignedWrite(FramePtrHeader, (u32 *) BaseAddress, LengthHeader);

			status = transfer_DMA(InstancePtr_DMA, DataDMA, (u32 *) (BaseAddress+LengthHeader), LengthData);
			//		if (status == 0) {
			//			print("Transfer DMA PASSED\r\n");
			//		}
			//		else {
			//			print("Transfer DMA FAILED\r\n");
			//		}


			/*
			 * The frame is in the buffer, now send it
			 */
			XEmacLite_mWriteReg(BaseAddress, XEL_TPLR_OFFSET,
					((LengthHeader+LengthData) & (XEL_TPLR_LENGTH_MASK_HI |
							XEL_TPLR_LENGTH_MASK_LO)));

			/*
			 * Update the Tx Status Register to indicate that there
			 * is a frame to send.
			 * If the interrupts are enabled then set the
			 * XEL_TSR_XMIT_ACTIVE_MASK flag which is used by the
			 * interrupt handler to call the callback function
			 * provided by the user to indicate that the frame has
			 * been transmitted.
			 */
			Register = XEmacLite_mGetTxStatus(BaseAddress);
			Register |= XEL_TSR_XMIT_BUSY_MASK;
			if ((Register & XEL_TSR_XMIT_IE_MASK) != 0) {
				Register |= XEL_TSR_XMIT_ACTIVE_MASK;
			}

			XEmacLite_mSetTxStatus(BaseAddress, Register);

			/*
			 * Do not switch to next buffer, there is a sync problem
			 * and the expected buffer should not change.
			 */
			return XST_SUCCESS;
		}
	}


	/*
	 * Buffer(s) was(were) full, return failure to allow for polling usage
	 */
	return XST_FAILURE;
}



/*****************************************************************************/
/**
*
* Receive a packet with a length. Intended to be called from the interrupt context or
* with a wrapper which waits for the receive frame to be available.
*
* @param	InstancePtr is a pointer to the XEmacLite instance.
* @param FramePtr is a pointer to a buffer where the S7P packet will
*		be stored. 
* @param ByteCount is the size of the data for the buffer mentioned above.
* 
* @return The total size of data received. If there is no packet it returns 0.
*
* @note This function call is not blocking in nature, i.e. it will not wait until
* a frame arrives.
* 
* @see XEmacLite_Recv()
*
******************************************************************************/
u16 XEmacExtend_Recv(XEmacLite * InstancePtr, u8 *FramePtr, u16 ByteCount)
{
	u16 EtherType;
	u16 Length;
	u32 Register;
	u32 BaseAddress;

	/*
	 * Verify that each of the inputs are valid.
	 */

	XASSERT_NONVOID(InstancePtr != NULL);

	/*
	 * Determine the expected buffer address
	 */
	BaseAddress = XEmacLite_mNextReceiveAddr(InstancePtr);

	/*
	 * Verify which buffer has valid data
	 */
	Register = XEmacLite_mGetRxStatus(BaseAddress);

	if ((Register & XEL_RSR_RECV_DONE_MASK) == XEL_RSR_RECV_DONE_MASK) {

		/*
		 * The driver is in sync, update the next expected buffer if
		 * configured
		 */

		if (InstancePtr->EmacLiteConfig.RxPingPong != 0) {
			InstancePtr->NextRxBufferToUse ^= XEL_BUFFER_OFFSET;
		}
	}
	else {
		/*
		 * The instance is out of sync, try other buffer if other
		 * buffer is configured, return 0 otherwise. If the instance is
		 * out of sync, do not update the 'NextRxBufferToUse' since it
		 * will correct on subsequent calls.
		 */
		if (InstancePtr->EmacLiteConfig.RxPingPong != 0) {
			BaseAddress ^= XEL_BUFFER_OFFSET;
		}
		else {
			return 0;	/* No data was available */
		}

		/*
		 * Verify that buffer has valid data
		 */
		Register = XEmacLite_mGetRxStatus(BaseAddress);
		if ((Register & XEL_RSR_RECV_DONE_MASK) !=
				XEL_RSR_RECV_DONE_MASK) {
			return 0;	/* No data was available */
		}
	}

	/*
	 * Get the Ethernet Type of the frame that arrived
	 */
	EtherType = XEmacLite_mGetReceiveDataLength(BaseAddress);
	Length=ByteCount;

	

	/*
	 * Read from the EmacLite
	 */
	XEmacLite_AlignedRead(((u32 *) (BaseAddress + XEL_RXBUFF_OFFSET)),
			      FramePtr, Length);

	/*
	 * Acknowledge the frame
	 */
	Register = XEmacLite_mGetRxStatus(BaseAddress);
	Register &= ~XEL_RSR_RECV_DONE_MASK;
	XEmacLite_mSetRxStatus(BaseAddress, Register);

	return Length;
}

