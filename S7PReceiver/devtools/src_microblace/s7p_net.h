#ifndef _S7P_NET_H_
#define _S7P_NET_H_


//====================================================
//  Includes
#include "s7p_hdr.h" //!< Add this header for S7P Protocol


#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "xutil.h"	 		//Add the typedef and everything ...
#include "xparameters.h"	//Give the direction in memory

#include "xemacextend.h"


//====================================================

#define NOF_CAMERA 1 //!< Number maximum of camera that the board can handle.

/**
 * @brief The different type of quality for JPEG compression.
 */
enum {
	JPEG_Q_HIGH,
	JPEG_Q_MEDIUM,
	JPEG_Q_LOW,
};


//====================================================

#define SIZEIB_PKT_MTU 1500	//!< Maximum size that the packet can reach
#define SIZEIB_PKT_HDR 24 	//!< Size of the S7P packet header: 12+2+2+4+4
#define SIZEIB_PKT_HAD 60 	//!< Size of the S7P packet with Header and data
#define SIZEIB_PKT_DATA 1464
#define MAC_TYPE_S7P 0xf007	//!< Ethernet type used for S7P
#define VFIRMWARE 	1		//!< Version of the firmware
#define VS7PROTO	1		//!< Version of the SevenSols Protocol.


//====================================================
// Structure of S7P packet.
/**
 * @brief The header of the S7P packet.
 */
typedef struct
{
	//standard mac header
	unsigned char mac_dst[6];
	unsigned char mac_src[6];
	unsigned short type;
	unsigned short zeros;	//!< This is added to align on 32-bits.
	//Smart 7 protocol
	unsigned char cmd;			//!< Command to execute
	unsigned char camID;		//!< Camera ID.
	unsigned short fpID;		//!< Frame or Parameters ID.
	unsigned int val;			//!< Value of the parameters or value of the offset.
	unsigned char data[200];		//!< Set up data in memory.
} s7p_pkt_t;



//====================================================
//  Structure for parameters

/**
 * @brief Specific parameters for the camera
 * @note The parameters need to be defined exactly in the
 * same order than the enumarated parameters S7P_PRM_CAM_xxx.
 */
typedef struct {

	//Parameters for the processor
	u32 mem_start; 		//!< Beginning of the camera memory to send.
	u32 mem_size;		//!< Size of the camera memory to send.
	u16 frame_id;		//!< The id of the actual frame.
	u16 zeros;			//!< To align on 32 bits.


	//Parameters for the FPGA memory.
	u32 is_enable;		//!< Set the camera enable or not.
	u32 im_resolution;	//!< The resolution of the image.
	u32 quality;		//!< JPEG quality.
	u32 bg_refresh;		//!< Refresh of the background
	u32 bg_thrs;		//!< Threshold for the background.
	u32 cc_minarea;		//!< Minimum area that a connex component mush have.
	u32 prm_debug;		//!< Parameters used temporary to debug.

} CamParams;


typedef struct {

	Xuint8 mac_src[6];		//!< Ethernet Hardware Address (48 bits)
	u16 zeros;				//!< align to 32-bits.


	u32 ip_src;		//!< The source IP address of the Board (32 bits)
	u32 port_src;	//!< The source port of the Board (Listening)
	u32 dhcp_on;	//!< Set if we want the IP to be automatically set.
	u32 modes;		//!< Modes (RTP, SEQ, FRATE)
	u32 dtype;		//!< Represent the data type. (DEPTHCODE | NOF_CHANNEL | COLSPACE )
	u32 win_size;	//!< The windows size of ACK	(Only with mode=SEQ)
	u32 sfrate;		//!< Sending frame rate (Only with mode={RTP,FRATE}).
	u32 rtp_port;	//!< Port used by RTP to send to the Server. @br

	u32 tmp;


	CamParams ar_cam[NOF_CAMERA];	//!< Array of camera.

} Parameters;




//====================================================
//  Prototype of C function

void start_S7Protocol(Parameters *params);
int param_GetSet(s7p_pkt_t *mypkt, s7p_pkt_t *xilpkt, Parameters *params);
void init_CamParams(CamParams *cam, u32 mem_start, u32 mem_size, u16 width, u16 height);

#endif //_S7P_NET_H_
