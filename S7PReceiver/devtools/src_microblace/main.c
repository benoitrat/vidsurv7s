/**
 * @file
 * @brief Main loop of the microblace.
 * @date 19-feb-2009
 * @author neub
 * @see @ref page_s7p
 * 
 * 
 * This file represents the main loop of the microblace which is combined with 
 * S7P_Receiver software to receive images:
 * 
 * To work it needs, the following files:
 * 
 * 	- s7p_net{.h,.c} 	: Files which contains the S7P Protocol and how to send to the images.
 * 	- xemacextend{.h,.c}	: Files which contains the extended function for sending with MAC interfaces. 
 * 	- s7p_hdr.h		: The definitions of the S7P header in a packet. THIS FILE MUST BE SYNCRHONIZED with S7P_Receiver!
 * 
 * In order to configure the data to send we need to configure the values in the Parameters register. 
 * This is performed by the function init_Parameters(), so we just need to change the parameters inside.
 * 
 * Then to start the S7P protocol we call the function start_S7Protocol(pParams);
 * 
 * 
 */

#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "s7p_net.h"


/**
 * @brief Initiate the parameters we can change
 */
void init_Parameters(Parameters *params) {

	u8 i;
	u16 width, height;
	u32 mem_size, mem_start;

	//Definition of main characteristic of each camera.
	mem_start=XPAR_SRAM_0_512KX32_MEM0_BASEADDR;
	width=360;
	height=288;
	mem_size=width*height*2;

	//User should only configure the following parameters.
	params->mac_src[5] = 0x00; //=0xCE; //Change the last byte of MAC 00:00:5E:00:FA:CE.

	params->ip_src= ((192 << 24) & (168 << 16) & (7 << 8) & 254);
	params->port_src=18000;
	params->dhcp_on=FALSE;
	params->modes=0;
	params->dtype=S7P_DTYPE_YUV422;
	params->win_size=6;	//Number of ACK frame pending before stopping the sending.
	params->sfrate=80;	//Sending frame rate in Mbps
	params->rtp_port=50000;	//Default RTP port.
	
	
	//DON'T MODIFY AFTER THIS!
	params->mac_src[0]=0x00; params->mac_src[1]=0x00; params->mac_src[2]=0x5E;
	params->mac_src[3]=0x00; params->mac_src[4]=0xFA;

	//Automatically set the camera memory.
	for(i=0;i<NOF_CAMERA;i++) {
		init_CamParams((CamParams*)&(params->ar_cam[i]),mem_start+mem_size*i,mem_size,width,height);
	}
}

int main (void) {

	
	//=========================================================================================================
	
	int i;
	Parameters params;
	Parameters *pParams=(Parameters *)&params;

	//=========================================================================================================

	init_Parameters(pParams);

   	//=========================================================================================================

	start_S7Protocol(pParams);

	//=========================================================================================================

}

