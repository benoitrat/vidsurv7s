/**
 *  @file
 *  @brief Contains the struct ip_packet
 *  @deprecated This class should not work correctly.
 *  @date 31-oct-2008
 *  @author Benoit RAT
 */


#ifndef IP_PACKET_H_
#define IP_PACKET_H_

#include "typedefs.h"
struct mac_packet;	//Should not be used


typedef unsigned char u_char;
typedef unsigned int u_int;
typedef unsigned short u_short;


/**
 * @brief The structure which represent the IP header.
 * @deprecated This structure should not work correctly.
 *
 *@code
 * ||----------------------------------------------------------------------------------------------------------||
 * || 00 01 02 03 04 05 06 07 || 08 09 10 11 12 13 14 15 || 16 17 18 19 20 21 22 23 || 24 25 26 27 28 29 30 31 ||
 * ||-------------------------||-------------------------||-------------------------||-------------------------||
 * ||  version   |    IHL     ||          TOS            ||                   Total Length                     ||
 * ||-------------------------||-------------------------||-------------------------||-------------------------||
 * ||                   Identification                   ||  Flags  |        Fragment offset                   ||
 * ||-------------------------||-------------------------||-------------------------||-------------------------||
 * ||           TTL           ||         Protocol        ||                  Header Checksum                   ||
 * ||----------------------------------------------------------------------------------------------------------||
 * ||                                             Source IP Protocol                                           ||
 * ||----------------------------------------------------------------------------------------------------------||
 * ||                                          Destination IP Protocol                                         ||
 * ||----------------------------------------------------------------------------------------------------------||
 * ||                                                   Data                                                   ||
 * ||                                                   ....                                                   ||
 * ||                                                   ....                                                   ||
 * ||----------------------------------------------------------------------------------------------------------||

 * @endcode
 *
 * @see http://www.tcpipguide.com/free/t_IPDatagramGeneralFormat.htm
 * @see http://www.networksorcery.com/enp/protocol/ip.htm
 *
 *
 */
typedef struct {

	/**
	 * @brief IP Version.
	 * Specifies the format of the IP packet header.
	 * @warning On 4-bits. Combined with IHL -> (version << 4 | IHL ).
	 * @note Constant value for all S400 boards: version=4 (IPv4).
	 */
	u_char version;

	/**
	 * @brief The Internet Header Length in 32 bit words.
	 * The normal value of this field when no options are used is 5 (5 32-bit words = 5*4 = 20 bytes).
	 * Contrast to the longer Total Length field below.
	 * @warning On 4-bits. Combined with version -> (version << 4| IHL ).
	 * @note Constant value for all S400 boards: IHL=5.
	 */
	u_char IHL;

	/**
	 * @brief Type of Service.
	 * Specifies the parameters for the type of service requested.
	 * The parameters may be utilized by networks to define the handling of the datagram during transport.
	 * This field is used as follow: IP Precedence (IPP on 3bits) | Unused. A good value for
	 * live video surveillance is 4. Therefore the suggested TOS value is: 100|00000
	 * @see Slides 15-18 on http://www.isc365.com/webinar/Traffic_Engineering.aspx
	 * @note Constant value for all S400 boards: TOS=0x80.
	 */
	u_char TOS;

	/**
	 *  @brief Specifies the total length of the IP datagram, in bytes.
	 *  Since this field is 16 bits wide, the maximum length of an IP datagram
	 *  is 65,535 bytes, though most are much smaller.
	 *
	 *  When an image data is sent we use as length => SIZE_HEAD_IP + (SIZE_HEAD_UDP + SIZE_DATA_UDP).
	 *  @note This value is not constant, and the checksum may be compiled when the length has changed.
	 *
	 */
	u_short length;

	/**
	 * @brief Used to identify the fragments of one datagram from those of another.
	 *
	 * This number correspond to the ID of a packet in a sequence. It is therefore
	 * incremented when a packet is sent but also when a packet is received.
	 *
	 * @warning This value change for each datagram send.
	 *
	 */
	u_short id;

	/**
	 * @brief Fragmentation flags and offset.
	 *
	 * - The fragmentation flag is on 3 bits:
	 * The 3 bits correspond to :
	 * | 0 | Don't Fragment | More Fragment |
	 * As we should not use fragmentation we can set these flags with the constant value of:<tt>010</tt>
	 *
	 * -The fragmentation offset:
	 * As we don't use the offset we should set all the other bits to 0.
	 *
	 * @note Constant value for all S400 boards: frags=0x4000.
	 */
	u_short frag;


	/**
	 * @brief Time to live of a packet datagram.
	 * The TTL field is set by the sender of the datagram, and reduced by every host on the route to its destination.
	 * If the TTL field reaches zero before the datagram arrives at its destination, then the datagram is discarded.
	 *
	 * It seems that a TTL of 60 is a god compromise since the packet use in the worst case 40 host to arrive
	 * to destination (See http://secfr.nerim.net/docs/fingerprint/en/ttl_default.html). For real video streaming
	 * the TTL should be inferior to 40 to avoid big latency.
	 * @note Constant value for all S400 boards: TTL = 60.
	 */
	u_char TTL;

	/**
	 * @brief This field specifies the next encapsulated protocol.
	 *
	 * The most popular protocol are: ICMP=1; TCP=6; UDP=17;
	 *
	 * @note Constant value for all S400 boards: protocol = 17 (UDP).
	 */
	u_char protocol;

	/**
	 * @brief Header checksum: A 16 bit one's complement checksum of the IP header and IP options.
	 *
	 * The checksum value need to be recomputed if a field change such as the length, src_ipaddr or dst_ipaddr.
	 *
	 * @see ip_hdr_t_ComputeCheckSum() functions to understand how the CheckSum is computed.
	 *
	 *
	 */
	u_short hchecksum;

	/**
	 * IP address of the sender.
	 */
	u_int src_ipaddr;

	/**
	 * IP address of the intended receiver.
	 */
	u_int dst_ipaddr;

	/**
	 * A pointer on the data in bytes.
	 */
	u_char *data; // SIZE_DATA_UDP + SIZE_HEAD_UDP

} ip_hdr_t;


ip_hdr_t IpHdr_Init(mac_packet *mac_packet,u_char *ip_src,u_char *ip_dst);
void IpHdr_InitHeaderForS400(ip_hdr_t *ip_packet);
void IpHdr_ToData(ip_hdr_t *ip_packet);
void IpHdr_FromData(ip_hdr_t *ip_packet);
void IpHdr_ComputeCheckSum(ip_hdr_t *ip_packet);
u_short IpHdr_FastCheckSum(u_short header_length, u_char *buff);
u_int IpHdr_GetU32Addr(u_char *ar_ipu8);




#endif /* IP_PACKET_H_ */
