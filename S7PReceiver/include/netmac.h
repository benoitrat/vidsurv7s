/**
 *  @file
 *  @brief Contains the class netmac.h
 *  @date 12-ene-2009
 *  @author neub
 */

#ifndef NETMAC_H_
#define NETMAC_H_

#define MAC_NOFEXA 6

#include <stdio.h>
#include <stdlib.h>
#include <pcap.h>

#ifndef PCAP_OPENFLAG_PROMISCUOUS
#define PCAP_OPENFLAG_PROMISCUOUS 1
#endif

#ifdef WIN32
#include <windows.h>
#include <iphlpapi.h>
#else
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#endif

#include "globals.h"
#include "s7p_packet.h"




typedef struct ifhard_s_ {
	char errbuf[PCAP_ERRBUF_SIZE];
	struct pcap_pkthdr *header;
	struct bpf_program fp;      /* hold compiled program     */
	pcap_t *adhandle;
	int timeout;	//If 0 we are in blocking mode.


}  ifhard_s;




//Interface Pcap function.
int NetMAC_openInterface(const char *ifname, ifhard_s *ifhard);
int NetMAC_closeInterface(ifhard_s *ifhard);
int NetMAC_setFilter(const char *filter, ifhard_s *ifhard);
int NetMAC_sndPacket(s7p_pkt_t *pkt, ifhard_s *ifhard, int size_inB);
int NetMAC_rcvPacket(s7p_pkt_t *pkt, ifhard_s *ifhard, bool filter);

//Manipulation of the interface name and device
int NetMAC_FindIfname(char *ifname);
int NetMAC_Ifname2EHA(const char *ifname, u_char *eha);
int NetMAC_EHA2Ifname(const u_char *eha,char *ifname);

//EHA functions.
void SetEHAFromHexaStr(const char *strHexa,u_char *eha);
int CompareEHA(const u_char *eha1, const u_char *eha2);
void CopyEHA(const u_char* src, u_char *dst);
void PrintEHA(const u_char *eha);
int hexa2i(char hexa);


#endif /* NETMAC_H_ */
