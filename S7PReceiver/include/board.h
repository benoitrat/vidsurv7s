/**
 *  @file
 *  @brief Contains the structure Board and Camera.
 *  @date 09-ene-2009
 *  @author neub
 *
 *  @page page_board The configuration of the board.
 */

#ifndef BOARD_H_
#define BOARD_H_

#define MAX_NOF_CAM 4

#include <stdio.h>
#include <cv.h>
#include "typedefs.h"

typedef struct {
	unsigned int value:24;		//! Value or ((Value_1 << 12) | Value_2)
	unsigned int auto_mode:1;	//!< Set the mode
	unsigned int on_off:1;		//!< On off this feature.
	unsigned int one_push:1;	//!< Write '1': begin to work, Read: '1' in operation.
	unsigned int reserved:3;	//!< Reserved
	unsigned int abs_ctrl:1;	//!< Absolute value control
	unsigned int presence:1;	//!< Presence of this features.
} fw_ctrl_t;



typedef struct _Camera {
	IplImage* rgb;	//!< The image to display
	FILE *fs;		//!< The file where the byte need to be written.
	IplImage* tmp;	//!< The image where the packet are received. (Can be see as a simple buffer)
	char name[255];
	int is_enable;
	int frame_num;		//!< The frame number
	u16 seq_Id;			//!< The sequence id of the frame received
	int pkt_cnt;		//!< Count the number of packet.

	/* Parameters from the IIDC standard (Firewire) */
	fw_ctrl_t white_bal;	//!< White balance (Value B/U | Value V/R)
	fw_ctrl_t brightness;	//!< Brightness	(Value)
	fw_ctrl_t hue;			//!< Hue (Value)

} Camera;


typedef struct _Board {
	unsigned char eha[6];	//!< The ethernet hardware address
	unsigned char ip[4];	//!< The IP address
	char name[255];			//!< The name of the Board
	char tpl_fname[255];	//!< Template for the filename.
	int nof_cam;			//!< The number of cam connected to the same board.
	int win_size;			//!< The size of the windows.
	int cam_id;				//!< The camera ID.
	Camera *cam;			//!< A pointer on a vector of camera.
} Board;


#endif /* S400_H_ */
