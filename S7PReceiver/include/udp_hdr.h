/**
 *  @file
 *  @brief Contains the class udp_header
 *  @date 03-nov-2008
 *  @author Benoit RAT
 */


#ifndef UDP_HEADER_H_
#define UDP_HEADER_H_


/**
 * @brief The structure of the UDP header.
 *
 *@code
 * ||----------------------------------------------------------------------------------------------------------||
 * || 00 01 02 03 04 05 06 07 || 08 09 10 11 12 13 14 15 || 16 17 18 19 20 21 22 23 || 24 25 26 27 28 29 30 31 ||
 * ||-------------------------||-------------------------||-------------------------||-------------------------||
 * ||                    Source Port                     ||                Destination Port                    ||
 * ||-------------------------||-------------------------||-------------------------||-------------------------||
 * ||           	        Length                       ||                    Data Checksum                   ||
 * ||----------------------------------------------------------------------------------------------------------||
 * ||                                                   Data                                                   ||
 * ||                                                   ....                                                   ||
 * ||                                                   ....                                                   ||
 * ||----------------------------------------------------------------------------------------------------------||

 * @endcode
 *
 * @see http://www.networksorcery.com/enp/protocol/udp.htm
 *
 */
typedef struct {

	/**
	 * @brief The port number of the sender.
	 *
	 * Cleared to zero if not used.
	 */
	u_short src_port;

	/**
	 * @brief The port this packet is addressed to.
	 */
	u_short dst_port;

	/**
	 * @brief The length in bytes of the UDP header and the encapsulated data.
	 *
	 * @note The minimum value for this field is 8.
	 *
	 */
	u_short length;

	/**
	 * @brief computed as the 16-bit one's complement of the one's complement sum of
	 * a pseudo header of information from the IP header, the UDP header, and the data,
	 *  padded as needed with zero bytes at the end to make a multiple of two bytes.
	 *  If the checksum is cleared to zero, then checksuming is disabled. If the computed checksum
	 *  is zero, then this field must be set to 0xFFFF.
	 */
	u_short checksum;

} udp_hdr_t;


#endif /* UDP_HEADER_H_ */
