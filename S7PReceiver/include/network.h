/**
 *  @file
 *  @brief Contains the class network.h
 *  @date 09-ene-2009
 *  @author neub
 *  @page page_network The configuration of the network connection.
 *
 *  @section page_network_xmlnode The XML node.
 *
 *  The parameters are set from in a XML file by the node @tt{network}. Here you have a sample of how configurating the parameters for processsing.
 *
 *
@code
<network>
	<mode>0</mode>
	<MAC>"00-80-5A-69-3E-8F"</MAC>
	<IP>192 168 7 1</IP>
</network>
@endcode

	If the mode is set to:
	- to 0 we will use MAC layer with winpcap library
	- to 1 we will use UDP layer with the system socket.
	- to 2 we will use RTP/JPEG layer (This software can't read for the moment the RTP packets.)
 *
 *  @see @ref page_config
 *
 */

#ifndef NETWORK_H_
#define NETWORK_H_

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>

#include "s7p_packet.h"
#include "globals.h"
#include "netmac.h"

/**
 * @brief The mode of communication with the boards.
 */
enum {
	NET_MODE_MAC=0, 	//!< Mode 0
	NET_MODE_UDP,		//!< Mode 1
};


/**
 * @brief Structure to handle UDP socket.
 * @note This structure is empty but should be put in the netudp.h
 */
typedef struct {

} udpsock_t;

/**
 * @brief Structure that manage different type of socket.
 */
typedef struct {
	int mode;		//!< The mode use determined the type of socket used.
	ifhard_s *mac;	//!< A pointer on the ethernet interface socket.
	udpsock_t *udp;	//!< A pointer on a typical UDP socket.

} netsocket_t;



//Standard function
netsocket_t* bindSocket(Params *params);
void closeSocket(netsocket_t* socket);
int sndPacket(s7p_pkt_t *pkt, netsocket_t *socket, int size_inB);
int rcvPacket(s7p_pkt_t *pkt, netsocket_t *socket, bool filter);

void S7P_close(s7p_pkt_t *mypkt,netsocket_t* socket);
int S7P_setup(Params *params,netsocket_t* socket);
int S7P_pingBoard(Params *params, netsocket_t* socket, int nof_tries);
int S7P_GetSetParam(netsocket_t *socket, s7p_pkt_t *mypkt, s7p_pkt_t *xilpkt, u32 *p_val, int max_tries);


//Start the simple sevensolution protocol
int runS7Protocol(Params *params, netsocket_t* socket);

//Function to manipulate the s7p packet
s7p_pkt_t* S7PPkt_init(Params *params, bool to_send);
void S7PPkt_release(s7p_pkt_t* pkt);
void S7PPkt_setHeaders(s7p_pkt_t* pkt);
void S7PPkt_setBuffer(s7p_pkt_t* pkt);
void S7PPkt_Show(s7p_pkt_t* pkt);
void S7P_SetParamName(u32 paramID,char *buff);

void swapEndian_u32(u32 *out, const u32 *in);
void swapEndian_u16(u16 *out, const u16 *in);
void swapEndian_u32buffer(u8* out, const u8* in, int size_inB);

void wait ( int seconds );


#endif /* NETWORK_H_ */
