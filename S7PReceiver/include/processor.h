/**
 *  @file
 *  @brief Contains the class processor.h
 *  @date 13-ene-2009
 *  @author neub
 *
 * @page page_processor Processing the received packets.
 *
 *  @section page_processor_xmlnode The XML configuration of the processor.
 *
 * The parameters are set from in a XML file by the node "processor".
 * Here you have a sample of how configurating the parameters for processsing.
 *
 @code
<processor>
	<dtype>0</dtype>
	<out_dir>.</out_dir>
	<out_fname>frame_%04d.jpg</out_fname>
	<show_im>0</show_im>
	<is_towrite>1</is_towrite>
</processor>
@endcode

 * @see @ref page_config
   @br@br@br
   @section page_processor_fields The Data fields meaning.

 * @subsection page_processor_fields_dtype Datatype of the packet
 *
 *	The datatype represents how the value are organized in the memory on the Board. The different value can be find in
 * @ref S7P_DTYPE_xxx.
 *
 * 	If the datatype is @ref S7P_DTYPE_BIN or @ref S7P_DTYPE_JPEG the field:
 * 		- ProcParams::show_im is set automatically to @FALSE.
 * 		- ProcParams::is_towrite is set to @TRUE.
 *
 * @see ProcParams::dtype
 *
 * @sepline
 * @subsection page_processor_fields_outdir Output Directory
 *
 * If we use only one camera the output directory can be the running directory @tt{"./"},*
 * otherwise the output directory should contains a name format with a @tt{"%d"} printf tag.
 * Such that we could create a separate folder for each camera.
 *
 * @warning This function should be improve to handle the video files also.
 * @see ProcParams::out_dir
 *
 * @sepline
 * @subsection page_processor_fields_outfname Output Filename
 *
 * The output filename @b MUST have a printf integer tag to create a new file for each new frames received.
 * @code
 * 	frame_%d.jpg	-->	frame_1.jpg,    ... , frame_22.jpg,   ... , frame_333.jpg,  ... , frame_4444.jpg
 * 	frame_%02d.jpg	-->	frame_01.jpg,   ... , frame_22.jpg,   ... , frame_333.jpg,  ... , frame_4444.jpg
 * 	frame_%04d.jpg	-->	frame_0001.jpg, ... , frame_0022.jpg, ... , frame_0333.jpg, ... , frame_4444.jpg
 * @endcode
 *
 * Moreover is extension determine the output format.
 *
 * @note
 * 		-	If the Datatype is S7P_DTYPE_BIN the output filename will be extension is set as @tt{.bin}.
 * 		-	If the Datatype is S7P_DTYPE_JPEG the output filename must be a @tt{.jpeg}.
 * 		-	Otherwise, the extension determine the output image format like in OpenCV  @tt{(.bmp\,.jpeg\,.jpg\,.png\,.pbm\,.ppm\,.tiff\,.tif)}
 *
 * @see ProcParams::out_fname
 *
 * @sepline
 * @subsection page_processor_fields_others Others fields
 *    The meaning of the other fields can be find in @ref ProcParams.
 *
*/
#ifndef PROCESSOR_H_
#define PROCESSOR_H_

#define CV_YUV4222RGB 0


#include <cv.h>
#include <highgui.h>
#include <string.h>
#include "globals.h"
#include "board.h"
#include "s7p_packet.h"

int Proc_SetupOutput(Params *param);

int Proc_FramePkt(s7p_pkt_t *pkt, Params *param);
int Proc_Data2Image(const u8 *buffer, u32 dsize, u32 offset, u32 dtype, IplImage *im);
int Proc_FrameSetup(s7p_pkt_t *pkt,Params *param);
int Proc_FrameLast(s7p_pkt_t *pkt,Params *param);

bool Proc_ImgConvertTmpToBGR(Camera *cam, int datatype);
bool Proc_SetFileName(char *fname, Params *param);

int Proc_DumpStream(s7p_pkt_t *pkt,Params *param);
#endif /* PROCESSOR_H_ */
