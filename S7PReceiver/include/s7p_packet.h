/**
 *  @file
 *  @brief Contains the class s7p_packet.h
 *  @date 09-ene-2009
 *  @author neub
 */

#ifndef S7P_PACKET_H_
#define S7P_PACKET_H_

#include "mac_hdr.h"
#include "s7p_hdr.h"


typedef s7p_hdr_le_t s7p_hdr_t;
typedef int ip_hdr_s;	//!< Temporary until created the correct ip header structure.
typedef int udp_hdr_s;	//!< Temporary until created the correct udp header structure.


#define SIZEIB_MTU	1500	//!< Maximum size of the packet

#define SIZEIB_HDR_MAC  14 	//!< Size of two EHA (2* 6 bytes) + types (2 bytes): 14 bytes
#define SIZEIB_HDR_ZMAC	16	//!< Size of MAC header with two zeros to align on 32-bits.
#define SIZEIB_HDR_IP  	20	//!< Size of IP header in bytes including IP header (12 bytes) + src (4 bytes) + dst (4 bytes).
#define SIZEIB_HDR_UDP 	 8 	//!< Size of UDP header in bytes: 2*port (2 bytes) + length (2 bytes) + checksum (2 bytes).
#define SIZEIB_HDR_S7P 	 8	//!< Size of S7P header in bytes. cmd + cmdID (2 bytes) + size (2 bytes) + val (2 bytes)

/**
 * @brief Special structure which represent the packet received.
 *
 * Depending on the  s7p_pkt_t::type some pointer are set to @NULL or not.
 *	- 	Before transmission we must use the method S7PPkt_setBuffer() to fill the buffer to send.
 * 	-	After reception we must use the method S7PPkt_setHeaders() to fill the headers.
 *
 * @see S7PPkt_init(), S7PPkt_release().
 */
typedef struct {
	unsigned char buff[SIZEIB_MTU];	//!<Buffer where we copy all the data.
	int type;	//!< If the Packet is MAC/S7P or UDP/S7P
	int dsize;	//!< Size of the data payload in byte (Excluding all headers)
	mac_hdr_t* mac_hdr;	//!< Pointer on MAC header
	ip_hdr_s*  ip_hdr;	//!< Pointer on the IP header.
	udp_hdr_s* udp_hdr;	//!< Pointer on the UDP header.
	s7p_hdr_t* s7p_hdr;	//!< Pointer on the S7P header.
	unsigned char *data;	//!< Point inside the  s7p_pkt_t::buff where the data really start.

} s7p_pkt_t;

#endif /* S7P_PACKET_H_ */
