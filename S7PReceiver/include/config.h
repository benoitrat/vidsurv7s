/**
 *  @file
 *  @brief Contains the class config.h
 *  @date 09-ene-2009
 *  @author neub
 *  @see @ref page_config
 *
 *  @page page_config Read XML configuration.
 *
 *	The Xml configuration files looks like this.
 *
@code
	<?xml version="1.0"?>
	<opencv_storage>
		<network>...</network>
		<board>...</board>
		<processor>...</processor>
	</opencv_storage>
@endcode
 *
 *	  For more information on what you should set inside @tt{network\,board\,processor} look at the following page:
 *	  - @ref page_network.
 *	  -	@ref page_board.
 *	  - @ref page_processor_xmlnode.
 *
 *
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include <cv.h>
#include <stdio.h>
#include "typedefs.h"
#include "board.h"
#include "globals.h"

void readConfigFromXML(const char *fname, Params *params);
void readIPaddrfromXML(CvFileNode *fnode, u_char *ip_addr);
void printf_aru8(const char *format,const u_char *buff, int length,const char* in_fmt);

#endif /* CONFIG_H_ */
