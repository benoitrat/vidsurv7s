/**
 *  @file
 *  @brief Contains the structure of the MAC header.
 *  @date 09-ene-2009
 *  @author Benoit RAT
 */

#ifndef MAC_HDR_H_
#define MAC_HDR_H_


#define MAC_TYPE_S7P 0xF007	//!< The value of S7P protocol (on MAC layer).
#define MAC_TYPE_IP 0x0800	//!< The value of IP protocol.

/**
 * @brief Structure of the MAC header
 *
 *
 * This header normally used 14 bits, but we used two
 * zero bytes to aligned it on 32-bits.
 *
 * @code
 * -------------------------------------------
 * || 00...07 | 08...15 | 16...23 | 24...32 ||
 * -------------------------------------------
 * ||              eha_dst[0-3]             ||
 * -------------------------------------------
 * ||  eha_dst[4-5]     |     eha_src[0-1]  ||
 * -------------------------------------------
 * ||              eha_src[2-5]             ||
 * -------------------------------------------
 * ||       type        |        zeros      ||
 * -------------------------------------------
 * @endcode
 *
 * @see For more information see http://www.networksorcery.com/enp/default1002.htm
 */
typedef struct {
	unsigned char eha_dst[6];	//!< Ethernet Hardware Adress for destination.
	unsigned char eha_src[6];	//!< Ethernet Hardware Adress for source.
	unsigned short type;		//!< Type of ethernet protocol (@ref MAC_TYPE_S7P)
	unsigned short zeros; 		//!< Add zeros to align on 32-bits.
} mac_hdr_t;


#endif
