/**
 *  @file
 *  @brief Contains the all the globals structure
 *  @date 09-ene-2009
 *  @author neub
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include "board.h"

enum {
	OUTPUT_TYPE_BIN=0,
	OUTPUT_TYPE_BINJPG,
	OUTPUT_TYPE_RAW,
	OUTPUT_TYPE_CVIMG,
	OUTPUT_TYPE_VIDEO,
};


typedef struct _NetParams {
	int mode;
	int nof_consec_frames;
	unsigned char eha[6];
	unsigned char ip[4];
} NetParams;

/**
 * @brief Parameters for the Processing of the packet.
 *
 *
 *
 */
typedef struct {
	int dtype;				//!< Look at @ref page_processor_fields_dtype
	char out_dir[155];		//!< Look at @ref page_processor_fields_outdir
	char out_fname[100];	//!< Look at @ref page_processor_fields_outfname
	bool show_im;			//!< Set to @TRUE if we want to output the image
	bool is_towrite;		//!< Set to @TRUE if we want to write the output files.
	int video_fps;
	int nof_frames_consec;
	int out_type;			//!< This option is guessed by the out_fname.
} ProcParams;


typedef struct _Params {
	NetParams net;
	ProcParams processor;
	Board board;
} Params;


#endif /* GLOBALS_H_ */
