/**
 *  @file
 *  @brief Contains the class typedefs
 *  @date 05-nov-2008
 *  @author Benoit RAT
 */


#ifndef TYPEDEFS_HPP_
#define TYPEDEFS_HPP_



#ifndef _MYTYPETAGS_DEFINED
	#define MYTYPETAG_UNKOWN	 0
	#define MYTYPETAG_U8 		 1
	#define MYTYPETAG_U16 		 2
	#define MYTYPETAG_U32 		 3
	#define MYTYPETAG_CHAR 		 4
	#define MYTYPETAG_SHORT 	 5
	#define MYTYPETAG_INT 		 6
	#define MYTYPETAG_LONG 		 7
	#define MYTYPETAG_BOOL		 8
	#define MYTYPETAG_FLOAT		10
	#define MYTYPETAG_DOUBLE 	11
	#define MYTYPETAG_STRING 	20
	#define MYTYPETAG_WXSTRING 	21
	#define MYTYPETAG_XMLNODE 	22
#define _MYTYPETAGS_DEFINED
#endif

#define TRUE 1	//!< Value which correspond to @true.
#define FALSE 0	//!< Value which correspond to @false.

typedef unsigned char bool;
typedef unsigned char  u8;
typedef unsigned short u16;
typedef unsigned int   u32;


#ifdef WIN32

#if !defined ( _BSDTYPES_DEFINED )
/* also defined in gmon.h and in cygwin's sys/types */
typedef unsigned char	u_char;
typedef unsigned short	u_short;
typedef unsigned int	u_int;
typedef unsigned long	u_long;
#define _BSDTYPES_DEFINED
#endif /* ! def _BSDTYPES_DEFINED  */

#else

#include "sys/types.h"

#endif


#endif /* TYPEDEFS_HPP_ */
