/**
 *  @file
 *  @brief Contains the functions that load the parameters.
 *  @date 09-ene-2009
 *  @author neub
 */
#include "config.h"
#include "network.h"
#include "netmac.h"



CvFileStorage *fsXML = NULL;
void readBoardFromXML(CvFileNode *fnode, Board *board);
void readNetFromXML(CvFileNode *fnode, NetParams *pcnet);
void readProcFromXML(CvFileNode *fnode, ProcParams *proc);
void readCamFromXML(CvFileNode *fnode, Camera *cam);
void printf_aru8(const char *format,const u_char *buff, int length,const char* in_fmt);


/**
 * @brief Read the "fname" XML file and set the values in Params.
 *
 * @see @ref page_config.
 */
void readConfigFromXML(const char *fname, Params *params) {
	CvFileNode *fnode;

	fsXML = cvOpenFileStorage(fname, NULL, CV_STORAGE_READ);
	if(!fsXML) {
		fprintf(stderr,"Config file: %s not found\n",fname);
		exit(EXIT_FAILURE);
	}
	else {
		printf("%s found:\n",fname);
	}

	fnode = cvGetFileNodeByName(fsXML,NULL,"network");
	if(!fnode) fprintf(stderr,"Node <network> not found\n");
	readNetFromXML(fnode,&params->net);

	fnode = cvGetFileNodeByName(fsXML,NULL,"processor");
	if(!fnode) fprintf(stderr,"Node <processor> not found\n");
	readProcFromXML(fnode,&params->processor);

	fnode = cvGetFileNodeByName(fsXML,NULL,"board");
	if(!fnode) fprintf(stderr,"Node <board> not found\n");
	readBoardFromXML(fnode,&params->board);

	printf("\n");
	fflush(stdout);
}

void readBoardFromXML(CvFileNode *fnode, Board *board) {
	const char* strEHA;
	CvFileNode *subfnode;
	int c;

	//Set the ip address
	subfnode = cvGetFileNodeByName(fsXML,fnode,"IP");
	readIPaddrfromXML(subfnode,board->ip);

	//Set the EHA (MAC address)
	strEHA = (char*)cvReadStringByName(fsXML,fnode,"MAC","FF:FF:FF:FF:FF:FF");
	SetEHAFromHexaStr(strEHA,board->eha);

	//Set the windows size.
	board->win_size=cvReadIntByName(fsXML,fnode,"win_size",6);


	//Add n camera to the board...
	board->nof_cam=cvReadIntByName(fsXML,fnode,"nof_cam",1);
	if(board->nof_cam > MAX_NOF_CAM) {
		printf("Maximum number of camera is 10");
		exit(EXIT_FAILURE);
	}


	board->cam = (Camera *)calloc(board->nof_cam,sizeof(Camera));

	//Set the images for each camera
	for(c=0;c<board->nof_cam;c++) {
		sprintf(board->cam[c].name,"cam_%d",c);
		subfnode = cvGetFileNodeByName(fsXML,fnode,board->cam[c].name);
		readCamFromXML(subfnode,&(board->cam[c]));
	}

	printf("\nBoard:\n");
	printf("\tnof_cam=%d\n",board->nof_cam);
	printf("\twin_size=%d\n",board->win_size);
	printf_aru8("\tMAC=%s\n",board->eha,6,"%02X:");
	printf_aru8("\tIP=%s\n",board->ip,4,"%d.");

}

/**
 * @brief Read the xml parameters for a camera.
 */
void readCamFromXML(CvFileNode *fnode, Camera *cam) {
	int tmp;

	cam->is_enable=FALSE;
	cam->fs=NULL;
	cam->tmp=NULL;
	cam->rgb=NULL;
	cam->frame_num=0;

	tmp = cvReadIntByName(fsXML,fnode,"white_bal",0xFFFFFFFF);
	memcpy(&cam->white_bal,&tmp,4);

	tmp= cvReadIntByName(fsXML,fnode,"brightness",0x60000FF);
	memcpy(&cam->brightness,&tmp,4);
}


void readNetFromXML(CvFileNode *fnode, NetParams *pcnet) {
	const char* strEHA;
	CvFileNode* fn_ip;
	int c;

	pcnet->mode=cvReadIntByName(fsXML,fnode,"mode",0);
	printf("\nNetwork:\n");
	printf("\tmode=%d\n",pcnet->mode);

	if(pcnet->mode == NET_MODE_UDP) {
		//Set the ip address
		fn_ip = cvGetFileNodeByName(fsXML,fnode,"IP");
		readIPaddrfromXML(fn_ip,pcnet->ip);
		printf_aru8("\tIP=%s\n",pcnet->ip,4,"%d.");
	}
	else {
		//Set the EHA (MAC address)
		strEHA = (char*)cvReadStringByName(fsXML,fnode,"MAC","FF:FF:FF:FF:FF:FF");
		SetEHAFromHexaStr(strEHA,pcnet->eha);
		printf("\tMAC="); PrintEHA(pcnet->eha); printf("\n");
	}
}

void readProcFromXML(CvFileNode *fnode, ProcParams *proc) {
	char* str;

	proc->dtype=cvReadIntByName(fsXML,fnode,"dtype",0);
	proc->show_im=cvReadIntByName(fsXML,fnode,"show_im",0);
	proc->is_towrite=cvReadIntByName(fsXML,fnode,"is_towrite",1);
	proc->video_fps=cvReadIntByName(fsXML,fnode,"video_fps",24);
	proc->nof_frames_consec=cvReadIntByName(fsXML,fnode,"nof_frames_consec",-1);
	str=(char*)cvReadStringByName(fsXML,fnode,"out_dir","./");
	sprintf(proc->out_dir,"%s",str);
	str=(char*)cvReadStringByName(fsXML,fnode,"out_fname","frame_%04d");
	sprintf(proc->out_fname,"%s",str);

	printf("\nProcessor:\n");
	printf("\tdtype=%d\n",proc->dtype);
	printf("\tout_dir=%s\n",proc->out_dir);
	printf("\tout_fname=%s\n",proc->out_fname);
	printf("\tis_towrite=%d\n",proc->is_towrite);
	printf("\tshow_im=%d\n",proc->show_im);
	printf("\tvideo_fps=%d\n",proc->video_fps);
}




void readIPaddrfromXML(CvFileNode *fnode, u_char *ip_addr) {
	int i;
	CvSeq *seq;
	if(fnode==NULL) return;

	seq=fnode->data.seq;
	if(seq==NULL) return;

	for(i=0;i<4;i++) {
		ip_addr[i] = cvReadInt((CvFileNode*)cvGetSeqElem(seq,i),0);
		//printf("%d.",ip_addr[i]);
	}
}

void printf_aru8(const char *format,const u_char *buff, int length,const char* in_fmt) {
	char ret[255];
	char tmp[50];
	int i;
	strcpy (ret,"");

	for(i=0;i<length;i++) {
		sprintf(tmp,in_fmt,buff[i]);	//Print the inside format
		strcat(ret,tmp);				//Concanate it to the real string
	}
	i = strlen(ret)-1; 		//Search the position of last character
	ret[i]='\0';			//Remove it
	printf(format,ret);		//Then format with the real string
}


