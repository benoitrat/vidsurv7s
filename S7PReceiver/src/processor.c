/**
 *  @file
 *  @brief Contains the functions that handle the reception of a packet and process it.
 *  @date 09-ene-2009
 *  @author neub
 */

#include "processor.h"
#define OPENCV_ESC_KEY 27
#define MAX_NOF_CHANNELS 4
#define R  0
#define G  1
#define B  2
#define Y  0
#define Cr 1
#define Cb 2

//! For signed variable.
#define CHECK_RANGE(v, min_in, max_out) \
	(min_in <= v && v < max_out)

//! For unsigned variable (>0)
#define CHECK_URANGE(v, max_out) \
	(v < max_out)


#define CV7_ARE_IMSIZEEQ(im1, im2) \
    ( ((im1)->height == (im2)->height) && ((im1)->width == (im2)->width) )

#define CV7_ARE_IMEQ(im1, im2) \
    ( ((im1)->height == (im2)->height) && ((im1)->width == (im2)->width) && ((im1)->nChannels == (im2)->nChannels) && ((im1)->depth == (im2)->depth))

#define CV7_ACHECK_IMG8U(im1, im2, nChan1, nChan2) \
    ((im1)->height == (im2)->height && (im1)->width == (im2)->width && (im1)->nChannels == nChan1 && (im2)->nChannels == nChan2 && (im1)->depth == 8 && (im2)->depth == 8)



//Prototype for YUV422
void TransformYUV4222RGB(const u8* im_yuv422, u8* im_rgb, int nof_pixels);
void TransformYUV4222BGR(const u8* im_yuv422, u8* im_bgr, int nof_pixels);
void YUV2pRGB(u8 y, u8 u, u8 v,u8* p_rgb);
void YUV2pBGR(u8 y, u8 u, u8 v,u8* p_bgr);
u8 clipByte_i32(int val);


//Prototype for RLE image.
void RLE_drawTupple(u8 id, u16 start, u16 end, u16 row, IplImage *out);
void RLE_drawBlobIm(const u8 *buff,int buff_size, IplImage *out);
void BWBin_drawPacket(u8 *memdst,const u8 *memsrc,int buff_size);


int count;
char fname[255];	//A temporary buffer to write the filename
CvVideoWriter *vidWriter;


//TODO: Check if output directory exist and how many cameras we have.
/**
 * @brief Create the output directory and the filename template.
 *
 */
int Proc_SetupOutput(Params *param) {
	char *ext;
	int dtype=param->processor.dtype;
	int ret=EXIT_SUCCESS;

	printf("Setup Output:\n");

	//Create the filename
	sprintf(param->board.tpl_fname,"%s/%s",param->processor.out_dir,param->processor.out_fname);

	//Obtain the extension of the filename.
	ext = strrchr(param->board.tpl_fname,'.');
	if(ext==NULL) {
		printf("Filename '%s' without extension! \n",param->board.tpl_fname);
		strcat(param->board.tpl_fname,".jpg");
		printf(" JPEG extension is set by default -> '%s'\n\n",param->board.tpl_fname);
		ext = strrchr(param->board.tpl_fname,'.');
	}

	//Find the output type according to the extension of the filename.

	if(strcmp(ext,".jpg")==0 && dtype == S7P_DTYPE_BIN) {
		param->processor.out_type=OUTPUT_TYPE_BINJPG;
	} else if(strcmp(ext,".bin")==0) {
		param->processor.out_type=OUTPUT_TYPE_BIN;
	} else if(strcmp(ext,".raw")==0) {
		param->processor.out_type=OUTPUT_TYPE_RAW;
	} else if(strcmp(ext,".avi")==0) {
		param->processor.out_type=OUTPUT_TYPE_VIDEO;
	} else {
		param->processor.out_type = OUTPUT_TYPE_CVIMG;
	}


	if(dtype==S7P_DTYPE_BIN || dtype==S7P_DTYPE_JPEG) {
		param->processor.show_im=0;
	}

	printf("\t- Show_image=%d\n",param->processor.show_im);
	printf("\t- Output type=%d (%s)\n",param->processor.out_type,ext);

	printf("\n");
	return ret;
}

/**
 * @brief Process a packet which is part of a frame.
 *
 * 		- 	When the first packet arrive (S7P_CMD_FRAME_SETUP):
 *  We setup an image buffer with the size of the image and the number of channel.
 * 		-	When a block packet arrives (S7P_CMD_FRAME_BLOCK):
 * We copy the payload data in the image buffer knowing the offset adress position.
 * 		-	Finally when the last packet arrives (S7P_CMD_FRAME_LAST):
 * We save the image in an output file or show the image (depends on the values in ProcParams)
 *
 */
int Proc_FramePkt(s7p_pkt_t *pkt, Params *param) {
	Camera *cam=&(param->board.cam[param->board.cam_id]);

	switch(param->processor.dtype) {
	case S7P_DTYPE_BIN:
	case S7P_DTYPE_JPEG:
	case S7P_DTYPE_RAW_RG:
	case S7P_DTYPE_RAW_GR:
	case S7P_DTYPE_RAW_BG:
	case S7P_DTYPE_RAW_GB:
	case S7P_DTYPE_GREY:
	case S7P_DTYPE_YUV422:
	case S7P_DTYPE_RGB:
	case S7P_DTYPE_BGR:
	case S7P_DTYPE_YCrCb:
	case S7P_DTYPE_YCbCr:
	case S7P_DTYPE_HSV:
	case S7P_DTYPE_HRL:
	case S7P_DTYPE_BWBIN:
		if(pkt->s7p_hdr->cmd==S7P_CMD_FRAME_SETUP) {
			return Proc_FrameSetup(pkt,param);
		}
		else if(pkt->s7p_hdr->cmd==S7P_CMD_FRAME_BLOCK) {
			return Proc_Data2Image(pkt->data, pkt->dsize, pkt->s7p_hdr->val,
					param->processor.dtype,cam->tmp);
		}
		else if(pkt->s7p_hdr->cmd==S7P_CMD_FRAME_LAST) {
			return Proc_FrameLast(pkt,param);
		}

	case S7P_DTYPE_STREAM:
		return Proc_DumpStream(pkt,param);

	default:
		fprintf(stderr,"Error: Datatype %X is unkown",param->processor.dtype);
		return EXIT_FAILURE;
	}
}

/**
 * @brief Process or copy a frame packet in the temporary image.
 */
int Proc_Data2Image(const u8 *buffer, u32 dsize, u32 offset, u32 dtype, IplImage *im) {

	//For the first frame the val is always 0.
	switch(dtype) {
	case S7P_DTYPE_HRL:
		RLE_drawBlobIm(buffer,dsize,im);	//Set the value in the image.
		break;
	case S7P_DTYPE_BWBIN:
		BWBin_drawPacket(im->imageDataOrigin+offset*8,buffer,dsize);
		break;
	default:
		memcpy(im->imageDataOrigin+offset,buffer,dsize); //Copy the packet value
		break;
	}
	return EXIT_SUCCESS;
}

/**
 * @brief Process the first Frame packet.
 *
 * In this function we set the size of the tmp image to copy all the frame packet inside.
 */
int Proc_FrameSetup(s7p_pkt_t *pkt,Params *param) {

	//Declare variable
	int width, height, nof_chan;
	s7p_hdr_t *hdr=pkt->s7p_hdr;
	Board *board = &(param->board);
	Camera *cam=&(board->cam[board->cam_id]);

	//Check if we execute the correct command.
	if(hdr->cmd != S7P_CMD_FRAME_SETUP) {
		fprintf(stderr,"Error: cmd:%0X != S7P_CMD_FRAME_SETUP",hdr->cmd);
		return EXIT_FAILURE;
	}


	//Get the number of channels
	nof_chan = (param->processor.dtype >> 8) & 0x0F;

	//In case we have a binary image.
	if(nof_chan == 0) {
		printf("data with zeros channels\n NOTHING DONE");
		return EXIT_FAILURE;
	}

	//In case we have an image (1,2,3 or 4 channels)
	else if(nof_chan <= MAX_NOF_CHANNELS) {
		width = hdr->val >> 16;
		height = (hdr->val & 0xFFFF);

		//Create an image
		if(cam->tmp == NULL) {
			printf("Create image (%d x %d x %d) for cam %s\n",width,height,nof_chan,cam->name);
			fflush(stdout);
			cam->tmp = cvCreateImage(cvSize(width,height),IPL_DEPTH_8U,nof_chan);
		}

		//If the size changed, resize it.
		if(cam->tmp->width!=width || cam->tmp->height!=height) {
			printf("Resize image (%d x %d x %d) for cam %s\n",width,height,nof_chan,cam->name);
			fflush(stdout);
			cvReleaseImage(&cam->tmp);
			cam->tmp = cvCreateImage(cvSize(width,height),IPL_DEPTH_8U,nof_chan);
		}

		//If HRL code we need to reset the image to zeros.
		if(param->processor.dtype==S7P_DTYPE_HRL) {
			cvSet(cam->tmp,cvScalar(0,0,0,0),NULL);
		}

		//Then process the data in the packet to the IPL image.
		Proc_Data2Image(pkt->data, pkt->dsize, 0, param->processor.dtype,cam->tmp);

	}
	else {
		fprintf(stderr,"Error: nof_chan=%d > MAX_NOF_CHANNELS (%d)",nof_chan,MAX_NOF_CHANNELS);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

/**
 * @brief Process the last block of a Frame packet.
 *
 * In this function we try to convert the image in a BGR displayed image. Then:
 * 	-	If ProcParams::to_show is @TRUE, we try to display an image if it was
 * possible to have a BGR image.
 * 	-	If ProcParams::is_towrite is @TRUE, we try to save the frame in a file according to its
 * datatype and the filename extension.
 *
 */
int Proc_FrameLast(s7p_pkt_t *pkt,Params *param) {

	//Declare variable
	s7p_hdr_t *hdr=pkt->s7p_hdr;
	Board *board = &(param->board);
	Camera *cam=&(board->cam[board->cam_id]);
	bool is_bgr;
	int twait,maxfc;
	int fourcc;
	char key;

	//Check if we execute the correct command.
	if(hdr->cmd != S7P_CMD_FRAME_LAST) {
		fprintf(stderr,"Error: cmd:%0X != S7P_CMD_FRAME_LAST",hdr->cmd);
		return EXIT_FAILURE;
	}

	//Start to process the last data.
	Proc_Data2Image(pkt->data, pkt->dsize, pkt->s7p_hdr->val, param->processor.dtype,cam->tmp);


	//Then check if we can convert the TMP image to a displayed image (BGR).
	is_bgr = Proc_ImgConvertTmpToBGR(cam, param->processor.dtype);
	maxfc=param->processor.nof_frames_consec;

	//And show the image.
	if(is_bgr && param->processor.show_im) {
		cvNamedWindow(cam->name,CV_WINDOW_AUTOSIZE);
		cvShowImage(cam->name,cam->rgb);
		if(maxfc > 0 && (cam->frame_num%maxfc)==0 && board->cam_id==0) {
			twait=0;
			printf("Press a key in the image windows to continue, or ESC to exit\n");
			fflush(stdout);
		}
		else twait=2;

		//		//Special add for miguel
		//		if(board->cam_id==0) {
		//		IplImage *imR,*imG,*imB;
		//		imR = cvCreateImage(cvGetSize(cam->rgb),IPL_DEPTH_8U,1);
		////		imG = cvCreateImage(cvGetSize(cam->rgb),IPL_DEPTH_8U,1);
		////		imB = cvCreateImage(cvGetSize(cam->rgb),IPL_DEPTH_8U,1);
		//		cvSplit(cam->rgb,NULL,NULL,imR,NULL);
		//		cvNamedWindow("R",CV_WINDOW_AUTOSIZE);
		//		cvShowImage("R",imR);
		////		cvNamedWindow("G",CV_WINDOW_AUTOSIZE);
		////		cvShowImage("G",imG);
		////		cvNamedWindow("B",CV_WINDOW_AUTOSIZE);
		////		cvShowImage("B",imB);
		//		cvReleaseImage(&imR);
		////		cvReleaseImage(&imG);
		////		cvReleaseImage(&imB);
		//		//End add for miguel
		//		}

		key = cvWaitKey(twait);
		if(key == OPENCV_ESC_KEY) {
			if(vidWriter!=NULL) cvReleaseVideoWriter(&vidWriter);
			exit(EXIT_SUCCESS);
		}
	}


	//Then we set the filename and check if it is okay
	if(Proc_SetFileName(fname,param)) {

		//In case the image is displayable we can save it with OpenCV.
		if(is_bgr && param->processor.out_type==OUTPUT_TYPE_CVIMG){
			cvSaveImage(fname,cam->rgb);
			printf("Write image in:%s\n",fname);
		}
		//If image can be displayed and we want to save in a video.
		else if(is_bgr && param->processor.out_type==OUTPUT_TYPE_VIDEO) {
			if(vidWriter == NULL) {
				#ifdef WIN32
					fourcc=-1;
				#else
					#ifdef HAVE_FFMPEG
						fourcc = CV_FOURCC('X','V','I','D');
					#else
						fourcc = CV_FOURCC('I','4','2','0');
					#endif
				#endif
				vidWriter = cvCreateVideoWriter(fname,fourcc
						,param->processor.video_fps, cvGetSize(cam->rgb),TRUE);
			}
			if(vidWriter) {
				#ifndef WIN32
					cvFlip(cam->rgb,cam->rgb,0);
				#endif
				cvWriteFrame(vidWriter,cam->rgb);
			}
		}
		//Otherwise we write in binary.
		else {
			cam->fs = fopen(fname,"wb");
			if(cam->fs==NULL) {
				fprintf(stderr,"Error: cam->fs = NULL (Can't open %s)\n",fname);
				return EXIT_FAILURE;
			}
			fwrite(cam->tmp->imageDataOrigin,1,cam->tmp->imageSize,cam->fs);
			fclose(cam->fs);
			printf("Write binary data in:%s\n",fname);

			if(maxfc > 0 && (cam->frame_num%maxfc)==0 && board->cam_id==0) {
				printf("Press a key to continue\n");
				scanf("%c",&key);
			}
		}
	}
}

/**
 * @brief Dump the stream send by the board to a File.
 * @warning This function should not be used because it
 * can make a lot of error by writing packet per packet.
 */
int Proc_DumpStream(s7p_pkt_t *pkt,Params *param) {
	s7p_hdr_t *hdr=pkt->s7p_hdr;
	Board *board = &(param->board);
	Camera *cam=&(board->cam[board->cam_id]);

	switch(hdr->cmd) {
	case S7P_CMD_FRAME_SETUP:
		cam->seq_Id=hdr->fpID;
		if(board->nof_cam == 1)  sprintf(fname,board->tpl_fname,cam->frame_num);
		else sprintf(fname,board->tpl_fname,board->cam_id,cam->frame_num);
		cam->fs = fopen(fname,"wb");
		if(cam->fs==NULL) {
			fprintf(stderr,"Error: cam->fs = NULL (Can't open %s)\n",fname);
			return EXIT_FAILURE;
		}
		//No break point because we want to dump in the binary file.
	case S7P_CMD_FRAME_BLOCK:
		if(cam->fs==NULL) {
			fprintf(stderr,"Error: cam->fs = NULL\n");
			return EXIT_FAILURE;
		}
		if(hdr->fpID==cam->seq_Id) {
			fwrite(pkt->data,1,pkt->dsize,cam->fs);
			//printf("%04x -> %d ",pkt->s7p_hdr->val,pkt->dsize);
		}
		break;
	case S7P_CMD_FRAME_LAST:
		if(cam->fs==NULL) {
			fprintf(stderr,"Error: filename %s was closed or never opened",fname);
			return EXIT_FAILURE;
		}
		if(hdr->fpID==cam->seq_Id) {
			fwrite(pkt->data,1,pkt->dsize,cam->fs);
		}
		fclose(cam->fs);
		break;
	}
	return EXIT_SUCCESS;
}

/**
 * @brief Convert image in the diplayed BGR format.
 *
 * The conversion can failed if we have 0 channel in the initial image,
 * or if the datatype is unknown.
 *
 * @return @TRUE if we have an image to display, @FALSE otherwise.
 */
bool Proc_ImgConvertTmpToBGR(Camera *cam, int datatype) {
	bool is_sizeneq, is_typeneq;
	double max_val,min_val,scale_val;
	IplImage *hsv;

	//If 0 channel
	if(((datatype >> 8) & 0xFF) == 0) {
		return FALSE;
	}

	printf("Convert image...\n");

	//Check if the memory of the output exist or create it.
	if(cam->rgb == NULL) {
		cam->rgb = cvCreateImage(cvGetSize(cam->tmp),IPL_DEPTH_8U,3);
		printf("image created\n");
	}
	else {
		if(!CV7_ARE_IMSIZEEQ(cam->tmp,cam->rgb)) {
			if(cam->rgb != NULL) cvReleaseImage(&(cam->rgb));
			cam->rgb = cvCreateImage(cvGetSize(cam->tmp),IPL_DEPTH_8U,3);
			printf("image created\n");
		}
	}

	//Create the convertion depending on the type of data.
	switch(datatype) {
	case S7P_DTYPE_RAW_RG:
		cvCvtColor(cam->tmp,cam->rgb,CV_BayerRG2RGB);
		break;
	case S7P_DTYPE_RAW_BG:
		cvCvtColor(cam->tmp,cam->rgb,CV_BayerBG2RGB);
		break;
	case S7P_DTYPE_RGB:
		cvCvtColor(cam->tmp,cam->rgb,CV_RGB2BGR);
	case S7P_DTYPE_RGBA:
		cvCvtColor(cam->tmp,cam->rgb,CV_RGBA2BGR);
		break;
	case S7P_DTYPE_YCrCb:
		cvCvtColor(cam->tmp,cam->rgb,CV_YCrCb2BGR);
		break;
	case S7P_DTYPE_HRL:

		hsv = cvCreateImage(cvGetSize(cam->tmp), IPL_DEPTH_8U,3);
		cvMinMaxLoc(cam->tmp,&min_val,&max_val,NULL,NULL,cam->tmp);
		scale_val=255/(max_val-min_val);
		cvSet(hsv,cvScalar(0,0,0,0),NULL);
		cvSet(hsv,cvScalar(0,255,255,0),cam->tmp);
		cvConvertScale(cam->tmp,cam->tmp,1,-min_val);
		cvConvertScale(cam->tmp,cam->tmp,scale_val,0);
		cvMerge(cam->tmp,NULL,NULL,NULL,hsv);
		cvCvtColor(hsv,cam->rgb,CV_HSV2BGR);
		cvReleaseImage(&hsv);

		break;
	case S7P_DTYPE_YUV422:
		if(cam->tmp->nChannels==2)
			TransformYUV4222BGR(cam->tmp->imageData,cam->rgb->imageData,cam->tmp->width*cam->tmp->height);
		else return FALSE;
		break;
	case S7P_DTYPE_BWBIN:
	case S7P_DTYPE_GREY:
		cvCvtColor(cam->tmp,cam->rgb,CV_GRAY2BGR);
		break;
	default:
		fprintf(stderr,"Error: Can't convert to BGR for dtype=%d\n",datatype);
		fflush(stderr);
		return FALSE;
	}
	return TRUE;
}

/**
 * @brief Set the Filename to output the image (also check if it is ok to output).
 *
 * @param fname A pointer on a buffer of memory to be filled with the filename.
 * @param param The structure with all the parameters.
 *
 * @return @TRUE if we are asked to write in a file and if the file can be created.
 */
bool Proc_SetFileName(char *fname, Params *param) {


	Board *board = &(param->board);
	Camera *cam=&(board->cam[board->cam_id]);

	if(param->processor.is_towrite) {
		if(board->nof_cam == 1)  sprintf(fname,board->tpl_fname,cam->frame_num);
		else sprintf(fname,board->tpl_fname,board->cam_id,cam->frame_num);
		return TRUE;
	}
	return FALSE;
}


//-----------------------------------------------



void BWBin_drawPacket(u8 *memdst,const u8 *memsrc,int buff_size) {


	u8 *p_in,*p_out,*end;
	u8 tmp;

	p_in=(u8*)memsrc;
	end=p_in+buff_size;
	p_out=memdst;
	int b;

	while(p_in<end) {

		tmp=*p_in;	//Set the value of each block of 8 pixels/bits in tmp.

		//Shift each bit and set it one p_out byte.
		for(b=0;b<8;b++) {
			if((tmp & 0x80)) *p_out = 255;	//If the most-left (first) bit is true.
			else *p_out=0;
			p_out++;		//Go to the next output pixel (bytes)
			tmp = tmp << 1;	//Shift from one bit.
		}

		//Go to next inputs (block of 8 pixels)
		p_in++;
	}
}


typedef struct {
	unsigned int id;
	unsigned int row:9;
	unsigned int end:9;
	unsigned int start:9;
	unsigned int zeros:5;
} rle_tupple_t;

/**
 * @brief Take a buffer with RLE code describes in @cite{Appiah2008}.
 * @param buff A pointer on the memory where we can read the tuples [Id,start,end,row].
 * @param buff_size The size in byte of the buffer.
 * @param hrl A pointer on the HueRunLenght image (1 channel)
 *
 * The RLE is in the following form:
 *
 * @code
 * ----------------------------------
 * || 31 ........16 15.......... 0 ||
 * ----------------------------------
 * ||   noise      |       ID      ||
 * || ? | start  |   end  |  stop  ||
 * ----------------------------------
 * ||                              ||
 * ||            ...               ||
 * ----------------------------------
 * @endcode
 *
 * Using the following bytes:
 * 		- ID is on 16 bits  [15<00].
 * 		- start on 9 bits   [24<18].
 * 		- end on 9 bits     [17<09].
 * 		- stop on 9 bits	[08<00].
 *
 * @note The field equivalence (eq) is not used in this decoder.
 */
void RLE_drawBlobIm(const u8 *buff,int buff_size, IplImage *hrl) {
	int i;
	u32 max=0, *p_in, *p_tup;
	int nof_tupple=buff_size/sizeof(rle_tupple_t);
	rle_tupple_t rle_tup;
	u8 id,jump_id;
	double max_val, min_val,scale_val;



	//Take paramateres for each tupples and draw them..
	p_in=(u32*)buff;
	p_tup=(u32*)(&rle_tup.id);
	for(i=0;i<nof_tupple;i++) {
		//Swap buffers
		swapEndian_u32(p_tup,p_in++);
		swapEndian_u32(p_tup+1,p_in++);

		if(rle_tup.id>max) {
			max=rle_tup.id;
		}

		//Break image when we received ID as zeros.
		if(rle_tup.id == 0) break;

		//Draw RLE tupple
		RLE_drawTupple(rle_tup.id,rle_tup.start,rle_tup.end,rle_tup.row, hrl);
	}
}

/**
 * @brief Take the value of a RLE tupple and draw it.
 */
void RLE_drawTupple(u8 id, u16 start, u16 end, u16 row, IplImage *out) {

	u8 *p_im, *p_end;
	u8 nC=out->nChannels;
	int w,h;

	if(out == NULL) {
		printf("Image is NULL");
		return;
	}

	//Check Range
	if(!CHECK_URANGE(start, out->width) | !CHECK_RANGE(end, start, out->width)) {
		printf("Error: #%d > start %d > width %d or end %d > width %d\n",id,start,out->width,end,out->width);
		return;
	}
	if(!CHECK_URANGE(row, out->height)) {
		printf("Error: #%d > row (%d) > height (%d)\n",id,row,out->height);
		return;
	}


	p_im =(u8*)(out->imageData + row*out->widthStep);
	p_end = (u8*)(p_im + (end*nC));

	p_im+=start*nC;
	while(p_im< p_end) {
		p_im[0]=id;
		if(nC>1) {
			p_im[1]=255;
			p_im[2]=255;
		}
		p_im+=nC;		// Jump to the next pixel.
	}
}

/**
 * @brief Convert an image in YUV 4:2:2 to BGR (OpenCV).
 *
 * Pixel are ordered 2 by 2 as:
 * @code
 * | U Y_0 V Y_1 | U Y_0 V Y_1 | ...
 * @endcode
 * @see YCrCb2pBGR.
 */
void TransformYUV4222BGR(const u8* im_yuv422, u8* im_bgr, int nof_pixels) {

	u_char *p_bgr, *p_uyvy,*end;

	p_bgr = im_bgr;
	p_uyvy = (u8*)im_yuv422;
	end = p_bgr + nof_pixels*3; //3 Channels for RGB

	//loop by 2 pixels
	while(p_bgr<end) {

		YUV2pBGR(p_uyvy[1],p_uyvy[0],p_uyvy[2], p_bgr);  		//Convert the 1st pixel
		YUV2pBGR(p_uyvy[3],p_uyvy[0],p_uyvy[2], p_bgr+3);		//Convert the 2nd pixel

		//Jump to the next pair of pixel.
		p_uyvy+=4;
		p_bgr+=6;
	}
}

/**
 * @brief Convert an image in YUV 4:2:2 to RGB.
 *
 * Pixel are ordered 2 by 2 as:
 * @code
 * | U Y_0 V Y_1 | U Y_0 V Y_1 | ...
 * @endcode
 * @see YCrCb2pRGB.
 */
void TransformYUV4222RGB(const u8* im_yuv422, u8* im_rgb, int nof_pixels) {

	u_char *p_rgb, *p_uyvy,*end;

	p_rgb = im_rgb;
	p_uyvy = (u8*)im_yuv422;
	end = p_rgb + nof_pixels*3; //3 Channels for RGB

	//loop by 2 pixels
	while(p_rgb<end) {

		YUV2pRGB(p_uyvy[1],p_uyvy[0],p_uyvy[2], p_rgb);  		//Convert the 1st pixel
		YUV2pRGB(p_uyvy[3],p_uyvy[0],p_uyvy[2], p_rgb+3);		//Convert the 2nd pixel

		//Jump to the next pair of pixel.
		p_uyvy+=4;
		p_rgb+=6;
	}
}

/**
 * @brief Convert values Y,U,V to an RGB pixel.
 *
 * This function is inspired from the Y'UV444toRGB888() that you can find on wikipedia.
 *
 * @note This conversion don't use floating point arithmetic.
 * @see http://en.wikipedia.org/wiki/YUV#Y.27UV444
 *
 */
void YUV2pRGB(u8 y, u8 u, u8 v,u8* p_rgb) {
	int C,E,D;

	C= y -16;
	D = u -128;
	E = v -128;

	p_rgb[R] = clipByte_i32((298*C + 409*E +128) >> 8);
	p_rgb[G] = clipByte_i32((298*C - 100*D - 208*E +128) >> 8);
	p_rgb[B] = clipByte_i32((298*C + 516*D +128) >> 8);
}

/**
 * @brief Convert values Y,U,V to an BGR pixel.
 *
 * This function is inspired from the Y'UV444toRGB888() that you can find on wikipedia.
 *
 * @note This conversion don't use floating point arithmetic.
 * @see http://en.wikipedia.org/wiki/YUV#Y.27UV444
 *
 */
void YUV2pBGR(u8 y, u8 u, u8 v,u8* p_bgr) {
	int C,E,D;

	C= y -16;
	D = u -128;
	E = v -128;

	p_bgr[B] = clipByte_i32((298*C + 409*E +128) >> 8);
	p_bgr[G] = clipByte_i32((298*C - 100*D - 208*E +128) >> 8);
	p_bgr[R] = clipByte_i32((298*C + 516*D +128) >> 8);
}

/**
 * @brief Clip a value between [0-255]and return it in u_char type.
 * @param val The input in int (signed 32 bits).
 * @return The output value on 8-bit.
 */
u8 clipByte_i32(int val) {
	int tmp=val;
	if(tmp< 0) tmp=(int)0;
	if(tmp > 255) tmp = (int)255;
	return (u_char)tmp;
}

