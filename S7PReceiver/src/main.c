/**
 *  @file
 *  @brief The main file of the project.
 *  @date 09-jan-2008
 *  @author Benoit RAT
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "config.h"
#include "network.h"

/**
 * @brief Main function which load an XML files.
 * @return EXIT_SUCCESS if the program as correctly run.
 */
int main(int argc,const char *argv[]) {

	Params params;
	netsocket_t *socket;

	//Check the minimum number of arguments
	if(argc!=2){
		printf("Error in the argument:\n");
		printf("Usage: %s [config.xml]\n",argv[0]);
		exit(EXIT_FAILURE);
	}

	readConfigFromXML(argv[1],&params);	//Read the config.xml file.
	socket = bindSocket(&params); 		//Listen to an interface or UDP socket
	runS7Protocol(&params,socket); 	//Start the real S7Protocol (Look at this function)
	closeSocket(socket);				//Then close the socket

	exit(EXIT_SUCCESS);
}
