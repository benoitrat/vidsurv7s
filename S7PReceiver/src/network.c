/**
 *  @file
 *  @brief Contains the functions that communicate with the S400 boards.
 *  @date 09-ene-2009
 *  @author neub
 */

#include "network.h"


/**
 * @brief Run the Smart/Simple Sevensols Protocol.
 * @param socket A pointer on the general netsocket opened.
 * @param params General parameters for this software.
 * @return @FALSE when an error has been detected, @TRUE otherwise.
 */
int runS7Protocol(Params *params, netsocket_t* socket) {


	s7p_pkt_t *mypkt, *xilpkt;
	int camID,ret,brcv,dtype;
	mypkt = S7PPkt_init(params,TRUE);
	xilpkt = S7PPkt_init(params,FALSE);


	if(S7P_pingBoard(params,socket,3)==FALSE) exit(EXIT_FAILURE);

	S7P_setup(params,socket);	//First make the Setup protocol
	Proc_SetupOutput(params);	//Once parameters are defined, set-up the output



	//Start to send data.
	mypkt->s7p_hdr->cmd=S7P_CMD_SEND_START;
	mypkt->s7p_hdr->camID=CAMID_1;
	sndPacket(mypkt,socket,0);

	while(TRUE) {
		if((brcv = rcvPacket(xilpkt,socket,TRUE))>0) {

			switch(xilpkt->s7p_hdr->cmd) {
			case S7P_CMD_FRAME_SETUP:

				//Set the camera ID.
				camID= xilpkt->s7p_hdr->camID;
				mypkt->s7p_hdr->camID=camID;
				params->board.cam_id=camID;

				//Set the camera sequence ID and increment frame number.
				params->board.cam[camID].seq_Id=xilpkt->s7p_hdr->fpID;
				params->board.cam[camID].frame_num++;
				params->board.cam[camID].pkt_cnt=0;

				//Process the packet.
				printf("BOARD -> PC: SETUP \t(cam=%d,frame=%d)\n",camID,params->board.cam[camID].seq_Id);
				ret = Proc_FramePkt(xilpkt,params);

				break;
			case S7P_CMD_FRAME_BLOCK:
				if(xilpkt->s7p_hdr->camID == camID ) {
					params->board.cam[camID].pkt_cnt++;
					ret = Proc_FramePkt(xilpkt,params);
					//					printf("memcpy #%d pkt[%d] -> 0x%X\n",params->board.cam[camID].pkt_cnt,xilpkt->dsize,xilpkt->s7p_hdr->val);
					//					fflush(stdout);
				}
				else {
					fprintf(stderr,"Error in packet: ");
					S7PPkt_Show(xilpkt);
					fflush(stdout);
				}
				break;
			case S7P_CMD_FRAME_LAST:
				if(xilpkt->s7p_hdr->camID == camID ) {
					printf("BOARD -> PC: LAST \t(cam=%d,frame=%d,count=%d)\n",
							camID,
							params->board.cam[camID].seq_Id,
							params->board.cam[camID].pkt_cnt);
					fflush(stdout);
					ret = Proc_FramePkt(xilpkt,params);
					mypkt->s7p_hdr->cmd=S7P_CMD_FRAME_ACKS;
					//mypkt->s7p_hdr->cmd=S7P_CMD_SEND_STOP;
					mypkt->s7p_hdr->camID=camID;
					mypkt->s7p_hdr->fpID=params->board.cam[camID].seq_Id;
					printf("PC -> BOARD: ACKS \t(cam=%d,frame=%d)\n\n",camID,params->board.cam[camID].seq_Id);
					fflush(stdout);
					sndPacket(mypkt,socket,0);
				}
				break;
			case S7P_CMD_DEBUG:
				//Do nothing.
				break;

			default:
				printf("Unkown command: ");
				S7PPkt_Show(xilpkt);
				break;
			}
			if(ret==EXIT_FAILURE) {
				S7P_close(mypkt,socket);
				exit(EXIT_FAILURE);
			}

		}
	}




	S7PPkt_release(mypkt);
	S7PPkt_release(xilpkt);
}

/**
 * @brief Try to stop the board and then close socket.
 */
void S7P_close(s7p_pkt_t *mypkt,netsocket_t* socket) {
	mypkt->s7p_hdr->cmd=S7P_CMD_SEND_STOP;
	sndPacket(mypkt,socket,0);
	if(socket->mode==NET_MODE_MAC) {
		NetMAC_closeInterface(socket->mac);
	}
	closeSocket(socket);
	exit(EXIT_FAILURE);
}

int S7P_setup(Params *params,netsocket_t* socket) {

	s7p_pkt_t *mypkt, *xilpkt;
	int ret;
	u32 *val;

	printf("\nSetup the board:\n");

	//Init the packet.
	mypkt = S7PPkt_init(params,TRUE);
	xilpkt = S7PPkt_init(params,FALSE);


	//First argument.
	mypkt->s7p_hdr->cmd=S7P_CMD_PARAM_GET;
	mypkt->s7p_hdr->fpID=S7P_PRM_BRD_DTYPE;
	val = (&(params->processor.dtype));
	ret = S7P_GetSetParam(socket,mypkt,xilpkt,val,3);


	//Release packet.
	S7PPkt_release(mypkt);
	S7PPkt_release(xilpkt);
	printf("\n");
}

/**
 * @brief Apply,Set or Get Parameters to the boards.
 *
 * @note The cmd and the type of parameters must be defined in mypkt before calling this function.
 */
int S7P_GetSetParam(netsocket_t *socket, s7p_pkt_t *mypkt, s7p_pkt_t *xilpkt, u32 *p_val, int max_tries) {
	int nof_tries=0,ret;
	u32 cmd=mypkt->s7p_hdr->cmd;
	char buff[100];

	if(cmd != S7P_CMD_PARAM_SET && cmd!=S7P_CMD_PARAM_APPLY && cmd!=S7P_CMD_PARAM_GET) {
		printf("Error: Can't set up with bad cmd: 0x%0X\n",cmd);
		return -1;
	}
	if(p_val == NULL) {
		printf("Error: p_val -> NULL");
		return -1;
	}
	mypkt->s7p_hdr->val=*p_val;
	sndPacket(mypkt,socket,0);
	while(nof_tries<max_tries) {
		ret = rcvPacket(xilpkt,socket,TRUE);
		if(ret>0 && xilpkt->s7p_hdr->fpID==mypkt->s7p_hdr->fpID) {
			if(xilpkt->s7p_hdr->cmd==S7P_CMD_PARAM_ACK) {
				S7P_SetParamName(mypkt->s7p_hdr->fpID,buff);
				switch(cmd)
				{
				case S7P_CMD_PARAM_APPLY: printf("Apply:\t"); break;
				case S7P_CMD_PARAM_SET: printf("Set:\t"); break;
				case S7P_CMD_PARAM_GET: printf("Get:\t"); break;
				}
				*p_val = xilpkt->s7p_hdr->val;
				printf("%s = %d (0x%X)\n",buff,*p_val, *p_val);
				return 1;
			}
			else if(xilpkt->s7p_hdr->cmd==S7P_CMD_PARAM_ERR) {
				switch(xilpkt->s7p_hdr->val)
				{
				case S7P_ERR_PRM_NOTDEF: fprintf(stderr,"Error: Param Not def\n"); break;
				case S7P_ERR_PRM_READONLY: fprintf(stderr,"Error: Param Read Only\n"); break;
				case S7P_ERR_UNKOWN: fprintf(stderr,"Error: Unkown\t"); break;
				}
				return  xilpkt->s7p_hdr->val;
			}
		}
		nof_tries++;
		printf("wait ..."); wait(1); printf("...\n");
	}
	return 0;	//Timeout.
}

int S7P_pingBoard(Params *params, netsocket_t* socket, int nof_tries) {

	int randid = rand();
	randid=0xA0B1C2D3;
	int try; int sleep_in_s=1;
	int i=0, ret;
	bool has_reply=FALSE;
	u8 eha_BC[6]= {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
	s7p_pkt_t *mypkt, *xilpkt;
	mypkt = S7PPkt_init(params,TRUE);
	xilpkt = S7PPkt_init(params,FALSE);


	mypkt->s7p_hdr->camID=CAMID_ALL;
	mypkt->s7p_hdr->cmd=S7P_CMD_BOARD_ASK;
	mypkt->s7p_hdr->val=randid;
	mypkt->s7p_hdr->fpID=0xAABB;
	CopyEHA(eha_BC,mypkt->mac_hdr->eha_dst);


	try=0;
	while(try < nof_tries) {

		if(try!=0) {
			printf(" (Retry in %d seconds)\n\n",sleep_in_s);
			fflush(stdout);
			wait(sleep_in_s);
			sleep_in_s*=2;
		}


		try++;
		printf_aru8("Ping board %s ",xilpkt->mac_hdr->eha_src,6,"%02X:");
		printf("(try:%d/%d)\n",try,nof_tries);
		sndPacket(mypkt,socket,0);

		for(i=0;i<10;i++) {
			ret = rcvPacket(xilpkt,socket,FALSE);
			if(ret <= 0) continue;
			if(xilpkt->s7p_hdr->cmd==S7P_CMD_BOARD_ACK) {
				if(xilpkt->s7p_hdr->val==randid) {
					if(CompareEHA(xilpkt->mac_hdr->eha_src,params->board.eha)) {
						printf_aru8("Board %s is connected\n",xilpkt->mac_hdr->eha_src,6,"%02X:");
						has_reply=TRUE;
					}
					else {
						printf_aru8("Others board is connected %s\n",xilpkt->mac_hdr->eha_src,6,"%02:");
					}
				}
				else {
					fprintf(stderr,"Wrong ID %X (should be %X)\n",xilpkt->s7p_hdr->val,randid);
				}
			}
			else {
				fprintf(stderr,"Not Correct cmd:%0x\n",xilpkt->s7p_hdr->cmd);
			}
		}
		if(has_reply) break;
		printf_aru8("Board %s is unreacheable",xilpkt->mac_hdr->eha_src,6,"%02X:");

	}
	printf("\n");

	S7PPkt_release(mypkt);
	S7PPkt_release(xilpkt);

	return has_reply;
}


void S7P_SetParamName(u32 paramID,char *buff) {

	switch(paramID) {

	/* S400 Fixed value */
	case S7P_PRM_RO_VFW: sprintf(buff,"%s","S7P_PRM_RO_VFW"); break;
	case S7P_PRM_RO_VS7P: sprintf(buff,"%s","S7P_PRM_RO_VS7P"); break;
	case S7P_PRM_RO_NOFCAM: sprintf(buff,"%s","S7P_PRM_RO_NOFCAM"); break;

	/* S400 Parameters */
	case S7P_PRM_BRD_IP: sprintf(buff,"%s","S7P_PRM_BRD_IP"); break;
	case S7P_PRM_BRD_PORT_S7P: sprintf(buff,"%s","S7P_PRM_PORT_BRD_S7P"); break;
	case S7P_PRM_BRD_DHCP_ON: sprintf(buff,"%s","S7P_PRM_BRD_DHCP_ON"); break;
	case S7P_PRM_BRD_MODES: sprintf(buff,"%s","S7P_PRM_BRD_MODES"); break;
	case S7P_PRM_BRD_DTYPE: sprintf(buff,"%s","S7P_PRM_BRD_DTYPE"); break;
	case S7P_PRM_BRD_WSIZE: sprintf(buff,"%s","S7P_PRM_BRD_WSIZE"); break;
	case S7P_PRM_BRD_FRAMERATE: sprintf(buff,"%s","S7P_PRM_BRD_FRAMERATE"); break;
	case S7P_PRM_BRD_PORT_RTP: sprintf(buff,"%s","S7P_PRM_BRD_PORT_RTP"); break;

	/* Camera Parameters */
	case S7P_PRM_CAM_ENABLE: sprintf(buff,"%s","S7P_PRM_CAM_ENABLE"); break;
	case S7P_PRM_CAM_RESO: sprintf(buff,"%s","S7P_PRM_CAM_RESO"); break;
	case S7P_PRM_CAM_QUALITY: sprintf(buff,"%s","S7P_PRM_CAM_QUALITY"); break;
	case S7P_PRM_CAM_BG_REFRESH: sprintf(buff,"%s","S7P_PRM_CAM_BG_REFRESH"); break;
	case S7P_PRM_CAM_BG_THRS: sprintf(buff,"%s","S7P_PRM_CAM_BG_THRS"); break;
	case S7P_PRM_CAM_CC_MINAREA: sprintf(buff,"%s","S7P_PRM_CAM_CC_MINAREA"); break;
	case S7P_PRM_CAM_DEBUG: sprintf(buff,"%s","S7P_PRM_CAM_DEBUG"); break;
	case S7P_PRM_IIDC_BRIGHTNESS: sprintf(buff,"%s","S7P_PRM_IIDC_BRIGHTNESS"); break;
	case S7P_PRM_IIDC_AUTO_EXPOSURE: sprintf(buff,"%s","S7P_PRM_IIDC_AUTO_EXPOSURE"); break;
	case S7P_PRM_IIDC_SHARPNESS: sprintf(buff,"%s","S7P_PRM_IIDC_SHARPNESS"); break;
	case S7P_PRM_IIDC_WHITE_BALANCE: sprintf(buff,"%s","S7P_PRM_IIDC_WHITE_BALANCE"); break;
	case S7P_PRM_IIDC_HUE: sprintf(buff,"%s","S7P_PRM_IIDC_HUE"); break;
	case S7P_PRM_IIDC_SATURATION: sprintf(buff,"%s","S7P_PRM_IIDC_SATURATION"); break;
	case S7P_PRM_IIDC_GAMMA: sprintf(buff,"%s","S7P_PRM_IIDC_GAMMA"); break;
	case S7P_PRM_IIDC_SHUTTER: sprintf(buff,"%s","S7P_PRM_IIDC_SHUTTER"); break;
	case S7P_PRM_IIDC_GAIN: sprintf(buff,"%s","S7P_PRM_IIDC_GAIN"); break;
	case S7P_PRM_IIDC_IRIS: sprintf(buff,"%s","S7P_PRM_IIDC_IRIS"); break;
	case S7P_PRM_IIDC_FOCUS: sprintf(buff,"%s","S7P_PRM_IIDC_FOCUS"); break;
	case S7P_PRM_IIDC_TEMPERATURE: sprintf(buff,"%s","S7P_PRM_IIDC_TEMPERATURE"); break;
	case S7P_PRM_IIDC_TRIGGER: sprintf(buff,"%s","S7P_PRM_IIDC_TRIGGER"); break;
	case S7P_PRM_IIDC_ZOOM: sprintf(buff,"%s","S7P_PRM_IIDC_ZOOM"); break;
	case S7P_PRM_IIDC_PAN: sprintf(buff,"%s","S7P_PRM_IIDC_PAN"); break;
	case S7P_PRM_IIDC_TILT: sprintf(buff,"%s","S7P_PRM_IIDC_TILT"); break;
	case S7P_PRM_IIDC_FILTER: sprintf(buff,"%s","S7P_PRM_IIDC_FILTER"); break;
	default: sprintf(buff,"%s","S7P_PRM_UNKNOWN"); break;
	}
}

/**
 * @warning Wait 1 seconds after sending a packet through MAC.
 */
int sndPacket(s7p_pkt_t *pkt, netsocket_t *socket, int size_inB) {
	int ret=0;
	switch(socket->mode) {
	case NET_MODE_MAC:
		if(socket->mac != NULL) {
			S7PPkt_setBuffer(pkt);
			ret = NetMAC_sndPacket(pkt,socket->mac,size_inB);
		}
		return ret;
		break;
	case NET_MODE_UDP:
		if(socket->udp != NULL)
			printf("Not implemented");
		break;
	}
	return 0;
}


int rcvPacket(s7p_pkt_t *pkt, netsocket_t *socket,bool filter) {
	int ret;
	switch(socket->mode) {
	case NET_MODE_MAC:
		if(socket->mac != NULL)
			ret= NetMAC_rcvPacket(pkt,socket->mac,filter);
		if(ret > 0) S7PPkt_setHeaders(pkt);
		return ret;
		break;
	case NET_MODE_UDP:
		if(socket->udp != NULL)
			printf("Not implemented");
		break;
	}
	return 0;
}



/**
 * @brief Allocate and init the memory for the S7P packet.
 * @note The field of the packet are initiated such as the packet is a send packet.
 * In case this packet should be received, the field are erased during the setHeaders() functions.
 */
s7p_pkt_t* S7PPkt_init(Params *params, bool to_send) {

	s7p_pkt_t *pkt = (s7p_pkt_t*)malloc(sizeof(s7p_pkt_t));
	pkt->type=params->net.mode;

	if(pkt->type==NET_MODE_MAC) {
		pkt->mac_hdr= (mac_hdr_t*)malloc(sizeof(mac_hdr_t));
		if(to_send) {
			CopyEHA(params->net.eha,pkt->mac_hdr->eha_src);
			CopyEHA(params->board.eha,pkt->mac_hdr->eha_dst);
		}
		else  {
			CopyEHA(params->net.eha,pkt->mac_hdr->eha_dst);
			CopyEHA(params->board.eha,pkt->mac_hdr->eha_src);
		}
		pkt->mac_hdr->type=MAC_TYPE_S7P;

	}
	if(pkt->type==NET_MODE_UDP) {
		pkt->ip_hdr=(ip_hdr_s*)malloc(sizeof(ip_hdr_s));
		pkt->udp_hdr=(udp_hdr_s*)malloc(sizeof(udp_hdr_s));
	}
	pkt->s7p_hdr= (s7p_hdr_t*)malloc(sizeof(s7p_hdr_t));
	pkt->s7p_hdr->val=0;
	pkt->s7p_hdr->camID=CAMID_ALL;

	//	printf("Init done\n"); fflush(stdout);

	return pkt;
}

void S7PPkt_release(s7p_pkt_t* pkt) {
	if(pkt->mac_hdr!=NULL) free(pkt->mac_hdr);
	if(pkt->ip_hdr !=NULL) free(pkt->ip_hdr);
	if(pkt->udp_hdr!=NULL) free(pkt->udp_hdr);
	if(pkt->s7p_hdr!=NULL) free(pkt->s7p_hdr);

	free(pkt);
}

/**
 * @brief Set S7P packet headers after receiving it from a socket.
 */
void S7PPkt_setHeaders(s7p_pkt_t* pkt) {

	swapEndian_u32buffer((u8*)pkt->s7p_hdr,(const u8*)(pkt->buff +SIZEIB_HDR_ZMAC),8);

	//	u32 *p;
	//	p = (u32*)(pkt->buff+SIZEIB_HDR_ZMAC);
	//	printf("buffer  (0x%X): %08X, %08X\n",(u32)p,p[0],p[1]); fflush(stdout);
	//	p=(u32*)pkt->s7p_hdr;
	//	printf("s7p_hdr (0x%X): %08X, %08X\n",(u32)p,p[0],p[1]); fflush(stdout);

}

/**
 * @brief Set S7P packet buffer before sending it to a socket.
 */
void S7PPkt_setBuffer(s7p_pkt_t* pkt) {

	//	u32 *p = (u32*)pkt->s7p_hdr;
	//	printf("s7p_hdr (0x%X): %08X, %08X\n",p,p[0],p[1]); fflush(stdout);

	swapEndian_u32buffer((u8*)(pkt->buff +SIZEIB_HDR_ZMAC),(const u8*)pkt->s7p_hdr,8);

	//	p = (u32*)(pkt->buff+SIZEIB_HDR_ZMAC);
	//	printf("swap buffer (0x%X): %08X %08X\n",p,p[0],p[1]); fflush(stdout);
	//	printf_aru8("buffer:%s",pkt->buff,SIZEIB_HDR_ZMAC+SIZEIB_HDR_S7P,"%02X,");
	//	fflush(stdout);

}

void S7PPkt_Show(s7p_pkt_t* pkt) {

	printf("# Pkt:");
	//Print mac
	if(pkt->type==NET_MODE_MAC) {
		printf_aru8("src:%s",pkt->mac_hdr->eha_src,6,"%02X:");
		printf_aru8(" -> dst:%s | ",pkt->mac_hdr->eha_dst,6,"%02X:");
	}
	printf("cmd=%02X, camID=%3d, ",pkt->s7p_hdr->cmd, pkt->s7p_hdr->camID);
	if(pkt->s7p_hdr->fpID >= 0xFA00) printf("sopt=0x%04X, ",pkt->s7p_hdr->fpID);
	else printf("sopt=%d, ",pkt->s7p_hdr->fpID);
	printf("val=%d (0x%X)\n",pkt->s7p_hdr->val,pkt->s7p_hdr->val);
}


void swapEndian_u32buffer(u8* out,const u8* in, int size_inB) {
	u32 *p_i32=(u32*)in;
	u32 *p_o32=(u32*)out;
	size_inB -= size_inB%4; //Remove the bytes that can't make a 32 words.
	const u32* end = p_i32 + size_inB/4;
	while(p_i32< end) {
		//printf("swap: %08x (0x%X) > %08x (0x%X)\n",*p_i32,(u32)p_i32,*p_o32,(u32)p_o32);
		swapEndian_u32(p_o32,p_i32);
		//printf("swap: %08x (0x%X) > %08x (0x%X)\n",*p_i32,(u32)p_i32,*p_o32,(u32)p_o32);
		p_i32++;
		p_o32++;
	}
}

void swapEndian_u32(u32 *out,const u32 *in) {
	u8 *p = (u8*)in;
	*out=0;
	(*out)+= (p[0] << 24);
	(*out)+= (p[1] << 16);
	(*out)+= (p[2] << 8);
	(*out)+= p[3];
}

void swapEndian_u16(u16 *out,const u16 *in) {
	u8 *p = (u8*)in;
	(*out)=0;
	(*out)+= (p[0] << 8);
	(*out)+= p[1];
}

/**
 * @brief Function which takes the general parameters.
 * @note This function could go in an general network file
 */
netsocket_t* bindSocket(Params *params) {
	u_char bcEHA[] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
	char ifname[255];
	char filter[255];
	netsocket_t *sock = (netsocket_t*)malloc(sizeof(netsocket_t)); //Init the memory of the net socket.

	switch(params->net.mode) {
	case NET_MODE_MAC:
		sock->mode=params->net.mode;
		sock->mac=(ifhard_s*)malloc(sizeof(ifhard_s)); //Init the memory of the interface hardware socket.
		sock->udp=NULL;

		if(CompareEHA(params->net.eha,bcEHA)) {
			NetMAC_FindIfname(ifname);
			NetMAC_Ifname2EHA(ifname,params->net.eha);
		}
		else {
			NetMAC_EHA2Ifname(params->net.eha,ifname);
		}
		NetMAC_openInterface(ifname,sock->mac);
		sprintf(filter,"ether proto 0x%X and not ether src %02X:%02X:%02X:%02X:%02X:%02X",
				MAC_TYPE_S7P,params->net.eha[0],params->net.eha[1],params->net.eha[2],
				params->net.eha[3],params->net.eha[4],params->net.eha[5]);
		NetMAC_setFilter(filter,sock->mac);


		break;
	case NET_MODE_UDP:
		printf("Not implemented yet");
		exit(EXIT_FAILURE);
		break;
	default:
		printf("Unknown network mode");
		exit(EXIT_FAILURE);
	}
	printf("\n");
	return sock;
}



/**
 * @brief Close the socket before exiting the software.
 * @param socket A pointer on the general netsocket.
 * @see S7P_close().
 */
void closeSocket(netsocket_t* socket) {
	if(socket->udp != NULL) {
		free(socket->udp);
		socket->udp=NULL;
	}

	if(socket->mac != NULL) {
		free(socket->mac);
		socket->mac=NULL;
	}

	free(socket);
	socket=NULL;	//Not usefull because value only valid in function.
}

void msleep(int msec) {
	//Look at Sleep() -> win32
	//Look at nanosleep()
}

void wait ( int seconds )
{


	clock_t endwait;
	endwait = clock () + seconds * CLOCKS_PER_SEC ;
	while (clock() < endwait) {}
}
