## Copyright (C) 2008 by Benoit RAT
## benoit@sevensols.com


## CMake compatibility issues: don't modify this, please!
CMAKE_MINIMUM_REQUIRED( VERSION 2.6 )

## Set PROJECT_NAME (${PROJECT_NAME}_BINARY_DIR and ${PROJECT_NAME}_SOURCE_DIR)
PROJECT(S7PReceiver)
set(${PROJECT_NAME}_MAJOR_VERSION 0 )
set(${PROJECT_NAME}_MINOR_VERSION 1 )

## Set Path for cmake scripts.
set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/devtools/cmake/)

## Set path for executable output path
set( EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin )

## Set Verbose Makefile
SET(CMAKE_VERBOSE_MAKEFILE ON)

##Project options
OPTION( BUILD_SHARED_LIBS "Set to OFF to build static libraries" ON )
OPTION( INSTALL_DOC "Set to OFF to skip build/install Documentation" ON )

## Change the build mode (None Debug Release RelWithDebInfo MinSizeRel)
IF(NOT CMAKE_BUILD_TYPE)
  SET(CMAKE_BUILD_TYPE Debug CACHE STRING
      "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel."
      FORCE)
ENDIF(NOT CMAKE_BUILD_TYPE)


## Add the MinGW include directory to the path because Eclipse can not find it.
IF(MINGW)
    include_directories(C:/MinGW/include)
ENDIF(MINGW)


## Add subdirectory
ADD_SUBDIRECTORY(src)


##Copy config file
# Handle single-configuration generators.
CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/config-sample.xml
               ${EXECUTABLE_OUTPUT_PATH}/config-sample.xml COPYONLY)
  


set(CPACK_SOURCE_IGNORE_FILES
    "~$"
    "/\\\\.svn/"
#     "^${PROJECT_SOURCE_DIR}/old/"
)